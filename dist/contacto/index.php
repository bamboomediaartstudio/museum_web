<?php

include('../museumSmartAdmin/dist/default/private/users/core/init.php');

?>

<!DOCTYPE html>

<head>

	<?php include_once('../private/php/includes/structure/meta.php');?>

	<title>Museo Del Holocausto - Contacto</title>

	<link href="../private/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<link href="../private/css/fontawesome/all.css" rel="stylesheet" type="text/css">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

	<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

	<link href="../private/css/sweetalert2.min.css" rel="stylesheet">

	<link href="../private/css/style.css" rel="stylesheet">

	<link href="../private/css/contact-override.css" rel="stylesheet">

	<?php include_once('../private/php/includes/structure/favicons.php');?>

</head>

<body id="page-top">

	<?php include('../private/php/includes/structure/nav.php');?>

	<?php include('../private/php/includes/modules/contact.php');?>

	<?php include('../private/php/includes/structure/footer.php');?>

	<script src="../private/vendor/jquery/jquery.min.js"></script>

	<script src="../private/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<script src="../private/vendor/jquery-easing/jquery.easing.min.js"></script>

	<script src="../private/vendor/scrollreveal/scrollreveal.min.js"></script>

	<script src="../private/vendor/gsap/src/minified/TweenMax.min.js"></script>

	<script src="../private/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

	<script src="../private/vendor/slick/slick.min.js"></script>
	
	<script src="../private/vendor/block-ui/jquery.blockUI.js"></script>

	<script src="../private/vendor/jquery-validation/jquery.validate.min.js"></script>

	<script src="../private/vendor/sweetalert2/sweetalert2.all.min.js"></script>

	<script src="../private/vendor/jquery-form/jquery.form.min.js"></script>

	<script src="../private/js/generalConf.min.js"></script>

	<script src="../private/js/helpers.min.js"></script>

	<script src="../private/js/contact.min.js"></script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtjbRNPncz0LJT5WPKwuTS4tPoOWPVU5U&callback=initMap" async defer></script>


</body>

</html>