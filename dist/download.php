<?php

/**
 * Transfer Files Server to Server using PHP Copy
 */
 
/* Source File URL */
$remote_file_url = 'http://www.aditivointeractivegroup.com/cuquito.jpg';

/* New file name and path for this file */
$local_file = 'testing/temp/images/cuquito-downloaded.jpg';
 
/* Copy the file from source url to server */
$copy = copy( $remote_file_url, $local_file );
 
/* Add notice for success/failure */
if( !$copy ) {
    echo "Doh! failed to copy...";
}
else{
    echo "WOOT! success to copy...";
}

?>