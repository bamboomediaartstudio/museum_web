<?php

include('../../museumSmartAdmin/dist/default/private/users/core/init.php');

$wording = new Wording();

$wording->load('prensa', 'prensa');
      
?>

<!DOCTYPE html>

<head>

      <?php include_once('../../private/php/includes/structure/meta.php');?>

      <title>Museo Del Holocausto - Capsulas Educativas</title>

      <link href="../../private/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

      <link href="../../private/css/fontawesome/all.css" rel="stylesheet" type="text/css">

      <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

      <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

      <link href="../../private/css/effects.css" rel="stylesheet" type="text/css">

      <link href="../../private/css/style.css" rel="stylesheet">
      
      <link href="../../private/css/capsulas.css" rel="stylesheet">

      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css"/>

      <?php include_once('../../private/php/includes/structure/favicons.php');?>

</head>

<body id="page-top">

      <style>

      .slick-next{ right: 20px; }

      .slick-prev{ left: 20px; }

</style>

<?php include('../../private/php/includes/structure/nav.php');?>

<div class="modal fade" tabindex="-1" role="dialog">

      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">

            <div class="modal-content">

                  <div class="modal-header">

                        <h5 class="modal-title"></h5>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                              <span aria-hidden="true">&times;</span>

                        </button>

                  </div>

                  <div class="modal-body">

                  </div>

                  <div class="modal-footer">

                        <button type="button" class="bridge-to-file read-btn-capsule btn button overide"></button>

                        <button type="button" class="read-btn-capsule btn button overide" data-dismiss="modal">CERRAR</button>

                  </div>

            </div>

      </div>

</div>

<?php include('../../private/php/includes/headers/capsulas-header.php');?>

<?php include('../../private/php/includes/modules/capsulas/capsulas-list.php');?>

<?php include('../../private/php/includes/structure/footer.php');?>

<script src="../../private/vendor/jquery/jquery.min.js"></script>

<script src="../../private/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="../../private/vendor/scrollreveal/scrollreveal.min.js"></script>

<script src="../../private/vendor/gsap/src/minified/TweenMax.min.js"></script>

<script src="../../private/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

<script src="../../private/vendor/slick/slick.min.js"></script>

<script src="../../private/vendor/imagesloaded/imagesloaded.pkgd.js"></script>

<script src="../../private/vendor/isotope-layout/isotope.pkgd.min.js"></script>

<script src="../../private/js/generalConf.min.js"></script>

<script src="../../private/js/helpers.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.umd.js"></script>

<script src="../../private/js/capsules.min.js"></script>

</body>

</html>