<?php

include('../museumSmartAdmin/dist/default/private/users/core/init.php');

header( "Content-type: text/xml");

echo "<?xml version='1.0' encoding='UTF-8'?>

<rss version='2.0'>

<channel>

<title>Museo Del Holocausto | RSS | Noticias</title>

<link>https://www.museodelholocausto.org.ar</link>

<description>RSS de noticias del Museo Del Holocausto, Buenos Aires, Argentina</description>

<language>es-ES</language>

<copyright>" . date("Y") . " Museo Del Holocausto, Buenos Aires, Argentina. Todos los derechos reservados.</copyright>

<webmaster>info@museodelholocausto.org.ar</webmaster>";

$db = DB::getInstance();

$result = $db->query('

	SELECT *, 

	mc.id                                     as mid, 

	img.unique_id                             as uid

	from museum_news                          as mc

	LEFT JOIN museum_images                   as img 

	ON                                        mc.id = img.sid 

	AND img.source                            = ?

	AND img.active                            = ?

	AND img.deleted                           = ?

	WHERE mc.active                           = ? 

	AND mc.deleted                            = ? 

	order by                                   mc.id' ,

	['news', 1, 0, 1, 0]);

foreach($result->results() as $result){

	$finalUrl = 'https://www.museodelholocausto.org.ar';

	if($result->uid != null){

		$defaultImg = true;

		$myImg = $finalUrl . '/museumSmartAdmin/dist/default/private/sources/images/news/' . $result->mid . '/' . $result->uid . '/' . $result->mid . '_' . $result->uid . '_original.jpeg';

	}else{

		$defaultImg = false;

		$myImg = $finalUrl . '/private/img/templates/image-template/template_original.jpg';

	}

	echo 

	'<item>

	<title>' . $result->title . '</title>

	<link>https://www.museodelholocausto.org.ar/prensa/'. $result->url . '</link>

	<description>' . utf8_encode(html_entity_decode(strip_tags($result->content))) . '</description>

	<author>Museo Del Holocausto</author>

	<pubDate>' . $result->added . '</pubDate>

	<image> ' . $myImg .  ' </image>

	<category>Noticias</category>

	</item>';

}

echo "</channel></rss>";

?>