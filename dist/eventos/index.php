<?php

include('../museumSmartAdmin/dist/default/private/users/core/init.php');

$firstLine = 'TE INTERESAN NUESTROS';

$secondLine = 'EVENTOS';

$thirdLine = 'SUSCRIBITE A NUESTRO NEWSLETTER Y RECIBILOS EN TU EMAIL.';

$wording = new Wording();

$wording->load('home', 'eventos');

?>

<!DOCTYPE html>

<head>

      <?php include_once('../private/php/includes/structure/meta.php');?>

      <title>Museo Del Holocausto - Eventos</title>

      <link href="../private/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

      <link href="../private/css/fontawesome/all.css" rel="stylesheet" type="text/css">

      <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

      <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

      <link href="../private/css/effects.css" rel="stylesheet" type="text/css">

      <link href="../private/css/style.css" rel="stylesheet">

      <link href="../private/vendor/toastr/toastr.min.css" rel="stylesheet">

      <?php include_once('../private/php/includes/structure/favicons.php');?>

</head>

<body id="page-top">

      <style>

      .slick-next{ right: 20px; }

      .slick-prev{ left: 20px; }

</style>

<script type="text/javascript">(function () {
            if (window.addtocalendar)if(typeof window.addtocalendar.start == "function")return;
            if (window.ifaddtocalendar == undefined) { window.ifaddtocalendar = 1;
                var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                s.type = 'text/javascript';s.charset = 'UTF-8';s.async = true;
                s.src = ('https:' == window.location.protocol ? 'https' : 'http')+'://addtocalendar.com/atc/1.5/atc.min.js';
                var h = d[g]('body')[0];h.appendChild(s); }})();
    </script>


<?php include('../private/php/includes/structure/nav.php');?>

<?php include('../private/php/includes/headers/events-header.php');?>

<?php include('../private/php/includes/modules/events/events-list.php');?>

<?php //include('../private/php/includes/modules/newsletter.php');?>

<?php //include('../private/php/includes/modules/newsletter-left.php');?>

<?php include('../private/php/includes/structure/footer.php');?>

<script src="../private/vendor/jquery/jquery.min.js"></script>

<script src="../private/vendor/jquery-validation/jquery.validate.min.js"></script>

<script src="../private/vendor/jquery-form/jquery.form.min.js"></script>

<script src="../private/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="../private/vendor/scrollreveal/scrollreveal.min.js"></script>

<script src="../private/vendor/gsap/src/minified/TweenMax.min.js"></script>

<script src="../private/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

<script src="../private/vendor/imagesloaded/imagesloaded.pkgd.js"></script>

<script src="../private/vendor/isotope-layout/isotope.pkgd.min.js"></script>

<script src="../private/vendor/slick/slick.min.js"></script>

<script src="../private/vendor/addtocalendar/addtocalendar.js"></script>

<script src="../private/vendor/toastr/toastr.min.js"></script>

<script src="../private/js/generalConf.min.js"></script>

<script src="../private/js/helpers.min.js"></script>

<script src="../private/js/events.min.js"></script>

</body>

</html>