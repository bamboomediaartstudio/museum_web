<?php

include('museumSmartAdmin/dist/default/private/users/core/init.php');

$wording = new Wording();

$wording->load('home', 'home');

$myValue = $wording->get('landing_second_line');

$origin = 'web';

?>

<!DOCTYPE html>

<html lang="es">

<head>

      <?php include_once('private/php/includes/structure/meta.php');?>

      <title>Museo Del Holocausto</title>

      <link href="private/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

      <link href="private/css/fontawesome/all.css" rel="stylesheet" type="text/css">

      <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

      <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

      <link href="private/css/sweetalert2.min.css" rel="stylesheet">

      <link href="private/vendor/toastr/toastr.min.css" rel="stylesheet">

      <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.css" rel="stylesheet">

      <link href="private/css/style.css" rel="stylesheet">

      <?php include_once('private/php/includes/structure/favicons.php');?>

</head> 

<body id="page-top">

      <?php include('private/php/includes/modals/alerts-modal.php');?>

      <?php include('private/php/includes/modals/sponsors-modal.php');?>

      <?php include('private/php/includes/modals/individual-reservation.php');?>

      <?php include('private/php/includes/modals/group-reservation.php');?>

      <?php include('private/php/includes/structure/nav.php');?>  

      <?php include('private/php/includes/headers/home.php');?>

      <?php

      $origin = 'home';

      $modulesQuery = DB::getInstance()->query('SELECT * from museum_home_modules WHERE active = ? ORDER BY internal_order ASC', [1]);

      foreach($modulesQuery->results() as $result){

            if (strpos($result->url, 'header') !== false){

                  $path = 'private/php/includes/headers/';

            }else{

                  $path = 'private/php/includes/modules/';

            }

            include($path . $result->url);
      }

      ?>

      

      <?php include('private/php/includes/structure/footer.php');?>

      <script src="private/vendor/jquery/jquery.min.js"></script>

      <script src="private/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

      <script src="private/vendor/jquery-easing/jquery.easing.min.js"></script>

      <script src="private/vendor/scrollreveal/scrollreveal.min.js"></script>

      <script src="private/vendor/gsap/src/minified/TweenMax.min.js"></script>

      <script src="private/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

      <script src="private/vendor/slick/slick.min.js"></script>

      <script src="private/vendor/block-ui/jquery.blockUI.js"></script>

      <script src="private/vendor/jquery-validation/jquery.validate.min.js"></script>

      <script src="private/vendor/sweetalert2/sweetalert2.all.min.js"></script>

      <script src="private/vendor/jquery-form/jquery.form.min.js"></script>

      <script src="private/vendor/toastr/toastr.min.js"></script>

      <script src="private/js/generalConf.min.js"></script>

      <script src="private/js/helpers.min.js"></script>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/locale/es.js"></script>

      <script src="private/js/main.min.js"></script>

      <script src="private/js/booking.min.js"></script>

      <!--<script src="private/js/contact.min.js"></script>-->
      
      <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtjbRNPncz0LJT5WPKwuTS4tPoOWPVU5U&callback=initMap" async defer></script>-->

</body>

</html>
