<?php
session_start();

$session_referer = $_SESSION["origURL"];

unset($_SESSION['origURL']);

//country data

$ip = $_SERVER['REMOTE_ADDR'];

$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}"));

//echo $details->country;

//...

require_once '../museumSmartAdmin/dist/default/private/users/core/init.php';

$db = DB::getInstance();

$status['init'] = true;

$ip = Helpers::getIP();

$os = Helpers::getOS();

$browser = Helpers::getBrowser();

$added = date("Y-m-d H:i:s");

$fields = array(

  'session_referer'=>$session_referer, 

  'referer' => $_SERVER["HTTP_REFERER"],

  'language' => $_SERVER['HTTP_ACCEPT_LANGUAGE'],

  'country' => $details->country,

  'region' => $details->region,

  'city' => $details->city,

  'loc' => $details->loc,

  'ip'=>$ip, 

  'browser'=>$browser,

  'os' => $os,
  
  'added' => $added

);

$db->insert('log_maintenance', $fields);

?>


<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="robots" content="noindex, nofollow">
    
    <meta name="description" content="">
    
    <meta name="author" content="">

    <title>MUSEO DEL HOLOCAUSTO / Sitio en Mantenimiento</title>

    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <link href="css/coming-soon.css" rel="stylesheet">

  </head>

  <body>

    <div class="overlay">
         
    </div>

   

    <div class="masthead">
      <div class="masthead-bg"></div>
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-12 my-auto">
            <div class="masthead-content text-white py-5 py-md-0">
              <h1 class="mb-3">SITIO EN MANTENIMIENTO</h1>
              <p class="mb-5">Estamos llevando a cabo tareas de actualización y mantenimiento que requieren que nuestro sitio esté momentáneamente offline.<br><br>Te invitamos a seguir en contacto con nosotros a través de nuestras redes sociales.</p>
      
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="social-icons">
      <ul class="list-unstyled text-center mb-0">
        <li class="list-unstyled-item">
          <a href="https://www.instagram.com/museoshoa/">
            <i class="fab fa-instagram"></i>
          </a>
        </li>
        <li class="list-unstyled-item">
          <a href="https://twitter.com/museoshoa?lang=es">
            <i class="fab fa-twitter"></i>
          </a>
        </li>
        <li class="list-unstyled-item">
          <a href="https://www.facebook.com/museoshoa/">
            <i class="fab fa-facebook-f"></i>
          </a>
        </li>
        
        
        <li class="list-unstyled-item">
          <a href="https://www.youtube.com/user/MuseodelHolocausto">
            <i class="fab fa-youtube"></i>
          </a>
        </li>
      </ul>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/coming-soon.min.js"></script>

  </body>

</html>
