<?php

include('../museumSmartAdmin/dist/default/private/users/core/init.php');

$firstLine = 'TE INTERESAN NUESTRAS';

$secondLine = 'MUESTRAS';

$thirdLine = 'SUSCRIBITE A NUESTRO NEWSLETTER.';

$wording = new Wording();

$wording->load('muestras', 'muestras');

$test = $wording->get('exhibitions_first_line');

?>

<!DOCTYPE html>

<head>

      <?php include_once('../private/php/includes/structure/meta.php');?>

      <title>Museo Del Holocausto - Muestras</title>

      <link href="../private/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

      <link href="../private/css/fontawesome/all.css" rel="stylesheet" type="text/css">

      <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

      <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

      <link href="../private/css/effects.css" rel="stylesheet" type="text/css">

      <link href="../private/css/style.css" rel="stylesheet">

      <?php include_once('../private/php/includes/structure/favicons.php');?>

</head>

<body id="page-top">

      <style>

      .slick-next{ right: 20px; }

      .slick-prev{ left: 20px; }

</style>


<?php include('../private/php/includes/structure/nav.php');?>

<?php include('../private/php/includes/headers/exhibitions-header.php');?>

<?php include('../private/php/includes/modules/exhibitions/exhibitions-list.php');?>

<?php include('../private/php/includes/structure/footer.php');?>

<script src="../private/vendor/jquery/jquery.min.js"></script>

<script src="../private/vendor/jquery-validation/jquery.validate.min.js"></script>

<script src="../private/vendor/jquery-form/jquery.form.min.js"></script>

<script src="../private/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="../private/vendor/scrollreveal/scrollreveal.min.js"></script>

<script src="../private/vendor/gsap/src/minified/TweenMax.min.js"></script>

<script src="../private/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

<script src="../private/vendor/imagesloaded/imagesloaded.pkgd.js"></script>

<script src="../private/vendor/isotope-layout/isotope.pkgd.min.js"></script>

<script src="../private/vendor/slick/slick.min.js"></script>

<script src="../private/js/generalConf.min.js"></script>

<script src="../private/js/helpers.min.js"></script>

<script src="../private/js/exhibitions.min.js"></script>

</body>

</html>