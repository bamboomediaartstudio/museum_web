<?php

include('../../museumSmartAdmin/dist/default/private/users/core/init.php');

include_once('../../private/php/libs/Mobile_Detect.php');

$page_file_temp = $_SERVER["PHP_SELF"];

$page_directory = dirname($page_file_temp);

$url = $page_directory;

$tokens = explode('/', $url);

$path =  $tokens[sizeof($tokens)-1];

$db = DB::getInstance();

$defaultImg = false;

$query = $db->query(

	'SELECT *, mc.id as mid, img.unique_id 				as uid,

	mc.description 										as exhibition_description

	from museum_exhibitions								as mc

	LEFT JOIN museum_images 							as img 

	ON mc.id = img.sid AND img.source 					= ?

	WHERE mc.url = ?', ['exhibitions', $path]);


$itemResult = $query->first();

if($itemResult->uid != null){

	$defaultImg = true;

	$myImg = '../../museumSmartAdmin/dist/default/private/sources/images/exhibitions/' . $itemResult->mid . '/' . $itemResult->uid . '/' . $itemResult->mid . '_' . $itemResult->uid . '_original.jpeg';

	$myImgRetina = '../../museumSmartAdmin/dist/default/private/sources/images/exhibitions/' . $itemResult->mid . '/' . $itemResult->uid . '/' . $itemResult->mid . '_' . $itemResult->uid . '_original@2x.jpeg';

}else{

	$defaultImg = false;

	$myImg = '../../private/img/templates/image-template/template_original.jpg';

	$myImgRetina = '../../private/img/templates/image-template/template_original@2x.jpg';

}

$actualLink = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

//tutors...

?>

<head>

	<!-- general meta -->

	<?php include_once('../../private/php/includes/structure/meta.php');?>

	<!-- Facebook-->

	<meta property="og:image" content="<?php echo $myImg;?>">

	<meta property="og:image:height" content="1000">

	<meta property="og:image:width" content="2000">

	<meta property="og:title" content="<?php echo $itemResult->name;?>">

	<meta property="og:description" content="<?php echo $itemResult->description;?>">

	<meta property="og:url" content="<?php echo $itemResult->actualLink;?>">

	<!-- Twitter -->

	<meta name="twitter:card" content="summary" />

	<meta name="twitter:site" content="@museoshoa" />

	<meta name="twitter:title" content="<?php echo $itemResult->name;?>"/>

	<meta name="twitter:description" content="<?php echo $itemResult->description;?>"/>

	<meta name="twitter:image" content="<?php echo $myImg;?>" />

	<!-- title -->

	<title><?php echo $itemResult->name;?></title>

	<!-- styles -->

	<link href="../../private/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<link href="../../private/css/sweetalert2.min.css" rel="stylesheet">

	<link href="../../private/css/fontawesome/all.css" rel="stylesheet" type="text/css">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

	<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

	<link href="../../private/vendor/blueimp-gallery/css/blueimp-gallery.min.css" rel="stylesheet" type="text/css">


	<link href="../../private/css/style.css" rel="stylesheet">

	<link href="../../private/css/effects.css" rel="stylesheet" type="text/css">


	<?php include_once('../../private/php/includes/structure/favicons.php');?>

</head>

<body id="page-top">

	<?php include('../../private/php/includes/modals/exhibition-request.php');?>

	<?php include('../../private/php/includes/structure/nav.php');?>

	<input type="hidden" id="item-id" name="item-id" value="<?php echo $itemResult->mid;?>">
	
	<input type="hidden" id="item-img" name="item-img" value="<?php echo $myImg;?>">

	<input type="hidden" id="item-img-retina" name="item-img-retina" value="<?php echo $myImgRetina;?>">
	
	<input type="hidden" id="default-img" name="default-img" value="<?php echo $defaultImg;?>">

	<?php include('../../private/php/includes/headers/exhibitions/individual-exhibition-header.php');?>

	<?php include('../../private/php/includes/modules/exhibitions/individual-exhibition-content.php');?>

	<script src="../../private/vendor/jquery/jquery.min.js"></script>

	<script src="../../private/vendor/jquery-easing/jquery.easing.min.js"></script>

	<script src="../../private/vendor/scrollreveal/scrollreveal.min.js"></script>

	<script src="../../private/js/production.min.js"></script>

	<script src="../../private/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<script src="../../private/vendor/gsap/src/minified/TweenMax.min.js"></script>

	<script src="../../private/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

	<script src="../../private/vendor/jquery-validation/jquery.validate.min.js"></script>

	<script src="../../private/vendor/jquery-form/jquery.form.min.js"></script>

	<script src="../../private/vendor/sweetalert2/sweetalert2.all.min.js"></script>

	<script src="../../private/vendor/blueimp-gallery/js/blueimp-gallery.min.js"></script>

	<script src="../../private/vendor/blueimp-gallery/js/blueimp-gallery-youtube.js"></script>

	<script src="../../private/vendor/block-ui/jquery.blockUI.js"></script>

	<script src="../../private/js/generalConf.min.js"></script>

	<script src="../../private/js/helpers.min.js"></script>

	<script src="../../private/js/individualExhibition.min.js"></script>

	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5be1e9ed36bf95da"></script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtjbRNPncz0LJT5WPKwuTS4tPoOWPVU5U&callback=initMap" async defer></script>



</body>