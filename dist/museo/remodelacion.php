<?php

include('../museumSmartAdmin/dist/default/private/users/core/init.php');

?>

<!DOCTYPE html>

<head>

      <?php include_once('../private/php/includes/structure/meta.php');?>

      <title>Museo Del Holocausto - La Remodelación</title>

      <link href="../private/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

      <link href="../private/css/fontawesome/all.css" rel="stylesheet" type="text/css">

      <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

      <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

      <link href="../private/vendor/blueimp-gallery/css/blueimp-gallery.min.css" rel="stylesheet" type="text/css">

      <link href="../private/css/style.css" rel="stylesheet">

      <?php include_once('../private/php/includes/structure/favicons.php');?>

</head>

<body id="page-top">

      <?php include('../private/php/includes/structure/nav.php');?>

      <?php include('../private/php/includes/headers/remodelacion-header.php');?>

      <?php include('../private/php/includes/modules/museum/about-remodelacion.php');?>

      <?php include('../private/php/includes/modules/museum/remodelacion-gallery.php');?>

      <script src="../private/vendor/jquery/jquery.min.js"></script>

      <script src="../private/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

      <script src="../private/vendor/jquery-easing/jquery.easing.min.js"></script>

      <script src="../private/vendor/scrollreveal/scrollreveal.min.js"></script>

      <script src="../private/vendor/gsap/src/minified/TweenMax.min.js"></script>

            <script src="../private/vendor/slick/slick.min.js"></script>


      <script src="../private/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

      <script src="../private/vendor/blueimp-gallery/js/blueimp-gallery.min.js"></script>

      <script src="../private/vendor/blueimp-gallery/js/blueimp-gallery-youtube.js"></script>

      <script src="../private/vendor/slick/slick.min.js"></script>

      <script src="../private/vendor/imagesloaded/imagesloaded.pkgd.js"></script>

      <script src="../private/vendor/isotope-layout/isotope.pkgd.min.js"></script>

      <script src="../private/js/generalConf.min.js"></script>

      <script src="../private/js/helpers.min.js"></script>

      <script src="../private/js/remodelacion.min.js"></script>

</body>

</html>