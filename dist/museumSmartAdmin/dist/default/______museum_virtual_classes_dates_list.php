<?php

$origin = 'admin';

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

setlocale(LC_TIME, 'es_ES', 'esp_esp');

$yesterday = date('Y-m-d',strtotime("-1 days"));

$today = date('Y-m-d');

$tomorrow = date('Y-m-d',strtotime("+1 days"));

$dayAfterTomorrow = date('Y-m-d',strtotime("+2 days"));

if(Input::exists('get')){

	if(Input::get('tabId')){

		$tabId = Input::get('tabId');

	}else{

		$tabId = 1;

	}

}else{

	$tabId = 1;

}

$userType = $user->data()->group_id;

$id = $user->data()->id;

//echo "useriD: ".$id;


/*if($user->data()->group_id == 6){

	$isHost = true;

}

if($user->data()->group_id == 4){

	$isGuide = true;

}*/


?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Visitas Virtuales | Turnos</title>

	<meta name="description" content="">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.css"/>


	<style>

		.summernote-description-error{

			color: red !important;

		}

		.museum-finder{

			cursor:pointer;

		}

		#editor {overflow:scroll; max-height:300px !important}

		input::-webkit-outer-spin-button,

		input::-webkit-inner-spin-button {

			-webkit-appearance: none;

			margin: 0;
		}

		.disabled {

			pointer-events: none !important;

			opacity: 0.4 !important;

		}

		#map {

			height: 500px;

			width:100%;

		}

		.tt-query {
			-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
			-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
			box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		}

		.tt-hint {
			color: #999
		}

		.tt-menu {    /* used to be tt-dropdown-menu in older versions */
			width: 422px;
			margin-top: 4px;
			padding: 4px 0;
			background-color: #fff;
			border: 1px solid #ccc;
			border: 1px solid rgba(0, 0, 0, 0.2);
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
			-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
			-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
			box-shadow: 0 5px 10px rgba(0,0,0,.2);
		}

		.tt-suggestion {
			padding: 3px 20px;
			line-height: 24px;
		}

		.tt-suggestion.tt-cursor,.tt-suggestion:hover {
			color: #fff;
			background-color: #0097cf;

		}

		.tt-suggestion p {
			margin: 0;
		}

		.twitter-typeahead{
			width: 100%;
		}

		.see-event{
			color: white !important;
			background-color: green !important;
		}

		.delete-event{
			color: white !important;
			background-color: red !important;
		}

		.define-ammount{
			color: white  !important;
			background-color: #27c9cf !important;
		}

		.define-guide{
			color: white  !important;
			background-color: #8873a2 !important;
		}

		.change-event-place{
			color: white  !important;
			background-color: #8873a2 !important;
		}

		.change-event-visitors{

			color: white  !important;
			background-color: #27c9cf !important;

		}

		.change-tickets-per-visitors{

			color: white  !important;

			background-color: #a2c7b5 !important;
		}

		.is-online{

			color: white !important;

			background-color: #4ebfa9 !important;

		}

		.is-offline{

			color: white !important;

			background-color: #f35872 !important;

		}

		.change-notes{

			color: white !important;

			background-color: #f06200 !important;

		}


		.trash-for-events{

			background-color: #fff;

			height: 8em;

			box-shadow: 0px 1px 15px 1px rgba(69, 65, 78, 0.08);

			border: solid 1px #f5f5f5;

		}


		.date-selector-div{

			background-color: #999 !important;

			width: 100%;
		}


		.tickets-stepper{

			background-color: #999;

			color: white;

			width: 50px;

			border:1px solid #fff !important;
		}

		.selected-date{

			cursor: pointer !important;

			background-color: #274369 !important;
		}

		.fc-highlight{
			background-color: #ff0066 !important;
		}

		.fc-bgevent{
			background-color: #ff0066 !important;
		}

		.open-modal-url{
			cursor:pointer;
		}

		.modal-title-style{
			margin-bottom:2rem;
		}


	</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<input type="hidden" id="yesterday" name="yesterday" value="<?php echo $yesterday;?>">

	<input type="hidden" id="today" name="today" value="<?php echo $today;?>">

	<input type="hidden" id="tomorrow" name="tomorrow" value="<?php echo $tomorrow;?>">

	<input type="hidden" id="dayAfterTomorrow" name="dayAfterTomorrow" value="<?php echo $dayAfterTomorrow;?>">

	<input type="hidden" id="token" name="token" value="<?php echo Token::generate();?>">

	<input type="hidden" id="user-type" name="user-type" value="<?php echo $userType?>">

	<input type="hidden" id="user-id" name="user-id" value="<?php echo $id?>">

	<div id="actions-modal" class="modal" tabindex="-1" role="dialog">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title">Asignar descripción a la imagen</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body">

					<form>

						<div class="form-group">

							<label for="image-	" class="col-form-label">Descripción:</label>

							<input type="text" class="form-control" id="image-description">

						</div>

					</form>

				</div>

				<div class="modal-footer">

					<button type="button" class="btn btn-primary save-description">Guardar Cambios</button>

					<button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<div id="list-modal" class="modal" tabindex="-1" role="dialog">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title-content">Listado</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body-content">

					<div class="tab-pane active" id="tab_3">

						<div class="m-portlet__body pl-3 pr-3 pt-3 pb-">

							<table id="tab-3" class="table display table-striped table-bordered" style="width:100%">

								<thead>

									<tr>

										<th>turno</th>

										<th>Nombre</th>

										<th>Apellido</th>

										<th>Teléfono</th>

										<th>Teléfono</th>

										<th>Email</th>

										<th>activo</th>

										<th>acciones</th>

										<th>cuenta zoom</th>

										<th>zoom url</th>

										<th>zoom id</th>

										<th>zoom password</th>

										<th>guia</th>

										<th>anfitrión</th>

										<th>cantidad de alumnos</th>

										<th>dirección</th>

										<th>provincia</th>

										<th>nivel</th>

										<th>tipo de institución</th>

										<th>persona de contacto</th>

										<th>teléfono del contacto</th>

										<th>email del contacto</th>

										<th>date_start</th>

									</tr>

								</thead>


							</table>

						</div>

					</div>

				</div>

				<div class="modal-footer">

					<button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<!-- Modal change class -->

	<div id="edit-class-modal" class="modal" tabindex="-1" role="dialog">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title-content">Modificar Clase</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body-content">

					<div class="tab-pane active" id="tab_3">

						<form>

							<input id="id_event_modified" name="id_event_modified" type="hidden" value="">

							<div class="m-portlet__body pl-3 pr-3 pt-3 pb-">

								<div class = "row">

									<div class="col-lg-12">

										<div class="m-portlet" id="m_portlet">

											<div class="m-portlet__body">

												<div id="m_calendar_external_events" class="fc-unthemed">

													<div class="align-items-center">

														<div class = "row" id = "virtual-classes-container">

															<div class="modify-form form-group m-form__group form-group m-form__group col-12">

																<label class="col-form-label">Cambiar clase virtual:
																	<br>

																</label>

																<div class="col-12">

																	<select id="selectClassName" title="Seleccioná la clase virtual" name="class_dropdown" class="form-control m-bootstrap-select m_selectpicker_3">

																	</select>

																</div>

															</div>

															<div class="form-group m-form__group col-6">

																<label class="col-form-label">Desde el día:<br>

																</label>

																<div class="col-12">

																	<div id="date-modified-from" class="input-group date">

																		<input type="text" class="form-control" name="calendar-date" autocomplete="off">

																		<div class="input-group-addon">

																			<i class="pt-2 far fa-calendar-alt"></i>

																		</div>

																	</div>

																</div>

															</div>

															<div class="form-group m-form__group col-6">

																<label class="col-form-label">Hasta el día:

																	<br>


																</label>

																<div class="col-12">

																	<div id="date-modified-to" class="input-group date">

																		<input type="text" class="form-control" name="calendar-date" autocomplete="off">

																		<div class="input-group-addon">

																			<i class="pt-2 far fa-calendar-alt"></i>

																		</div>

																	</div>

																</div>

															</div>

															<div class="form-group m-form__group form-group m-form__group col-6">

																<label class="col-form-label">Desde las:

																	<br>

																</label>

																<div class="col-12">

																	<div class="input-group timepicker" >

																		<input autocomplete="off" type="text" id="from-modified-time" class="form-control m-input" readonly="" placeholder="seleccionar horario de comienzo">

																		<span class="input-group-addon">

																			<i class="la la-clock-o mt-2"></i>

																		</span>

																	</div>

																</div>

															</div>

															<div class="form-group m-form__group form-group m-form__group col-6">

																<label class="col-form-label">Hasta las:

																	<br>

																</label>

																<div class="col-12">

																	<div class="input-group timepicker" >

																		<input autocomplete="off" type="text" id="to-modified-time" class="form-control m-input" readonly="" placeholder="seleccionar horario de comienzo">

																		<span class="input-group-addon">

																			<i class="la la-clock-o mt-2"></i>

																		</span>

																	</div>

																</div>

															</div>


														</div>

													</div>

												</div>

											</div>

										</div>

									</div>


								</div>


							</div>

						</form>

					</div>

				</div>

				<div class="modal-footer">

					<button type="button" class="btn btn-primary save-new-class">Guardar Cambios</button>

					<button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<!-- /End modal change class -->

	<!-- Modal show event info -->

	<div id="event-info-modal" class="modal" tabindex="-1" role="dialog">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title-content">Información del evento</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

					<input type="hidden" id="contact_name" name="contact_name" value="">
					<input type="hidden" id="class_name" name="class_name" value="">
					<input type="hidden" id="event_id" name="event_id" value="">

				</div>

				<div class="modal-body-content">

					<div class="tab-pane active" id="tab_3">

						<div class="m-portlet__body pl-3 pr-3 pt-3 pb-">

							<div class = "row">

								<div class="col-12">

									<div class="portlet light">

										<div class="portlet-body">

											<div style="padding: 0.75rem 1.25rem;">

												<p class="">
													Este enlace contiene los datos del evento.
													<br>
													Podes ingresar haciendo click para ver la información compartida, o copiarlo haciendo click en el botón.
												</p>

												<span><a class="event-url" href="" target="_blank"></a><button data-toggle="tooltip" data-placement="bottom" title="Copiar Link" class="copy-link-btn btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-copy"></i></button></span>

												<div class="text-center mt-3">

													<br>

													<!--<p>Enviar este enlace al contacto registrado: <b><span class="send-email-to"></span></b></p>-->

													<button type="button" class="btn btn-primary resend-email-link">Enviar por mail</button>

												</div>

											</div>

										</div>

									</div>

								</div><!-- /.col-12 -->

							</div><!-- /.row -->

							<div class="row">

								<div class="col-12">

									<div class="text-center">

										<p><i>Mail de contacto: <b><span class="send-email-to"></span></b></i></p>

									</div>

								</div>

							</div>


							<div class="row">

								<div class="col-12">

									<div class="card-header border-0 pt-5 mt-3">

										<div class="">

											<h5 class="modal-title-style">Actualmente los datos de zoom son: </h5>

										</div>

										<p>Zoom Cuenta: <b class="zoom-account"></b></p>

	  									<p>Zoom url: <b class="zoom-url"></b></p>

										<p>Zoom ID: <b class="zoom-id"></b></p>

										<p>Zoom Password: <b class="zoom-pass"></b></p>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

				<div class="modal-footer">

					<button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<!-- /End modal url event info -->

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Clases Virtuales: Turnos del día</h3>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="row">

						<div class="col-lg-4">

							<div class="m-portlet" id="m_portlet">

								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">

										<div class="m-portlet__head-title">

											<span class="m-portlet__head-icon">

												<i class="flaticon-add"></i>

											</span>

											<h3 class="m-portlet__head-text">Filtar</h3>

										</div>

									</div>

								</div>

								<div class="m-portlet__body">

									<div id="m_calendar_external_events" class="fc-unthemed">

										<div class="form-group m-form__group row">

											<label class="col-form-label">Desde:

												<br>

											</label>

											<div class="col-12">

												<div id="date-from" class="input-group date">

													<input type="text" class="form-control" name="calendar-date" autocomplete="off">

													<div class="input-group-addon">

														<i class="pt-2 far fa-calendar-alt"></i>

													</div>

												</div>

											</div>

										</div>

										<!-- hasta -->

										<div class="form-group m-form__group row">

											<label class="col-form-label">Hasta:

												<br>

											</label>

											<div class="col-12">

												<div id="date-to" class="input-group date">

													<input type="text" class="form-control" name="calendar-date" autocomplete="off">

													<div class="input-group-addon">

														<i class="pt-2 far fa-calendar-alt"></i>

													</div>

												</div>

											</div>

										</div>

										<!-- end hasta -->


										<div class="m-separator m-separator--dashed m-separator--space"></div>

										<div class="align-items-center">

											<button type="button" class="w-100 go-to-today btn btn-success">hoy</button>

											<div class="m-separator m-separator--dashed m-separator--space"></div>

											<button type="button" class="w-100 go-to-tomorrow btn btn-danger">mañana</button>

											<div class="m-separator m-separator--dashed m-separator--space"></div>

											<button type="button" class="w-100 go-to-day-after-tomorrow btn btn-accent">pasado mañana</button>

										</div>


									</div>

								</div>

							</div>

						</div>

						<div class="col-lg-12">

							<div class="m-portlet" id="m_portlet">

								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">

										<div class="m-portlet__head-title">

											<span class="m-portlet__head-icon">

												<i class="flaticon-calendar-2"></i>

											</span>

											<h3 class="m-portlet__head-text">

												Listados

											</h3>

										</div>

									</div>

									<?php if($userType != 4 && $userType != 6){ ?>

									<div class="m-portlet__head-tools">

										<ul class="m-portlet__nav">

											<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">

												<a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
													Acciones
												</a>
												<div class="m-dropdown__wrapper">

													<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 40.2222px;"></span>

													<div class="m-dropdown__inner">

														<div class="m-dropdown__body">

															<div class="m-dropdown__content">

																<ul class="m-nav">

																	<li class="m-nav__item">

																		<a href="museum_virtual_classes_list.php" class="m-nav__link">

																			<i class="m-nav__link-icon flaticon-list"></i>

																			<span class="m-nav__link-text">

																				ir a la lista de clases virtuales

																			</span>

																		</a>

																	</li>

																	<li class="m-nav__item">

																		<a href="museum_virtual_classes_batch.php" class="m-nav__link">

																			<i class="m-nav__link-icon flaticon-file"></i>

																			<span class="m-nav__link-text">

																				agregar en batch

																			</span>

																		</a>

																	</li>

																	<li class="m-nav__item add-date">

																		<a href="#" class="m-nav__link">

																			<i class="m-nav__link-icon flaticon-calendar"></i>

																			<span class="m-nav__link-text">

																				agregar Fecha!

																			</span>

																		</a>

																	</li>

																	<li class="m-nav__item export-excel">

																		<a href="#" class="m-nav__link">

																			<i class="m-nav__link-icon flaticon-graph"></i>

																			<span class="m-nav__link-text">

																				Exportar Excel

																			</span>

																		</a>

																	</li>

																</ul>

															</div>

														</div>

													</div>

												</div>

											</li>

										</ul>

									</div>

									<?php } ?>

								</div>

								<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">

									<div class="m-portlet__head">

										<div class="m-portlet__head-tools">

											<ul id="myTab" class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--left m-tabs-line--primary"
											role="tablist">

											<li class="nav-item m-tabs__item" data-id="'. $categoryResult->id .'">

												<a class="nav-link m-tabs__link active " data-toggle="tab" href="#tab_1" role="tab">

												<i class="flaticon-share m--hide"></i>Instituciones</a>

											</li>

											<li class="nav-item m-tabs__item" data-id="'. $categoryResult->id .'">

												<a class="nav-link m-tabs__link " data-toggle="tab" href="#tab_2" role="tab">

												<i class="flaticon-share m--hide"></i>Personas</a>

											</li>


										</ul>

									</div>

								</div>

								<div class="tab-content">

									<div class="tab-pane active" id="tab_1">

										<div class="m-portlet__body">

											<table id="tab-1" class="table display table-striped table-bordered responsive nowrap" style="width:100%">

												<thead>

													<tr>

														<th>turno</th>

														<th><i class="fas fa-exclamation-triangle text-warning"></i></th>

														<th>fecha</th>

														<th>hora</th>

														<th>clase <i data-toggle="tooltip" title="Reenviar enlace con datos de clase. Podes ver más información haciendo click en la clase" class="fas fa-info-circle"></i></th>

														<th>institución</th>

														<th>activo</th>

														<th>acciones</th>

														<th>cuenta zoom</th>

														<th>zoom url</th>

														<th>zoom id</th>

														<th>zoom password</th>

														<th>guia</th>

														<th>anfitrión</th>

														<th>cantidad de alumnos</th>

														<th>dirección</th>

														<th>provincia</th>

														<th>nivel</th>

														<th>tipo de institución</th>

														<th>persona de contacto</th>

														<th>teléfono del contacto</th>

														<th>email del contacto</th>

														<th>date_start</th>

													</tr>

												</thead>

											</table>

										</div>

									</div>

									<div class="tab-pane" id="tab_2">

										<div class="m-portlet__body">

											<table id="tab-2" class="table display table-striped table-bordered responsive nowrap" style="width:100%" width="100%">

												<thead>

													<tr>

														<th>turno</th>

														<th><i class="fas fa-exclamation-triangle text-warning"></i></th>

														<th>fecha</th>

														<th>hora</th>

														<th>clase</th>

														<th>inscriptos</th>

														<th>activo</th>

														<th>acciones</th>

														<th>cuenta zoom</th>

														<th>zoom url</th>

														<th>zoom id</th>

														<th>zoom password</th>

														<th>guia</th>

														<th>anfitrión</th>

														<th>cantidad de alumnos</th>

														<th>dirección</th>

														<th>provincia</th>

														<th>nivel</th>

														<th>tipo de institución</th>

														<th>persona de contacto</th>

														<th>teléfono del contacto</th>

														<th>email del contacto</th>

														<th>date_start</th>

													</tr>

												</thead>

											</table>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/vendors/custom/jquery-ui/jquery-ui.bundle.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.es.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>

<script src="https://cdn.datatables.net/plug-ins/1.10.21/sorting/datetime-moment.js"></script>




<script src="../../../private/vendor/gsap/src/minified/TweenMax.min.js"></script>

<script src="../../../private/js/generalConf.min.js" type="text/javascript"></script>

<script src="../../../private/js/helpers.min.js" type="text/javascript"></script>

<script src="assets/app/js/cucumberry/FormUtils.js" type="text/javascript"></script>

<script src="assets/app/js/cucumberry/Utils.js" type="text/javascript"></script>

<script src="assets/app/js/museum/classes/museum-virtual-classes-dates-list.js" type="text/javascript"></script>

</body>

</html>
