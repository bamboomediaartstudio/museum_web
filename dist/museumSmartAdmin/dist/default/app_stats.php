<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

setlocale(LC_ALL, "es_ES", 'Spanish_Spain', 'Spanish');

//---------------------------------------------------------------------------------------------------------------
//interactions hoy...

$realUsers = DB::getInstance()->query('SELECT COUNT(*) as total_users FROM app_device_users')->first();

$usersSessions = DB::getInstance()->query('SELECT COUNT(*) as total_sessions FROM app_device_user_sessions')->first();

$usersSessionsToday = DB::getInstance()->query('SELECT *, COUNT(*) as total_sessions FROM app_device_user_sessions as db WHERE DATE(db.session) = DATE(CURDATE())' )->first();

$sessionsOfTheWeek = DB::getInstance()->query('SELECT *, COUNT(*) as total_sessions FROM app_device_user_sessions as db WHERE YEARWEEK(db.session) = YEARWEEK(CURDATE())' )->first();

$sessionsOfTheMonth = DB::getInstance()->query("SELECT *, COUNT(*) as total_sessions FROM app_device_user_sessions as db WHERE (db.session between DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW())")->first();

$sessionsOfTheYear = DB::getInstance()->query("SELECT *, COUNT(*) as total_sessions FROM app_device_user_sessions as db WHERE YEAR(db.session) = YEAR(CURDATE())")->first();


$unlocks = DB::getInstance()->query("SELECT * FROM app_museum_categories");

foreach($unlocks->results() as $result){

	if($result->name == 'Perpetradores'){ $perpetradoresCounter = $result->unlocked_times; }
	
	if($result->name == 'Justos Entre Las Naciones'){ $justosCounter = $result->unlocked_times; }

	if($result->name == 'Insignias'){ $insigniasCounter = $result->unlocked_times; }
	
	if($result->name == 'Guetos'){ $guetosCounter = $result->unlocked_times; }
	









	if($result->name == 'Guetos'){ $guetosCounter = $result->unlocked_times; }

}


$monicaQuery = DB::getInstance()->query("SELECT sum(unlocked_times) as unlocked_times from beacons_families_relations WHERE id_family = 1 GROUP by id_family")->first();


$moisesQuery = DB::getInstance()->query("SELECT sum(unlocked_times) as unlocked_times from beacons_families_relations WHERE id_family = 2 GROUP by id_family")->first();

//echo $monicaQuery->unlocked_times;


//SELECT *, COUNT(cicks.id) as interactions from app_device_menu as menu inner join app_device_menu_clicks as cicks on menu.section_id = cicks.app_id GROUP by menu.section_id


//lista
//SELECT menu.name as app_name, COUNT(cicks.id) as interactions from app_device_menu as menu inner join app_device_menu_clicks as cicks on menu.section_id = cicks.app_id WHERE menu.deleted = 0 AND menu.active = 1 GROUP by menu.section_id 


?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Estadísticas | Encuesta</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() { sessionStorage.fonts = true; }

		});
		
	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
	
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="modal" tabindex="-1" role="dialog">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title"></h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body">

					<div class="m-widget24 add-data">

					</div>

				</div>

				<div class="modal-footer">

					<button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<input type="hidden" name="user-id" id="user-id" value="<?php echo $actualUser;?>">

	<input type="hidden" name="recomend" id="recomend" value="<?php echo $recomend;?>">

	<input type="hidden" name="not-recomend" id="not-recomend" value="<?php echo $notRecomend;?>">

	<input type="hidden" name="guided" id="guided" value="<?php echo $guided;?>">

	<input type="hidden" name="not-guided" id="not-guided" value="<?php echo $notGuided;?>">

	<input type="hidden" name="completed" id="completed" value="<?php echo $completed;?>">

	<input type="hidden" name="not-completed" id="not-completed" value="<?php echo $notCompleted;?>">

	<input type="hidden" name="all-rows" id="all-rows" value="<?php echo $allRows;?>">

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">
						
						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">

								Estadíticas de la app

							</h3>

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon">

										<i class="m-nav__link-icon la la-home"></i>

									</a>

								</li>

								<li class="m-nav__separator"> - </li>

								<li class="m-nav__item">

									<a href="index.php" class="m-nav__link">

										<span class="m-nav__link-text"> Home

										</span>

									</a>

								</li>							

							</ul>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="m-portlet">

						<div class="m-portlet__head">

							<div class="m-portlet__head-caption">
								
								<div class="m-portlet__head-title">

									<h3 class="m-portlet__head-text">
										
										DETALLE DE DESCARGA Y USO<br><small>Descargas hitóricas, sesiones diarias, semanales, mensuales, anuales, históricas.</small>
										
									</h3>
									
									<br><br>

								</div>

							</div>

						</div>

						<div class="m-portlet__body m-portlet__body--no-padding">

							<div class="row m-row--no-padding m-row--col-separator-xl">

								<div class="col-md-12 col-lg-12 col-xl-4">

									<div class="m-widget1">

										<div class="m-widget1__item">

											<div class="row m-row--no-padding align-items-center" data-skin="dark" data-toggle="m-tooltip"  data-placement="bottom" title="Contempla descargas únicas, desde el 4to release de la app (1ro de febrero de 2020).">

												<div class="col">

													<h3 class="m-widget1__title">ESTADISCIAS GENERALES</h3>
													
													<span class="m-widget1__desc">Info diaria, semanal, anual, histórica</span>
													
												</div>
												
												
											</div>
											
										</div>

										<div class="m-widget1__item">

											<div class="row m-row--no-padding align-items-center" data-skin="dark" data-toggle="m-tooltip"  data-placement="bottom" title="Contempla todas las veces que se abrió la app desde el inicio de los tiempos.">
												
												<div class="col">

													<h3 class="m-widget1__title">Sesiones históricas:</h3>

													<span class="m-widget1__desc">(desde el 1/2/2020)</span>
													
												</div>

												<div class="col m--align-right">

													<span class="m-widget1__number m--font-danger"><?php echo $usersSessions->total_sessions;?></span>
													
												</div>
												
											</div>
											
										</div>


									</div>

								</div>

								<div class="col-md-12 col-lg-12 col-xl-4">

									<div class="m-widget1">

										<div class="m-widget1__item">

											<div class="row m-row--no-padding align-items-center" data-skin="dark" data-toggle="m-tooltip"  data-placement="bottom" title="Contempla todas las veces que se abrió la app durante el día de hoy.">
												
												<div class="col">
													
													<h3 class="m-widget1__title">
														Sesiones del día
													</h3>
													<span class="m-widget1__desc"><?php echo strftime("%A, %d de %B de %Y")?></span>
												</div>

												<div class="col m--align-right">

													<span class="m-widget1__number m--font-accent">

														<?php echo $usersSessionsToday->total_sessions;?>										
													</span>
													
												</div>
												
											</div>
											
										</div>

										<div class="m-widget1__item">

											<div class="row m-row--no-padding align-items-center" data-skin="dark" data-toggle="m-tooltip"  data-placement="bottom" title="Desde las 00:00 del día lunes, hasta la 23:59 del domingo siguiente">

												<div class="col">

													<h3 class="m-widget1__title">
														Sesiones de la semana:
													</h3>

													<span class="m-widget1__desc">de lunes a lunes</span>
													
												</div>

												<div class="col m--align-right">
													
													<span class="m-widget1__number m--font-info">
														<?php echo $sessionsOfTheWeek->total_sessions;?>
													</span>

												</div>
												
											</div>
											
										</div>

									</div>

								</div>

								<div class="col-md-12 col-lg-12 col-xl-4">

									<div class="m-widget1">

										<div class="m-widget1__item">
											
											<div class="row m-row--no-padding align-items-center" data-skin="dark" data-toggle="m-tooltip"  data-placement="bottom" title="Desde el primer día de <?php echo strftime("%B");?> al <?php echo strftime("%A, %d de %B de %Y")?>">
												
												<div class="col">

													<h3 class="m-widget1__title">Sesiones del mes</h3>
													
													<span class="m-widget1__desc">Desde el 1ro de <?php echo strftime("%B");?></span>
													
												</div>
												<div class="col m--align-right">

													<span class="m-widget1__number m--font-success">

														<?php echo $sessionsOfTheMonth->total_sessions;?>

													</span>
													
												</div>
												
											</div>
											
										</div>

										<div class="m-widget1__item">
											
											<div class="row m-row--no-padding align-items-center" data-skin="dark" data-toggle="m-tooltip"  data-placement="bottom" title="Desde el 1ro de enero de <?php echo strftime("%Y");?> al 31 de diciembre de <?php echo strftime("%Y");?>">

												<div class="col">

													<h3 class="m-widget1__title">Sesiones del año</h3>

													<span class="m-widget1__desc">Durante el <?php echo strftime("%Y");?></span>
													
												</div>

												<div class="col m--align-right">

													<span class="m-widget1__number m--font-danger"><?php echo $sessionsOfTheYear->total_sessions;?></span>
													
												</div>
												
											</div>
											
										</div>

									</div>
									
								</div>

							</div>

						</div>

					</div>	

					<div class="row">

						<div class="col-xl-8">

							<div class="m-portlet m-portlet--full-height">

								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">

										<div class="m-portlet__head-title">

											<h3 class="m-portlet__head-text"> Lo más visto de la app </h3>

										</div>

									</div>

								</div>

								<div class="m-portlet__body">

									<div class="m-list-timeline m-list-timeline--skin-light">

										<div class="m-list-timeline__items">

											<?php

											$ranking = DB::getInstance()->query("SELECT menu.type as type, menu.name as app_name, COUNT(cicks.id) as interactions from app_device_menu as menu inner join app_device_menu_clicks as cicks on menu.section_id = cicks.app_id WHERE menu.deleted = 0 AND menu.active = 1 GROUP by menu.section_id ORDER BY interactions DESC");

											$counter = 0;

											foreach($ranking->results() as $results){

												$class;

												$label;

												$counter+=1;

												if($results->type == 'content'){

													$class = 'success';

													$label = 'contenido';

												}else if($results->type == 'hidden'){

													$class = 'info';

													$label = 'contenido oculto';

												}else if($results->type == 'lock'){

													$class = 'danger';

													$label = 'lock';

												}else if($results->type == 'link'){

													$class = 'warning';

													$label = 'link';
												}

												?>

												<div class="m-list-timeline__item">

													<span class="m-list-timeline__badge m-list-timeline__badge--<?php echo $class;?>"></span>

													<span class="m-list-timeline__text">

														<?php echo $counter . ' - ' . $results->app_name;?>

														<span class="m-badge m-badge--<?php echo $class;?> m-badge--wide">
															<?php echo $label;?>
														</span>

													</span>

													<span class="m-list-timeline__time"><?php echo $results->interactions;?></span>

												</div>

											<?php } ?>

										</div>

									</div>

								</div>

							</div>

						</div>

						<div class="col-xl-4">

							<div class="m-portlet m-portlet--bordered-semi m-portlet--widget-fit m-portlet--full-height m-portlet--skin-light ">
								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">

										<div class="m-portlet__head-title">

											<h3 class="m-portlet__head-text m--font-light">Desbloqueos </h3>

										</div>

									</div>

								</div>

								<div class="m-portlet__body">

									<div class="m-widget17">

										<div class="m-widget17__visual m-widget17__visual--chart m-portlet-fit--top m-portlet-fit--sides m--bg-danger">

											<div class="m-widget17__chart" style="height:120px;"> </div>

										</div>

										<div class="m-widget17__stats">

											<div class="m-widget17__items m-widget17__items-col1">

												<div class="m-widget17__item">

													<span class="m-widget17__icon">

														<img src="private/sources/images/icons/criminales.jpg" width="60px" height="60px">

													</span>

													<span class="m-widget17__subtitle">Perpetradores</span>

													<span class="m-widget17__desc"><?php echo $perpetradoresCounter;?></span>

												</div>

												<div class="m-widget17__item">

													<span class="m-widget17__icon">

														<img src="private/sources/images/icons/insignias.jpg" width="60px" height="60px">

													</span>

													<span class="m-widget17__subtitle">Insignias</span>

													<span class="m-widget17__desc"><?php echo $insigniasCounter;?></span>

												</div>

												<div class="m-widget17__item">

													<span class="m-widget17__icon">

														<img src="private/sources/images/icons/guetos.jpg" width="60px" height="60px">

													</span>

													<span class="m-widget17__subtitle">Guetos</span>

													<span class="m-widget17__desc"><?php echo $guetosCounter;?></span>

												</div>

												<div class="m-widget17__item">

													<span class="m-widget17__icon">

														<img src="private/sources/images/icons/finale.jpg" width="60px" height="60px">

													</span>

													<span class="m-widget17__subtitle">Finale</span>

													<span class="m-widget17__desc"><?php echo $guetosCounter;?></span>

												</div>

											</div>

											<div class="m-widget17__items m-widget17__items-col2">

												<div class="m-widget17__item">

													<span class="m-widget17__icon">

														<img src="private/sources/images/icons/justos.jpg" width="60px" height="60px">

													</span>

													<span class="m-widget17__subtitle">justos</span>

													<span class="m-widget17__desc"><?php echo $justosCounter;?></span>

												</div>

												<div class="m-widget17__item">

													<span class="m-widget17__icon">

														<img src="private/sources/images/icons/sobrevivientes.jpg" width="60px" height="60px">

													</span>

													<span class="m-widget17__subtitle">Sobrevivientes</span>

													<span class="m-widget17__desc"><?php echo $justosCounter;?></span>

												</div>

												<div class="m-widget17__item">

													<span class="m-widget17__icon">

														<img src="private/sources/images/icons/propaganda.jpg" width="60px" height="60px">

													</span>

													<span class="m-widget17__subtitle">Propaganda</span>

													<span class="m-widget17__desc"><?php echo $justosCounter;?></span>

												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>		

					</div>

					<div class="grid row">	

						<div class="col-xl-6">

							<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">

								<div class="m-portlet__head m-portlet__head--fit">

									<div class="m-portlet__head-caption">

										<div class="m-portlet__head-action">

										</div>

									</div>

								</div>

								<div class="m-portlet__body">

									<div class="m-widget19">

										<div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">

											<img src="private/sources/images/beacons/2.jpg" alt="">

											<div class="m-widget19__shadow"></div>

										</div>

										<div class="m-widget19__content">

											<div class="m-widget19__header">

												<span class="m-widget21__icon">

													<span class="m-widget21__icon">

														<span class="btn btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">

															<i class="fab fa-bluetooth-b"></i>

														</span>

													</span>

												</span>

												<div class="m-widget19__info">


													<span class="m-widget19__time">a través de beacons y QRs</span>

												</div>

												<div class="m-widget19__stats" data-skin="dark" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Contempla las interaccione históricas, desde que se instaló la app.">

													<span class="m-widget19__number m--font-brand"><?php echo $moisesQuery->unlocked_times;?></span>

													<span class="m-widget19__comment">Interacciones</span>

												</div>

											</div>

											<div class="m-widget19__body">Moisés nació en Sokoly, un pueblo al Este de Polonia. Vivía con sus padres y dos hermanos, él era el más chico. Tenía 12 años y ya se estaba preparando para su Bar Mitzvá cuando entraron los Nazis. Desde allí comenzaron los problemas para los judíos. Logró sobrevivir a siete campos de concentración y exterminio nazi durante el Holocausto, mientras moría toda su familia.</div>

											<div class="m-widget15__items">

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

						<div class="col-xl-6">

							<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">

								<div class="m-portlet__head m-portlet__head--fit">

									<div class="m-portlet__head-caption">

										<div class="m-portlet__head-action">

										</div>

									</div>

								</div>

								<div class="m-portlet__body">

									<div class="m-widget19">

										<div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides">

											<img src="private/sources/images/beacons/1.jpg" alt="">

											<div class="m-widget19__shadow"></div>

										</div>

										<div class="m-widget19__content">

											<div class="m-widget19__header">

												<span class="m-widget21__icon">

													<span class="btn btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">

														<i class="fab fa-bluetooth-b"></i>

													</span>

												</span>

												<div class="m-widget19__info">


													<span class="m-widget19__time">a través de beacons y QRs</span>

												</div>

												<div class="m-widget19__stats" data-skin="dark" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Contempla las interaccione históricas, desde que se instaló la app.">

													<span class="m-widget19__number m--font-brand"><?php echo $monicaQuery->unlocked_times;?></span>

													<span class="m-widget19__comment">Interacciones</span>

												</div>

											</div>

											<div class="m-widget19__body">Mónica nació en el Gueto de Lida, en 1941, en medio del horror y la devastación que la Shoá representaba. Sus padres tuvieron que tomar la decisión más valiente y amorosa: entregarla a una familia polaca para que la protegiera. Así comienza un extenso recorrido de cambio de países, de familias, de idiomas y de nombres.</div>

											<div class="m-widget15__items">

											</div>

										</div>


									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

	<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i> </div>

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="assets/vendors/custom/jquery-ui/jquery-ui.bundle.js" type="text/javascript"></script>

<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>

<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/museum/stats/museum-survey-stats.js" type="text/javascript"></script>

</body>

</html>


