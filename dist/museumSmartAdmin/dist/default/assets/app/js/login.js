    //== Class Definition
    var Login = function() {

        var login = $('#m_login');

        var loginErrors = 0;

        $('.g-recaptcha').hide();

        var showErrorMsg = function(form, type, msg) {

            var alert = $('<div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
             <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
             <span></span>\
             </div>');

            form.find('.alert').remove();

            alert.prependTo(form);

            alert.animateClass('fadeIn animated');

            alert.find('span').html(msg);
        }

        //== Private Functions

        var displaySignUpForm = function() {

            login.removeClass('m-login--forget-password');

            login.removeClass('m-login--signin');

            login.addClass('m-login--signup');

            login.find('.m-login__signup').animateClass('flipInX animated');
        }

        var displaySignInForm = function() {

            login.removeClass('m-login--forget-password');

            login.removeClass('m-login--signup');

            login.addClass('m-login--signin');

            login.find('.m-login__signin').animateClass('flipInX animated');
        }

        var displayForgetPasswordForm = function() {

            login.removeClass('m-login--signin');

            login.removeClass('m-login--signup');

            login.addClass('m-login--forget-password');

            login.find('.m-login__forget-password').animateClass('flipInX animated');
        }

        var handleFormSwitch = function() {

            $('#m_login_forget_password').click(function(e) {

                e.preventDefault();

                displayForgetPasswordForm();

            });

            $('#m_login_forget_password_cancel').click(function(e) {

                e.preventDefault();

                displaySignInForm();

            });

            $('#m_login_signup').click(function(e) {

                e.preventDefault();

                displaySignUpForm();

            });

            $('#m_login_signup_cancel').click(function(e) {

                e.preventDefault();

                displaySignInForm();

            });
        }

        var handleSignInFormSubmit = function() {

            $('#m_login_signin_submit').click(function(e) {

                e.preventDefault();

                var btn = $(this);

                var form = $(this).closest('form');

                form.validate({

                    rules: {

                        email: {

                            required: true,

                            email: true

                        },

                        password: {

                            required: true

                        },

                        hiddenRecaptcha: {

                            required: function () {

                                if (grecaptcha.getResponse() == '' && loginErrors >= 2) {

                                    return true;

                                } else {

                                    return false;

                                }

                            }

                        }

                    },
                    messages: {

                        email:'falta o es incorrecto el email.',

                        password: 'falta la contraseña.',

                        hiddenRecaptcha: 'debes pasar este control de seguridad.'

                    }

                });


                if (!form.valid()) {

                    return;

                }

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                var response = grecaptcha.getResponse();

                form.ajaxSubmit({

                    cache: false,

                    url: 'private/users/login.php',

                    type: 'post',

                    dataType: 'json',

                    data: {browserName: navigator.appName, browserEngine:navigator.product},

                    success: function(response, status, xhr, $form) {

                        console.log(response);

                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                        if(response.errors == true){

                            loginErrors+=1;

                            if(loginErrors >= 2){

                                $('.g-recaptcha').show();

                                grecaptcha.reset();

                            }

                            $(".swal2-modal").css('background-color', '#000');

                            swal({

                              title: 'Opps!',

                              html: response.errorsList[0],

                              type: 'warning',

                              confirmButtonText: 'ok'
                          })

                        }else{

                            if(response.success == true){

                                //window.location.href = "index.php";

                                //window.location.replace("index.php");

                                window.location.assign("index.php");


                            }

                        }

                    },

                    error: function(response, status, xhr, $form){

                     var err = eval("(" + xhr.responseText + ")");

                     console.log(err);
                     console.log("Status: "+ status);

                 }

             });

            });

        }


        var handleSignUpFormSubmit = function() {

            $('#m_login_signup_submit').click(function(e) {

                e.preventDefault();

                var btn = $(this);

                var form = $(this).closest('form');

                form.validate({

                    rules: {

                        fullname: {

                            required: true
                        },
                        email: {
                            required: true,

                            email: true
                        },

                        password: {

                            required: true

                        },

                        rpassword: {

                            required: true

                        },

                        agree: {

                            required: true

                        }
                    }
                });

                if (!form.valid()) {
                    return;
                }

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                form.ajaxSubmit({
                    url: '',
                    success: function(response, status, xhr, $form) {
                    	// similate 2s delay
                    	setTimeout(function() {
                           btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                           form.clearForm();
                           form.validate().resetForm();

    	                    // display signup form
    	                    displaySignInForm();
    	                    var signInForm = login.find('.m-login__signin form');
    	                    signInForm.clearForm();
    	                    signInForm.validate().resetForm();

    	                    showErrorMsg(signInForm, 'success', 'Thank you. To complete your registration please check your email.');
    	                }, 2000);
                    }
                });
            });
        }

        var handleForgetPasswordFormSubmit = function() {

            $('#m_login_forget_password_submit').click(function(e) {

                e.preventDefault();

                var btn = $(this);

                var form = $(this).closest('form');

                form.validate({

                    rules: {

                        email_reset: {

                            required: true,

                            email: true

                        }

                    },

                    messages: {

                        email_reset: 'Debes indicar tu email para que te enviemos las instrucciones para resetear tu contraseña.',

                    }

                });

                if (!form.valid()) { return; }

                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                form.ajaxSubmit({

                    url: 'private/users/forget_password.php',

                    type: 'post',

                    dataType: 'json',

                    success: function(response, status, xhr, $form) {

                        console.log(response);

                        swal({

                            title: response.title,

                            html: response.msg,

                            type: response.alert,

                            confirmButtonText: 'ok'

                        }).then((result) => {

                            switch(Number(response.status)){

                                case 1:
                                case 2:
                                case 3:
                                case 5:

                                location.reload();

                                break;

                                case 4:

                                btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                                form.clearForm();

                                form.validate().resetForm();

                                displaySignInForm();

                                var signInForm = login.find('.m-login__signin form');

                                signInForm.clearForm();

                                signInForm.validate().resetForm();

                                showErrorMsg(signInForm, 'success', response.msg);

                                break;

                            }


                        });

                    },

                    error: function(response, status, xhr, $form){

                        console.log("error:");

                        console.log(response);
                    }

                });

            });

        }

        return {

            init: function() {

                console.log("oki");

                handleFormSwitch();

                handleSignInFormSubmit();

                handleSignUpFormSubmit();

                handleForgetPasswordFormSubmit();

            }

        };

    }();

    jQuery(document).ready(function() { Login.init(); });

    var correctCaptcha = function(response) {

    };
