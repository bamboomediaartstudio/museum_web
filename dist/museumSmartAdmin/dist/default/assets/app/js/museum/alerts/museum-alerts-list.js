/**
 * @summary Manage all the alerts actions.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */
 
   /**
    * @function MuseumAlertsList
    * @description Initialize and include all the methods of this class.
    */

    var MuseumAlertsList = function() {

        helper = new Helper();

         /**

         * @function addListeners
         * @description Asign all the listeners / event habdlers for this page.
         */

         var addListeners = function(){

            $("[name='my-checkbox']").bootstrapSwitch();
        }


    /**
    * @function createDataTable
    * @description Create all the datatables that we use as lists.
    */

    var createDataTable = function(){

        var table = $('table.display').DataTable({

            pageLength: 50,

            "language": helper.getDataTableLanguageConfig(),

            //rowReorder: true,

            //rowReorder: { update: true },

            //"ordering": true,

            order:[[0,"desc"]],

            bAutoWidth: false, 

            processing:true,

            "columns": [

            { "visible": false, orderable:false },

            { "visible": false, orderable:false },

            { "width": "85%", responsivePriority: 1, orderable: false, targets: '_all' }, 

            { "width": "5%", responsivePriority: 0, orderable: false, targets: '_all', }, 

            { "width": "5%", responsivePriority: 0, orderable: false, targets: '_all' }
            ]

        });

        $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

            var closestRow = $(this).closest('tr');

            var data = table.row(closestRow).data();

            var order = data[0];

            var id = data[1];

            var name = data[2];

            updateStatus(id, state, name, 'update', closestRow, table.row(closestRow), null, null, 'active');

     });

        $('tbody').on( 'click', '.item-string', function () {

            var id = table.row( $(this).parents('tr') ).data()[1];

            window.location.href = "museum_alert_add.php?id=" + id;

        });

        $('tbody').on( 'click', '.edit_row', function () {

            var id = table.row( $(this).parents('tr') ).data()[1];

            window.location.href = "museum_alert_add.php?id=" + id;

        });


        $('tbody').on( 'click', '.delete_row', function () {

            var row = table.row($(this).parents('tr'));

            var animatable = $(this).parents('tr');

            var data = table.row( $(this).parents('tr') ).data();

            var order = data[0];

            var id = data[1];

            var stringValue = data[2];

            var actualTable = $("#tab-1").DataTable();

            Swal({

                title: "Eliminar alerta",

                html: 'Esta acción eliminará la alerta <b>'+stringValue+'</b>.<br><br>Esta acción no se puede deshacer. <br><br>Recordá que también podés usar botón de <b>online</b> y <b>offline</b> para modificar la visibilidad del mismo.<br><br>¿Eliminar de todos modos?',

                type: 'error',

                showCancelButton: true,

                confirmButtonText: 'Si, eliminar',

                cancelButtonText: 'no, salir'

            }).then((result) => {

                //console.log(id, 'true', stringValue, 'delete', animatable, row, actualTable, null,  'deleted');

                //return;

                if(result.value == true) updateStatus(id, 'true', name, 'delete', animatable, row, actualTable, null,  'deleted');

            })

        });

    }



    /**
    * @function updateStatus
    *
    * @description Update / Delete user status from list: we use this to mark the user as online / offline and
    * for delete an user completely from the list.
    *
    * @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
    * @param {boolean} state                    - True or false: use it for both, update and delete.
    * @param {string} name                  - The name of the person being manipulated.
    * @param {string} action                    - Posibilities: 'update' or 'delete'. 
    * @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
    * @param {DataTable} row                    - Reference to the row that we are dealing with.
    * @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
    * @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
    */

    var updateStatus = function(id, state, name, action, animatable, row, processTable = null, order = null, defaultColumn = null){

        helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando opinión...');

        var url = "private/users/museum/general/update_status.php"; 

        $.ajax({

            type: "POST",

            url: url,

            data: {id: id, status:state, action:action, name:name, source: 'alerts',

            table:'museum_alerts', defaultColumn:defaultColumn},

            success: function(result){

                helper.unblockStage();

                if(action == 'update'){

                    console.log(result.msg);

                    var newStatus = (result.changeStatus == 'true') ? 'online' : 'offline';

                    var previousStatus = (newStatus == 'online') ? 'offline' : 'online';

                    var st1 = result.msg.replace('%name%', '<b>' + name + '</b>');

                    var st2 = st1.replace('%newStatus%', '<b>' + newStatus + '</b>');

                    var st3 = st2.replace('%previousStatus%', '<b>' + previousStatus + '</b>');

                    console.log(st3);

                    helper.showToastr(result.title, st3);

                }else{

                    animatable.fadeOut('slow','linear',function(){

                        console.log(result.msg);

                        helper.showToastr('ELIMINADO', 'La alerta se eliminó exitosamentne.');

                        var count = 0;

                        var data = processTable.rows().data();

                        processTable.rows().every( function (rowIdx, tableLoop, rowLoop) {

                            if(processTable.cell(rowIdx, 0).data() > Number(order)){

                                var saveValue = Number(processTable.cell(rowIdx, 0).data() - 1);

                                processTable.cell(rowIdx, 0).data(saveValue);

                            }

                        });

                        row.remove().draw(true);

                        processTable.rows().invalidate().draw(false);

                    });

                }

            },

            error: function(xhr, status, error) {

                console.log(xhr);

                var err = eval("(" + xhr.responseText + ")");

                alert(err.Message);
            },

            dataType: "json"
        });

    }

    return {

        init: function() {

            addListeners();

            createDataTable();
            
        }

    };

}();

jQuery(document).ready(function() { MuseumAlertsList.init(); });