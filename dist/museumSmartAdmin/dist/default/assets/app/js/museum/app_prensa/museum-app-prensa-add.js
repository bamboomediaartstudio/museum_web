/**
 * @summary Add Prensa Add.
 *
 * @description - APP Prensa - Add Prensa Data
 *
 * @author Colo baggins <colorado@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */


/**
 * @function MuseumAppPrensaAdd
 * @description Initialize and include all the methods of this class.
 */

 var MuseumAppPrensaAdd = function() {

    helper = Helper();

    var editMode;

    var redirectId;

    var prensaId;

    var prensaTitle;

    var addRedirectId = 0;

    var defaultImageWidth = 1000;

    var defaultImageHeight = 1000;

    var isFirstLoad = true;

    var keywordsArr = [];

    var form = $('#add-prensa-data-form');

    //cover config...

    initialPreviewCoverConfig = [];

    initialPreviewCoverPaths = [];

    /**
     * @function addFormValidations
     * @description Asign all the form validations for this page.
     */

     var addFormValidations = function() {

        jQuery.validator.setDefaults({

            ignore: ":hidden, [contenteditable='true']:not([name])"

        });


        form.validate({

            rules: {

                title: {
                    required: true
                },

                id_newspaper: {
                    required: true
                },

                date: {
                    required: true
                },

                'checkboxes[]': {
                    required: !0
                }

            },
            messages: {

                title: helper.createErrorLabel('Titulo', 'REQUIRED'),

                id_newspaper: helper.createErrorLabel('Diario', 'REQUIRED'),

                date: helper.createErrorLabel('Fecha', 'REQUIRED'),

                'checkboxes[]': helper.createErrorLabel('categoría', 'AT_LEAST_ONE')

            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();



                window.scroll({top: 0, left: 0, behavior: 'smooth' });
            },

            submitHandler: function(form, event) {

                event.preventDefault();

                event.stopPropagation();

                helper.blockStage("actualizando datos...");

                var formData = new FormData(form);

                formData.append('keywords', JSON.stringify(keywordsArr));

                var request = $.ajax({

                    url: "private/users/museum/apps/prensa/prensa_add.php",

                    type: "POST",

                    contentType: false,

                    processData: false,

                    data: formData,

                    dataType: "json"
                });

                request.done(function(response) {

                  console.log("response antes de swal y toastr");

                  console.log(response);

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch (Number(response.status)) {

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {

                            switch (Number(response.status)) {

                                case 1:

                                window.location.replace("museum_app_prensa_list.php");

                                break;

                            }

                        }

                    });


                    helper.showToastr("Se acualizo", 'Se actualizo');

                    helper.unblockStage();

                });

                request.fail(function(jqXHR, textStatus) {

                    console.log(jqXHR);

                    console.log("error");

                    helper.showToastr("ops", 'error');

                    helper.unblockStage();

                });

            }

        });
      }

      /**
       * @function createNewsImg
       * @description Create the newspaper image.
       */

       var createNewsImg = function(){

          $("#picture").fileinput({

              initialPreviewAsData: true,

              initialPreview: initialPreviewCoverPaths,

              initialPreviewConfig: initialPreviewCoverConfig,

              theme: "fas",

              uploadUrl: "private/users/museum/apps/prensa/prensa_img_upload.php", //on modification image upload only!!

              deleteUrl: "private/users/museum/apps/prensa/prensa_delete.php",

              showCaption: true,

              showPreview: true,

              showRemove: false,

              showUpload: true,

              showCancel: true,

              showClose: false,

              browseOnZoneClick: true,

              previewFileType: 'any',

              language: "es",

              maxFileSize: 0,

              allowedFileTypes: ["image"],

              overwriteInitial: false,

              allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],

              maxFilePreviewSize: 10240,

              minImageWidth: 1080,

              minImageHeight: 1920,

              maxImageWidth: 1080,

              maxImageHeight: 1920,

              uploadExtraData: function() {   //On modification img upload data!!

                  return {

                      id: prensaId,

                      editMode: editMode,

                      source: 'prensa',

                      galleryFolder: 'prensa'

                  };

              }

          }).on('filesorted', function(e, params) {



          }).on('fileuploaded', function(e, params) {


          }).on('filebatchuploadcomplete', function(event, files, extra) {

              location.reload();

          }).on("filepredelete", function(jqXHR) {


          }).on('filedeleted', function(event, key, jqXHR, data) {

              return {

                  source: 'prensa',

                  galleryFolder: 'prensa'

              };

              console.log("data: "+data);
              console.log(data);

              helper.showToastr("Imagen eliminada", 'Se eliminó la imagen del diario');

          }).on('fileimageresizeerror', function(event, data, msg) {

              showErrorSwal(msg);

          }).on('fileuploaderror', function(event, data, msg) {

              var form = data.form, files = data.files, extra = data.extra,

              response = data.response, reader = data.reader;

              console.log(event, data, msg);

              //showErrorSwal(msg);

          }).on('filebeforedelete', function() {

              return new Promise(function(resolve, reject) {

                  swal({

                      title: "¿Eliminar imagen?",

                      allowOutsideClick: false,

                      html: "La misma no aparecerá más listada en esta muestra.",

                      type: "warning",

                      showCancelButton: true,

                      confirmButtonText: 'Si, borrar',

                      cancelButtonText: 'no, salir'

                  }).then((result) => {

                      if (result.value) {

                          resolve();

                      }else{

                          reject();
                      }

                  })

              });
          });

          $('.kv-cust-btn').on('click', function() {

              origin = $(this).attr('data-origin');

              var $btn = $(this);

              var key = $btn.data('key');

              openKey = key;

              var item = initialPreviewCoverConfig.find(item => item.key == key);

              $('#image-description').val(item.description);

              $('#myModal').modal('show');

          });



      }// /.createNewsImg


    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

     var addListeners = function() {

       $('[data-toggle="tooltip"]').tooltip();

       //Delete key / tag from array no db
       $(document).on('click', '.tag a', function(e){

         if(editMode){


         }else{

           e.preventDefault();

           console.log("Eliminar tag de la vista y del array");

           var tagText = $(this).parent().find('.tag-value').text();

           //Si se elimina del array, lo elimino de la vista

           if(deleteKeyFromArr(tagText)){

              $(this).parent().remove();

           }

         }



       });

       //Delete key / tag from db (We are in editmode. These key are from db only).
      $('.deleteTag a').click(function(){

        var tagId = $(this).parent().attr('id');

        var name = "keyword Test Name";

        console.log("Delete tag id: "+tagId);


        Swal({

            title: "Eliminar Keyword?",

            html: 'Esta acción eliminará  <b>'+name+'</b>.<br><br>Esta acción no se puede deshacer. <br><br>¿Eliminar de todos modos?',

            type: 'error',

            showCancelButton: true,

            confirmButtonText: 'Si, eliminar',

            cancelButtonText: 'no, salir'

        }).then((result) => {

          if(result.value == true){

            deleteTag(tagId);

          }


        });

       });

       //Agrega keywords (fuera de editMode)en un array y las muestra. Se manda a la db al enviar el formulario entero.

       $('#add-keyword').click(function(){

         var keyword = $(this).parent().parent().find("#keyword");

         console.log("Add keyword to array");

         if(keyword.valid()){


           if(keyword.val() != ""){   //Agregar key a array - Si no es vacío

              console.log("keyword: "+keyword.val());

              addKeyToArr(keyword.val());

           }


           $("#keyword").val('');   //limpiar input

         }

       });

       //Agrega directamente keywords a la db por estar dentro de editMode

       $("#add-keyword-direct").click(function(){

         var tagName = $('#keyword').val();


         if(tagName!=''){

           console.log("agregar tag: "+tagName);

           var totalTags = $('.tagsinput .tag-value').length;
           var tagsChecked = 0;
           console.log("totalTags: "+totalTags);

           $('.tagsinput .tag-value').each(function(i, obj){

             //console.log(obj);


             var existingTag = $(this).text();
             console.log(existingTag);

             if(existingTag.toLowerCase() ===tagName.toLowerCase()){

               //No agregar tags, ya esta vinculada

             }else{

               console.log("Agregar keyword, no esta actualmente agregada");
               tagsChecked++;
             }

             if(totalTags == tagsChecked){

               //Agregar tags al server

               addDirectTag(tagName);

             }

           });

         }else{
           console.log("tagname empty");
         }






       });

       $('#date').datepicker({

           format: 'yyyy-mm-dd',

           todayBtn: true,

           todayHighlight: true,

           autoclose: true,

           defaultViewDate: {year: '1930'}

       });


       t = new Bloodhound({

           datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

           queryTokenizer: Bloodhound.tokenizers.whitespace,

           prefetch:{

               url : "private/users/services/get_list_of_keywords.php",

               cache: false
           }

       })

       var context = $("#key-list .typeahead").typeahead(null, {

           hint: false,

           highlight: false,

           minLength: 1,

           name: "best-pictures",

           display: "value",

           source: t,

           autoselect: true,

           templates: {

               suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")

           }

       }).on("typeahead:render", function() {

           var searchTerm = $(this).val();

           $('#key-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

       }).on('typeahead:selected', function (e, datum) {

           $('#hidden-key-id').val(datum.id);
           console.log("Para que sirve este id?: "+datum.id);

       }).on('keyup', this, function (event) {


       }).on('keydown', this, function (event) {

       }).on('focus', this, function(event){

           if(editMode) $('#hidden-key-id').val("");

       });


       $('#exit_from_form').click(function(e) {
           window.location.replace("index.php");
       });

       $('#back_to_list').click(function(e) {
           window.location.replace("museum_app_prensa_list.php?tabId=" + redirectId);
       });

       $(".update-btn").click(function() {
         console.log("UPDATE FIELD");

           if (!editMode) return;

           var dbName = $(this).attr('data-db-value');

           var id = $(this).attr('data-id');

           var newValue = $(this).parent().parent().find(':input').val();

           var fieldName = $(this).parent().parent().find(':input').attr('name');

           console.log("dbName: "+dbName);
           console.log("id: "+id);
           console.log("newValue: "+newValue);
           console.log("fieldName: "+fieldName);



            var valid = $('#' + fieldName).valid();

            if (dbName != newValue && valid) {

                console.log("changing something! Go to Update");

                $(this).attr('data-db-value', newValue);

                updateValue(id, fieldName, newValue, 'app_museum_prensa');

            }else{
              console.log("nothing changed, nothing happens");
            }



       });


       $('.toggler-info:checkbox').change(function() {

           if (!editMode) return;

           var fieldName = $(this).attr('name');

           var newValue = ($(this).is(":checked")) ? 1 : 0;

           updateValue(prensaId, fieldName, newValue, 'app_museum_prensa');

       });

     }// /.addListeners


     /**
      * @function addDirectTag
      * @description add tag direct on edit mode
      * @param {string} tagName -
      *
      */

     var addDirectTag = function(tagName){

         var request = $.ajax({

               url: "private/users/museum/apps/prensa/prensa_service_ajax.php",

               type: "POST",

               data: {

                   id: prensaId,

                   action: 'addDirectTag',

                   tagName: tagName
               },

               dataType: "json"
           });

           request.done(function(response) {

             console.log("response antes de swal y toastr");

             console.log(response);

               swal({

                   title: response.title,

                   allowOutsideClick: false,

                   html: response.msg,

                   type: response.alert,

                   showCancelButton: (response.status == 1) ? true : false,

                   confirmButtonText: response.button,

                   cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

               }).then((result) => {

                   location.reload();

               });


               helper.showToastr("Se acualizo", 'Se actualizo');

               helper.unblockStage();

           });

           request.fail(function(jqXHR, textStatus) {

               console.log(jqXHR);

               console.log("error");

               helper.showToastr("ops", 'error');

               helper.unblockStage();

           });
     }

     /**
      * @function deleteTag
      * @description delete relation tag in DB.
      * @param {int} tagId - id relation tag
      *
      */

     var deleteTag = function(tagId){

       var request = $.ajax({

             url: "private/users/museum/apps/prensa/prensa_service_ajax.php",

             type: "POST",

             data: {

                 id: tagId,

                 action: 'deleteTag'
             },

             dataType: "json"
         });

         request.done(function(response) {

           console.log("response antes de swal y toastr");

           console.log(response);

             swal({

                 title: response.title,

                 allowOutsideClick: false,

                 html: response.msg,

                 type: response.alert,

                 showCancelButton: (response.status == 1) ? true : false,

                 confirmButtonText: response.button,

                 cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

             }).then((result) => {

                 location.reload();

             });


             helper.showToastr("Se acualizo", 'Se actualizo');

             helper.unblockStage();

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

             console.log("error");

             helper.showToastr("ops", 'error');

             helper.unblockStage();

         });

     }

     /**
      * @function updateValue
      * @description update individual value in DB.
      *
      * @param {int} id                 - object id.
      * @param {string} fieldName       - the field to modify
      * @param {string} newValue        - object value.
      * @param {int} filter             - filter type.
      */

      var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

         console.log("Update - FieldName: "+fieldName + " - newValue: "+newValue + " - table: "+filter);

         console.log("update value...only once?");

         helper.blockStage("actualizando...");

         var request = $.ajax({

             url: "private/users/museum/general/update_value.php",

             type: "POST",

             data: {

                 id: id,

                 fieldName: fieldName,

                 newValue: newValue,

                 source: "prensa",

                 filter: filter
             },

             dataType: "json"

         });

         request.done(function(result) {

             console.log("tracing result!");

             console.log(result);

             helper.showToastr(result.title, result.msg);

             if (callback != undefined) callback(callbackParams);

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

             console.log("error");

         });

         if (callback == undefined) helper.unblockStage();

         localStorage.setItem('refresh', 'true');

     }// /.updateValue


     /**
      * @function addKeyToArr
      * @description add item to array if not exists
      *
      * @param {key} key                 - keyword
      *
      */

     var addKeyToArr = function(key){

        var found = jQuery.inArray(key, keywordsArr);

        if (found >= 0) {
            // Element was found, remove it.
            //keywordsArr.splice(key, 1);
        } else {
            // Element was not found, add it.
            keywordsArr.push(key);

            //add item to ul view
            strKeyLi = '<span class="tag"><span><span class="tag-value">'+key+'</span>&nbsp;&nbsp;</span><a href="#" title="Eliminar Keyword">x</a></span>';
            $('.tagsinput').append(strKeyLi);

        }


        console.log("keywordsArr:");
        console.log(keywordsArr);

     }


     /**
      * @function deleteKeyFromAr
      * @description delete item from array by name
      *
      * @param {key} key                 - keyword
      *
      */

     var deleteKeyFromArr = function(key){

       keywordsArr.splice(keywordsArr.indexOf(key), 1);

       var found = jQuery.inArray(key, keywordsArr);

       console.log("se elimino key. Array actual: ");
       console.log(keywordsArr);

       if (found >= 0) { } else { return true; }



     }

     /**
      * @function loadAjaxImages
      * @param loadSource - evaluate this param to get one batch or a single one.
      * @description if we are in edition mode load all the images for Bootstrap File Input.
      */

      var loadAjaxImages = function(from, field, loadSource, config, myPreviews) {

         console.log("loadAjaxImages: from: "+from+" - laodSource: "+loadSource+" - config: "+config+" - myPreviews: "+myPreviews);

         var request = $.ajax({

             url: "private/users/services/get_list_of_images_new_single.php",

             type: "POST",

             data: {

                 id: prensaId,

                 field: field,

                 source: loadSource,

                 from: from
             },

             dataType: "json"
         });

         request.done(function(result) {

           console.log(result);

           //Si hay imagen (path en db) armo vista de img
           if(result[0].picture != ""){

             for (var i = 0; i < result.length; i++) {

                 var id = result[i].id;
                 var imgPath = result[i].picture;
                 var splitImgName = imgPath.split('/');

                 var imgName = prensaId + '_' + splitImgName[1] + '_original.jpeg';

                 var path;

                path = 'private/sources/images/prensa/' + result[i].picture + '/' + imgName;

                 myPreviews.push(path);

                 config.push({

                     'key': id,

                     //'internal_order': internalOrder,


                 });

             }// /.ends for


           }


             createNewsImg();

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

         });

     }//loadAjaxImages Ends


    return {

        init: function() {

            editMode = ($('#edit-mode').val() == 1) ? true : false;

            helper.setMenu();

            if (editMode) {

              console.log("Estamos en edit mode!");

                var temp;

                if (document.referrer != "") {

                    temp = helper.getGETVariables(document.referrer, 'tabId');

                    console.log("temP: "+temp);


                } else {

                    temp = null;
                }

                redirectId = (temp == null) ? 1 : temp;

                prensaId = $('#prensa-id').val();

            } else {

                isFirstLoad = false;
            }

            addListeners();

            addFormValidations();

            if(editMode){

              loadAjaxImages('app_museum_prensa', 'id', 'prensa', initialPreviewCoverConfig, initialPreviewCoverPaths)

            }else{

                createNewsImg();


            }// /.editMode if else



        } // /.init function

    }; // /.return

 }();

 jQuery(document).ready(function() { MuseumAppPrensaAdd.init(); });
