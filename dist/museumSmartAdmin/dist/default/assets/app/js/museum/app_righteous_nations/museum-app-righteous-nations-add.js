/**
 * @summary Righteous among Nations Add.
 *
 * @description - APP Righteous - Add Righteous Data
 *
 * @author Colo baggins <colorado@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */


/**
 * @function MuseumAppRighteousNationsAdd
 * @description Initialize and include all the methods of this class.
 */

 var MuseumAppRighteousNationsAdd = function() {

    helper = Helper();

    var editMode;

    var redirectId;

    var righteousId;

    var righteousName;

    var addRedirectId = 0;

    var defaultImageWidth = 1000;

    var defaultImageHeight = 1000;

    var isFirstLoad = true;

    var myElement;

    var form = $('#add-righteous-data-form');

    var recognitionDatepicker = $("#recognition_date");

    var deathDatepicker = $("#death_date");

    //cover config...

    initialPreviewCoverConfig = [];

    initialPreviewCoverPaths = [];

    /**
     * @function addFormValidations
     * @description Asign all the form validations for this page.
     */

     var addFormValidations = function() {

        jQuery.validator.setDefaults({

            ignore: ":hidden, [contenteditable='true']:not([name])"

        });


        form.validate({

            rules: {

                name: {
                    required: true
                },

                surname: {
                    required: true
                },

                gender: {
                  required: true
                },

                'checkboxes[]': {
                    required: !0
                }

            },
            messages: {

                name: helper.createErrorLabel('Nombre', 'REQUIRED'),

                surname: helper.createErrorLabel('Apellido', 'REQUIRED'),

                gender: helper.createErrorLabel('Género', 'REQUIRED'),

                'checkboxes[]': helper.createErrorLabel('categoría', 'AT_LEAST_ONE')

            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();



                window.scroll({top: 0, left: 0, behavior: 'smooth' });
            },

            submitHandler: function(form, event) {

                event.preventDefault();

                event.stopPropagation();

                helper.blockStage("actualizando datos...");

                var formData = new FormData(form);


                var request = $.ajax({

                    url: "private/users/museum/apps/righteous/righteous_add.php",

                    type: "POST",

                    contentType: false,

                    processData: false,

                    data: formData,

                    dataType: "json"
                });

                request.done(function(response) {

                  console.log("response antes de swal y toastr");

                  console.log(response);

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch (Number(response.status)) {

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {

                            switch (Number(response.status)) {

                                case 1:

                                window.location.replace("museum_app_righteousNations_list.php");

                                break;

                            }

                        }

                    });


                    helper.showToastr("Se acualizo", 'Se actualizo');

                    helper.unblockStage();

                });

                request.fail(function(jqXHR, textStatus) {

                    console.log(jqXHR);

                    console.log("error");

                    helper.showToastr("ops", 'error');

                    helper.unblockStage();

                });

            }

        });
      }

    /**
     * @function createNewsImg
     * @description Create the newspaper image.
     */

     var createNewsImg = function(){

       var btns = '<button data-origin="image" type="button" class="kv-cust-btn btn btn-sm btn-kv btn-default btn-outline-secondary" title="Editar"{dataKey}>' +
       '<i class="fas fa-pen"></i>' + '</button>';

       $("#picture").fileinput({

           initialPreviewAsData: true,

           initialPreview: initialPreviewCoverPaths,

           initialPreviewConfig: initialPreviewCoverConfig,

           theme: "fas",

           uploadUrl: "private/users/museum/apps/righteous/righteous_img_upload.php", //on modification image upload only!!

           deleteUrl: "private/users/museum/apps/righteous/righteous_delete.php",

           showCaption: true,

           showPreview: true,

           showRemove: false,

           showUpload: true,

           showCancel: true,

           showClose: false,

           browseOnZoneClick: true,

           previewFileType: 'any',

           language: "es",

           maxFileSize: 0,

           allowedFileTypes: ["image"],

           overwriteInitial: false,

           allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],

           maxFilePreviewSize: 10240,

           minImageWidth: 500,

           minImageHeight: 500,

           uploadExtraData: function() {   //On modification img upload data!!

               return {

                   id: righteousId,

                   editMode: editMode,

                   source: 'righteous',

                   galleryFolder: 'righteous'

               };

           }

       }).on('filesorted', function(e, params) {



       }).on('fileuploaded', function(e, params) {


       }).on('filebatchuploadcomplete', function(event, files, extra) {

           location.reload();

       }).on("filepredelete", function(jqXHR) {


       }).on('filedeleted', function(event, key, jqXHR, data) {

           return {

               source: 'righteous',

               galleryFolder: 'righteous'

           };

           console.log("data: "+data);
           console.log(data);

           helper.showToastr("Imagen eliminada", 'Se eliminó la imagen de perfil de Justos');

       }).on('fileimageresizeerror', function(event, data, msg) {

           showErrorSwal(msg);

       }).on('fileuploaderror', function(event, data, msg) {

           var form = data.form, files = data.files, extra = data.extra,

           response = data.response, reader = data.reader;

           console.log(event, data, msg);

           //showErrorSwal(msg);

       }).on('filebeforedelete', function() {

           return new Promise(function(resolve, reject) {

               swal({

                   title: "¿Eliminar imagen?",

                   allowOutsideClick: false,

                   html: "La misma no aparecerá más listada en esta muestra.",

                   type: "warning",

                   showCancelButton: true,

                   confirmButtonText: 'Si, borrar',

                   cancelButtonText: 'no, salir'

               }).then((result) => {

                   if (result.value) {

                       resolve();

                   }else{

                       reject();
                   }

               })

           });
       });

       $('.kv-cust-btn').on('click', function() {

           origin = $(this).attr('data-origin');

           var $btn = $(this);

           var key = $btn.data('key');

           openKey = key;

           var item = initialPreviewCoverConfig.find(item => item.key == key);

           $('#image-description').val(item.description);

           $('#myModal').modal('show');

       });

    }// /.createNewsImg


    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

     var addListeners = function() {

       $('[data-toggle="tooltip"]').tooltip();

       //Nationality predictive
       t = new Bloodhound({

           datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

           queryTokenizer: Bloodhound.tokenizers.whitespace,

           prefetch:{

               url : "private/users/services/get_list_of_countries.php",

               cache: false
           }

       })

       var context = $("#nationality-list .typeahead").typeahead(null, {

           hint: false,

           highlight: false,

           minLength: 1,

           name: "best-pictures",

           display: "value",

           source: t,

           autoselect: true,

           templates: {

               suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")

           }

       }).on("typeahead:render", function() {

           var searchTerm = $(this).val();

           $('#nationality-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

       }).on('typeahead:selected', function (e, datum) {

           $('#hidden-nationality-id').val(datum.id);
           console.log("Para que sirve este id?: "+datum.id);

       }).on('keyup', this, function (event) {


       }).on('keydown', this, function (event) {

       }).on('focus', this, function(event){

           if(editMode) $('#hidden-nationality-id').val("");

       });

       //Birth datepicker

       $('#birth_date').datepicker({

           format: 'yyyy-mm-dd',

           todayBtn: false,

           todayHighlight: true,

           autoclose: true,

           changeYear: true,

           defaultViewDate: {year: '1930'}

       });

       //Check if zl checkbox is checked - if yes, disable death datepicker

       $('#zl').change(function(){
         console.log("change checkbox");
          if($(this).is(':checked')){
            deathDatepicker.prop('disabled', false);

          }else{
            deathDatepicker.prop('disabled', true);
            deathDatepicker.val('');
          }
       });

       //Death datepicker

       $('#death_date').datepicker({

           format: 'yyyy-mm-dd',

           todayBtn: false,

           todayHighlight: true,

           autoclose: true,

           changeYear: true,

           defaultViewDate: {year: '1930'}

       });

       //Check if recognition checkbox is checked - if yes, disable recognitin datepicker
       $('#is_righteous').change(function(){
         console.log("change checkbox");
          if($(this).is(':checked')){
            recognitionDatepicker.prop('disabled', false);
          }else{
            recognitionDatepicker.prop('disabled', true);
            recognitionDatepicker.val('');
          }
       });

       //Recognition datepicker

       $('#recognition_date').datepicker({

           format: 'yyyy-mm-dd',

           todayBtn: false,

           todayHighlight: true,

           autoclose: true,

           changeYear: true,

           defaultViewDate: {year: '1930'}

       });

       //Bio

       myElement = $('#bio').summernote({

           disableDragAndDrop: true,

           fontNames : ['CooperHewitt-Medium', 'Arial', 'sans-serif'],

           height: 200,

           toolbar: [

           ['style', ['bold', 'italic', 'underline']],

           ['Misc', ['fullscreen', 'undo', 'redo']]

           ],

           callbacks: {

               onInit: function() { },

               onEnter: function() { },

               onKeyup: function(e) {

                   if($(this).attr('name') == 'description'){

                       summernoteDescriptionCounter = e.currentTarget.innerText;

                       if(summernoteDescriptionCounter !=0){

                           $('.summernote-description-error').addClass('d-none');

                       }else{

                           $('.summernote-description-error').removeClass('d-none');

                       }

                   }

               },

               onKeydown: function(e) { },

               onPaste: function(e) { },

               onChange: function(contents, $editable) { },

               onBlur: function(){ },

               onFocus: function() { }

           }

       });


       $('#exit_from_form').click(function(e) {
           window.location.replace("index.php");
       });

       $('#back_to_list').click(function(e) {
           window.location.replace("museum_app_righteousNations_list.php?tabId=" + redirectId);
       });

       $(".update-btn").click(function() {
         console.log("UPDATE FIELD");

           if (!editMode) return;

           var dbName = $(this).attr('data-db-value');

           var id = $(this).attr('data-id');

           var newValue = $(this).parent().parent().find(':input').val();

           var fieldName = $(this).parent().parent().find(':input').attr('name');

           console.log("dbName: "+dbName);
           console.log("id: "+id);
           console.log("newValue: "+newValue);
           console.log("fieldName: "+fieldName);

           //Update solo para Country, validación diferente por ubicación de inputs
           if(dbName == "countryFlag"){

             updateValue(id, 'nationality', $('#hidden-nationality-id').val(), 'app_museum_righteous_among_nations');

           }else{

             var valid = $('#' + fieldName).valid();

             if (dbName != newValue && valid) {

                 console.log("changing something! Go to Update");

                 $(this).attr('data-db-value', newValue);

                 updateValue(id, fieldName, newValue, 'app_museum_righteous_among_nations');

             }else{
               console.log("nothing changed, nothing happens");
             }
           }

       });


       $('.toggler-info:checkbox').change(function() {

           if (!editMode) return;

           var fieldName = $(this).attr('name');

           var newValue = ($(this).is(":checked")) ? 1 : 0;

           updateValue(righteousId, fieldName, newValue, 'app_museum_righteous_among_nations');

           if(fieldName == "zl"){

             if(newValue!=1){
                updateValue(righteousId, 'death_date', '0000-00-00', 'app_museum_righteous_among_nations');
             }

           }

           if(fieldName == "is_righteous"){

             if(newValue!=1){
                updateValue(righteousId, 'recognition_date', '0000-00-00', 'app_museum_righteous_among_nations');
             }

           }

       });

     }// /.addListeners


     /**
      * @function updateValue
      * @description update individual value in DB.
      *
      * @param {int} id                 - object id.
      * @param {string} fieldName       - the field to modify
      * @param {string} newValue        - object value.
      * @param {int} filter             - filter type.
      */

      var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

         console.log("Update - FieldName: "+fieldName + " - newValue: "+newValue + " - table: "+filter);

         console.log("update value...only once?");

         helper.blockStage("actualizando...");

         var request = $.ajax({

             url: "private/users/museum/general/update_value.php",

             type: "POST",

             data: {

                source: "justos",

                 id: id,

                 fieldName: fieldName,

                 newValue: newValue,

                 filter: filter
             },

             dataType: "json"

         });

         request.done(function(result) {

             console.log("tracing result!");

             console.log(result);

             helper.showToastr(result.title, result.msg);

             if (callback != undefined) callback(callbackParams);

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

             console.log("error");

         });

         if (callback == undefined) helper.unblockStage();

         localStorage.setItem('refresh', 'true');

     }// /.updateValue


     /**
      * @function loadAjaxImages
      * @param loadSource - evaluate this param to get one batch or a single one.
      * @description if we are in edition mode load all the images for Bootstrap File Input.
      */

      var loadAjaxImages = function(from, field, loadSource, config, myPreviews) {

         console.log("loadAjaxImages: from: "+from+" - laodSource: "+loadSource+" - config: "+config+" - myPreviews: "+myPreviews);
         console.log("Righteous id: "+righteousId);
         var request = $.ajax({

             url: "private/users/services/get_list_of_images_new_single.php",

             type: "POST",

             data: {

                 id: righteousId,

                 field: field,

                 source: loadSource,

                 from: from
             },

             dataType: "json"
         });

         request.done(function(result) {

           console.log("result: ");
           console.log(result);
           console.log("picture: "+result[0].picture);

           //Si hay imagen (path en db) armo vista de img
           if(result[0].picture != ""){

             for (var i = 0; i < result.length; i++) {

                 var id = result[i].id;
                 var imgPath = result[i].picture;
                 var splitImgName = imgPath.split('/');

                 var imgName = righteousId + '_' + splitImgName[1] + '_original.jpeg';

                 var path;

                path = 'private/sources/images/righteous/' + result[i].picture + '/' + imgName;

                 myPreviews.push(path);

                 config.push({

                     'key': id,

                     //'internal_order': internalOrder,


                 });

             }// /.ends for

           }


             createNewsImg();

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

         });

     }

    return {

        init: function() {

            editMode = ($('#edit-mode').val() == 1) ? true : false;

            helper.setMenu();

            if (editMode) {

              console.log("Estamos en edit mode!");

                var temp;

                if (document.referrer != "") {

                    temp = helper.getGETVariables(document.referrer, 'tabId');

                    console.log("temP: "+temp);


                } else {

                    temp = null;
                }

                redirectId = (temp == null) ? 1 : temp;

                righteousId = $('#righteous-id').val();

            } else {

                isFirstLoad = false;
            }

            addListeners();

            addFormValidations();

            if(editMode){

              loadAjaxImages('app_museum_righteous_among_nations', 'id', 'righteous', initialPreviewCoverConfig, initialPreviewCoverPaths)

            }else{

                createNewsImg();


            }// /.editMode if else



        } // /.init function

    }; // /.return

 }();

 jQuery(document).ready(function() { MuseumAppRighteousNationsAdd.init(); });
