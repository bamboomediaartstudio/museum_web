/**
 * @sumy Add Prensa Add.
 *
 * @description - APP Survivors Project - Add Data
 *
 * @author Colo baggins <colorado@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */


/**
 * @function MuseumAppSurvivorsProjectAdd
 * @description Initialize and include all the methods of this class.
 */

 var MuseumAppSurvivorsProjectAdd = function() {

    helper = Helper();

    var sendYear = '0000';

    var sendMonth = '00';

    var sendDay = '00';

    var isFromYear = true;

    var editMode;

    var redirectId;

    var survivorId;

    var survivorName;

    var addRedirectId = 0;

    var defaultImageWidth = 1000;

    var defaultImageHeight = 1000;

    var isFirstLoad = true;

    var myElement;

    var saveYear;

    var form = $('#add-survivor-project-data-form');

    //cover config...

    initialPreviewCoverConfig = [];

    initialPreviewCoverPaths = [];

    var letT;

    /**
     * @function addFormValidations
     * @description Asign all the form validations for this page.
     */

     var addFormValidations = function() {

        jQuery.validator.setDefaults({ ignore: ":hidden, [contenteditable='true']:not([name])" });

        form.validate({

            rules: {

                name: { required: true },

                surname: { required: true },

                'checkboxes[]': { required: !0 }

            },

            messages: {

                name: helper.createErrorLabel('Nombre', 'REQUIRED'),

                surname: helper.createErrorLabel('Apellido', 'REQUIRED'),

                'checkboxes[]': helper.createErrorLabel('categorÃ­a', 'AT_LEAST_ONE')

            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();

                window.scroll({top: 0, left: 0, behavior: 'smooth' });
            },

            submitHandler: function(form, event) {

                event.preventDefault();

                event.stopPropagation();

                helper.blockStage("actualizando datos...");

                var formData = new FormData(form);

                formData.append("year", $('#birthday_year').val());
                
                formData.append("month", $('#birthday_month').val());
                
                formData.append("day", $('#birthday_day').val());

                formData.append("countryId", $('#hidden-country-id').val());

                formData.append("cityId", $('#hidden-city-id').val());
                
                formData.append("atThatMomentId", $('#hidden-at-that-moment-id').val());

                formData.append("atTheBeginningId", $('#hidden-at-the-beginning-id').val());

                formData.append("liberationId", $('#hidden-liberation-place-id').val());

                var alternativeNamesVector = new Array();

                $(".alternative-name-repeater").each(function() {

                    var getName = $(this).find(".new_alternative_name").val();

                    var getSurname = $(this).find(".new_alternative_surname").val();

                    if (getName !== "" || getSurname !== "") {

                        var object = {};

                        object.name = $(this).find(".new_alternative_name").val();

                        object.surname = $(this).find(".new_alternative_surname").val();

                        alternativeNamesVector.push(object);

                    }

                });

                formData.append("alternartiveNames", JSON.stringify(alternativeNamesVector));

                console.log("important:");

                console.log(alternativeNamesVector);

                var request = $.ajax({

                    url: "private/users/museum/apps/survivors_project/survivors_project_add.php",

                    type: "POST",

                    contentType: false,

                    processData: false,

                    data: formData,

                    dataType: "json"
                });

                request.done(function(response) {

                  console.log("response antes de swal y toastr");

                  console.log(response);

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch (Number(response.status)) {

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {

                            switch (Number(response.status)) {

                                case 1:

                                window.location.replace("museum_app_survivors_project_list.php");

                                break;

                            }

                        }

                    });


                    helper.showToastr("Se acualizo", 'Se actualizo');

                    helper.unblockStage();

                });

                request.fail(function(jqXHR, textStatus) {

                    console.log(jqXHR);

                    console.log("error");

                    helper.showToastr("ops", 'error');

                    helper.unblockStage();

                });

            }

        });
      }

    /**
     * @function createProfileImage
     * @description Create the newspaper image.
     */

     var createProfileImage = function(){

        $("#main-image").fileinput({

            initialPreviewAsData: true,

            initialPreview: initialPreviewCoverPaths,

            initialPreviewConfig: initialPreviewCoverConfig,

            theme: "fas",

            uploadUrl: "private/users/museum/apps/survivors/survivors_img_upload.php", //on modification image upload only!!

            deleteUrl: "private/users/museum/apps/survivors/survivors_delete.php",

            showCaption: true,

            showPreview: true,

            showRemove: false,

            showUpload: true,

            showCancel: true,

            showClose: false,

            browseOnZoneClick: true,

            previewFileType: 'any',

            language: "es",

            maxFileSize: 0,

            allowedFileTypes: ["image"],

            overwriteInitial: false,

            allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],

            maxFilePreviewSize: 10240,

            minImageWidth: 500,

            mixImageHeight: 500,

            uploadExtraData: function() {   //On modification img upload data!!

                return {

                    id: survivorId,

                    editMode: editMode,

                    source: 'survivors',

                    galleryFolder: 'survivors'

                };

            }

        }).on('filesorted', function(e, params) {



        }).on('fileuploaded', function(e, params) {


        }).on('filebatchuploadcomplete', function(event, files, extra) {

            location.reload();

        }).on("filepredelete", function(jqXHR) {


        }).on('filedeleted', function(event, key, jqXHR, data) {

            return {

                source: 'survivors',

                galleryFolder: 'survivors'

            };

            helper.showToastr("Imagen eliminada", 'Se eliminó la imagen del survivors');

        }).on('fileimageresizeerror', function(event, data, msg) {

            showErrorSwal(msg);

        }).on('fileuploaderror', function(event, data, msg) {

            var form = data.form, files = data.files, extra = data.extra,

            response = data.response, reader = data.reader;

            console.log(event, data, msg);

            //showErrorSwal(msg);

        }).on('filebeforedelete', function() {

            return new Promise(function(resolve, reject) {

                swal({

                    title: "¿Eliminar imagen?",

                    allowOutsideClick: false,

                    html: "La misma no aparecerá más listada. Esta acción no se puede deshacer.",

                    type: "warning",

                    showCancelButton: true,

                    confirmButtonText: 'Si, borrar',

                    cancelButtonText: 'no, salir'

                }).then((result) => {

                    if (result.value) {

                        resolve();

                    }else{

                        reject();
                    }

                })

            });
        });

        $('.kv-cust-btn').on('click', function() {

            origin = $(this).attr('data-origin');

            var $btn = $(this);

            var key = $btn.data('key');

            openKey = key;

            var item = initialPreviewCoverConfig.find(item => item.key == key);

            $('#image-description').val(item.description);

            $('#myModal').modal('show');

        });



    }// /.createProfileImage


    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

     var addListeners = function() {

        $('input[type=radio][name=radio_group]').change(function() {

            if(editMode) updateValue(survivorId, 'liberation_army', this.value, 'app_museum_survivors_project');
           
        });


        $('#bio').summernote({

            disableDragAndDrop: true,

            height: 200,

            toolbar: [

            ['style', ['bold', 'italic', 'underline']],

            ['Misc', ['fullscreen', 'undo', 'redo']]

            ],

            callbacks: {

                onInit: function() { },

                onEnter: function() { },

                onKeyup: function(e) { },

                onKeydown: function(e) { },

                onPaste: function(e) { },

                onChange: function(contents, $editable) { },

                onBlur: function(){ },

                onFocus: function() { }

            }

        });

        $("#m_repeater_1").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_2").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_3").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_4").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_5").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_6").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_7").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_8").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_9").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $('.tutor-selector').select2({ tokenSeparators: [',', ' '], placeholder: "seleccioná o agregá otros paises de referencia" });

        $('.spouse-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá a su/s cónyugue/s"

        });

        $('.son-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá a sus/s hijo/s"

        });

        $('.grandson-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá a su/s hijo/s"

        });

        $('.book-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá a su/s libro/s"

        });

        $('.film-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá a su/s películas/s"

        });

        $('.honor-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá a su/s distinciones/s"

        });

        $('.gueto-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá los guetos donde estuvo"

        });

        $('.campo-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá los campos donde estuvo"

        });

       $('[data-toggle="tooltip"]').tooltip();

       createCountryFinder();

       createAtThatMomentFinder();

       createAtTheBeginningFinder();

       createLiberationPlace();

       if(editMode && $('#hidden-country-id').val() != "") findCities($('#hidden-country-id').val()); 

       $('#entry_year').datepicker({

           format: 'yyyy',

           todayBtn: false,

           todayHighlight: true,

           autoclose: true,

           changeYear: true,

           changeMonth: false,

            viewMode: "years", 

            minViewMode: "years",

           defaultViewDate: {year: '1930'}

       }).datepicker().on('changeDate', function (ev) {

            calculateAge();

            if(editMode){

                updateValue(survivorId, 'entry_year', $('#entry_year').val(), 'app_museum_survivors_project');

            }

        });


       $('#birthday_year').datepicker({

           format: 'yyyy',

           todayBtn: false,

           todayHighlight: true,

           autoclose: true,

           changeYear: true,

           changeMonth: false,

            viewMode: "years", 

            minViewMode: "years",

           defaultViewDate: {year: '1930'}

       }).datepicker().on('changeDate', function (ev) {

            $('#birthday_month').removeAttr('disabled');

            if(editMode){

                console.log("updateDate: " + updateDate);

                var updateDate = $('#birthday_year').val() + '-' + $('#birthday_month').val() + '-' + $('#birthday_day').val(); 

                updateValue(survivorId, 'birthday', updateDate, 'app_museum_survivors_project');

            }else{

                saveYear = ev.date.getFullYear();

                sendYear = ev.date.getFullYear();

                sendMonth = '00';

                sendDay = '00';

                $('#birthday_month').datepicker("setDate", new Date(ev.date.getFullYear(),1,1) );

                $('#birthday_month').val('');

                $('#birthday_day').addAttr('disabled');

            }

    });

       $('#birthday_month').datepicker({

           format: 'mm',

           todayBtn: false,

           todayHighlight: true,

           autoclose: true,

           changeYear: false,

           changeMonth: false,

            viewMode: "months", 

            minViewMode: "months",

            showButtonPanel: false,

            defaultViewDate: {year: '1955'}

       }).datepicker().on('changeDate', function (ev) {

        $('#birthday_day').removeAttr('disabled');

        if(editMode){

            console.log("updateDate: " + updateDate);

            var updateDate = $('#birthday_year').val() + '-' + $('#birthday_month').val() + '-' + $('#birthday_day').val(); 

            updateValue(survivorId, 'birthday', updateDate, 'app_museum_survivors_project');
        
        }else{

            sendMonth = ev.date.getMonth();

            sendDay = '00';

            $('#birthday_day').datepicker("setDate", new Date(saveYear,ev.date.getMonth(),1) );

            $('#birthday_day').val('');

        }

    });

       $('#birthday_day').datepicker({

           format: 'dd',

           todayBtn: false,

           todayHighlight: true,

           autoclose: true,

           changeYear: false,

           changeMonth: false,

            viewMode: "days", 

            minViewMode: "days",

            showButtonPanel: false,

            defaultViewDate: {year: '1955'}


       });

       $('#birthday_day').datepicker().on('changeDate', function (ev) {

        if(editMode){
           
            console.log("updateDate: " + updateDate);

            var updateDate = $('#birthday_year').val() + '-' + $('#birthday_month').val() + '-' + $('#birthday_day').val(); 

            updateValue(survivorId, 'birthday', updateDate, 'app_museum_survivors_project');

        }else{

            sendDay = ev.date.getDate();

            console.log("all: " + sendYear + " - " + sendMonth + " - " + sendDay);

        }

       });


       $('#death_date').datepicker({

           format: 'yyyy-mm-dd',

           todayBtn: false,

           todayHighlight: true,

           autoclose: true,

           changeYear: true,

           defaultViewDate: {year: '1930'}

       });

       


       $('.emigrated').change(function() {

            var checked = ($(this).prop('checked'));

            console.log("checked: " + checked);

            $('#emigrated_place').prop("disabled", !checked);

        });


       $('.war-emigrated').change(function() {

            var checked = ($(this).prop('checked'));

            console.log("checked: " + checked);

            $('#war_emigrated_place').prop("disabled", !checked);

        });


       $('.war-ghetto').change(function() {

            var checked = ($(this).prop('checked'));

            console.log("checked: " + checked);

            $('#war_ghetto_name').prop("disabled", !checked);

        });


       $('.war-ghetto').change(function() {

            var checked = ($(this).prop('checked'));

            console.log("checked: " + checked);

            $('#war_ghetto_name').prop("disabled", !checked);

        });

        $('.entry-legal').change(function() {
            console.log("Change entry legal");

            var checked = ($(this).prop('checked'));

            console.log("checked: " + checked);

            $('#entry_illegal_route').prop("disabled", checked);

        });



       $('#exit_from_form').click(function(e) {
           window.location.replace("index.php");
       });

       $('#back_to_list').click(function(e) {
           window.location.replace("museum_app_survivors_list.php?tabId=" + redirectId);
       });

       $(".update-btn").click(function() {
           
           if (!editMode) return;

           var dbName = $(this).attr('data-db-value');

           var id = $(this).attr('data-id');

           var newValue = $(this).parent().parent().find(':input').val();

           var fieldName = $(this).parent().parent().find(':input').attr('name');

           var valid = $('#' + fieldName).valid();

           if (dbName != newValue && valid) {

                $(this).attr('data-db-value', newValue);

                updateValue(id, fieldName, newValue, 'app_museum_survivors_project');

            }

       });


       $('.toggler-info:checkbox').change(function() {

           if (!editMode) return;

           var fieldName = $(this).attr('name');

           var newValue = ($(this).is(":checked")) ? 1 : 0;

           updateValue(survivorId, fieldName, newValue, 'app_museum_survivors_project');

       });

     }

     var createCountryFinder = function(){

        t = new Bloodhound({

           datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

           queryTokenizer: Bloodhound.tokenizers.whitespace,

           prefetch:{

               url : "private/users/services/get_list_of_countries.php",

               cache: false
           }

       })

       var context = $("#country-list .typeahead").typeahead(null, {

           hint: false,

           highlight: false,

           minLength: 1,

           name: "best-pictures",

           display: "value",

           source: t,

           autoselect: true,

           templates: {

               suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")

           }

       }).on("typeahead:render", function() {

           var searchTerm = $(this).val();

           $('#country-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

       }).on('typeahead:selected', function (e, datum) {

            findCities(datum.id);

            if(editMode){

                updateValue(survivorId, 'nationality', datum.id, 'app_museum_survivors_project');
                
                updateValue(survivorId, 'city', -1, 'app_museum_survivors_project');

            }else{

                $('#hidden-country-id').val(datum.id);

            }

            if(datum.id == 51 || datum.id == 11 || datum.id == 236){

                $('.emigrated-toggle').removeClass('disabled');

            }else{

                $('.emigrated-toggle').prop("disabled", true);

                $('.emigrated-toggle').addClass('disabled');

                var test = $('.toggler-info-emigrated:checkbox').is(":checked");

                if(test){

                    $('.toggler-info-emigrated').click();

                }



            }

            

       }).on('keyup', this, function (event) {

       }).on('keydown', this, function (event) {

       }).on('focus', this, function(event){

           if(editMode) $('#hidden-country-id').val("");

       });

     }

     var calculateAge = function(){

        var entry = parseInt($('#entry_year').val());

        var birth = parseInt($('#birthday_year').val());

        if(isNaN(birth)) return;

        if(entry < birth){

            swal({

                title: 'opss!',

                allowOutsideClick: false,

                html: 'La fecha de llega a la argentina no puede ser menor a la fecha de su nacimiento. Por favor, revisá ambos valores',

                type: 'error',

                showCancelButton: false,

                confirmButtonText: 'entendido!'

            })

        }else{

            var name = $('#name').val();

            var surname = $('#surname').val();

            var st = '<b>' + name + ' ' + surname + '</b> tenía <b>' + (entry - birth).toString() + '</b> años al momento de su ingreso llegada';

            $('.age-calculator').html(st);

        }

     }

     var createAtThatMomentFinder = function(){

        t = new Bloodhound({

           datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

           queryTokenizer: Bloodhound.tokenizers.whitespace,

           prefetch:{ url : "private/users/services/get_list_of_countries.php", cache: false }

       })

       var context = $("#at-that-moment-list .typeahead").typeahead(null, {

           hint: false, highlight: false, minLength: 1, name: "best-pictures",

           display: "value", source: t, autoselect: true,

           templates: { suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")}

       }).on("typeahead:render", function() {

           var searchTerm = $(this).val();

           $('#country-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

       }).on('typeahead:selected', function (e, datum) {

            if(editMode){

                updateValue(survivorId, 'nationality_at_that_moment', datum.id, 'app_museum_survivors_project');
                
            }else{

                $('#hidden-at-that-moment-id').val(datum.id);

            }

       }).on('keyup', this, function (event) {

       }).on('keydown', this, function (event) {

       }).on('focus', this, function(event){

           if(editMode) $('#hidden-country-id').val("");

       });

     }

     var createAtTheBeginningFinder = function(){

        t = new Bloodhound({

           datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

           queryTokenizer: Bloodhound.tokenizers.whitespace,

           prefetch:{ url : "private/users/services/get_list_of_countries.php", cache: false }

       })

       var context = $("#at-the-beginning-list .typeahead").typeahead(null, {

           hint: false, highlight: false, minLength: 1, name: "best-pictures",

           display: "value", source: t, autoselect: true,

           templates: { suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")}

       }).on("typeahead:render", function() {

           var searchTerm = $(this).val();

           $('#country-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

       }).on('typeahead:selected', function (e, datum) {

            if(editMode){

                updateValue(survivorId, 'nationality_at_the_beginning_of_war', datum.id, 'app_museum_survivors_project');
                
            }else{

                $('#hidden-at-the-beginning-id').val(datum.id);

            }

       }).on('keyup', this, function (event) {

       }).on('keydown', this, function (event) {

       }).on('focus', this, function(event){

           //if(editMode) $('#hidden-country-id').val("");

       });

     }

     var createLiberationPlace = function(){

        t = new Bloodhound({

           datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

           queryTokenizer: Bloodhound.tokenizers.whitespace,

           prefetch:{ url : "private/users/services/get_list_of_countries.php", cache: false }

       })

       var context = $("#liberation-place .typeahead").typeahead(null, {

           hint: false, highlight: false, minLength: 1, name: "best-pictures",

           display: "value", source: t, autoselect: true,

           templates: { suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")}

       }).on("typeahead:render", function() {

           var searchTerm = $(this).val();

           $('#country-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

       }).on('typeahead:selected', function (e, datum) {

            if(editMode){

                updateValue(survivorId, 'liberation_place', datum.id, 'app_museum_survivors_project');
                
            }else{

                $('#hidden-liberation-place-id').val(datum.id);

            }

       }).on('keyup', this, function (event) {

       }).on('keydown', this, function (event) {

       }).on('focus', this, function(event){

           //if(editMode) $('#hidden-country-id').val("");

       });

     }
     
     var findCities = function(id){

        $('#cities-list').removeClass('disabled');

        if(letT != undefined){

            letT.clear();

            letT.clearPrefetchCache();
            
            letT.clearRemoteCache();

            $('.cleanner').empty();

            $('.cleanner').append('<input type="text" id="xcv" name="xcv" class="typeahead form-control m-input" placeholder="Ciudad" >');

            $('.cleanner').append('<input type="hidden" id="hidden-city-id" name="hidden-city-id" >');
        }

        letT = new Bloodhound({

            datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

            queryTokenizer: Bloodhound.tokenizers.whitespace,

            prefetch:{

                url : "private/users/services/get_list_of_cities_by_country_id.php?id=" + id,

                cache: false

            }

        })

        letT.initialize(true);

        var context = $("#cities-list .typeahead").typeahead(null, {

           hint: false,

           highlight: false,

           minLength: 1,

           name: "best-pictures",

           display: "value",

           source: letT,

           autoselect: true,

           templates: { suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>") }

       }).on("typeahead:render", function() {

           $('#cities-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

       }).on('typeahead:selected', function (e, datum) {

            if(editMode){
                
                updateValue(survivorId, 'city', datum.id, 'app_museum_survivors_project');

            }else{

                $('#hidden-city-id').val(datum.id);
            }


       }).on('keyup', this, function (event) {

       }).on('keydown', this, function (event) {

       }).on('focus', this, function(event){

           if(editMode) $('#hidden-city-id').val("");

       });

     }


     /**
      * @function updateValue
      * @description update individual value in DB.
      *
      * @param {int} id                 - object id.
      * @param {string} fieldName       - the field to modify
      * @param {string} newValue        - object value.
      * @param {int} filter             - filter type.
      */

      var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

         console.log("Update - FieldName: "+fieldName + " - newValue: "+newValue + " - table: "+filter);

         console.log("update value...only once?");

         helper.blockStage("actualizando...");

         var request = $.ajax({

             url: "private/users/museum/general/update_value.php",

             type: "POST",

             data: {

                 id: id,

                 fieldName: fieldName,

                 newValue: newValue,

                 source: "sobrevivientesProject",

                 filter: filter
             },

             dataType: "json"

         });

         request.done(function(result) {

             console.log("tracing result!");

             console.log(result);

             helper.showToastr(result.title, result.msg);

             if (callback != undefined) callback(callbackParams);

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

             console.log("error");

         });

         if (callback == undefined) helper.unblockStage();

         localStorage.setItem('refresh', 'true');

     }

     /**
      * @function loadAjaxImages
      * @param loadSource - evaluate this param to get one batch or a single one.
      * @description if we are in edition mode load all the images for Bootstrap File Input.
      */

      var loadAjaxImages = function(from, field, loadSource, config, myPreviews) {

         console.log("loadAjaxImages: from:" + from + " - laodSource: " + loadSource + " - config: " + config + " - myPreviews: " + myPreviews);

         var request = $.ajax({

             url: "private/users/services/get_survivor_project_main_image.php",

             type: "POST",

             data: {

                 id: survivorId,

                 field: field,

                 source: loadSource,

                 from: from
             },

             dataType: "json"
         });

         request.done(function(result) {

            console.log(result.length);

           if(result.length >0){

             for (var i = 0; i < result.length; i++) {

                 var id = result[i].id;
                 
                 var imgPath = result[i].picture;
                 
                 var itemName = id + '_' + imgPath + '_original.jpeg';
                 
                 path = 'private/sources/images/survivors_project/' + id + '/' + imgPath + '/' + itemName;

                 console.log("prueba: " + path);

                 myPreviews.push(path);

                 config.push({

                    'key': id,

                     //'internal_order': internalOrder,


                 });

             }

           }else{
             
             console.log("sin imagenes para este item");
           
           }

             createProfileImage();

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

         });

     }

    return {

        init: function() {

            editMode = ($('#edit-mode').val() == 1) ? true : false;

            helper.setMenu();

            if (editMode) {

              console.log("Estamos en edit mode!");

                var temp;

                if (document.referrer != "") {

                    temp = helper.getGETVariables(document.referrer, 'tabId');

                    console.log("temP: "+temp);


                } else {

                    temp = null;
                }

                redirectId = (temp == null) ? 1 : temp;

                survivorId = $('#survivor-project-id').val();

            } else {

                isFirstLoad = false;
            }

            addListeners();

            addFormValidations();

            if(editMode){

                loadAjaxImages('app_museum_survivors_project_main_picture', 'sid', 'survivors', initialPreviewCoverConfig, initialPreviewCoverPaths)

            }else{

                createProfileImage();


            }

        }

    };

 }();

 jQuery(document).ready(function() { MuseumAppSurvivorsProjectAdd.init(); });