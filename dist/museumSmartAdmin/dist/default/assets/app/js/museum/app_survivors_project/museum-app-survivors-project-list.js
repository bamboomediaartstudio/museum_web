/**
* @summary survivors project list
*
* @description -  
*
* @author <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/

/**
* @function ModulesList
* @description Initialize and include all the methods of this class.
*/

var ModulesList = function() {

    helper = new Helper();

    var cat;

         /**

         * @function addListeners
         * @description Asign all the listeners / event habdlers for this page.
         */

         var addListeners = function(){


            $(window).focus(function() {

                var refresh = localStorage.getItem('refresh');

                if(refresh == 'true'){

                    localStorage.removeItem('refresh');

                    location.reload();

                } 

            });

            $(window).blur(function() { localStorage.removeItem('refresh'); });

        }


    /**
    * @function createDataTable
    * @description Create all the datatables that we use as lists.
    */

    var createDataTable = function(){

        var table = $('#tab-1').DataTable({

            "language": helper.getDataTableLanguageConfig(),

             "processing": true,

             "serverSide": true,

             'serverMethod': 'post',

             "ajax": {

                 url:"private/users/museum/apps/survivors_project/dataController.php",

                 dataSrc: "data",

                 dataType:'json',

                 "data":function(outData){

                     // what is being sent to the server
                     
                     outData.table = "app_museum_survivors_project";
                     
                     outData.searchColumn = "name";
                     
                     outData.searchColumnTwo = "surname";
                    
                     console.log(outData);
                     
                     return outData;
                 },
                 dataFilter:function(inData){
                                          
                     return inData;
                 },
                 error:function(err, status){
                                        
                     console.log(err);
                 },
             },

            "columns": [

            { "data": "name", "width": "25%", responsivePriority: 0, orderable: false, targets: '_all', render: function ( data, type, row ) { return data.substr( 0, 50 ) + "..." }}, 
            
            { "data": "surname", "width": "25%", responsivePriority: 0, orderable: false, targets: '_all', render: function ( data, type, row ) { return data.substr( 0, 50 ) + "..." }}, 
            
            { "data": "alternative_surname", "width": "25%", responsivePriority: 0, orderable: false, targets: '_all', render: function ( data, type, row ) { return data.substr( 0, 50 ) + "..." }}, 

            { "defaultContent": '',

            render: function(data, type, row) {

                var st;

                if(cat == 3){

                    st = '-';
               
                }else{

                    var checked = (row.active == 1 ) ? 'checked' : '';

                    //var label = (row.active == 1 ) ? 'activo' : 'inactivo'; 

                    //st = '<td><input '  + checked + ' type="checkbox" name="checky" data-name = ' + row.name + ' data-id=' +  row.id +  ' class="form-check-input"><label class="form-check-label" for="form-check-label">' + label + '</label></div></td>';
                    st = '<td><input '  + checked + ' type="checkbox" name="checky" data-name = ' + row.name + ' data-id=' +  row.id +  ' class="form-check-input"><label class="form-check-label" for="form-check-label"></label></div></td>';

                }

                return st;
           
            }, responsivePriority: 1, orderable: false, targets: '_all'

            },

        
            { "defaultContent": '',

            render: function(data, type, row) {


                var st;

                st = '<td><span><button class="edit_row m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"><i class="la la-edit"></i></button><button class="delete_row m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"><i class="la la-trash"></i></button></span></td>';

                return st;                

            }, responsivePriority: 1, orderable: false, targets: '_all'

            }
            
            ],

             rowCallback: function ( row, data ) {


                //console.log(data.active + " - " + row + " - data URL: " + data.url);

               //$('input.editor-active', row).prop( 'checked');// 'true');//data.active == 1 ); 

               //$('.my-checkbox').on('change.bootstrapSwitch', function(e) {

                //console.log(e.target.checked);

            //});


               
             },

             
             "fnDrawCallback": function(settings){

                $("[name='checky']").click(function(){

                    var id = $(this).attr("data-id");

                    var name = $(this).attr("data-name");

                    var action = 'update';

                    var status;

                if($(this).is(':checked')){

                    status = "true";

                } else {

                    status = "false";
                
                }

                //updateUserStatus(id, status, name, 'update');

                updateUserStatus(id, status, name, 'update', null, null, null, null, 'active');

                //updateUserStatus(id, state, name, 'update', closestRow, table.row(closestRow), null, null, 'active');

            });

                //console.log("otra");

                //console.log($("[name='my-checkbox']").prop( 'checked'));

               // $("[name='my-checkbox']").bootstrapSwitch();

               // $("[name='my-checkbox']").prop('checked', 1);



                
                //console.log("so why not?");

                //console.log($("[name='my-checkbox']").prop( 'checked'));//, 1)

                //$("[name='my-checkbox']").bootstrapSwitch();
                
                //alert("que pasa");

                //$('input.editor-active').prop( 'checked', 'true');// 'true');//data.active == 1 ); 

             }

        });

        $('body').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

            var closestRow = $(this).closest('tr');

            var data = table.row(closestRow).data();

            var data_row = table.row($(this).closest('tr')).data();

            var id = data_row.id;

            var name = data_row.name;

            console.log("state update: id: "+id+ " name: "+name + " - "  + state);

            updateUserStatus(id, state, name, 'update', closestRow, table.row(closestRow), null, null, 'active');

        });

        
        $('body').on( 'click', '.edit_row', function () {

            var data_row = table.row($(this).closest('tr')).data();

            var id = data_row.id;

            //window.location.href = "/museum_app_survivors_project_list.php?id=" + id; //En local va a not found...
            window.location.href = "museum_app_survivors_project_add.php?id=" + id;


        });

        
        

        $('body').on( 'click', '.delete_row', function () {

            var row = table.row($(this).parents('tr'));

            
            var data_row = table.row($(this).closest('tr')).data();

            var animatable = $(this).parents('tr');

            var name = data_row.name;

            var id = data_row.id;

            console.log(row);

            console.log(row.data());

            console.log(id);


            var actualTable = $("#tab-1").DataTable();

            /*var row = table.row($(this).parents('tr'));

            var animatable = $(this).parents('tr');

            var data = table.row( $(this).parents('tr') ).data();

            var order = data[0];

            var id = data[1];

            var news = data[2];

            var actualTable = $("#tab-1").DataTable();*/

            Swal({

                title: "¿Eliminar?",

                html: 'Esta acción eliminará el sobreviviente <b>'+name+'</b>.<br><br>Esta acción no se puede deshacer. <br><br>Recordá que también podés usar botón de <b>activo</b> y <b>inactivo</b> para modificar la visibilidad del mismo.<br><br>¿Eliminar de todos modos?',

                type: 'error',

                showCancelButton: true,

                confirmButtonText: 'Si, eliminar',

                cancelButtonText: 'no, salir'

            }).then((result) => {

                if(result.value == true){

                    updateUserStatus(id, 'true', name, 'delete', animatable, row, actualTable, null,  'deleted');

                }

            })

        });

    }

    /**
    * @function updateUserStatus
    *
    * @description Update / Delete user status from list: we use this to mark the user as online / offline and
    * for delete an user completely from the list.
    *
    * @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
    * @param {boolean} state                    - True or false: use it for both, update and delete.
    * @param {string} name                  - The name of the person being manipulated.
    * @param {string} action                    - Posibilities: 'update' or 'delete'. 
    * @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
    * @param {DataTable} row                    - Reference to the row that we are dealing with.
    * @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
    * @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
    */

    var updateUserStatus = function(id, state, name, action, animatable, row, processTable = null, order = null, defaultColumn = null){

        console.log(id, state, name, action, defaultColumn);

        //return;

        
        helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando sobreviviente...');

        var url = "private/users/museum/general/update_status.php"; 

        $.ajax({

            type: "POST",

            url: url,

            data: {id: id, status:state, action:action, name:name, source: 'survivors_project',

            table:'app_museum_survivors_project', defaultColumn:defaultColumn},

            success: function(result){

                console.log(result);

                //return;

                helper.unblockStage();

                if(action == 'update'){

                    helper.showToastr("Actualización", "Se actualizó el estado del sobreviviente.");

                }else{

                    animatable.fadeOut('slow','linear',function(){

                        console.log(result.msg);

                        helper.showToastr('ELIMINADO', 'El sobreviviente se eliminó exitosamennte.');

                        var count = 0;

                        var data = processTable.rows().data();

                        processTable.rows().every( function (rowIdx, tableLoop, rowLoop) {

                            if(processTable.cell(rowIdx, 0).data() > Number(order)){

                                var saveValue = Number(processTable.cell(rowIdx, 0).data() - 1);

                                processTable.cell(rowIdx, 0).data(saveValue);

                            }

                        });

                        row.remove().draw(true);

                        processTable.rows().invalidate().draw(false);

                    });

                }

            },

            error: function(xhr, status, error) {

                console.log(xhr);

                var err = eval("(" + xhr.responseText + ")");

                alert(err.Message);
            },

            dataType: "json"
        });

    }

    /**
    * @function animateAddedElement.
    * @description animate the last object when we come from editing.
    */

    var animateAddedElement = function(){

        TweenMax.to(window, 1, {scrollTo:{y:"max"}, delay:1});

        var y = document.getElementsByClassName('row-1');

        var n = $(y).css("backgroundColor");

        var c = $(y).css("color");

        TweenMax.to(y, 1, {backgroundColor:"rgba(132,173,169,0.37)", color: '#1c9081', ease:Cubic.easeOut, delay:2});

        TweenMax.to(y, 1, {backgroundColor:n, color:c, ease:Cubic.easeOut, delay:.5, delay:3});

    }

    return {

        init: function() {

            cat = $('#cat').val();

            addListeners();

            createDataTable();

            helper.setMenu();

            var highlight = (helper.getGETVariables(window.location, "highlight") == "true") ? true : false;
            
            if(highlight == true) animateAddedElement();

        }

    };

}();

jQuery(document).ready(function() { ModulesList.init(); });