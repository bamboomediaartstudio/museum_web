/**
 * @summary News list.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
* @function AssistancesManager
* @description Manage all the events.
*/

var AssistancesManager = function() {

    helper = new Helper();

    var calendar = $('#m_calendar');

    var readableDateFormat = "dd-mm-yyyy";
    //var readableDateFormat = "yyyy-mm-dd";

    
    /**
    * @function addListeners
    * @description Add all the listeners.
    */

    var addListeners = function(){

        $('[data-toggle="tooltip"]').tooltip();

        $('#date-from').datepicker({orientation: 'bottom', autoclose: true, language: 'es',  format: readableDateFormat});

        $('#date-to').datepicker({ autoclose: true, language: 'es',  format: readableDateFormat});

        $('.export-excel-individual, .export-excel').click(function(){

            var name = $(this).attr('name');

            var dateFrom = $('#date-from').data('datepicker').getFormattedDate('yyyy-mm-dd'); //$('#date-from').datepicker('getFormattedDate');

             var dateTo = $('#date-to').data('datepicker').getFormattedDate('yyyy-mm-dd'); //$('#date-to').datepicker('getFormattedDate');

             console.log(">> " + dateFrom, dateTo);

             //return;

            if(dateFrom == null || dateFrom == ""){

                var title = "ops!! y la fecha de comienzo?";

                var msg = "Debes indicar la fecha desde la cual querés exportar!";

                showErrorSwal(title, msg);

            }else if(dateTo == null || dateTo == "") {
              
                var title = "ops!! y la fecha de fin?";

                var msg = "Debes indicar la fecha hasta la cual querés exportar!";

                showErrorSwal(title, msg);
          
            }else{

                if(dateTo <= dateFrom){
                    
                    var title = "ehh!!";

                    var msg = "la fecha de fin tiene que ser mayor a la de inicio. Por favor, revisá las fechas!";

                    showErrorSwal(title, msg);

                }else{

                    if(name == 'group'){

                        $finalURL = 'https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/private/users/download/download-by-date.php?from=' + dateFrom + '&to=' + dateTo;

                    }else{

                        $finalURL = 'https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/private/users/download/download-ind-by-date.php?from=' + dateFrom + '&to=' + dateTo;

                    }

                    window.location.href = $finalURL;
                }

            }
        
        })

    }

    /**
     * @function showErrorSwal
     * @param msg - the msg.
     * @description Swal alert.
     */

     var showErrorSwal = function(title, msg){

        Swal({

            title: title,

            html: msg,

            type: 'error',

            confirmButtonText: 'Entendido!'

        })
    }

    return {

        init: function() {

            addListeners();

        }
    };

}();

jQuery(document).ready(function() { AssistancesManager.init(); });