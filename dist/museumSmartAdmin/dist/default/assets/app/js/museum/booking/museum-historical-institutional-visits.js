/**
* @summary historical! 
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/

/**
* @function Init
* @description Initialize and include all the methods of this class.
*/

var Init = function() {

    helper = new Helper();

    //var host = window.location.host;

     /**

     * @function addListeners
     * @description Asign all the listeners / event habdlers for this page.
     */

     var addListeners = function(){

        $("[name='my-checkbox']").bootstrapSwitch();

        $("[name='my-payment-checkbox']").bootstrapSwitch();

        $(window).focus(function() {

            var refresh = localStorage.getItem('refresh');

            if(refresh == 'true'){

                localStorage.removeItem('refresh');

                location.reload();

            } 

        });

        $(window).blur(function() { localStorage.removeItem('refresh'); });

    }


/**
* @function createDataTable
* @description Create all the datatables that we use as lists.
*/

var createDataTable = function(){

    var fileName = $('#historical-list').val();

    var table = $('table.display').DataTable({

        dom: 'Bfrtip',

        buttons: [ 'copy', 'csv', 'excel', 'pdf', 'print'],

        buttons: ['copy', 'print',

        {
            extend: 'csvHtml5',
            title: fileName
        },


        {
            extend: 'excelHtml5',
            title: fileName
        },
        {
            extend: 'pdfHtml5',
            title: fileName
        }
        ],

        pageLength: 50,

        "language": helper.getDataTableLanguageConfig(),

        bAutoWidth: false, 

        processing:true,

        "columns": [

        { "width": "5", responsivePriority: 4, orderable: true, targets: 0}, 

        { "visible": false, orderable:false },

        { "width": "20%", responsivePriority: 1, orderable: false, targets: '_all' }, 
                
        { "width": "75%", responsivePriority: 0, orderable: false, targets: '_all' }
        ]

    });

}


return {

    init: function() {

        addListeners();

        createDataTable();
    }

};

}();

jQuery(document).ready(function() { Init.init(); });