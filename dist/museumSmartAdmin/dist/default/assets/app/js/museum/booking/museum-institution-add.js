/**
 * @summary Add new institution.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
 * @function PageInit
 * @description Initialize and include all the methods of this class.
 */

 var PageInit = function() {

    helper = Helper();

    var editMode;

    var institutionId;

    var form = $('#add-data-form');
    
    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

     var addListeners = function() {

        $('select').change(function(){

            if($(this).attr('name') == 'booking_add'){

                addBooking($(this).val());

            }else{

                updateValue(institutionId, $(this).attr('name'), $(this).val(), 'museum_booking_institutions');

            }

        });

        $('.delete_row').click(function(e){

            var id = $(this).attr('data-booking-id');

            swal({

                title: '¿ELIMINAR TURNO?',

                allowOutsideClick: false,

                html: 'Esta acción no se puede deshacer. Este turno volverá a estar online para que cualquier otra institución que se registre pueda tomarlo. ¿Continuar?',

                type: 'error',

                showCancelButton: true,

                confirmButtonText: 'si, eliminar',

                cancelButtonText: 'no, salir'

            }).then((result) => {

                if (result.value) {

                    deleteBooking(id, $(this).parent());

                }

            });

        });

        $('.date-div').click(function(e){

            if($(this).hasClass('is-not-selectable')){

                swal({

                    title: "opss!",

                    allowOutsideClick: false,

                    html: "No podés eliminar un evento pasado",

                    type: "error",

                    confirmButtonText: "Entendido!"

                })


            }

        })

        $('[data-toggle="tooltip"]').tooltip();

        $('#exit_from_form').click(function(e) { window.location.replace("index.php"); });

        $('#back_to_list').click(function(e) { window.location.replace("museum_historical_institutions.php"); });

        $(".update-btn").click(function() {

            if (!editMode) return;

            var dbName = $(this).attr('data-db-value');

            var id = $(this).attr('data-id');

            var newValue = $(this).parent().parent().find(':input').val();

            var fieldName = $(this).parent().parent().find(':input').attr('name');

            var valid = true;

            if (dbName != newValue && valid) {

                $(this).attr('data-db-value', newValue);

                updateValue(id, fieldName, newValue, 'museum_booking_institutions');

            }

        });


    }

    var addBooking = function(id){ 

        var institutionId = $('#institution-id').val();

        helper.blockStage("agregando turno...");

        var request = $.ajax({

            url: "private/users/museum/booking/add_booking_for_institution.php",

            type: "POST",

            data: {id: id, institutionId:institutionId},

            dataType: "json"

        });

        request.done(function(result) {

            helper.unblockStage();

            location.reload();


        });

        request.fail(function(jqXHR, textStatus) {

            helper.unblockStage();

            console.log(jqXHR);

            console.log("error");

        });

    };

    var deleteBooking = function(id, item){

        helper.blockStage("eliminando turno...");

        var request = $.ajax({

            url: "private/users/museum/booking/delete_booking.php",

            type: "POST",

            data: {id: id},

            dataType: "json"

        });

        request.done(function(result) {

            console.log(result);

            helper.unblockStage();

            item.fadeOut('slow', function() {

                helper.showToastr("Se eliminó un turno!", 'El turno fue liberado, aparecerá de forma automática en la web y podrá ser tomado por otra institución.');

                location.reload();

            })

        });

        request.fail(function(jqXHR, textStatus) {

            helper.unblockStage();

            console.log(jqXHR);

            console.log("error");

        });
    }

    /**
     * @function window.onbeforeunload
     * @description scroll to top
     */

     window.onbeforeunload = function () { if(!editMode) window.scrollTo(0,0); }

    /**
     * @function updateValue
     * @description update individual value in DB.
     *
     * @param {int} id                 - object id.
     * @param {string} fieldName       - the field to modify
     * @param {string} newValue        - object value.
     * @param {int} filter             - filter type.
     */

     var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/update_value.php",

            type: "POST",

            data: {

                id: id,
                
                fieldName: fieldName,
                
                newValue: newValue,
                
                filter: filter
            },

            dataType: "json"

        });

        request.done(function(result) {

            console.log(result);

            helper.showToastr(result.title, result.msg);

            if (callback != undefined) callback(callbackParams);

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });

        if (callback == undefined) helper.unblockStage();

        localStorage.setItem('refresh', 'true');

    }

    return {

        init: function() {

            editMode = ($('#edit-mode').val() == 1) ? true : false;

            if (editMode) {

                institutionId = $('#institution-id').val();

            }

            addListeners();

            helper.setMenu();

        }

    };

}();

jQuery(document).ready(function() { PageInit.init(); });