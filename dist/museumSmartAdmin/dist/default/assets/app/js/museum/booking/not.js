/**
* @summary booking list!
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/

/**
* @function MuseumBookingEventList
* @description Initialize and include all the methods of this class.
*/

var MuseumBookingEventList = function() {

    helper = new Helper();

    //var host = window.location.host;

     /**

     * @function addListeners
     * @description Asign all the listeners / event habdlers for this page.
     */

     var addListeners = function(){

        $("[name='my-checkbox']").bootstrapSwitch();

        $("[name='my-payment-checkbox']").bootstrapSwitch();

        $(window).focus(function() {

            var refresh = localStorage.getItem('refresh');

            if(refresh == 'true'){

                localStorage.removeItem('refresh');

                location.reload();

            } 

        });

        $(window).blur(function() { localStorage.removeItem('refresh'); });

    }


/**
* @function createDataTable
* @description Create all the datatables that we use as lists.
*/

var createDataTable = function(){

    var fileName = $('#booking-name').val();

    var table = $('table.display').DataTable({

        dom: 'Bfrtip',

        buttons: [ 'copy', 'csv', 'excel', 'pdf', 'print'],

        buttons: ['copy', 'print',

        {
            extend: 'csvHtml5',
            title: fileName
        },


        {
            extend: 'excelHtml5',
            title: fileName
        },
        {
            extend: 'pdfHtml5',
            title: fileName
        }
        ],

        pageLength: 50,

        "language": helper.getDataTableLanguageConfig(),

        bAutoWidth: false, 

        processing:true,

        "columns": [

        { "width": "5", responsivePriority: 4, orderable: true, targets: 0}, 

        { "visible": false, orderable:false },

        { "width": "85%", responsivePriority: 1, orderable: false, targets: '_all' }, 
        
        { "width": "85%", responsivePriority: 1, orderable: false, targets: '_all' }, 

        { "width": "85%", responsivePriority: 1, orderable: false, targets: '_all' }, 

        { "width": "5%", responsivePriority: 0, orderable: false, targets: '_all', }, 

        { "width": "5%", responsivePriority: 0, orderable: false, targets: '_all' }
        ]

    });

    $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-payment-checkbox"]', function(event, state) {

        var closestRow = $(this).closest('tr');

        var data = table.row(closestRow).data();

        var id = data[1];

        var name = data[2];

        //console.log(id, name);

        updateUserStatus(id, state, name, 'update', closestRow, table.row(closestRow), null, null, 'payment_done');


    });

    $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

        var closestRow = $(this).closest('tr');

        var data = table.row(closestRow).data();

        var id = data[1];

        var name = data[2];

        updateUserStatus(id, state, name, 'update', closestRow, table.row(closestRow), null, null, 'active');

    });



    

    $('tbody').on( 'click', '.aditional_data', function () {

        var row = table.row($(this).parents('tr'));

        var animatable = $(this).parents('tr');

        var data = table.row( $(this).parents('tr') ).data();

        var order = data[0];

        var id = data[1];

        var name = data[2];

        var email = data[3];

        var phone = data[4];

        var paymentMethod = data[5];

        var paymentDone = ($(this).attr('data-payment-done') == 0) ? 'no' : 'si';

        var city = $(this).attr('data-city');

        var address = $(this).attr('data-address');

        var medium = $(this).attr('data-medium');

        var firstTime = $(this).attr('data-first-time');

        var finalString = '<strong>Nombre: </strong>' + name + '<br>';
        
        finalString += '<strong>email: </strong>' + email + '<br>';

        finalString += '<strong>teléfono: </strong>' + phone + '<br>';

        finalString += '<strong>ciudad: </strong>' + city + '<br>';

        finalString += '<strong>dirección: </strong>' + address + '<br>';
        
        finalString += '<strong>medio por el cual llegó: </strong>' + medium + '<br>';

        finalString += '<strong>¿Primera inscripción? </strong>' + firstTime + '<br>';

        finalString += '<strong>Método de pago: </strong>' + paymentMethod + '<br>';

        finalString += '<strong>Pago efectuado: </strong>' + paymentDone + '<br>';


        //alert(city + " - " + address + " - " + medium  + ' - ' + firstTime +  " - " + paymentMethod + " - " + paymentDone);

        console.log("modal?");

        $('.modal').modal('show'); 

        $('.modal-title').text(name);

        $('.modal-body').html(finalString);


    });


    $('tbody').on( 'click', '.delete_row', function () {

        var row = table.row($(this).parents('tr'));

        var animatable = $(this).parents('tr');

        var data = table.row( $(this).parents('tr') ).data();

        var order = data[0];

        var id = data[1];

        var name = data[2];

        var actualTable = $("#tab-1").DataTable();

        Swal({

            title: "Eliminar Inscripto",

            html: 'Esta acción eliminará a <b>'+name+'</b>.<br><br>Esta acción no se puede deshacer.<br><br>¿Eliminar de todos modos?',

            type: 'error',

            showCancelButton: true,

            confirmButtonText: 'Si, eliminar',

            cancelButtonText: 'no, salir'

        }).then((result) => {

            if(result.value == true) updateUserStatus(id, 'true', name, 'delete', animatable, row, actualTable, null,  'deleted');

             //updateUserStatus(id, 'true', name, 'delete', animatable, row, actualTable, order);

         })

    });

}

/**
* @function updateOrder
* @description Update / sort the order of the items inside the list.
*
* @param {Object[]} usersToSort             - Array with objects. Each object has the info for the sort.
* @param {int} usersToSort[].id             - Object id.
* @param {int} usersToSort[].oldData        - The old position of the object.
* @param {int} usersToSort[].newData        - The new position of the object.
* @param {string} usersToSort[].name        - The name of the sorted person. 
*
* @param {string} selectedName              - The actual person.
* @param {int} setOld                       - The old position of the actual person.
* @param {int} setNew                       - The new position of the actual person.
*/

var updateOrder = function(usersToSort, selectedId, selectedName, setOld, setNew){

    helper.blockStage('Actualizando...');

    $.ajax({

        type: "POST",
        
        url: "private/users/museum/general/update_order.php",
        
        data: {array:usersToSort, table:'museum_faq', selectedId:selectedId, source:'faq'},

        success: function(result){

            var st1 = result.msg.replace('%name%', '<b>' + selectedName + '</b>');

            var st2 = st1.replace('%oldPosition%', '<b>' + setOld + '</b>');

            var st3 = st2.replace('%newPosition%', '<b>' + setNew + '</b>');
            
            helper.unblockStage();

            helper.showToastr(result.title, st3);

        },

        error: function(xhr, status, error) {

            console.log(xhr.responseText);



            var err = eval("(" + xhr.responseText + ")");

            console.log(err);

        },

        dataType: "json"
    });

}

/**
* @function updateUserStatus
*
* @description Update / Delete user status from list: we use this to mark the user as online / offline and
* for delete an user completely from the list.
*
* @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
* @param {boolean} state                    - True or false: use it for both, update and delete.
* @param {string} name                  - The name of the person being manipulated.
* @param {string} action                    - Posibilities: 'update' or 'delete'. 
* @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
* @param {DataTable} row                    - Reference to the row that we are dealing with.
* @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
* @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
*/

var updateUserStatus = function(id, state, name, action, animatable, row, processTable = null, order = null, defaultColumn = null){

    helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando opinión...');

    //var url = (action == 'update') ? "private/users/museum/general/update_status.php"  : 

    //"private/users/museum/general/delete_item.php";

    var url = "private/users/museum/general/update_status.php"; 

    $.ajax({

        type: "POST",

        url: url,

        data: {id: id, status:state, action:action, name:name, source: 'courses',

        table:'museum_courses_inscriptions', defaultColumn:defaultColumn},

        success: function(result){

            console.log(result);

            helper.unblockStage();

            if(action == 'update'){

                var newStatus = (result.changeStatus == 'true') ? 'pago' : 'inpago';

                var value = 'Cambiaste el estado del pago de ' + name + ' a ' + newStatus;

                helper.showToastr('CAMBIO EN EL ESTADO DEL PAGO', value);

            }else{

                animatable.fadeOut('slow','linear',function(){

                    console.log(result.msg);

                    helper.showToastr('ELIMINADO', 'Eliminaste a ' + name + ' del curso.');

                    var count = 0;

                    var data = processTable.rows().data();

                    processTable.rows().every( function (rowIdx, tableLoop, rowLoop) {

                        if(processTable.cell(rowIdx, 0).data() > Number(order)){

                            var saveValue = Number(processTable.cell(rowIdx, 0).data() - 1);

                            processTable.cell(rowIdx, 0).data(saveValue);

                        }

                    });

                    row.remove().draw(true);

                    processTable.rows().invalidate().draw(false);

                });

            }

        },

        error: function(xhr, status, error) {

            console.log(xhr);

            var err = eval("(" + xhr.responseText + ")");

            alert(err.Message);
        },

        dataType: "json"
    });

}

/**
* @function animateAddedElement.
* @description animate the last object when we come from editing.
*/

var animateAddedElement = function(){

    TweenMax.to(window, 1, {scrollTo:{y:"max"}, delay:1});

    var y = document.getElementsByClassName('row-1');

    var n = $(y).css("backgroundColor");

    var c = $(y).css("color");

    TweenMax.to(y, 1, {backgroundColor:"rgba(132,173,169,0.37)", color: '#1c9081', ease:Cubic.easeOut, delay:2});

    TweenMax.to(y, 1, {backgroundColor:n, color:c, ease:Cubic.easeOut, delay:.5, delay:3});

}

return {

    init: function() {

        addListeners();

        createDataTable();

        var highlight = (helper.getGETVariables(window.location, "highlight") == "true") ? true : false;
        
        if(highlight == true) animateAddedElement();

    }

};

}();

jQuery(document).ready(function() { MuseumBookingEventList.init(); });