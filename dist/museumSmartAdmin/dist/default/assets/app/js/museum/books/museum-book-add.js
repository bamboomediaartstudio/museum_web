/**
 * @summary Add new book.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
 * @function PageInit
 * @description Initialize and include all the methods of this class.
 */

 var PageInit = function() {

    helper = Helper();

    var editMode;

    var imagePath = '';

    var redirectId;

    var bookId;

    var bookTitle;

    var addRedirectId = 0;

    var defaultImageWidth = 2000;

    var defaultImageHeight = 1000;

    var form = $('#add-data-form');
    
    var youtubeForm = $('#youtube-form');

    var origin;

    var summernoteDescriptionCounter = 0;

    //cover config...

    initialPreviewCoverConfig = [];

    initialPreviewCoverPaths = [];

    //gallery config...
    
    initialPreviewGalleryConfig = [];

    initialPreviewgalleryPaths = [];

    //videos config...

    initialPreviewVideosConfig = [];

    initialPreviewVideosPaths = [];

    var openKey;

    var openVideoKey;

    var createVideosGallery = function(){

        var btns = '<button data-origin="video" type="button" class="kv-cust-video-btn btn btn-sm btn-kv btn-default btn-outline-secondary" title="Editar"{dataKey}>' +
        '<i class="fas fa-pen"></i>' + '</button>';

        $("#videos-batch").fileinput({

            otherActionButtons: btns,

            showBrowse: false,

            language: "es",

            initialPreviewAsData: true,

            initialPreview: initialPreviewVideosPaths,

            initialPreviewConfig: initialPreviewVideosConfig,  

            theme: "fas",

            uploadUrl: "private/users/museum/general/update_images_batch.php",

            deleteUrl: "private/users/museum/videos_general/delete_video.php",

            uploadExtraData: function() {

                return {

                    id: bookId,

                    source: 'book'
                };

            }

        }).on('filesorted', function(e, params) {

            rearangeStack('museum_youtube_videos', JSON.stringify(params.stack));
            
        }).on('filedeleted', function(event, key, jqXHR, data) {

            console.log(event, key, jqXHR, data);

            helper.showToastr("Video eliminado", 'Se eliminó un video del libro ' + bookTitle);

        }).on('filebeforedelete', function() {

            return new Promise(function(resolve, reject) {

                swal({

                    title: "¿Eliminar video?",

                    allowOutsideClick: false,

                    html: "El mismo no aparecerá más listado en este libro.",

                    type: "warning",

                    showCancelButton: true,

                    confirmButtonText: 'Si, borrar',

                    cancelButtonText: 'no, salir'

                }).then((result) => {

                    if (result.value) {

                        resolve();

                    }else{

                        reject();
                    }

                })

            });
        });

        $('.kv-cust-video-btn').on('click', function() {

            origin = $(this).attr('data-origin');

            var $btn = $(this);

            var key = $btn.data('key');

            openKey = key;

            var item = initialPreviewVideosConfig.find(item => item.key == key);

            $('#image-description').val(item.description);

            $('#myModal').modal('show'); 

        });

    }

    /**
     * @function createCover
     * @description Create the cover image.
     */

     var createCover = function(){

         $("#main-image").fileinput({

            initialPreview : (editMode) ? initialPreviewCoverPaths : '',

            initialPreviewConfig: initialPreviewCoverConfig,

            initialPreviewAsData: true,

            theme: "fas",

            uploadUrl: "private/users/museum/images_general/add_image.php",

            deleteUrl: "private/users/museum/general/delete_image.php",

            showCaption: true,

            showPreview: true,

            showRemove: false,

            showUpload: false,

            showCancel: false,

            showDrag: false,

            fileActionSettings: {showDrag: false, showUpload:false},

            showClose: false,

            browseOnZoneClick: (editMode) ? ((imagePath == '') ? true : false)  : true,

            showBrowse: (editMode) ? ((imagePath == '') ? true : false)  : true,

            previewFileType: 'any',

            language: "es",

            maxFileSize: 0,

            allowedFileTypes: ["image"],

            overwriteInitial: false,

            preferIconicPreview: true,

            allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],

            maxFilePreviewSize: 10240,

            minImageWidth: 2000,

            minImageHeight: 1000,

            uploadExtraData: function() {

                return {

                    id: bookId,

                    source: 'books'
                };

            }

        }).on('fileloaded', function(event, file, previewId, index, reader) {



        }).on('fileerror', function(event, data, msg) {



        }).on('fileimageresizeerror', function(event, data, msg) {

            showErrorSwal(msg);

        }).on('fileuploaderror', function(event, data, msg) {

            var form = data.form, files = data.files, extra = data.extra,

            response = data.response, reader = data.reader;

            showErrorSwal(msg);

        }).on('filedeleted', function(event, key, jqXHR, data) {

            $('#main-image').fileinput('refresh', 

                {browseLabel: 'Buscar otra...', showBrowse: true, browseOnZoneClick: true, 

                uploadAsync: false, showUpload: false, showRemove: false});

        }).on("filebatchselected", function(event, files) {

            if(editMode) $('#main-image').fileinput("upload");

        });

    }

    /**
     * @function createGallery
     * @description Create the gallery.
     */

     var createGallery = function() {

        var btns = '<button data-origin="image" type="button" class="kv-cust-btn btn btn-sm btn-kv btn-default btn-outline-secondary" title="Editar"{dataKey}>' +
        '<i class="fas fa-pen"></i>' + '</button>';

        $("#images-batch").fileinput({

            otherActionButtons: btns,

            initialPreviewAsData: true,

            initialPreview: initialPreviewgalleryPaths,

            initialPreviewConfig: initialPreviewGalleryConfig,  

            theme: "fas",

            uploadUrl: "private/users/museum/general/update_images_batch.php",

            deleteUrl: "private/users/museum/general/delete_image.php",

            showCaption: true,

            showPreview: true,

            showRemove: false,

            showUpload: true,

            showCancel: true,

            showClose: false,

            browseOnZoneClick: true,

            previewFileType: 'any',

            language: "es",

            maxFileSize: 0,

            allowedFileTypes: ["image"],

            overwriteInitial: false,

            allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],

            maxFilePreviewSize: 10240,

            uploadExtraData: function() {

                return {

                    id: bookId,

                    source: 'individual_book'

                };

            }

        }).on('filesorted', function(e, params) {

            rearangeStack('museum_images', JSON.stringify(params.stack));
            
        }).on('fileuploaded', function(e, params) {

        }).on('filebatchuploadcomplete', function(event, files, extra) {

            location.reload();

        }).on("filepredelete", function(jqXHR) {


        }).on('filedeleted', function(event, key, jqXHR, data) {

            console.log(event, key, jqXHR, data);

            helper.showToastr("Imagen eliminada", 'Se eliminó la imagen del libro ' + bookTitle);

        }).on('fileuploaderror', function(event, data, msg) {

            var form = data.form, files = data.files, extra = data.extra,

            response = data.response, reader = data.reader;

            console.log(event, data, msg);

            //showErrorSwal(msg);

        }).on('filebeforedelete', function() {

            return new Promise(function(resolve, reject) {

                swal({

                    title: "¿Eliminar imagen?",

                    allowOutsideClick: false,

                    html: "La misma no aparecerá más listada en este libro.",

                    type: "warning",

                    showCancelButton: true,

                    confirmButtonText: 'Si, borrar',

                    cancelButtonText: 'no, salir'

                }).then((result) => {

                    if (result.value) {

                        resolve();

                    }else{

                        reject();
                    }

                })

            });
        });

        $('.kv-cust-btn').on('click', function() {

            origin = $(this).attr('data-origin');

            var $btn = $(this);

            var key = $btn.data('key');

            openKey = key;

            var item = initialPreviewGalleryConfig.find(item => item.key == key);

            $('#image-description').val(item.description);

            $('#myModal').modal('show'); 

        });

    }

    /**
     * @function rearangeStack
     * @param db - db to rearange by order.
     * @param stack - the items to rearange.
     * @description Modify items order.
     */

     var rearangeStack = function(db, stack){

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/rearange_images_sort.php",

            type: "POST",

            data: {

                stack: stack,

                db: db

            },

            dataType: "json",

            success: function(result) {

                helper.showToastr(result.title, result.body);

                helper.unblockStage();

            },

            error: function(request, error) {

                console.log(request);

                helper.unblockStage();
            }
        });

    }

    /**
     * @function loadAjaxImages
     * @param loadSource - evaluate this param to get one batch or a single one.
     * @description if we are in edition mode load all the images for Bootstrap File Input.
     */

     var loadAjaxImages = function(from, loadSource, config, myPreviews) {

        console.log(loadSource + " - " + from);

        var request = $.ajax({

            url: "private/users/services/get_list_of_images.php",

            type: "POST",

            data: {

                id: bookId,

                source: loadSource,

                from: from
            },

            dataType: "json"
        });

        request.done(function(result) {

            var extension;

            if(loadSource == 'books'){

                extension = 'png';

            }else{
                
                extension = 'jpeg';

            }

            for (var i = 0; i < result.length; i++) {

                console.log(i + " : " + loadSource)

                var internalOrder = result[i].internal_order;

                var id = result[i].id;

                var source = result[i].source;

                var description = result[i].description;

                var imgName = bookId + '_' + result[i].unique_id + '_original.' + extension;

                var path;

                if(loadSource == 'books'){

                    path = 'private/sources/images/books/' + bookId + '/' + result[i].unique_id + '/' + imgName;

                    console.log("path: " + path);

                }else if(loadSource == 'individual_books'){

                    path = 'private/sources/images/books/' + bookId + '/images/' + result[i].unique_id + '/' + imgName;

                }else if(loadSource == 'book_video'){

                    path = "//img.youtube.com/vi/" + result[i].unique_id + "/0.jpg"

                }

                myPreviews.push(path);

                config.push({

                    'key': id,
                    
                    'internal_order': internalOrder,
                    
                    'source': source,

                    'description' : description,

                    'caption' : (description != null) ? description : ''

                    //'url' : 'private/users/museum/general/delete_image.php'

                });

            }

            if(loadSource == 'books'){

                createCover();

            }else if(loadSource == 'individual_book'){

                createGallery();

            }else if(loadSource == 'book_video'){

                createVideosGallery();
            }


        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

        });

    }

    /**
     * @function addFormValidations
     * @description Asign all the form validations for this page.
     */

     var addFormValidations = function() {

        jQuery.validator.setDefaults({

            ignore: ":hidden, [contenteditable='true']:not([name])"

        });

        helper.addyoutubeValidationMethod($.validator);

        youtubeForm.validate({

            rules: {

                youtube_id: {required: true, url:true, youtube:true}

            },

            messages: {

                youtube_id:{

                    required: helper.createErrorLabel('link a video', 'REQUIRED'),
                    
                    url: helper.createErrorLabel('link a video', 'INVALID_URL'),

                    youtube: helper.createErrorLabel('url', 'YOUTUBE_VIDEO')

                }

            },

            invalidHandler: function(e, r) {

                console.log("invalid");

            },

            submitHandler: function(form, event) {

                event.preventDefault();

                helper.blockStage("actualizando datos...");

                var videoId = helper.getYoutubeVideoId($('#youtube_id').val());

                var request = $.ajax({

                    url: "private/users/museum/videos_general/add_youtube_video.php",

                    type: "POST",

                    data: {

                        id: bookId,

                        youtube_id: videoId,

                        source: 'book_video'

                    },

                    dataType: "json",

                    success: function(result) {

                        console.log(result);

                        helper.showToastr(result.title, result.msg);

                        helper.unblockStage();

                        //var item = initialPreviewVideosConfig.find(item => item.key == openKey);

                        //item.caption = item.description = description;

                        initialPreviewVideosConfig = [];

                        initialPreviewVideosPaths = [];

                        $("#videos-batch").fileinput('destroy');

                        loadAjaxImages('museum_youtube_videos', 'book_video', initialPreviewVideosConfig, initialPreviewVideosPaths);


                        //createVideosGallery();

                    },

                    error: function(request, error) {

                        console.log(request);

                        helper.unblockStage();
                    }

                });

            }

        })

        form.validate({

            rules: {

                title: { required: true },

                'main-image':{ required: true }

            },

            messages: {

                title: helper.createErrorLabel('título', 'REQUIRED'),

                'main-image': helper.createErrorLabel('imagen', 'REQUIRED')

            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();

                window.scroll({top: 0, left: 0, behavior: 'smooth' });
            },

            submitHandler: function(form, event) {

                event.preventDefault();

                event.stopPropagation();

                helper.blockStage("actualizando datos...");

                var formData = new FormData(form);

                formData.append("id", bookId);

                formData.append("bookTitle", bookTitle);

                formData.append("source", "books");

                formData.append("desciption", $('#desciption').summernote('code'));

                formData.append("url", $('#url').val());

                var request = $.ajax({

                    url: "private/users/museum/books/book_add.php",

                    type: "POST",

                    contentType: false,
                    
                    processData: false,

                    data: formData,

                    dataType: "json"
                });

                request.done(function(response) {

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch (Number(response.status)) {

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {

                            switch (Number(response.status)) {

                                case 1:

                                window.location.replace("museum_books_list.php");

                                break;

                            }

                        }

                    });


                    helper.showToastr("Se acualizo", 'Se actualizo');

                    helper.unblockStage();

                });

                request.fail(function(jqXHR, textStatus) {

                    console.log(jqXHR);

                    console.log("error");

                    helper.showToastr("ops", 'error');

                    helper.unblockStage();

                });

            }

        });
    }


    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

     var addListeners = function() {

        $('.tab-navigation').click(function(){ 

            setQueryStringParameter('tab', $(this).attr('href').substring(1)); 

        })

        $('#description').summernote({

           /*onPaste: function (e) {

            var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
            
            e.preventDefault();
            
            document.execCommand('insertText', false, bufferText);

        },*/

        fontNames : ['CooperHewitt-Medium', 'Arial', 'sans-serif'],

        disableDragAndDrop: true,

        height: 200,

        toolbar: [

        ['style', ['bold', 'italic', 'underline']],

        ['Misc', ['fullscreen', 'undo', 'redo']],

        ['link', ['linkDialogShow', 'unlink']]

        ],

        callbacks: {

            onInit: function() { },

            onEnter: function() { },

            onKeyup: function(e) {

                /*if($(this).attr('name') == 'content'){

                    summernoteDescriptionCounter = e.currentTarget.innerText;

                    if(summernoteDescriptionCounter !=0){

                        $('.summernote-description-error').addClass('d-none');

                    }else{

                        $('.summernote-description-error').removeClass('d-none');

                    }

                }*/

            },

            onKeydown: function(e) { },

            onPaste: function(e) { },

            onChange: function(contents, $editable) { },

            onBlur: function(){ },

            onFocus: function() { }

        }

    });

        $('.save-description').click(function(event){

            console.log("origin: " + origin);

            var description = $('#image-description').val();

            $('#myModal').modal('hide'); 

            var findArray;

            var fileInput;

            var table;

            if(origin == 'image'){

                findArray = initialPreviewGalleryConfig;

                fileInput = $("#images-batch");

                table = 'museum_images';

            }else if(origin == 'video'){

                findArray = initialPreviewVideosConfig;

                fileInput = $("#videos-batch");

                table = 'museum_youtube_videos';

            }

            var item = findArray.find(item => item.key == openKey);

            item.caption = item.description = description;   

            fileInput.fileinput('destroy');

            (origin == 'image') ? createGallery() : createVideosGallery();

            updateValue(openKey, 'description', description, table);

        })

        
        $('#title').on('input', function(e) {

            if (editMode) return;

            $('#validate-title').addClass('btn-info');

            $('#validate-title').removeClass('btn-success');

            $('#validate-title').text('validar');

            if ($(this).val() == "") {

                $('#validate-title').attr('disabled', true);

            } else {

                $('#validate-title').attr('disabled', false);

            }

        });

        $('#title').on('blur', function() {

            if (editMode) return;

            if ($(this).attr('id') == 'title' && $(this).val() != "") {

                validateTitle($('#title').val(), $('#url').val());

            }

        });

        $('#validate-title').click(function(event) {

            if ($('#title').val() != "") validateTitle($('#title').val(), $('#url').val())

        })

        $('[data-toggle="tooltip"]').tooltip();

        /*autosize for the textareas */

        autosize(document.querySelector('textarea'));

        /*permalink approach...*/

        $('#title').on('input', function() {

            if (editMode) return;

            var permalink;

            permalink = $.trim($(this).val());

            permalink = permalink.replace(/\s+/g, ' ');

            $('#url').val(permalink.toLowerCase());

            $('#url').val($('#url').val().replace(/\W/g, ' '));

            $('#url').val($.trim($('#url').val()));

            $('#url').val($('#url').val().replace(/\s+/g, '-'));

        });

        
        $('#exit_from_form').click(function(e) {
            window.location.replace("index.php");
        });

        $('#back_to_list').click(function(e) {
            window.location.replace("museum_books_list.php?tabId=" + redirectId);
        });

        $(".update-btn").click(function() {

            if (!editMode) return;

            var dbName = $(this).attr('data-db-value');

            var id = $(this).attr('data-id');

            var newValue = $(this).parent().parent().find(':input').val();

            var fieldName = $(this).parent().parent().find(':input').attr('name');

            var valid = $('#' + fieldName).valid();

            if (dbName != newValue && valid) {

                $(this).attr('data-db-value', newValue);

                updateValue(id, fieldName, newValue, 'museum_books');

            }

        });

        $('.toggler-info:checkbox').change(function() {

            if (!editMode) return;

            var fieldName = $(this).attr('name');

            var newValue = ($(this).is(":checked")) ? 1 : 0;

            updateValue(bookId, fieldName, newValue, 'museum_books')

        });

    }

    /**
     * @function showErrorSwal
     * @param msg - the msg.
     * @description Swal alert.
     */

     var showErrorSwal = function(msg){

        Swal({

            title: 'Opss!.',

            html: msg,

            type: 'error',

            confirmButtonText: 'Entendido!'

        })
    }

    /**
     * @function window.onbeforeunload
     * @description scroll to top
     */

     window.onbeforeunload = function () { if(!editMode) window.scrollTo(0,0); }

    /**
     * @function validateTitle
     * @description Send the URL and the permalink to validate.
     *
     * @param {String} name                 - The name of the new item.
     * @param {String} url                  - The url of the new item.
     */

     var validateTitle = function(name, url) {

        helper.blockStage("validando nombre...");

        var request = $.ajax({

            url: "private/users/services/validate_field.php",

            type: "POST",

            data: {

                name: name,

                url: url,

                table: 'museum_books'

            },

            dataType: "json"
        });

        request.done(function(result) {

            if (result.valid) {

                helper.showToastr("VALIDO! :)", "El nombre <b>" + name + "</b> está disponible para su uso.");

                $('#validate-title').removeClass('btn-info');

                $('#validate-title').addClass('btn-success');

                $('#validate-title').text('valido!');

            } else {

                $('#validate-title').addClass('btn-info');

                $('#validate-title').text('validar');

                Swal({

                    title: 'Ya existe un libro con ese título.',

                    html: 'Ya existe un link creado con la URL <b>' + url + '</b><br>No pueden haber dos libros con el mismo nombre.',

                    type: 'error',

                    confirmButtonText: 'Entendido!'

                }).then((result) => {

                    $("#title").val('');

                    $("#title").focus();

                });

            }

            helper.unblockStage();

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

            helper.unblockStage();

        });

    }

    /**
     * @function updateValue
     * @description update individual value in DB.
     *
     * @param {int} id                 - object id.
     * @param {string} fieldName       - the field to modify
     * @param {string} newValue        - object value.
     * @param {int} filter             - filter type.
     */

     var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/update_value.php",

            type: "POST",

            data: {

                id: id,
                
                fieldName: fieldName,
                
                newValue: newValue,
                
                filter: filter
            },

            dataType: "json"

        });

        request.done(function(result) {

            console.log(result);

            helper.showToastr(result.title, result.msg);

            if (callback != undefined) callback(callbackParams);

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });

        if (callback == undefined) helper.unblockStage();

        localStorage.setItem('refresh', 'true');

    }

    function setQueryStringParameter(name, value) {

        const params = new URLSearchParams(location.search);

        params.set(name, value);

        window.history.replaceState({}, "", decodeURIComponent(`${location.pathname}?${params}`));

    }


    return {

        init: function() {

            editMode = ($('#edit-mode').val() == 1) ? true : false;

            if (editMode) {

                var temp;

                if (document.referrer != "") {

                    temp = helper.getGETVariables(document.referrer, 'tabId');

                } else {

                    temp = null;
                }

                redirectId = (temp == null) ? 1 : temp;

                imagePath = $('#image-path').val();

                bookId = $('#book-id').val();

                bookTitle = $('#book-name').val();

            }

            addListeners();

            addFormValidations();

            if(editMode){

                loadAjaxImages('museum_images', 'books', initialPreviewCoverConfig, initialPreviewCoverPaths)

                loadAjaxImages('museum_images', 'individual_book', initialPreviewGalleryConfig, initialPreviewgalleryPaths);

                loadAjaxImages('museum_youtube_videos', 'book_video', initialPreviewVideosConfig, initialPreviewVideosPaths);

            }else{

                createCover();

                createGallery();

                createVideosGallery();
            }

            helper.setMenu();

        }

    };

}();

jQuery(document).ready(function() { PageInit.init(); });