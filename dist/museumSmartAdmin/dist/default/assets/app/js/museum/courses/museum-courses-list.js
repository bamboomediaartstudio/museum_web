/**
 * @summary Courses list.
 *
 * @description -
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

   /**
    * @function CoursesList
    * @description Initialize and include all the methods of this class.
    */

    var CoursesList = function() {

        helper = new Helper();

        //var host = window.location.host;


         /**

         * @function addListeners
         * @description Asign all the listeners / event habdlers for this page.
         */

         var addListeners = function(){

            $("[name='my-checkbox']").bootstrapSwitch();

            $("[name='my-enrollment-checkbox']").bootstrapSwitch();

            $(window).focus(function() {

                var refresh = localStorage.getItem('refresh');

                if(refresh == 'true'){

                    localStorage.removeItem('refresh');

                    location.reload();

                }

            });

            $(window).blur(function() { localStorage.removeItem('refresh'); });

        }


    /**
    * @function createDataTable
    * @description Create all the datatables that we use as lists.
    */

    var createDataTable = function(){

        var table = $('table.display').DataTable({

            pageLength: 50,

            "language": helper.getDataTableLanguageConfig(),

            //rowReorder: false,

            //rowReorder: { update: true },

            "ordering": false,

            bAutoWidth: false,

            processing:true,

            "columns": [

            { "width": "5%", responsivePriority: 4, orderable: true, targets: 0},

            { "visible": false, orderable:false },

            { "width": "20%", responsivePriority: 0, orderable: false, targets: '_all' },

            { "width": "5%", responsivePriority: 2, orderable: false, targets: '_all', },

            { "width": "5%", responsivePriority: 2, orderable: false, targets: '_all', },

            { "width": "5%", responsivePriority: 3, orderable: false, targets: '_all' }
            ]

        });

        $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-enrollment-checkbox"]', function(event, state) {

            //console.log("enrollment");

            var closestRow = $(this).closest('tr');

            var data = table.row(closestRow).data();

            var id = data[1];

            var name = data[2];

            updateUserStatus(id, state, name, 'update', closestRow, table.row(closestRow), null, null, 'allow_enrollment');


        });

        $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

            var closestRow = $(this).closest('tr');

            var data = table.row(closestRow).data();

            var id = data[1];

            var name = data[2];

            updateUserStatus(id, state, name, 'update', closestRow, table.row(closestRow), null, null, 'active');

        });

        $('tbody').on( 'click', '.edit_row', function () {

            var id = table.row( $(this).parents('tr') ).data()[1];

            window.location.href = "museum_course_add.php?id=" + id;

        });

        $('tbody').on( 'click', '.go-to-inscription-list', function () {

            var closestRow = $(this).closest('tr');

            var data = table.row(closestRow).data();

            var id = data[1];

            window.open('museum_course_inscriptions_list.php?id=' + id, '_self');

        });





        $('tbody').on( 'click', '.open-link', function () {

            var host = 'http://' + window.location.host + '/cursos/';

            var attr = $(this).attr('data-url');

            var url = host + attr;

            window.open(url, '_blank');

            console.log(url);

        });


        $('tbody').on( 'click', '.delete_row', function () {

            var row = table.row($(this).parents('tr'));

            var animatable = $(this).parents('tr');

            var data = table.row( $(this).parents('tr') ).data();

            var order = data[0];

            var id = data[1];

            var course = data[2];

            var actualTable = $("#tab-1").DataTable();

            Swal({

                title: "Eliminar Curso",

                html: 'Esta acción eliminará el curso <b>'+course+'</b>.<br><br>Esta acción no se puede deshacer. <br><br>Recordá que también podés usar botón de <b>online</b> y <b>offline</b> para modificar la visibilidad la misma.<br><br>¿Eliminar de todos modos?',

                type: 'error',

                showCancelButton: true,

                confirmButtonText: 'Si, eliminar',

                cancelButtonText: 'no, salir'

            }).then((result) => {

                if(result.value == true) updateUserStatus(id, 'true', name, 'delete', animatable, row, actualTable, null,  'deleted');

                 //updateUserStatus(id, 'true', name, 'delete', animatable, row, actualTable, order);

            })

        });

    }

    /**
    * @function updateOrder
    * @description Update / sort the order of the items inside the list.
    *
    * @param {Object[]} usersToSort             - Array with objects. Each object has the info for the sort.
    * @param {int} usersToSort[].id             - Object id.
    * @param {int} usersToSort[].oldData        - The old position of the object.
    * @param {int} usersToSort[].newData        - The new position of the object.
    * @param {string} usersToSort[].name        - The name of the sorted person.
    *
    * @param {string} selectedName              - The actual person.
    * @param {int} setOld                       - The old position of the actual person.
    * @param {int} setNew                       - The new position of the actual person.
    */

    var updateOrder = function(usersToSort, selectedId, selectedName, setOld, setNew){

        helper.blockStage('Actualizando...');

        $.ajax({

            type: "POST",

            url: "private/users/museum/general/update_order.php",

            data: {array:usersToSort, table:'museum_faq', selectedId:selectedId, source:'faq'},

            success: function(result){

                var st1 = result.msg.replace('%name%', '<b>' + selectedName + '</b>');

                var st2 = st1.replace('%oldPosition%', '<b>' + setOld + '</b>');

                var st3 = st2.replace('%newPosition%', '<b>' + setNew + '</b>');

                helper.unblockStage();

                helper.showToastr(result.title, st3);

            },

            error: function(xhr, status, error) {

                console.log(xhr.responseText);



                var err = eval("(" + xhr.responseText + ")");

                console.log(err);

            },

            dataType: "json"
        });

    }

    /**
    * @function updateUserStatus
    *
    * @description Update / Delete user status from list: we use this to mark the user as online / offline and
    * for delete an user completely from the list.
    *
    * @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
    * @param {boolean} state                    - True or false: use it for both, update and delete.
    * @param {string} name                  - The name of the person being manipulated.
    * @param {string} action                    - Posibilities: 'update' or 'delete'.
    * @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
    * @param {DataTable} row                    - Reference to the row that we are dealing with.
    * @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
    * @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
    */

    var updateUserStatus = function(id, state, name, action, animatable, row, processTable = null, order = null, defaultColumn = null){

        helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando opinión...');

        //var url = (action == 'update') ? "private/users/museum/general/update_status.php"  :

        //"private/users/museum/general/delete_item.php";

        var url = "private/users/museum/general/update_status.php";

        $.ajax({

            type: "POST",

            url: url,

            data: {id: id, status:state, action:action, name:name, source: 'courses',

            table:'museum_courses', defaultColumn:defaultColumn},

            success: function(result){

                console.log(result);

                helper.unblockStage();

                if(action == 'update'){

                    console.log(result.msg);

                    var newStatus = (result.changeStatus == 'true') ? 'online' : 'offline';

                    var previousStatus = (newStatus == 'online') ? 'offline' : 'online';

                    var st1 = result.msg.replace('%name%', '<b>' + name + '</b>');

                    var st2 = st1.replace('%newStatus%', '<b>' + newStatus + '</b>');

                    var st3 = st2.replace('%previousStatus%', '<b>' + previousStatus + '</b>');

                    console.log(st3);

                    helper.showToastr(result.title, st3);

                }else{

                    animatable.fadeOut('slow','linear',function(){

                        console.log(result.msg);

                        helper.showToastr('ELIMINADO', 'El curso se eliminó exitosamentne.');

                        var count = 0;

                        var data = processTable.rows().data();

                        processTable.rows().every( function (rowIdx, tableLoop, rowLoop) {

                            if(processTable.cell(rowIdx, 0).data() > Number(order)){

                                var saveValue = Number(processTable.cell(rowIdx, 0).data() - 1);

                                processTable.cell(rowIdx, 0).data(saveValue);

                            }

                        });

                        row.remove().draw(true);

                        processTable.rows().invalidate().draw(false);

                    });

                }

            },

            error: function(xhr, status, error) {

                console.log(xhr);

                var err = eval("(" + xhr.responseText + ")");

                alert(err.Message);
            },

            dataType: "json"
        });

    }

    /**
    * @function animateAddedElement.
    * @description animate the last object when we come from editing.
    */

    var animateAddedElement = function(){

        TweenMax.to(window, 1, {scrollTo:{y:"max"}, delay:1});

        var y = document.getElementsByClassName('row-1');

        var n = $(y).css("backgroundColor");

        var c = $(y).css("color");

        TweenMax.to(y, 1, {backgroundColor:"rgba(132,173,169,0.37)", color: '#1c9081', ease:Cubic.easeOut, delay:2});

        TweenMax.to(y, 1, {backgroundColor:n, color:c, ease:Cubic.easeOut, delay:.5, delay:3});

    }

    return {

        init: function() {

            helper.setMenu();

            addListeners();

            createDataTable();

            var highlight = (helper.getGETVariables(window.location, "highlight") == "true") ? true : false;

            if(highlight == true) animateAddedElement();

        }

    };

}();

jQuery(document).ready(function() { CoursesList.init(); });
