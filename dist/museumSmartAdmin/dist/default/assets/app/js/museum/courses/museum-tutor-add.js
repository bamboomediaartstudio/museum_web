/**
 * @summary Add new tutor.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */


/**
 * @function MuseumTutorAdd
 * @description Initialize and include all the methods of this class.
 */

 var MuseumTutorAdd = function() {

    helper = Helper();

    var editMode;

    var imagePath = '';

    var tutorId;

    var tutorName;

    //cover config...

    initialPreviewCoverConfig = [];

    initialPreviewCoverPaths = [];

    var form = $('#add-data-form');
    
    /**
     * @function createCover
     * @description Create the cover image.
     */

     var createCover = function(){

         $("#main-image").fileinput({

            initialPreview : (editMode) ? initialPreviewCoverPaths : '',

            initialPreviewConfig: initialPreviewCoverConfig,

            initialPreviewAsData: true,

            theme: "fas",

            uploadUrl: "private/users/museum/images_general/add_image.php",

            deleteUrl: "private/users/museum/general/delete_image.php",

            showCaption: true,

            showPreview: true,

            showRemove: false,

            showUpload: false,

            showCancel: false,

            showDrag: false,

            fileActionSettings: {showDrag: false, showUpload:false},

            showClose: false,

            browseOnZoneClick: (editMode) ? ((imagePath == '') ? true : false)  : true,

            showBrowse: (editMode) ? ((imagePath == '') ? true : false)  : true,

            previewFileType: 'any',

            language: "es",

            maxFileSize: 0,

            allowedFileTypes: ["image"],

            overwriteInitial: false,

            preferIconicPreview: true,

            allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],

            maxFilePreviewSize: 10240,

            minImageWidth: 500,

            minImageHeight: 500,

            uploadExtraData: function() {

                return {

                    id: tutorId,

                    source: 'tutors'
                };

            }

        }).on('fileloaded', function(event, file, previewId, index, reader) {


        }).on('fileerror', function(event, data, msg) {


        }).on('fileimageresizeerror', function(event, data, msg) {

            showErrorSwal(msg);

        }).on('fileuploaderror', function(event, data, msg) {

            var form = data.form, files = data.files, extra = data.extra,

            response = data.response, reader = data.reader;

            showErrorSwal(msg);

        }).on('filedeleted', function(event, key, jqXHR, data) {

            $('#main-image').fileinput('refresh', 

                {browseLabel: 'Buscar otra...', showBrowse: true, browseOnZoneClick: true, 

                uploadAsync: false, showUpload: false, showRemove: false});

        }).on("filebatchselected", function(event, files) {

            if(editMode) $('#main-image').fileinput("upload");

        });

    }

    
    /**
     * @function showErrorSwal
     * @param msg - the msg.
     * @description Swal alert.
     */

     var showErrorSwal = function(msg){

        Swal({

            title: 'Opss!.',

            html: msg,

            type: 'error',

            confirmButtonText: 'Entendido!'

        })
    }

    /**
     * @function loadAjaxImages
     * @param loadSource - evaluate this param to get one batch or a single one.
     * @description if we are in edition mode load all the images for Bootstrap File Input.
     */

     var loadAjaxImages = function(from, loadSource, config, myPreviews) {

        var request = $.ajax({

            url: "private/users/services/get_list_of_images.php",

            type: "POST",

            data: {

                id: tutorId,

                source: loadSource,

                from: from
            },

            dataType: "json"
        });

        request.done(function(result) {

            for (var i = 0; i < result.length; i++) {

                var internalOrder = result[i].internal_order;

                var id = result[i].id;

                var source = result[i].source;

                var description = result[i].description;

                var imgName = tutorId + '_' + result[i].unique_id + '_original.jpeg';

                var path;

                if(loadSource == 'tutors'){

                    path = 'private/sources/images/tutors/' + tutorId + '/' + result[i].unique_id + '/' + imgName;

                }

                myPreviews.push(path);

                config.push({

                    'key': id,
                    
                    'internal_order': internalOrder,
                    
                    'source': source,

                    'description' : description,

                    'caption' : (description != null) ? description : ''

                });

            }

            createCover();

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

        });

    }

    /**
     * @function addFormValidations
     * @description Asign all the form validations for this page.
     */

     var addFormValidations = function() {

        jQuery.validator.setDefaults({

            ignore: ":hidden, [contenteditable='true']:not([name])"

        });

        helper.addyoutubeValidationMethod($.validator);

        form.validate({

            rules: {

                name: {
                    required: true
                },

                surname: {
                    required: true
                },

                email: {
                    email: true
                }

            },
            messages: {

                name: helper.createErrorLabel('nombre', 'REQUIRED'),

                surname: helper.createErrorLabel('apellido', 'REQUIRED'),

                email: helper.createErrorLabel('email', 'EMAIL')
            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();

                
                window.scroll({top: 0, left: 0, behavior: 'smooth' });
            },

            submitHandler: function(form, event) {

                event.preventDefault();

                event.stopPropagation();

                helper.blockStage("actualizando datos...");

                var formData = new FormData(form);

                formData.append("id", tutorId);

                formData.append("tutorName", tutorName);

                formData.append("source", "tutors");

                formData.append("bio", $('#bio').summernote('code'));

                //formData.append("url", $('#url').val());

                var request = $.ajax({

                    url: "private/users/museum/courses/tutor_add.php",

                    type: "POST",

                    contentType: false,
                    
                    processData: false,

                    data: formData,

                    dataType: "json"
                });

                request.done(function(response) {

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch (Number(response.status)) {

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {

                            switch (Number(response.status)) {

                                case 1:

                                window.location.replace("museum_tutors_list.php");

                                break;

                            }

                        }

                    });


                    helper.showToastr("Se acualizo", 'Se actualizo');

                    helper.unblockStage();

                });

                request.fail(function(jqXHR, textStatus) {

                    console.log(jqXHR);

                    console.log("error");

                    helper.showToastr("ops", 'error');

                    helper.unblockStage();

                });

            }

        });

    }

    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

     var addListeners = function() {

        $('#bio').summernote({

            disableDragAndDrop: true,

            height: 200,

            toolbar: [

            ['style', ['bold', 'italic', 'underline']],

            ['Misc', ['fullscreen', 'undo', 'redo']]

            ],

            callbacks: {

                onInit: function() { },

                onEnter: function() { },

                onKeyup: function(e) { },

                onKeydown: function(e) { },

                onPaste: function(e) { },

                onChange: function(contents, $editable) { },

                onBlur: function(){ },

                onFocus: function() { }

            }

        });

         $('[data-toggle="tooltip"]').tooltip();

         $('.show_address').change(function() {

            var checked = ($(this).prop('checked'));

            if (checked == true) {

                $('.museum_map_block, .museum_map_address_block, #map').removeClass('disabled');

            } else {

                $('.museum_map_block, .museum_map_address_block, #map').addClass('disabled');
            }

        })

         /*autosize for the textareas */

         autosize(document.querySelector('textarea'));

         $('#exit_from_form').click(function(e) {  window.location.replace("index.php"); });

         $('#back_to_list').click(function(e) { window.location.replace("museum_tutors_list.php"); });

         $(".update-btn").click(function() {

            if (!editMode) return;

            var dbName = $(this).attr('data-db-value');

            var id = $(this).attr('data-id');

            var newValue = $(this).parent().parent().find(':input').val();

            var fieldName = $(this).parent().parent().find(':input').attr('name');

            var valid = $('#' + fieldName).valid();

            if (dbName != newValue && valid) {

                $(this).attr('data-db-value', newValue);

                updateValue(id, fieldName, newValue, 'museum_courses_tutors');

            }

        });


     }

    /**
     * @function window.onbeforeunload
     * @description scroll to top
     */

     window.onbeforeunload = function () { if(!editMode) window.scrollTo(0,0); }

    /**
     * @function updateValue
     * @description update individual value in DB.
     *
     * @param {int} id                 - object id.
     * @param {string} fieldName       - the field to modify
     * @param {string} newValue        - object value.
     * @param {int} filter             - filter type.
     */

     var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/update_value.php",

            type: "POST",

            data: {

                id: id,
                
                fieldName: fieldName,
                
                newValue: newValue,
                
                filter: filter
            },

            dataType: "json"

        });

        request.done(function(result) {

            console.log(result);

            helper.showToastr(result.title, result.msg);

            if (callback != undefined) callback(callbackParams);

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });

        if (callback == undefined) helper.unblockStage();

        localStorage.setItem('refresh', 'true');

    }


    return {

        init: function() {

            editMode = ($('#edit-mode').val() == 1) ? true : false;

            if (editMode) {

                imagePath = $('#image-path').val();

                tutorId = $('#tutor-id').val();

                tutorName = $('#tutor-name').val();

            }

            addListeners();

            addFormValidations();

            if(editMode){

                loadAjaxImages('museum_images', 'tutors', initialPreviewCoverConfig, initialPreviewCoverPaths);

            }else{

                createCover();
            }
        }

    };

}();

jQuery(document).ready(function() { MuseumTutorAdd.init(); });