/**
 * @summary Events list.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */
 
   /**
    * @function EventsList
    * @description Initialize and include all the methods of this class.
    */

    var EventsList = function() {

        helper = new Helper();


         /**

         * @function addListeners
         * @description Asign all the listeners / event habdlers for this page.
         */

         var addListeners = function(){

            $("[name='my-checkbox']").bootstrapSwitch();

        }


    /**
    * @function createDataTable
    * @description Create all the datatables that we use as lists.
    */

    var createDataTable = function(){

        var table = $('table.display').DataTable({

            pageLength: 50,

            "language": helper.getDataTableLanguageConfig(),

            //rowReorder: false,

            //rowReorder: { update: true },

            "ordering": false,

            bAutoWidth: false, 

            processing:true,

            "columns": [

            { "width": "5", responsivePriority: 1, orderable: true, targets: 0},

            { "width": "30%", responsivePriority: 0, orderable: false, targets: '_all' }, 

            { "width": "30%", responsivePriority: 3, orderable: false, targets: '_all' }, 

            { "width": "5%", responsivePriority: 2, orderable: false, targets: '_all', }, 

            { "width": "5%", responsivePriority: 1, orderable: false, targets: '_all' }
            ]

        });


        $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

            var closestRow = $(this).closest('tr');

            var data = table.row(closestRow).data();

            var id = data[0];

            var name = data[1];

            //console.log(data[1]);

           // console.log("outa");

            //return;

            updateUserStatus(id, state, name, 'update', closestRow, table.row(closestRow), null, null, 'active');

        });

        /*$('tbody').on( 'click', '.pomex', function () {

            var id = table.row( $(this).parents('tr') ).data()[1];

            window.location.href = "museum_event_add.php?id=" + id;

        });*/

        $('tbody').on( 'click', '.edit_row', function () {

            var attr = $(this).attr('data-id');

            window.location.href = "museum_event_add.php?id=" + attr;

        });

        $('tbody').on( 'click', '.add_images', function () {

            var id = $(this).attr('data-id');

            window.location.href = "museum_event_add.php?id=" + id + '&tab=images-tab';

        });

        $('tbody').on( 'click', '.add_videos', function () {

            var id = $(this).attr('data-id');

            window.location.href = "museum_event_add.php?id=" + id + '&tab=videos-tab';

        });

        $('tbody').on( 'click', '.open-link', function () {

            var host = 'http://' + window.location.host + '/eventos/';

            var attr = $(this).attr('data-url');

            var url = host + attr;

            window.open(url, '_blank'); 

            console.log(url);

        });

        $('tbody').on( 'click', '.go-to-inscription-list', function () {

            var closestRow = $(this).closest('tr');

            var data = table.row(closestRow).data();

            var id = data[0];

            window.open('museum_event_inscriptions_list.php?id=' + id, '_self');

        });

        

        $('.modal').on('hidden.bs.modal', function () {

            $( "#qrcode").empty();

        });

        $('.copy-to-clipboard').click(function(){

            var copyText = document.getElementById("qr-generated-url");

            copyText.select();

            document.execCommand("copy");

            $('.modal').modal('hide');

            helper.showToastr('COPY TO CLIPBOARD!', 'El texto se copió al portapapeles.');



        })


        $('tbody').on( 'click', '.qr_generator', function () {

            $('.modal').modal('show');

            var dataUrl = $(this).attr('data-url');

            $('.modal-title').html($(this).attr('data-name'));

            var url = 'https://www.museodelholocausto.org.ar/eventos/' + dataUrl;

            $("#qr-generated-url").val(url);

            $('#qrcode').qrcode(url);

        });

        $('tbody').on( 'click', '.delete_row', function () {

            


            var row = table.row($(this).parents('tr'));

            var animatable = $(this).parents('tr');

            var id = $(this).attr('data-id');

            var name = $(this).attr('data-name');

            var actualTable = $("#tab-1").DataTable();

            Swal({

                title: "Eliminar Muestra",

                html: 'Esta acción eliminará el evento <b>'+name+'</b>.<br><br>Esta acción no se puede deshacer. <br><br>Recordá que también podés usar botón de <b>online</b> y <b>offline</b> para modificar la visibilidad del mismo.<br><br>¿Eliminar de todos modos?',

                type: 'error',

                showCancelButton: true,

                confirmButtonText: 'Si, eliminar',

                cancelButtonText: 'no, salir'

            }).then((result) => {

                if(result.value == true) updateUserStatus(id, 'true', name, 'delete', animatable, row, actualTable, null,  'deleted');

            })

        });

    }

    /**
    * @function updateUserStatus
    *
    * @description Update / Delete user status from list: we use this to mark the user as online / offline and
    * for delete an user completely from the list.
    *
    * @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
    * @param {boolean} state                    - True or false: use it for both, update and delete.
    * @param {string} name                  - The name of the person being manipulated.
    * @param {string} action                    - Posibilities: 'update' or 'delete'. 
    * @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
    * @param {DataTable} row                    - Reference to the row that we are dealing with.
    * @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
    * @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
    */

    var updateUserStatus = function(id, state, name, action, animatable, row, processTable = null, order = null, defaultColumn = null){

        console.log(id, state, name, action, animatable);

        helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando opinión...');

        var url = "private/users/museum/general/update_status.php"; 

        $.ajax({

            type: "POST",

            url: url,

            data: {id: id, status:state, action:action, name:name, source: 'events',

            table:'museum_events', defaultColumn:defaultColumn},

            success: function(result){

                console.log(result);

                helper.unblockStage();

                if(action == 'update'){

                    console.log(result.msg);

                    var newStatus = (result.changeStatus == 'true') ? 'online' : 'offline';

                    var previousStatus = (newStatus == 'online') ? 'offline' : 'online';

                    var st1 = result.msg.replace('%name%', '<b>' + name + '</b>');

                    var st2 = st1.replace('%newStatus%', '<b>' + newStatus + '</b>');

                    var st3 = st2.replace('%previousStatus%', '<b>' + previousStatus + '</b>');

                    console.log(st3);

                    helper.showToastr(result.title, st3);

                }else{

                    animatable.fadeOut('slow','linear',function(){

                        console.log(result.msg);

                        helper.showToastr('ELIMINADO', 'El curso se eliminó exitosamentne.');

                        var count = 0;

                        var data = processTable.rows().data();

                        processTable.rows().every( function (rowIdx, tableLoop, rowLoop) {

                            if(processTable.cell(rowIdx, 0).data() > Number(order)){

                                var saveValue = Number(processTable.cell(rowIdx, 0).data() - 1);

                                processTable.cell(rowIdx, 0).data(saveValue);

                            }

                        });

                        row.remove().draw(true);

                        processTable.rows().invalidate().draw(false);

                    });

                }

            },

            error: function(xhr, status, error) {

                console.log(xhr);

                var err = eval("(" + xhr.responseText + ")");

                alert(err.Message);
            },

            dataType: "json"
        });

    }


    return {

        init: function() {

            addListeners();

            createDataTable();
            
        }

    };

}();

jQuery(document).ready(function() { EventsList.init(); });