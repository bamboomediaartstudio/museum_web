var GeneralInfo = function() {

    var profile = $('#profile');

    var isInMapEditionMode = false;

    helper = Helper();

    var handleGeneralUpdateSubmit = function() {

        $('#update_general_info').click(function(e) {

            e.preventDefault();

            var btn = $(this);

            var form = $(this).closest('form');

            form.validate({

                rules: {

                    te: {required: true},

                    email: {required: true, email: true },

                    email_2: {email: true },

                    email_3: {email: true },

                    address: {required: true},

                    address_number: {required: true, number:true},

                    zip_code: {required: true},

                    city: {required: true}

                },
                messages: {

                    te: helper.createErrorLabel('teléfono', 'REQUIRED'),

                    email: helper.createErrorLabel('email', 'REQUIRED'),

                    email_2: helper.createErrorLabel('email', 'EMAIL'),

                    email_3: helper.createErrorLabel('email', 'EMAIL'),

                    address: helper.createErrorLabel('dirección', 'REQUIRED'),

                    address_number: {

                        required: helper.createErrorLabel('número', 'REQUIRED'),

                        number: helper.createErrorLabel('número', 'NUMERIC')

                    },

                    zip_code: helper.createErrorLabel('código opostal', 'REQUIRED'),

                    city: helper.createErrorLabel('ciudad', 'REQUIRED')

                }

            });


            if (!form.valid()) { return; }

            updateGeneralInfo(btn, form);

        });

    }

    var updateGeneralInfo = function(btn, form){


        btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

        form.ajaxSubmit({

            cache: false,

            url: 'private/users/museum/general.php',

            type: 'post',

            dataType: 'json',

            success: function(response, status, xhr, $form) {

                helper.unblockStage();

                swal({

                    title: response.title,

                    html: response.msg,

                    type: response.alert,

                    confirmButtonText: 'Genial :)'

                }).then((result) => {

                    switch(Number(response.status)){

                        case 0:

                        location.reload();

                        break;

                        case 1:

                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                        break;

                    }

                });

            },


            error: function(response, status, xhr, $form){

                console.log("error");

                console.log(response);

            },

            beforeSubmit: function(){ helper.blockStage("actualizando datos..."); }

        });
    }

    var handleSocialMediaUpdateSubmit = function(){

       $('#update_social_media').click(function(e) {

        e.preventDefault();

        var btn = $(this);

        var form = $(this).closest('form');

        form.validate({

            rules: {

                facebook: { url: true },

                linkedin: { url: true },

                twitter: { url: true },

                instagram: { url: true },

                youtube: { url: true },

                vimeo: { url: true },

                flickr: { url: true }

            },
            messages: {

                facebook: helper.createErrorLabel('facebook', 'INVALID_URL'),

                twitter: helper.createErrorLabel('twitter', 'INVALID_URL'),

                instagram: helper.createErrorLabel('instagram', 'INVALID_URL'),

                linkedin: helper.createErrorLabel('linkedin', 'INVALID_URL'),

                youtube: helper.createErrorLabel('youtube', 'INVALID_URL'),

                vimeo: helper.createErrorLabel('vimeo', 'INVALID_URL'),

                flickr: helper.createErrorLabel('flickr', 'INVALID_URL')

            }
        });

        if (!form.valid()) {return; }

        form.ajaxSubmit({

            cache: false,

            url: 'private/users/museum/social_media.php',

            type: 'post',

            dataType: 'json',

            success: function(response, status, xhr, $form) {

                console.log(response);

                helper.unblockStage();

                swal({

                    title: response.title,

                    html: response.msg,

                    type: response.alert,

                    confirmButtonText: response.button

                }).then((result) => {

                    switch(Number(response.status)){

                        case 0:
                        case 1:
                        case 2:

                        location.reload();

                        break;

                        case 3:

                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                        break;

                    }

                });

            },

            error: function(response, status, xhr, $form){

                console.log(response);

            },

            beforeSubmit: function(){ helper.blockStage("actualizando redes sociales..."); }

        });


    });
   }

   var addListeners = function(){

    $('#exit_from_general, #exit_from_social_media, #exit_from_map').click(function(e) {

        window.location.replace("index.php");

    });

    $('#edit-map').click(function(e) {

        if(isInMapEditionMode) return;

        Swal({

            title: 'Edición de mapa',

            html: 'Recordá que la modificación de esta información repercute en la web, la app, el newsletter y el sistema de mailing, entre otros tantos servicios.<br><br>Si tenés dudas, comunicate antes con soporte técnico.',

            type: 'warning',

            showCancelButton: true,

            confirmButtonText: 'Entendido!',

            cancelButtonText: 'Mejor no'

        }).then((result) => {

            if (result.value) {

                isInMapEditionMode = true;

                $("#latitud, #longitud").removeAttr('disabled');

            } else if (result.dismiss === Swal.DismissReason.cancel) {

             alert("se fue tiene miedito");
         }

     })

    });
}

var handleMap = function(){

    var map = new GMaps({

        width:'100%',

        height:'400px',

        overflow:'hidden',

        el: '#map',

        lat: -12.043333,

        lng: -77.028333

    });

    $('#geocoding_form').submit(function(e){

        e.preventDefault();

        GMaps.geocode({

            address: $('#input_address').val().trim(),

            callback: function(results, status){

                if(status=='OK'){

                    var latlng = results[0].geometry.location;

                    map.setCenter(latlng.lat(), latlng.lng());

                    map.addMarker({

                        lat: latlng.lat(),

                        lng: latlng.lng()

                    });

                }

            }

        });

    });

}

return {

    init: function() {

        addListeners();

        handleGeneralUpdateSubmit();

        handleSocialMediaUpdateSubmit();

        helper.setMenu();

        handleMap();

    }

};

}();

jQuery(document).ready(function() { GeneralInfo.init(); });
