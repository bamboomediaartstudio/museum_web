/**
 * @summary Add new guide.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
 * @function PageInit
 * @description Initialize and include all the methods of this class.
 */

 var PageInit = function() {

    helper = Helper();

    var editMode;

    var guideId;

    var defaultImageWidth = 2000;

    var defaultImageHeight = 1000;

    var form = $('#add-data-form');
    
    var youtubeForm = $('#youtube-form');

    
    /**
     * @function addFormValidations
     * @description Asign all the form validations for this page.
     */

     var addFormValidations = function() {

        jQuery.validator.setDefaults({

            ignore: ":hidden, [contenteditable='true']:not([name])"

        });

        form.validate({

            rules: {

                name: { required: true },

                surname: { required: true },

                email: { email: true },

                'checkboxes[]': {
                    required: !0
                }

            },

            messages: {

                name: helper.createErrorLabel('Nombre', 'REQUIRED'),

                surname: helper.createErrorLabel('Apellido', 'REQUIRED'),

                email: helper.createErrorLabel('Email', 'EMAIL'),

                'checkboxes[]': helper.createErrorLabel('idioma', 'AT_LEAST_ONE')

            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();

                window.scroll({top: 0, left: 0, behavior: 'smooth' });
            },

            submitHandler: function(form, event) {

                event.preventDefault();

                event.stopPropagation();

                helper.blockStage("actualizando datos...");

                var formData = new FormData(form);

                formData.append("id", guideId);

                formData.append("source", "guide");

                var request = $.ajax({

                    url: "private/users/museum/guides/guide_add.php",

                    type: "POST",

                    contentType: false,
                    
                    processData: false,

                    data: formData,

                    dataType: "json"
                });

                request.done(function(response) {

                    console.log("done");

                    console.log(response);

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch (Number(response.status)) {

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {

                            switch (Number(response.status)) {

                                case 1:

                                window.location.replace("museum_guides_list.php");

                                break;

                            }

                        }

                    });


                    helper.showToastr(response.title, response.msg);

                    helper.unblockStage();

                });

                request.fail(function(jqXHR, textStatus) {

                    console.log(jqXHR);

                    console.log("error");

                    helper.showToastr("ops...!", 'error');

                    helper.unblockStage();

                });

            }

        });
    }

    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

     var addListeners = function() {

        $('[data-toggle="tooltip"]').tooltip();

        $('#exit_from_form').click(function(e) { window.location.replace("index.php"); });

        $('#back_to_list').click(function(e) { window.location.replace("museum_guides_list.php"); });

        $(".update-btn").click(function() {

            if (!editMode) return;

            var dbName = $(this).attr('data-db-value');

            var id = $(this).attr('data-id');

            var newValue = $(this).parent().parent().find(':input').val();

            var fieldName = $(this).parent().parent().find(':input').attr('name');

            var valid = $('#' + fieldName).valid();

            if (dbName != newValue && valid) {

                $(this).attr('data-db-value', newValue);

                updateValue(id, fieldName, newValue, 'museum_guides');

            }

        });

        $('.toggler-info:checkbox').change(function() {

            if (!editMode) return;

            var fieldName = $(this).attr('name');

            var newValue = ($(this).is(":checked")) ? 1 : 0;

            updateValue(guideId, fieldName, newValue, 'museum_guides')

        });

        $('.m-checkbox-list :checkbox').change(function() {

            if (!editMode) return;

            if ($('.m-checkbox-list input[type=checkbox]:checked').length == 0) {

                $(this).prop('checked', true);

                swal({

                    title: "opss!! minutito, minutito!",

                    allowOutsideClick: false,

                    html: "Cada guía debe tener al menos a idioma asignado.",

                    type: "warning",

                    confirmButtonText: "Entendido!"

                })

                return;

            }

            var item = $(this).attr('name');

            var value = $(this).attr('value');

            var newValue = ($(this).is(":checked")) ? 1 : 0;

            var id = $(this).attr('data-id');

            updateNonOrderableCheckbox(id, value, newValue);

        });

    }

    /**
     * @function updateNonOrderableCheckbox
     * @description update checkbox value...
     *
     * @param {int} id                    - item id.
     * @param {int} value                 - The relation id.
     * @param {int} newValue              - The the new value for the relation.
     */

     var updateNonOrderableCheckbox = function(id, value, newValue) {

        console.log(id, value, newValue);

        //sreturn;

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/update_non_orderable_checkboxes.php",

            type: "POST",

            data: {

                id: id,

                value: value,

                newValue: newValue,

                filter: 'museum_guides_languages_relations',

                table: 'id_guide',

                secondTable: 'id_language'
            },

            dataType: "json"
        });

        request.done(function(result) {

            helper.showToastr("Se acualizo", 'Se actualizo');

            helper.unblockStage();

        });

        request.fail(function(jqXHR, textStatus) {


        });
    }


    /**
     * @function showErrorSwal
     * @param msg - the msg.
     * @description Swal alert.
     */

     var showErrorSwal = function(msg){

        Swal({

            title: 'Opss!.',

            html: msg,

            type: 'error',

            confirmButtonText: 'Entendido!'

        })
    }

    /**
     * @function window.onbeforeunload
     * @description scroll to top
     */

     window.onbeforeunload = function () { if(!editMode) window.scrollTo(0,0); }

    
    /**
     * @function updateValue
     * @description update individual value in DB.
     *
     * @param {int} id                 - object id.
     * @param {string} fieldName       - the field to modify
     * @param {string} newValue        - object value.
     * @param {int} filter             - filter type.
     */

     var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/update_value.php",

            type: "POST",

            data: {

                id: id,
                
                fieldName: fieldName,
                
                newValue: newValue,
                
                filter: filter
            },

            dataType: "json"

        });

        request.done(function(result) {

            console.log(result);

            helper.showToastr(result.title, result.msg);

            if (callback != undefined) callback(callbackParams);

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });

        if (callback == undefined) helper.unblockStage();

        localStorage.setItem('refresh', 'true');

    }

    return {

        init: function() {

            editMode = ($('#edit-mode').val() == 1) ? true : false;

            if (editMode) {

                guideId = $('#guide-id').val();

            }

            addListeners();

            addFormValidations();

            helper.setMenu();

        }

    };

}();

jQuery(document).ready(function() { PageInit.init(); });