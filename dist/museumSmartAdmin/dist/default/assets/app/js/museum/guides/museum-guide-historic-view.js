/**
* @summary historical! 
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/

/**
* @function Init
* @description Initialize and include all the methods of this class.
*/

var Init = function() {

    helper = new Helper();

     /**

     * @function addListeners
     * @description Asign all the listeners / event habdlers for this page.
     */

     var addListeners = function(){

    }


    /**
    * @function createDataTable
    * @description Create all the datatables that we use as lists.
    */

    var createDataTable = function(){

        var fileName = $('#historical-list').val();

        var table = $('table.display').DataTable({

            dom: 'Bfrtip',

            buttons: [ 'copy', 'csv', 'excel', 'pdf', 'print'],

            buttons: ['copy', 'print',

            {
                extend: 'csvHtml5',
                title: fileName
            },


            {
                extend: 'excelHtml5',
                title: fileName
            },
            {
                extend: 'pdfHtml5',
                title: fileName
            }
            ],

            pageLength: 50,

            "language": helper.getDataTableLanguageConfig(),

            bAutoWidth: true,

            responsive:true, 

            processing:true,

            "columns": [

            { "width": "5", responsivePriority: 4, orderable: true, targets: 0}, 

            { "width": "5", responsivePriority: 0, orderable: true, targets: 0}, 

            { "width": "35%", responsivePriority: 1, orderable: true, targets: '_all' }, 

            { "width": "35%", responsivePriority: 2, orderable: true, targets: '_all' }, 

            { "width": "5%", responsivePriority: 3, orderable: true, targets: '_all' }
                        ]

        });

        var table2 = $('table.display2').DataTable({

            dom: 'Bfrtip',

            buttons: [ 'copy', 'csv', 'excel', 'pdf', 'print'],

            buttons: ['copy', 'print',

            {
                extend: 'csvHtml5',
                title: fileName
            },


            {
                extend: 'excelHtml5',
                title: fileName
            },
            {
                extend: 'pdfHtml5',
                title: fileName
            }
            ],

            pageLength: 50,

            "language": helper.getDataTableLanguageConfig(),

            bAutoWidth: true,

            responsive:true, 

            processing:true,

            "columns": [

            { "width": "10%", responsivePriority: 4, orderable: true, targets: 0}, 

            { "width": "40%", responsivePriority: 0, orderable: true, targets: 0}, 

            { "width": "40%", responsivePriority: 2, orderable: true, targets: '_all' }, 

            { "width": "10%", responsivePriority: 3, orderable: true, targets: '_all' }
                        ]

        });


        var table3 = $('table.display3').DataTable({

            dom: 'Bfrtip',

            buttons: [ 'copy', 'csv', 'excel', 'pdf', 'print'],

            buttons: ['copy', 'print',

            {
                extend: 'csvHtml5',
                title: fileName
            },


            {
                extend: 'excelHtml5',
                title: fileName
            },
            {
                extend: 'pdfHtml5',
                title: fileName
            }
            ],

            pageLength: 50,

            "language": helper.getDataTableLanguageConfig(),

            bAutoWidth: true,

            responsive:true, 

            processing:true,

            "columns": [

            { "width": "10%", responsivePriority: 4, orderable: true, targets: 0}, 

            { "width": "40%", responsivePriority: 0, orderable: true, targets: 0}, 

            { "width": "40%", responsivePriority: 2, orderable: true, targets: '_all' }, 

            { "width": "10%", responsivePriority: 3, orderable: true, targets: '_all' }
                        ]

        });
        /*$('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-scholarship-checkbox"]', function(event, state) {

            var closestRow = $(this).closest('tr');

            var data = table.row(closestRow).data();

            var name = data[1];

            var itemId = $(this).attr('data-id');

            updateUserStatus(itemId, state, name, 'update', closestRow, table.row(closestRow), null, null, 'scholarship_asigned', 'scholarship_asigned');

        });*/


        /*$('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-upload-checkbox"]', function(event, state) {

            var closestRow = $(this).closest('tr');

            var data = table.row(closestRow).data();

            var name = data[1];

            var itemId = $(this).attr('data-id');

            updateUserStatus(itemId, state, name, 'update', closestRow, table.row(closestRow), null, null, 'uploaded_to_plataforma', 'uploaded_to_plataforma');


        });*/

        /*$('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-payment-checkbox"]', function(event, state) {

            var closestRow = $(this).closest('tr');

            var data = table.row(closestRow).data();

            var name = data[1];

            var itemId = $(this).attr('data-id');

            updateUserStatus(itemId, state, name, 'update', closestRow, table.row(closestRow), null, null, 'payment_done', 'institution_payment_done');


        });*/

        /*$('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-assisted-checkbox"]', function(event, state) {

            var closestRow = $(this).closest('tr');

            var data = table.row(closestRow).data();

            var name = data[1];

            var itemId = $(this).attr('data-id');

            updateUserStatus(itemId, state, name, 'update', closestRow, table.row(closestRow), null, null, 'assisted', 'institution_assisted');


        });*/

        /*$('tbody').on( 'click', '.delete_row', function () {

            var row = table.row($(this).parents('tr'));

            var animatable = $(this).parents('tr');

            var data = table.row( $(this).parents('tr') ).data();

            var order = data[0];

            var name = data[1];

            var actualTable = $("#tab-1").DataTable();

            var tickets = $(this).attr('data-tickets');

            var date = $(this).attr('data-date');

            var id = $(this).attr('data-booking-id');

            var inputValue = name + ", te enviamos este correo para avisarte que fuiste dado de baja de la visita guiada del día " + date + " del Museo Del Holocausto. Recordá que si querés volver a solicitar un turno, lo podés hacer desde la siguiente url: https://museodelholocausto.org.ar/visitas/";

            Swal({

                //input: 'textarea',

                //inputValue: inputValue,

                title: "¿Eliminar Institución?",

                html: 'Esta acción eliminará a <b>'+name+'</b>. Esta acción no se puede deshacer.<br><br>¿Eliminar de todos modos? Esta acción liberará los turnos que hayan sido tomados por la institución.',

                type: 'error',

                showCancelButton: true,

                confirmButtonText: 'Si, eliminar',

                cancelButtonText: 'no, salir'

            }).then((result) => {

                if(result.value != undefined) {

                    updateUserStatus(id, 'true', name, 'delete', animatable, row, actualTable, null,  'deleted');

                    deleteInstitution(id);

                }else{

                }

            })

        });*/

    }

    /*var deleteInstitution = function(id){

        $.ajax({

            type: "POST",

            url: "private/users/museum/booking/delete_institution.php",

            data: {id:id},

            success: function(result){

                console.log(result);

            },

            error: function(xhr, status, error) {

                console.log(xhr.responseText);

                var err = eval("(" + xhr.responseText + ")");

                console.log(err);

            },

            dataType: "json"
        });

    }*/

    /**
* @function updateUserStatus
*
* @description Update / Delete user status from list: we use this to mark the user as online / offline and
* for delete an user completely from the list.
*
* @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
* @param {boolean} state                    - True or false: use it for both, update and delete.
* @param {string} name                  - The name of the person being manipulated.
* @param {string} action                    - Posibilities: 'update' or 'delete'. 
* @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
* @param {DataTable} row                    - Reference to the row that we are dealing with.
* @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
* @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
*/

/*var updateUserStatus = function(id, state, name, action, animatable, row, processTable = null, order = null, defaultColumn = null, source = 'delete_booking_member'){

    helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando visitante...');

    var url = "private/users/museum/general/update_status.php"; 

    $.ajax({

        type: "POST",

        url: url,

        data: {id: id, status:state, action:action, name:name, source: source,

            table:'museum_booking_institutions', defaultColumn:defaultColumn},

            success: function(result){

                console.log(result);

                helper.unblockStage();

                if(action == 'update'){

                    //var newStatus = (result.changeStatus == 'true') ? 'asistió' : 'no asistió';

                    //var value = 'Cambiaste la asistencia de  ' + name + ' a: ' + newStatus;

                    helper.showToastr(result.title, result.msg);

                }else{

                    animatable.fadeOut('slow','linear',function(){

                        console.log(result.msg);

                        helper.showToastr('ELIMINADO', 'Eliminaste a ' + name + ' de la visita guiada.');

                        var count = 0;

                        var data = processTable.rows().data();

                        processTable.rows().every( function (rowIdx, tableLoop, rowLoop) {

                            if(processTable.cell(rowIdx, 0).data() > Number(order)){

                                var saveValue = Number(processTable.cell(rowIdx, 0).data() - 1);

                                processTable.cell(rowIdx, 0).data(saveValue);

                            }

                        });

                        row.remove().draw(true);

                        processTable.rows().invalidate().draw(false);

                    });

                }

            },

            error: function(xhr, status, error) {

                console.log(xhr);

                var err = eval("(" + xhr.responseText + ")");

                alert(err.Message);
            },

            dataType: "json"
        });

}*/



return {

    init: function() {

        //addListeners();

        createDataTable();
    }

};

}();

jQuery(document).ready(function() { Init.init(); });