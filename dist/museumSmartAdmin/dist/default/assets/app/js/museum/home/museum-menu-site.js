/**
 * @summary Manage all the modules for the home.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */
 
/**
* @function MenuSite
* @description Initialize and include all the methods of this class.
*/

var MenuSite = function() {

    helper = new Helper();

    /**

     * @function addListeners
     * @description Asign all the listeners / event habdlers for this page.
     */

     var addListeners = function(){

        $("[name='my-checkbox']").bootstrapSwitch();

    };


    /**
    * @function createDataTable
    * @description Create all the datatables that we use as lists.
    */

    var createDataTable = function(){

        var table = $('table.display').DataTable({

            pageLength: 50,

            "language": helper.getDataTableLanguageConfig(),

            rowReorder: true,

            rowReorder: { update: true },

            "ordering": true,

            bAutoWidth: false, 

            processing:true,

            "columns": [

            { "width": "5%", responsivePriority: 4, "className": "table-cell-edit", orderable: true, targets: 0}, 

            { "visible": false, orderable:false },

            { "width": "80%", responsivePriority: 1, orderable: false, targets: '_all' }, 

            { "width": "15%", responsivePriority: 3, orderable: false, targets: '_all' }
            
            ]

        });

        table.on( 'row-reorder', function (e, diff, edit) {

            var selectedId = edit.triggerRow.data()[1];

            var selectedName = edit.triggerRow.data()[2];

            var array = new Array();

            for (var i=0, ien=diff.length ; i<ien ; i++ ) {

                var rowData = table.row(diff[i].node).data();

                if(rowData[1] == selectedId){

                    var setOld = diff[i].oldData;

                    var setNew = diff[i].newData; 

                }

                array.push({id:rowData[1], oldPosition:diff[i].oldData, newPosition:diff[i].newData, name:rowData[2]})

            }

            if(array.length>0) updateOrder(array, selectedName, setOld, setNew);

        }); 

        table.on( 'row-reordered', function ( e, diff, edit ) {

            for ( var i=0, ien=diff.length ; i<ien ; i++ ) {

            //$(diff[i].node).addClass("reordered");

        }

    });

        $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

           var closestRow = $(this).closest('tr');

           var data = table.row(closestRow).data();

           var id = data[1];

           var name = data[2];

           updateUserStatus(id, state, name, 'update', closestRow, table.row(closestRow));

       });

    }

    /**
    * @function updateOrder
    * @description Update / sort the order of the items inside the list.
    *
    * @param {Object[]} usersToSort             - Array with objects. Each object has the info for the sort.
    * @param {int} usersToSort[].id             - Object id.
    * @param {int} usersToSort[].oldData        - The old position of the object.
    * @param {int} usersToSort[].newData        - The new position of the object.
    * @param {string} usersToSort[].name        - The name of the sorted person. 
    *
    * @param {string} selectedName              - The actual person.
    * @param {int} setOld                       - The old position of the actual person.
    * @param {int} setNew                       - The new position of the actual person.
    */

    var updateOrder = function(usersToSort, selectedName, setOld, setNew){

        helper.blockStage('Actualizando...');

        $.ajax({

            type: "POST",
            
            url: "private/users/museum/general/update_order.php",
            
            data: {array:usersToSort, table:'museum_site_menu', selectedName:selectedName, source:'home_menu'},

            success: function(result){

                var newStatus = (result.changeStatus == 'true') ? 'online' : 'offline';

                var previousStatus = (newStatus == 'online') ? 'offline' : 'online';

                var st1 = result.msg.replace('%name%', '<b>' + selectedName + '</b>');

                var st2 = st1.replace('%oldPosition%', '<b>' + setOld + '</b>');

                var st3 = st2.replace('%newPosition%', '<b>' + setNew + '</b>');
                
                helper.unblockStage();

                helper.showToastr(result.title, st3);

            },

            error: function(xhr, status, error) {

                console.log(xhr.responseText);

                var err = eval("(" + xhr.responseText + ")");

                console.log(err);

            },

            dataType: "json"
        });

    }

    /**
    * @function updateUserStatus
    *
    * @description Update / Delete user status from list: we use this to mark the user as online / offline and
    * for delete an user completely from the list.
    *
    * @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
    * @param {boolean} state                    - True or false: use it for both, update and delete.
    * @param {string} name                      - The name of the person being manipulated.
    * @param {string} action                    - Posibilities: 'update' or 'delete'. 
    * @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
    * @param {DataTable} row                    - Reference to the row that we are dealing with.
    * @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
    * @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
    */

    var updateUserStatus = function(id, state, name, action, animatable, row, processTable = null, order = null){

        helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando opinión...');

        var url = (action == 'update') ? "private/users/museum/general/update_status.php"  : 

        "private/users/museum/general/delete_item.php";

        $.ajax({

          type: "POST",

          //url: "private/users/museum/opinions/opinion_update_status.php",
          
          url: url,

          data: {id: id, token:$('#token').val(), status:state, action:action, name:name, source: 'home_menu',

          table:'museum_site_menu'},

          success: function(result){

            console.log(result);

            helper.unblockStage();

            if(action == 'update'){

                var newStatus = (result.changeStatus == 'true') ? 'online' : 'offline';

                var previousStatus = (newStatus == 'online') ? 'offline' : 'online';

                var st1 = result.msg.replace('%name%', '<b>' + name + '</b>');

                var st2 = st1.replace('%newStatus%', '<b>' + newStatus + '</b>');

                var st3 = st2.replace('%previousStatus%', '<b>' + previousStatus + '</b>');
                
                helper.showToastr(result.title, st3);

            }

        },

        error: function(xhr, status, error) {

            console.log(xhr);

            var err = eval("(" + xhr.responseText + ")");

            alert(err.Message);
        },

        dataType: "json"
    });

    }

    return {

        init: function() {

            helper.setMenu();

            addListeners();

            createDataTable();
            
        }

    };

}();

jQuery(document).ready(function() { MenuSite.init(); });