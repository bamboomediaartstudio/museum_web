/**
 * @summary Library :)
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
 * @function MuseumLibrary
 * @description Initialize and include all the methods of this class.
 */

 var MuseumLibrary = function() {

    helper = Helper();

    var imagePath = '';

    var librarySid = 1;

    var defaultImageWidth = 2000;

    var defaultImageHeight = 1000;

    var form = $('#add-data-form');
    
    var youtubeForm = $('#youtube-form');

    var origin;

    var summernoteDescriptionCounter = 0;

    //cover config...

    initialPreviewCoverConfig = [];

    initialPreviewCoverPaths = [];

    //gallery config...
    
    initialPreviewGalleryConfig = [];

    initialPreviewgalleryPaths = [];

    //videos config...

    initialPreviewVideosConfig = [];

    initialPreviewVideosPaths = [];

    var openKey;

    var openVideoKey;



    /**
     * @function createGallery
     * @description Create the gallery.
     */

     var createGallery = function() {

        var btns = '<button data-origin="image" type="button" class="kv-cust-btn btn btn-sm btn-kv btn-default btn-outline-secondary" title="Editar"{dataKey}>' +
        '<i class="fas fa-pen"></i>' + '</button>';

        $("#images-batch").fileinput({

            otherActionButtons: btns,

            initialPreviewAsData: true,

            initialPreview: initialPreviewgalleryPaths,

            initialPreviewConfig: initialPreviewGalleryConfig,  

            theme: "fas",

            uploadUrl: "private/users/museum/general/update_images_batch.php",

            deleteUrl: "private/users/museum/general/delete_image.php",

            showCaption: true,

            showPreview: true,

            showRemove: false,

            showUpload: true,

            showCancel: true,

            showClose: false,

            showBrowse: true,

            browseOnZoneClick: true,

            previewFileType: 'any',

            language: "es",

            maxFileSize: 0,

            allowedFileTypes: ["image"],

            overwriteInitial: false,

            allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],

            maxFilePreviewSize: 10240,

            minImageWidth: 500,

            minImageHeight: 500,

            uploadExtraData: function() {

                return {

                    id: librarySid,

                    source: 'library'

                };

            }

        }).on('filesorted', function(e, params) {

            rearangeStack('museum_images', JSON.stringify(params.stack));
            
        }).on('fileuploaded', function(e, params) {

        }).on('filebatchuploadcomplete', function(event, files, extra) {

            location.reload();

        }).on("filepredelete", function(jqXHR) {

        }).on('fileerror', function(event, data, msg) {

            //alert("errorio");

        }).on('fileimageresizeerror', function(event, data, msg){

            console.log("fileimageresizeerror");

            showErrorSwal(msg);

        }).on('filedeleted', function(event, key, jqXHR, data) {

            console.log(event, key, jqXHR, data);

            helper.showToastr("Imagen eliminada", 'Se eliminó la imagen');

        }).on('fileuploaderror', function(event, data, msg) {

            var form = data.form, files = data.files, extra = data.extra,

            response = data.response, reader = data.reader;

            showErrorSwal(msg);

            console.log("fileuploaderror");

        }).on('filebeforedelete', function() {

            return new Promise(function(resolve, reject) {

                swal({

                    title: "¿Eliminar imagen?",

                    allowOutsideClick: false,

                    html: "La misma no aparecerá más listada.",

                    type: "warning",

                    showCancelButton: true,

                    confirmButtonText: 'Si, borrar',

                    cancelButtonText: 'no, salir'

                }).then((result) => {

                    if (result.value) {

                        resolve();

                    }else{

                        reject();
                    }

                })

            });
        });

        $('.kv-cust-btn').on('click', function() {

            origin = $(this).attr('data-origin');

            var $btn = $(this);

            var key = $btn.data('key');

            openKey = key;

            var item = initialPreviewGalleryConfig.find(item => item.key == key);

            $('#image-description').val(item.description);

            $('#myModal').modal('show'); 

        });

    }

    /**
     * @function rearangeStack
     * @param db - db to rearange by order.
     * @param stack - the items to rearange.
     * @description Modify items order.
     */

     var rearangeStack = function(db, stack){

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/rearange_images_sort.php",

            type: "POST",

            data: {

                stack: stack,

                db: db

            },

            dataType: "json",

            success: function(result) {

                helper.showToastr(result.title, result.body);

                helper.unblockStage();

            },

            error: function(request, error) {

                console.log(request);

                helper.unblockStage();
            }
        });

    }

    /**
     * @function loadAjaxImages
     * @param loadSource - evaluate this param to get one batch or a single one.
     * @description if we are in edition mode load all the images for Bootstrap File Input.
     */

     var loadAjaxImages = function(from, loadSource, config, myPreviews) {

        var request = $.ajax({

            url: "private/users/services/get_list_of_images.php",

            type: "POST",

            data: {

                id: librarySid,

                source: loadSource,

                from: from
            },

            dataType: "json"
        });

        request.done(function(result) {

            console.log(result);

            for (var i = 0; i < result.length; i++) {

                var internalOrder = result[i].internal_order;

                var id = result[i].id;

                var source = result[i].source;

                var description = result[i].description;

                var imgName = librarySid + '_' + result[i].unique_id + '_original.jpeg';

                var path;

                path = 'private/sources/images/library/' + librarySid + '/images/' + result[i].unique_id + '/' + imgName;

                myPreviews.push(path);

                config.push({

                    'key': id,
                    
                    'internal_order': internalOrder,
                    
                    'source': source,

                    'description' : description,

                    'caption' : (description != null) ? description : ''

                    //'url' : 'private/users/museum/general/delete_image.php'

                });

            }

            createGallery();


        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

        });

    }

    
    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

     var addListeners = function() {

        $('.save-description').click(function(event){

            var description = $('#image-description').val();

            $('#myModal').modal('hide'); 

            var findArray;

            var fileInput;

            var table;

            findArray = initialPreviewGalleryConfig;

            fileInput = $("#images-batch");

            table = 'museum_images';
 
            var item = findArray.find(item => item.key == openKey);

            item.caption = item.description = description;   

            fileInput.fileinput('destroy');

            (origin == 'image') ? createGallery() : createVideosGallery();

            updateValue(openKey, 'description', description, table);

        })

        
        $('[data-toggle="tooltip"]').tooltip();

        /*autosize for the textareas */

        autosize(document.querySelector('textarea'));

    }

    /**
     * @function updateValue
     * @description update individual value in DB.
     *
     * @param {int} id                 - object id.
     * @param {string} fieldName       - the field to modify
     * @param {string} newValue        - object value.
     * @param {int} filter             - filter type.
     */

     var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/update_value.php",

            type: "POST",

            data: {

                id: id,
                
                fieldName: fieldName,
                
                newValue: newValue,
                
                filter: filter
            },

            dataType: "json"

        });

        request.done(function(result) {

            console.log(result);

            helper.showToastr(result.title, result.msg);

            if (callback != undefined) callback(callbackParams);

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });

        if (callback == undefined) helper.unblockStage();

        localStorage.setItem('refresh', 'true');

    }

    /**
     * @function showErrorSwal
     * @param msg - the msg.
     * @description Swal alert.
     */

     var showErrorSwal = function(msg){

        Swal({

            title: 'Opss!.',

            html: msg,

            type: 'error',

            confirmButtonText: 'Entendido!'

        })
    }

    /**
     * @function window.onbeforeunload
     * @description scroll to top
     */

     window.onbeforeunload = function () { 

        window.scrollTo(0,0); 

    }



    return {

        init: function() {

            helper.setMenu();

            addListeners();

            loadAjaxImages('museum_images', 'library', initialPreviewGalleryConfig, initialPreviewgalleryPaths);

        }

    };

}();

jQuery(document).ready(function() { MuseumLibrary.init(); });