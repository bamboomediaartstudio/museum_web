/**
 * @summary Add new opinion.
 *
 * @description - add a new opinion to the site...
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

 Dropzone.autoDiscover = false;

/**
* @function MuseumOpinionAdd
* @description Initialize and include all the methods of this class.
*/

var MuseumOpinionAdd = function() {

	helper = Helper();

	var editMode;

	var imagePath;

	var userId;

	var userName;

	var form = $('#my-awesome-dropzone');

	/**
	* @function addFormValidations
	* @description Asign all the form validations for this page.
	*/

	var addFormValidations = function() {

		form.validate({

			rules: {

				name: {required: true},

				surname: {required: true},

				quote: {required: true},

				role: {required:true},

				company: {required:true}

			},
			messages: {

				name: helper.createErrorLabel('nombre', 'REQUIRED'),

				surname: helper.createErrorLabel('apellido', 'REQUIRED'),

				quote: helper.createErrorLabel('frase', 'REQUIRED'),

				role: helper.createErrorLabel('rol', 'REQUIRED'),

				company: helper.createErrorLabel('empresa', 'REQUIRED')


			},

			invalidHandler: function(e, r) {

				$("#error_msg").removeClass("m--hide").show();

				window.scroll({top: 0, left: 0, behavior: 'smooth'});
			},

		});

	}

	/**
	* @function addListeners
	* @description Asign all the listeners.
	*/

	var addListeners = function(){

		$('#exit_from_form').click(function(e) { window.location.replace("index.php"); });
		
		$('#back_to_list').click(function(e) { window.location.replace("museum_opinions_list.php"); });

		$(".update-btn").click(function(){

			if(!editMode) return;

			var dbName = $(this).attr('data-db-value');

			var id = $(this).attr('data-id');

			var newValue = $(this).parent().parent().find(':input').val();

			var fieldName = $(this).parent().parent().find(':input').attr('name');

			var valid = $('#' + fieldName).valid();

			if(dbName != newValue && valid){

				$(this).attr('data-db-value', newValue);

				updateValue(id, fieldName, newValue, 'string', 0, 'update');

			}

		});

	}

	/**
	* @function updateValue
	* @description update individual value in DB.
	*
	* @param {int} id                 - object id.
	* @param {string} column          - object column.
	* @param {string} newValue        - object value.
	* @param {string} dataType        - the object data type.
	* @param {int} sid                - internal id.
	* @param {string} action          - what should we do?
	*/

	var updateValue = function(id, column, newValue, dataType, sid, action){

		helper.blockStage("actualizando...");

		var request = $.ajax({

			url: "private/users/museum/opinions/opinion_update_field.php",

			type: "POST",

			data: {id:id, column:column, newValue:newValue, dataType:dataType, sid:sid, action:action},

			dataType: "json"

		});

		request.done(function(result) {

			console.log(result);

			helper.showToastr(result.title, result.msg);

		});

		request.fail(function(jqXHR, textStatus) {

			console.log(jqXHR);

		});

		helper.unblockStage();

		localStorage.setItem('refresh', 'true');
		
		localStorage.setItem('make', 'donskys');

		console.log(localStorage.getItem('make'));
	}

	var deleteImage = function(id, file){

		helper.blockStage("Eliminando imagen...");

		var request = $.ajax({

			url: "private/users/museum/images_general/delete_image.php",

			type: "POST",

			data: {id:id, file:file, source:'testimonials', userName:userName},

			dataType: "json"

		});

		request.done(function(result) {

			console.log(result);

			helper.unblockStage();

			helper.showToastr(result.title, result.msg);

			myDropzone.removeAllFiles();

			myDropzone.options.maxFiles = 1;

			myDropzone.setupEventListeners();

		});

		request.fail(function(jqXHR, textStatus) {

			console.log("error");
			
			console.log(textStatus);

			console.log(jqXHR);

		});

		helper.unblockStage();


	}

	/**
	* @function handleDropZone
	* @description manage all in relation woth DropZoneJS for images.
	*/

	var handleDropZone = function(){

		var urlPath = (editMode) ? 'private/users/museum/images_general/add_image.php' : 'private/users/museum/opinions/opinion_add.php';

		var autoProcessQueue = (editMode) ? true : false;

		myDropzone = new Dropzone("#my-awesome-dropzone", {

			paramName: 'file',

			url: urlPath,

			clickable: true,

			acceptedFiles: 'image/*',

			autoProcessQueue: autoProcessQueue,

			maxFilesize: 2000,

			uploadMultiple: false,

			addRemoveLinks: true,

			dictRemoveFile: 'eliminar archivo',

			maxFiles: 1,

			thumbnailWidth: 350,

			thumbnailHeight:350,

			thumbnailMethod: 'contain',

			previewsContainer: '.dropzone-previews',

			dictRemoveFileConfirmation: '¿Eliminar imagen de perfil?',

			init: function() {

				var _this = this;

				var myDropzone = this;

				this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {

					e.preventDefault();

					e.stopPropagation();

					if (!form.valid()) { return; 

					}

					$('#add_new_opinion').addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

					helper.blockStage("actualizando datos...");

					if (form.valid() == true) { 

						if (myDropzone.getQueuedFiles().length > 0) {  

							myDropzone.processQueue();  

						} else {  

							var blob = new Blob();

							blob.upload = { 'chunked': myDropzone.defaultOptions.chunking };

							myDropzone.uploadFile(blob);

						} 

					} 

				});

				this.on("maxfilesexceeded", function(file){ });

				this.on("complete", function(file){ });

				this.on("removedfile", function(file) { 

					deleteImage(userId, file.name);

					//myDropzone.destroy();

				});


				this.on("error", function(file) { 

					console.log("error");
					
					console.log(file);
				});

				this.on("sending", function(file, xhr, formData) {

					if(editMode){

						formData.append("id", userId);

						formData.append("userName", userName);
                        
                        formData.append("source", "opinions");

					}

				});

				this.on("success", function(file, data) {

					console.log("entra");

					console.log(data);

					console.log(file);

					var response = JSON.parse(file.xhr.responseText);

					console.log(response);

					myDropzone.removeEventListeners();

					helper.unblockStage();

					$('#add_new_opinion').removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

					swal({

						title: response.title,

						allowOutsideClick: false,

						html: response.msg,

						type: response.alert,

						showCancelButton: (response.status == 1) ? true : false,

						confirmButtonText: response.button,

						cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

					}).then((result) => {

						if (result.value) {

							switch(Number(response.status)){

								case 0:
								case 1:
								case 2:
								case 3:
								case 4:

								location.reload();

								break;

							}

						} else if (result.dismiss === Swal.DismissReason.cancel) {

							switch(Number(response.status)){

								case 1:

								window.location.replace("museum_opinions_list.php?highlight=true");

								break;

							}

						}

					});

				});

			}

		});

		if(editMode && imagePath != ""){

			var mockFile = { name: imagePath, size: 5668, width:200, height:200 };

			myDropzone.emit("addedfile", mockFile);

			myDropzone.emit("thumbnail", mockFile, imagePath);

			myDropzone.emit("complete", mockFile);

			var existingFileCount = 1;

			myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;

			myDropzone.removeEventListeners();

			console.log(myDropzone.options.maxFiles);
		}

		Dropzone.confirm = function(question, accepted, rejected) {

			Swal({

				title: question,

				html: 'Si eliminas la imagen, mostraremos un avatar a forma de reemplazo.',

				type: 'warning',

				showCancelButton: true,

				confirmButtonText: 'Entendido! Eliminar',

				cancelButtonText: 'salir'

			}).then((result) => {

				if (result.value) accepted();

			})

		};

	}

	return {

		init: function() {

			editMode = ($('#edit-mode').val() == 1) ? true : false;

			if(editMode){

				var temp;

				if(document.referrer != ""){

					temp = helper.getGETVariables(document.referrer, 'tabId');

				}else{

					temp = null;
				}

				redirectId = (temp == null) ? 1 : temp; 

				imagePath = $('#image-path').val();

				userId = $('#opinion-id').val();

				userName = $('#opinion-person-name').val();

				console.log(imagePath);
			}

			addListeners();

			addFormValidations();

			handleDropZone();

			helper.setMenu();

		}

	};

}();

jQuery(document).ready(function() { MuseumOpinionAdd.init(); });

