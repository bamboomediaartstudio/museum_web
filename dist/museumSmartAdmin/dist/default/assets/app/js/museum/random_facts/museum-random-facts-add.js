/**
 * @summary Add random facts
 *
 * @description - Add radnom facts
 *
 * @author Colo baggins <colorado@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */


/**
 * @function MuseumRandomFatcsAdd
 * @description Initialize and include all the methods of this class.
 */

 var MuseumRandomFatcsAdd = function() {

    helper = Helper();

    var editMode;

    var redirectId;

    var factId;

    var factDescription;

    var addRedirectId = 0;

    var isFirstLoad = true;

    var myElement;


    var form = $('#add-fact-data-form');


    /**
     * @function addFormValidations
     * @description Asign all the form validations for this page.
     */

     var addFormValidations = function() {

        jQuery.validator.setDefaults({

            ignore: ":hidden, [contenteditable='true']:not([name])"

        });


        form.validate({

            rules: {

                description: {
                    required: true
                },

                'checkboxes[]': {
                    required: !0
                },

                'checkboxesFeedback[]': {
                    required: !0
                }


            },
            messages: {

                description: helper.createErrorLabel('Descripcion', 'REQUIRED'),

                'checkboxes[]': helper.createErrorLabel('categoría', 'AT_LEAST_ONE'),

                'checkboxesFeedback[]': helper.createErrorLabel('Feedback', 'AT_LEAST_ONE')

            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();



                window.scroll({top: 0, left: 0, behavior: 'smooth' });
            },

            submitHandler: function(form, event) {

                event.preventDefault();

                event.stopPropagation();

                helper.blockStage("actualizando datos...");

                var formData = new FormData(form);

                var request = $.ajax({

                    url: "private/users/museum/random_facts/facts_add.php",

                    type: "POST",

                    contentType: false,

                    processData: false,

                    data: formData,

                    dataType: "json"
                });

                request.done(function(response) {

                  console.log("response antes de swal y toastr");

                  console.log(response);

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch (Number(response.status)) {

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {

                            switch (Number(response.status)) {

                                case 1:

                                window.location.replace("museum_random_facts_list.php");

                                break;

                            }

                        }

                    });


                    helper.showToastr("Se acualizo", 'Se actualizo');

                    helper.unblockStage();

                });

                request.fail(function(jqXHR, textStatus) {

                    console.log(jqXHR);

                    console.log("error");

                    helper.showToastr("ops", 'error');

                    helper.unblockStage();

                });

            }

        });
      }



    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

     var addListeners = function() {

       $('[data-toggle="tooltip"]').tooltip();


       //Description (random fact description)

       myElement = $('#description').summernote({

           disableDragAndDrop: true,

           fontNames : ['CooperHewitt-Medium', 'Arial', 'sans-serif'],

           height: 200,

           toolbar: [

           ['style', ['bold', 'italic', 'underline']],

           ['Misc', ['fullscreen', 'undo', 'redo']]

           ],

           callbacks: {

               onInit: function() { },

               onEnter: function() { },

               onKeyup: function(e) {

                   if($(this).attr('name') == 'description'){

                       summernoteDescriptionCounter = e.currentTarget.innerText;

                       if(summernoteDescriptionCounter !=0){

                           $('.summernote-description-error').addClass('d-none');

                       }else{

                           $('.summernote-description-error').removeClass('d-none');

                       }

                   }

               },

               onKeydown: function(e) { },

               onPaste: function(e) { },

               onChange: function(contents, $editable) { },

               onBlur: function(){ },

               onFocus: function() { }

           }

       });



       $('#exit_from_form').click(function(e) {
           window.location.replace("index.php");
       });

       $('#back_to_list').click(function(e) {
           window.location.replace("museum_random_facts_list.php?tabId=" + redirectId);
       });

       $(".update-btn").click(function() {
         console.log("UPDATE FIELD");

           if (!editMode) return;

           var dbName = $(this).attr('data-db-value');

           var id = $(this).attr('data-id');

           var newValue = $(this).parent().parent().find(':input').val();

           var fieldName = $(this).parent().parent().find(':input').attr('name');

           console.log("dbName: "+dbName);
           console.log("id: "+id);
           console.log("newValue: "+newValue);
           console.log("fieldName: "+fieldName);

           if(dbName == "categoriesFlag"){

             console.log("categorias actualizar!!");


             var categoriesChecked = $('input[name="checkboxes[]"]:checked');


             var arrCategories = [];

             if(categoriesChecked.length > 0){

               categoriesChecked.each(function(){

                //Put id of checked categories into array
                //console.log($(this).val());

                arrCategories.push($(this).val());

              });

            }else{

              //Ninguna categoria fue seleccionada o  se deseleccionaron todas las categorias

            }


            //Valid at least one category checked

            var valid = $('input[name="checkboxes[]').valid();

            if(valid){

                updateCheckboxesService(arrCategories, 'updateCategories');

            }


          }else if(dbName == "feedbackFlag"){ //Update feedback options for user

            console.log("feedback actualizar!!");


            var feedbackOptionsChecked = $('input[name="checkboxesFeedback[]"]:checked');


            var arrFeedbackOptions = [];

            if(feedbackOptionsChecked.length > 0){

              feedbackOptionsChecked.each(function(){

               //Put id of checked categories into array
               //console.log($(this).val());

               arrFeedbackOptions.push($(this).val());

             });

           }else{

             //Ninguna opcion de feedback fue seleccionada o  se deseleccionaron todos

           }


           //Valid at least one category checked

           var valid = $('input[name="checkboxesFeedback[]').valid();

           if(valid){

                console.log("Feedback Options: ");
                console.log(arrFeedbackOptions)

               updateCheckboxesService(arrFeedbackOptions, 'updateFeedbackOptions');

           }

          }else{

             var valid = $('#' + fieldName).valid();

             if (dbName != newValue && valid) {

                 console.log("changing something! Go to Update");

                 $(this).attr('data-db-value', newValue);

                 updateValue(id, fieldName, newValue, 'museum_random_facts');

             }else{
               console.log("nothing changed, nothing happens");
             }

           }

       });


/*
       $('.toggler-info:checkbox').change(function() {

           if (!editMode) return;

           var fieldName = $(this).attr('name');

           var newValue = ($(this).is(":checked")) ? 1 : 0;

           updateValue(filmId, fieldName, newValue, 'app_museum_films');

       });*/

     }// /.addListeners


     /**
      * @function updateValue
      * @description update individual value in DB.
      *
      * @param {int} id                 - object id.
      * @param {string} fieldName       - the field to modify
      * @param {string} newValue        - object value.
      * @param {int} filter             - filter type.
      */

      var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

         console.log("Update - FieldName: "+fieldName + " - newValue: "+newValue + " - table: "+filter);

         console.log("update value...only once?");

         helper.blockStage("actualizando...");

         var request = $.ajax({

             url: "private/users/museum/general/update_value.php",

             type: "POST",

             data: {

                 id: id,

                 fieldName: fieldName,

                 newValue: newValue,

                 filter: filter
             },

             dataType: "json"

         });

         request.done(function(result) {

             console.log("tracing result!");

             console.log(result);

             helper.showToastr(result.title, result.msg);

             if (callback != undefined) callback(callbackParams);

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

             console.log("error");

         });

         if (callback == undefined) helper.unblockStage();

         localStorage.setItem('refresh', 'true');

     }// /.updateValue




     /**
      * @function updateCategories
      * @param id -
      * @param categoriesTotal - Total categories selected/checked
      * @description if we are in edition mode load all the images for Bootstrap File Input.
      */
     var updateCheckboxesService = function(arrOption, actionUpdate){

       var request = $.ajax({

             url: "private/users/museum/random_facts/facts_service_ajax.php",

             type: "POST",

             data: {

               id: factId,

               'arrOptionsChecked[]': arrOption,  ////'arrCategories[]': arrOption,

               action: actionUpdate         // action: 'updateCategories'

             },

             dataType: "json"
         });

         request.done(function(response) {

           console.log("response antes de swal y toastr");

           console.log(response);

             swal({

                 title: response.title,

                 allowOutsideClick: false,

                 html: response.msg,

                 type: response.alert,

                 showCancelButton: (response.status == 1) ? true : false,

                 confirmButtonText: response.button,

                 cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

             }).then((result) => {

                 location.reload();

             });


             helper.showToastr("Se acualizo", 'Se actualizo');

             helper.unblockStage();

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

             console.log("error");

             helper.showToastr("ops", 'error');

             helper.unblockStage();

         });


     }// Ends updateCategories()



    return {

        init: function() {

            editMode = ($('#edit-mode').val() == 1) ? true : false;

            helper.setMenu();

            if (editMode) {

              console.log("Estamos en edit mode!");

                var temp;

                if (document.referrer != "") {

                    temp = helper.getGETVariables(document.referrer, 'tabId');

                    console.log("temP: "+temp);


                } else {

                    temp = null;
                }

                redirectId = (temp == null) ? 1 : temp;

                factId = $('#fact-id').val();

            } else {

                isFirstLoad = false;
            }

            addListeners();

            addFormValidations();



        } // /.init function

    }; // /.return

 }();

 jQuery(document).ready(function() { MuseumRandomFatcsAdd.init(); });
