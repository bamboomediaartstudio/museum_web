/**
 * @summary edit the Shoa Description.
 *
 * @description - ...
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */


/**
* @function MuseumOpinionAdd
* @description Initialize and include all the methods of this class.
*/

var MuseumOpinionAdd = function() {

	helper = Helper();

	var v;

	var form = $('#shoa-description-form');

	/**
	* @function addFormValidations
	* @description Asign all the form validations for this page.
	*/

	var addFormValidations = function() {

		$.validator.setDefaults({

			highlight: function(element){

				$(element)

				.closest('.form-group')

				.addClass('has-danger');

			},

			unhighlight: function(element){

				$(element)

				.closest('.form-group')

				.removeClass('has-danger');

			},

			errorPlacement: function(error, element){

				if(element.prop('type')==='textarea'){

					error.insertAfter(element.next());

				}else{

					error.insertAfter(element);

				}

			}

		})

		form.validate({

			rules: {

				'shoa-description': {required: true}

			},
			messages: {

				'shoa-description': helper.createErrorLabel('descripción', 'REQUIRED')
			},

			invalidHandler: function(e, r) {

				$("#error_msg").removeClass("m--hide").show();

				window.scroll({top: 0, left: 0, behavior: 'smooth'});
			},

		});

	}

	/**
	* @function addListeners
	* @description Asign all the listeners.
	*/

	var addListeners = function(){

		myElement = $('#shoa-description');

		myElement.summernote({

			disableDragAndDrop: true,

			height: 200,


			toolbar: [

			['style', ['bold', 'italic', 'underline']],

			['Misc', ['fullscreen', 'undo', 'redo']]

			],

			callbacks: {

				onInit: function() { },

				onEnter: function() { },

				onKeyup: function(e) { },

				onKeydown: function(e) { },

				onPaste: function(e) { },

				onChange: function(contents, $editable) {

					var element = $(this);

					console.log(element);

					element.val(element.summernote('isEmpty') ? "" : contents);

					v.element(element);
				},

				onBlur: function(){ },

				onFocus: function() { }

			}

		});
		
		$('#back_to_home').click(function(e) { window.location.replace("index.php"); });

		$('#update_description').click(function(e) {

            e.preventDefault();

            var btn = $(this);
            
            if (!form.valid()) { return; }

            updateValue(1, 'description', $('#shoa-description').val(), 'museum_shoa_description');

        });

	}

	/**
    * @function updateValue
    * @description update individual value in DB.
    *
    * @param {int} id                 - object id.
    * @param {string} fieldName       - the field to modify
    * @param {string} newValue        - object value.
    * @param {int} filte              - filter type.
    */

    var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined){

    	helper.blockStage("actualizando...");

    	console.log(id, fieldName, newValue, filter);

        var request = $.ajax({

            url: "private/users/museum/general/update_value.php",

            type: "POST",

            data: {id:id, fieldName:fieldName, newValue:newValue, filter:filter},

            dataType: "json"

        });

        request.done(function(result) {

        	console.log(result);

            helper.showToastr(result.title, result.msg);

            if(callback != undefined) callback(callbackParams);

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

            
        });

        if(callback == undefined) helper.unblockStage();

        localStorage.setItem('refresh', 'true');

    }

    return {

		init: function() {

			addListeners();

			addFormValidations();

		}

	};

}();

jQuery(document).ready(function() { MuseumOpinionAdd.init(); });

