/**
 * @summary edit the a FAQ.
 *
 * @description - ...
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */


/**
* @function MuseumOpinionAdd
* @description Initialize and include all the methods of this class.
*/

var MuseumOpinionAdd = function() {

	helper = Helper();

	var v;

	var form = $('#shoa-faq-form');

	/**
	* @function addFormValidations
	* @description Asign all the form validations for this page.
	*/

	var addFormValidations = function() {

		$.validator.setDefaults({

			highlight: function(element){

				$(element)

				.closest('.form-group')

				.addClass('has-danger');

			},

			unhighlight: function(element){

				$(element)

				.closest('.form-group')

				.removeClass('has-danger');

			},

			errorPlacement: function(error, element){

				if(element.prop('type')==='textarea'){

					error.insertAfter(element.next());

				}else{

					error.insertAfter(element);

				}

			}

		})

		form.validate({

			rules: {

				'question': {required: true},

				'answer': {required: true},

			},
			messages: {

				'question': helper.createErrorLabel('pregunta', 'REQUIRED'),

				'answer': helper.createErrorLabel('respuesta', 'REQUIRED'),
			},

			invalidHandler: function(e, r) {

				$("#error_msg").removeClass("m--hide").show();

				window.scroll({top: 0, left: 0, behavior: 'smooth'});
			},

		});

	}

	/**
	* @function addListeners
	* @description Asign all the listeners.
	*/

	var addListeners = function(){

		myElement = $('#answer');

		myElement.summernote({

			disableDragAndDrop: true,

			height: 200,

			toolbar: [

			['style', ['bold', 'italic', 'underline']],

			['Misc', ['fullscreen', 'undo', 'redo']]

			],

			callbacks: {

				onInit: function() { },

				onEnter: function() { },

				onKeyup: function(e) { },

				onKeydown: function(e) { },

				onPaste: function(e) { },

				onChange: function(contents, $editable) {

					var element = $(this);

					element.val(element.summernote('isEmpty') ? "" : contents);

					v.element(element);
				},

				onBlur: function(){ },

				onFocus: function() { }

			}

		});
		
		$('#back_to_faq').click(function(e) { window.location.replace("museum_shoa_faq_list.php"); });

		$('#add_faq').click(function(e) {

			e.preventDefault();

			var btn = $(this);

			if (!form.valid()) { return; }

			addNewValue($('#question').val(), $('#answer').val());

		});

		$('#update_faq').click(function(e) {

			e.preventDefault();

			var btn = $(this);

			if (!form.valid()) { return; }

			console.log($('#question').val(), $('#answer').val(), $('#faq-id').val());

			updateValue(

				$('#faq-id').val(), 

				['question', 'answer'], 

				[$('#question').val(), $('#answer').val()], 

				'museum_shoa_faq');

		});

	}

	/**
    * @function addNewValue
    * @description add a new value to the FAQ...
    *
    * @param {string} question        - the question to add.
    * @param {string} answer          - the answer to the question :)
    */


    var addNewValue = function(question, answer){

    	helper.blockStage("Agregando nueva FAQ...");

    	var request = $.ajax({

    		url: "private/users/museum/shoa/shoa_faq_add.php",

    		type: "POST",

    		data: {question:question, answer:answer},

    		dataType: "json"

    	});

    	request.done(function(result) {

    		console.log(result);

    		swal({

    			title: result.title,

    			allowOutsideClick: false,

    			html: result.msg,

    			type: result.alert,

    			showCancelButton: (result.status == 1) ? true : false,

    			confirmButtonText: result.button,

    			cancelButtonText: (result.status == 1) ? 'ir a la lista' : ''

    		}).then((swalResult) => {

    			if (swalResult.value) {

    				switch(Number(result.status)){

    					case 0:
    					case 1:
    					case 2:
    					case 3:
    					case 4:

    					location.reload();

    					break;

    				}

    			} else if (swalResult.dismiss === Swal.DismissReason.cancel) {

    				switch(Number(result.status)){

    					case 1:

    					window.location.replace("museum_shoa_faq_list.php?highlight=true");

    					break;

    				}

    			}

    		});


            //helper.showToastr(result.title, result.msg);

            if(callback != undefined) callback(callbackParams);

        });

    	request.fail(function(jqXHR, textStatus) {

    		console.log(jqXHR);

    		console.log("error");


    	});

    	helper.unblockStage();

    	localStorage.setItem('refresh', 'true');

    }

	/**
    * @function updateValue
    * @description update individual value in DB.
    *
    * @param {int} id                 - object id.
    * @param {string} fieldName       - the field to modify
    * @param {string} newValue        - object value.
    * @param {int} filter             - filter type.
    */

    var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined){

    	helper.blockStage("actualizando...");

    	var request = $.ajax({

    		url: "private/users/museum/general/update_value.php",

    		type: "POST",

    		data: {id:id, fieldName:fieldName, newValue:newValue, filter:filter},

    		dataType: "json"

    	});

    	request.done(function(result) {

    		console.log(result);

    		helper.showToastr(result.title, result.msg);

    		if(callback != undefined) callback(callbackParams);

    	});

    	request.fail(function(jqXHR, textStatus) {

    		console.log(jqXHR);

    		console.log("error");


    	});

    	if(callback == undefined) helper.unblockStage();

    	localStorage.setItem('refresh', 'true');

    }

    return {

    	init: function() {

    		addListeners();

    		addFormValidations();

    	}

    };

}();

jQuery(document).ready(function() { MuseumOpinionAdd.init(); });

