/**
 * @summary Add new user.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

 Dropzone.autoDiscover = false;

/**
* @function MuseumStaffAdd
* @description Initialize and include all the methods of this class.
*/

var MuseumStaffAdd = function() {

    helper = Helper();

    var editMode;

    var imagePath;

    var redirectId;

    var userId;

    var userName;

    var addRedirectId = 0;

    var form = $('#my-awesome-dropzone');

    /**
    * @function addFormValidations
    * @description Asign all the form validations for this page.
    */

    var addFormValidations = function() {

        form.validate({

            rules: {

                name: {required: true},

                surname: {required: true},

                'checkboxes[]': {

                    required: !0

                },

                email: {email:true},

                role: {required:false}

            },
            messages: {

                name: helper.createErrorLabel('nombre', 'REQUIRED'),

                surname: helper.createErrorLabel('apellido', 'REQUIRED'),

                email: helper.createErrorLabel('email', 'EMAIL'),

                'checkboxes[]': helper.createErrorLabel('categoría', 'AT_LEAST_ONE')

            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();

                window.scroll({top: 0, left: 0, behavior: 'smooth'});
            },

        });

    }

    /**
    * @function addListeners
    * @description Asign all the listeners.
    */

    var addListeners = function(){

        window.onbeforeunload = function () { window.scrollTo(0, 0); }

        $(document).on("keypress", "form", function(event){ return event.keyCode != 13; });

        $('#exit_from_form').click(function(e) { window.location.replace("index.php"); });
        
        $('#back_to_list').click(function(e) { window.location.replace("museum_staff_list.php?tabId=" + redirectId); });

        t = new Bloodhound({

            datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

            queryTokenizer: Bloodhound.tokenizers.whitespace,

            prefetch:{

                url : "private/users/services/get_list_of_staff_roles.php",

                cache: false
            }

        })

        var context = $("#roles-list .typeahead").typeahead(null, {

            hint: false,

            highlight: false,

            minLength: 1,

            name: "best-pictures",

            display: "value",

            source: t,

            autoselect: true,

            templates: {

                suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")

            }

        }).on("typeahead:render", function() {

            var searchTerm = $(this).val();

            $('#roles-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

        }).on('typeahead:selected', function (e, datum) {

            $('#hidden-role-id').val(datum.id);                

        }).on('keyup', this, function (event) {


        }).on('keydown', this, function (event) {

        }).on('focus', this, function(event){

            if(editMode) $('#hidden-role-id').val("");

        });

        $('.m-checkbox-list :checkbox').change(function(){

            $(".m-checkbox-list :checkbox").each(function(){

                if($(this).prop('checked')){

                    addRedirectId = $(this).attr('value');

                    return;
                }
            });

            if(!editMode) return;

            if($('.m-checkbox-list input[type=checkbox]:checked').length==0) {

                $(this).prop('checked', true);

                swal({

                    title: "opss!",

                    allowOutsideClick: false,

                    html: "Cada usuario debe pertener al menos a una categoría. Al menos una de las opciones debe quedar activa.<br><br>Si lo que queres es eliminar a un miembro o pasarlo a offline, podés hacerlo el <a href='museum_staff_list.php'>siguiente link</a>.",

                    type: "warning",

                    confirmButtonText: "Entendido!"

                })

            }

            var item = $(this).attr('name');

            var value = $(this).attr('value');

            var newValue = ($(this).is(":checked")) ? 1 : 0;

            var id = $(this).attr('data-id');

            updateValue(id, value, newValue, 'boolean', 1, 'update');

        });

        $('.toggler-info:checkbox').change(function() {

            if(!editMode) return;

            var item = $(this).attr('name');

            var value = $(this).attr('value');

            test = item.replace(/-/g, "_");

            var newValue = ($(this).is(":checked")) ? 1 : 0;

            var id = $(this).attr('data-id');

            updateValue(id, test, newValue, 'boolean', 0, 'update');

        }); 

        $('.upate-for-typeahead').click(function(){

            var dbName = $(this).attr('data-db-value');

            var id = $(this).attr('data-id');

            var idValue = $('#hidden-role-id').val();

            var newValue = $('input.typeahead.tt-input').val();

            if(newValue != dbName) updateValue(id, idValue, newValue, 'int', 2, 'update');

        })



        $(".update-btn").click(function(){

            if(!editMode) return;

            var dbName = $(this).attr('data-db-value');

            var id = $(this).attr('data-id');

            var newValue = $(this).parent().parent().find(':input').val();

            var fieldName = $(this).parent().parent().find(':input').attr('name');

            var valid = $('#' + fieldName).valid();

            if(dbName != newValue && valid){

                $(this).attr('data-db-value', newValue);

                updateValue(id, fieldName, newValue, 'string', 0, 'update');

            }

        });

    }

    /**
    * @function updateValue
    * @description update individual value in DB.
    *
    * @param {int} id                 - object id.
    * @param {string} column          - object column.
    * @param {string} newValue        - object value.
    * @param {string} dataType        - the object data type.
    * @param {int} sid                - internal id.
    * @param {string} action          - what should we do?
    */

    var updateValue = function(id, column, newValue, dataType, sid, action){

        helper.blockStage("actualizando...");

        var token = $('input[name=addToken]').val();

        var request = $.ajax({

            url: "private/users/museum/staff/staff_update_field.php",

            type: "POST",

            data: {id:id, column:column, newValue:newValue, dataType:dataType, sid:sid, action:action,

                token: token},

                dataType: "json"

            });

        request.done(function(result) {

            helper.showToastr(result.title, result.msg);

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

        });

        helper.unblockStage();

        localStorage.setItem('refresh', 'true');
        
        localStorage.setItem('make', 'donskys');

        console.log(localStorage.getItem('make'));
    }

    var deleteImage = function(id, file){

        console.log("deleting " + id + " - " + file);

        helper.blockStage("Eliminando imagen...");

        var request = $.ajax({

            url: "private/users/museum/images_general/delete_image.php",

            type: "POST",

            data: {id:id, file:file, source:'staff', userName:userName},

            dataType: "json"

        });

        request.done(function(result) {

            console.log(result);

            helper.unblockStage();

            helper.showToastr(result.title, result.msg);

            myDropzone.removeAllFiles();

            myDropzone.options.maxFiles = 1;

             myDropzone.setupEventListeners();

            //myDropzone.files = [];


        });

        request.fail(function(jqXHR, textStatus) {

            console.log("error");
            
            console.log(textStatus);

            console.log(jqXHR);

        });

        helper.unblockStage();


    }

    /**
    * @function handleDropZone
    * @description manage all in relation woth DropZoneJS for images.
    */

    var handleDropZone = function(){

        //var urlPath = (editMode) ? 'private/users/museum/staff/staff_add_image.php' : 'private/users/museum/staff/staff_add.php';
        
        var urlPath = (editMode) ? 'private/users/museum/images_general/add_image.php' : 'private/users/museum/staff/staff_add.php';

        var autoProcessQueue = (editMode) ? true : false;

        myDropzone = new Dropzone("#my-awesome-dropzone", {

            paramName: 'file',

            url: urlPath,

            clickable: true,

            acceptedFiles: 'image/*',

            autoProcessQueue: autoProcessQueue,

            maxFilesize: 2000,

            uploadMultiple: false,

            addRemoveLinks: true,//(editMode) ? true : false,

            dictRemoveFile: 'eliminar archivo',

            maxFiles: 1,

            thumbnailWidth: 350,

            thumbnailHeight:350,

            thumbnailMethod: 'contain',

            previewsContainer: '.dropzone-previews',

            dictRemoveFileConfirmation: '¿Eliminar imagen de perfil?',

            init: function() {

                var _this = this;

                var myDropzone = this;

                this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {

                    e.preventDefault();

                    e.stopPropagation();

                    if (!form.valid()) { return; }

                    $('#add_staff_member').addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                    helper.blockStage("actualizando datos...");

                    if (form.valid() == true) { 

                        if (myDropzone.getQueuedFiles().length > 0) {  

                            myDropzone.processQueue();  

                        } else {  

                         var blob = new Blob();

                         blob.upload = { 'chunked': myDropzone.defaultOptions.chunking };

                         myDropzone.uploadFile(blob);

                     }                                    
                 }            

             });


                this.on("maxfilesexceeded", function(file){

                    //alert("No more files please!");

                    // this.removeFile(file);

                });

                this.on("complete", function(file){

                    console.log("complete");



                });

                this.on("removedfile", function(file) {  deleteImage(userId, file.name); });


                this.on("error", function(file) { 

                    console.log("........");

                    console.log("error");
                    
                    console.log(file);
                });

                this.on("sending", function(file, xhr, formData) {

                    if(editMode){

                        formData.append("id", userId);
                        
                        formData.append("userName", userName);
                        
                        formData.append("source", "staff");

                    }

                });

                this.on("success", function(file, data) {

                    console.log("ok antes");

                    console.log(file, data);

                    var response = JSON.parse(file.xhr.responseText);

                    console.log("responde");

                    console.log(response);

                    myDropzone.removeEventListeners();

                    helper.unblockStage();

                    $('#add_staff_member').removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch(Number(response.status)){

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {



                            switch(Number(response.status)){

                             case 1:

                             window.location.replace("museum_staff_list.php?tabId=" + addRedirectId + "&highlight=true");

                             break;

                         }

                     }

                 });

                });
            }

        });

if(editMode && imagePath != ""){

    var mockFile = { name: imagePath, size: 5668, width:200, height:200 };

    myDropzone.emit("addedfile", mockFile);

    myDropzone.emit("thumbnail", mockFile, imagePath);

    myDropzone.emit("complete", mockFile);

    var existingFileCount = 1;

    myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;

    myDropzone.removeEventListeners();

    console.log(myDropzone.options.maxFiles);
}

Dropzone.confirm = function(question, accepted, rejected) {

    Swal({

        title: question,

        html: 'Si eliminas la imagen, mostraremos un avatar a forma de reemplazo.',

        type: 'warning',

        showCancelButton: true,

        confirmButtonText: 'Entendido! Eliminar',

        cancelButtonText: 'salir'

    }).then((result) => {

                /*var checkedValue = $('#not-show-again-checkbox:checked').val();

                if(checkedValue == "on"){

                    myDropzone.options.dictRemoveFileConfirmation = null;

                    localStorage.setItem('dictRemoveFileConfirmation', true);

                }*/

                if (result.value) accepted();

            })
};


        /*$('#process-queue').click(function(e) {

            var files = myDropzone.getQueuedFiles();

            files.sort(function(a, b){

                return ($(a.previewElement).index() > $(b.previewElement).index()) ? 1 : -1;

            })

            myDropzone.removeAllFiles();
          
            myDropzone.handleFiles(files);
          
            myDropzone.processQueue();

        });*/
    }

    return {

        init: function() {

            editMode = ($('#edit-mode').val() == 1) ? true : false;

            helper.setMenu();

            if(editMode){

                var temp;

                if(document.referrer != ""){

                    temp = helper.getGETVariables(document.referrer, 'tabId');

                }else{

                    temp = null;
                }

                redirectId = (temp == null) ? 1 : temp; 

                imagePath = $('#image-path').val();

                userId = $('#staff-member-id').val();

                userName = $('#staff-member-name').val();

                console.log(imagePath);
            }

            addListeners();

            addFormValidations();

            handleDropZone();

        }

    };

}();

jQuery(document).ready(function() { MuseumStaffAdd.init(); });

