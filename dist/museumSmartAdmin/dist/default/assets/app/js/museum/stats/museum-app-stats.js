/**
 * @summary Manage all the users actions.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

var appId;

/**
 * @function AppStatsTouchScreens
 * @description Initialize and include all the methods of this class.
 */
 var AppStatsTouchScreens = function() {

    var helper = Helper();

    

    /**
    *
    * loadAverageDaysInMonthInThisYear();
    * @description ...
    */

    var loadAverageDaysInMonthInThisYear = function() {

        if ($('#chart_activity_by_month_in_year').length == 0) { return; }

        var request = $.ajax({

            url: "private/users/services/stats/get_historical_data_by_months_in_year.php",

            data: { "appId" : appId},

            type: "POST", dataType: "json"
        });

        request.done(function(response) {

            console.log(response);

            var addResponse = new Array();

            for(var i = 0; i <response[0].length; i++){

                addResponse.push({'label' : response[0][i], 'value' : response[1][i] });
            }

            Morris.Donut({

                element: 'chart_stats_by_level_2', 

                data: addResponse, 

                colors: [mUtil.getColor('accent'), mUtil.getColor('danger'), mUtil.getColor('brand'), mUtil.getColor('success'), '#ff0066', '#cccccc', '#333333']
            });



            var chartData = {

                labels: response[0],

                datasets: [{

                    label: "Promedio de usuarios en este mes",  backgroundColor: mUtil.getColor('danger'),  data: response[1]  

                }]
                };

                var chartContainer = $('#chart_activity_by_month_in_year');

                if (chartContainer.length == 0) { return; }

                var chart = new Chart(chartContainer, {

                    type: 'bar',

                    data: chartData,

                    options: {

                        title: { display: false, },

                        tooltips: { intersect: false, mode: 'nearest', xPadding: 10, yPadding: 10, caretPadding: 10 },

                        legend: { display: false },

                        responsive: true, maintainAspectRatio: false, barRadius: 4,

                        scales: {

                            xAxes: [{ display: false, gridLines: false, stacked: true }],

                            yAxes: [{ display: false, stacked: true, gridLines: false }]
                        },

                        layout: { padding: { left: 0, right: 0, top: 0, bottom: 0 } }
                    }
                });

            });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });
    }

    /**
    *
    * loadAverageDaysInMonth();
    * @description ...
    */

    var loadAverageDaysInMonth = function() {

        if ($('#chart_average_by_days_in_month').length == 0) { return; }

        var request = $.ajax({

            url: "private/users/services/stats/get_historical_data_in_month.php",

            type: "POST", 

            data: { "appId" : appId},

            dataType: "json"
        });

        request.done(function(response) {

            var chartData = {

                labels: response[0],

                datasets: [{

                    label: "Promedio de usuarios en este día del mes", backgroundColor: mUtil.getColor('success'), data: response[1]
             
                }]
            };

            var chartContainer = $('#chart_average_by_days_in_month');

            if (chartContainer.length == 0) { return; }

            var chart = new Chart(chartContainer, {

                type: 'bar', data: chartData,

                options: {

                    title: { display: false, },

                    tooltips: { intersect: false, mode: 'nearest', xPadding: 10, yPadding: 10, caretPadding: 10 },

                    legend: { display: false }, responsive: true, maintainAspectRatio: false, barRadius: 4,

                    ykeys: ["lala", "lala", "lala", "lala"],

                    scales: {

                        xAxes: [{ display: false, gridLines: false, stacked: true }],

                        yAxes: [{ display: false, stacked: true, gridLines: false }]
                    },

                    layout: { padding: { left: 0, right: 0, top: 0, bottom: 0 } }
                }
            });

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });

    }

    
    /**
    *
    * loadAverageInteractionsPerDay();
    * @description ...
    */

    var loadAverageInteractionsPerDay = function(){

        var request = $.ajax({

            url: "private/users/services/stats/get_historical_data_by_day.php",

            type: "POST", 

            data: { "appId" : appId},

            dataType: "json"
        });

        request.done(function(response) {

            var addResponse = new Array();

            for(var i = 0; i <response[0].length; i++){

                addResponse.push({'label' : response[0][i], 'value' : response[1][i] });
            }

             Morris.Donut({

                element: 'chart_stats_by_level', 

                data: addResponse, 

                colors: [mUtil.getColor('accent'), mUtil.getColor('danger'), mUtil.getColor('brand')]
            });

            var config2 = {

                type: 'line',

                data: {
                    
                    labels: response[0],

                    datasets: [{

                        label: "Promedio de usuarios en este día",

                        borderColor: mUtil.getColor('brand'),

                        borderWidth: 2,

                        pointBackgroundColor: mUtil.getColor('succes'),

                        backgroundColor: mUtil.getColor('danger'),

                        pointHoverBackgroundColor: mUtil.getColor('danger'),

                        pointHoverBorderColor: Chart.helpers.color(mUtil.getColor('danger')).alpha(0.2).rgbString(),

                        data: response[1]

                    }]
                },
                options: {

                    title: { display: false, },
                    
                    tooltips: { intersect: false, mode: 'nearest', xPadding: 10, yPadding: 10, caretPadding: 10 },

                    legend: { display: false, labels: { usePointStyle: false } },

                    responsive: true,

                    maintainAspectRatio: false,

                    hover: { mode: 'index' },

                    scales: {
                        xAxes: [{ display: false, gridLines: false, scaleLabel: { display: true, labelString: 'Month' } }],
                        yAxes: [{ display: false, gridLines: false, scaleLabel: { display: true, labelString: 'Value' } }]
                    },

                    elements: { point: { radius: 3, borderWidth: 0, hoverRadius: 8, hoverBorderWidth: 2 } }
                }
            };

            var chart2 = new Chart($('#chart_activity_by_day_in_week'), config2);


        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });
    }

    /**
    *
    * loadAverageInteractionsPerHour();
    * @description ...
    */

    var loadAverageInteractionsPerHour = function() {

        if ($('#chart_activity_by_hour_today').length == 0) { return; }

        var request = $.ajax({

            url: "private/users/services/stats/get_historical_data_by_hour.php",

            type: "POST", 

            data: { "appId" : appId},

            dataType: "json"
        });

        request.done(function(response) {

            var addResponse = new Array();

            for(var i = 0; i <response[0].length; i++){

                addResponse.push({'label' : response[0][i], 'value' : response[1][i] });
            }

            console.log("test: ");

            console.log(addResponse);

            Morris.Donut({

                element: 'chart_stats_by_level_3', 

                data: addResponse, 

                colors: [mUtil.getColor('accent'), mUtil.getColor('danger'), mUtil.getColor('brand'), mUtil.getColor('success'), '#ff0066', '#cccccc', '#333333']
            });

            var config = {

                type: 'line',

                data: {

                    labels: response[0],

                    datasets: [{

                        label: "Promedio de usuarios a esta hora",

                        borderColor: mUtil.getColor('brand'),

                        borderWidth: 2,

                        pointBackgroundColor: mUtil.getColor('success'),

                        backgroundColor: mUtil.getColor('accent'),

                        pointHoverBackgroundColor: mUtil.getColor('brand'),

                        pointHoverBorderColor: Chart.helpers.color(mUtil.getColor('brand')).alpha(0.2).rgbString(),

                        data: response[1]
                    }]
                },
                options: { title: { display: false, },

                tooltips: { intersect: true, mode: 'nearest', xPadding: 10, yPadding: 10, caretPadding: 10 },

                legend: { display: false, labels: { usePointStyle: false } }, responsive: true,  maintainAspectRatio: false,

                hover: { mode: 'index' },

                scales: {

                    xAxes: [{ display:  false, gridLines: false, scaleLabel: { display: true,  labelString: 'hora' } }],

                    yAxes: [{ display: false, gridLines: false, scaleLabel: { display: true, labelString: 'Value' } }]
                },

                elements: { point: { radius: 3, borderWidth: 0, hoverRadius: 8,  hoverBorderWidth: 2 } }
            }
        };

        var chart = new Chart($('#chart_activity_by_hour_today'), config);

    });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });

    }

   

    return {

        init: function() {

            appId = $('#app-id').val();

            loadAverageInteractionsPerHour();

            loadAverageInteractionsPerDay();

            loadAverageDaysInMonth();
            
            loadAverageDaysInMonthInThisYear();

        }

    };

}();

jQuery(document).ready(function() { AppStatsTouchScreens.init(); });