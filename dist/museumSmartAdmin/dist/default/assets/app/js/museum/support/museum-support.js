/**
 * @summary Support.
 *
 * @description - Support
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */


/**
* @function MuseumSupport
* @description Initialize and include all the methods of this class.
*/

var MuseumSupport = function() {

	helper = Helper();

	var v;

	var form = $('#report-form');

	/**
	* @function addFormValidations
	* @description Asign all the form validations for this page.
	*/

	var addFormValidations = function() {

		$.validator.setDefaults({

			highlight: function(element){

				$(element)

				.closest('.form-group')

				.addClass('has-danger');

			},

			unhighlight: function(element){

				$(element)

				.closest('.form-group')

				.removeClass('has-danger');

			},

			errorPlacement: function(error, element){

				if(element.prop('type')==='textarea'){

					error.insertAfter(element.next());

				}else{

					error.insertAfter(element);

				}

			}

		})

		form.validate({

			rules: {

				'report': {required: true}

			},
			messages: {

				'report': helper.createErrorLabel('mensaje', 'REQUIRED')
			},

			invalidHandler: function(e, r) {

				$("#error_msg").removeClass("m--hide").show();

				window.scroll({top: 0, left: 0, behavior: 'smooth'});
			},

		});

	}

	/**
	* @function addListeners
	* @description Asign all the listeners.
	*/

	var addListeners = function(){

		/*autosize for the textareas */

       autosize(document.querySelector('textarea'));

		$('#back').click(function(e) { window.location.replace("index.php"); });

		$('#send-report').click(function(e) {

			e.preventDefault();

			var btn = $(this);

			if (!form.valid()) { return; }

			var subjectString = $('#subjects').find(":selected").text();

			var subject = $('#subjects option:selected').val();

			var report = $('#report').val();
			
			var reporter = $('#user').val();

			var reporterId = $('#user-id').val();

			sendReport(subject, report, reporter, reporterId, subjectString);

		});

	}

	/**
    * @function sendReport
    * @description Send report to check.
    *
    * @param {string} subject         - Subject.
    * @param {string} report          - The user report.
    * @param {string} reporter        - Who is reporting?
    */

    var sendReport = function(subject, report, reporter, reporterId, subjectString){

    	console.log(subject, report, reporter, reporterId, subjectString);

    	//return;

    	helper.blockStage("Enviando mensaje...");

    	var request = $.ajax({

    		url: "private/users/museum/support/support_add.php",

    		type: "POST",

    		data: {subject:subject, report:report, reporter:reporter, reporterId:reporterId, 

    			subjectString:subjectString},

    		dataType: "json"

    	});

    	request.done(function(result) {

    		console.log(result);

    		helper.unblockStage();

    		swal({

    			title: result.title,

    			allowOutsideClick: false,

    			html: result.msg,

    			type: result.alert,

    			confirmButtonText: result.button

    		}).then((swalResult) => {

    			window.location.replace("index.php");

    		});

    	});

    	request.fail(function(jqXHR, textStatus) {

    		console.log(jqXHR);

    		console.log("error");

    		helper.unblockStage();


    	});


    }

	return {

    	init: function() {

    		addListeners();

    		addFormValidations();

    	}

    };

}();

jQuery(document).ready(function() { MuseumSupport.init(); });

