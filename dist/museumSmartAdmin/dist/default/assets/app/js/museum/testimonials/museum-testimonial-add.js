/**
 * @summary Add new testimonial.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

 Dropzone.autoDiscover = false;

/**
* @function MuseumTestimonialAdd
* @description Initialize and include all the methods of this class.
*/

var MuseumTestimonialAdd = function() {

    helper = Helper();

    var editMode;

    var imagePath;

    var redirectId;

    var userId;

    var userName;

    var countryId;

    var regionId;

    var cityId;

    var addRedirectId = 0;

    var form = $('#my-awesome-dropzone');

    var regionBloodhound;

    var cityBloodhound;

    var regionPromise;

    var cityPromise;

    /**
    * @function addFormValidations
    * @description Asign all the form validations for this page.
    */

    var addFormValidations = function() {

        form.validate({

            rules: {

                name: {required: true},

                surname: {required: true},

                'checkboxes[]': {

                    required: !0

                },

                'yt-video': {required: true, url:true, youtube:true}

            },
            messages: {

                name: helper.createErrorLabel('nombre', 'REQUIRED'),

                surname: helper.createErrorLabel('apellido', 'REQUIRED'),

                'yt-video':{

                    required: helper.createErrorLabel('link a video', 'REQUIRED'),
                    
                    url: helper.createErrorLabel('link a video', 'INVALID_URL'),

                    youtube: helper.createErrorLabel('url', 'YOUTUBE_VIDEO')

                }
            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();

                window.scroll({top: 0, left: 0, behavior: 'smooth'});
            },

        });

    }

    /**
    * @function clearAutocompleteForms
    * @description Clear and reset all the data an typeahead objects.
    * This is useful when changing the country or the region.
    *
    * @param {boolean} clearCountries   -       indicated that the countries data should be clear.
    * @param {boolean} clearRegions     -       indicated that the region data should be clear.
    * @param {boolean} clearCities      -       indicates that he cities data should be clear.
    */

    var clearAutocompleteForms = function(clearCountries, clearRegions, clearCities){

        if(clearCountries){

            $("#regions-list").find("*").prop("disabled", true);

            $('#country-input').val('');

        }

        if(clearRegions){

            regionPromise = null;

            $('#region-input').val('');

            $('#hidden-region-id').val('');

            if(regionBloodhound){

                regionBloodhound.clear();

                regionBloodhound = null;

                $("#regions-list .typeahead").typeahead('destroy').off("typeahead:render").off("typeahead:selected").off("typeahead:focus");

            }

        }

        if(clearCities){

            cityPromise = null;

            $('#city-input').val('');

            $('#hidden-city-id').val('');
            
            if(cityBloodhound){

                cityBloodhound.clear();

                cityBloodhound = null;

                $("#cities-list .typeahead").typeahead('destroy');

                $("#cities-list .typeahead").typeahead('destroy').off("typeahead:render").off("typeahead:selected").off("typeahead:focus");

            }

            if(clearRegions) $("#cities-list").find("*").prop("disabled", true);

        }

    }

    /**
    * @function addListeners
    * @description Asign all the listeners.
    */

    var addListeners = function(){

        $('#youtube_id').on('input', function() {

           if($(this).valid()){

                $('#preview-1, #preview-2, #preview-3').empty();

                $('.yt-info').html('Video válido! :)');
                
                $('.yt-info').addClass('text-success');

                 var videoId = helper.getYoutubeVideoId($(this).val());

                 $('#hidden-yt-id').val(videoId);

                 var video_thumbnail1 = $('<img src="//img.youtube.com/vi/'+videoId+'/mqdefault.jpg" width=134px>');
                 
                 var video_thumbnail2 = $('<img src="//img.youtube.com/vi/'+videoId+'/mqdefault.jpg" width=134px>');
                 
                 var video_thumbnail3 = $('<img src="//img.youtube.com/vi/'+videoId+'/mqdefault.jpg" width=134px>');

                 $('#preview-1').append(video_thumbnail1);

                 $('#preview-2').append(video_thumbnail2);

                 $('#preview-3').append(video_thumbnail3);

            }

        });


      
        $("#yt-video").on('paste', function() {

           /* if($("#yt-video").valid()){

                console.log("entra?");

                $('#preview-1, preview-2, preview-3').remove();

                var videoId = helper.getYoutubeVideoId($(this).val());

                 var video_thumbnail1 = $('<img src="//img.youtube.com/vi/'+videoId+'/1.jpg">');

                 var video_thumbnail2 = $('<img src="//img.youtube.com/vi/'+videoId+'/2.jpg">');
                 
                 var video_thumbnail3 = $('<img src="//img.youtube.com/vi/'+videoId+'/3.jpg">');

                 $('#preview-1').append(video_thumbnail1);

                 $('#preview-2').append(video_thumbnail2);

                 $('#preview-3').append(video_thumbnail3);

            }*/

        }); 

        $("#yt-video").blur(function(){

            /*if($(this).valid()){

                var videoId = helper.getYoutubeVideoId($(this).val());

                console.log('<img src="//img.youtube.com/vi/'+videoId+'/0.jpg">');

                 var video_thumbnail1 = $('<img src="//img.youtube.com/vi/'+videoId+'/1.jpg">');

                 var video_thumbnail2 = $('<img src="//img.youtube.com/vi/'+videoId+'/2.jpg">');
                 
                 var video_thumbnail3 = $('<img src="//img.youtube.com/vi/'+videoId+'/3.jpg">');

                 $('#preview-1').append(video_thumbnail1);

                 $('#preview-2').append(video_thumbnail2);

                 $('#preview-3').append(video_thumbnail3);

            
            }*/


        });

        helper.addyoutubeValidationMethod($.validator);

        $("#birthdate-picker").datepicker({

            autoclose:true, format: 'yyyy-mm-dd', clearBtn: true, language:'es'

        }).on('changeDate', function(e) {

            if(editMode){

                var fieldName = 'birthdate';

                var newValue = $("#birthdate-picker").data('datepicker').getFormattedDate('yyyy-mm-dd');

                console.log("newValue: " + newValue);

                if(newValue == ""){
                    
                    newValue = "0000-00-00";
                }

                updateValue(userId, fieldName, newValue, 'museum_testimonials');
            }

        });

        window.onbeforeunload = function () { window.scrollTo(0, 0); }

        $(document).on("keypress", "form", function(event){ return event.keyCode != 13; });

        $('#exit_from_form').click(function(e) { window.location.replace("index.php"); });
        
        $('#back_to_list').click(function(e) { window.location.replace("museum_testimonials_list.php?tabId=" + redirectId); });

        t = new Bloodhound({

            datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

            queryTokenizer: Bloodhound.tokenizers.whitespace,

            prefetch:{

                url : "private/users/services/get_list_of_countries.php",

                cache: false
            }

        })

        $("#countries-list .typeahead").typeahead(null, {

            hint: false,

            highlight: false,

            minLength: 1,

            name: "best-pictures",

            display: "value",

            source: t,

            autoselect: true,

            templates: {

                suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")

            }

        }).on("typeahead:render", function() {

            var searchTerm = $(this).val();

            $('#countries-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

        }).on('typeahead:selected', function (e, datum) {

            $('#hidden-country-id').val(datum.id);

            helper.blockStage("Cargando provincias de " + datum.value + "...");

            if(!editMode){

                clearAutocompleteForms(false, true, true);

                loadRegionData(datum.id);

            }else{

                var locationId = $(this).attr('data-db-reference');

                //console.log(locationId);

                //return;

                clearAutocompleteForms(false, true, true);

                helper.blockStage("actualizando...");

                updateValue(locationId, 

                    ['country_id', 'region_id', 'city_id'], 

                    [datum.id, null, null], 

                    'world_location_content', loadRegionData, [datum.id]);
            }

        }).on('focus', this, function(event){

            if(editMode) $('#hidden-country-id').val("");

        });

        $('.m-checkbox-list :checkbox').change(function(){

            $(".m-checkbox-list :checkbox").each(function(){

                if($(this).prop('checked')){

                    addRedirectId = $(this).attr('value');

                    return;
                }
            });

            if(!editMode) return;

            if($('.m-checkbox-list input[type=checkbox]:checked').length==0) {

                $(this).prop('checked', true);

                swal({

                    title: "opss!",

                    allowOutsideClick: false,

                    html: "Cada testimonio debe pertener al menos a una categoría. Al menos una de las opciones debe quedar activa.<br><br>Si lo que queres es eliminar un testimonio o pasarlo a offline, podés hacerlo el <a href='museum_testimonials_list.php'>siguiente link</a>.",

                    type: "warning",

                    confirmButtonText: "Entendido!"

                })

            }

            var fieldName = $(this).attr('name');

            var sid = $(this).attr('value');

            var action = ($(this).is(":checked")) ? 1 : 0;

            updateCheckboxesValue(

                userId, 

                'id_museum_testimonial',

                sid, 

                action, 

                'id_museum_testimonial_category', 

                'museum_testimonials_categories_relations'
            );

        });

        $('.toggler-info:checkbox').change(function() {

            if(!editMode) return;

            var fieldName = $(this).attr('name');

            var newValue = ($(this).is(":checked")) ? 1 : 0;

            updateValue(userId, fieldName, newValue, 'museum_testimonials')

        }); 

        $('.upate-for-typeahead').click(function(){

            var dbName = $(this).attr('data-db-value');

            var id = $(this).attr('data-id');

            var idValue = $('#hidden-country-id').val();

            var newValue = $('input.typeahead.tt-input').val();

            if(newValue != dbName) updateValue(id, idValue, newValue, 'int', 2, 'update');

        })

        $(".delete-btn").click(function(){

            var locationId = $(this).attr('data-db-reference');

            switch($(this).attr('name')){

                case 'country':

                clearAutocompleteForms(true, true, true);

                updateValue(locationId, ['country_id', 'region_id', 'city_id'],  [null, null, null], 'world_location_content');

                break;

                case 'region':

                $('#region-input').val('');

                $("#cities-list").find("*").prop("disabled", true);

                clearAutocompleteForms(false, false, true);

                updateValue(locationId, ['region_id', 'city_id'],  [null, null], 'world_location_content');

                break;

                case 'city':

                $('#city-input').val('');

                console.log(">>" + locationId);

                updateValue(locationId, 'city_id', 'null', 'world_location_content');

                break;


            }

            return;


        })


        $(".update-btn").click(function(){

            if(!editMode) return;

            var dbName = $(this).attr('data-db-value');

            var id = $(this).attr('data-id');

            var newValue = $(this).parent().parent().find(':input').val();

            var fieldName = $(this).parent().parent().find(':input').attr('name');

            var valid = $('#' + fieldName).valid();

            if(dbName != newValue && valid){

                $(this).attr('data-db-value', newValue);

                if(fieldName == 'name' || fieldName == 'surname' || fieldName == 'quote'){

                    updateValue(id, fieldName, newValue, 'museum_testimonials');

                }else if(fieldName == 'youtube_id'){

                    var newValue = $('#hidden-yt-id').val();
                
                    updateValue(id, fieldName, newValue, 'museum_youtube_videos');

                    
                }
            }

        });

    }

    var onResetCountry = function(){
        
        console.log("ok...nos vamos");

    }

    /**
    * @function loadRegionData
    * @description create a new BloodHound and a new typeahead according o the country id.
    *
    * @param {int} id                 - object id.
    */

    var loadRegionData = function(id){

        console.log("load region");

        $("#regions-list").find("*").prop("disabled", false);

        regionBloodhound = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

            queryTokenizer: Bloodhound.tokenizers.whitespace,

            prefetch:{ url : "private/users/services/get_list_of_regions.php?id=" + id, cache: false }

        })

        regionPromise = regionBloodhound.initialize();

        regionPromise.done(function() { 

            $("#regions-list .typeahead").typeahead(null, { hint: false, highlight: false, minLength: 0, 

                name: "best-pictures", display: "value", source: regionBloodhound, autoselect: true,

                templates: {

                    suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")

                }

            }).on("typeahead:render", function() {

                var searchTerm = $(this).val();

                $('#regions-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

            }).on('typeahead:selected', function (e, datum) {

                $('#hidden-region-id').val(datum.id);

                helper.blockStage("Cargando ciudades de " + datum.value + "...");

                if(!editMode){

                    clearAutocompleteForms(false, false, true);

                    loadCityData(datum.id);

                }else{

                    var locationId = $(this).attr('data-db-reference');

                    clearAutocompleteForms(false, false, true);

                    updateValue(locationId, 

                        ['region_id', 'city_id'], 

                        [datum.id, null], 

                        'world_location_content', loadCityData, [datum.id]);
                }

            });

            helper.unblockStage();

        });

    }

    /**
    * @function loadCityData
    * @description create a new BloodHound and a new typeahead according to the region id.
    *
    * @param {int} id                 - object id.
    */

    var loadCityData = function(id){

        $("#cities-list").find("*").prop("disabled", false);

        cityBloodhound = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

            queryTokenizer: Bloodhound.tokenizers.whitespace,

            prefetch:{ url : "private/users/services/get_list_of_cities.php?id=" + id, cache: false }

        })

        var promise = cityBloodhound.initialize();

        promise.done(function() { 

            $("#cities-list .typeahead").typeahead(null, { hint: false, highlight: false, minLength: 0, limit:10, 

                name: "best-pictures", display: "value", source: cityBloodhound, autoselect: true,

                templates: {

                    suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")

                }

            }).on("typeahead:render", function() {

                var searchTerm = $(this).val();

                $('#cities-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

            }).on('typeahead:selected', function (e, datum) {

                if(!editMode){

                    $('#hidden-city-id').val(datum.id);

                }else{

                    var locationId = $(this).attr('data-db-reference');

                    updateValue(locationId, 'city_id', datum.id, 'world_location_content');

                }

            });

            console.log("unblock!");

            helper.unblockStage();

        })

    }

    var updateCheckboxesValue = function(id, idName, sid, action, newValueName, filter){

        console.log("id: " + id + " - idName: " + idName + " sid: " + sid + " action: " + action + " newValueName: " + newValueName + " filter: " + filter);

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/update_orderable_checkboxes.php",

            type: "POST",

            data: {id:id, idName:idName, sid:sid, action:action, filter:filter, newValueName:newValueName},

            dataType: "json"

        });

        request.done(function(result) {

            helper.showToastr(result.title, result.msg);

        });

        request.fail(function(jqXHR, textStatus) {

            console.log("error");

            console.log(jqXHR);

            
        });

        helper.unblockStage();

        localStorage.setItem('refresh', 'true');
    }

     /**
    * @function updateValue
    * @description update individual value in DB.
    *
    * @param {int} id                 - object id.
    * @param {string} fieldName       - the field to modify
    * @param {string} newValue        - object value.
    * @param {int} filte              - filter type.
    */

    var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined){

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/update_value.php",

            type: "POST",

            data: {id:id, fieldName:fieldName, newValue:newValue, filter:filter},

            dataType: "json"

        });

        request.done(function(result) {

            helper.showToastr(result.title, result.msg);

            if(callback != undefined) callback(callbackParams);

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

            
        });

        if(callback == undefined) helper.unblockStage();

        localStorage.setItem('refresh', 'true');

    }

    var deleteImage = function(id, file){

        console.log("delete???");

        helper.blockStage("Eliminando imagen...");

        var request = $.ajax({

            url: "private/users/museum/images_general/delete_image.php",

            type: "POST",

            data: {id:id, file:file, source:'testimonials', userName:userName},

            dataType: "json"

        });

        request.done(function(result) {

            helper.unblockStage();

            helper.showToastr(result.title, result.msg);

            myDropzone.removeAllFiles();

            myDropzone.options.maxFiles = 1;

            myDropzone.setupEventListeners();

            //myDropzone.files = [];


        });

        request.fail(function(jqXHR, textStatus) {


        });

        helper.unblockStage();


    }

    /**
    * @function handleDropZone
    * @description manage all in relation woth DropZoneJS for images.
    */

    var handleDropZone = function(){

        var urlPath = (editMode) ? 'private/users/museum/images_general/add_image.php' : 'private/users/museum/testimonials/testimonial_add.php';

        var autoProcessQueue = (editMode) ? true : false;

        myDropzone = new Dropzone("#my-awesome-dropzone", {

            paramName: 'file',

            url: urlPath,

            clickable: true,

            acceptedFiles: 'image/*',

            autoProcessQueue: autoProcessQueue,

            maxFilesize: 2000,

            uploadMultiple: false,

            addRemoveLinks: true,//(editMode) ? true : false,

            dictRemoveFile: 'eliminar archivo',

            maxFiles: 1,

            thumbnailWidth: 350,

            thumbnailHeight:350,

            thumbnailMethod: 'contain',

            previewsContainer: '.dropzone-previews',

            dictRemoveFileConfirmation: '¿Eliminar imagen de perfil?',

            init: function() {

                var _this = this;

                var myDropzone = this;

                this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {

                    e.preventDefault();

                    e.stopPropagation();

                    if (!form.valid()) { return; }

                    $('#add_testomony').addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                    helper.blockStage("actualizando datos...");

                    if (form.valid() == true) { 

                        if (myDropzone.getQueuedFiles().length > 0) {  

                            myDropzone.processQueue();  

                        } else {  

                           var blob = new Blob();

                           blob.upload = { 'chunked': myDropzone.defaultOptions.chunking };

                           myDropzone.uploadFile(blob);

                       }                                    
                   }            

               });

                this.on("maxfilesexceeded", function(file){

                });

                this.on("complete", function(file){

                    console.log(file);

                });

                this.on("removedfile", function(file) {  deleteImage(userId, file.name); });


                this.on("error", function(file) { 

                    console.log(file);
                });

                this.on("sending", function(file, xhr, formData) {

                    if(editMode){

                        formData.append("id", userId);
                        
                        formData.append("userName", userName);
                        
                        formData.append("source", "testimonials");

                    }

                });

                this.on("success", function(file, data) {

                    console.log(file, data);

                    var response = JSON.parse(file.xhr.responseText);

                    console.log(response);

                    myDropzone.removeEventListeners();

                    helper.unblockStage();

                    $('#add_testomony').removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch(Number(response.status)){

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {



                            switch(Number(response.status)){

                               case 1:

                               window.location.replace("museum_testimonials_list.php?tabId=" + addRedirectId + "&highlight=true");

                               break;

                           }

                       }

                   });

                });
            }

        });

if(editMode && imagePath != ""){

    var mockFile = { name: imagePath, size: 5668, width:200, height:200 };

    myDropzone.emit("addedfile", mockFile);

    myDropzone.emit("thumbnail", mockFile, imagePath);

    myDropzone.emit("complete", mockFile);

    var existingFileCount = 1;

    myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;

    myDropzone.removeEventListeners();

}

Dropzone.confirm = function(question, accepted, rejected) {

    Swal({

        title: question,

        html: 'Si eliminas la imagen, mostraremos un avatar a forma de reemplazo.',

        type: 'warning',

        showCancelButton: true,

        confirmButtonText: 'Entendido! Eliminar',

        cancelButtonText: 'salir'

    }).then((result) => {

        if (result.value) accepted();

    })

};


}

return {

    init: function() {

        editMode = ($('#edit-mode').val() == 1) ? true : false;

        $("#regions-list, #cities-list").find("*").prop("disabled", true);

        if(editMode){

            var temp;

            if(document.referrer != ""){

                temp = helper.getGETVariables(document.referrer, 'tabId');

            }else{

                temp = null;
            }

            redirectId = (temp == null) ? 1 : temp; 

            imagePath = $('#image-path').val();

            userId = $('#testimonial-id').val();

            userName = $('#testimonial-name').val();

            countryId = $('#hidden-country-id').val();

            regionId = $('#hidden-region-id').val();

            if(countryId != 0) loadRegionData(countryId);

            if(regionId != 0) loadCityData(regionId);
        }

        addListeners();

        addFormValidations();

        handleDropZone();



    }

};

}();

jQuery(document).ready(function() { MuseumTestimonialAdd.init(); });

