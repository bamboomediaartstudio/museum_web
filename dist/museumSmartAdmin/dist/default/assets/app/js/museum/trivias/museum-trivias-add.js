/**
 * @summary Add trivias
 *
 * @description - Add trivias
 *
 * @author Colo baggins <colorado@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */


/**
 * @function MuseumTriviasAdd
 * @description Initialize and include all the methods of this class.
 */

 var MuseumTriviasAdd = function() {

    helper = Helper();

    var editMode;

    var redirectId;

    var triviaId;

    var factDescription;

    var addRedirectId = 0;

    var isFirstLoad = true;

    var myElement;

    var myReply;

    var form = $('#add-trivia-data-form');


    /**
     * @function addFormValidations
     * @description Asign all the form validations for this page.
     */

     var addFormValidations = function() {

        jQuery.validator.setDefaults({

            ignore: ":hidden, [contenteditable='true']:not([name])"

        });


        form.validate({

            rules: {

                trivia: {
                    required: true
                },

                'checkboxes[]': {
                    required: !0
                },

                'checkboxesFeedback[]': {
                    required: !0
                }


            },
            messages: {

                trivia: helper.createErrorLabel('trivia', 'REQUIRED'),

                'checkboxes[]': helper.createErrorLabel('categoría', 'AT_LEAST_ONE'),

                'checkboxesFeedback[]': helper.createErrorLabel('Feedback', 'AT_LEAST_ONE')

            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();

                if ($('#trivia').summernote('isEmpty'))
                {

                    $('.summernote-description-error').removeClass('d-none');

                }

                window.scroll({top: 0, left: 0, behavior: 'smooth' });
            },

            submitHandler: function(form, event) {
             
              if ($('#trivia').summernote('isEmpty')){

                $('.summernote-description-error').removeClass('d-none');

                    window.scroll({top: 0, left: 0, behavior: 'smooth' });

                    return;
              }

                event.preventDefault();

                event.stopPropagation();

                helper.blockStage("actualizando datos...");

                var formData = new FormData(form);

                var request = $.ajax({

                    url: "private/users/museum/trivias/trivias_add.php",

                    type: "POST",

                    contentType: false,

                    processData: false,

                    data: formData,

                    dataType: "json"
                });

                request.done(function(response) {

                  /*console.log("response antes de swal y toastr");

                  console.log(response);
                  
                  console.log("last id: " + response.lastId);*/

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch (Number(response.status)) {

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                //museum_trivias_add.php?id=42&tab=reply-tab

                                 window.location.replace("museum_trivias_add.php?id=" + response.lastId + "&tab=reply-tab");

                                //location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {

                            switch (Number(response.status)) {

                                case 1:

                                window.location.replace("museum_trivias_list.php");

                                break;

                            }

                        }

                    });


                    helper.showToastr("Se acualizo", 'Se actualizo');

                    helper.unblockStage();

                });

                request.fail(function(jqXHR, textStatus) {

                    console.log(jqXHR);

                    console.log("error");

                    helper.showToastr("ops", 'error');

                    helper.unblockStage();

                });

            }

        });
      }



    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

     var addListeners = function() {

       $("[name='my-checkbox']").bootstrapSwitch();

       $('[data-toggle="tooltip"]').tooltip();

       //Tab Navigation
       $('.tab-navigation').click(function(){

           setQueryStringParameter('tab', $(this).attr('href').substring(1));

       });

       //Description Trivia

       //Description
       myElement = $('#trivia').summernote({

           disableDragAndDrop: true,

           fontNames : ['CooperHewitt-Medium', 'Arial', 'sans-serif'],

           height: 200,

           toolbar: [

           ['style', ['bold', 'italic', 'underline']],

           ['Misc', ['fullscreen', 'undo', 'redo']]

           ],

           callbacks: {

               onInit: function() { },

               onEnter: function() { },

               onKeyup: function(e) {

                   if($(this).attr('name') == 'trivia'){

                       summernoteDescriptionCounter = e.currentTarget.innerText;

                       if(summernoteDescriptionCounter !=0){

                           $('.summernote-description-error').addClass('d-none');

                       }else{

                           $('.summernote-description-error').removeClass('d-none');

                       }

                   }

               },

               onKeydown: function(e) { },

               onPaste: function(e) { },

               onChange: function(contents, $editable) { },

               onBlur: function(){ },

               onFocus: function() { }

           }

       });

       myReply = $('#reply').summernote({

           disableDragAndDrop: true,

           fontNames : ['CooperHewitt-Medium', 'Arial', 'sans-serif'],

           height: 200,

           toolbar: [

           ['style', ['bold', 'italic', 'underline']],

           ['Misc', ['fullscreen', 'undo', 'redo']]

           ],

           callbacks: {

               onInit: function() { },

               onEnter: function() { },

               onKeyup: function(e) {

                   if($(this).attr('name') == 'description'){

                       summernoteDescriptionCounter = e.currentTarget.innerText;

                       if(summernoteDescriptionCounter !=0){

                           $('.summernote-description-error').addClass('d-none');

                       }else{

                           $('.summernote-description-error').removeClass('d-none');

                       }

                   }

               },

               onKeydown: function(e) { },

               onPaste: function(e) { },

               onChange: function(contents, $editable) { },

               onBlur: function(){ },

               onFocus: function() { }

           }

       });


       $('#exit_from_form').click(function(e) {
           window.location.replace("index.php");
       });

       $('#back_to_list').click(function(e) {
           window.location.replace("museum_trivias_list.php?tabId=" + redirectId);
       });

       $(".update-btn").click(function() {
         console.log("UPDATE FIELD");

           if (!editMode) return;

           var dbName = $(this).attr('data-db-value');

           var id = $(this).attr('data-id');

           var newValue = $(this).parent().parent().find(':input').val();

           var fieldName = $(this).parent().parent().find(':input').attr('name');

           console.log("dbName: "+dbName);
           console.log("id: "+id);
           console.log("newValue: "+newValue);
           console.log("fieldName: "+fieldName);


           var categoriesChecked = $('input[name="checkboxes[]"]:checked');

           var arrCategories = [];

           if(dbName == "categoriesFlag"){

             console.log("categorias actualizar!!");


             var categoriesChecked = $('input[name="checkboxes[]"]:checked');


             var arrCategories = [];

             if(categoriesChecked.length > 0){

               categoriesChecked.each(function(){

                //Put id of checked categories into array
                //console.log($(this).val());

                arrCategories.push($(this).val());

              });


              //Valid at least one category checked

              var valid = $('input[name="checkboxes[]').valid();

              if(valid){

                  updateCheckboxesService(arrCategories, 'updateCategories');

              }

            }else{

              //Ninguna categoria fue seleccionada o  se deseleccionaron todas las categorias
              swal({
<<<<<<< HEAD

                  title: "Actualizar categorias",

                  allowOutsideClick: false,

                  html: "Al menos una categoria debe ser seleccionada",

                  type: "info",

                  showCancelButton: false,

                  confirmButtonText: "Entendido"

              }).then((result) => {

                  location.reload();

              });
=======
>>>>>>> e0b157755a5f16e0c10cd2dba9a0139208e88f34

                  title: "Actualizar categorias",

                  allowOutsideClick: false,

<<<<<<< HEAD
          }else{

            var valid = $('#' + fieldName).valid();

            if (dbName != newValue && valid) {

                console.log("changing something! Go to Update");

                $(this).attr('data-db-value', newValue);

                updateValue(id, fieldName, newValue, 'museum_trivias');
=======
                  html: "Al menos una categoria debe ser seleccionada",

                  type: "info",

                  showCancelButton: false,

                  confirmButtonText: "Entendido"

              }).then((result) => {

                  location.reload();

              });
>>>>>>> e0b157755a5f16e0c10cd2dba9a0139208e88f34

            }else{
              console.log("nothing changed, nothing happens");
            }

<<<<<<< HEAD
          }

       });



       //Switch cuando estamos en ediy mode, pestaña de respuestas
       $('#replies').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

         var switchId = $(this).attr('id');

         console.log("state: "+state);

         var newState = (state=='true') ? 1 : 0;

         //   updateValue(id, fieldName, newValue, 'museum_trivias');
         console.log("Actualizar correcta/incorrecta - antes checkear que sea el único checkbox checked");

         console.log("changed STATE: "+state);

         var actualCheckedId;

         if(state == 'false'){
           //NO SE PUEDE CAMBIAR ESTADO. PARTIMOS DE QUE SOLO 1 VA A ESTAR CORRECTO NO ES NECESARIO CHEQUEAR NADA.
         }else{
           //Cambiar el que esta en true a false, actualizarlo y poner el seleccionado como correcto
           //get id checked checkbox

           $('input[name="my-checkbox"]').each(function(){

              var actualCheckbox = $(this);

              if(actualCheckbox.prop('checked')){

                actualCheckedId = actualCheckbox.attr("id");

                //correct 0 la respuesta anteriormente checkeada.

                updateValue(actualCheckedId, "correct", 0, 'museum_trivias_custom_replies');

              //  $($(this)).bootstrapSwitch('toggleState', true, true);

                //$($(this)).bootstrapSwitch('state', false);

              }else{
                console.log("NO habia nada checked");
              }

           });


           console.log("Checkbox ya checked: id_: "+actualCheckedId);

         }

         //correct 1 la respuesta actualmente checkeada

         updateValue(switchId, "correct", 1, 'museum_trivias_custom_replies');

         location.reload(true);

       });


       $('.add-new-reply').click(function(){

        if ($('#reply').summernote('isEmpty')){

          Swal({ 

            type: "error",

            confirmButtonText: "cerrar",
            
            title: "opss! ¿y la pregunta?",

            html: "este campo no puede estar vacío. Es una de las posibles respuestas a la trivia que estás editando"

          });

          return;

        }

        helper.blockStage("agregando respuesta...");

         var reply = $("#reply");

         if(reply.val() != ''){

           console.log("POdemos agregar la respuesta");

           var request = $.ajax({

                 url: "private/users/museum/trivias/add_reply.php",

                 type: "POST",

                 data: {

                     id: triviaId,

                     reply: reply.val()
                 },

                 dataType: "json"
             });

             request.done(function(response) {

               console.log("response antes de swal y toastr");

               console.log(response);

               swal({

                    title: response.title,

                     allowOutsideClick: false,

                     html: response.msg,

                     type: response.alert,

                     showCancelButton: true,

                     confirmButtonText: response.button,

                     cancelButtonText: 'salir'

                 }).then((result) => {

                  if(result.value == true){

                    location.reload();

                  } else{

                    window.location.replace("museum_trivias_list.php");

                  }

                     
=======
          }else{

            var valid = $('#' + fieldName).valid();

            if (dbName != newValue && valid) {

                console.log("changing something! Go to Update");

                $(this).attr('data-db-value', newValue);

                updateValue(id, fieldName, newValue, 'museum_trivias');

            }else{
              console.log("nothing changed, nothing happens");
            }

          }

       });



       //Switch cuando estamos en ediy mode, pestaña de respuestas
       $('#replies').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

         var switchId = $(this).attr('id');

         console.log("state: "+state);

         var newState = (state=='true') ? 1 : 0;

         //   updateValue(id, fieldName, newValue, 'museum_trivias');
         console.log("Actualizar correcta/incorrecta - antes checkear que sea el único checkbox checked");

         console.log("changed STATE: "+state);

         var actualCheckedId;

         if(state == 'false'){
           //NO SE PUEDE CAMBIAR ESTADO. PARTIMOS DE QUE SOLO 1 VA A ESTAR CORRECTO NO ES NECESARIO CHEQUEAR NADA.
         }else{
           //Cambiar el que esta en true a false, actualizarlo y poner el seleccionado como correcto
           //get id checked checkbox

           $('input[name="my-checkbox"]').each(function(){

              var actualCheckbox = $(this);

              if(actualCheckbox.prop('checked')){

                actualCheckedId = actualCheckbox.attr("id");

                //correct 0 la respuesta anteriormente checkeada.

                updateValue(actualCheckedId, "correct", 0, 'museum_trivias_custom_replies');

              //  $($(this)).bootstrapSwitch('toggleState', true, true);

                //$($(this)).bootstrapSwitch('state', false);

              }else{
                console.log("NO habia nada checked");
              }

           });


           console.log("Checkbox ya checked: id_: "+actualCheckedId);

         }

         //correct 1 la respuesta actualmente checkeada

         updateValue(switchId, "correct", 1, 'museum_trivias_custom_replies');

         location.reload();
>>>>>>> e0b157755a5f16e0c10cd2dba9a0139208e88f34

                 });


<<<<<<< HEAD
                 helper.showToastr("Se añadió una nueva respuesta!", ':)');

                 helper.unblockStage();

             });

             request.fail(function(jqXHR, textStatus) {

                 console.log(jqXHR);

                 console.log("error");

                 helper.showToastr("ops", 'error');

                 helper.unblockStage();

             });

=======
       $('.add-new-reply').click(function(){

         var reply = $("#reply");

         if(reply.val() != ''){

           console.log("POdemos agregar la respuesta");

           var request = $.ajax({

                 url: "private/users/museum/trivias/add_reply.php",

                 type: "POST",

                 data: {

                     id: triviaId,

                     reply: reply.val()
                 },

                 dataType: "json"
             });

             request.done(function(response) {

               console.log("response antes de swal y toastr");

               console.log(response);

                 swal({

                     title: response.title,

                     allowOutsideClick: false,

                     html: response.msg,

                     type: response.alert,

                     showCancelButton: (response.status == 1) ? true : false,

                     confirmButtonText: response.button,

                     cancelButtonText: (response.status == 1) ? 'Listado de trivias' : ''

                 }).then((result) => {

                     location.reload();

                 });


                 helper.showToastr("Se acualizo", 'Se actualizo');

                 helper.unblockStage();

             });

             request.fail(function(jqXHR, textStatus) {

                 console.log(jqXHR);

                 console.log("error");

                 helper.showToastr("ops", 'error');

                 helper.unblockStage();

             });

>>>>>>> e0b157755a5f16e0c10cd2dba9a0139208e88f34
         }


       });



     }// /.addListeners


     var createDataTable = function(){
<<<<<<< HEAD

         var table = $('table.display').DataTable({
         //var table = $('#replies').DataTable({

             pageLength: 50,

             "language": helper.getDataTableLanguageConfig(),

             //rowReorder: false,

             //rowReorder: { update: true },

             "ordering": false,

             bAutoWidth: false,

             processing:true,

             "columns": [

             { "width": "5%", responsivePriority: 4, orderable: true, targets: 0},

             { "visible": false, orderable:false },

             { "width": "60%", responsivePriority: 0, orderable: false, targets: '_all' },

             { "width": "5%", responsivePriority: 2, orderable: false, targets: '_all' },

             { "width": "5%", responsivePriority: 2, orderable: false, targets: '_all' }

             ]

         });




         $('tbody').on( 'click', '.delete_row_reply', function () {

             var row = table.row($(this).parents('tr'));

=======

         var table = $('table.display').DataTable({
         //var table = $('#replies').DataTable({

             pageLength: 50,

             "language": helper.getDataTableLanguageConfig(),

             //rowReorder: false,

             //rowReorder: { update: true },

             "ordering": false,

             bAutoWidth: false,

             processing:true,

             "columns": [

             { "width": "5%", responsivePriority: 4, orderable: true, targets: 0},

             { "visible": false, orderable:false },

             { "width": "20%", responsivePriority: 0, orderable: false, targets: '_all' },

             { "width": "5%", responsivePriority: 2, orderable: false, targets: '_all' },

             { "width": "5%", responsivePriority: 2, orderable: false, targets: '_all' }

             ]

         });




         $('tbody').on( 'click', '.delete_row_reply', function () {

             var row = table.row($(this).parents('tr'));

>>>>>>> e0b157755a5f16e0c10cd2dba9a0139208e88f34
             var animatable = $(this).parents('tr');

             var data = table.row( $(this).parents('tr') ).data();

             var order = data[0];

             var id = data[1];

             var actualTable = $("#replies").DataTable();

             Swal({

                 title: "Eliminar Respuesta",

                 html: 'Esta acción eliminará la respuesta seleccionada.<br><br>Esta acción no se puede deshacer. <br><br>¿Eliminar de todos modos?',

                 type: 'error',

                 showCancelButton: true,

                 confirmButtonText: 'Si, eliminar',

                 cancelButtonText: 'no, salir'

             }).then((result) => {

                 if(result.value == true) updateUserStatus(id, 'true', name, 'delete', animatable, row, actualTable, null,  'deleted');


             })

         });

     }


     /**
      * @function updateValue
      * @description update individual value in DB.
      *
      * @param {int} id                 - object id.
      * @param {string} fieldName       - the field to modify
      * @param {string} newValue        - object value.
      * @param {int} filter             - filter type.
      */

      var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {


         console.log("Update - FieldName: "+fieldName + " - newValue: "+newValue + " - table: "+filter);

         console.log("update value...only once?");

         helper.blockStage("actualizando...");



         var request = $.ajax({

             url: "private/users/museum/general/update_value.php",

             type: "POST",

             //async: false,

             data: {

                 id: id,

                 fieldName: fieldName,

                 newValue: newValue,

                 filter: filter
             },

             dataType: "json"

         });

         request.done(function(result) {

             console.log("tracing result!");

             console.log(result);

             helper.showToastr(result.title, result.msg);

             if (callback != undefined) callback(callbackParams);

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

             console.log("error");

         });

         if (callback == undefined) helper.unblockStage();

         localStorage.setItem('refresh', 'true');

     }// /.updateValue


     //updateUserStatus EN ESTE CASO SOLO SE ESTA UTILIZANDO PARA EL DELETE DE LAS RESPUESTAS A TRIVIAS. ESTA FUNCIÓN ES DE USO EN COMUN EN LAS PAGINAS DE "LIST"

     /**
     * @function updateUserStatus
     *
     * @description Update / Delete user status from list: we use this to mark the user as online / offline and
     * for delete an user completely from the list.
     *
     * @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
     * @param {boolean} state                    - True or false: use it for both, update and delete.
     * @param {string} name                  - The name of the person being manipulated.
     * @param {string} action                    - Posibilities: 'update' or 'delete'.
     * @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
     * @param {DataTable} row                    - Reference to the row that we are dealing with.
     * @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
     * @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
     */

     var updateUserStatus = function(id, state, name, action, animatable, row, processTable = null, order = null, defaultColumn = null){

         helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando hecho...');

         //var url = "private/users/museum/general/update_status.php";
        var url= "private/users/museum/trivias/delete_reply.php";

        var hideTd = 0;

         $.ajax({

             type: "POST",

             url: url,

             data: {

                 id: id,

                 idTrivia: triviaId
             },

             success: function(result){

                 console.log(result);
<<<<<<< HEAD

                 helper.unblockStage();

                 if(result.status == 0){

                   swal({

                       title: result.title,

                       allowOutsideClick: false,

                       html: result.msg,

=======

                 helper.unblockStage();

                 if(result.status == 0){

                   swal({

                       title: result.title,

                       allowOutsideClick: false,

                       html: result.msg,

>>>>>>> e0b157755a5f16e0c10cd2dba9a0139208e88f34
                       type: "warning",

                       showCancelButton: (result.status == 1) ? true : false,

                       confirmButtonText: result.button,

                       cancelButtonText: (result.status == 1) ? 'ir a la lista' : ''

                   })

                 }else{


                   animatable.fadeOut('slow','linear',function(){

                       console.log(result.msg);

                       helper.showToastr('ELIMINADO', 'Esta trivia se eliminó exitosamentne.');

                       var count = 0;

                       var data = processTable.rows().data();

                       processTable.rows().every( function (rowIdx, tableLoop, rowLoop) {

                           if(processTable.cell(rowIdx, 0).data() > Number(order)){

                               var saveValue = Number(processTable.cell(rowIdx, 0).data() - 1);

                               processTable.cell(rowIdx, 0).data(saveValue);

                           }

                       });

                       row.remove().draw(true);

                       processTable.rows().invalidate().draw(false);

                   });


                 }




             }, // Ends success

             error: function(xhr, status, error) {

                 console.log(xhr);

                 var err = eval("(" + xhr.responseText + ")");

                 alert(err.Message);
             },

             dataType: "json"
         });

     }

     /**
      * @function updateCategories
      * @param id -
      * @param categoriesTotal - Total categories selected/checked
      * @description if we are in edition mode load all the images for Bootstrap File Input.
      */
     var updateCheckboxesService = function(arrOption, actionUpdate){

       var request = $.ajax({

             url: "private/users/museum/trivias/trivias_service_ajax.php",

             type: "POST",

             data: {

               id: triviaId,

               'arrOptionsChecked[]': arrOption,  ////'arrCategories[]': arrOption,

               action: actionUpdate         // action: 'updateCategories'

             },

             dataType: "json"
         });

         request.done(function(response) {

           console.log(response);

             swal({

                 title: response.title,

                 allowOutsideClick: false,

                 html: response.msg,

                 type: response.alert,

                 showCancelButton: (response.status == 1) ? true : false,

                 confirmButtonText: response.button,

                 cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

             }).then((result) => {

                 location.reload();

             });


             helper.showToastr("Se acualizo", 'Se actualizo');

             helper.unblockStage();

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

             console.log("error");

             helper.showToastr("ops", 'error');

             helper.unblockStage();

         });


     }// Ends updateCategories()

     //For tab navigation
     function setQueryStringParameter(name, value) {

         const params = new URLSearchParams(location.search);

         params.set(name, value);

         window.history.replaceState({}, "", decodeURIComponent(`${location.pathname}?${params}`));

     }


    return {

        init: function() {

            editMode = ($('#edit-mode').val() == 1) ? true : false;

            helper.setMenu();

            if (editMode) {

              console.log("Estamos en edit mode!");

                var temp;

                if (document.referrer != "") {

                    temp = helper.getGETVariables(document.referrer, 'tabId');

                    console.log("temP: "+temp);


                } else {

                    temp = null;
                }

                redirectId = (temp == null) ? 1 : temp;

                triviaId = $('#trivia-id').val();

            } else {

                isFirstLoad = false;
            } 

            addListeners();

            createDataTable();

            addFormValidations();




        } // /.init function

    }; // /.return

 }();

 jQuery(document).ready(function() { MuseumTriviasAdd.init(); });
