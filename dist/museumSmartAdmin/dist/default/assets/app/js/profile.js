var Profile = function() {

    var profile = $('#profile');

    var loginErrors = 0;

    var userEmail;

    var somethingChanged = false;

    var globalScore = 0;

    var doNotShowDeleteAlertAnyMore = false;

    var addMoreImages;

    var initialPreviewCoverConfig = [];

    var initialPreviewCoverPaths = [];

    var userId = $('#user-id').val();

    helper = Helper();

    var showErrorMsg = function(form, type, msg) {

        var alert = $('<div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
           <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
           <span></span>\
           </div>');

        form.find('.alert').remove();

        alert.prependTo(form);
        
        alert.animateClass('fadeIn animated');
        
        alert.find('span').html(msg);
    }

    //== Private Functions

    var displaySignUpForm = function() {

        login.removeClass('m-login--forget-password');
        
        login.removeClass('m-login--signin');

        login.addClass('m-login--signup');
        
        login.find('.m-login__signup').animateClass('flipInX animated');
    }

    var displaySignInForm = function() {

        login.removeClass('m-login--forget-password');
        
        login.removeClass('m-login--signup');

        login.addClass('m-login--signin');
        
        login.find('.m-login__signin').animateClass('flipInX animated');
    }

    var displayForgetPasswordForm = function() {

        login.removeClass('m-login--signin');
        
        login.removeClass('m-login--signup');

        login.addClass('m-login--forget-password');
        
        login.find('.m-login__forget-password').animateClass('flipInX animated');
    }

    var handleFormSwitch = function() {

        $('#m_login_forget_password').click(function(e) {

            e.preventDefault();

            displayForgetPasswordForm();

        });

        $('#m_login_forget_password_cancel').click(function(e) {

            e.preventDefault();

            displaySignInForm();

        });

        $('#m_login_signup').click(function(e) {

            e.preventDefault();

            displaySignUpForm();

        });

        $('#m_login_signup_cancel').click(function(e) {

            e.preventDefault();

            displaySignInForm();

        });
    }

    var handleProfileUpdateSubmit = function() {

        $('#exit_from_profile').click(function(e) {

            if(somethingChanged){

                Swal({

                    title: 'Opss...Hay cambios sin guardar!',

                    html: 'Estas viendo este cartel porque modificaste datos del formulario pero no los guardaste. Si querés hacerlo, cerrá este cartel y presioná el botón que dice "guardar cambios".',

                    type: 'warning',

                    showCancelButton: true,

                    confirmButtonText: 'Entendido!',

                    cancelButtonText: 'salir'

                }).then((result) => {

                    if (result.value) {

                    } else if (result.dismiss === Swal.DismissReason.cancel) {

                        window.location.replace("index.php");

                    }

                })

            }

        });

        $('#update_profile').click(function(e) {

            e.preventDefault();

            var btn = $(this);
            
            var form = $(this).closest('form');

            form.validate({

                rules: {

                    name: {required: true },

                    surname: { required: true },

                    nickname: { required: true },

                    email: {required: true, email: true },

                    facebook:{url:true},
                    
                    instagram:{url:true},
                    
                    twitter:{url:true},
                    
                    linkedin:{url:true}
                },
                messages: {

                    name: helper.createErrorLabel('nombre', 'REQUIRED'),

                    surname: helper.createErrorLabel('apellido', 'REQUIRED'),

                    nickname: helper.createErrorLabel('nickname', 'REQUIRED'),

                    email: helper.createErrorLabel('email', 'REQUIRED'),

                    facebook: helper.createErrorLabel('facebook', 'INVALID_URL'),
                    
                    twitter:helper.createErrorLabel('twitter', 'INVALID_URL'),

                    instagram: helper.createErrorLabel('instagram', 'INVALID_URL'),

                    linkedin:helper.createErrorLabel('linkedin', 'INVALID_URL'),

                }

            });

            
            if (!form.valid()) { return; }

            newEmail = $('#email').val();

            if(userEmail != newEmail){

                Swal({

                    title: 'Vas a cambiar el e-mail! :)',

                    html: 'y no hay ningún problema en que lo hagas. Pero recordá que tu correo electrónico es tu usuario para acceder al administrador. La próxima vez que hagas un login, deberás hacerlo con <b>' + newEmail + '</b>.',

                    type: 'warning',

                    showCancelButton: true,

                    confirmButtonText: 'Entendido! Actualizar!',

                    cancelButtonText: 'salir'

                }).then((result) => {

                    if (result.value) {

                        updateProfile(btn, form);

                    } else if (result.dismiss === Swal.DismissReason.cancel) {


                    }

                })

            }else{

                updateProfile(btn, form);

            }

        });

    }

    var updateProfile = function(btn, form){

        btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

        form.ajaxSubmit({

            cache: false,

            url: 'private/users/profile.php',

            type: 'post',

            dataType: 'json',

            success: function(response, status, xhr, $form) {

                console.log("check this: ");

                console.log(status);

                btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                helper.unblockStage();

                helper.showToastr("información actualizada! :)", "la información se actualizó con éxito.");

            },

            error: function(response, status, xhr, $form){

                console.log("error");

                console.log(response);

            },

            beforeSubmit: function(){ helper.blockStage("actualizando..."); }

        });
    }

    var handleResetPassword = function(){

        $('#cancel_update_password').click(function(e){

            window.location.replace("index.php");

        });

        $('#update_password').click(function(e) {

            e.preventDefault();

            var btn = $(this);
            
            var form = $(this).closest('form');

            form.validate({

                rules: {

                    actualpassword: { required: true },

                    newpassword: { required: true, minlength: 4, score: true, notEqualTo: $('#actualpassword').val() },

                    repeatnewpassword:{ required: true, equalTo: newpassword }
                },
                messages: {

                    actualpassword: helper.createErrorLabel('contraseña', 'REQUIRED'),

                    newpassword:{

                        notEqualTo: helper.createErrorLabel('nueva contraseña', 'DIFFERENT', ['contraseña actual']),

                        required: helper.createErrorLabel('nueva contraseña', 'REQUIRED'),

                        minlength: helper.createErrorLabel('la nueva contraseña', 'MIN_LENGTH', ['4 caracteres.']),

                        score: helper.createErrorLabel('la nueva contraseña', 'STRENGHT')

                    },

                    repeatnewpassword: {

                        required: helper.createErrorLabel('repetir contraseña', 'REQUIRED'),

                        equalTo: helper.createErrorLabel('repetir contraseña', 'MATCH', ['nueva contraseña.'])

                    }
                }
            });

            if (!form.valid()) {return; }

            form.ajaxSubmit({

                cache: false,

                url: 'private/users/changepassword.php',

                type: 'post',

                dataType: 'json',

                success: function(response, status, xhr, $form) {

                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                    helper.unblockStage();

                    //helper.showToastr("lorem ipsum", "loremet.");

                    var msgType;

                    var msgString;

                    var title;

                    var button;

                    switch(response.status){

                        case 0:

                        title = "Ops! algo salió mal.";

                        msgType = 'error';

                        msgString = 'Ocurrió un error. Por favor volvé a intentarlo';

                        button = 'ok. Va de nuevo.';

                        break;

                        case 1:

                         //la contrasena es incorrecta...

                         title = "Ops! algo salió mal.";

                         msgType = 'error';

                         msgString = 'La contraseña actual que ingresaste es incorrecta. Volvé a intentarlo.';

                         button = 'Volver a intentar';

                         break;

                         case 2:

                         title = "Cambio realizado! :)"

                         msgType = 'success';

                         msgString = 'La contraseña se actualizó correctamente! :) recordá usarla en tu próximo login.';

                         button = 'Entendido!';

                         break;

                         case 3:

                         msgType = 'error';

                         msgString = 'El formulario no pasó la validación en el servidor.';

                         button = 'ok';

                         break;
                     }

                     Swal({

                        title: title,

                        html: msgString,

                        type: msgType,

                        confirmButtonText: button

                    }).then((result) => {

                        switch(response.status){

                            case 1:

                            setTimeout(function() { $('input[name="actualpassword"]').focus() }, 500);

                            break;

                            case 2:

                            $("#change-password-form").trigger("reset");

                            $('.progress-bar').width('0%');

                            helper.showToastr("Contraseña actualizada", "la contraseña se actualizó con éxito.");

                            break;
                        }


                    });

                },

                error: function(response, status, xhr, $form){

                    console.log("error");

                    console.log(response);

                    console.log(status);

                    console.log(xhr);


                },

                beforeSubmit: function(){ helper.blockStage("actualizando contraseña..."); }

            });


        });
}

    /**
     * @function loadAjaxImages
     * @param loadSource - evaluate this param to get one batch or a single one.
     * @description if we are in edition mode load all the images for Bootstrap File Input.
     */

     var loadAjaxImages = function(from, loadSource, config, myPreviews) {

        var request = $.ajax({

            url: "private/users/services/get_list_of_images.php",

            type: "POST",

            data: {

                id: userId,

                source: loadSource,

                from: from
            },

            dataType: "json"
        });

        request.done(function(result) {

            for (var i = 0; i < result.length; i++) {

                var internalOrder = result[i].internal_order;

                var id = result[i].id;

                var source = result[i].source;

                var description = result[i].description;

                var imgName = userId + '_' + result[i].unique_id + '_original.jpeg';

                var path = 'private/sources/images/profile/' + userId + '/' + result[i].unique_id + '/' + imgName;

                myPreviews.push(path);

                config.push({

                    'key': id,
                    
                    'internal_order': internalOrder,
                    
                    'source': source,

                    'description' : description,

                    'caption' : (description != null) ? description : ''

                });

            }

            createCover();

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

        });

    }

    /**
     * @function createCover
     * @description Create the cover image.
     */

     var createCover = function(){

        var editMode = (initialPreviewCoverPaths.length>0) ? true : false;

       $("#main-image").fileinput({

        initialPreview : (editMode) ? initialPreviewCoverPaths : '',

        initialPreviewConfig: initialPreviewCoverConfig,

        initialPreviewAsData: true,

        theme: "fas",

        uploadUrl: "private/users/museum/images_general/add_image.php",

        deleteUrl: "private/users/museum/general/delete_image.php",

        showCaption: true,

        showPreview: true,

        showRemove: true,

        showUpload: true,

        showCancel: true,

        showDrag: false,

        fileActionSettings: {showDrag: false, showUpload:false},

        showClose: false,

        browseOnZoneClick: (editMode) ? false  : true,

        showBrowse: (editMode) ? false  : true,

        previewFileType: 'any',

        language: "es",

        maxFileSize: 0,

        allowedFileTypes: ["image"],

        overwriteInitial: false,

        preferIconicPreview: true,

        allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],

        maxFilePreviewSize: 10240,

        minImageWidth: 500,

        minImageHeight: 500,

        uploadExtraData: function() {

            return {

                id: userId,

                source: 'profile'
            };

        }

    }).on('fileloaded', function(event, file, previewId, index, reader) {


    }).on('fileerror', function(event, data, msg) {


    }).on('fileimageresizeerror', function(event, data, msg) {

        showErrorSwal(msg);

    }).on('fileuploaderror', function(event, data, msg) {

        var form = data.form, files = data.files, extra = data.extra,

        response = data.response, reader = data.reader;

        showErrorSwal(msg);

    }).on('filedeleted', function(event, key, jqXHR, data) {

        $('#main-image').fileinput('refresh', 

            {browseLabel: 'Buscar otra...', showBrowse: true, browseOnZoneClick: true, 

            uploadAsync: false, showUpload: false, showRemove: false});

    }).on("filebatchselected", function(event, files) {

        if(editMode) $('#main-image').fileinput("upload");

    });

}

return {

    init: function() {

        userEmail = $('#email').val();

        $("#birthday-picker").datepicker({autoclose:true, format: 'yyyy-mm-dd'});

        $('input').change(function() {  

            somethingChanged = true; 

        }); 

        $.validator.addMethod("score", function (value, element) {

            return (globalScore >= 0) ? true : false;

        }, '');

        $.validator.addMethod("notEqualTo", function(v, e, p) {

            return this.optional(e) || v != p;

        }, '');


        $('#newpassword').keyup(function() {

            globalScore = helper.passwordScore($(this).val());

            $('.progress-bar').width(globalScore * 25 + '%');

        });

        handleFormSwitch();

        handleProfileUpdateSubmit();

        handleResetPassword();

        loadAjaxImages('museum_images', 'profile', initialPreviewCoverConfig, initialPreviewCoverPaths);


    }

};

}();

jQuery(document).ready(function() { Profile.init(); });

