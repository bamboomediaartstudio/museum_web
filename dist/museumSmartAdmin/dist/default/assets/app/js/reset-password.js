//== Class Definition
var ResetPassword = function() {

    var login = $('#m_reset');

    helper = Helper();

    var globalScore = 0;

    var vericode;

    var showErrorMsg = function(form, type, msg) {

        var alert = $('<div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
           <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
           <span></span>\
           </div>');

        form.find('.alert').remove();

        alert.prependTo(form);
        
        alert.animateClass('fadeIn animated');
        
        alert.find('span').html(msg);
    }

    var handleSignInFormSubmit = function() {

        $('#m_reset_signin_submit').click(function(e) {

            e.preventDefault();

            var btn = $(this);
            
            var form = $(this).closest('form');

            form.validate({

                rules: {

                    email: {

                        required: true,

                        email: true

                    },

                    newpassword: { required: true, minlength: 4, score: true },

                    repeatnewpassword:{ required: true, equalTo: newpassword }

                },
                messages: {

                    email:helper.createErrorLabel('email', 'REQUIRED'),

                    newpassword:{

                        required: helper.createErrorLabel('nueva contraseña', 'REQUIRED'),

                        minlength: helper.createErrorLabel('la nueva contraseña', 'MIN_LENGTH', ['4 caracteres.']),

                        score: helper.createErrorLabel('la nueva contraseña', 'STRENGHT')

                    },

                    repeatnewpassword: {

                        required: helper.createErrorLabel('repetir contraseña', 'REQUIRED'),

                        equalTo: helper.createErrorLabel('repetir contraseña', 'MATCH', ['nueva contraseña.'])

                    }

                }

            });

            if (!form.valid()) return;

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({

                cache: false,

                url: 'private/users/reset_password.php',

                type: 'post',

                dataType: 'json',

                data: {vericode: vericode},

                success: function(response, status, xhr, $form) {

                    console.log(response);

                    swal({

                        title: response.title,

                        html: response.msg,

                        type: response.alert,

                        confirmButtonText: 'ok'

                        }).then((result) => {

                            switch(Number(response.status)){

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 5:

                                location.reload();

                                break;

                                case 4:

                                btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                                form.clearForm();
                        
                                form.validate().resetForm();

                                var signInForm = login.find('.m-login__signin form');
                                
                                signInForm.clearForm();
                                
                                signInForm.validate().resetForm();

                                showErrorMsg(signInForm, 'success', response.msg);

                                break;

                            }

                        
                        });

                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                },

                error: function(response, status, xhr, $form){

                    console.log("error");

                    console.log(response);
                }

            });

        });

    }

    var addListeners = function(){

        $.validator.addMethod("score", function (value, element) {

            return (globalScore >= 4) ? true : false;

        }, '');

        $.validator.addMethod("notEqualTo", function(v, e, p) {

            return this.optional(e) || v != p;

        }, '');


        $('#newpassword').keyup(function() {

            globalScore = helper.passwordScore($(this).val());

            $('.progress-bar').width(globalScore * 25 + '%');

        });
    }


    return {

        init: function() {

            vericode = helper.getGETVariables(window.location.href, "vericode");

            addListeners();

            handleSignInFormSubmit();
            
        }

    };

}();

jQuery(document).ready(function() { ResetPassword.init(); });
