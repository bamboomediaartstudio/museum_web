<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

if($user->data()->group_id == 1 || $user->data()->group_id == 2){

	$allTheStaff = DB::getInstance()->query("SELECT * FROM museum_staff")->count();

	$allTheObjectives = DB::getInstance()->query("SELECT * FROM museum_objectives WHERE deleted = ?", array(0))->count();

	$allTheTestimonies = DB::getInstance()->query("SELECT * FROM museum_testimonials WHERE deleted = ?", array(0))->count();

	$allTheOpinions = DB::getInstance()->query("SELECT * FROM museum_opinions WHERE deleted = ?", array(0))->count();

	$allTheFAQ = DB::getInstance()->query("SELECT * FROM museum_faq WHERE deleted = ?", array(0))->count();

	$allTheCourses = DB::getInstance()->query("SELECT * FROM museum_courses WHERE deleted = ?", array(0))->count();

	$allTheExhibitions = DB::getInstance()->query("SELECT * FROM museum_exhibitions WHERE deleted = ?", array(0))->count();

	$allTheNews = DB::getInstance()->query("SELECT * FROM museum_news WHERE deleted = ?", array(0))->count();

	$allTheEvents = DB::getInstance()->query("SELECT * FROM museum_events WHERE deleted = ?", array(0))->count();

	$allTheWebmails = DB::getInstance()->query("SELECT * FROM webmail_messages")->count();

	$allTheNewsletters = DB::getInstance()->query("SELECT * FROM museum_newsletter_backup")->count();

	$allTheApplicants = DB::getInstance()->query("SELECT * FROM museum_newsletter_backup")->count();

	$allTheAlerts = DB::getInstance()->query("SELECT * FROM museum_alerts")->count();

}else if($user->data()->group_id == 4){

	$today = date("Y/m/d");

	$queryLoop = $queryCopy = $query = DB::getInstance()->query(

		"SELECT * FROM museum_booking_guides as mbg

		INNER JOIN museum_guides as mg ON

		mbg.id_guide = mg.id

		INNER JOIN users as u ON

		mg.email = u.email

		INNER JOIN museum_booking as mb ON

		mbg.id_booking = mb.id

		LEFT JOIN museum_booking_institutions_relations AS mbir

		ON mbir.id_booking = mb.id

		LEFT JOIN museum_booking_institutions as mbi

		ON mbi.id = mbir.id_institution

		WHERE u.email = ? AND mb.sid = ? AND mb.date_start >= ? ORDER BY mb.date_start ASC", [$user->data()->email, 1, $today]);

	$groupTotal = $query->count();

	$groupConfirmed = 0;

	foreach($query->results() as $result){

		if($result->booked_places == 1){ $groupConfirmed+=1;  }

	}

	if($groupTotal >0){

		$groupPercent = $groupConfirmed * 100 / $groupTotal;

	}else{

		$groupPercent = 0;

	}

	$queryCopyContent = $queryCopy->first();

	$queryLoopContent = $queryLoop->results();

	$dt = new DateTime($queryCopyContent->date_start);

	$date = $dt->format('d-m-Y');

	$time = $dt->format('H:i');

	$name = $queryCopyContent->institution_name;

	//...
	//query2

	$queryLoop2 = $query2Copy = $query2 = DB::getInstance()->query(

		"SELECT * FROM museum_booking_guides as mbg

		INNER JOIN museum_guides as mg ON

		mbg.id_guide = mg.id

		INNER JOIN users as u ON

		mg.email = u.email

		LEFT JOIN museum_booking as mb ON

		mbg.id_booking = mb.id

		WHERE u.email = ? AND mb.sid = ? AND mb.date_start >= ? ORDER BY mb.date_start ASC", [$user->data()->email, 2, $today]);

	$results2 = $query2->results();

	$individualTotal = $query2->count();

	$queryLoopContent2 = $queryLoop2->results();

	$individualConfirmed = 0;

	foreach($query2->results() as $result2){

		if($result2->booked_places >= 1){

			$individualConfirmed+=1;

			$insciptionsCounter = $result2->booked_places;

		}


	}

	if($individualTotal > 0){

		$individualPercent = $individualConfirmed * 100 / $individualTotal;

	}else{

		$individualPercent = 0;

	}

	$query2Copy = $query2->first();

	$dt2 = new DateTime($query2Copy->date_start);

	$date2 = $dt2->format('d-m-Y');

	$time2 = $dt2->format('H:i');

	$queryLoop3 = $query3 = DB::getInstance()->query(

		"SELECT * FROM museum_booking_guides as mbg

		INNER JOIN museum_guides as mg ON

		mbg.id_guide = mg.id

		INNER JOIN users as u ON

		mg.email = u.email

		LEFT JOIN museum_booking as mb ON

		mbg.id_booking = mb.id

		WHERE u.email = ? AND mb.sid = ? AND mb.date_start >= ? ORDER BY mb.date_start ASC", [$user->data()->email, 3, $today]);

	$results3 = $query3->first();

	$queryLoopContent3 = $queryLoop3->results();

	$stickyNote = $results3->notes;

	$dt3 = new DateTime($results3->date_start);

	$date3 = $dt3->format('d-m-Y');

	$time3 = $dt3->format('H:i');

	$institutionalTotal = $query3->count();

	if($institutionalTotal == 0){

		$institutionalConfirmed = 0;

		$institutionalPercent = 0;

	}else{

		$institutionalConfirmed = 1;

		$institutionalPercent = 100;


	}

}

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Administrador de contenidos :)</title>

	<meta name="description" content="Latest updates and statistic charts">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php';?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title">Bienvenido al Administrador!</h3>
							<p>Bienvenida/o, <strong><?php echo $user->data()->name;?>! :)</strong></p>

						</div>

					</div>
				</div>

				<?php if($user->data()->group_id == 1 || $user->data()->group_id == 2){?>

					<div class="m-content">

						<div class="m-portlet">

							<div class="m-portlet__body m-portlet__body--no-padding">

								<div class="row m-row--no-padding m-row--col-separator-xl">

									<div class="col-md-12 col-lg-12 col-xl-4">

										<div class="m-widget1">

											<div class="m-widget1__item">

												<div class="row m-row--no-padding align-items-center">

													<div class="col">

														<h3 class="m-widget1__title"><i class="fas fa-users mr-2"></i>Staff</h3>

														<a href="museum_staff_add.php">agregar nuevo</a> | <a href="museum_staff_list.php">ver lista</a>

													</div>

													<div class="col m--align-right">

														<span class="m-widget1__number m--font-brand"><?php echo $allTheStaff;?></span>

													</div>

												</div>

											</div>

											<div class="m-widget1__item">

												<div class="row m-row--no-padding align-items-center">

													<div class="col">

														<h3 class="m-widget1__title"><i class="fas fa-list-ul mr-2"></i>Objetivos</h3>

														<a href="museum_objective_add.php">agregar nuevo</a> | <a href="museum_objectives_list.php">ver lista</a>

													</div>

													<div class="col m--align-right">

														<span class="m-widget1__number m--font-brand"><?php echo $allTheObjectives;?></span>

													</div>

												</div>
											</div>

											<div class="m-widget1__item">

												<div class="row m-row--no-padding align-items-center">

													<div class="col">

														<h3 class="m-widget1__title"><i class="fas fa-pen-nib mr-2"></i>Testimonios</h3>

														<a href="museum_testimonial_add.php">agregar nuevo</a> | <a href="museum_testimonials_list.php">ver lista</a>

													</div>
													<div class="col m--align-right">
														<span class="m-widget1__number m--font-brand"><?php echo $allTheTestimonies;?></span>

													</div>

												</div>

											</div>

											<div class="m-widget1__item">

												<div class="row m-row--no-padding align-items-center">

													<div class="col">

														<h3 class="m-widget1__title"><i class="fas fa-exclamation-triangle mr-2"></i>Alertas</h3>

														<a href="museum_alert_add.php">agregar nuevo</a> | <a href="museum_alerts_list.php">ver lista</a>

													</div>
													<div class="col m--align-right">
														<span class="m-widget1__number m--font-brand"><?php echo $allTheAlerts;?></span>

													</div>

												</div>

											</div>

										</div>

									</div>

									<div class="col-md-12 col-lg-12 col-xl-4">

										<div class="m-widget1">

											<div class="m-widget1__item">

												<div class="row m-row--no-padding align-items-center">

													<div class="col">

														<h3 class="m-widget1__title"><i class="fas fa-comments mr-2"></i>Opiniones</h3>

														<a href="museum_opinion_add.php">agregar nueva</a> | <a href="museum_opinions_list.php">ver lista</a>

													</div>

													<div class="col m--align-right">

														<span class="m-widget1__number m--font-brand"><?php echo $allTheOpinions;?></span>

													</div>

												</div>

											</div>

											<div class="m-widget1__item">

												<div class="row m-row--no-padding align-items-center">

													<div class="col">

														<h3 class="m-widget1__title"><i class="fas fa-question-circle mr-2"></i> F.A.Q</h3>

														<a href="museum_faq_add.php">agregar nueva</a> | <a href="museum_faq_list.php">ver lista</a>

													</div>

													<div class="col m--align-right">

														<span class="m-widget1__number m--font-brand"><?php echo $allTheFAQ;?></span>

													</div>

												</div>

											</div>

											<div class="m-widget1__item">

												<div class="row m-row--no-padding align-items-center">

													<div class="col">

														<h3 class="m-widget1__title"><i class="fas fa-graduation-cap mr-2"></i>Cursos</h3>

														<a href="museum_course_add.php">agregar nuevo</a> | <a href="museum_courses_list.php">ver lista</a>

													</div>

													<div class="col m--align-right">

														<span class="m-widget1__number m--font-brand"><?php echo $allTheCourses;?></span>

													</div>

												</div>

											</div>

										</div>

									</div>

									<div class="col-md-12 col-lg-12 col-xl-4">

										<div class="m-widget1">

											<div class="m-widget1__item">

												<div class="row m-row--no-padding align-items-center">

													<div class="col">

														<h3 class="m-widget1__title"><i class="fas fa-box mr-2"></i>Muestras</h3>

														<a href="museum_exhibition_add.php">agregar nueva</a> | <a href="museum_exhibitions_list.php">ver lista</a>

													</div>

													<div class="col m--align-right">

														<span class="m-widget1__number m--font-brand"><?php echo $allTheExhibitions;?></span>

													</div>

												</div>

											</div>

											<div class="m-widget1__item">

												<div class="row m-row--no-padding align-items-center">

													<div class="col">

														<h3 class="m-widget1__title"><i class="fas fa-newspaper mr-2"></i>Noticias</h3>

														<a href="museum_news_add.php">agregar nueva</a> | <a href="museum_news_list.php">ver lista</a>

													</div>

													<div class="col m--align-right">

														<span class="m-widget1__number m--font-brand"><?php echo $allTheNews;?></span>

													</div>

												</div>

											</div>

											<div class="m-widget1__item">

												<div class="row m-row--no-padding align-items-center">

													<div class="col">

														<h3 class="m-widget1__title"><i class="fas fa-calendar-alt mr-2"></i> Eventos</h3>

														<a href="museum_event_add.php">agregar nuevo</a> | <a href="museum_events_list.php">ver lista</a>

													</div>

													<div class="col m--align-right">

														<span class="m-widget1__number m--font-brand"><?php echo $allTheEvents;?></span>

													</div>

												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

					<div class="m-subheader ">

						<div class="d-flex align-items-center">

							<div class="mr-auto">

								<h3 class="m-subheader__title">Algunas listas</h3>
								<p class="col-12">Todas las acciones que los usuarios llevan a cabo en la web, tienen un <strong>back-up</strong> a modo de listas en el admin.
									Si olvidaste algún <strong>email</strong>, querés chequear las suscripciones al <strong>newsletter</strong> o buscar alguna solicitud de <strong>donantes</strong>, <strong>amigo del museo</strong> o <strong>sponsor</strong>, este es tu lugar.</p>

								</div>

							</div>

						</div>

						<div class="m-content">

							<div class="m-portlet">

								<div class="m-portlet__body m-portlet__body--no-padding">

									<div class="row m-row--no-padding m-row--col-separator-xl">

										<div class="col-md-12 col-lg-12 col-xl-4">

											<div class="m-widget1">

												<div class="m-widget1__item">

													<div class="row m-row--no-padding align-items-center">

														<div class="col">

															<h3 class="m-widget1__title"><i class="fas fa-envelope mr-2"></i> Webmails</h3>

															<a href="museum_emails_backups.php">ver lista</a>

														</div>

														<div class="col m--align-right">

															<span class="m-widget1__number m--font-brand"><?php echo $allTheWebmails;?></span>

														</div>

													</div>

												</div>

											</div>

										</div>

										<div class="col-md-12 col-lg-12 col-xl-4">

											<div class="m-widget1">

												<div class="m-widget1__item">

													<div class="row m-row--no-padding align-items-center">

														<div class="col">

															<h3 class="m-widget1__title"><i class="fab fa-mailchimp mr-2"></i>Newsletter</h3>

															<a href="museum_newsletter_backups.php">ver lista</a>

														</div>

														<div class="col m--align-right">

															<span class="m-widget1__number m--font-brand"><?php echo $allTheNewsletters;?></span>

														</div>

													</div>

												</div>


											</div>

										</div>

										<div class="col-md-12 col-lg-12 col-xl-4">

											<div class="m-widget1">

												<div class="m-widget1__item">

													<div class="row m-row--no-padding align-items-center">

														<div class="col">

															<h3 class="m-widget1__title"><i class="fas fa-user-edit mr-2"></i>Solicitudes</h3>

															<a href="museum_applicants_backups.php">ver lista</a>

														</div>

														<div class="col m--align-right">

															<span class="m-widget1__number m--font-brand"><?php echo $allTheApplicants;?></span>

														</div>

													</div>

												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					<?php }else if($user->data()->group_id == 4){?>

						<div class="m-subheader ">

							<div class="d-flex align-items-center">

								<div class="mr-auto">

									<h3 class="m-subheader__title">Tus próxima visitas:</h3>

									<p class="col-12">


									</p>

								</div>

							</div>

						</div>

						<div class="m-portlet">

							<div class="m-portlet__body m-portlet__body--no-padding">

								<div class="row m-row--no-padding m-row--col-separator-xl">

									<div class="col-12">

										<div class="m-widget1">

											<div class="m-widget1__item">

												<div class="row m-row--no-padding align-items-center">

													<div class="col">

														<h3 class="m-widget1__title">

															<?php

															if($groupTotal >0){

																echo 'Tu próxima visita grupal:';

															}else{

																echo 'No tienes visitas grupale asignadas';

															}

															?>

														</h3>

														<span class="m-widget1__desc">

															<?php echo $name;?>

														</span>

													</div>

													<div class="col m--align-right">

														<span class="m-widget1__number m--font-success">

															El <?php echo $date . ' a las ' . $time;?>

														</span>

													</div>

												</div>

											</div>

											<div class="m-widget1__item">

												<div class="row m-row--no-padding align-items-center">

													<div class="col">

														<h3 class="m-widget1__title">

															<?php

															if($individualTotal >0){

																echo 'Tu próxima visita individual:';

															}else{

																echo 'No tienes visitas individuales asignadas';

															}

															?>

														</h3>

														<span class="m-widget1__desc">

															<?php if($individualTotal >0){ ?>

																<?php echo $insciptionsCounter;?> persona/s inscripta/s! :)

															<?php } ?>

														</span>

													</div>

													<div class="col m--align-right">
														<span class="m-widget1__number m--font-danger">

															<?php if($individualTotal >0){ ?>

																El <?php echo $date2 . ' a las ' . $time2;?>

															<? } ?>
														</span>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">

												<div class="row m-row--no-padding align-items-center">

													<div class="col">

														<h3 class="m-widget1__title">

															<?php if($institutionalTotal >0){

																echo 'Tu próxima visita institucional:';

															}else{

																echo 'No tienes visitas istitucionales asignadas.';

															}

															?>

														</h3>

														<span class="m-widget1__desc">

															<?php

															if($institutionalTotal >0){

																echo $stickyNote;

															}

															?>

														</span>

													</div>

													<div class="col m--align-right">

														<span class="m-widget1__number m--font-warning">
															<?php if($institutionalTotal >0){ ?>
																El <?php echo $date3 . ' a las ' . $time3;?>

															<?php } ?>

														</span>

													</div>

												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

						<div class="m-portlet">

							<div class="m-subheader ">

								<div class="d-flex align-items-center">

									<div class="mr-auto">

										<h3 class="m-subheader__title">Generales y estadísticas:</h3>

										<p class="col-12">


										</p>

									</div>

								</div>

							</div>

							<div class="m-portlet__body  m-portlet__body--no-padding">

								<div class="row m-row--no-padding m-row--col-separator-xl">

									<div class="col-md-12 col-lg-6 col-xl-4">

										<div class="m-widget24">

											<div class="m-widget24__item">

												<h4 class="m-widget24__title">

													Visitas Grupales

												</h4>

												<br>

												<span class="m-widget24__desc">

													Cantidad total:

												</span>

												<span class="m-widget24__stats m--font-success">

													<?php echo $groupTotal;?>

												</span>

												<div class="m--space-10"></div>

												<div class="progress m-progress--sm">

													<div class="progress-bar m--bg-success" role="
													progressbar" style="width: <?php echo $groupPercent;?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>

												</div>

												<span class="m-widget24__change">

													Confirmadas: <?php echo $groupConfirmed;?>

												</span>

												<span class="m-widget24__number">

													<?php echo $groupPercent. '%'?>


												</span>

											</div>

										</div>

									</div>

									<div class="col-md-12 col-lg-6 col-xl-4">

										<div class="m-widget24">

											<div class="m-widget24__item">

												<h4 class="m-widget24__title">Visitas Individuales</h4>

												<br>

												<span class="m-widget24__desc">

													Cantidad total:

												</span>

												<span class="m-widget24__stats m--font-danger">

													<?php echo $individualTotal;?>

												</span>

												<div class="m--space-10"></div>

												<div class="progress m-progress--sm">

													<div class="progress-bar m--bg-danger" role="progressbar" style="width: <?php echo $individualPercent;?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">

													</div>

												</div>

												<span class="m-widget24__change">

													Con asistencia confirmada: <?php echo $individualConfirmed;?>

												</span>

												<span class="m-widget24__number">

													<?php echo $individualPercent;?> %

												</span>

											</div>

										</div>

									</div>

									<div class="col-md-12 col-lg-6 col-xl-4">

										<div class="m-widget24">

											<div class="m-widget24__item">

												<h4 class="m-widget24__title">

													Visitas Institucionales

												</h4>

												<br>

												<span class="m-widget24__desc">

													Cantidad total:
												</span>

												<span class="m-widget24__stats m--font-warning">

													<?php echo $institutionalTotal;?>

												</span>

												<div class="m--space-10"></div>

												<div class="progress m-progress--sm">

													<div class="progress-bar m--bg-warning" role="progressbar" style="width: <?php echo $institutionalPercent;?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>

												</div>

												<span class="m-widget24__change">

													Confirmadas: <?php echo $institutionalTotal;?>

												</span>

												<span class="m-widget24__number">

													<?php echo $institutionalPercent;?>%

												</span>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

						<div class="row">

							<!-- grupales -->

							<div class="col-xl-4">

								<div class="m-portlet m-portlet--full-height">

									<div class="m-portlet__head">

										<div class="m-portlet__head-caption">

											<div class="m-portlet__head-title">

												<h3 class="m-portlet__head-text">

													Próximas grupales

												</h3>

											</div>

										</div>

									</div>

									<div class="m-portlet__body">

										<div class="m-widget6">

											<div class="m-widget6__head">

												<div class="m-widget6__item">

													<span class="m-widget6__caption">

														Institucion

													</span>

													<span class="m-widget6__caption m--align-right">

														fecha

													</span>

													<span class="m-widget6__caption m--align-right">

														hora

													</span>

												</div>

											</div>

											<div class="m-widget6__body">

												<?php foreach($queryLoopContent as $result){

													$dt = new DateTime($result->date_start);

													$date = $dt->format('d-m-Y');

													$time = $dt->format('H:i');

													$name = $result->institution_name;

													?>

													<div class="m-widget6__item">

														<span class="m-widget6__text">

															<?php echo $name;?>

														</span>
														<span class="m-widget6__text m--align-right m--font-boldest m--font-success">

															<?php echo $date;?>

														</span>

														<span class="m-widget6__text m--align-right m--font-boldest m--font-success">

															<?php echo $time;?>
														</span>

													</div>

												<?php } ?>

											</div>

										</div>

									</div>

								</div>

							</div>

							<!-- proximas individuales -->


							<div class="col-xl-4">

								<div class="m-portlet m-portlet--full-height">

									<div class="m-portlet__head">

										<div class="m-portlet__head-caption">

											<div class="m-portlet__head-title">

												<h3 class="m-portlet__head-text">

													Próximas Individuales

												</h3>

											</div>

										</div>

									</div>

									<div class="m-portlet__body">

										<div class="m-widget6">

											<div class="m-widget6__head">

												<div class="m-widget6__item">

													<span class="m-widget6__caption">

														Institucion

													</span>

													<span class="m-widget6__caption m--align-right">

														fecha

													</span>

													<span class="m-widget6__caption m--align-right">

														hora

													</span>

												</div>

											</div>

											<div class="m-widget6__body">

												<?php foreach($queryLoopContent2 as $result){

													$dt = new DateTime($result->date_start);

													$date = $dt->format('d-m-Y');

													$time = $dt->format('H:i');

													$bookedPlaces = $result->booked_places;

													?>

													<div class="m-widget6__item">

														<span class="m-widget6__text">
															<?php echo $bookedPlaces . ' inscriptos.';?>
														</span>
														<span class="m-widget6__text m--align-right m--font-boldest m--font-danger">
															<?php echo $date;?>
														</span>
														<span class="m-widget6__text m--align-right m--font-boldest m--font-danger">
															<?php echo $time;?>
														</span>
													</div>

												<?php } ?>


											</div>

										</div>

									</div>

								</div>

							</div>

							<!-- institucional -->

							<div class="col-xl-4">

								<div class="m-portlet m-portlet--full-height">

									<div class="m-portlet__head">

										<div class="m-portlet__head-caption">

											<div class="m-portlet__head-title">

												<h3 class="m-portlet__head-text">

													Próximas Institucionales

												</h3>

											</div>

										</div>

									</div>

									<div class="m-portlet__body">

										<div class="m-widget6">

											<div class="m-widget6__head">

												<div class="m-widget6__item">

													<span class="m-widget6__caption">

														Notas

													</span>

													<span class="m-widget6__caption m--align-right">

														fecha

													</span>

													<span class="m-widget6__caption m--align-right">

														hora

													</span>

												</div>

											</div>

											<div class="m-widget6__body">

												<?php foreach($queryLoopContent3 as $result){

													$dt = new DateTime($result->date_start);

													$date = $dt->format('d-m-Y');

													$time = $dt->format('H:i');

													$notes = $result->notes;

													?>

													<div class="m-widget6__item">

														<span class="m-widget6__text">
															<?php echo $notes;?>
														</span>
														<span class="m-widget6__text m--align-right m--font-boldest m--font-warning">
															<?php echo $date;?>
														</span>
														<span class="m-widget6__text m--align-right m--font-boldest m--font-warning">
															<?php echo $time;?>
														</span>
													</div>

												<?php } ?>


											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					<?php } ?>

				</div>

			</div>

			<?php require_once 'private/includes/footer.php'; ?>

		</div>


		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">

			<i class="la la-arrow-up"></i>

		</div>

		<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

		<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

		<script src="assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>

		<script src="assets/app/js/dashboard.js" type="text/javascript"></script>

	</body>

	</html>
