<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Criptoactivos</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});
		
	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
	
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="modal" tabindex="-1" role="dialog">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title">...</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body">

					<div class="modal-body-line-1"></div>

					<input type="text" id="target-address" name="target-address" class="form-control" value="">

					<button class="mt-3 btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent btn green mt-clipboard-container"> copiar link</button>

					<div class="modal-body-line-2"></div>

					<div id="qr-conainer" class="text-center">

						<div id="qrcode"></div>

					</div>

				</div>

				<div class="modal-footer">

					<button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<input type="hidden" name="user-id" id="user-id" value="<?php echo $actualCrypto;?>">

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title">CRIPTOACTIVOS</h3>

							<p>Estos son los criptoactivos a través de los cuales el Museo Del Holocausto puede recibir donaciones. En caso de querer añadir o eliminar activos, por motivos de seguridad, se debe proceder manualmente.</p>

						</div>

					</div>

				</div>
				
				<div class="m-content">

					<div class="row">

						<?php

						$query = DB::getInstance()->query(

							"SELECT * FROM app_cryptos as crypto

							WHERE crypto.active = ? AND crypto.deleted = ? ORDER BY crypto.id ", array(1, 0));

						$list = $query->results();

						$counter = 0;

						foreach($list as $actualCrypto){

							$name = $actualCrypto->name;

							$address = $actualCrypto->address;

							$blockchain = $actualCrypto->blockchain;

							$id = $actualCrypto->id;

							$cryptoImage = 'private/sources/images/crypto/' . $actualCrypto->abbreviation . '.png';

							?>

							<div class="col-xl-3 col-lg-4">

								<div class="m-portlet m-portlet--full-height  ">

									<div class="m-portlet__body">

										<div class="m-card-profile">

											<div class="m-card-profile__pic">

												<div class="m-card-profile__pic-wrapper">

													<img src="<?php echo $cryptoImage;?>" alt="<?php echo $name;?>"/>

												</div>

											</div>

											<div class="m-card-profile__details">

												<span class="m-card-profile__name">

													<?php echo $name;?>

												</span>

												<p>

													<?php echo $actualCrypto->abbreviation;?>

												</p>

											</div>


										</div>


									</div>

									<div class="text-center pb-3"> 

										<button

											data-address= "<?php echo $actualCrypto->address;?>" 
											
											data-name= "<?php echo $actualCrypto->name;?>" 

											type="button" class="info-button btn m-btn m-btn--pill m-btn--air m-btn--gradient-from-metal m-btn--gradient-to-accent">address</button>

											<button 

											data-name= "<?php echo $actualCrypto->name;?>" 

											data-id= "<?php echo $actualCrypto->id;?>" 
											
											data-blockchain= "<?php echo $actualCrypto->blockchain;?>" 

											type="button" class="open-blockchain btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning">blockchain</button>


									</div>

								</div>

							</div>

						<?php } ?>	

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
		<i class="la la-arrow-up"></i>
	</div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/vendors/custom/jquery-ui/jquery-ui.bundle.js" type="text/javascript"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.qrcode/1.0/jquery.qrcode.min.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="assets/app/js/museum/crypto/crypto.js" type="text/javascript"></script>


</body>

</html>
