<?php

require_once 'private/users/core/init.php';

$settingsQ = DB::getInstance()->query("Select * FROM support_settings");

$settings = $GLOBALS['settings'] = $settingsQ->first();


?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | reset password</title>

	<meta name="description" content="Latest updates and statistic charts">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});
		
	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico?v=2" />

	<script src='https://www.google.com/recaptcha/api.js?theme=dark'></script>
	
</head>

<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_reset">

			<div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">

				<div class="m-stack m-stack--hor m-stack--desktop">

					<div class="m-stack__item m-stack__item--fluid">

						<div class="m-login__wrapper">

							<div class="m-login__signin">

								<div class="m-login__head">

									<h3 class="m-login__title text-danger">

										LINK EXPIRADO

									</h3>

									<p align="center">La página a la que intentás entrar expiró<br>Si lo que querés es cambiar el password de tu cuenta, por favor volvé a iniciar el proceso.<br><br>Si necesitás ayuda, nos podés llamar al:<br><br><strong><?php echo $settings->help_phone;?></strong><br><br>También nos podes escribir a:<br><br> <strong><?php echo $settings->help_email;?></strong></p>

									<br>

									<div class="text-center">

										<!--<form action='peper.php?ids=1'>

											<input class="btn btn-focus btn-red m-btn m-btn--pill m-btn--custom m-btn--air" type="submit" value="resetear password" />

										</form>-->

										<input class="btn btn-focus btn-red m-btn m-btn--pill m-btn--custom m-btn--air" type="button" onclick="location.href='login.php?action=reset';" value="Resetear Password" />


									</div>

								</div>


								
							</div>

						</div>

					</div>
					
				</div>
			</div>

			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content" style="background-image: url(assets/app/media/img//bg/bg-1.jpg)">
				
				<div class="m-grid__item m-grid__item--middle">

					<h3 class="m-login__welcome">Museo del Holocausto</h3>

					<p class="m-login__msg">Sistema de administración para el Museo del Holocausto.</p>
					
				</div>
				
			</div>
			
		</div>
		
	</div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

</body>

</html>
