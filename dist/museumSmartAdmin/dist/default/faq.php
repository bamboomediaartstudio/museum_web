<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | FAQ</title>

	<meta name="description" content="Latest updates and statistic charts">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php';?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="">

							<h3 class="m-subheader__title m-subheader__title--separator">

								FAQ / PREGUNTAS FRECUENTES

							</h3>

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon">

										<i class="m-nav__link-icon la la-home"></i>

									</a>

								</li>

								<li class="m-nav__separator">-</li>
								
								<li class="m-nav__item">
								
									<a href="index.php" class="m-nav__link">
								
										<span class="m-nav__link-text">Volver a la home</span>
									
									</a>
								</li>

							</ul>
	
						</div>
	
					</div>
	
				</div>

				<div class="m-content">

					<div class="row">

						<div class="col-lg-6">

							<div class="m-portlet m-portlet--full-height">

								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">

										<div class="m-portlet__head-title">

											<h3 class="m-portlet__head-text">Soporte Técnico</h3>
										
										</div>
							
									</div>
							
								</div>
							
								<div class="m-portlet__body">
							
									<div class="m-accordion m-accordion--bordered" id="m_accordion_2" role="tablist">
							
										<div class="m-accordion__item">
							
											<div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_2_item_1_head" data-toggle="collapse" href="#m_accordion_2_item_1_body" aria-expanded="false">
												
												<span class="m-accordion__item-icon"><i class="fas fa-phone"></i></span>
												
												<span class="m-accordion__item-title">Soporte telefónico</span>
												
												<span class="m-accordion__item-mode"><i class="la la-plus"></i> </span>
											
											</div>
											
											<div class="m-accordion__item-body collapse" id="m_accordion_2_item_1_body" role="tabpanel" aria-labelledby="m_accordion_2_item_1_head" data-parent="#m_accordion_2">
											
												<span>
													Ante cualquier duda o consulta sobre el funcionamiento del administrador de contenidos, podés llamar a las oficinas de Aditivo Interactive Group S.A de <b>lunes a viernes</b> de <b>9 AM a 18 PM</b> al <b><a href="tel:+54 9 36538006">+54 9 36538006</a></b>

												</span>
											
											</div>
									
										</div>

										<div class="m-accordion__item">
		
											<div class="m-accordion__item-head collapsed" role="tab" id="
											m_accordion_2_item_3_head" data-toggle="collapse" href="#m_accordion_2_item_3_body" aria-expanded="false">
												
												<span class="m-accordion__item-icon">
												
													<i class="fas fa-envelope"></i>
												
												</span>
												<span class="m-accordion__item-title">Soporte por correo electrónico
												</span>
												
												<span class="m-accordion__item-mode"><i class="la la-plus"></i></span>
											</div>
											
											<div class="m-accordion__item-body collapse" id="m_accordion_2_item_3_body" role="tabpanel" aria-labelledby="m_accordion_2_item_3_head" data-parent="#m_accordion_2">
												
												<span>
													En caso de que necesites soporte técnicos con el administrador de contenidos, podés escribirnos a <b><a href="mailto:info@aditivointeractivegroup.com">info@aditivointeractivegroup.com</a></b>
												</span>
											</div>
										</div>
									
										<div class="m-accordion__item">
									
											<div class="m-accordion__item-head collapsed" role="tab" id="
											m_accordion_2_item_2_head" data-toggle="collapse" href="#m_accordion_2_item_2_body" aria-expanded="false">
												
												<span class="m-accordion__item-icon"><i class="fas fa-map-marked-alt"></i>
										
												</span>
										
												<span class="m-accordion__item-title">Visitanos</span>

												<span class="m-accordion__item-mode"><i class="la la-plus"></i>
										
												</span>
										
											</div>
										
											<div class="m-accordion__item-body collapse" id="m_accordion_2_item_2_body" role="tabpanel" aria-labelledby="m_accordion_2_item_2_head" data-parent="#m_accordion_2">
										
												<span>
													
													Las oficinas de Aditivo Interactive Group S.A se encuentran en la ciudad de La Plata.
		
												</span>
		
											</div>
		
										</div>
		
									</div>

								</div>
						
							</div>

						</div>

						<div class="col-lg-6">

							<div class="m-portlet m-portlet--full-height">

								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">

										<div class="m-portlet__head-title">

											<h3 class="m-portlet__head-text">Seguridad</h3>
										
										</div>
							
									</div>
							
								</div>
							
								<div class="m-portlet__body">
							
									<div class="m-accordion m-accordion--bordered" id="m_accordion_3" role="tablist">
							
										<div class="m-accordion__item">
							
											<div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_1_head" data-toggle="collapse" href="#m_accordion_3_item_1_body" aria-expanded="false">
												
												<span class="m-accordion__item-icon"><i class="fas fa-key"></i></span>
												
												<span class="m-accordion__item-title">Contraseñas y claves de acceso</span>
												
												<span class="m-accordion__item-mode"><i class="la la-plus"></i> </span>
											
											</div>
											
											<div class="m-accordion__item-body collapse" id="m_accordion_3_item_1_body" role="tabpanel" aria-labelledby="m_accordion_3_item_1_head" data-parent="#m_accordion_3">
											
												<span>
													Para tu seguridad, se emplea un sistema de hash / salt (Key derivation function - KDFs) para el almacenamiento y la verificación de contraseñas en cualquier proceso de login / recuperación de contraseñas / credenciales. Esto garantiza que solamente vos concocés y tenés acceso a tus claves.
												</span>
											
											</div>
									
										</div>

										<div class="m-accordion__item">
		
											<div class="m-accordion__item-head collapsed" role="tab" id="
											m_accordion_3_item_3_head" data-toggle="collapse" href="#m_accordion_3_item_3_body" aria-expanded="|lse">
												
												<span class="m-accordion__item-icon">
												
													<i class="fab fa-expeditedssl"></i>
												
												</span>
												<span class="m-accordion__item-title">HTTPS / SSL
												</span>
												
												<span class="m-accordion__item-mode"><i class="la la-plus"></i></span>
											</div>
											
											<div class="m-accordion__item-body collapse" id="m_accordion_3_item_3_body" role="tabpanel" aria-labelledby="m_accordion_3_item_3_head" data-parent="#m_accordion_3">
												
												<span>
													Este administrador de contenidos utiliza HTTPS como protocolo, por lo que el envío y recepción de datos está cifrado y se evita el envío en formato plaintext. 
													De este modo, se bloquea cualquier técnica de eavesdropping / escucha. Del mismo modo, este certificado SSL es efectivo contra wiretapping / packet sniffing.
												</span>
											</div>
										</div>
									
										<div class="m-accordion__item">
									
											<div class="m-accordion__item-head collapsed" role="tab" id="
											m_accordion_3_item_2_head" data-toggle="collapse" href="#m_accordion_3_item_2_body" aria-expanded="false">
												
												<span class="m-accordion__item-icon"><i class="fas fa-toggle-on"></i>
										
												</span>
										
												<span class="m-accordion__item-title">Remember me / persistencia de datos</span>

												<span class="m-accordion__item-mode"><i class="la la-plus"></i>
										
												</span>
										
											</div>
										
											<div class="m-accordion__item-body collapse" id="m_accordion_3_item_2_body" role="tabpanel" aria-labelledby="m_accordion_3_item_2_head" data-parent="#m_accordion_3">
										
												<span>
													
													Si bien se emplean cookies para persistencia / recordar al usuario, se los emplea por medio del mismo sistema de Hash / Salt. Esto aporta una nueva capa de seguridad que evita vulnerabilidades siempre y cuando uses este administrador siempre desde un mismo equipo.
		
												</span>
		
											</div>
		
										</div>
		
									</div>

								</div>
						
							</div>

						</div>

						<div class="col-lg-6">

							<div class="m-portlet m-portlet--full-height">

								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">

										<div class="m-portlet__head-title">

											<h3 class="m-portlet__head-text">Imágenes</h3>
										
										</div>
							
									</div>
							
								</div>
							
								<div class="m-portlet__body">
							
									<div class="m-accordion m-accordion--bordered" id="m_accordion_3" role="tablist">
							
										<div class="m-accordion__item">
							
											<div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_4_item_1_head" data-toggle="collapse" href="#m_accordion_4_item_1_body" aria-expanded="false">
												
												<span class="m-accordion__item-icon"><i class="fas fa-images"></i></span>


												<span class="m-accordion__item-title">Extensiones de imágenes permitidas</span>
												
												<span class="m-accordion__item-mode"><i class="la la-plus"></i> </span>
											
											</div>
											
											<div class="m-accordion__item-body collapse" id="m_accordion_4_item_1_body" role="tabpanel" aria-labelledby="m_accordion_4_item_1_head" data-parent="#m_accordion_3">
											
												<span>
													La plataforma permite el upload/subida de archivos en los siguientes formatos: <b>jpg</b>, <b>jpeg</b>, <b>png</b> y <b>gif</b>. Los servidores harán las conversiones necesarias para optimizar las imágenes para la web / apps.
												</span>
											
											</div>
									
										</div>

										<div class="m-accordion__item">
		
											<div class="m-accordion__item-head collapsed" role="tab" id="
											m_accordion_4_item_2_head" data-toggle="collapse" href="#m_accordion_4_item_2_body" aria-expanded="|lse">
												
												<span class="m-accordion__item-icon">
												
													<i class="fas fa-file-image"></i>
												
												</span>
												<span class="m-accordion__item-title">Tamaños de imágenes
												</span>
												
												<span class="m-accordion__item-mode"><i class="la la-plus"></i></span>
											</div>
											
											<div class="m-accordion__item-body collapse" id="m_accordion_4_item_2_body" role="tabpanel" aria-labelledby="m_accordion_4_item_2_head" data-parent="#m_accordion_3">
												
												<span>
													Se permite el upload de archivos de <b>hasta 20 MB</b>. No es necesario optimizarlas para web: el servidor cuenta con un sistema de resize/conversión que se encarga de ello.
												</span>
											</div>
										</div>
									
										<div class="m-accordion__item">
									
											<div class="m-accordion__item-head collapsed" role="tab" id="
											m_accordion_4_item_3_head" data-toggle="collapse" href="#m_accordion_4_item_3_body" aria-expanded="false">
												
												<span class="m-accordion__item-icon">

													<i class="far fa-window-maximize"></i>
										
												</span>
										
												<span class="m-accordion__item-title">Retina / 4K</span>

												<span class="m-accordion__item-mode"><i class="la la-plus"></i>
										
												</span>
										
											</div>
										
											<div class="m-accordion__item-body collapse" id="m_accordion_4_item_3_body" role="tabpanel" aria-labelledby="m_accordion_4_item_3_head" data-parent="#m_accordion_3">
										
												<span>
													
													El sitio está optimizado para mostrar imágenes para <b>retina display</b> y <b>pantallas 4k</b>. Es por este motivo que el administrador requiere de imágenes grandes (2000x1000) como punto de partida.<br><br>A partir de la imagen principal, se generan una serie de resizes, los cuales se emplean para distintos dispositivos, distintas resoluciones y distintas situaciones:<br><br>

													<h5>retina:</h5>

													<b>retina original: </b> filename_original@2x.jpeg<br>
													<b>retina original square: </b> filename_original_sq@2x.jpeg<br>
													<b>retina medium: </b> filename_medium@2x.jpeg<br>
													<b>retina medium: </b> filename_medium@2x.jpeg<br>
													<b>retina small: </b> filename_small@2x.jpeg<br>
													<b>retina small square: </b> filename_small_sq@2x.jpeg<br>

													<br>

													<h5>normal:</h5>

													<b>normal original: </b> filename_original.jpeg<br>
													<b>retina original square: </b> filename_original_sq.jpeg<br>
													<b>normal medium: </b> filename_medium.jpeg<br>
													<b>normal medium: </b> filename_medium.jpeg<br>
													<b>normal small: </b> filename_small.jpeg<br>
													<b>normal small square: </b> filename_small_sq.jpeg<br>

		
												</span>
		
											</div>
		
										</div>
		
									</div>

								</div>
						
							</div>

						</div>

						<div class="col-lg-6">

							<div class="m-portlet m-portlet--full-height">

								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">

										<div class="m-portlet__head-title">

											<h3 class="m-portlet__head-text">Videos</h3>
										
										</div>
							
									</div>
							
								</div>
							
								<div class="m-portlet__body">
							
									<div class="m-accordion m-accordion--bordered" id="m_accordion_3" role="tablist">
							
										<div class="m-accordion__item">
							
											<div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_5_item_1_head" data-toggle="collapse" href="#m_accordion_5_item_1_body" aria-expanded="false">
												
												<span class="m-accordion__item-icon">

													<i class="fab fa-youtube"></i>

												</span>

												<span class="m-accordion__item-title">Videos / YouTube</span>
												
												<span class="m-accordion__item-mode"><i class="la la-plus"></i> </span>
											
											</div>
											
											<div class="m-accordion__item-body collapse" id="m_accordion_5_item_1_body" role="tabpanel" aria-labelledby="m_accordion_5_item_1_head" data-parent="#m_accordion_3">
											
												<span>
													La plataforma permite hacer uso de la API de <b>YouTube</b> y tomar los videos de dicha plataforma. De esta se evita el upload de videos en dos oportunidades, y se toma ventaja de todo lo vinculado a <b>Search Engine Optimization</b> y <b>métricas de los videos</b>.
												</span>
											
											</div>
									
										</div>

									</div>

								</div>
						
							</div>

						</div>
				
					</div>

				</div>
			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light">

		<div class="m-quick-sidebar__content m--hide">
			
			<span id="m_quick_sidebar_close" class="m-quick-sidebar__close">

				<i class="la la-close"></i>

			</span>
			
		</div>
		
	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
		
		<i class="la la-arrow-up"></i>
		
	</div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/dashboard.js" type="text/javascript"></script>

</body>

</html>
