//-----------------------------------------------------------------------------
//------------------------------------------------------------------- constants

const PRIVATE_DIR = "private/";

var name;

var surname;

var email;

var priority;

//-----------------------------------------------------------------------------
//------------------------------------------------------------------- variables

var AdminAdd = function () {

    /**
    *
    * handleValidation();
    * maneja la validacion del form para dar de alta un nuevo usuario...
    */

    var handleValidation = function() {

        var form = $('#new-admin-form');

        var error = $('.alert-danger', form);

        var success = $('.alert-success', form);

        form.validate({

            errorElement: 'span',
            
            errorClass: 'help-block help-block-error',
            
            focusInvalid: false,
            
            ignore: "",
            
            rules: {

                userEmail: { required:true, email:true},

                surname:{ required:true },

                name:{ required:true },

                radio1:{ required: true }

            },

            messages: {

                userEmail: "El campo <b>Email</b> es obligatorio.",

                surname: "El campo <b>Apellido</b> es obligatorio.",

                name: "El campo <b>Nombre</b> es obligatorio.",

                radio1: "Debes definir la <b>Prioridad</b> del nuevo administrador."

            },

            invalidHandler: function (event, validator) {      

                success.hide();

                error.show();

                App.scrollTo(error, -200);
            },

            errorPlacement: function (error, element) {

                var icon = $(element).parent('.input-icon').children('i');

                icon.removeClass('fa-check').addClass("fa-warning");  

                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});

                if (element.is(':radio')) {

                    error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));

                }else{

                    error.insertAfter(element);
                }

            },

            highlight: function (element) { $(element).closest('.form-group').removeClass("has-success").addClass('has-error');  },

            unhighlight: function (element) { $(element).closest('.form-group').removeClass('has-error');  },

            success: function (label, element) {

                var icon = $(element).parent('.input-icon').children('i');

                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');

                icon.removeClass("fa-warning").addClass("fa-check");
            },

            submitHandler: function (form) {

                success.hide();

                error.hide();

                name = $('#user-name').val();

                surname = $('#user-surname').val();

                email = $('#user-email').val();

                priority = document.querySelector('input[name="radio1"]:checked').value;

                addNewAdmin(name, surname, email, priority);

            }

        });
    }

    /**
    *
    * addNewAdmin();
    * respuestas del server:
    * 1 - faltan datos o error.
    * 2 - ya hay un usuario registrado con ese email.
    * 3 - hay un usuario marcado como pending.
    * 4 - se dio de alta al usuario.
    */

    var addNewAdmin = function(name, surname, email, priority){

        $.blockUI({ message: 'CREANDO NUEVO ADMINISTRADOR...' ,

            css: {border:'0px solid #FFFFFF',cursor:'wait',backgroundColor:'none', color:'#FFF'},

            overlayCSS:  { backgroundColor: '#000000',opacity:0.5,cursor:'wait'} 

        });

        $.ajax({

            type: "post",

            dataType: "json",

            data: {name:name, surname:surname, email:email, priority:priority, refererId:refererId},

            url: PRIVATE_DIR + 'actions/users-management/admin_add.php',

            error: function(xhr, status, error) {

                var err = eval("(" + xhr.responseText + ")");

            },

            success: function(data) {

                $.unblockUI();

                console.log("data!" + data.code);

                console.log("data!" + data.msg);

                $('#new-admin-form')[0].reset();

                if(data.code == 1){

                    //1 - missing info / error...

                }else if(data.code == 2){

                    //marcado como registrado...

                    adviceUserAlreadyExists(email, name, surname);

                }else if(data.code == 3){

                     adviceUserIsPending(email, name, surname);

                }else{

                    adviceInvitationWasSent(email, name, surname);

                    //se dio de alta...
                }
            }

        });

    }

    /**
    *
    * adviceInvitationWasSent(email, name, surname);
    * @param email - el email del usuario...
    * @param name - el nombre del usuario...
    * @param surname - el apellido del usuario...
    *
    * hacer uso de un modal indicandole al usuario que se envio la invitacion y 
    * definir el proximo paso: invitar a otro o redireccionar / salis...
    */

    var adviceInvitationWasSent = function(email, name, surname){

        var string = "Se acaba de enviar la invitación de administrador a <b>" + email + "</b><br><br>Avisale a <b>" + name + " " + surname + "</b> que revise su casilla de correo y que no olvide chequear los mensajes no deseados y la casilla de spam.<br>";

        bootbox.confirm({

            message: string,

            closeButton: false,

            buttons: {

                confirm: {

                    label: 'INVITAR A OTRO USUARIO',

                    className: 'btn-success'

                },

                cancel: {

                    label: 'SALIR',

                    className: 'btn-danger'

                }

            },
            callback: function (result) {

                if(result == true){

                    // $('input:text:visible:first').focus();

                }else{

                    doExit();

                }

            }

        });
    }

    /**
    *
    * doExit();
    * salir...
    */

    var doExit = function(){

        $.blockUI({ message: 'REDIRECCIONANDO...' ,

            css: {border:'0px solid #FFFFFF',cursor:'wait',backgroundColor:'none', color:'#FFF'},

            overlayCSS:  { backgroundColor: '#000000',opacity:0.5,cursor:'wait'} 

        });

        $('#new-admin-form')[0].reset();

        window.location.assign("index.php");

    }

    /**
    *
    * adviceUserAlreadyExists(email, name, surname);
    * @param email - el email del usuario...
    * @param name - el nombre del usuario...
    * @param surname - el apellido del usuario...
    *
    * hacer uso de un modal indicandole al usuario que a quien esta intentando invitar
    * ya es un usuario que se encuentra dado de alta...
    */

    var adviceUserAlreadyExists = function(email, name, surname){

        $('#new-admin-form')[0].reset();

        var string = "El email <b>" + email + "</b> ya se encuentra registrado en el sistema.<br><br>En caso de que <b>"  + name + " " + surname + "</b> haya olvidado su contraseña, recordale que la puede restablecer desde el siguiente link:<br><br> <a target = '_blank' href='http://localhost/shoa-museum/admin/login.php?section=recover'>http://localhost/shoa-museum/admin/login.php?section=recover</a>";

        bootbox.alert(string);
    }

    /**
    *
    * adviceUserIsPending();
    */

    var adviceUserIsPending = function(email, name, surname){

        var string = "El email <b>" + email + "</b> ya fue invitado previamente a participar, pero aun no se dio de alta.<br><br> ¿Querés que le reenviemos la invitación? Recuerdale a <b>" + name + " " + surname + "</b> que revise el correo no deseado.";

        bootbox.confirm({

            message: string,

            closeButton: false,

            buttons: {

                confirm: {

                    label: 'SI, REENVIAR INVITACION',

                    className: 'btn-success'

                },

                cancel: {

                    label: 'SALIR',

                    className: 'btn-danger'

                }

            },
            callback: function (result) {

                if(result == true){

                    resendNotification(email, name, surname);

                }else{

                    doExit();

                }

            }

        });

    }

    /**
    *
    * resendNotification(email, name, surname);
    * @param email - el email del usuario...
    * @param name - el nombre del usuario...
    * @param surname - el apellido del usuario...
    *
    * reenviar...
    */

    var resendNotification = function(email, name, surname){

        $.blockUI({ message: 'REENVIANDO NOTIFICACIÓN...' ,

            css: {border:'0px solid #FFFFFF',cursor:'wait',backgroundColor:'none', color:'#FFF'},

            overlayCSS:  { backgroundColor: '#000000',opacity:0.5,cursor:'wait'} 

        });

        $.ajax({

            type: "post",

            dataType: "json",

            data: {name:name, surname:surname, email:email, refererId:refererId, priority:priority},

            url: PRIVATE_DIR + 'actions/users-management/resend_admin_notification.php',

            error: function(xhr, status, error) { var err = eval("(" + xhr.responseText + ")"); },

            success: function(data) {

                $.unblockUI();

                console.log(data);

            }

        });

    }

    /**
    *
    * addListeners();
    * eventos...
    */

    var addListeners = function() { $(".exit-button").click(function() { window.location.assign("index.php"); } ); }

    return {

        init: function () {

            handleValidation();

            addListeners();

            bootbox.alert("opss! :) Esta página no va a ser 100% funcional hasta que el admin no tenga un dominio / hosting final. No invitar a nadie por el momento.");
        }

    };

}();

jQuery(document).ready(function() { AdminAdd.init(); } );