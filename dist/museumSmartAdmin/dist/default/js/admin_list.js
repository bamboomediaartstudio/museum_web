//-----------------------------------------------------------------------------
//------------------------------------------------------------------- constants

const PRIVATE_DIR = "private/";

var name;

var surname;

var email;

var priority;

//-----------------------------------------------------------------------------
//------------------------------------------------------------------- variables

var AdminList = function () {

    /**
    *
    * deleteAdmin();
    * elimina una propiedad...
    */

    var deleteAdmin = function(id){

        $.blockUI({ message: 'ELIMINANDO ADMINISTRADOR...' ,

            css: {border:'0px solid #FFFFFF',cursor:'wait',backgroundColor:'none', color:'#FFF'},

            overlayCSS:  { backgroundColor: '#000000',opacity:0.5,cursor:'wait'} 

        });

        $.ajax({

            type: "post",

            dataType: "json",

            data: {id:id},

            url: PRIVATE_DIR + 'actions/admin-management/delete_admin.php',

            error: function(xhr, status, error) { var err = eval("(" + xhr.responseText + ")"); },

            success: function(data) {

                $('#item-' + id).hide('slow', function() {
                    
                });

                toastr.info("Se eliminó al usuario de la lista de administradores.");

                $.unblockUI();


            }

        });

    }

    var changeAdminPriority = function(adminId, adminPriority){

        $.ajax({

            type: "post",

            dataType: "json",

            data: {id:adminId, priority:adminPriority},

            url: PRIVATE_DIR + 'actions/admin-management/change_admin_priority.php',

            error: function(xhr, status, error) { var err = eval("(" + xhr.responseText + ")"); },

            success: function(data) {

                toastr.info("Se modificó la prioridad del administrador.");

            }

        });
    }

    /**
    *
    * addListeners();
    * eventos...
    */

    var addListeners = function() { 

         $('input[type=radio]').change(function() {

            var adminId = $(this).attr('name');

            var adminPriority = $(this).attr('value');

            changeAdminPriority(adminId, adminPriority);

         });

        $(".exit-button").click(function() { window.location.assign("index.php"); } ); 

        $( ".master-btn" ).click(function() {

            var id = $(this).attr('id').split('-')[1];

            var fullName = $(this).attr('data-fullname');

             bootbox.alert("<b>" + fullName.toUpperCase() + "</b> Es un usuario <b>master</b> o de prioridad alta!<br><br>Este tipo de usuario no se puede ser eliminado por otros usuarios, ni se le pueden cambiar los privilegios.");


        });

        $( ".delete-btn" ).click(function() {

            var id = $(this).attr('id').split('-')[1];

            var fullName = $(this).attr('data-fullname');

            bootbox.dialog({

                message: '<b>¡ATENCIÓN, ' + userName.toUpperCase() +'!</b></b><br><br>Estás a punto de eliminar de manera definitiva a <b>' + fullName + '</b> del sistema, y esta acción no se puede deshacer.<br><br>Recordá que podés modificar su prioridad a <b>baja</b>, lo que le dará permisos de solo lectura, lo que le permitirá seguir usando el administrador pero no generar contenido.<br><br>En caso de que deseas eliminarlo de manera definitiva, presiona <b>CONTINUAR</b>',

                buttons: {

                    success: {

                        label: "CONTINUAR",

                        className: "btn-success",

                        callback: function() { 

                            deleteAdmin(id);

                        }

                    },

                    danger: {

                        label: "CANCELAR",

                        className: "btn-danger",

                        callback: function() {

                        }

                    }

                }

            });
        });

    }

    return {

        init: function () {

            //handleValidation();

            toastr.options = {"closeButton": true, "debug": false, "newestOnTop": false, "progressBar": false, "positionClass": "toast-bottom-left",

                "preventDuplicates": false, "onclick": null, "showDuration": "300", "hideDuration": "1000", "timeOut": "5000", "extendedTimeOut": "1000",

                "showEasing": "swing", "hideEasing": "linear", "showMethod": "fadeIn", "hideMethod": "fadeOut"}

            addListeners();
        }

    };

}();

jQuery(document).ready(function() { AdminList.init(); } );