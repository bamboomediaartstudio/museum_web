/**
*
* Login();
* login a la pagina master del admin...
*/

const PRIVATE_DIR = "private/";

var Login = function() {

    /**
    *
    * getURLParameter();
    * chequear la URL para cambio de secciones y demas...
    * a esto habría que mandarlo a un helper...
    */

    var getURLParameter = function(name) {

        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;

    }

    /**
    *
    * handleRecoverPassword();
    * envio de mail para recuperar el password...
    */

    var handleRecoverPassword = function() {

        var form = $('.forget-form');

        form.validate({

            errorElement: 'span',

            errorClass: 'help-block help-block-error',

            focusInvalid: false,

            ignore: "",

            messages: {
                
                email:{ 
                
                    required: "Ingresá tu email.",

                    email: "Ingresá una dirección válida.",

                }
            
            },

            rules: {
                
                email: {
                    
                    required: true,

                    email:true
                }
            },

            invalidHandler: function (event, validator) {       
                
                //success.hide();
                
                //error.show();
                
                //App.scrollTo(error, -200);
            },

            highlight: function (element) {

                $(element).closest('.form-group').addClass('has-error');
            
            },

            unhighlight: function (element) {
                
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {
                
                label.closest('.form-group').removeClass('has-error');
            },

            submitHandler: function (form) {

                var email = $('#recover-email').val();

                $.blockUI({ message: 'ENVIANDO EMAIL...' ,

                    css: {border:'0px solid #FFFFFF',cursor:'wait',backgroundColor:'none', color:'#FFF'},

                    overlayCSS:  { backgroundColor: '#000000',opacity:0.5,cursor:'wait'} 

                });

                $.ajax({

                    type: "post",

                    dataType: "json",

                    data: {email:email},

                    url: PRIVATE_DIR + 'actions/login-management/reset_user_password.php',
                    
                    error: function(xhr, status, error) {

                        var err = eval("(" + xhr.responseText + ")");

                        console.log("error: " + error);

                    },

                    success: function(data) {

                        console.log("success: " + data.code + " - " + data.msg);

                        $.unblockUI();

                        var msg = 'Se envió un email a la dirección que indicaste para resetear la contraseña.'; 

                        bootbox.dialog({
                      
                              message: msg,
                              
                              buttons: {

                                success: {

                                    label: "ACEPTAR",

                                    className: "btn-success",

                                    callback: function() { 

                                        //window.location.reload(true);

                                    }
                                    
                                }

                            }
                        });
                    }
                })
            }
        });
    }

    /**
    *
    * handleLogin();
    * todas las funciones para el manejo del login...
    */

    var handleLogin = function() {

        $('.login-form').validate({
            
            errorElement: 'span',
            
            errorClass: 'help-block',
            
            focusInvalid: false,
            
            rules: {
                
                email: {
                    
                    required: true,

                    email:true
                
                },
                
                password: {
                    
                    required: true
                },
                
                remember: {
                    
                    required: false
                
                }
            },

            messages: {
                
                email: {
                    
                    required: "Campo obligatorio."
                
                },
                
                password: {
                    
                    required: "Password is required."
                
                }
            },

            invalidHandler: function(event, validator) { 

                $('.login-form').show();

                $('.alert-danger').fadeIn(200).delay(3000).fadeOut(1000, function() {

                    $('.alert-danger').hide();
                    
                });
            
            },

            highlight: function(element) {
                
                $(element).closest('.form-group').addClass('has-error');

            },

            unhighlight: function (element) {
                
                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function(label) {
                
                label.closest('.form-group').removeClass('has-error');
                
                label.remove();
            
            },

            errorPlacement: function(error, element) {
                
                error.insertAfter(element.closest('.input-icon'));
            
            },

            submitHandler: function(form) {

                $.blockUI({ message: 'INICIANDO SESIÓN...' ,

                    css: {border:'0px solid #FFFFFF',cursor:'wait',backgroundColor:'none', color:'#FFF'},

                    overlayCSS:  { backgroundColor: '#000000',opacity:0.5,cursor:'wait'} 

                });

                var l = Ladda.create( document.querySelector('.send-data'));

                l.start();

                var email = $('#email').val();

                var pass = $('#password').val();

                var rememberUser = ($("input[name='remember']").is(":checked")) ? true : false;

                $.ajax({

                    type: "post",

                    dataType: "json",

                    data: {email : email, password:pass, rememberUser:rememberUser},

                    url: PRIVATE_DIR + 'actions/login-management/login.php',

                    error: function(xhr, status, error) {

                        var err = eval("(" + xhr.responseText + ")");

                        console.log("error: " + error);

                    },

                    success: function(data) {

                        $.unblockUI();

                        if(data.code == 1){

                            window.location.assign("index.php");

                        }else{

                            $('.modal-body p').text(data.msg);

                            $('#login-modal').modal();

                            var l = Ladda.create( document.querySelector('.send-data'));

                            l.stop();

                        }
                    }
                });
            }
        });

        $('.login-form input').keypress(function(e) {
            
            if (e.which == 13) {
                
                if ($('.login-form').validate().form()) {
                    
                    $('.login-form').submit();
                
                }
                
                return false;
            }
        
        });

        $('.forget-form input').keypress(function(e) {
            
            if (e.which == 13) {
                
                if ($('.forget-form').validate().form()) {
                    
                    $('.forget-form').submit();
                
                }
                
                return false;
            
            }
        
        });

        $('#forget-password').click(function(){
            
            $('.login-form').hide();
            
            $('.forget-form').show();
        
        });

        $('#back-btn').click(function(){
            
            $('.login-form').show();
            
            $('.forget-form').hide();
        });
    }


    /**
    *
    * backgroundSlider();
    * crea e inicializa el slider de imagenes...
    */

    var backgroundSlider = function(){

        $('.login-bg').backstretch(["img/login/bg1.jpg", "img/login/bg2.jpg", "img/login/bg3.jpg"], {

        fade: 1000,

        duration: 8000}); 
    }
    

    return {
        
        init: function() {

            handleLogin();

            handleRecoverPassword();

            backgroundSlider();

            var actualSection;

            actualSection = getURLParameter('section');

            if(actualSection == 'recover'){

                $('.login-form').hide();

                $('.forget-form').show();

            }else{

                $('.login-form').show();

                $('.forget-form').hide();

            }

        }

    };

}();

jQuery(document).ready(function() { Login.init(); });