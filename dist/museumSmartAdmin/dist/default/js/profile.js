/**
*
* Profile();
* Todo lo relacionado al perfil de usuario: update de datos, de
* passwords, etc...
*/


//-----------------------------------------------------------------------------
//------------------------------------------------------------------- variables

var jcrop_api;                                              //api para crop

var boundX;                                                 //limite en width para el crop

var boundY;                                                 //limite en height para el crop

var newCanvas = document.createElement('canvas');           //para copiar pixeles de preview...

newCanvas.width = 500;                                      //max bounds tambien para canvas...

var maxWidth = 500;                                         //maximo permitido de crop en W

var minWidth = 100;                                         //minimo permitido de crop en W...

var maxHeight = 500;                                        //maximo permitido de crop en H...

var minHeight = 100;                                        //minimo permitido de crop en H...

var canvasData;                                             //la data de los pixeles...

var sendX;                                                  //data de cropeo para server...                                                    

var sendY;

var sendX2;

var sendY2;

var sendWidth;

var sendHeight;

var originalForm;                                           //serialize del form para comparaciones...

var isUpdating = false;                                     //flag para saber cuando finalizo de updatear...

var isActualImage = true;                                   //flag para saber si esta en la default o en otra...

//-----------------------------------------------------------------------------
//------------------------------------------------------------------- constants

const PRIVATE_DIR = "private/";

/**
*
* Profile();
* constructor...
*/

var Profile = function() {

    /**
    *
    * handlePasswordStrengthChecker();
    * asignar un handler para el trackeo del pass. Es bastante floja
    * la documentación y le falta un poco de laburo. Incompatibilidad con distintos
    * bootrstraps...
    */

    var handlePasswordStrengthChecker = function () {

      var initialized = false;
        
        var input = $("#newpassword");

        if (initialized === false) {

            options = {

                common: {

                    minChar:8,

                    zxcvbn:false
                },

                rules: {

                    activated: {

                        wordTwoCharacterClasses: true,

                        wordRepetitions: true

                    }
                },

                ui: {

                    colorClasses: ["danger", "danger", "danger", "warning", "warning", "success"],

                    showProgressBar:true,

                    showVerdictsInsideProgressBar: false,

                    showVerdicts: false,

                    scores: [0, 14, 26, 38, 50],
                    
                    verdicts: ["Débil", "Normal", "Media", "Fuerte", "Muy Fuerte"],

                    progressBarEmptyPercentage: 0,

                    showErrors: false

                }

            };
            
            input.pwstrength(options);

            initialized = true;
        }
    }



    /**
    *
    * handlePasswordValidation();
    * para validacion de password. Lo unico distinto es que tiene un metodo 'notEqualTo'
    * que se le añadio...
    *
    * functionamiento basico:
    *
    * - la contraseña no puede ser menor a 8 caracteres...
    * - la contraseña no puede ser igual a la anterior...
    * - tiene que repetir dos veces el mismo pass...
    */

    var handlePasswordValidation = function(){

        jQuery.validator.addMethod("notEqualTo",

            function(value, element, param) {

                var notEqual = true;

                value = $.trim(value);

                for (i = 0; i < param.length; i++) {

                    if (value == $.trim($(param[i]).val())) { notEqual = false; }

                }

                return this.optional(element) || notEqual;

            },

            "..."
            );
        
        var form = $('#change-password');

        var error = $('.alert-danger', form);
        
        var success = $('.alert-success', form);

        form.validate({

            errorElement: 'span',

            errorClass: 'help-block help-block-error',

            focusInvalid: false,

            ignore: "",

            messages: {

                actualpassword:{ 

                    required: 'Debes indicar tu contraseña actual.'

                },

                newpassword:{

                   required: "Debes indicar la nueva contraseña.",

                   minlength: "La nueva contraseña debe tener al menos 8 caracteres.",

                   notEqualTo: "La contraseña nueva no puede ser igual igual a la actual."
               },

               repeatpassword:{

                   required: "Campo incompleto.",

                   minlength: "La nueva contraseña debe tener al menos 8 caracteres.",

                   equalTo: "Las contraseñas no coinciden."
               }

           },

           rules: {

            actualpassword: {

                required: true
            },

            newpassword: {

                minlength: 8,

                required: true,

                notEqualTo: ["#actualpassword"]

            },

            repeatpassword: {

                minlength: 8,

                required: true,

                equalTo: "#newpassword"

            }

        },

        invalidHandler: function (event, validator) {       

            success.hide();

            error.show();

            App.scrollTo(error, -200);
        },

        highlight: function (element) {

            $(element).closest('.form-group').addClass('has-error');
            
        },

        unhighlight: function (element) {

            $(element).closest('.form-group').removeClass('has-error');
        },

        success: function (label) {

            label.closest('.form-group').removeClass('has-error');
        },

        submitHandler: function (form) {

            var oldPassword = $('input[name=actualpassword]').val();

            var newPassword = $('input[name=newpassword]').val();

            var id = $(".dropdown-user").attr('id');

            $.blockUI({ message: 'actualizando la contraseña...',

                    css: {border:'0px solid #FFFFFF',cursor:'wait',backgroundColor:'none', color:'#FFF'},

                    overlayCSS:  { backgroundColor: '#000000',opacity:0.5,cursor:'wait'} 

                });

            $.ajax({

                type: "post",

                dataType: "json",

                data: {oldPassword:oldPassword, newPassword:newPassword, id:id},

                url: PRIVATE_DIR + 'actions/profile-management/profile_update_password.php',

                error: function(xhr, status, error) {

                    var err = eval("(" + xhr.responseText + ")");

                },

                success: function(data) {

                    $.unblockUI();

                    $('#change-password')[0].reset();

                    bootbox.dialog({

                      message: data.msg,

                      buttons: {

                        success: {

                            label: (data.code == 4) ? "ACEPTAR" : "VOLVER A INTENTAR",

                            className: (data.code == 4) ? "btn-success" : "btn-danger",

                            callback: function() { 

                                /*if(data.code == 4){

                                    window.location.reload(true);

                                }*/
                            }
                        }

                    }
                });

                }
            })
        }
    });

    }

    /**
    *
    * handleValidation();
    * validaciones del form de update de datos...
    */

    var handleValidation = function() {

        var form = $('#profile-form');

        originalForm = form.serialize();

        var error = $('.alert-danger', form);
        
        var success = $('.alert-success', form);

        form.validate({

            errorElement: 'span',

            errorClass: 'help-block help-block-error',

            focusInvalid: false,

            ignore: "",

            messages: {

                name: 'El campo nombre es obligatorio',

                surname: 'El apellido es obligatorio',

                nickname: 'Necesitamos un nick para identificarte en el sitio'

            },

            rules: {

                name: {

                    minlength: 2,
                    
                    required: true
                },

                surname: {

                    minlength: 2,
                    
                    required: true
                },

                nickname:{

                    minlength: 2,
                    
                    required: true  

                }

            },

            invalidHandler: function (event, validator) {       

                success.hide();
                
                error.show();
                
                App.scrollTo(error, -200);
            },

            highlight: function (element) {

                $(element).closest('.form-group').addClass('has-error');

            },

            unhighlight: function (element) {

                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {

                label.closest('.form-group').removeClass('has-error');
            },

            submitHandler: function (form) {

                error.hide();

                var serializeData = $('#profile-form').serialize();

                if (serializeData == originalForm) { return; }

                $.blockUI({ message: 'actualizando datos de perfil...',

                    css: {border:'0px solid #FFFFFF',cursor:'wait',backgroundColor:'none', color:'#FFF'},

                    overlayCSS:  { backgroundColor: '#000000',opacity:0.5,cursor:'wait'} 

                });

                var name = $('input[name=name]').val();

                var surname = $('input[name=surname]').val();

                var nickname = $('input[name=nickname]').val();

                var bio = $('textarea[name=bio]').val();

                var rol = $('input[name=rol]').val();

                var id = $(".dropdown-user").attr('id');

                $.ajax({

                    type: "post",

                    dataType: "json",

                    data: {name:name, surname:surname, nickname:nickname, bio:bio, id:id, rol:rol},
                    
                    url: PRIVATE_DIR + 'actions/profile-management/profile_update_personal_info.php',

                    error: function(xhr, status, error) {

                      var err = eval("(" + xhr.responseText + ")");
                      
                      alert(err.Message);
                  },

                  success: function(data) {

                   window.location.replace("profile.php");

                }

            });

            }

        });

    }

    /**
    *
    * handleButtonsAndAlerts();
    * botones, alertas, etc...
    */

    var handleButtonsAndAlerts = function() {

        $( ".profile-userpic" ).css( 'cursor', 'pointer' );

        $( ".profile-userpic" ).click(function() { $('.nav-tabs a[href="#avatar"]').tab('show'); });
        
        $( "#cancel-submit, #cancel-submit-description" ).click(function() {

            var serializeData = $('#profile-form').serialize();

            if (serializeData == originalForm) { 

                window.location.replace("index.php");

            }else{

                bootbox.dialog({

                  message: "Editaste el contenido pero no lo guardate. ¿Salir de todos modos?",
                  
                  buttons: {

                    success: {

                        label: "SI, SALIR",

                        className: "btn-success",

                        callback: function() {

                            window.location.replace("index.php");

                        }

                    },

                    danger: {

                        label: "NO, SEGUIR EDITANDO",

                        className: "btn-danger",

                        callback: function() {

                        }

                    }

                }

            });

            }

        });

    }

    /**
    *
    * handleImageCrop();
    * el crop controlado de la imagen...
    */

    var handleImageCrop = function(image, x1, y1, x2, y2) {

        var img = new Image();

        img.onload = function() {

            $.unblockUI();

            $(".jcrop-preview").attr("src", image);

            $("#cropImage").attr("src", image);

            if(jcrop_api != undefined){

                jcrop_api.setImage(image, function(){});

            }

            $preview = $('#preview-pane');

            $pcnt = $('#preview-pane .preview-container');

            $pimg = $('#preview-pane .preview-container img');

            xsize = $pcnt.width();

            ysize = $pcnt.height();

            var imageWidth = img.width;

            var imageHeight = img.height;

            $('#cropImage').Jcrop({bgFade: true, bgOpacity: .3, onSelect: updateOnChange, onRelease: releaseCheck, onChange: updatePreview},

            function(){

                jcrop_api = this;

                boundX = imageWidth;

                boundY = imageHeight;

                jcrop_api.setOptions( { aspectRatio: 1 } );

                jcrop_api.setOptions( {minSize: [ minWidth, minHeight ], maxSize: [ maxWidth, maxHeight ]} );

                jcrop_api.animateTo([x1, y1, x2, y2]);

                $preview.appendTo(jcrop_api.ui.holder);

            });   

        }

        img.src = image;

        function updateOnChange(c){

            if(!isUpdating && isActualImage){

                isUpdating = true;

                $('.update-button').hide();

                $('.update-button').removeClass('hide');

                $('.update-button').fadeIn(300);

            }

            updatePreview(c)
        }

        function updatePreview(c){

            sendX = c.x;

            sendY = c.y;

            sendX2 = c.x2;

            sendY2 = c.y2;

            sendWidth = c.w;

            sendHeight = c.h;

            if (parseInt(c.w) > 0)
            {
                var rx = xsize / c.w;

                var ry = ysize / c.h;

                $pimg.css({

                    width: Math.round(rx * boundX) + 'px',

                    height: Math.round(ry * boundY) + 'px',

                    marginLeft: '-' + Math.round(rx * c.x) + 'px',

                    marginTop: '-' + Math.round(ry * c.y) + 'px'

                });

            }

        }

        function releaseCheck() {  jcrop_api.setOptions({ allowSelect: true });  };

    }

    /**
    *
    * addListeners();
    * los listeners de la seccion...
    */

    var addListeners = function(){

        $('.update-button').click(function(e){

            if(canvasData == undefined){

                var img = new Image();

                img.onload = function(){

                    var canvas = document.createElement('CANVAS');

                    var ctx = canvas.getContext('2d');

                    canvas.height = img.height;

                    canvas.width = img.width;

                    ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

                    canvasData = canvas.toDataURL();

                    canvas = null; 
                    
                    uploadImage();
                }

                img.src = $("#cropImage").attr('src');

            }

        });

        $('.upload-button').click(function(e){

            //bootbox.alert("Subiendo imagen al servidor. Esto puede tardar unos segundos...");

            uploadImage();

        });

        $("#image_file").change(function(e) {

            $('.update-button').addClass('hide');

            isActualImage = false;

            $('.crop-main-container').fadeOut(10);

            $('.fileinput-button').addClass('hide');

            $('.cancel-button').removeClass('hide');

            $('.upload-button').removeClass('hide');

            for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {

                var file = e.originalEvent.srcElement.files[i];

                var img = document.createElement("img");

                var reader = new FileReader();

                reader.onloadend = function() {

                    img.onload = function () {

                        var ctx = newCanvas.getContext('2d');

                        var calculateHeight = 500 * img.height / img.width;

                        newCanvas.height = calculateHeight;

                        ctx.drawImage(img, 0, 0, 500, calculateHeight);

                        boundX = 500;

                        boundY = calculateHeight;

                        canvasData = newCanvas.toDataURL();

                        jcrop_api.setImage(canvasData, function(){});

                        var centerX = maxWidth/2;

                        var centerX1 = centerX - minWidth/2;

                        var centerX2 = centerX + minWidth/2;

                        var centerY = calculateHeight/2;

                        var centerY1 = centerY - minHeight/2;

                        var centerY2 = centerY + minHeight/2;

                        jcrop_api.animateTo([centerX1, centerY1, centerX2, centerY2]);

                        $(".jcrop-preview").attr("src",canvasData);

                        ctx.clearRect(0, 0, newCanvas.width, newCanvas.height);

                        $('.crop-main-container').fadeIn(3000);

                        bootbox.hideAll();
                    }

                    img.src = reader.result;

                }

                reader.readAsDataURL(file);

            }

        });

    }

    /**
    *
    * loadActualImage();
    * carga la data de la imagen actual...
    */

    var loadActualImage = function(){

        var id = $(".dropdown-user").attr('id');

        $.ajax({

            type: "post",

            dataType: "json",

            data: {id:id},

            url: PRIVATE_DIR + 'actions/profile-management/profile_get_image.php',

            error: function(xhr, status, error) {

                var err = eval("(" + xhr.responseText + ")");

            },

            success: function(data) {

                handleImageCrop(data.original_picture, data.x1, data.y1, data.x2, data.y2);

            }

        });

    }

    /**
    *
    * uploadImage();
    * subir la imagen al server. Va en base64, se encodea del otro lado...
    */

    var uploadImage = function(){

        $.blockUI({ message: 'SUBIENDO IMAGEN...' ,

            css: {border:'0px solid #FFFFFF',cursor:'wait',backgroundColor:'none', color:'#FFF'},

            overlayCSS:  { backgroundColor: '#000000',opacity:0.5,cursor:'wait'} 

        });

        var id = $(".dropdown-user").attr('id');

        $.ajax({

            type: "POST",

            dataType:'json',
            
            url: PRIVATE_DIR + 'actions/profile-management/profile_update_image.php',

            data: {id:id, imgBase64: canvasData, x:sendX, y:sendY, x2:sendX2, y2:sendY2, width:sendWidth, height: sendHeight},

            error: function(xhr, status, error) {

                var err = eval("(" + xhr.responseText + ")");

            },

            success: function(data) {

                window.location.replace("profile.php");
            }   

        });

    }


    return {

        init: function() {

            $.blockUI({ message: 'CARGANDO CONTENIDO...' ,

                css: {border:'0px solid #FFFFFF',cursor:'wait',backgroundColor:'none', color:'#FFF'},

                overlayCSS:  { backgroundColor: '#000000',opacity:0.5,cursor:'wait'} 

            });


            if (!jQuery().Jcrop) { return; }

            handlePasswordStrengthChecker();

            handleValidation();

            handlePasswordValidation();

            handleButtonsAndAlerts();

            addListeners();

            loadActualImage();

           $('.nav-tabs a[href="#personal"]').tab('show');

            toastr.options = {"closeButton": true, "debug": false, "newestOnTop": false, "progressBar": false, "positionClass": "toast-bottom-left",

                "preventDuplicates": false, "onclick": null, "showDuration": "300", "hideDuration": "1000", "timeOut": "5000", "extendedTimeOut": "1000",

                "showEasing": "swing", "hideEasing": "linear", "showMethod": "fadeIn", "hideMethod": "fadeOut"}
            
        },
    };

}();

jQuery(document).ready(function() { Profile.init(); });