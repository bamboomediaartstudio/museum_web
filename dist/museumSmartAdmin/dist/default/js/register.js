//-----------------------------------------------------------------------------
//------------------------------------------------------------------- constants

const PRIVATE_DIR = "private/";

var getEmail;

/**
*
* Reset();
* reset password...
*/

var Reset = function() {

    
    var filterSection = function(status){
    
        switch(Number(status)){

            case 0:
            case 1:
            case 2:

            $('#nothing').removeClass('hide');

            break;

            case 3:

            $('#expired').removeClass('hide');

            break;

            case 4:

            $('#reset-password').removeClass('hide');
            
            break;

        }

    }

    /**
    *
    * handlePasswordStrengthChecker();
    * asignar un handler para el trackeo del pass. Es bastante floja
    * la documentación y le falta un poco de laburo. Incompatibilidad con distintos
    * bootrstraps...
    */

    var handlePasswordStrengthChecker = function () {
        
        var initialized = false;
        
        var input = $("#newpassword");

        if (initialized === false) {

            options = {

                common: {

                    minChar:8,

                    zxcvbn:false
                },

                rules: {

                    activated: {

                        wordTwoCharacterClasses: true,

                        wordRepetitions: true

                    }
                },

                ui: {

                    colorClasses: ["danger", "success", "danger", "danger", "danger", "danger"],

                    showProgressBar:true,

                    showVerdictsInsideProgressBar: false,

                    scores: [17, 26, 40, 50],
                    
                    verdicts: ["Débil", "Normal", "Media", "Fuerte", "Muy Fuerte"],

                    progressBarEmptyPercentage: 0,

                    showErrors: false

                }

            };
            
            input.pwstrength(options);

            initialized = true;
        }
    }

    /**
    *
    * handleRegisterForm();
    * form control para pass...
    */

    var handleRegisterForm = function() {

        //-------------------------------------------------------------------------
        //al menos debe contener una mayuscula...

        $.validator.addMethod("atLeastOneUppercaseLetter", function (value, element) {

            return this.optional(element) || /[A-Z]+/.test(value);
        
        }, "La contraseña debe tener al menos una mayúscula.");

        //-------------------------------------------------------------------------
        //al menos tiene que contener un numero...

        $.validator.addMethod("atLeastOneNumber", function (value, element) {

            return this.optional(element) || /[0-9]+/.test(value);

        }, "La contraseña debe tener al menos un número.");

        //-------------------------------------------------------------------------
        //al menos un caracter lowecase...

        $.validator.addMethod("atLeastOneLowercaseLetter", function (value, element) {

            return this.optional(element) || /[a-z]+/.test(value);

        }, "La contraseña debe tener al menos una minúscula.");

        //-------------------------------------------------------------------------
        //exact length...

        jQuery.validator.addMethod("exactlength", function(value, element, param) {

            return this.optional(element) || value.length == param;

        }, "Please enter exactly {0} characters.");

        
        var form = $('#reset-password');

        form.validate({

            errorElement: 'span',

            errorClass: 'help-block',

            focusInvalid: false,

            ignore: "",

            messages: {

                code:{

                    required: "El código es obligatorio. Se te envió por email.",

                    exactlength: "El código es de cuatro caracteres."

                },
                
                newpassword:{

                     required: "Debes indicar la nueva contraseña.",

                     minlength: "La nueva contraseña debe tener al menos 8 caracteres.",

                     atLeastOneNumber: 'La contraseña debe contener al menos un número.',

                     atLeastOneUppercaseLetter: 'La contraseña debe contener al menos una mayúscula.',

                     atLeastOneLowercaseLetter: 'La contraseña debe tener al menos una minúscula.'
                },

                repeatpassword:{

                     required: "Campo incompleto.",

                     equalTo: "Las contraseñas no coinciden."
                }
            
            },

            rules: {

                code:{

                    required:true,

                    exactlength:4
                
                },

                repeatpassword:{

                    equalTo: "#newpassword"
                
                },
                
                newpassword: {
                    
                    minlength: 8,
                    
                    required: true,

                    atLeastOneNumber: true,

                    atLeastOneUppercaseLetter: true,

                    atLeastOneLowercaseLetter: true

                }

            },

            invalidHandler: function (event, validator) {       
                
                //App.scrollTo(error, -200);
            },

            highlight: function (element) { $(element).closest('.form-group').addClass('has-error'); },

            unhighlight: function (element) { $(element).closest('.form-group').removeClass('has-error'); },

            success: function (label) { label.closest('.form-group').removeClass('has-error'); },

            submitHandler: function (form) {

                var pass = $('#newpassword').val();

                var code = $('#code').val();

                console.log(pass, getEmail, code);

                $.ajax({

                    type: "post",

                    dataType: "json",

                    data: {pass:pass, email:getEmail, code:code},

                    url: PRIVATE_DIR + 'actions/users-management/register_new_user.php',

                    error: function(xhr, status, error) { var err = eval("(" + xhr.responseText + ")"); },

                    success: function(data) {

                        console.log(data);
                        
                        console.log(data.code);
                        
                        console.log(data.msg);

                        if(data.code == 1){

                            bootbox.alert('El código que indicaste no coincide con el que te enviamos por email. Por favor, volvé a intentarlo', function(){

                                $('#code').closest('.form-group').addClass('has-error');

                            });

                        }else if(data.code == 2){

                            bootbox.alert('Acabás de ser dado de alta! :)', function(){

                                window.open('login.php');

                            });

                        }

                    }

                });
            }

        });
        
    }

    /**
    *
    * backgroundSlider();
    * crea e inicializa el slider de imagenes...
    */

    var backgroundSlider = function(){

        $('.login-bg').backstretch(["img/login/bg1.jpg", "img/login/bg2.jpg", "img/login/bg3.jpg"], {

        fade: 1000,

        duration: 8000}); 
    }

    /**
    *
    * addEvents();
    * asignar eventos, clicks, etc...
    */

    var addEvents = function(){

        $('.go-to-change').click(function(){ window.open('login.php?section=recover'); });

        $('.go-to-login').click(function(){ window.open('login.php'); });

    }
    

    return {
        
        init: function() {

            handlePasswordStrengthChecker();

            handleRegisterForm();

            backgroundSlider();

            addEvents();

            var status = $('#dom-target').text();

            getEmail = $('#dom-email').text();
            
            filterSection(status);

        }

    };

}();

jQuery(document).ready(function() { Reset.init(); });
