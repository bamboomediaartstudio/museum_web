/**
*
* Profile();
* Todo lo relacionado al perfil de usuario: update de datos, de
* passwords, etc...
*/


//-----------------------------------------------------------------------------
//------------------------------------------------------------------- variables

var jcrop_api;                                              //api para crop

var boundX;                                                 //limite en width para el crop

var boundY;                                                 //limite en height para el crop

var newCanvas = document.createElement('canvas');           //para copiar pixeles de preview...

newCanvas.width = 500;                                      //max bounds tambien para canvas...

var maxWidth = 500;                                         //maximo permitido de crop en W

var minWidth = 100;                                         //minimo permitido de crop en W...

var maxHeight = 500;                                        //maximo permitido de crop en H...

var minHeight = 100;                                        //minimo permitido de crop en H...

var canvasData;                                             //la data de los pixeles...

var sendX;                                                  //data de cropeo para server...                                                    

var sendY;

var sendX2;

var sendY2;

var sendWidth;

var sendHeight;

var form;

var originalForm;                                           //serialize del form para comparaciones...

var isUpdating = false;                                     //flag para saber cuando finalizo de updatear...

var isActualImage = true;                                   //flag para saber si esta en la default o en otra...

var actualId;

var isUsingOtherInternationalDate = false;

var isUsingOtherNationalDate = false;

var isMainEditMode = false;

var minYear =  1900;

var minMonth = 0;

var minDay = 1;

var whiteList;

var editMode;

//-----------------------------------------------------------------------------
//------------------------------------------------------------------- constants

const PRIVATE_DIR = "private/";

/**
*
* Profile();
* constructor...
*/

var Profile = function() {

    /**
    *
    * handleValidationMainForm();
    * validaciones del form de update de datos...
    */

    var handleValidationMainForm = function() {

        form = $('#new-event-form');

        originalForm = form.serialize();

        var error = $('.alert-danger', form);
        
        var success = $('.alert-success', form);

        form.validate({

            errorElement: 'span',

            errorClass: 'help-block help-block-error',

            focusInvalid: false,

            ignore: "",

            messages: {

                eventName: 'El nombre del evento es obligatorio.',

                eventDescription: 'La descripción del evento es obligatoria'


            },

            rules: {

                eventName: {

                    minlength: 2,
                    
                    required: true
                },

                eventDescription: {

                    minlength: 2,

                    required:true
                }

            },

            invalidHandler: function (event, validator) {       

                success.hide();
                
                error.show();
                
                App.scrollTo(error, -200);
            },

            highlight: function (element) {

                $(element).closest('.form-group').addClass('has-error');

            },

            unhighlight: function (element) {

                $(element).closest('.form-group').removeClass('has-error');
            },

            success: function (label) {

                label.closest('.form-group').removeClass('has-error');
            },

            submitHandler: function (form) {

                error.hide();

                var eventName = $('#event-name').val();

                var eventDescription = $('#event-description').val();

                var string = "Estas a punto de dar de alta el evento:<b>" + eventName;

                bootbox.confirm({

                    message: string,

                    closeButton: false,

                    buttons: {

                        confirm: {

                            label: 'CREAR EVENTO',

                            className: 'btn-success'

                        },

                        cancel: {

                            label: 'SALIR',

                            className: 'btn-danger'

                        }

                    },
                    callback: function (result) {

                        if(result == true){

                            createEvent(eventName, eventDescription, getEventDate());
                        }
                    }

                });
            }

        });

    }

    var handleValidationInternationalForm = function(){

        var form_3 = $('#international-form');

        var error_3 = $('.alert-danger-3', form_3);
        
        var success_3 = $('.alert-success-3', form_3);

        form_3.validate({errorElement: 'span', errorClass: 'help-block help-block-error', focusInvalid: false, ignore: "",

            messages: { eventInternationalDescription: 'La descripción del panorama internacional del evento es obligatoria.' },

            rules: { eventInternationalDescription: { minlength: 10, required: true } },

            invalidHandler: function (event, validator) {       

                success_3.hide();
                
                error_3.show();
                
                App.scrollTo(error_3, -200);
            },

            highlight: function (element) { $(element).closest('.form-group').addClass('has-error'); },

            unhighlight: function (element) { $(element).closest('.form-group').removeClass('has-error'); },

            success: function (label) { label.closest('.form-group').removeClass('has-error'); },

            submitHandler: function (form) {

                error_3.hide();

                var internationalTitle = $('#international-title').val();

                var internationalDescription = $('#international-description').val();

                if(isUsingOtherInternationalDate){

                    var year = $("#years-slider-international").data("ionRangeSlider").result.from;

                    var month = $("#months-slider-international").data("ionRangeSlider").result.from;

                    var day = $("#days-slider-international").data("ionRangeSlider").result.from;

                    var date = ($(".toggle-days-slider-international").prop('checked') == false) ? (year + "-" + month + "-00") : date = (year + "-" + month + "-" + day);

                }else{

                    date = "";

                }

                var type = 'international';

                updateGeneralInformation(internationalTitle, internationalDescription, date, type);

            }

        });

    }

    //nacional...

    var handleValidationNationalForm = function(){

        var form_4 = $('#national-form');

        var error_4 = $('.alert-danger-4', form_4);
        
        var success_4 = $('.alert-success-4', form_4);

        form_4.validate({errorElement: 'span', errorClass: 'help-block help-block-error', focusInvalid: false, ignore: "",

            messages: { eventInternationalDescription: 'La descripción del panorama nacional del evento es obligatoria.' },

            rules: { eventInternationalDescription: { minlength: 10, required: true } },

            invalidHandler: function (event, validator) {       

                success_4.hide();
                
                error_4.show();
                
                App.scrollTo(error_4, -200);
            },

            highlight: function (element) { $(element).closest('.form-group').addClass('has-error'); },

            unhighlight: function (element) { $(element).closest('.form-group').removeClass('has-error'); },

            success: function (label) { label.closest('.form-group').removeClass('has-error'); },

            submitHandler: function (form) {

                error_4.hide();

                var nationalTitle = $('#national-title').val();

                var nationalDescription = $('#national-description').val();

                if(isUsingOtherNationalDate){

                    var year = $("#years-slider-national").data("ionRangeSlider").result.from;

                    var month = $("#months-slider-national").data("ionRangeSlider").result.from;

                    var day = $("#days-slider-national").data("ionRangeSlider").result.from;

                    var date = ($(".toggle-days-slider-national").prop('checked') == false) ? (year + "-" + month + "-00") : date = (year + "-" + month + "-" + day);

                }else{

                    date = "";

                }

                var type = 'national';

                updateGeneralInformation(nationalTitle, nationalDescription, date, type);

            }

        });

    }



    var updateGeneralInformation = function(title, description, date, type){

        blockScreen("AGREGANDO INFROMACION...");

        $.ajax({

            type: "post", dataType: "json", data: {id:actualId, title:title, description:description, date:date, type:type},
            
            url: PRIVATE_DIR + 'actions/timeline-management/update_general_information.php',

            error: function(xhr, status, error) { var err = eval("(" + xhr.responseText + ")"); },

            success: function(data) {

                unblockScreen("Se agregó la información al evento.");

            }

        });

    }

    
    var deleteContextInformation = function(type, db){

        var string;

        var deleteString;

        var form;

        var resetYear;

        var resetMonth;

        var resetDay;

        var titleToggle;

        var dateToggle;

        if(type == 'international'){

            string = 'ELIMINANDO INFORMACIÓN INTERNACIONAL...';

            deleteString = 'información contextual internacional eliminada.';

            form = $('#international-form')[0];

            resetYear = $("#years-slider-international").data("ionRangeSlider");

            resetMonth = $("#months-slider-international").data("ionRangeSlider");

            resetDay = $("#days-slider-international").data("ionRangeSlider");

            //ojo...

            titleToggle = $('.toggle-international-date');

            dateToggle = $('.toggle-international-title');

        }else{

            deleteString = 'información contextual nacional eliminada.';

            string = 'ELIMINANDO INFORMACIÓN NACIONAL...';

            form = $('#national-form')[0];

            resetYear = $("#years-slider-national").data("ionRangeSlider");

            resetMonth = $("#months-slider-national").data("ionRangeSlider");

            resetDay = $("#days-slider-national").data("ionRangeSlider");

            //ojo...


            titleToggle = $('.toggle-national-date');

            dateToggle = $('.toggle-national-title');

        }

        blockScreen(string);

        $.ajax({

            type: "post", dataType: "json", data: {id:actualId, db:db},
            
            url: PRIVATE_DIR + 'actions/timeline-management/delete_context_event.php',

            error: function(xhr, status, error) { var err = eval("(" + xhr.responseText + ")"); },

            success: function(data) {

                unblockScreen(deleteString);

                resetYear.update({from: minYear});

                resetMonth.update({from: minMonth});

                resetDay.update({from: minDay});

                resetYear.update({"disable": true});

                resetMonth.update({"disable": true});

                resetDay.update({"disable": true});

                $(form).trigger("reset");

                titleToggle.bootstrapToggle('off');

                dateToggle.bootstrapToggle('off');
           }

       });
    }
    
    var deleteEvent = function(){

        blockScreen("ELIMINANDO EVENTO...");

        $.ajax({

            type: "post", dataType: "json", data: {id:actualId},
            
            url: PRIVATE_DIR + 'actions/timeline-management/delete_event.php',

            error: function(xhr, status, error) { var err = eval("(" + xhr.responseText + ")"); },

            success: function(data) {

                actualId = null;

                $('#new-event-form')[0].reset(); 

                $('.nav-tabs a[href="#main-data"]').tab('show');

                $('#delete-event').addClass('hide');

                //$('#create-event').removeClass('hide');

                $('#create-event').text("CREAR EVENTO");

                $(".nav-tab").each(function() {

                    $(this).css( 'cursor', 'not-allowed');

                    $(this).addClass('tab-disabled');

                    $(this).attr("data-toggle", "");

                });

                unblockScreen("evento eliminado exitosamente.");
            }

        });

    }

    var createEvent = function(eventName, eventDescription, date){

        blockScreen("CREANDO EVENTO...");

        $.ajax({

            type: "post",

            dataType: "json",

            data: {eventName:eventName, eventDescription:eventDescription, date:date},

            url: PRIVATE_DIR + 'actions/timeline-management/create_event.php',

            error: function(xhr, status, error) {

                var err = eval("(" + xhr.responseText + ")");

            },

            success: function(data) {

                actualId = data.eventId;

                unblockScreen("evento creado exitosamente.");

                imagesGallery();

                //isMainEditMode = true;

                $('#delete-event').removeClass('hide');

                //$('#create-event').addClass('hide');

                $('#create-event').text("ACTUALIZAR CONTENIDO");


                $(".tab-disabled").each(function() {

                    $(this).css( 'cursor', 'pointer');

                    $(this).removeClass('tab-disabled');

                    $(this).attr("data-toggle", "tab");

                });

            }

        });
    }

    var confirmResetForm = function(type){

        var string;

        var db;

        if(type == 'international'){

            db = 'timeline_event_international';

            string = "<b>IMPORTANTE!</b><br><br>La descripción del evento es el único campo obligatorio de la información de <b>contexto internacional</b>. Este campo <b>NO</b> puede estar vacío.<br><br>Lo que podés hacer es Eliminar toda la información contextual internacional.<br><br>¿Proceder?";
        
        }else if(type == 'national'){

            db = 'timeline_event_national';

            string = "<b>IMPORTANTE!</b><br><br>La descripción del evento es el único campo obligatorio de la información de <b>contexto nacional</b>. Este campo <b>NO</b> puede estar vacío.<br><br>Lo que podés hacer es Eliminar toda la información contextual nacional.<br><br>¿Proceder?";

        }

        bootbox.confirm({

            message: string,

            closeButton: false,

            buttons: {

                confirm: {label: 'SI, ELIMINAR', className: 'btn-red red'},

                cancel: {label: 'SALIR', className: 'btn-danger'}

            },
            callback: function (result) {

                if(result == true){ 

                    deleteContextInformation(type, db);

                }

            }

        });

    }

    var addListeners = function(){

        $('#event-name, #event-description, #international-title, #international-description, #national-title').focus(function () {  $(this).attr('data-info', $(this).val()); });

        $('#event-name').blur(function(){ 

            if(isMainEditMode && $(this).val() != $(this).attr('data-info')){

                updateValue('timeline_events', 'title', $(this).val(), 'actualizando nombre del evento...', 'Nombre del evento actualizado correctamente.');

                $(this).attr('data-info', $(this).val());

            }

        });

        $('#event-description').blur(function(){ 

            if(isMainEditMode && $(this).val() != $(this).attr('data-info')){

                 updateValue('timeline_events', 'description', $(this).val(), 'actualizando descripcion del evento...', 'Descripción actualizada correctamente.'); 

                 $(this).attr('data-info', $(this).val());

            }
        });

        $('#international-title').blur(function(){ 

            if(isMainEditMode && $(this).val() != $(this).attr('data-info')){

                updateValue('timeline_event_international', "title", $(this).val(), 'actualizando titulo de evento internaciconal...', 'Título actualizado correctamente.');

                $(this).attr('data-info', $(this).val());

            }

        });
        
        $('#international-description').blur(function(){ 

            if(isMainEditMode && $(this).val() != $(this).attr('data-info')){

                var length = $(this).val().length;

                $(this).attr('data-info', $(this).val());

                if(length<=10){

                    confirmResetForm('international');

                }else{

                    updateValue('timeline_event_international', "description", $(this).val(), 'actualizando descripción de evento internaciconal...', 'Descripción actualizada correctamente.');
                }

            }

        });

        $('#national-title').blur(function(){ 

            if(isMainEditMode && $(this).val() != $(this).attr('data-info')){

                updateValue('timeline_event_national', "title", $(this).val(), 'actualizando titulo de evento nacional...', 'Título actualizado correctamente.');

                $(this).attr('data-info', $(this).val());

            }

        });

        $('#national-description').blur(function(){ 

            if(isMainEditMode && $(this).val() != $(this).attr('data-info')){

                var length = $(this).val().length;
                
                $(this).attr('data-info', $(this).val());
                
                if(length<=10){

                    confirmResetForm('national');

                }else{

                    updateValue('timeline_event_national', "description", $(this).val(), 'actualizando descripción de evento nacional...', 'Descripción actualizada correctamente.');

                }

            }

        });

        
        
        //manejo de los tres sliders de tiempo: contexto general, internacional y nacional.
        //separados, por si se quiere modificar una de las data paralelas...
        //si el slider lleva por nombre event year y no hay fechas particulares, se utilizan las mismas...  


        $("#years-slider, #years-slider-international, #years-slider-national").ionRangeSlider({ 

            hide_min_max: true, keyboard: true, min: minYear, max: 2020, from: 1900, to: 2020, step: 1, grid: true, prefix: "año: ", prettify_enabled: true,

            onChange: function (data) {

                if(data.input.prop('name') == 'eventYear'){

                    if(!isUsingOtherInternationalDate) $("#years-slider-international").data("ionRangeSlider").update({from: data.from});

                    if(!isUsingOtherNationalDate) $("#years-slider-national").data("ionRangeSlider").update({from: data.from});
                }

            },

            onFinish: function(data){

                if(data.input.prop('name') == 'eventYear' && isMainEditMode){

                    updateValue('timeline_events', 'date_start', getEventDate(), 'Actualizando año del evento.', 'Año del evento actualizado correctamente.');

                }

                if(data.input.prop('name') == 'eventInternationalYear' && isMainEditMode){

                    updateValue('timeline_event_international', 'date_start', getEventInternationalDate(), 'Actualizando año del evento internacional.', 'Año del evento actualizado correctamente.');

                }

                if(data.input.prop('name') == 'eventNationalYear' && isMainEditMode){

                    updateValue('timeline_event_national', 'date_start', getEventNationalDate(), 'Actualizando año del evento nacional.', 'Año del evento actualizado correctamente.');

                }

            }

        });

        //meses...logica similar a anios. Si el slider es de evento y tanto nacional como internacional no tienen
        //fecha propia, los updatea...
        //por otro lado, si el slider es de internacional o nacional, update el respectivo slider de dias para que tenga
        //la cantidad correcta de dias en el mes...  

        $("#months-slider, #months-slider-international, #months-slider-national").ionRangeSlider({grid: !0, from: minMonth, keyboard: true,

            values: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],

            onChange: function (data){ 

                if(data.input.prop('name') == 'eventMonth'){

                    $("#days-slider").data("ionRangeSlider").update({max: new Date(2017, data.from + 1, 0).getDate()}); 

                    if(!isUsingOtherInternationalDate) $("#months-slider-international").data("ionRangeSlider").update({from: data.from});

                    if(!isUsingOtherNationalDate) $("#months-slider-national").data("ionRangeSlider").update({from: data.from});

                }else if(data.input.prop('name') == 'eventInternationalMonth'){

                    $("#days-slider-international").data("ionRangeSlider").update({max: new Date(2017, data.from + 1, 0).getDate()}); 

                }else if(data.input.prop('name') == 'eventNationalMonth'){

                    $("#days-slider-national").data("ionRangeSlider").update({max: new Date(2017, data.from + 1, 0).getDate()}); 

                }

            },

            onFinish: function(data){

                if(data.input.prop('name') == 'eventMonth' && isMainEditMode){

                    updateValue('timeline_events', 'date_start', getEventDate(), 'Actualizando mes del evento.', 'Mes del evento actualizado correctamente.');

                }

                if(data.input.prop('name') == 'eventInternationalMonth' && isMainEditMode){

                    updateValue('timeline_event_international', 'date_start', getEventInternationalDate(), 'Actualizando mes del evento.', 'Mes del evento actualizado correctamente.');

                }

                if(data.input.prop('name') == 'eventNationalMonth' && isMainEditMode){

                    updateValue('timeline_event_national', 'date_start', getEventNationalDate(), 'Actualizando mes del evento.', 'Mes del evento actualizado correctamente.');

                }

            }

        })

        //sliders de dias. Si es el general, modifica al internacional y nacional
        //si estos estan en autoupdate...

        $("#days-slider, #days-slider-international, #days-slider-national").ionRangeSlider({ hide_min_max: true, keyboard: true, min: minDay, max: 31, from: 1, to: 31, step: 1, grid: true, prefix: "día: ",

            prettify_enabled: true,

            onChange: function (data) { 

                if(data.input.prop('name') == 'eventDay'){

                    if(!isUsingOtherInternationalDate) $("#days-slider-international").data("ionRangeSlider").update({from: data.from});

                    if(!isUsingOtherNationalDate) $("#days-slider-national").data("ionRangeSlider").update({from: data.from});

                }
            },

            onFinish: function(data){

                if(data.input.prop('name') == 'eventDay' && isMainEditMode){

                    updateValue('timeline_events', 'date_start', getEventDate(), 'actualizando!');

                }

                if(data.input.prop('name') == 'eventInternationalDay' && isMainEditMode){

                    updateValue('timeline_event_international', 'date_start', getEventInternationalDate(), 'Actualizando mes del evento internacional.', 'Mes del internacional evento actualizado correctamente.');

                }

                if(data.input.prop('name') == 'eventNationalDay' && isMainEditMode){

                    updateValue('timeline_event_national', 'date_start', getEventNationalDate(), 'Actualizando día del evento.', 'Día del evento actualizado correctamente.');

                }

            }

        });


        $(".toggle-days-slider, .toggle-days-slider-international, .toggle-days-slider-national").change(function() { 

            if($(this).prop('name') == 'toggleDaysSlider'){

                $("#days-slider").data("ionRangeSlider").update({disable:$(this).prop('checked') ? 0 : 1});

                if(isMainEditMode){

                    updateValue('timeline_events', 'date_start', getEventDate(), 'actualizando!');

                }


            }else if($(this).prop('name') == 'toggleDaysSliderInternational'){

                $("#days-slider-international").data("ionRangeSlider").update({disable:$(this).prop('checked') ? 0 : 1}); 

                 if(isMainEditMode){

                    updateValue('timeline_event_international', 'date_start', getEventInternationalDate(), 'actualizando!');

                }

            }else if($(this).prop('name') == 'toggleDaysSliderNational'){

                $("#days-slider-national").data("ionRangeSlider").update({disable:$(this).prop('checked') ? 0 : 1});

                if(isMainEditMode){

                    updateValue('timeline_event_national', 'date_start', getEventNationalDate(), 'actualizando!');

                }
            }

        });


        $(".toggle-international-title, .toggle-national-title").change(function() {

            var div;

            var snippet;

            var type;

            if($(this).prop('name') == 'toggleInternationalTitle'){

                div = "#international-context-title";

                snippet = '.snippet-international-title';

                type = true;

                }else if($(this).prop('name') == 'toggleNationalTitle'){

                    div = "#national-context-title";

                    snippet = '.snippet-national-title';

                    type = false;

                }

                if(!$(this).prop('checked')){

                    blockDiv(div);

                    $(snippet).fadeTo("fast", 1);

                    if(type){

                        $('#international-title').val("");

                        updateValue('timeline_event_international', 'title', '', 'Actualizando...', 'Actualizado.');

                    }else{

                        $('#national-title').val("");

                        updateValue('timeline_event_national', 'title', '', 'Actualizando...', 'Actualizado.');

                    }


                }else{

                    unblockDiv(div);

                    $(snippet).fadeTo("fast", 0);

                }

            });

        $(".toggle-international-date, .toggle-national-date").change(function() {

            var snippet;

            var yearDiv;

            var monthDiv;

            var dayDiv;

            var toggle;

            var type;

            if($(this).prop('name') == 'toggleInternationalDate'){

                type = true;

                snippet = '.snippet-international-date';

                yearDiv = '#years-slider-international';

                monthDiv = '#months-slider-international';

                dayDiv = '#days-slider-international';

                toggle = "#toggle-days-slider-international";

            }else if($(this).prop('name') == 'toggleNationalDate'){

                type = false;

                snippet = '.snippet-national-date';

                yearDiv = '#years-slider-national';

                monthDiv = '#months-slider-national';

                dayDiv = '#days-slider-national';

                toggle = "#toggle-days-slider-national";

            }

            if(!$(this).prop('checked')){

                (type) ? isUsingOtherInternationalDate = false : isUsingOtherNationalDate = false;

                $(snippet).fadeTo("fast", 0 );

                $(yearDiv).data("ionRangeSlider").update({"disable": true});

                $(monthDiv).data("ionRangeSlider").update({"disable": true});

                $(dayDiv).data("ionRangeSlider").update({"disable": true});

                blockDiv(toggle);

                if(type){

                    updateValue('timeline_event_international', 'date_start', getEventDate(), 'Actualizando año del evento internacional.', 'Año del evento actualizado correctamente.');

                }else{

                    updateValue('timeline_event_national', 'date_start', getEventDate(), 'Actualizando año del evento nacional.', 'Año del evento actualizado correctamente.');

                }



            }else{

                (type) ? isUsingOtherInternationalDate = true : isUsingOtherNationalDate = true;

                $(snippet).fadeTo("fast", 1);

                $(yearDiv).data("ionRangeSlider").update({"disable": false});
                
                $(monthDiv).data("ionRangeSlider").update({"disable": false});

                $(dayDiv).data("ionRangeSlider").update({"disable": false});

                unblockDiv(toggle);
            }

        });

        

        $('#delete-event').on("click", function(){

            var string = "<b>IMPORTANTE!</b><br><br>Estás a punto de <b>eliminar un evento.</b><br><br>Esta acción no se puede deshacer. Recordá que los eventos los podés mantener <b>offline</b> mientras estás en proceso de edición y luego pasarlos <b>online</b> al expotar el contenido.<br><br>Si eliminás el evento se perderá todo el material almacenado: títulos, descripciones, imágenes y videos.<br><br>¿Estás seguro de continuar?";

            bootbox.confirm({

                message: string,

                closeButton: false,

                buttons: {

                    confirm: {label: 'SALIR', className: 'btn-danger'  },

                    cancel: {label: 'SI, ELIMINAR', className: 'btn-red red'}

                },
                callback: function (result) {

                    if(result == false){ deleteEvent();

                    }

                }

            });

        });


        $(".tab-disabled").each(function() {

            $(this).css( 'cursor', 'not-allowed');

            $(this).on("click", function(){

                if($(this).hasClass('tab-disabled')){

                    bootbox.alert("Debes crear el evento antes de poder pasar a estos campos. <br><br>Completá el formulario de esta página y luego presioná el botón <b>Crear Evento</b>");

                }

            });
        });

    }

    /**
    *
    * getEventDate();
    * tengo que hacer esta consulta unas cuantas veces, asi que esto me resulta
    * bastante practico :)
    */

    var getEventDate = function(){

        var year = $("#years-slider").data("ionRangeSlider").result.from;

        var month = $("#months-slider").data("ionRangeSlider").result.from + 1;

        var day = $("#days-slider").data("ionRangeSlider").result.from;

        var date = ($(".toggle-days-slider").prop('checked') == false) ? (year + "-" + month + "-00") : date = (year + "-" + month + "-" + day);
        
        return date;

    }

    var getEventInternationalDate = function(){

        var year = $("#years-slider-international").data("ionRangeSlider").result.from;

        var month = $("#months-slider-international").data("ionRangeSlider").result.from + 1;

        var day = $("#days-slider-international").data("ionRangeSlider").result.from;

        var date = ($(".toggle-days-slider-international").prop('checked') == false) ? (year + "-" + month + "-00") : date = (year + "-" + month + "-" + day);
        
        return date;

    }

    var getEventNationalDate = function(){

        var year = $("#years-slider-national").data("ionRangeSlider").result.from;

        var month = $("#months-slider-national").data("ionRangeSlider").result.from + 1;

        var day = $("#days-slider-national").data("ionRangeSlider").result.from;

        var date = ($(".toggle-days-slider-national").prop('checked') == false) ? (year + "-" + month + "-00") : date = (year + "-" + month + "-" + day);
        
        return date;

    }

    /**
    *
    * blockScreen(msg);
    * param msg - el mensaje a mostrar en el centro de la pantalla...
    * bloquea la pantalla mientras se opera con el server...
    */

    var blockScreen = function(msg){

        $.blockUI({ message: msg,

            css: {border:'0px solid #FFFFFF',cursor:'wait',backgroundColor:'none', color:'#FFF'},

            overlayCSS:  { backgroundColor: '#000000',opacity:0.5,cursor:'wait'} 

        });
    }

    /**
    *
    * unblockScreen(msg);
    * param msg - el mensaje que va a ir en el toastr en el borde inferior izquierdo...
    * se completo una opereta...
    */

    var unblockScreen = function(msg){

        $.unblockUI()

        toastr.info(msg);
    }

    /**
    *
    * blockDiv(div);
    * param div - pequeno workaround para bloquear y desbloquear divs...
    */

    var blockDiv = function(div){

        $(div + " *").prop('disabled',true);

        $(div).fadeTo("fast", 0.33);

        $(div).css('cursor', 'not-allowed');
    }

    /**
    *
    * unblockDiv(div);
    * param div - pequeno workaround para bloquear y desbloquear divs...
    */

    var unblockDiv = function(div){

        $(div + " *").prop('disabled', false);

        $(div).fadeTo("fast", 1);

        $(div).css('cursor', 'pointer');
    }

    var imagesGallery = function(){

        $('#fileupload').bind("change", function(){ 

            $('.start-upload-btn').fadeTo("fast", 1);

        });


        $('#fileupload, #fileupload-2').fileupload({

            formData: {id: actualId},

            disableImageResize: false,

            autoUpload: false,

            disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),

            maxFileSize: 50000000,

            acceptFileTypes: /(\.|\/)(gif|jpe?g|png|mp4)$/i,

            complete: function(xhr) {

            },

            stop: function (e) {

                $('.fileupload-progress').removeClass('fade in');

                $('.fileupload-progress').addClass('fade out');

                var string = "Las imágenes se subieron exitosamente! <br><br> ¿Querés ir a editarlas?";

                bootbox.confirm({

                    message: string,

                    closeButton: false,

                    buttons: {

                        confirm: {

                            label: 'EDITAR AHORA',

                            className: 'btn-success'

                        },

                        cancel: {

                            label: 'EDITAR DESPUÉS',

                            className: 'btn-danger'

                        }

                    },
                    callback: function (result) {

                        if(result == true){

                            window.location.href = 'timeline_images_gallery.php?idEvent=' + actualId;

                        }

                    }

                });

            }

        });

        $('#fileupload').fileupload(

            'option',

            'redirect',

            window.location.href.replace(

                /\/[^\/]*$/,

                '/cors/result.html?%s'

                )

            );

        if ($.support.cors) {

            $.ajax({

                type: 'HEAD'

            }).fail(function () {

                $('<div class="alert alert-danger"/>')
                
                .text('Upload server currently unavailable - ' +

                    new Date())

                .appendTo('#fileupload');

            });

        }

        $('#fileupload').addClass('fileupload-processing');

        $.ajax({

            url: $('#fileupload').attr("action"),

            dataType: 'json',

            context: $('#fileupload')[0]
            
        }).always(function () {

            $(this).removeClass('fileupload-processing');

        }).done(function (result, data) {

            $(this).fileupload('option', 'done')

            .call(this, $.Event('done'), {result: result});

            var r = data.result;

        })
    }

    /**
    *
    * updateValue = function(field, value);
    * @param - field...el campo a modificar en la DB...
    * @param - value...el valor a agregar en la DB....
    * actualizar el valor...
    */

    var updateValue = function(table, field, value, blockMsg = "", unblockMsg = ""){

        blockScreen(blockMsg);

        $.ajax({

            type: "post",

            dataType: "json",

            data: {table:table, field:field, value:value, id:actualId},

            url: PRIVATE_DIR + 'actions/timeline-management/update_content.php',

            error: function(xhr, status, error) { 

                var err = eval("(" + xhr.responseText + ")"); 

            },

            success: function(data) {

                unblockScreen(unblockMsg);

            }

        });

    }

    /**
    *
    * initialSetup();
    * setting inicial con algunos bloqueos y demas...
    */

    var initialSetup = function(){

        $(".snippet-national-title, .snippet-international-title").fadeTo("fast", 1);

        $("#years-slider-international").data("ionRangeSlider").update({"disable": true});
        
        $("#months-slider-international").data("ionRangeSlider").update({"disable": true});

        $("#days-slider-international").data("ionRangeSlider").update({"disable": true});

        $("#years-slider-national").data("ionRangeSlider").update({"disable": true});
        
        $("#months-slider-national").data("ionRangeSlider").update({"disable": true});

        $("#days-slider-national").data("ionRangeSlider").update({"disable": true});

        blockDiv("#toggle-days-slider-international");

        blockDiv("#toggle-days-slider-national");

        blockDiv('#international-context-title');

        blockDiv('#national-context-title');

        $('.start-upload-btn').fadeTo("fast", 0);
    }

    /**
    *
    * loadTags();
    * levanta todos los tags del sistema y los whitelistea...
    */

    var loadTags = function(){

        $.ajax({

            type: "post", dataType: "json", data: {id:actualId},
            
            url: PRIVATE_DIR + 'actions/timeline-management/get_all_tags.php',

            error: function(xhr, status, error) { var err = eval("(" + xhr.responseText + ")"); },

            success: function(data) {

                whiteList = new Array();

                for(var i = 0; i <data.length; i++){

                    whiteList.push(data[i].tag);
            
                }

                var tagsInput = document.querySelector('input[name=tags]'),
            
                tagify = new Tagify(tagsInput, {

                    duplicates : true,
                    
                    whitelist : whiteList,

                    blacklist : ["fuck", "shit"]
                
                })

                tagify.on('remove', onRemoveTag);

                tagify.on('add', onAddTag);

                //tagify.on('notWhitelisted', onAddNotWhitelistedTag);


            }

        });

    }

    /*var onAddNotWhitelistedTag =  function(e){

        console.log(e, e.detail);

    };*/

    /**
    *
    * onRemoveTag();
    * se elimina un tag de la lista...
    */

    var onRemoveTag = function(e){

        $.ajax({

            type: "post", dataType: "json", data: {id:actualId, tag:e.detail.value},
            
            url: PRIVATE_DIR + 'actions/timeline-management/delete_tag.php',

            error: function(xhr, status, error) { var err = eval("(" + xhr.responseText + ")"); },

            success: function(data) {

                unblockScreen("Se eliminó el tag");

            }

        });
    }

    /**
    *
    * onAddTag();
    * se agrega un tag de la lista...
    */

    var onAddTag = function(e){

        var isInWhiteList = false;

        var tag = e.detail.value;

        for(var i = 0; i< whiteList.length; i++){

            if(e.detail.value == whiteList[i]){

                isInWhiteList = true;

                break;
            }
        }

        $.ajax({

            type: "post", dataType: "json", data: {id:actualId, isInWhiteList:isInWhiteList, tag:tag},
            
            url: PRIVATE_DIR + 'actions/timeline-management/add_tag.php',

            error: function(xhr, status, error) { var err = eval("(" + xhr.responseText + ")"); },

            success: function(data) {

                unblockScreen("Se agregó el tag.");

            }

        });
    }


    /**
    *
    * handlePasswordStrengthChecker();
    * asignar un handler para el trackeo del pass. Es bastante floja
    * la documentación y le falta un poco de laburo. Incompatibilidad con distintos
    * bootrstraps...
    */

    return {

        init: function() {


            toastr.options = {"closeButton": true, "debug": false, "newestOnTop": false, "progressBar": true, "positionClass": "toast-bottom-left",

            "preventDuplicates": true, "onclick": null, "showDuration": "300", "hideDuration": "1000", "timeOut": "5000", "extendedTimeOut": "1000",

            "showEasing": "swing", "hideEasing": "linear", "showMethod": "fadeIn", "hideMethod": "fadeOut"}

            handleValidationMainForm();

            handleValidationInternationalForm();

            handleValidationNationalForm();

            addListeners();

            imagesGallery();

            $('.nav-tabs a[href="#main-data"]').tab('show');

            loadTags();

            if(editMode == true){

                isMainEditMode = true;

                //national...

                $("#years-slider").data("ionRangeSlider").update({from: mainYear});

                $("#months-slider").data("ionRangeSlider").update({from: mainMonth - 1});
                
                $("#days-slider").data("ionRangeSlider").update({from: mainDay});

                //international...

                $("#years-slider-international").data("ionRangeSlider").update({from: internationalYear});

                $("#months-slider-international").data("ionRangeSlider").update({from: internationalMonth - 1});
                
                $("#days-slider-international").data("ionRangeSlider").update({from: internationalDay});

                //international...

                $("#years-slider-national").data("ionRangeSlider").update({from: nationalYear});

                $("#months-slider-national").data("ionRangeSlider").update({from: nationalMonth  -1});
                
                $("#days-slider-national").data("ionRangeSlider").update({from: nationalDay});

               // bootbox.alert("Estás en modo edición! cada cambio que hagas se actualizará de forma automática.");

                $(".form-button").addClass('hide');

                //...

                if($('#international-title').val() == "") blockDiv('#international-context-title');

                if($('#national-title').val() == "") blockDiv('#national-context-title');

                //...

                if(dateStart == internationalDate){

                    $("#years-slider-international").data("ionRangeSlider").update({"disable": true});
        
                    $("#months-slider-international").data("ionRangeSlider").update({"disable": true});

                    $("#days-slider-international").data("ionRangeSlider").update({"disable": true});

                    $('.toggle-international-date').prop('checked', false).change();

                }else{

                    $('.toggle-international-date').prop('checked', true).change();

                    if(internationalDay == "00"){

                        $(".toggle-days-slider-international").prop('checked', false).change();

                        $("#days-slider-international").data("ionRangeSlider").update({"disable": true});
                    }
                }

                //...

                if(dateStart == nationalDate){

                    $("#years-slider-national").data("ionRangeSlider").update({"disable": true});
        
                    $("#months-slider-national").data("ionRangeSlider").update({"disable": true});

                    $("#days-slider-national").data("ionRangeSlider").update({"disable": true});

                    $('.toggle-national-date').prop('checked', false).change();
                
                }else{

                    $('.toggle-national-date').prop('checked', true).change();

                    if(nationalDay == "00"){

                        $(".toggle-days-slider-national").prop('checked', false).change();

                        $("#days-slider-national").data("ionRangeSlider").update({"disable": true});
                    }

                }

                if(mainDay == "00"){

                    $(".toggle-days-slider").prop('checked', false).change();

                    $("#days-slider").data("ionRangeSlider").update({"disable": true});
                }

            }else{

                initialSetup();

            }
        }
    }

}();

jQuery(document).ready(function() { Profile.init(); });