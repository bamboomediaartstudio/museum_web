//-----------------------------------------------------------------------------
//------------------------------------------------------------------- constants

const PRIVATE_DIR = "private/";


var TimelineDownloadLastVersion = function () {

    /**
    *
    * addListeners();
    * agrega los listeners para togglear la visibilidad o para eliminar, etc...
    */

    var addListeners = function(){

        $( ".btn-download" ).click(function() {

            var version = $(this).attr('data-version');

            var path = 'exports/timelineApp/' + version  + '/bin.zip';

            console.log(path);

            window.location.assign(path);
            
        });
    }

    return {

        init: function () {

            addListeners();

        }

    };

}();

jQuery(document).ready(function() { TimelineDownloadLastVersion.init(); });

