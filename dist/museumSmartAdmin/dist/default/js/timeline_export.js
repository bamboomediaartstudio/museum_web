	//-----------------------------------------------------------------------------
	//------------------------------------------------------------------- constants

	const PRIVATE_DIR = "private/";

	var parsedArray = [];

	/**
    *
    * TimelineExport();
    * todo lo relativo a exports....
    */

	var TimelineExport = function () {

		/**
		*
		* addListeners();
		* listeners...
		*/

		var addListeners = function(){

			$('.btn-preview').click(function() { window.open('../timelineApp/bin/index.html', '_blank'); });

			$('.btn-export').click(function() {

				blockScreen('Exportando datos...');

				$.ajax({

		            type: "post",

		            dataType: "json",

		            url: 'services/timelineApp/timeline_create_data.php',

		            error: function(xhr, status, error) { 

		            	console.log("error");

		            	var err = eval("(" + xhr.responseText + ")");

		            },

		            success: function(data) { 

		            	unblockScreen('Los datos se exportaron correctamente :)');

		            }

		        });

			});

			$('.btn-download').click(function() {

				blockScreen('¡Paciencia! Esto va a llevar un rato :)<br>Por favor, no salgas de esta ventana. Apenas termine el proceso, se descargará un archivo ZIP a tu PC.');

				$.ajax({

		            type: "post",

		            dataType: "json",

		            url: 'services/timelineApp/timeline_download_content.php',

		            error: function(xhr, status, error) { var err = eval("(" + xhr.responseText + ")");

		            },

		            success: function(data) { 

		            	unblockScreen('Archivo zippeado correctamente. Descargando.');

		            	var path = 'exports/timelineApp/' + data.newVersion  + '/bin.zip';

		            	console.log(path);

		            	window.location.assign(path);


		            }

		        });

			});

		}

		/**
    *
    * blockScreen(msg);
    * param msg - el mensaje a mostrar en el centro de la pantalla...
    * bloquea la pantalla mientras se opera con el server...
    */

    var blockScreen = function(msg){

        $.blockUI({ message: msg,

            css: {border:'0px solid #FFFFFF',cursor:'wait',backgroundColor:'none', color:'#FFF'},

            overlayCSS:  { backgroundColor: '#000000',opacity:0.8,cursor:'wait'} 

        });
    }

    /**
    *
    * unblockScreen(msg);
    * param msg - el mensaje que va a ir en el toastr en el borde inferior izquierdo...
    * se completo una opereta...
    */

    var unblockScreen = function(msg){

        $.unblockUI()

        toastr.info(msg);
    }

		
	    return {

	    	init: function () {

	    		toastr.options = {"closeButton": true, "debug": false, "newestOnTop": false, "progressBar": true, "positionClass": "toast-bottom-left",

	            "preventDuplicates": true, "onclick": null, "showDuration": "300", "hideDuration": "1000", "timeOut": "5000", "extendedTimeOut": "1000",

	            "showEasing": "swing", "hideEasing": "linear", "showMethod": "fadeIn", "hideMethod": "fadeOut"};

	    		addListeners();

	    		$('.dropdown-menu-list').slimScroll({ height: '250px' });

			}

		};

	}();

	jQuery(document).ready(function() { 

		TimelineExport.init(); 

	});