//-----------------------------------------------------------------------------
//------------------------------------------------------------------- constants

const PRIVATE_DIR = "private/";

var showCustomAlert = true;

var divId;

var video;

var PicturesView = function () {

    /**
    *
    * getURLParameter();
    * chequear la URL para cambio de secciones y demas...
    * a esto habría que mandarlo a un helper...
    */

    var getURLParameter = function(name) {

        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;

    }

    /**
    *
    * addListeners();
    * agrega los listeners para togglear la visibilidad o para eliminar, etc...
    */

    var addListeners = function(){

        var url = PRIVATE_DIR + 'actions/images-management/reorder_images.php?id=' + idEvent;

        $( "#sortable" ).sortable({

            update: function (event, ui) {

                var data = $(this).sortable('serialize');

                $.ajax({

                    data: data,

                    type: 'POST',   

                    url: url,

                    error: function(xhr, status, error) {

                        var err = eval("(" + xhr.responseText + ")");

                    },

                    success: function(data) {

                    }

                });

            }

        });
        
        $( "#sortable" ).disableSelection();

        $( ".do-description" ).click(function() {

            divId = $(this).closest('.general').attr('id');

            getDescriptionByid(divId.split("_")[1]);

        });


        $( ".do-delete" ).click(function() {

            divId = $(this).closest('.general').attr('id');

            (showCustomAlert) ? showAlertMsg() : deleteImage();

        });

        $( ".btn-get-thumb" ).click(function() {

            divId = $(this).closest('.general').attr('id');

            videoId = $(this).closest('.general').find('.player').attr('id');

            video = document.getElementById(videoId);

            var name = $(video).attr("data-video-name");

            //console.log(divId, videoId, video, name);

            capture(video, idEvent, name);

        });
        
        $( ".do-play-video" ).click(function() {

            videoId = $(this).closest('.general').find('.player').attr('id');

            video = document.getElementById(videoId);

            controlsOverlay = $(this).closest('.mt-overlay');

            if(video.paused){

                video.play();

                controlsOverlay.hide();

                video.addEventListener("seeking", function() { controlsOverlay.hide(); return; }, true);

                video.addEventListener("pause", function() { 

                    controlsOverlay.show(); 

                }, true);


            }

        });
        

        $('input[type=radio]').change(function() {

            var name = $(this).attr('name');

            var value = $(this).attr('value');

            changeImageGroup(name, value);

        });
    }

    /**
    *
    * capture(video);
    * @param video...
    * hacer un thumbnail del video... :)
    */

    
    var capture = function(video, idEvent, name){

        blockScreen('creando miniatura para el video');

        console.log(video, idEvent, name);

        var canvas = document.createElement('canvas');

        canvas.height = video.videoHeight;

        canvas.width = video.videoWidth;

        var ctx = canvas.getContext('2d');

        ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

        var img = new Image();

        img.src = canvas.toDataURL();

        var block = img.src.split(";");

        var contentType = block[0].split(":")[1];

        var realData = block[1].split(",")[1];

        var blob = b64toBlob(realData, contentType);

        var formDataToUpload = new FormData();

        formDataToUpload.append("image", blob);
        
        formDataToUpload.append("idEvent", idEvent);

        formDataToUpload.append("videoName", name);

        $.ajax({

            url: PRIVATE_DIR + 'actions/images-management/change_video_thumbnail.php',
            
            data: formDataToUpload,
            
            type:"POST",
            
            contentType:false,
            
            processData:false,
            
            cache:false,
            
            dataType:"json",
            
            error:function(err){
                
                console.error(err);
            },
            
            success:function(data){
               
                console.log(data);

                unblockScreen('miniatura creada :)');
            },
            complete:function(){
               
                console.log("Request finished.");
            }
        });

    }

    /**
     * Convert a base64 string in a Blob according to the data and contentType.
     * 
     * @param b64Data {String} Pure base64 string without contentType
     * @param contentType {String} the content type of the file i.e (image/jpeg - image/png - text/plain)
     * @param sliceSize {Int} SliceSize to process the byteCharacters
     * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
     * @return Blob
     */

     function b64toBlob(b64Data, contentType, sliceSize) {

        contentType = contentType || '';
        
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
        
            for (var i = 0; i < slice.length; i++) {
        
                byteNumbers[i] = slice.charCodeAt(i);
        
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, {type: contentType});
        
        return blob;
    }


    /**
    *
    * changeImageGroup(name, value);
    * @param name - el id...
    * @param value - justos...
    */


    var changeImageGroup = function(name, value){

        console.log("changeImageGroup: " + name + " - " + value);

        $.ajax({

            type: "post",

            dataType: "json",

            data: {id:name, type:value},

            url: PRIVATE_DIR + 'actions/timeline-management/set_timeline_image_type.php',

            error: function(xhr, status, error) {

                var err = eval("(" + xhr.responseText + ")");

            },

            success: function(data) {

                unblockScreen("Se actualizó el grupo de la imagen..."); 
            }

        });

    }

    /**
    *
    * getDescriptionByid(id);
    * @param id - id de la foto a levantar...
    * levantar el mensaje actual...
    */

    var getDescriptionByid = function(id){

        blockScreen("OBTENIENDO DATOS...");

        $.ajax({

            type: "post",

            dataType: "json",

            data: {id:id},

            url: PRIVATE_DIR + 'actions/timeline-management/get_timeline_image_description.php',

            error: function(xhr, status, error) { var err = eval("(" + xhr.responseText + ")"); },

            success: function(data) {

                unblockScreen();

                showDescriptionPrompt(data.description, data.id);

            }

        });
    }

    /**
    *
    * showDescriptionPrompt(description, id);
    * @param description - la descripcion de la imagen...
    * @param id - id de la foto a levantar...
    * mostrar el prompt y usar los datos :)
    */

    var showDescriptionPrompt = function(description, id){

        var saveLastValue = description;

        var promptTitle;

        if(description == '' || description == null){

            promptTitle = "Agregar una descripción para esta imagen:";

        }else{

            promptTitle = "Actualizar la descripción de esta imagen:";

        }

        bootbox.prompt({

            title: promptTitle,

            inputType: 'textarea',

            value: description,

            callback: function (result) {

                if(saveLastValue != result && result != null){

                    updateImageDescription(result, id);

                }
            }

        })

    }

    /**
    *
    * updateImageDescription(imageDescription, id);
    * @param imageDescription - la descripcion de la imagen a guardar...
    * @param id - id de la foto a levantar...
    * mostrar el prompt y usar los datos :)
    */

    var updateImageDescription = function(imageDescription, id){

        blockScreen("ACTUALIZANDO PIE DE IMAGEN...")

        $.ajax({

            type: "post",

            dataType: "json",

            data: {id:id, description:imageDescription},

            url: PRIVATE_DIR + 'actions/timeline-management/set_timeline_image_description.php',

            error: function(xhr, status, error) {

                var err = eval("(" + xhr.responseText + ")");

            },

            success: function(data) {

                unblockScreen("Se actualizó el pie de la imagen..."); 
            }

        });

    }

    /**
    *
    * showAlertMsg();
    * mostrar mensaje antes de eliminar...
    */


    var showAlertMsg = function(){

        bootbox.confirm({

            message: "<b>ATENCIÓN!</b><br><br>Estás a punto de eliminar una imagen. Esta no se mostrará más en la linea de tiempo.<br><br>¿Continuar?",

            closeButton: false,

            buttons: {

                confirm: {

                    label: 'SI, CONFIRMAR',

                    className: 'btn-red red'

                },

                cancel: {

                    label: 'CANCELAR',

                    className: 'btn-danger'

                }

            },
            callback: function (result) {

                if(result == true){

                    showCustomAlert = false;

                    deleteImage();

                }

            }

        });
    }

    /**
    *
    * blockScreen(msg);
    * param msg - el mensaje a mostrar en el centro de la pantalla...
    * bloquea la pantalla mientras se opera con el server...
    */

    var blockScreen = function(msg){

        $.blockUI({ message: msg,

            css: {border:'0px solid #FFFFFF',cursor:'wait',backgroundColor:'none', color:'#FFF'},

            overlayCSS:  { backgroundColor: '#000000',opacity:0.5,cursor:'wait'} 

        });
    }

    /**
    *
    * unblockScreen(msg);
    * param msg - el mensaje que va a ir en el toastr en el borde inferior izquierdo...
    * se completo una opereta...
    */

    var unblockScreen = function(msg = ""){

        $.unblockUI()

        if(msg != "") toastr.info(msg);
    }

    /**
    *
    * deleteImage();
    * elimina una imagen...
    */

    var deleteImage = function(){

        blockScreen("ELIMINANDO IMAGEN...");

        $.ajax({

            type: "post",

            dataType: "json",

            data: {id:divId.split("_")[1], idEvent:idEvent},

            url: PRIVATE_DIR + 'actions/timeline-management/delete_timeline_image.php',

            error: function(xhr, status, error) {

                var err = eval("(" + xhr.responseText + ")");

            },

            success: function(data) {

                $('#' + divId).fadeOut(1000);

                unblockScreen("La imagen se eliminó exitosamente.");

            }

        });

    }

    return {

        init: function () {

            addListeners();

            toastr.options = {"closeButton": true, "debug": false, "newestOnTop": false, "progressBar": false, "positionClass": "toast-bottom-left",

            "preventDuplicates": false, "onclick": null, "showDuration": "300", "hideDuration": "1000", "timeOut": "5000", "extendedTimeOut": "1000",

            "showEasing": "swing", "hideEasing": "linear", "showMethod": "fadeIn", "hideMethod": "fadeOut" }

        }

    };

}();

jQuery(document).ready(function() { PicturesView.init(); });

