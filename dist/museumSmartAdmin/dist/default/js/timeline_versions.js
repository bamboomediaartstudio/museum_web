//-----------------------------------------------------------------------------
//------------------------------------------------------------------- constants

const PRIVATE_DIR = "private/";


var TimelineVersions = function () {

    /**
    *
    * getURLParameter();
    * chequear la URL para cambio de secciones y demas...
    * a esto habría que mandarlo a un helper...
    */

    var getURLParameter = function(name) {

        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;

    }

    /**
    *
    * initTable();
    * inicializa la tabla principal...
    */

    var initTable = function () {

        var table = $('#events-list');

        var oTable = table.dataTable({

            language: {
                
                aria: {
                    
                    sortAscending: ": activar filtro de manera ascendente",
                    
                    sortDescending: ": activar filtro de manera descendiente"
                    
                },

                processing: "Procesando...",
                
                emptyTable: "No hay información disponible",
                
                info: "Mostrando _START_ de _END_ de un total de _TOTAL_ propiedades",
                
                infoEmpty: "No se encontraron registros",
                
                infoFiltered: "",
                
                lengthMenu: "_MENU_ registros",
                
                search: "Buscar:",
                
                zeroRecords: "No se encontraron coincidencias",
                
                paginate: {
                    
                    first:      "primera página",
                    
                    previous:   "anterior",
                    
                    next:       "siguiente",
                    
                    last:       "última página"
                }
            },

            buttons: [
            
            ],

            responsive: true,

            colReorder: true,

            order: [ [0, 'desc'] ],
            
            lengthMenu: [
            
            [5, 10, 15, 20, -1],
            
            [5, 10, 15, 20, "todos"]
            
            ],

            iDisplayLength: -1,
            
            pageLength: 200,

            dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        });

    }

    /**
    *
    * addListeners();
    * agrega los listeners para togglear la visibilidad o para eliminar, etc...
    */

    var addListeners = function(){

        $( ".toggle-event" ).change(function() {

            var id = $(this).parent().parent().parent().attr('id').split('-')[1];
            
            var active = $(this).prop('checked');

            updateEventStatus(active, Number(id));
            
        });

        $( ".edit-event" ).click(function() {

            blockScreen('REDIRECCIONANDO...');

            var id = $(this).parent().parent().attr('id').split('-')[1];

            window.location.href = "timeline_add.php?id=" + id;

            //...
            
        });

        
        $( ".edit-event-pictures" ).click(function() {

            blockScreen('REDIRECCIONANDO...');

            var id = $(this).parent().parent().attr('id').split('-')[1];

            window.location.href = "timeline_images_gallery.php?idEvent=" + id

        });


        $( ".delete-event" ).click(function() {

            var id = $(this).parent().parent().attr('id').split('-')[1];

            bootbox.dialog({
              
              message: '<b>¡ATENCIÓN!</b><br><br>Estás a punto de eliminar de manera definitiva un evento del sistema. Esta acción no se puede deshacer. Recordá que también podés usar el <b>botón ONLINE / OFFLINE</b>. Esto hará que el evento no se muestre en la timeline pero continúe guardado en el sistema.<br><br>¿Continuar?',

              buttons: {
                
                success: {

                    label: "SI, ELIMINAR EVENTO",

                    className: "btn-red red",

                    callback: function() { 

                        deleteEvent(id);

                    }
                    
                },

                danger: {

                    label: "CANCELAR",

                    className: "btn-danger",

                    callback: function() {

                    }

                }

            }
        });

        });

    }

    /**
    *
    * updateEventStatus();
    * actualiza el estado...
    */

    var updateEventStatus = function(active, id){

        $.ajax({

            type: "post",

            dataType: "json",

            data: {id:id, active:active},

            url: PRIVATE_DIR + 'actions/timeline-management/update_event_status.php',

            error: function(xhr, status, error) {

                var err = eval("(" + xhr.responseText + ")");

            },

            success: function(data) {

                var string;

                if(active){

                    string = "El evento ahora está ONLINE";
                
                }else{

                    string = "El evento ahora está OFFLINE";

                }

                toastr.info(string);
 
            }

        });

    }

    /**
    *
    * deleteEvent();
    * elimina una propiedad...
    */

    var deleteEvent = function(id){

        var removeId = id;

        $.ajax({

            type: "post",

            dataType: "json",

            data: {id:removeId},

            url: PRIVATE_DIR + 'actions/timeline-management/delete_event.php',

            error: function(xhr, status, error) {

                var err = eval("(" + xhr.responseText + ")");

            },

            success: function(data) {

                $('#tritem-' + removeId).hide('slow', function() {
                    
                });

                toastr.info("Se eliminó el evento.");

            }

        });

    }

    /**
    *
    * doHighlight();
    * do highlight directly...
    */

    var doHighlight = function(id){

        var colorFrom = '#80C572';

        var colorTo = '#FFFFFF';

        $('#tritem-' + id).css("background-color", colorFrom);

        var item = $('#tritem-' + id);

        TweenLite.to(item, 3, {backgroundColor:colorTo});

    }

    /**
    *
    * blockScreen(msg);
    * param msg - el mensaje a mostrar en el centro de la pantalla...
    * bloquea la pantalla mientras se opera con el server...
    */

    var blockScreen = function(msg){

        console.log("bloquea?!?!");

        $.blockUI({ message: msg,

            css: {border:'0px solid #FFFFFF',cursor:'wait',backgroundColor:'none', color:'#FFF'},

            overlayCSS:  { backgroundColor: '#000000',opacity:0.5,cursor:'wait'} 

        });
    }

    return {

        init: function () {

            if (!jQuery().dataTable) { return; }

            initTable();

            addListeners();

            var id = getURLParameter('highlight');

            if(id != null) doHighlight(id);

            toastr.options = {"closeButton": true, "debug": false, "newestOnTop": false, "progressBar": false, "positionClass": "toast-bottom-left", "preventDuplicates": false,

                "onclick": null, "showDuration": "300", "hideDuration": "1000", "timeOut": "5000", "extendedTimeOut": "1000", "showEasing": "swing",

                "hideEasing": "linear", "showMethod": "fadeIn", "hideMethod": "fadeOut"
            }

        }

    };

}();

jQuery(document).ready(function() { TimelineVersions.init(); });

