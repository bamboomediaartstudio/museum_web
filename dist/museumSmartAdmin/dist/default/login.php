<?php

require_once 'private/users/core/init.php';

$token = Token::generate();

?>

<!DOCTYPE html>

<html lang="en" >

	<head>

		<meta charset="utf-8" />

		<title>Museo de la Shoá | Login para administradores</title>
		
		<meta name="description" content="Latest updates and statistic charts">
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		
		<script>
        
          WebFont.load({
        
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
        
            active: function() {
        
                sessionStorage.fonts = true;
        
            }
        
          });
		
		</script>
		
		<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		
		<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
		
		<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico?v=2" />

		<script src='https://www.google.com/recaptcha/api.js?theme=dark'></script>
	
	</head>
	
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

		<div class="m-grid m-grid--hor m-grid--root m-page">

			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">

				<div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">

					<div class="m-stack m-stack--hor m-stack--desktop">

						<div class="m-stack__item m-stack__item--fluid">

							<div class="m-login__wrapper">
								
								<div class="m-login__signin">

									<div class="m-login__head">

										<h3 class="m-login__title">

											Login para administradores del Museo del Holocausto - CABA.

										</h3>

									</div>

									<form class="m-login__form m-form" action="" autocomplete="off">

										<input style="opacity: 0;position: absolute;">
										<input type="password" style="opacity: 0;position: absolute;">


										<div class="form-group m-form__group">

											<input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off" value="">

										</div>

										<div class="form-group m-form__group">

											<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Contraseña" name="password" value="">

										</div>

										<div class="form-group m-form__group" style="transform:scale(0.7);transform-origin:0 0">

											<br>	

											<div class="g-recaptcha" data-sitekey="6Le55WgUAAAAAJ-Jipq9CFuIi839nF-hRExdmUti"  data-callback="correctCaptcha"></div>
											<input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">



										</div>

										<div class="row m-login__form-sub">

											<div class="col m--align-left">

												<label class="m-checkbox m-checkbox--focus">

													<input type="checkbox" name="remember">Recordar
													
													<span></span>
												
												</label>
											</div>
											
											<div class="col m--align-right">
											
												<a href="javascript:;" id="m_login_forget_password" class="m-link">
											
													¿olvidaste el pass?
											
												</a>
											
											</div>
										</div>
										
										<div class="m-login__form-action">

											<input type="hidden" name="token" value="<?php echo $token;?>">
										
											<button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">Ingresar</button>
										
										</div>
									
									</form>
								
								</div>
								
								<div class="m-login__forget-password">
								
									<div class="m-login__head">
								
										<h3 class="m-login__title">¿Olvidaste tu contraseña? </h3>
								
										<div class="m-login__desc">Ingresá tu email para resetear la contraseña: </div>
									
									</div>
									
									<form class="m-login__form m-form" action="" >
									
										<div class="form-group m-form__group">
									
											<input class="form-control m-input" type="text" placeholder="Email" name="email_reset" id="email_reset" autocomplete="off" value="mariano.makedonsky@gmail.com">
									
										</div>
									
										<div class="m-login__form-action">

											<input type="hidden" name="token" value="<?php echo $token;?>">										
											<button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">Enviar</button>
											
											<button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">Cancelar</button>
									
										</div>
								
									</form>
								
								</div>
						
							</div>
					
						</div>
					
					</div>
				</div>
				
				<div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content" style="background-image: url(assets/app/media/img//bg/bg-1.jpg)">
				
					<div class="m-grid__item m-grid__item--middle">
				
						<h3 class="m-login__welcome">Museo del Holocausto</h3>

						<p class="m-login__msg">Sistema de administración para el Museo del Holocausto.</p>
					
					</div>
				
				</div>
			
			</div>
		
		</div>

		<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		
		<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
		
		<script src="assets/app/js/login.js" type="text/javascript"></script>

	</body>

</html>
