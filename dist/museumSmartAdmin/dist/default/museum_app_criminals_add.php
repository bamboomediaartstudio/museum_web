<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$actualId = Input::get('id');

$actualTab = 'general-tab';

if(Input::exists('get') && Input::get('tab')){

	$actualTab = Input::get('tab');

	//echo 'existe';

}

//echo $actualTab;

//return;

$editMode = false;

if(Input::exists("get") && Input::get('id')){

	$editMode = true;


	$mainQuery = DB::getInstance()->query('SELECT * FROM  app_museum_criminals

		WHERE id=? AND deleted = ?', [Input::get('id'), 0]);

	$actualCriminal = $mainQuery->first();

	$criminalName = $actualCriminal->name;

	$criminalSurname = $actualCriminal->surname;

	$criminalBirthDate = $actualCriminal->birth_date;

	$criminalBirthPlace = $actualCriminal->birth_place;

	$criminalDeathDate = $actualCriminal->death_date;

	$criminalDeathPlace = $actualCriminal->death_place;

	$criminalNationality = $actualCriminal->nationality;

	$criminalBranch = $actualCriminal->branch;

	$criminalParty = $actualCriminal->party;

	$criminalNickname = $actualCriminal->nickname;

	$criminalUnity = $actualCriminal->unity;

	$criminalRange = $actualCriminal->range;

	$criminalBio = $actualCriminal->bio;

	$criminalPicture = $actualCriminal->picture;

	$criminalHighlighted = ($actualCriminal->highlighted == 1) ? 'checked' : '';

	$criminalWasInArgentina = ($actualCriminal->was_in_argentina) ? 'checked' : '';


	//Country query from criminalNationality(id) - get Country name_es
	if($criminalNationality != 0){
		$countryQuery 					= DB::getInstance()->query('SELECT id, name_es FROM world_location_countries WHERE id = ?', [$criminalNationality]);


		$actualCriminalCountry 	= $countryQuery->first();

		$criminalCountry 				= $actualCriminalCountry->name_es;
	}else{

		$criminalCountry = "Seleccione un país";

	}


}else{


	$criminalName = '';

	$criminalSurname = '';

	$criminalBirthDate = '';

	$criminalBirthPlace = '';

	$criminalDeathDate = '';

	$criminalDeathPlace = '';

	$criminalNationality = '';

	$criminalBranch = '';

	$criminalParty = '';

	$criminalNickname = '';

	$criminalUnity = '';

	$criminalRange = '';

	$criminalBio = '';

	$criminalPicture = '';

	$criminalHighlighted = '';

	$criminalWasInArgentina = '';

}

//museum data...

$museumData = DB::getInstance();

$museumDataQ = $museumData->query('SELECT * FROM museum_data');

if($museumDataQ->count()>0) $museumDataObject = $museumDataQ->first();

//museum social media data...

$museumSocialMediaData = DB::getInstance();

$museumSocialMediaDataQ = $museumSocialMediaData->query('SELECT * FROM museum_social_media');

if($museumSocialMediaDataQ->count()>0) $museumSocialMediaObject = $museumSocialMediaDataQ->first();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Noticias | Agregar Noticia</title>

	<meta name="description" content="add new news">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{

		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	#map {

		height: 500px;

		width:100%;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}

</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<!--<input type="hidden" id="image-path" name="image-path" value="<?php //echo $imagePath;?>">-->

		<input type="hidden" id="criminal-id" name="criminal-id" value="<?php echo $actualId;?>">

		<!--<input type="hidden" id="news-url" name="news-url" value="<?php //echo $newsUrl;?>">-->

		<!--<input type="hidden" id="news-name" name="news-name" value="<?php //echo $newsTitle;?>">-->

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Noticias</h3>
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>

								<li class="m-nav__separator">-</li>

								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text">Agregar Noticia</span>

									</a>

								</li>

							</ul>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">

							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link <?php if($actualTab =='general-tab') echo 'active';?>" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Información general

												</a>

											</li>

											<!--
											<li class="nav-item m-tabs__item <?php if(!$editMode) echo 'disabled'; ?>">
												<a class="<?php if($actualTab =='images-tab') echo 'active';?> tab-navigation images-tab nav-link m-tabs__link" data-toggle="tab" href="#images-tab" role="tab">

													Imágenes

												</a>

											</li>
											-->

											<!--
											<li class="nav-item m-tabs__item <?php if(!$editMode) echo 'disabled'; ?>">
												<a class="<?php if($actualTab =='videos-tab') echo 'active';?> tab-navigation videos-tab nav-link m-tabs__link" data-toggle="tab" href="#videos-tab" role="tab">

													Videos

												</a>

											</li>
											-->
										</ul>

									</div>

								</div>


								<div class="tab-content">

									<!--
									<div class="tab-pane <?php if($actualTab =='videos-tab') echo 'active';?>" id="videos-tab">

										<div class="mt-5 mb-5 col-lg-12">

											<form id="youtube-form">

												<div class="form-group">


													<div class="form-group m-form__group row">

														<label class="col-form-label col-lg-3 col-sm-12">Link al video*:</label>

														<div class="col-lg-4 col-md-9 col-sm-12">

															<input id="youtube_id" type="text" class="form-control m-input" name="youtube_id" placeholder="URL del video" value="">

															<small class="yt-info m-form__help">Copiá y pegá acá la URL del video de <b>YouTube.</b></small>

															<div id="previews">

																<div id="preview-1"></div>

																<div id="preview-2"></div>

																<div id="preview-3"></div>


															</div>

															<input type="hidden" id="hidden-yt-id" name="hidden-yt-id" value="">

														</div>



														<div class="">

															<button data-db-value="" data-id="1" class="add-video-btn btn btn-default" type="submit">agregar

															</button>

														</div>


													</div>

												</div>

											</form>

											<input type="file" id="videos-batch" name="videos-batch[]" accept="image/*" multiple>

										</div>
									</div>-->

									<!--
									<div class="tab-pane <?php if($actualTab =='images-tab') echo 'active';?>" id="images-tab">

										<div class="mt-5 mb-5 col-lg-12">

											<input type="file" id="images-batch" name="images-batch[]" accept="image/*" multiple>

										</div>
									</div>
									-->

									<div class="tab-pane <?php if($actualTab =='general-tab') echo 'active';?>" id="general-tab">

										<form id="add-criminals-data-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>

												</div>

											</div>


											<!-- Name -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Nombre del criminal:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="name" type="text" class="form-control m-input" name="name" placeholder="Nombre" value="<?php echo $criminalName;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $criminalName;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Name -->

											<!-- Surname -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Apellido del criminal:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="surname" type="text" class="form-control m-input" name="surname" placeholder="Apellido" value="<?php echo $criminalSurname;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $criminalSurname;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Surname -->


											<!-- Birth date -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Fecha de nacimiento:

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Fecha de nacimiento del criminal">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-12">

													<div class="input-group date">
														<input id="birth_date" name="birth_date" type="text" class="form-control" value="<?php if($editMode) echo $criminalBirthDate;?>">
														<div class="input-group-addon">
															<i class="pt-2 far fa-calendar-alt"></i>
														</div>
													</div>

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $criminalBirthDate;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /. Birth date -->

											<!-- Birth place -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Lugar de nacimiento:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="birth_place" type="text" class="form-control m-input" name="birth_place" placeholder="Lugar de nacimiento" value="<?php echo $criminalBirthPlace;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $criminalBirthPlace;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Birth place -->


											<!-- death date -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Fecha de muerte:

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Fecha de muerte del criminal">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-12">

													<div class="input-group date">
														<input id="death_date" name="death_date" type="text" class="form-control" value="<?php if($editMode) echo $criminalDeathDate;?>">
														<div class="input-group-addon">
															<i class="pt-2 far fa-calendar-alt"></i>
														</div>
													</div>

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $criminalDeathDate;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /. Death date -->

											<!-- Death place -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Lugar de muerte:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="death_place" name="death_place" type="text" class="form-control m-input" placeholder="Lugar de muerte" value="<?php echo $criminalDeathPlace;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $criminalDeathPlace;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Death place -->



											<!-- Nationality -->
											<div id="country-list" class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Nacionalidad

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Comenzá a escribir el pais y seleccionalo de la lista">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input type="text" id="nationality" name="nationality" class="typeahead form-control m-input" placeholder="Nacionalidad" value="<?php echo  ($editMode) ? $criminalCountry : ''; ?>">

													<input type="hidden" id="hidden-country-id" name="hidden-country-id" value="<?php echo $criminalNationality; ?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="countryFlag" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Nationality -->

											<!-- Branch -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Rama</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="branch" type="text" class="form-control m-input" name="branch" placeholder="Rama" value="<?php echo $criminalBranch;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $criminalBranch;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Branch -->

											<!-- Party -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Partido</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="party" type="text" class="form-control m-input" name="party" placeholder="Partido" value="<?php echo $criminalParty;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $criminalParty;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Party -->

											<!-- Nickname -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Apodo</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="nickname" type="text" class="form-control m-input" name="nickname" placeholder="Apodo" value="<?php echo $criminalNickname;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $criminalNickname;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Nickname -->

											<!-- Unity -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Unidad</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="unity" type="text" class="form-control m-input" name="unity" placeholder="Unidad" value="<?php echo $criminalUnity;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $criminalUnity;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Unity -->

											<!-- Range -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Rango</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="range" type="text" class="form-control m-input" name="range" placeholder="Rango" value="<?php echo $criminalRange;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $criminalRange;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Range -->

											<!-- bio -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Biografía:</label>

												<div class="col-lg-6 col-md-9 col-sm-12">

													<textarea numlines="20" type="text" class="form-control m-input description" id="bio" name="bio" placeholder="Biografía"><?php echo trim($criminalBio); ?></textarea>

													<?php if($editMode){  ?>

														<br>

														<div class="">

															<button data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>

												</div>

											</div>
											<!-- /.bio -->

											<!-- Profile Picture -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Imagen de Perfil*:
													<br><small>Es la imagen de perfil del criminal y tiene que ser como minimo de 300 x 500 px.<br>
													</small>

													<?php if($editMode){ ?>
														<br />
														<small data-toggle="tooltip" data-placement="bottom" title="Una vez seleccionada la imagen, apretar en 'subir archivo' para guardarla correctamente">
															Como actualizar la imagen?
														</small>

													<?php }else{

														?>

														<small data-toggle="tooltip" data-placement="bottom" title="Porque al partir de una imagen grande, nuestros servidores pueden resamplearla a diversas resoluciones sin pérdida de calidad. El tamaño máximo es de 20 MB.">
															¿por qué este tamaño?
														</small>

														<?php
													}

													?>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input id="picture" name="picture" type="file">

												</div>

											</div>
											<!-- /.Profile Picture -->

											<!-- Was in argentina -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Estuvo en Argentina?

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Tildar a los criminales que estuvieron en Argentina">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="was_in_argentina" type="checkbox" <?php echo $criminalWasInArgentina;?> class="highlighted toggler-info" name="was_in_argentina" data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>
											<!-- /.Was in argentina -->

											<!-- Highlighted -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Resaltar

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Resalta este criminal por algún motivo especial">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="highlighted" type="checkbox" <?php echo $criminalHighlighted;?> class="highlighted toggler-info" name="highlighted" data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>
											<!-- /.Highlighted -->

											<!-- ... -->






											<!-- ... -->

											<div class="m-portlet__foot m-portlet__foot--fit">

												<div class="m-form__actions">

													<div class="row">

														<div class="col-2"></div>

														<div class="col-7">

															<input type="hidden" name="token" value="<?php echo Token::generate()?>">

															<?php

															$hiddenClass = ($editMode) ? 'd-none' : '';

															?>

															<button id="add_news" type="submit"

															class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar Criminal</button>
															&nbsp;&nbsp;

															<?php

															$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

															$buttonId =

															(!$editMode) ? 'exit_from_form' : 'back_to_list';

															?>

															<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																<?php echo $buttonLabel;?>

															</button>

														</div>

													</div>

												</div>

											</div>

										</form>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>


	<script src="assets/app/js/museum/app_criminals/museum-app-criminals-add.js" type="text/javascript"></script>

</body>

</html>
