<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$actualId = Input::get('id');

$actualTab = 'general-tab';

if(Input::exists('get') && Input::get('tab')){

	$actualTab = Input::get('tab');

	//echo 'existe';

}

//echo $actualTab;

//return;

$editMode = false;

$actualRelationsList = [];

if(Input::exists("get") && Input::get('id')){

	$editMode = true;

	$mainQuery = DB::getInstance()->query('SELECT * FROM  app_museum_films

		WHERE id=? AND deleted = ?', [Input::get('id'), 0]);

	$actualFilm				    = $mainQuery->first();

	$filmTitle 						= $actualFilm->title;

	$filmDescription 			= $actualFilm->description;

	$filmSynopsis 			  = $actualFilm->synopsis;

	$filmDirector 				= $actualFilm->director;

	$filmYear 			      = $actualFilm->year;

	$filmLinkNetflix 			= $actualFilm->link_netflix;

	$filmLinkYoutube 		  = $actualFilm->link_youtube;

	$filmLinkImdb				  = $actualFilm->link_imdb;

	$filmLinkOther 			  = $actualFilm->link_other;

	$filmCover			      = $actualFilm->picture;

	$filmIdCountry        = $actualFilm->id_country;

	//Country query from country - get Country name_es
	if($filmIdCountry != 0){	//id = 0 doesnt exist in countries

		$countryQuery 			= DB::getInstance()->query('SELECT id, name_es FROM world_location_countries WHERE id = ?', [$filmIdCountry]);

		$actualFilmCountry 	= $countryQuery->first();

		$filmCountry 		    = $actualFilmCountry->name_es;

	}else{

		$filmCountry = "";

	}

	//Get catgories relations for this film

	$filmRelationQuery = DB::getInstance()->query('SELECT id, id_film, id_category, deleted FROM app_museum_films_relations WHERE id_film = ? AND deleted = ?', [Input::get('id'), 0]);

	$actualRelationsList = $filmRelationQuery->results(true);


}else{

  $filmTitle 						= '';

	$filmDescription 			= '';

  $filmSynopsis         = '';

	$filmDirector 				= '';

	$filmYear 			      = '';

	$filmLinkNetflix 			= '';

	$filmLinkYoutube 		  = '';

	$filmLinkImdb				  = '';

	$filmLinkOther 			  = '';

	$filmCover			      = '';

	$filmIdCountry        = '';


}

//museum data...

$museumData = DB::getInstance();

$museumDataQ = $museumData->query('SELECT * FROM museum_data');

if($museumDataQ->count()>0) $museumDataObject = $museumDataQ->first();

//museum social media data...

$museumSocialMediaData = DB::getInstance();

$museumSocialMediaDataQ = $museumSocialMediaData->query('SELECT * FROM museum_social_media');

if($museumSocialMediaDataQ->count()>0) $museumSocialMediaObject = $museumSocialMediaDataQ->first();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Peliculas | Agregar Película</title>

	<meta name="description" content="add new Survivor">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{

		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}


</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">


		<input type="hidden" id="film-id" name="film-id" value="<?php echo $actualId;?>">


		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Peliculas</h3>
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>

								<li class="m-nav__separator">-</li>

								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text">Agregar Pelicula</span>

									</a>

								</li>

							</ul>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">

							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link <?php if($actualTab =='general-tab') echo 'active';?>" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Información general

												</a>

											</li>

                      <!--
											<li class="nav-item m-tabs__item <?php if(!$editMode) echo 'disabled'; ?>">
												<a class="<?php if($actualTab =='images-tab') echo 'active';?> tab-navigation images-tab nav-link m-tabs__link" data-toggle="tab" href="#images-tab" role="tab">

													Imágenes

												</a>

											</li>-->



										</ul>

									</div>

								</div>


								<div class="tab-content">


									<!--<div class="tab-pane <?php if($actualTab =='images-tab') echo 'active';?>" id="images-tab">

										<div class="mt-5 mb-5 col-lg-12">

											<input type="file" id="images-batch" name="images-batch[]" accept="image/*" multiple>

										</div>

									</div>-->

									<!-- general tab -->

									<div class="tab-pane <?php if($actualTab =='general-tab') echo 'active';?>" id="general-tab">

										<form id="add-film-data-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>

												</div>

											</div>


											<!-- Title -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Titulo:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="title" name="title" type="text" class="form-control m-input" placeholder="Título" value="<?php echo $filmTitle;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $filmTitle;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Title -->

											<!-- Director -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Director</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="director" name="director" type="text" class="form-control m-input" placeholder="Director" value="<?php echo $filmDirector; ?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $filmDirector; ?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Director -->

											<!-- Year -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Año

												</label>

												<div class="col-lg-4 col-12 input-group">

													<div class="input-group date">
														<input type="text" class="form-control m-input" id="year" name="year" value="<?php if($editMode) echo ($filmYear==0) ? '' : $filmYear; ?>">
														<div class="input-group-addon">
															<i class="pt-2 far fa-calendar-alt"></i>
														</div>
													</div>

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $filmYear; ?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /. Year -->

                      <!-- Synopsis -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Sinopsis:</label>

												<div class="col-lg-6 col-md-9 col-sm-12">

													<textarea numlines="20" type="text" class="form-control m-input description" id="synopsis" name="synopsis" placeholder="Sinopsis"><?php echo trim($filmSynopsis); ?></textarea>

													<?php if($editMode){  ?>

														<br>

														<div class="">

															<button data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>

												</div>

											</div>
											<!-- /.Synopsis -->

											<!-- País -->
											<div id="country-list" class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">País:</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input type="text" id="id_country" name="id_country" class="typeahead form-control m-input" placeholder="País" value="<?php echo ($editMode) ? $filmCountry : ''; ?>">

													<input type="hidden" id="hidden-country-id" name="hidden-country-id" value="<?php echo $filmIdCountry; ?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="countryFlag" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.País -->

											<!-- Link Netflix -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Link Netflix:*</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="link_netflix" name="link_netflix" type="text" class="form-control m-input link" placeholder="Link Netflix" value="<?php echo $filmLinkNetflix; ?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $filmLinkNetflix; ?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Link Netflix -->

                      <!-- Link Youtube -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Link Youtube:*</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="link_youtube" name="link_youtube" type="text" class="form-control m-input link" placeholder="Link Youtube" value="<?php echo $filmLinkYoutube; ?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $filmLinkYoutube; ?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Link Youtube -->

                      <!-- Link IMDB -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Link IMDB:*</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="link_imdb" name="link_imdb" type="text" class="form-control m-input link" placeholder="Link Youtube" value="<?php echo $filmLinkImdb; ?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $filmLinkImdb; ?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Link IMDB -->

                      <!-- Link Otro -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Link Otro:*</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="link_other" name="link_other" type="text" class="form-control m-input link" placeholder="Link Otro" value="<?php echo $filmLinkOther; ?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $filmLinkOther; ?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Link Otro -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12"></label>

												<div class="col-lg-4 col-12 input-group">

													<span class="alert-text"><b>* Ingresá</b> al menos un Link
													</span>

												</div>

											</div>


											<!-- Descripcion -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Descripción:</label>

												<div class="col-lg-6 col-md-9 col-sm-12">

													<textarea numlines="20" type="text" class="form-control m-input description" id="description" name="description" placeholder="Description"><?php echo trim($filmDescription); ?></textarea>

													<?php if($editMode){  ?>

														<br>

														<div class="">

															<button data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>

												</div>

											</div>
											<!-- /.Descripcion -->


											<!-- Categories -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Categorias:*

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="No olvides seleccionar al menos una cateogoría">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-checkbox-list">

														<?php

														$categoriesQuery = DB::getInstance()->query('SELECT * FROM app_museum_categories');

														foreach($categoriesQuery->results() as $c){

															$checked = '';


															foreach($actualRelationsList as $idRel => $item){

																if($c->id == $item["id_category"]) $checked = 'checked';
															}

															?>

															<label class="m-checkbox m-checkbox--solid m-checkbox--success">

																<input <?php echo $checked;?> value="<?php echo $c->id;?>" name="checkboxes[]" type="checkbox" class="" data-id="<?php echo $actualId;?>">

																<?php echo $c->name;?>

																<span></span>

															</label>


														<?php } ?>

													</div>

													<span class="m-form__help"><b>Seleccioná</b> al menos una de las opciones.
													</span>

													<?php if($editMode){  ?>

														<br>

														<div class="">

															<button data-db-value="categoriesFlag" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
															</button>

														</div>

													<?php }?>

												</div>

											</div>
											<!-- /.categories -->

											<!-- Cover -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Foto de Pelicula:
													<br><small>Es la imagen de la película y tiene que ser de 1200 x 1600 px (aspect ratio 4:3).<br>

														<small data-toggle="tooltip" data-placement="bottom" title="Porque al partir de una imagen grande, nuestros servidores pueden crear otras versiones a diversas resoluciones sin pérdida de calidad. El tamaño máximo es de 20 MB.">
															¿por qué tan grande?
														</small>


													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input id="picture" name="picture" type="file">

												</div>

											</div>
											<!-- /.Cover -->


											<!-- ... -->

											<div class="m-portlet__foot m-portlet__foot--fit">

												<div class="m-form__actions">

													<div class="row">

														<div class="col-2"></div>

														<div class="col-7">

															<input type="hidden" name="token" value="<?php echo Token::generate()?>">

															<?php

															$hiddenClass = ($editMode) ? 'd-none' : '';

															?>

															<button id="add_film" type="submit"

															class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar Pelicula</button>
															&nbsp;&nbsp;

															<?php

															$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

															$buttonId =

															(!$editMode) ? 'exit_from_form' : 'back_to_list';

															?>

															<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																<?php echo $buttonLabel;?>

															</button>

														</div>

													</div>

												</div>

											</div>

										</form>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="assets/app/js/museum/app_films/museum-app-films-add.js" type="text/javascript"></script>

</body>

</html>
