<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$actualId = Input::get('id');

$actualTab = 'general-tab';

if(Input::exists('get') && Input::get('tab')){

	$actualTab = Input::get('tab');

	//echo 'existe';

}

//echo $actualTab;

//return;

$editMode = false;

if(Input::exists("get") && Input::get('id')){

	$editMode = true;

	$mainQuery = DB::getInstance()->query('SELECT * FROM  app_museum_guetos

		WHERE id=? AND deleted = ?', [Input::get('id'), 0]);


	$actualGueto 				= $mainQuery->first();

	$guetoName			 		= $actualGueto->name;

	$guetoIdCountry 		= $actualGueto->id_country;

	$guetoYearStart 		= $actualGueto->year_start;

	$guetoYearEnd 			= $actualGueto->year_end;

	$guetoLat 					= $actualGueto->lat;

	$guetoLng 					= $actualGueto->lng;

	$guetoDescription 	= $actualGueto->description;

	//Country query from id_country - get Country name_es

	$countryQuery 			= DB::getInstance()->query('SELECT id, name_es FROM world_location_countries WHERE id = ?', [$guetoIdCountry]);

	$actualGuetoCountry = $countryQuery->first();

	$guetoCountry 			= $actualGuetoCountry->name_es;

}else{

	$guetoName 				= '';

	$guetoCountry 		= '';

	$guetoYearStart 	= '';

	$guetoYearEnd 		= '';

	$guetoLat 				= '';

	$guetoLng 				= '';

	$guetoDescription = '';

	$guetoCountry 		= '';
}

//museum data...

$museumData = DB::getInstance();

$museumDataQ = $museumData->query('SELECT * FROM museum_data');

if($museumDataQ->count()>0) $museumDataObject = $museumDataQ->first();

//museum social media data...

$museumSocialMediaData = DB::getInstance();

$museumSocialMediaDataQ = $museumSocialMediaData->query('SELECT * FROM museum_social_media');

if($museumSocialMediaDataQ->count()>0) $museumSocialMediaObject = $museumSocialMediaDataQ->first();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Guetos | Agregar Guetos</title>

	<meta name="description" content="add new news">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{

		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	#map {

		height: 500px;

		width:100%;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}

	element.style {
    top: 441.063px;
    left: 566px;
    z-index: 10;
    display: block;
}


</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<!-- image-modal-description -->

	<div id="myModal" class="modal" tabindex="-1" role="dialog">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title">Asignar descripción a la imagen</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body">

					<form>

						<div class="form-group">

							<label for="image-description" class="col-form-label">Descripción:</label>

							<input type="text" class="form-control" id="image-description">

						</div>

					</form>

				</div>

				<div class="modal-footer">

					<button type="button" class="save-description btn btn-primary">guardar</button>

					<button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>

				</div>

			</div>

		</div>

	</div>
	<!-- /. image-modal-description -->


	<div class="m-grid m-grid--hor m-grid--root m-page">

		<input type="hidden" id="gueto-id" name="gueto-id" value="<?php echo $actualId;?>">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Guetos</h3>

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>

								<li class="m-nav__separator">-</li>

								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text">Agregar Gueto</span>

									</a>

								</li>

							</ul>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">

							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<!-- Tabs titles -->
								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link <?php if($actualTab =='general-tab') echo 'active';?>" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Información general

												</a>

											</li>

											<li class="nav-item m-tabs__item <?php if(!$editMode) echo 'disabled'; ?>">
												<a class="<?php if($actualTab =='images-tab') echo 'active';?> tab-navigation images-tab nav-link m-tabs__link" data-toggle="tab" href="#images-tab" role="tab">

													Imágenes

												</a>

											</li>


										</ul>

									</div>

								</div>

								<!-- Tabs contents -->
								<div class="tab-content">

									<div class="tab-pane <?php if($actualTab =='images-tab') echo 'active';?>" id="images-tab">

										<div class="mt-5 mb-5 col-lg-12">

											<input type="file" id="images-batch" name="images-batch[]" accept="image/*" multiple>

										</div>
									</div>
									<!--
									<div class="tab-pane <?php if($actualTab =='images-tab') echo 'active';?>" id="images-tab">

										<div class="mt-5 mb-5 col-lg-12">

											<input type="file" id="images-batch" name="images-batch[]" accept="image/*" multiple>

										</div>
									</div>-->

									<div class="tab-pane <?php if($actualTab =='general-tab') echo 'active';?>" id="general-tab">

										<form id="add-guetos-data-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>

												</div>

											</div>


											<!-- Name -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">* Nombre del Gueto:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="name" type="text" class="form-control m-input" name="name" placeholder="* Nombre" value="<?php echo $guetoName;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $guetoName;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Name -->

											<!-- Country -->
											<div id="country-list" class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">* País</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input type="text" id="country" name="country" class="typeahead form-control m-input" placeholder="* País" value="<?php echo $guetoCountry; ?>">

													<input type="hidden" id="hidden-country-id" name="hidden-country-id" value="<?php echo $guetoIdCountry; ?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="countryFlag" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Country -->

											<!-- Year Start -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">* Año de Inicio

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Año de inicio de actividades de este Gueto">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-12">

													<div class="input-group date">
														<input type="text" class="form-control" id="year_start" name="year_start" placeholder="* Seleccionar año de inicio" value="<?php if($editMode) echo $guetoYearStart; ?>">
														<div class="input-group-addon">
															<i class="pt-2 far fa-calendar-alt"></i>
														</div>
													</div>

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $guetoYearStart;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /. Year Start -->

											<!-- Year End -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">* Año de Fin

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Año de fin de actividades de este gueto">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-12">

													<div class="input-group date">
														<input type="text" class="form-control" id="year_end" name="year_end" disabled placeholder="* Seleccionar año de fin" value="<?php if($editMode) echo $guetoYearEnd; ?>">
														<div class="input-group-addon">
															<i class="pt-2 far fa-calendar-alt"></i>
														</div>
													</div>

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $guetoYearEnd; ?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Year End -->

											<!-- Bio -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Descripcion:</label>

												<div class="col-lg-6 col-md-9 col-sm-12">

													<textarea numlines="20" type="text" class="form-control m-input description" id="description" name="description" placeholder="Descripcion"><?php echo trim($guetoDescription); ?></textarea>

													<?php if($editMode){  ?>

														<br>

														<div class="">

															<button data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>

												</div>

											</div>
											<!-- /.Bio -->

											<!-- lat & lng -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">* Coordenadas

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Ingresá la Latitud y Longitud del Gueto. ESTOS CAMPOS SON OBLIGATORIOS">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="input-group">

														<input id="guetoLat" type="text" class="form-control m-input" name="guetoLat" placeholder="* Latitud" value="<?php echo $guetoLat;?>">

														<div class=""> <span class="input-group-text">

															<i class="la la-ellipsis-h"></i> </span>

														</div>

														<input id="guetoLng" type="text" class="form-control" name="guetoLng" placeholder="* Longitud" value="<?php echo $guetoLng;?>">

													</div>

													<!--<span class="m-form__help"></span>-->

													<div class="">

														<button data-db-value="" data-id="<?php echo $actualId;?>" class="search-gueto-map search-btn btn btn-default" type="button">Localizar en mapa</button>

														<?php if($editMode){ ?>

																<button data-db-value="latLngFlag" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
																</button>

														<?php }?>

													</div>

												</div>

											</div>

											<!-- /. lat & lng -->


												<!-- map -->
												<div class="form-group m-form__group row pt-4">


													<div class="offset-lg-3 col-lg-6 col-md-9 col-12 input-group">

														<div id="map"></div>

														<input type="hidden" id="lat" name="lat" value="<?php echo $guetoLat;?>">

														<input type="hidden" id="lng" name="lng" value="<?php echo $guetoLng;?>">

													</div>

												</div>
												<!-- /.map -->




											<!-- ... -->

											<div class="m-portlet__foot m-portlet__foot--fit">

												<div class="m-form__actions">

													<div class="row">

														<div class="col-2"></div>

														<div class="col-7">

															<input type="hidden" name="token" value="<?php echo Token::generate()?>">

															<?php

															$hiddenClass = ($editMode) ? 'd-none' : '';

															?>

															<button id="add_gueto" type="submit"

															class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar Gueto</button>
															&nbsp;&nbsp;

															<?php

															$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

															$buttonId =

															(!$editMode) ? 'exit_from_form' : 'back_to_list';

															?>

															<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																<?php echo $buttonLabel;?>

															</button>

														</div>

													</div>

												</div>

											</div>

										</form>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="assets/app/js/museum/app_guetos/museum-app-guetos-add.js" type="text/javascript"></script>

	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtjbRNPncz0LJT5WPKwuTS4tPoOWPVU5U&callback=initMap"></script>

</body>

</html>
