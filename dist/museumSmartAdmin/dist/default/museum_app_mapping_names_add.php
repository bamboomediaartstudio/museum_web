<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';


/**
** Page General Data
** Set
*/

$page_title = "Museo de la Shoá | Mapping | Agregar Datos";
$page_subtitle_h3 = "Mapping";
$page_subtitle_li = "Agregar Datos";
$page_subtitle_i = "Mapping";

$page_code_url = "assets/app/js/museum/mapping/museum-app-mapping-add.js";

/* End Set general data */


$actualId = Input::get('id');

$actualTab = 'general-tab';

if(Input::exists('get') && Input::get('tab')){

	$actualTab = Input::get('tab');

}

$editMode = false;

if(Input::exists("get") && Input::get('id')){

	$editMode = true;


	$mainQuery = DB::getInstance()->query('SELECT * FROM  app_museum_mapping

		WHERE id=? AND deleted = ?', [Input::get('id'), 0]);

	$actualMapping = $mainQuery->first();

	$name = $actualMapping->name;

	$surname = $actualMapping->surname;

	$birthDate = $actualMapping->birth_date;

	$mappingNationality = $actualMapping->nationality;


	//Country query from criminalNationality(id) - get Country name_es
	if($mappingNationality != 0){
		$countryQuery 					= DB::getInstance()->query('SELECT id, name_es FROM world_location_countries WHERE id = ?', [$mappingNationality]);


		$actualMappingCountry 	= $countryQuery->first();

		$mappingCountry 				= $actualMappingCountry->name_es;
	}else{

		$mappingCountry = "Seleccione un país";

	}


}else{

	$name = '';

	$surname = '';

	$birthDate = '';

	$mappingNationality = '';

}


//museum data...

$museumData = DB::getInstance();

$museumDataQ = $museumData->query('SELECT * FROM museum_data');

if($museumDataQ->count()>0) $museumDataObject = $museumDataQ->first();

//museum social media data...

$museumSocialMediaData = DB::getInstance();

$museumSocialMediaDataQ = $museumSocialMediaData->query('SELECT * FROM museum_social_media');

if($museumSocialMediaDataQ->count()>0) $museumSocialMediaObject = $museumSocialMediaDataQ->first();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title><?php echo $page_title; ?></title>

	<meta name="description" content="add new Survivor">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{

		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}


</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">


		<input type="hidden" id="mapping-data-id" name="mapping-data-id" value="<?php echo $actualId;?>">


		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator"><?php echo $page_subtitle_h3; ?></h3>
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>

								<li class="m-nav__separator">-</li>

								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text"><?php echo $page_subtitle_li; ?></span>

									</a>

								</li>

							</ul>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">

							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link <?php if($actualTab =='general-tab') echo 'active';?>" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Información general

												</a>

											</li>


										</ul>

									</div>

								</div>


								<div class="tab-content">

									<!-- general tab -->

									<div class="tab-pane <?php if($actualTab =='general-tab') echo 'active';?>" id="general-tab">

										<form id="add-mapping-data-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>

												</div>

											</div>


											<!-- Name -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Nombre:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="name" type="text" class="form-control m-input" name="name" placeholder="Nombre" value="<?php echo $name;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $name;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Name -->

											<!-- Surname -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Apellido</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="surname" type="text" class="form-control m-input" name="surname" placeholder="Apellido" value="<?php echo $surname; ?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $surname; ?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Surname -->

											<!-- Birth Date -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Fecha de nacimiento

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Fecha de nacimiento">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-12">

													<div class="input-group date">
														<input type="text" class="form-control" id="birth_date" name="birth_date" value="<?php if($editMode) echo $birthDate; ?>">
														<div class="input-group-addon">
															<i class="pt-2 far fa-calendar-alt"></i>
														</div>
													</div>

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $birthDate;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /. Birth Date -->

											<!-- Nationality -->
											<div id="country-list" class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Nacionalidad

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Comenzá a escribir el pais y seleccionalo de la lista">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input type="text" id="nationality" name="nationality" class="typeahead form-control m-input" placeholder="Nacionalidad" value="<?php echo  ($editMode) ? $mappingCountry : ''; ?>">

													<input type="hidden" id="hidden-country-id" name="hidden-country-id" value="<?php echo $mappingNationality; ?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="countryFlag" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Nationality -->

											<!-- ... -->

											<div class="m-portlet__foot m-portlet__foot--fit">

												<div class="m-form__actions">

													<div class="row">

														<div class="col-2"></div>

														<div class="col-7">

															<input type="hidden" name="token" value="<?php echo Token::generate()?>">

															<?php

															$hiddenClass = ($editMode) ? 'd-none' : '';

															?>

															<button id="add_data" type="submit"

															class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar</button>
															&nbsp;&nbsp;

															<?php

															$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

															$buttonId =

															(!$editMode) ? 'exit_from_form' : 'back_to_list';

															?>

															<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																<?php echo $buttonLabel;?>

															</button>

														</div>

													</div>

												</div>

											</div>

										</form>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="<?php echo $page_code_url; ?>" type="text/javascript"></script>

</body>

</html>
