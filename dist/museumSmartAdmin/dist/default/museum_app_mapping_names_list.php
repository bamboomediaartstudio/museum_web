<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

/**
** Page General Data
** Set
*/
$page_title = "Museo de la Shoá | Mapping | Lista";
$page_subtitle_h3 = "Mapping";
$page_subtitle_li = "Nombre de Mapping";
$page_subtitle_i = "Mapping";

$page_header_note = 'Esta página incluye los <strong>nombres</strong> de la app del Mapping del museo. Podés cambiar la visibilidad de los mismas a través del botón de <strong>Estado</strong>. También podés <i class="la la-edit"></i><strong>editar</strong> y <i class="la la-trash"></i><strong>eliminar</strong> cada Trivia.<br><br>¿Querés agregar un nuevo nombre?<br><br>';
$page_header_href = 'museum_app_mapping_names_add.php';

$page_code_url = "assets/app/js/museum/mapping/museum-app-mapping-list.js";

/* End Set general data */


$url = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);

$errorReportQuery = DB::getInstance()->query("SELECT id FROM museum_bugs_subjects WHERE url = ?", array($url));

$errorReportId =  $errorReportQuery->first()->id;


//Get all countries

$countryQuery 	= DB::getInstance()->query('SELECT id, name_es FROM world_location_countries');

$countryArray = $countryQuery->results(true); // true for assoc array

?>


<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title><?php echo $page_title; ?></title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/af-2.3.0/r-2.2.2/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>

	<link href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.1/cropper.min.css" rel="stylesheet" type="text/css" />


	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.table-cell-edit{

		background-color: #efefef !important;

		cursor:move;

		border: 1px solid #efefef;

		-webkit-box-shadow: 5px 0 5px -2px #ddd;

		box-shadow: 5px 0 5px -2px #ddd;

	}

	.modal-lg {

		min-width: 80%;

		/*margin: auto;*/

	}

	.temp-test{

		max-width: 100%;

		max-height:550px;

		overflow: hidden !important;

	}

	#result{

		width: 600px;

		height:600px;

		overflow: hidden !important;

	}

	.preview-container, .real-container{

		overflow: hidden !important;

	}


</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">

								<?php echo $page_subtitle_h3; ?>

							</h3>

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon">

										<i class="m-nav__link-icon la la-home"></i>

									</a>

								</li>

								<li class="m-nav__separator">-</li>

								<li class="m-nav__item"> <span class="m-nav__link-text"><?php echo $page_subtitle_li; ?></span></li>

							</ul>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">

						<div class="m-alert__icon"><i class="flaticon-exclamation m--font-brand"></i></div>

						<div class="m-alert__text">

							<?php echo $page_header_note; ?>

							<a class="btn btn-xs btn-success" href="<?php echo $page_header_href; ?>">Agregar</a><br><br>

							¿Algo no funciona? <a class="" href="museum_bug_report.php?id=<?php echo $errorReportId;?>">reportar error.</a><br><br>

						</div>

					</div>

					<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">

						<div class="m-portlet__head">

							<div class="m-portlet__head-tools">

								<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
								role="tablist">

								<li class="nav-item m-tabs__item" data-id="">

									<a class="nav-link m-tabs__link  active" data-toggle="tab_1" href="" role="tab">

										<i class="flaticon-share m--hide"></i><?php echo $page_subtitle_i; ?>

									</a>

								</li>

							</ul>

						</div>

					</div>

					<div class="tab-content">

						<div class="tab-pane active" id="tab_1">

							<div class="m-portlet__body">

								<table id="tab-1" class="table display table-striped table-bordered responsive nowrap" style="width:100%">

									<thead>

										<tr>

											<th>Orden</th>

											<th>id</th>

											<th>Nombre</th>

											<th>Apellido</th>

											<th>Estado</th>

											<th>Acciones</th>

										</tr>

									</thead>

								</table>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

	<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">

	<i class="la la-arrow-up"></i>

</div>

<input type="hidden" id="token" name="token" value="<?php echo Token::generate();?>">


<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/ScrollToPlugin.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/af-2.3.0/r-2.2.2/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>


<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="<?php echo $page_code_url; ?>" type="text/javascript"></script>

</body>

</html>
