<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$actualId = Input::get('id');

$actualTab = 'general-tab';

if(Input::exists('get') && Input::get('tab')){

	$actualTab = Input::get('tab');

	//echo 'existe';

}

//Sql - get all newspapers for select
$getNewspapersQuery = DB::getInstance()->query("SELECT id, name, deleted FROM app_museum_prensa_newspapers WHERE deleted = ?", array(0));

$newspapersListArray = $getNewspapersQuery->results(true);


//echo $actualTab;

//return;

$editMode = false;

if(Input::exists("get") && Input::get('id')){

	$editMode = true;

	$mainQuery = DB::getInstance()->query('SELECT * FROM  app_museum_prensa

		WHERE id=? AND deleted = ?', [Input::get('id'), 0]);

	$actualPrensa = $mainQuery->first();

	$prensaTitle = $actualPrensa->title;

	$id_newspaper_actual = $actualPrensa->id_newspaper;

	$prensaDate = $actualPrensa->date;

	$prensaImage = $actualPrensa->picture;


}else{

	$prensaTitle = '';

//	$prensaNewspaper = '';

	$id_newspaper_actual = 0;

	$prensaDate = '';

	$prensaImage = '';


}

//museum data...

$museumData = DB::getInstance();

$museumDataQ = $museumData->query('SELECT * FROM museum_data');

if($museumDataQ->count()>0) $museumDataObject = $museumDataQ->first();

//museum social media data...

//TODO::Borrar social media, no se esta usando.

//$museumSocialMediaData = DB::getInstance();

//$museumSocialMediaDataQ = $museumSocialMediaData->query('SELECT * FROM museum_social_media');

//if($museumSocialMediaDataQ->count()>0) $museumSocialMediaObject = $museumSocialMediaDataQ->first();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Prensa | Agregar Prensa</title>

	<meta name="description" content="add new news">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{

		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	#map {

		height: 500px;

		width:100%;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}

	/* Tags styles */
	div.tagsinput { background: #FFF; padding:5px; width:300px; height:100px; overflow-y: auto;}
	div.tagsinput span.tag { border: 1px solid #a5d24a; -moz-border-radius:2px; -webkit-border-radius:2px; display: block; float: left; padding: 5px; text-decoration:none; background: #cde69c; color: #638421; margin-right: 5px; margin-bottom:5px;font-family: helvetica;  font-size:13px;}
	div.tagsinput span.tag a { font-weight: bold; color: #82ad2b; text-decoration:none; font-size: 11px;  }
	div.tagsinput input { width:80px; margin:0px; font-family: helvetica; font-size: 13px; border:1px solid transparent; padding:5px; background: transparent; color: #000; outline:0px;  margin-right:5px; margin-bottom:5px; }
	div.tagsinput div { display:block; float: left; }
	.tags_clear { clear: both; width: 100%; height: 0px; }
	.not_valid {background: #FBD8DB !important; color: #90111A !important;}
	/* Tags styles Ends*/

</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">


		<input type="hidden" id="prensa-id" name="prensa-id" value="<?php echo $actualId;?>">


		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Prensa</h3>
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>

								<li class="m-nav__separator">-</li>

								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text">Agregar Prensa</span>

									</a>

								</li>

							</ul>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">

							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link <?php if($actualTab =='general-tab') echo 'active';?>" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Información general

												</a>

											</li>

										</ul>

									</div>

								</div>


								<div class="tab-content">

									<div class="tab-pane <?php if($actualTab =='general-tab') echo 'active';?>" id="general-tab">

										<form id="add-prensa-data-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>

												</div>

											</div>


											<!-- Title -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Título:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="title" type="text" class="form-control m-input" name="title" placeholder="Título" value="<?php echo $prensaTitle;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $prensaTitle;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Title -->

											<!-- Newspaper -->

											<div class="form-group m-form__group row pt-4">

                        <label class="col-form-label col-lg-3 col-12">Diario</label>

                        <div class="col-lg-4 col-12 input-group">

													<select id="id_newspaper" name="id_newspaper" class="browser-default custom-select required">

														<option value="" <?php echo ($id_newspaper_actual==0) ? "selected" : ""; ?>>Seleccionar</option>

														<?php

															foreach($newspapersListArray as $id_newspaper => $data){

																	$id_newspaper = $id_newspaper +1;

																	?>

																	<option value="<?php echo $id_newspaper; ?>" <?php echo ($id_newspaper==$id_newspaper_actual) ? 'selected' : ''; ?>><?php echo $data["name"]; ?></option>

																	<?php

															}

														?>

													</select>

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $prensaTitle;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Newspaper -->


											<!-- Date -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Fecha de publicación

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Fecha de publicación del diario">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-12">

													<div class="input-group date">
														<input type="text" class="form-control" id="date" name="date" value="<?php if($editMode) echo $prensaDate;?>">
														<div class="input-group-addon">
															<i class="pt-2 far fa-calendar-alt"></i>
														</div>
													</div>

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $prensaDate;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /. Date -->

											<!-- Tags / Keys -->
											<div id="key-list" class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Keywords:
													<br><small>Al agregar, se listan debajo<br>

														<small data-toggle="tooltip" data-placement="bottom" title="Escribir la palabra clave relacionada a la notica y apretar el botón agregar para listarla debajo.">
															¿Cómo lo hago?
														</small>


													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input type="text" id="keyword" name="keyword" class="typeahead form-control m-input" placeholder="Keyword / tag" value="">

													<input type="hidden" id="hidden-key-id" name="hidden-key-id" >

													<!--<span class="m-form__help"><b>ejemplos:</b> vocal, guía, investigación, museología, etc.</span>-->

												</div>

												<?php

												if(!$editMode){

													?>

													<div class="input-group-append">

														<button id="add-keyword" class="btn btn-default" name="add-keyword" type="button">Agregar</button>

													</div>

													<?php

												}else{

													?>

													<div class="input-group-append">

														<button id="add-keyword-direct" class="btn btn-default " type="button">Agregar</button>

													</div>

													<?php

												}

												?>


											</div>
											<!-- /. Tags / Keys -->

											<!-- Tags list dynamically -->
											<div class="form-group m-form__group row pt-4">

												<label class="control-label col-md-3"></label>

												<div class="col-md-9">

													<div id="" class="tagsinput" style="width: auto; height: 100px;">

														<!-- If we are in editMode, load actual tags for this item -->
														<?php

															if($editMode){


																//LEVANTAR TODOS LOS TAGS DE UN DIARIO

																//Para que se muestre la relación, tiene que estar delted = 0 la kewyord y la relacion keyword/diario

																$getTagsQuery = DB::getInstance()->query("SELECT ampk.name, ampk.deleted, ampkr.id as id_relation, ampkr.id_prensa, ampkr.id_keyword, ampkr.deleted FROM app_museum_prensa_keywords as ampk, app_museum_prensa_keywords_relations as ampkr WHERE ampkr.id_keyword = ampk.id AND ampkr.id_prensa = ? AND ampk.deleted = ? AND ampkr.deleted = ?", array($actualId, 0, 0));

																$tagsList = $getTagsQuery->results(true);

																/*
																 echo "<pre>";
																 print_r($tagsList);
																 echo "</pre>";*/

																 //Para borrar el keyword de un diario, se borra la relación. El keyword debe estar disponible para otras relaciones

																foreach($tagsList as $idTag => $dataTag){

																?>

																	<span id="<?php echo $dataTag["id_relation"]; ?>" class="tag deleteTag"><span><span class="tag-value"><?php echo $dataTag["name"]; ?></span>&nbsp;&nbsp;</span><a href="#" title="Eliminar Keyword">x</a></span>

																<?php

																}

															}

														?>

													</div>

												</div>

											</div>
											<!-- /.Tags list dynamically -->

											<!-- Image -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Imagen de diario*:
													<br><small>Es la imagen del diario y tiene que ser de 1080 x 1920 px solamente.<br></small>

													<?php if($editMode){ ?>
														<br />
														<small data-toggle="tooltip" data-placement="bottom" title="Una vez seleccionada la imagen, apretar en 'subir archivo' para guardarla correctamente">
															Como actualizar la imagen?
														</small>

													<?php }else{

														?>

														<small data-toggle="tooltip" data-placement="bottom" title="Porque al partir de una imagen grande, nuestros servidores pueden resamplearla a diversas resoluciones sin pérdida de calidad. El tamaño máximo es de 20 MB.">
															¿por qué este tamaño?
														</small>

														<?php
													}

													?>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input id="picture" name="picture" type="file">

												</div>

											</div>
											<!-- /.Image -->


											<!-- ... -->

											<div class="m-portlet__foot m-portlet__foot--fit">

												<div class="m-form__actions">

													<div class="row">

														<div class="col-2"></div>

														<div class="col-7">

															<input type="hidden" name="token" value="<?php echo Token::generate()?>">

															<?php

															$hiddenClass = ($editMode) ? 'd-none' : '';

															?>

															<button id="add_prensa" type="submit"

															class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar Nota</button>
															&nbsp;&nbsp;

															<?php

															$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

															$buttonId =

															(!$editMode) ? 'exit_from_form' : 'back_to_list';

															?>

															<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																<?php echo $buttonLabel;?>

															</button>

														</div>

													</div>

												</div>

											</div>

										</form>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="assets/app/js/museum/app_prensa/museum-app-prensa-add.js" type="text/javascript"></script>

</body>

</html>
