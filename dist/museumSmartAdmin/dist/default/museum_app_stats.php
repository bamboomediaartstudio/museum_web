<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

setlocale(LC_ALL, "es_ES", 'Spanish_Spain', 'Spanish');


$query = DB::getInstance()->query(

	"SELECT *, count(interactions.id) as interactions_value, apps.id as main_id FROM 

	app_stats as apps LEFT JOIN app_stats_levels as lv ON apps.app_level = lv.level_id 

	LEFT JOIN app_stats_interactions as interactions ON apps.id = interactions.id_app

	WHERE apps.active = ? AND apps.deleted = ? AND apps.id = ? GROUP by apps.id ORDER BY apps.id", 

	array(1, 0, Input::get('id')));


$actualApp = $query->first();

$now = new DateTime();

$endTime = new DateTime('20:00');

$openTime = new DateTime('08:00');

if( (date('D') != 'Sat' && date('D') != 'Sun') &&  ($now >= $openTime && $now <= $endTime))  { 
	
	$nowClass = 'btn-success';

	$nowStatus = 'Ahora online';

}else{

	$nowStatus = 'Ahora offline';

	$nowClass = 'btn-danger';

}

$appId = $actualApp->main_id;

if($actualApp->is_editable == 1){ $editableLabel = 'editable'; }else{ $editableLabel = ''; }

//...

$secondQuery = DB::getInstance()->query(

	"SELECT * FROM app_stats_versions as versions  WHERE versions.id_app = ? ORDER BY versions.id DESC", 

	array(Input::get('id')));

$versionsList = $secondQuery->results();

$versions = '';

$altArray = array('m-timeline-3__item--info', 'm-timeline-3__item--success', 'm-timeline-3__item--warning');

$count = 0;

//---------------------------------------------------------------------------------------------------------------
//interactions hoy...

$todayInteractions =  DB::getInstance()->query("SELECT COUNT(*) as today FROM app_stats as stats INNER JOIN app_stats_interactions as interactions  ON stats.id = interactions.id_app WHERE interactions.date = CURDATE() AND stats.id = ?", [Input::get('id')])->first()->today;

$yesterdayInteractions =  DB::getInstance()->query("SELECT COUNT(*) as today FROM app_stats as stats INNER JOIN app_stats_interactions as interactions  ON stats.id = interactions.id_app WHERE interactions.date = CURDATE() - 1 AND stats.id = ?", [Input::get('id')])->first()->today;

$todayInteractionsPercent = 100;

$todayTooltip = 'Contempla el total de interacciones de todas las pantallas durante todo el día de hoy.';

$dayTooltip = '';

$diff;

$diffString;

if($yesterdayInteractions > $todayInteractions){

	$todayInteractionsPercent = round(($todayInteractions * 100) / $yesterdayInteractions, 0);

	$diff = ($yesterdayInteractions - $todayInteractions);

	$todayInteractionsQuote = $diff . ' interacciones menos que ayer'; 

	$diffString = $diff . ' menos respecto a ayer';

}else if($yesterdayInteractions == $todayInteractions){

	$todayInteractionsQuote = $todayInteractions . ' interacciones: misma cantidad de interacciones que ayer'; 

	$diffString = 'respecto a ayer';

}else{

	$diff = ($todayInteractions - $yesterdayInteractions);

	$todayInteractionsQuote = $diff . ' interaccion/es más que ayer!'; 

	$diffString = $diff . ' más respecto a ayer';
}

//---------------------------------------------------------------------------------------------------------------
//end interactions hoy...

//---------------------------------------------------------------------------------------------------------------
//interactions week...

$weekInteractions =  DB::getInstance()->query("SELECT COUNT(*) as week FROM app_stats as stats INNER JOIN app_stats_interactions as interactions ON stats.id = interactions.id_app 

	WHERE  YEARWEEK(interactions.date, 1) = YEARWEEK(CURDATE(), 1) AND stats.id = ?", [Input::get('id')])->first()->week;

$lastWeekInteractions =  DB::getInstance()->query("SELECT COUNT(*) as week FROM app_stats as stats INNER JOIN app_stats_interactions as interactions ON stats.id = interactions.id_app 

	WHERE YEARWEEK(interactions.date, 1) = YEARWEEK(CURDATE(), 1) - 1 AND stats.id = ?", [Input::get('id')])->first()->week;

$weekInteractionsPercent = 100;

$diffWeekStreet;

if($lastWeekInteractions > $weekInteractions ){

	$weekInteractionsPercent = round(($weekInteractions * 100) / $lastWeekInteractions , 0);

	$diff = ( $lastWeekInteractions - $weekInteractions);

	$weekInteractionsQuote = $diff . ' interacciones menos que la semana pasada'; 

	$diffWeekStreet = '(' . $diff . ' menos respecto a la semana pasada)';

}else if($weekInteractions == $lastWeekInteractions){

	$weekInteractionsQuote = $lastWeekInteractions . ' interacciones: misma cantidad de interacciones que la semana pasada'; 

	$diffWeekStreet = '(respecto a ayer)';

}else{

	$diff = ($weekInteractions - $lastWeekInteractions);

	$weekInteractionsQuote = $diff . ' interacciones más que la semana pasada!'; 

	$diffWeekStreet = '(' . $diff . ' más respecto a ayer)';
}

$weekTooltip = "Contempla el total de interacciones desde el comienzo de la semana hasta el día de hoy. Se contabiliza de lunes a viernes.";

//---------------------------------------------------------------------------------------------------------------
//interactions month...

$monthTooltip = "Contempla el total de interacciones desde el primer día del mes al último.";

$monthInteractions =  DB::getInstance()->query("SELECT COUNT(*) as today FROM app_stats as stats INNER JOIN app_stats_interactions as interactions  ON stats.id = interactions.id_app  WHERE (interactions.date between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() ) AND stats.id = ? ", [Input::get('id')])->first()->today;

//---------------------------------------------------------------------------------------------------------------
//interactions year...

$yearInteractions =  DB::getInstance()->query("SELECT COUNT(*) as year FROM app_stats as stats INNER JOIN app_stats_interactions as interactions ON stats.id = interactions.id_app WHERE YEAR (interactions.date) = YEAR(CURDATE()) AND stats.id = ?", [Input::get('id')])->first()->year;


//---------------------------------------------------------------------------------------------------------------
//historic

$historicInteractions =  DB::getInstance()->query("SELECT COUNT(*) as year FROM app_stats as stats INNER JOIN app_stats_interactions as interactions ON stats.id = interactions.id_app WHERE stats.id = ?", [Input::get('id')])->first()->year;

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Estadísticas | Pantallas Touch</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() { sessionStorage.fonts = true; }

		});


		
	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<input type="hidden" name="user-id" id="user-id" value="<?php echo $actualUser;?>">

	<input type="hidden" name="app-id" id="app-id" value="<?php echo Input::get('id');?>">

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-content">

					<div class="row">

						<div class="col-xl-4 element-item '<?php echo $editableLabel;?>">

							<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">

								<div class="m-portlet__head m-portlet__head--fit">

									<div class="m-portlet__head-caption">

										<div class="m-portlet__head-action">

											<?php if($actualApp->is_editable){ ?>



											<?php } ?>							

										</div>

									</div>

								</div>

								<div class="m-portlet__body">

									<div class="m-widget19">

										<div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides" style="min-height-: 286px">

											<img src="private/sources/images/screens/<?php echo $appId . '.jpg';?>"  alt="">

											<h3 class="m-widget19__title m--font-light"> <?php echo  strtoupper($actualApp->name);?></h3>

											<div class="m-widget19__shadow"></div>

										</div>

										<div class="m-widget19__content">

											<div class="m-widget19__header">

												<span class="m-widget21__icon">

													<a href="#" class="btn <?php echo $nowClass;?> m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="bottom" title="Las estadísticas se generan en los días y horarios indicados: de lunes a viernes, de 08:00 a 20:00">

														<i class="fas fa-bullseye"></i>

													</a>

												</span>

												<div class="m-widget19__info">

													<span class="m-widget19__time"> <?php echo $nowStatus;?> </span>

												</span> <br>

											</div>

											<div class="m-widget19__stats">

												<span class="m-widget19__number m--font-brand"> <?php echo  $actualApp->interactions_value;?> </span>

												<span class="m-widget19__comment">Interacciones

												</span>

											</div>

										</div>

										<div class="m-widget19__body"><?php echo $actualApp->description;?>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>				

				<div class="col-xl-4 col-lg-12">

					<div class="m-portlet m-portlet--full-height">
						
						<div class="m-portlet__head">
							
							<div class="m-portlet__head-caption">
								
								<div class="m-portlet__head-title">
									
									<h3 class="m-portlet__head-text"> Versionado de la app </h3>

								</div>

							</div>

						</div>
						
						<div class="m-portlet__body">
							
							<div class="tab-content">
								
								<div class="tab-pane active" id="m_widget2_tab1_content">

									<div class="m-timeline-3">
										
										<div class="m-timeline-3__items">

											<?php


											foreach($versionsList as $v){

												if($count % count($altArray) == 0){ $count = 0; }

												$color = $altArray[$count];

												$count+=1;

												?>

												<div class="m-timeline-3__item check-color <?php echo $color;?>">

													<span class="m-timeline-3__item-time" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="<?php echo $v->app_date;?>"><?php echo $v->version;?>

												</span>

												<div class="m-timeline-3__item-desc">

													<span class="m-timeline-3__item-text"><?php echo $v->description;?></span>

													<br>

													<span class="m-timeline-3__item-user-name">

														<a href="https://www.aditivointeractivegroup.com" target="_blank" class="m-link m-link--metal m-timeline-3__item-link">
															Aditivo Interactive Group S.A 
														</a>

													</span>	
													
												</div>

											</div>

										<?php } ?>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

			<div class="col-xl-4 col-lg-12">

				<div class="m-portlet m-portlet--full-height">

					<div class="m-portlet__head">

						<div class="m-portlet__head-caption">

							<div class="m-portlet__head-title">

								<h3 class="m-portlet__head-text"> Generales </h3>

							</div>

						</div>

					</div>

					<div class="tab-content">

						<div class="m-widget1">

							<div class="m-widget1__item">
								
								<div class="row m-row--no-padding align-items-center">
									
									<div class="col">
										
										<h3 class="m-widget1__title"> horario de encendido: </h3>
										
										<span class="m-widget1__desc"> (de lunes a viernes) </span>

									</div>
									
									<div class="col m--align-right">
										
										<span class="m-widget1__number m--font-brand">
											
											<?php echo $actualApp->power_on;?>

										</span>

									</div>

								</div>

							</div>
							
							<div class="m-widget1__item">
								
								<div class="row m-row--no-padding align-items-center">
									
									<div class="col">
										
										<h3 class="m-widget1__title"> Horario de apagado: </h3>
										
										<span class="m-widget1__desc"> (de lunes a viernes) </span>

									</div>
									
									<div class="col m--align-right">
										
										<span class="m-widget1__number m--font-danger">
											
											<?php echo $actualApp->power_off;?>

										</span>

									</div>

								</div>

							</div>

							<div class="m-widget1__item">
								
								<div class="row m-row--no-padding align-items-center">
									
									<div class="col">
										
										<h3 class="m-widget1__title"> Horario de screensaver: </h3>
										
										<span class="m-widget1__desc"> (de lunes a viernes) </span>

									</div>
									
									<div class="col m--align-right">
										
										<span class="m-widget1__number m--font-success">
											
											<?php echo $actualApp->power_off;?>

										</span>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<div class="row">

			<div class="col-xl-4">

				<div class="m-portlet m-portlet--full-height m-portlet--fit ">

					<div class="m-portlet__head">

						<div class="m-portlet__head-caption">

							<div class="m-portlet__head-title">

								<h3 class="m-portlet__head-text">Sobre el equipo </h3>

							</div>

						</div>

					</div>

					<div class="m-portlet__body">

						<div class="m-widget4 m-widget4--chart-bottom" style="min-height: 350px">

							<div class="m-widget4__item">

								<div class="m-widget4__ext"> <i class="flaticon-interface-3"></i>

								</div>

								<div class="m-widget4__info"> <span class="m-widget4__text">Memoria</span> </div>

								<div class="m-widget4__ext">

									<span class="m-widget4__number m--font-accent"> <?php echo preg_replace('/\s+/','',$actualApp->memory);?> </span>

								</div>

							</div>

							<div class="m-widget4__item">

								<div class="m-widget4__ext"> <i class="flaticon-network"></i>

								</div>

								<div class="m-widget4__info">

									<span class="m-widget4__text"> IP: </span>

								</div>

								<div class="m-widget4__ext">

									<span class="m-widget4__stats m--font-info">

										<span class="m-widget4__number m--font-accent">

											<?php echo preg_replace('/\s+/','',$actualApp->ip);?> 

										</span>

									</span>

								</div>

							</div>

							<div class="m-widget4__item">

								<div class="m-widget4__ext"> <i class="flaticon-laptop"></i>

								</div>

								<div class="m-widget4__info">

									<span class="m-widget4__text"> Sistema Operativo: </span>

								</div>

								<div class="m-widget4__ext">

									<span class="m-widget4__stats m--font-info">

										<span class="m-widget4__number m--font-accent">

											<?php echo preg_replace('/\s+/','',$actualApp->so);?> 

										</span>

									</span>

								</div>

							</div>

							<div class="m-widget4__item">

								<div class="m-widget4__ext"> <i class="flaticon-imac"></i>

								</div>

								<div class="m-widget4__info">

									<span class="m-widget4__text"> Resolución: </span>

								</div>

								<div class="m-widget4__ext">

									<span class="m-widget4__stats m--font-info">

										<span class="m-widget4__number m--font-accent">

											<?php echo preg_replace('/\s+/','',$actualApp->monitor);?> 

										</span>

									</span>

								</div>

							</div>

							<div class="m-widget4__item">

								<div class="m-widget4__ext"> <i class="flaticon-stopwatch"></i> </div>

								<div class="m-widget4__info">

									<span class="m-widget4__text"> Procesador: </span>

								</div>

								<div class="m-widget4__ext">

									<span class="m-widget4__stats m--font-info">

										<span class="m-widget4__number m--font-accent">

											<?php echo preg_replace('/\s+/','',$actualApp->pc);?> 

										</span>

									</span>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

			<div class="col-xl-8">

				<div class="m-portlet m-portlet--full-height m-portlet--fit ">

					<div class="m-portlet__head">

						<div class="m-portlet__head-caption">

							<div class="m-portlet__head-title">

								<h3 class="m-portlet__head-text">Estadísticas</h3>

							</div>

						</div>

					</div>

					<div class="m-portlet__body">

						<div class="row m-row--no-padding m-row--col-separator-xl">

							<div class="col-md-12 col-lg-6 col-xl-6">

								<div class="m-widget24">

									<div class="m-widget24__item">

										<h4 class="m-widget24__title">hoy: </h4>

										<br>

										<span class="m-widget24__desc"><?php echo date("d/m/Y");?></span>

										<span class="m-widget24__stats m--font-brand" data-skin="dark" data-toggle="m-tooltip" 

										data-placement="right" title="<?php echo $todayTooltip;?>" 

										data-original-title="Contempla el total de interacciones de todas las pantallas durante todo el día de hoy."><?php echo $todayInteractions;?> </span>

										<div class="m--space-10"></div>

										<div class="progress m-progress--sm">

											<div class="progress-bar m--bg-brand" role="progressbar" style="width: <?php echo $todayInteractionsPercent;?>%;" data-skin="dark" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="<?php echo $todayInteractionsQuote;?>"></div>
										</div>

										<span class="m-widget24__change"><?php echo $diffString;?></span>

										<span class="m-widget24__number"><?php echo $todayInteractionsPercent;?>%</span>

									</div>

								</div>

							</div>

							<div class="col-md-12 col-lg-6 col-xl-6">

								<div class="m-widget24">

									<div class="m-widget24__item">

										<h4 class="m-widget24__title">Semana:</h4>
										<br>
										<span class="m-widget24__desc">de lunes a viernes</span>

										<span class="m-widget24__stats m--font-info" data-skin="dark" data-toggle="m-tooltip" 

										data-placement="right" title="" 

										data-original-title="<?php echo $weekTooltip;?>"><?php echo $weekInteractions;?></span>

										<div class="m--space-10"></div>

										<div class="progress m-progress--sm">

											<div class="progress-bar m--bg-info" role="progressbar" style="width: <?php echo $weekInteractionsPercent;?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" data-skin="dark" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="<?php echo $weekInteractionsQuote;?>"></div>

										</div>

										<span class="m-widget24__change">respecto a la semana pasada</span>

										<span class="m-widget24__number"><?php echo $weekInteractionsPercent;?>%</span>

									</div>

								</div>

							</div>

							<div class="col-md-12 col-lg-6 col-xl-6">

								<div class="m-widget24">

									<div class="m-widget24__item">

										<h4 class="m-widget24__title">Mes:</h4>

										<br>

										<span class="m-widget24__desc">durante <?php echo strftime("%B");?></span>

										<span class="m-widget24__stats m--font-danger" data-skin="dark" data-toggle="m-tooltip" 

										data-placement="right" title="" 

										data-original-title="<?php echo $monthTooltip;?>"><?php echo $monthInteractions;?></span>

										<div class="m--space-10"></div>

										<div class="progress m-progress--sm">
											<div class="progress-bar m--bg-danger" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
										<span class="m-widget24__change">Desde el primer día del mes a la fecha</span>

										<span class="m-widget24__number">100%</span>

									</div>

								</div>

							</div>

							<div class="col-md-12 col-lg-6 col-xl-6">

								<div class="m-widget24">

									<div class="m-widget24__item">

										<h4 class="m-widget24__title">año:</h4>

										<br>

										<span class="m-widget24__desc">durante el año <?php echo strftime("%Y");?></span>

										<span class="m-widget24__stats m--font-success" data-skin="dark" data-toggle="m-tooltip" 

										data-placement="right" title="" 

										data-original-title="<?php echo $monthTooltip;?>"><?php echo $yearInteractions;?></span>

										<div class="m--space-10"></div>

										<div class="progress m-progress--sm">
											<div class="progress-bar m--bg-success" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
										<span class="m-widget24__change">Desde el primer día del año a la fecha</span>

										<span class="m-widget24__number">100%</span>

									</div>

								</div>

							</div>

							<div class="col-md-12 col-lg-6 col-xl-12">

								<div class="m-widget24">

									<div class="m-widget24__item">

										<h4 class="m-widget24__title">histórico:</h4>

										<br>

										<span class="m-widget24__stats m--font-warning" data-skin="dark" data-toggle="m-tooltip" 

										data-placement="right" title="" 

										data-original-title="<?php echo $monthTooltip;?>"><?php echo $historicInteractions;?></span>

										<div class="m--space-10"></div>

										<div class="progress m-progress--sm">
											<div class="progress-bar m--bg-warning" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
										<span class="m-widget24__change">Desde el inicio de los tiempos	</span>

										<span class="m-widget24__number">100%</span>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<div class="d-flex align-items-center">

			<div class="mr-auto">

				<h3 class="m-subheader__title">GRÁFICOS Y ESTADÍSTICAS</h3><br>

			</div>

		</div>

		<div class = "row">

			<div class="col-xl-6">

				<div class="">

					<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">

						<div class="m-portlet__head">

							<div class="m-portlet__head-caption">

								<div class="m-portlet__head-title">

									<h3 class="m-portlet__head-text">

										Estadística histórica promedio de usuarios por hora:

									</h3>

								</div>

							</div>

						</div>

						<div class="m-portlet__body">

							<div class="m-widget15">

								<div class="m-widget15__chart" style="height:180px;">

									<canvas  id="chart_activity_by_hour_today"></canvas>

								</div>

								<div class="m-widget15__desc">
									* Se toman en cuenta todos los usuarios desde el inicio del museo.
								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

			<div class="col-xl-6">

				<div class="">

					<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">

						<div class="m-portlet__head">

							<div class="m-portlet__head-caption">

								<div class="m-portlet__head-title">

									<h3 class="m-portlet__head-text">

										Estadística histórica promedio de usuarios por día

									</h3>

								</div>

							</div>

						</div>

						<div class="m-portlet__body">

							<div class="m-widget15">

								<div class="m-widget15__chart" style="height:180px;">

									<canvas  id="chart_activity_by_day_in_week"></canvas>

								</div>

								<div class="m-widget15__desc">

									* Se toman en cuenta todos los usuarios desde el inicio del museo.

								</div>

							</div>

						</div>
					</div>

				</div>

			</div>

		</div>

		<div class="row">

			<div class="col-xl-6">

				<div class="m-widget14 m-portlet m-portlet--bordered-semi m-portlet--full-height">

					<div class="m-widget14__header m--margin-bottom-30">

						<h3 class="m-widget14__title"> Interacciones por día en el mes de <?php echo strftime("%B");?> </h3>

						<span class="m-widget14__desc">Comparado con máximos históricos</span>

					</div>

					<div class="m-widget14__chart" style="height:120px;">

						<canvas  id="chart_average_by_days_in_month"></canvas>

					</div>

				</div>

			</div>

			<div class="col-xl-6">

				<div class="m-widget14 m-widget14 m-portlet m-portlet--bordered-semi m-portlet--full-height">

					<div class="m-widget14__header m--margin-bottom-30">

						<h3 class="m-widget14__title">

							Interacciones por mes para <?php echo strftime("%Y");?>

						</h3>

						<span class="m-widget14__desc">

							de enero a diciembre

						</span>

					</div>

					<div class="m-widget14__chart" style="height:120px;">
						<canvas  id="chart_activity_by_month_in_year"></canvas>

					</div>

				</div>

			</div>

		</div>

		<div class = "row">

			<div class="col-xl-3">

				<div class="m-widget14 m-widget14 m-portlet m-portlet--bordered-semi m-portlet--full-height">

					<div class="m-widget14__header">

						<h3 class="m-widget14__title">Uso según hora del día </h3>

						<span class="m-widget14__desc">de 08:00 a 20:00</span>

					</div>

					<div class="row align-items-center">

						<div class="col">

							<div id="chart_stats_by_level_3" class="m-widget14__chart1"></div>

						</div>

					</div>
					
				</div>

			</div>

			<div class="col-xl-3">

				<div class="m-widget14 m-widget14 m-portlet m-portlet--bordered-semi m-portlet--full-height">

					<div class="m-widget14__header">

						<h3 class="m-widget14__title">Uso según el día </h3>

						<span class="m-widget14__desc">De lunes a viernes, históricos.</span>

					</div>

					<div class="row align-items-center">

						<div class="col">

							<div id="chart_stats_by_level" class="m-widget14__chart1"></div>

						</div>

					</div>
					
				</div>

			</div>

			<div class="col-xl-3">

				<div class="m-widget14 m-widget14 m-portlet m-portlet--bordered-semi m-portlet--full-height">

					<div class="m-widget14__header">

						<h3 class="m-widget14__title">Uso según el mes </h3>

						<span class="m-widget14__desc">De enero a diciembre.</span>

					</div>

					<div class="row align-items-center">

						<div class="col">

							<div id="chart_stats_by_level_2" class="m-widget14__chart1"></div>

						</div>

					</div>
					
				</div>

			</div>

		</div>

	</div>

</div>

</div>

<?php require_once 'private/includes/footer.php'; ?>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i> </div>

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="assets/vendors/custom/jquery-ui/jquery-ui.bundle.js" type="text/javascript"></script>

<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>

<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/museum/stats/museum-app-stats.js" type="text/javascript"></script>

</body>

</html>
