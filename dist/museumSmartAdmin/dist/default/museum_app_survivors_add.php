<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$actualId = Input::get('id');

$actualTab = 'general-tab';

if(Input::exists('get') && Input::get('tab')){

	$actualTab = Input::get('tab');

	//echo 'existe';

}

//echo $actualTab;

//return;

$editMode = false;

if(Input::exists("get") && Input::get('id')){

	$editMode = true;

	$mainQuery = DB::getInstance()->query('SELECT * FROM  app_museum_survivors

		WHERE id=? AND deleted = ?', [Input::get('id'), 0]);

	$actualSurvivor 				= $mainQuery->first();

	$survivorName 					= $actualSurvivor->name;

	$survivorSurname 				= $actualSurvivor->surname;

	$survivorNick 					= $actualSurvivor->nick_name;

	$survivorBirthDate 			= $actualSurvivor->birth_date;

	$survivorIdBirthCountry	= $actualSurvivor->id_birth_country;

	$survivorZl 						= ($actualSurvivor->zl) ? 'checked' : '';

	$survivorPicture 				= $actualSurvivor->picture;

	$survivorBio 						= $actualSurvivor->bio;


	//Country query from id_birth_country - get Country name_es
	if($survivorIdBirthCountry != 0){
		$countryQuery 					= DB::getInstance()->query('SELECT id, name_es FROM world_location_countries WHERE id = ?', [$survivorIdBirthCountry]);


		$actualSurvivorCountry 	= $countryQuery->first();

		$survivorCountry 				= $actualSurvivorCountry->name_es;
	}else{

		$survivorCountry = "Seleccione un país";

	}



}else{

	$survivorName 					= '';

	$survivorSurname 				= '';

	$survivorNick 					= '';

	$survivorBirthDate 			= '';

	$survivorIdBirthCountry	= '';

	$survivorZl 						= '';

	$survivorPicture 				= '';

	$survivorBio 						= '';


}

//museum data...

$museumData = DB::getInstance();

$museumDataQ = $museumData->query('SELECT * FROM museum_data');

if($museumDataQ->count()>0) $museumDataObject = $museumDataQ->first();

//museum social media data...

$museumSocialMediaData = DB::getInstance();

$museumSocialMediaDataQ = $museumSocialMediaData->query('SELECT * FROM museum_social_media');

if($museumSocialMediaDataQ->count()>0) $museumSocialMediaObject = $museumSocialMediaDataQ->first();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Sobrevivientes | Agregar Sobreviviente</title>

	<meta name="description" content="add new Survivor">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{

		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}


</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">


		<input type="hidden" id="survivor-id" name="survivor-id" value="<?php echo $actualId;?>">


		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Sobrevivientes</h3>
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>

								<li class="m-nav__separator">-</li>

								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text">Agregar Sobreviviente</span>

									</a>

								</li>

							</ul>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">

							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link <?php if($actualTab =='general-tab') echo 'active';?>" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Información general

												</a>

											</li>

											<!--
											<li class="nav-item m-tabs__item <?php if(!$editMode) echo 'disabled'; ?>">
												<a class="<?php if($actualTab =='images-tab') echo 'active';?> tab-navigation images-tab nav-link m-tabs__link" data-toggle="tab" href="#images-tab" role="tab">

													Imágenes

												</a>

											</li>

											<li class="nav-item m-tabs__item <?php if(!$editMode) echo 'disabled'; ?>">
												<a class="<?php if($actualTab =='videos-tab') echo 'active';?> tab-navigation videos-tab nav-link m-tabs__link" data-toggle="tab" href="#videos-tab" role="tab">

													Videos

												</a>

											</li>-->

										</ul>

									</div>

								</div>


								<div class="tab-content">
									<!--
									<div class="tab-pane <?php if($actualTab =='videos-tab') echo 'active';?>" id="videos-tab">

										<div class="mt-5 mb-5 col-lg-12">

											<form id="youtube-form">

												<div class="form-group">


													<div class="form-group m-form__group row">

														<label class="col-form-label col-lg-3 col-sm-12">Link al video*:</label>

														<div class="col-lg-4 col-md-9 col-sm-12">

															<input id="youtube_id" type="text" class="form-control m-input" name="youtube_id" placeholder="URL del video" value="">

															<small class="yt-info m-form__help">Copiá y pegá acá la URL del video de <b>YouTube.</b></small>

															<div id="previews">

																<div id="preview-1"></div>

																<div id="preview-2"></div>

																<div id="preview-3"></div>


															</div>

															<input type="hidden" id="hidden-yt-id" name="hidden-yt-id" value="">

														</div>



														<div class="">

															<button data-db-value="" data-id="1" class="add-video-btn btn btn-default" type="submit">agregar

															</button>

														</div>


													</div>

												</div>

											</form>

											<input type="file" id="videos-batch" name="videos-batch[]" accept="image/*" multiple>

										</div>
									</div>-->
									<!--
									<div class="tab-pane <?php if($actualTab =='images-tab') echo 'active';?>" id="images-tab">

										<div class="mt-5 mb-5 col-lg-12">

											<input type="file" id="images-batch" name="images-batch[]" accept="image/*" multiple>

										</div>
									</div>-->

									<!-- general tab -->

									<div class="tab-pane <?php if($actualTab =='general-tab') echo 'active';?>" id="general-tab">

										<form id="add-survivor-data-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>

												</div>

											</div>


											<!-- Name -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Nombre:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="name" type="text" class="form-control m-input" name="name" placeholder="Nombre" value="<?php echo $survivorName;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $survivorName;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Name -->

											<!-- Surname -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Apellido</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="surname" type="text" class="form-control m-input" name="surname" placeholder="Apellido" value="<?php echo $survivorSurname; ?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $survivorSurname; ?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Surname -->

											<!-- Nickname -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Apodo</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="nick_name" type="text" class="form-control m-input" name="nick_name" placeholder="Apodo" value="<?php echo $survivorNick; ?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $survivorNick;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /.Nickname -->

											<!-- Birth Date -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Fecha de nacimiento

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Fecha de nacimiento">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-12">

													<div class="input-group date">
														<input type="text" class="form-control" id="birth_date" name="birth_date" value="<?php if($editMode) echo $survivorBirthDate; ?>">
														<div class="input-group-addon">
															<i class="pt-2 far fa-calendar-alt"></i>
														</div>
													</div>

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $survivorBirthDate;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											<!-- /. Birth Date -->

											<!-- Birth Place -->
											<div id="birth-place-list" class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Lugar de nacimiento</label>

												<div class="col-lg-4 col-12 input-group">

													<input type="text" id="birth_place" name="birth_place" class="typeahead form-control m-input" placeholder="Lugar de nacimiento" value="<?php echo ($editMode) ? $survivorCountry : '';?>">

													<input type="hidden" id="hidden-birth-place-id" name="hidden-birth-place-id" value="<?php echo $survivorIdBirthCountry; ?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="countryFlag" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<!-- /.Birth Place -->

											<!-- Zl -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Z"L

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Este sobreviviente aún está vivo?">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="zl" type="checkbox" <?php echo $survivorZl;?> class="highlighted toggler-info" name="zl" data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>
											<!-- /.Zl -->

											<!-- Description -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Biografía:</label>

												<div class="col-lg-6 col-md-9 col-sm-12">

													<textarea numlines="20" type="text" class="form-control m-input description" id="bio" name="bio" placeholder="Biografía"><?php echo trim($survivorBio); ?></textarea>

													<?php if($editMode){  ?>

														<br>

														<div class="">

															<button data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>

												</div>

											</div>
											<!-- /.Description -->


											<!-- Picture -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Foto del sobreviviente*:
													<br><small>Es la imagen de perfil del sobreviviente y tiene que ser de 300 x 500 px.<br>

														<small data-toggle="tooltip" data-placement="bottom" title="Porque al partir de una imagen grande, nuestros servidores pueden crear otras versiones a diversas resoluciones sin pérdida de calidad. El tamaño máximo es de 20 MB.">
															¿por qué tan grande?
														</small>


													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input id="picture" name="picture" type="file">

												</div>

											</div>
											<!-- /.Picture -->


											<!-- ... -->

											<div class="m-portlet__foot m-portlet__foot--fit">

												<div class="m-form__actions">

													<div class="row">

														<div class="col-2"></div>

														<div class="col-7">

															<input type="hidden" name="token" value="<?php echo Token::generate()?>">

															<?php

															$hiddenClass = ($editMode) ? 'd-none' : '';

															?>

															<button id="add_survivor" type="submit"

															class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar Sobreviviente</button>
															&nbsp;&nbsp;

															<?php

															$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

															$buttonId =

															(!$editMode) ? 'exit_from_form' : 'back_to_list';

															?>

															<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																<?php echo $buttonLabel;?>

															</button>

														</div>

													</div>

												</div>

											</div>

										</form>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="assets/app/js/museum/app_survivors/museum-app-survivors-add.js" type="text/javascript"></script>

</body>

</html>
