<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$actualId = Input::get('id');

$actualTab = 'general-tab';

if(Input::exists('get') && Input::get('tab')){ $actualTab = Input::get('tab'); }

$actualId = Input::get('id');

//alternative names...

$alternativeNamesQuery = DB::getInstance()->query('SELECT * FROM app_museum_survivors_project_alternative_name WHERE sid =? AND active = ? AND deleted = ?', [$actualId, 1, 0]);

$alternativeNames = $alternativeNamesQuery->results();

//countries...

$countriesQuery = DB::getInstance()->query('SELECT * FROM world_location_countries');

$countries = $countriesQuery->results();

//rescuers... 

$rescuersQuery = DB::getInstance()->query('SELECT * FROM app_museum_survivors_project_rescuers WHERE active = ? AND deleted = ?', [1, 0]);

$rescuers = $rescuersQuery->results();



//relations...

$relationsQuery = DB::getInstance()->query('SELECT * FROM app_museum_survivors_projects_family_relations');

$relations = $relationsQuery->results();

//books...

$booksQuery = DB::getInstance()->query('SELECT * FROM app_museum_survivors_project_books');

$books = $booksQuery->results();



//Films

$filmsQuery = DB::getInstance()->query('SELECT * FROM app_museum_survivors_project_films');

$films = $filmsQuery->results();

//Honors

$honorsQuery = DB::getInstance()->query('SELECT * FROM app_museum_survivors_project_honors');

$honors = $honorsQuery->results();



//guetos list

$guetosQuery = DB::getInstance()->query('SELECT * FROM app_museum_guetos WHERE active = ? AND deleted = ?', [1, 0]);

$guetos = $guetosQuery->results();

//campos...

$camposQuery = DB::getInstance()->query('SELECT * FROM app_museum_survivors_campos WHERE active = ? AND deleted = ?', [1, 0]);

$campos = $camposQuery->results();


if(Input::exists("get") && Input::get('id')){

	$editMode = true;

	$survivorQuery = DB::getInstance()->query('SELECT * FROM  app_museum_survivors_project WHERE id= ? AND deleted = ?', [Input::get('id'), 0]);

	$actualSurvivor = $survivorQuery->first();

	$name = $actualSurvivor->name;

	$surname = $actualSurvivor->surname;

	$alternative_surname = $actualSurvivor->alternative_surname;

	$son_num = ($actualSurvivor->son_num == 0) ? '' : $actualSurvivor->son_num;

	$grandson_num = ($actualSurvivor->grandson_num == 0) ? '' : $actualSurvivor->grandson_num;

	$parts = explode('-', $actualSurvivor->birthday);

	//$year = $parts[0];
	$year = ($parts[0] == 0000) ? '' : $parts[0];
	
	$month = ($parts[1] == 00) ? '' : $parts[1];

	$day = ($parts[2]) == 00 ? '' : $parts[2];

	 //...

	$survivorNationality = $actualSurvivor->nationality;

	if($survivorNationality != NULL && $survivorNationality != 0){

		$countryQuery = DB::getInstance()->query('SELECT id, name_es FROM world_location_countries WHERE id = ?', 

			[$survivorNationality]);

		$actualSurvivorCountry = $countryQuery->first();

		$survivorCountry = $actualSurvivorCountry->name_es;

	}else{

		$survivorCountry = "";

	}

	//...

	$survivorCity = $actualSurvivor->city;

	$bio = $actualSurvivor->bio;

	if($survivorCity != NULL && $survivorCity != 0 && $survivorCity != -1){

		$cityQuery = DB::getInstance()->query('SELECT id, name FROM world_location_cities WHERE id = ?', 

			[$survivorCity]);

		$actualSurvivorCity = $cityQuery->first();

		$survivorCityName = $actualSurvivorCity->name;

	}else{

		$survivorCityName = "";

	}

	//...

	$survivorAtThatMoment = $actualSurvivor->nationality_at_that_moment;

	if($survivorAtThatMoment != NULL && $survivorAtThatMoment != 0){

		$atThatMomentquery = DB::getInstance()->query('SELECT id, name_es FROM world_location_countries WHERE id = ?', 

			[$survivorAtThatMoment]);

		$actualSurvivorAtThatMoment = $atThatMomentquery->first();

		$atThatMomentCountryName = $actualSurvivorAtThatMoment->name_es;

	}else{

		$atThatMomentCountryName = "";

	}
	//...


	//...

	$survivorAtTheBeginning = $actualSurvivor->nationality_at_the_beginning_of_war;

	if($survivorAtTheBeginning != NULL && $survivorAtTheBeginning != 0){

		$query = DB::getInstance()->query('SELECT id, name_es FROM world_location_countries WHERE id = ?', 

			[$survivorAtTheBeginning]);

		$first = $query->first();

		$atTheBeginning = $first->name_es;

	}else{

		$atTheBeginning = "Seleccione una ciudad";

	}
	//...


	//...

	$liberationPlace = $actualSurvivor->liberation_place;

	if($liberationPlace != NULL && $liberationPlace != 0 && $liberationPlace != -1){

		$query = DB::getInstance()->query('SELECT id, name_es FROM world_location_countries WHERE id = ?', 

			[$liberationPlace]);

		$first = $query->first();

		$liberationPlaceName = $first->name_es;

	}else{

		$liberationPlaceName = "Seleccione un país";

	}
	//...

	$entryYear = ($actualSurvivor->entry_year == 0) ? '' : $actualSurvivor->entry_year;;

	$emigrated = ($actualSurvivor->emigrated == 1) ? 'checked' : '';

	$deathMarches = ($actualSurvivor->death_marches == 1) ? 'checked' : '';
	
	$hideAtFamilyHouses = ($actualSurvivor->hide_at_family_houses == 1) ? 'checked' : '';

	$hideOnWoods = ($actualSurvivor->hide_on_woods == 1) ? 'checked' : '';

	$hideOnInstitutions = ($actualSurvivor->hide_on_institutions == 1) ? 'checked' : '';

	$useFalseIdentity = ($actualSurvivor->use_false_identity == 1) ? 'checked' : '';

	$wasDeported = ($actualSurvivor->was_deported == 1) ? 'checked' : '';

	$wasRescued = ($actualSurvivor->was_rescued == 1) ? 'checked' : '';

	$isRighteous = ($actualSurvivor->is_righteous == 1) ? 'checked' : '';
	
	$participated = ($actualSurvivor->participated == 1) ? 'checked' : '';
	
	$partisan = ($actualSurvivor->partisan == 1) ? 'checked' : '';

	$ally = ($actualSurvivor->ally == 1) ? 'checked' : '';
		
	$dpCamp = ($actualSurvivor->dp_camp == 1) ? 'checked' : '';
	
	$legal = ($actualSurvivor->legal == 1) ? 'checked' : '';

	$marriage = ($actualSurvivor->marriage == 1) ? 'checked' : '';


	//**
	//Rescuer

	$rescuerStringQuery = "SELECT amspr.id AS idRescuer, amspr.name, amspr.surname, amspr.active, amspr.deleted,
								amsp.id AS idSurvivor,
								amsprr.id AS idRelation, amsprr.id_survivor, amsprr.id_rescuer, amsprr.active, amsprr.deleted
							FROM app_museum_survivors_project_rescuers as amspr, 
								app_museum_survivors_project AS amsp,
								app_museum_survivors_project_rescuers_relations AS amsprr
							WHERE amsp.id = amsprr.id_survivor
							AND amsprr.id_rescuer = amspr.id
							AND amsp.id = ?
							AND amspr.active = ?
							AND amspr.deleted = ?
							AND amsprr.active = ?
							AND amsprr.deleted = ?";

	$getRescuerQuery = DB::getInstance()->query($rescuerStringQuery, [$actualId, 1, 0, 1, 0]);

	$rescuerList = $getRescuerQuery->results(true);


	//Reordeno array de rescuers relacionados para luego comparacion en select en modo edicion..

	$rescuersAssignedToSurvivor = array();

	foreach($rescuerList as $key => $value){

		$rescuersAssignedToSurvivor[$value['id_rescuer']] = [

			'idRelation'		=> $value['idRelation'],
			'name'				=> $value['name'],
			'surname'			=> $value['surname'],
			'idRescuer'			=> $value['id_rescuer']

		];

	}


	//**
	//Family Relations (to this survivor, edit mode)

	$getFamilyStringQuery = "SELECT amspfr.name, amspfr.surname, amspfr.active, amspfr.deleted,
									amsp.id AS idSurvivor, amsp.active, amsp.deleted,
									amspfrrt.id AS id_relation_table, amspfrrt.id_survivor, amspfrrt.id_relation, amspfrrt.id_relation_type
							FROM app_museum_survivors_projects_family_relations AS amspfr,
									app_museum_survivors_projects_family_relations_relations_table AS amspfrrt,
									app_museum_survivors_project AS amsp
							WHERE amsp.id = amspfrrt.id_survivor
							AND amspfrrt.id_relation = amspfr.id
							AND amsp.id = ?
							AND amspfr.active = ?
							AND amspfr.deleted = ?";

	$getFamilyRelationsQuery = DB::getInstance()->query($getFamilyStringQuery, [$actualId, 1, 0]);

	
	$familyList = $getFamilyRelationsQuery->results(true);


	//Reordeno array para select en modo edicion.. no repetir opciones de select con lo que ya tiene relacionado / asignado este sobreviviente.

	$familyAssignedToSurvivor = array();

	foreach($familyList as $key =>$value){

		$familyAssignedToSurvivor[$value['id_relation']] = array(

			'id_relation'	=> $value['id_relation'],
			'name'			=> $value['name'],
			'surname'		=> $value['surname'],
			'relation_type'	=> $value['id_relation_type'],
			'idSurvivor'	=> $value['idSurvivor']

		);

	}


	//**
	//Books relations

	$getBooksStringQuery = "SELECT amspb.id AS idBook, amspb.name,
									amspbr.id AS idRelation, amspbr.id_survivor AS idSurvivorRelation, amspbr.id_book AS idBookRelation,
									amsp.id AS idSurvivor, amsp.active, amsp.deleted
							FROM app_museum_survivors_project_books AS amspb,
									app_museum_survivors_project_books_relations AS amspbr,
									app_museum_survivors_project AS amsp
							WHERE amsp.id = amspbr.id_survivor
							AND amspb.id = amspbr.id_book
							AND amsp.id = ?";

	$getBooksQuery = DB::getInstance()->query($getBooksStringQuery, [$actualId]);

	$booksList = $getBooksQuery->results(true);

	//Reordeno array de books relacionados para luego comparacion en select en modo edicion..

	$booksAssignedToSurvivor = array();

	foreach($booksList as $key => $value){

		$booksAssignedToSurvivor[$value['idBook']] = array(

				'idBook'	=> $value['idBook'],
				'name'		=> $value['name'],
				'idRelation'=> $value['idRelation'],
				'idSurvivor'=> $value['idSurvivor']
		);

	}


	//**
	//Films relations assigned to this survivor

	$getFilmsStringQuery = "SELECT amspf.id AS idFilm, amspf.name,
								amspfr.id AS idRelation, amspfr.id_survivor AS idSurvivorRelation, amspfr.id_film AS idFilmRelation,
								amsp.id AS idSurvivor, amsp.active, amsp.deleted
							FROM app_museum_survivors_project_films AS amspf,
								app_museum_survivors_project_films_relations AS amspfr,
								app_museum_survivors_project AS amsp
							WHERE amspf.id =  amspfr.id_film
							AND amsp.id = amspfr.id_survivor
							AND amsp.id = ?";

	$getFilmsQuery = DB::getInstance()->query($getFilmsStringQuery, [$actualId]);

	$filmsList = $getFilmsQuery->results(true);

	
	// Todos los films que tienen relacion a este sobreviviente, ordenado con el index del idFilm en nuevo array
	// Luego los comparo con $films que tienen todos los films en el <select> asi no muestro las opciones que ya estan relacionadas a este survivor.
	
	$filmsAssignedToSurvivor = array();

	foreach ($filmsList as $key => $value) {
		
		$filmsAssignedToSurvivor[$value['idFilm']] = array(

					"idFilm"		=> $value['idFilm'],
					"name"			=> $value['name'],
					"idRelation"	=> $value['idRelation'],
					"idSurvivor"	=> $value['idSurvivor']
		);

	}



	//**
	//Honors relations assigned to this survivor

	$getHonorsStringQuery = "SELECT amsph.id AS idHonor, amsph.name,
									amsphr.id AS idRelation, amsphr.id_survivor AS idSurvivorRelation, amsphr.id_honor AS idHonorRelation,
									amsp.id AS idSurvivor, amsp.active, amsp.deleted
							FROM app_museum_survivors_project_honors AS amsph,
									app_museum_survivors_project_honors_relations AS amsphr,
									app_museum_survivors_project AS amsp
							WHERE amsp.id = amsphr.id_survivor
							AND amsph.id = amsphr.id_honor
							AND amsp.id = ?
							ORDER BY amsphr.id ASC";

	$getHonorsQuery = DB::getInstance()->query($getHonorsStringQuery, [$actualId]);

	$honorsList = $getHonorsQuery->results(true);

	//Reordeno array honors para select en modo editar...

	$honorsAssignedToSurvivor = array();

	foreach($honorsList as $key => $value){

		$honorsAssignedToSurvivor[$value['idHonor']] = array(

			'idHonor'	=> $value['idHonor'],
			'name'		=> $value['name'],
			'idRelation'=> $value['idRelation'],
			'idSurvivor'=> $value['idSurvivor']

		);

	}


	//**
	//Guetos relations assigned to this survivor

	$guetosAssignedToSurvivor = array();

	$getGuetosStringQuery = "SELECT amg.id AS idGueto, amg.name, amg.active, amg.deleted,
								amsp.id AS idSurvivor, amsp.active, amsp.deleted,
								amspgr.id AS idRelation, amspgr.id_survivor, amspgr.id_gueto
							FROM app_museum_guetos AS amg,
								app_museum_survivors_project AS amsp,
								app_museum_survivors_project_guetos_relations AS amspgr
							WHERE amg.id = amspgr.id_gueto
								AND amsp.id = amspgr.id_survivor
								AND amsp.id = ?";

	$getGuetosQuery = DB::getInstance()->query($getGuetosStringQuery, [$actualId]);

	$guetosList = $getGuetosQuery->results(true);

	foreach($guetosList as $key => $value){

		$guetosAssignedToSurvivor[$value['idGueto']] = array(

				'idGueto'		=> $value['idGueto'],
				'name'			=> $value['name'],
				'idRelation'		=> $value['idRelation'],
				'idSurvivor'	=> $value['idSurvivor']	

		);

	}
		

	//**
	//Campos relations assigned to this survivor

	$camposAssignedToSurvivor = array();

	$getCamposStringQuery = "SELECT amsc.id AS idCampo, amsc.name, amsc.active, amsc.deleted,
								amsp.id AS idSurvivor, amsp.active, amsp.deleted,
								amspcr.id AS idRelation, amspcr.id_survivor, amspcr.id_campo
							FROM app_museum_survivors_campos AS amsc,
								app_museum_survivors_project AS amsp,
								app_museum_survivors_project_campos_relations AS amspcr
							WHERE amsc.id = amspcr.id_campo
							AND amsp.id = amspcr.id_survivor
							AND amsp.id = ?";

	$getCamposQuery = DB::getInstance()->query($getCamposStringQuery, [$actualId]);

	$camposList = $getCamposQuery->results(true);

	foreach($camposList as $key => $value){

		$camposAssignedToSurvivor[$value['idCampo']] = array(

				'idCampo'		=> $value['idCampo'],
				'name'			=> $value['name'],
				'idRelation'	=> $value['idRelation'],
				'idSurvivor'	=> $value['idSurvivor']	

		);

	}

	//Alternative names assigned to this survivor
	$alternativeNamesStringQuery = "SELECT amspan.id AS idAlternativeName, amspan.sid, amspan.alternative_name, amspan.alternative_surname, amspan.active, amspan.deleted,
										amsp.id AS idSurvivor
									FROM app_museum_survivors_project_alternative_name AS amspan,
										app_museum_survivors_project AS amsp
									WHERE amsp.id = amspan.sid
									AND amsp.id = ?";

	$alternativesQuery = DB::getInstance()->query($alternativeNamesStringQuery, [$actualId]);

	$alternativesNameList = $alternativesQuery->results(true);

	$referencedCountryAssigned = array();

	$referencedCountriesStringQuery = "SELECT wlc.id AS idCountry, wlc.name_es,
											amsp.id AS idSurvivor,
											amsprcr.id_survivor, amsprcr.id_country, amsprcr.id AS idRelation
										FROM world_location_countries AS wlc,
											app_museum_survivors_project AS amsp,
											app_museum_survivors_project_referenced_countries_relations AS amsprcr
										WHERE amsp.id = amsprcr.id_survivor
										AND amsprcr.id_country = wlc.id
										AND amsp.id = ?";

	$referencedQuery = DB::getInstance()->query($referencedCountriesStringQuery, [$actualId]);

	$referencedCountryList = $referencedQuery->results(true);

	//Reordeno array de referenced countries

	foreach($referencedCountryList as $key => $value){

		$referencedCountryAssigned[$value['id_country']] = array(

			'idCountry'		=> $value['idCountry'],
			'name_es'		=> $value['name_es'],
			'idRelation'	=> $value['idRelation'],
			'idSurvivor'	=> $value['idSurvivor']

		);

	}

	/*
	echo '<pre>';
	print_r($referencedCountryList);
	echo '</pre>';
	*/



}else{

	


	$editMode = false;

	$name = '';

	$surname = '';

	$alternative_surname = '';

	$year = '0000';

	$month = '00';

	$day = '00';

	$bio = '';

	$birthdayYear = '';

	$entryYear = '';

	$survivorNationality = '';

	$survivorCity = '';

	$liberationPlaceName = '';

	$birthPlace = '';

	$emigrated = '';
	
	$deathMarches = '';

	$hideAtFamilyHouses = '';

	$hideOnWoods = '';

	$hideOnInstitutions = '';

	$useFalseIdentity = '';

	$wasDeported = '';

	$wasRescued = '';

	$isRighteous = '';

	$participated = '';

	$partisan = '';

	$ally = '';

	$dpCamp = '';

	$legal = '';

	$marriage = '';
	
	$survivorAtThatMoment = '';

	$survivorAtTheBeginning = '';

	$liberationPlace = '';
}

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Proyecto Sobrevivientes | Agregar</title>

	<meta name="description" content="add new class">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{

		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	#map {

		height: 500px;

		width:100%;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}



</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<input type="hidden" id="survivor-project-id" name="survivor-project-id" value="<?php echo $actualId;?>">

	<div class="m-grid m-grid--hor m-grid--root m-page">


		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">

								<?php echo ($editMode) ? 'editando sobreviviente: ' . $name : 'Proyecto Sobrevivientes';?>

							</h3>
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>

								<li class="m-nav__separator">-</li>

								<li class="m-nav__item">

									<a href="museum_app_survivors_project_add.php" class="m-nav__link">

										<span class="m-nav__link-text">Agregar Información sobre Sobreviviente</span>

									</a>

								</li>

								<span> | </span>


								<li class="m-nav__item">

									<a href="museum_app_survivors_project_list.php" class="m-nav__link">

										<span class="m-nav__link-text">Ver listado</span>

									</a>

								</li>

							</ul>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">

							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link <?php if($actualTab =='general-tab') echo 'active';?>" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Información general

												</a>

											</li>

											<li class="nav-item m-tabs__item <?php if(!$editMode) echo 'disabled'; ?>">
												
												<a class="<?php if($actualTab =='images-tab') echo 'active';?> tab-navigation images-tab nav-link m-tabs__link" data-toggle="tab" href="#images-tab" role="tab">

													Imágenes

												</a>

											</li>

											<li class="nav-item m-tabs__item <?php if(!$editMode) echo 'disabled'; ?>">
												
												<a class="<?php if($actualTab =='videos-tab') echo 'active';?> tab-navigation videos-tab nav-link m-tabs__link" data-toggle="tab" href="#videos-tab" role="tab">

													Videos

												</a>

											</li>

										</ul>

									</div>

								</div>


								<div class="tab-content">

									<div class="tab-pane <?php if($actualTab =='images-tab') echo 'active';?>" id="images-tab">

										<div class="mt-5 mb-5 col-lg-12">

											<input type="file" id="images-batch" name="images-batch[]" accept="image/*" multiple>

										</div>
									</div>


									<div class="tab-pane <?php if($actualTab =='general-tab') echo 'active';?>" id="general-tab">

										<form  autocomplete="off" id="add-survivor-project-data-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>

												</div>

											</div>

											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">

													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															Identidad

														</h3>


													</div>

												</div>

											</div>

											<!-- Survivor name -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Nombre de origen:</label>

												<div class="col-lg-4 col-md-9 col-8 input-group">

													<input id="name" name="name" type="text" class="form-control m-input" placeholder="Nombre" value="<?php echo $name;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $name;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											
											<!-- Survivor surname-->

											<div class="form-group m-form__group row pt-4">

												<label for="surname" class="col-form-label col-lg-3 col-sm-12">Apellido:</label>

												<div class="col-lg-4 col-md-9 col-8 input-group">

													<input id="surname" name="surname" type="text" class="form-control m-input" placeholder="Apellido" value="<?php echo $surname;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $surname;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<!-- other surname-->

											<div class="form-group m-form__group row pt-4">

												<label for="alternative_surname" class="col-form-label col-lg-3 col-sm-12">Apellido alternativo de casada:</label>

												<div class="col-lg-4 col-md-9 col-8 input-group">

													<input id="alternative_surname" name="alternative_surname" type="text" class="form-control m-input" placeholder="apellido alternativo" value="<?php echo $alternative_surname;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $surname;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<!-- repeater! -->

											<div id="m_repeater_7"> 

												<div class="form-group  m-form__group row" id="m_repeater_7">

													<label class="col-form-label col-lg-3 col-sm-12">¿Usó otros nombres?<br> <small>Agregalos desde acá</small></label>

													<div data-repeater-list="" class="col-lg-8 col-md-9 col-sm-12">

														<div  data-repeater-item="" class="alternative-name-repeater form-group m-form__group row align-items-center" style="padding-left:0px !important; padding-right: 0px !important"> 

															<div class="col-md-3">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name="new_alternative_name" type="text" class="form-control m-input new_alternative_name" placeholder="nombre alternativo" holder="Nombre alternativo"> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															<div class="col-md-3">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name='new_alternative_surname' type="text" class="form-control m-input new_alternative_surname" placeholder="Apellido alternativo"> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															<div class="col-md-1">

																<?php if(!$editMode){?>

																<div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">

																	<span>

																		<i class="la la-trash-o"></i>

																		<span>Eliminar</span>

																	</span>

																</div>

																<?php }else{ ?>

																<div class="btn-add-in-edition btn-add-in-edition-alternatives btn-sm btn btn-info m-btn m-btn--icon m-btn--pill">

																	<span>

																		<i class="la la-plus"></i>

																		<span>Agrregar</span>

																	</span>

																</div>


																<?php } ?>

															</div>

													</div>

												</div>  

											</div>


											<?php if(!$editMode){ ?>

												<div class="m-form__group form-group row">

													<label class="col-form-label col-lg-3 col-sm-12"></label>

													<div class="col-lg-4">

														<div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">

															<span>

																<i class="la la-plus"></i>

																<span>Agregar otro</span>

															</span>

														</div>

													</div>                                        

												</div>

											<?php } ?>

										</div>

										<!-- alternative name tags on edit mode... -->

										<?php

											if($editMode){

											?>

												<div class="form-group m-form__group row pt-4">

												<label class="control-label col-md-3"></label>

												<div class="col-md-9">
													
													<div class="alternatives-tags">
														
														<?php

															foreach($alternativesNameList as $key => $alternativeData){

																
																?>

																<div class="btn-group">
																	<a  href="#" role="button" class="btn btn-default btn-xs disabled" aria-disabled="true"><?php echo $alternativeData['alternative_name'] . " " . $alternativeData['alternative_surname']; ?></a>
																	<a id="<?php echo $alternativeData['idAlternativeName']; ?>" data-item-id="<?php echo $alternativeData['idAlternativeName']; ?>" class="btn btn-xs delete-alternative-relation-tag" data-toggle="tooltip" title="Eliminar">
																		<i class="fas fa-times-circle"></i>
																	</a>
																</div>

																<?php


															}

														?>

													</div>

												</div>
												
											</div>

											<?php

											}

											?>

											<!-- /.alternative name tags on edit mode -->

										<!-- alternative - names -->

										<!-- Birth date -->

										<div class="form-group m-form__group row">

											<label for="birthday_year" class="col-form-label col-lg-3 col-sm-12">Año de nacimiento:</label>

											<div class="col-lg-4 col-12">

												<div class="input-group date">

													<input id="birthday_year" name="birthday_year" type="text" class="form-control" value="<?php if($editMode) echo $year;?>">

													<div class="input-group-addon">

														<i class="pt-2 far fa-calendar-alt"></i>

													</div>

												</div>

											</div>

										</div>

										<!-- / end of Birth date -->

										<!-- Month date -->

										<div class="form-group m-form__group row">

											<label for="birthday_month" class="col-form-label col-lg-3 col-sm-12">Mes de nacimiento:</label>

											<div class="col-lg-4 col-12">

												<div class="input-group date">

													<input disabled id="birthday_month" name="birthday_month" type="text" class="form-control" value="<?php if($editMode) echo $month;?>">
													<div class="input-group-addon">
														<i class="pt-2 far fa-calendar-alt"></i>
													</div>

												</div>

											</div>

										</div>

										<!-- /. Month date -->

										<!-- Day date -->

										<div class="form-group m-form__group row">

											<label for="birthday_day" class="col-form-label col-lg-3 col-sm-12">Día de nacimiento:</label>

											<div class="col-lg-4 col-12">

												<div class="input-group date">

													<input disabled id="birthday_day" name="birthday_day" type="text" class="form-control" value="<?php if($editMode) echo $day;?>">
													<div class="input-group-addon">
														<i class="pt-2 far fa-calendar-alt"></i>
													</div>

												</div>

											</div>
											
										</div>

										<!-- /. Month date -->

										<!-- Nationality -->

										<div id="country-list" class="form-group m-form__group row pt-4">

											<label for="nationality" class="col-form-label col-lg-3 col-12">País de nacimiento<br>

												<small data-toggle="tooltip" data-placement="bottom" title="Comenzá a escribir el pais y seleccionalo de la lista">¡Lee esto!</small>

											</label>

											<div class="col-lg-4 col-md-9 col-sm-12">

												<input autocomplete = "new-password" type="text" id="nationality" name="nationality" class="typeahead form-control m-input" placeholder="país" value="<?php echo  ($editMode) ? $survivorCountry : ''; ?>">

												<input autocomplete = "new-password" type="hidden" id="hidden-country-id" name="hidden-country-id" value="<?php echo $survivorNationality; ?>">

											</div> 

										</div>

										<!-- end of nationality -->

										

										<!-- city -->

										<div id="cities-list" class="form-group m-form__group row pt-4 disabled">

											<label for="xcv" class="col-form-label col-lg-3 col-12">Lugar de nacimiento:

												<br><small data-toggle="tooltip" data-placement="bottom" title="Comenzá a escribir la ciudad y seleccionalo de la lista">¡Lee esto!</small>

											</label>

											<div class="col-lg-4 col-md-9 col-sm-12 cleanner">

												<input type="text" id="xcv" name="xcv" class="typeahead form-control m-input" placeholder="Ciudad" value="<?php echo  ($editMode) ? $survivorCityName : ''; ?>">

												<input type="hidden" id="hidden-city-id" name="hidden-city-id" value="<?php echo $survivorCity; ?>">

											</div>

										</div>

										<!-- end of city --> 
										
										<!-- en ese momento --> 

										<div id="at-that-moment-list" class="form-group m-form__group row pt-4">

											<label for="at-that-moment" class="col-form-label col-lg-3 col-12">en ese entonces el país era:
												<br><small data-toggle="tooltip" data-placement="bottom" title="Comenzá a escribir el pais y seleccionalo de la lista">¡Lee esto!</small>

											</label>

											<div class="col-lg-4 col-md-9 col-sm-12">

												<input type="text" id="at-that-moment" name="at-that-moment" class="typeahead form-control m-input" placeholder="Nacionalidad" value="<?php echo  ($editMode) ? $atThatMomentCountryName : ''; ?>">

												<input type="hidden" id="hidden-at-that-moment-id" name="hidden-at-that-moment-id" value="<?php echo $survivorAtThatMoment; ?>">

											</div>

										</div>

										<!-- paises de referencia -->

										<div class="form-group m-form__group row">

											<label class="col-form-label col-lg-3 col-sm-12">Otros paises de referencia:</label>

											<div class="col-lg-4 col-md-9 col-sm-12">

												<select class="col-12 referenced-selector selector-for-update" name="states[]" multiple="multiple">

													<?php 

														foreach($countries as $country){

															if(!isset($referencedCountryAssigned[$country->id]))
															{

																?>

																	<option value="<?php echo $country->id;?>"><?php echo $country->name_es;?></option>

																<?php 

															}
													
														
														}

													?>

												</select>

											</div>

											<?php if($editMode){  ?>

												<div class=""> 

													<button data-id="<?php echo $actualId;?>" data-action="referenceds" class="select-update-btn btn btn-default" type="button">actualizar</button>

												</div>

												<?php }?>

										</div>

										<!-- referenced country tags on edit mode... -->

										<?php

											if($editMode){

											?>

												<div class="form-group m-form__group row pt-4">

												<label class="control-label col-md-3"></label>

												<div class="col-md-9">
													
													<div class="referenceds-tags">
														
														<?php

															foreach($referencedCountryList as $key => $referencedData){

																
																?>

																<div class="btn-group">
																	<a  href="#" role="button" class="btn btn-default btn-xs disabled" aria-disabled="true"><?php echo $referencedData['name_es']; ?></a>
																	<a id="<?php echo $referencedData['idRelation']; ?>" data-item-id="<?php echo $referencedData['idCountry']; ?>" class="btn btn-xs delete-referenced-relation-tag" data-toggle="tooltip" title="Eliminar">
																		<i class="fas fa-times-circle"></i>
																	</a>
																</div>

																<?php


															}

														?>

													</div>

												</div>
												
											</div>

											<?php

											}

											?>

											<!-- /.referenced country tags on edit mode -->

										
										<!-- init -->

										<div id="at-the-beginning-list" class="form-group m-form__group row pt-4">

											<label for="nationality" class="col-form-label col-lg-3 col-12">País en el que vivía al comenzar la guerra:

											</label>

											<div class="col-lg-4 col-md-9 col-sm-12">

												<input type="text" id="nationality" name="nationality" class="typeahead form-control m-input" placeholder="país" value="<?php echo  ($editMode) ? $atTheBeginning : ''; ?>">

												<input type="hidden" id="hidden-at-the-beginning-id" name="hidden-at-the-beginning-id" value="<?php echo $survivorAtTheBeginning; ?>">

											</div>
											
										</div>


										<!-- Emigrated -->

										<div class="emigrated-group form-group m-form__group row">

											<label for="emigrated" class="col-form-label col-lg-3 col-sm-12">¿Emigró antes de la guerra?<br>

												<small data-toggle="tooltip" data-placement="bottom" title="Esta opción está solo disponible para los los casos de Alemania, Austria y Checoslovaquia (a partir de 1938)">importante!</small>

											</label>

											<div class="col-lg-4 col-md-9 col-sm-12 emigrated-toggle disabled">

												<div class="m-form__group form-group row">

													<div class="col-3">

														<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

															<label>
																<input id="emigrated" name="emigrated" type="checkbox" <?php echo $emigrated;?> class="highlighted toggler-info emigrated toggler-info-emigrated" data-on="Enabled" data-id="<?php echo $actualId;?>">

																<span></span>

															</label>

														</span>

													</div>

												</div>

											</div>

										</div>

										<!-- /.Emigrated -->

										<!-- guetos! check! -->

										<div class="m-portlet__head">

											<div class="m-portlet__head-caption">

												<div class="m-portlet__head-title">

													<h3 class="m-portlet__head-text">

														Guetos y campos

													</h3>


												</div>

											</div>

										</div>

											<div class="form-group m-form__group row">

											<label class="col-form-label col-lg-3 col-sm-12">Gueto/s en los que estuvo:</label>

											<div class="col-lg-4 col-md-9 col-sm-12">

												<select class="col-12 gueto-selector selector-for-update" name="states[]" multiple="multiple">

													<?php 

														foreach($guetos as $key){


															if(!isset($guetosAssignedToSurvivor[$key->id]))
															{

															?>

																<option value="<?php echo $key->id; ?>"><?php echo $key->name; ?></option>


															<?php

															}

															?>


														<?php 

														} 

													?>

													</select>

												</div>

												<?php if($editMode){  ?>

													<div class=""> 

														<button data-id="<?php echo $actualId;?>" data-action="guetos" class="select-update-btn btn btn-default" type="button">actualizar</button>

													</div>

												<?php }?>	

											</div>

											<!-- repeater! -->

											<div id="m_repeater_8"> 

												<div class="form-group  m-form__group row" id="m_repeater_8">

													<label class="col-form-label col-lg-3 col-sm-12">Añadir gueto:

														<br><small>¿No está en la lista? agregalo</small>

													</label>
													
													<div class="col-form-label col-lg-3 col-sm-12">
														<p>Desde el siguiente enlace: <a href="museum_app_guetos_add.php">Añadir un Gueto</a></p>
													</div>
													

												</div>
													
											</div>


												<!-- Guetos tags on edit mode... -->

												<?php

													if($editMode){

													?>

														<div class="form-group m-form__group row pt-4">

														<label class="control-label col-md-3"></label>

														<div class="col-md-9">
															
															<div class="guetos-tags">
																
																<?php

																	foreach($guetosList as $key => $guetoData){

																		
																		?>

																		<div class="btn-group">
												                            <a  href="#" role="button" class="btn btn-default btn-xs disabled" aria-disabled="true"><?php echo $guetoData['name']; ?></a>
												                            <a id="<?php echo $guetoData['idRelation']; ?>" data-item-id="<?php echo $guetoData['idGueto']; ?>" class="btn btn-xs delete-gueto-relation-tag" data-toggle="tooltip" title="Eliminar">
												                                <i class="fas fa-times-circle"></i>
												                            </a>
												                        </div>

																		<?php


																	}

																?>

															</div>

														</div>
														
													</div>

													<?php
													
													}

												?>

												<!-- /.Guetos tags on edit mode -->




											<!-- campos -->

											<div class="form-group m-form__group row">

											<label class="col-form-label col-lg-3 col-sm-12">Campo/s en los que estuvo:</label>

											<div class="col-lg-4 col-md-9 col-sm-12">

												<select class="col-12 campo-selector selector-for-update" name="states[]" multiple="multiple">

													<?php 

														foreach($campos as $key){

															if(!isset($camposAssignedToSurvivor[$key->id])){

																?>

																	<option value="<?php echo $key->id; ?>"><?php echo $key->name; ?></option>

																<?php

															}
													
														}

													?>

													</select>

												</div>

												<?php if($editMode){  ?>

													<div class=""> 

														<button data-id="<?php echo $actualId;?>" data-action="campos" class="select-update-btn btn btn-default" type="button">actualizar</button>

													</div>

												<?php }?>

											</div>

											<!-- repeater! -->

											<div id="m_repeater_9"> 

												<div class="form-group  m-form__group row" id="m_repeater_9">

													<label class="col-form-label col-lg-3 col-sm-12">Añadir campo:

														<br><small>¿No está en la lista? agregalo</small>

													</label>

													<div data-repeater-list="" class="col-lg-8 col-md-9 col-sm-12">

														<div data-repeater-item="" class="my-repeater-item form-group m-form__group row align-items-center" style="padding-left:0px !important; padding-right: 0px !important"> 

															<div class="col-md-6">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name="new_campo_name" type="text" class="form-control m-input new_campo_name" placeholder="nombre" holder="Nombre"> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															

															<div class="col-md-1">

																<?php if(!$editMode){?>

																	<div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">

																		<span>

																			<i class="la la-trash-o"></i>

																			<span>Eliminar</span>

																		</span>

																	</div>

																<?php }else{ ?>

																	<div class="btn-add-in-edition btn-add-in-edition-campo btn-sm btn btn-info m-btn m-btn--icon m-btn--pill">

																		<span>

																			<i class="la la-plus"></i>

																			<span>Agrregar</span>

																		</span>

																	</div>


																<?php } ?>

															</div>

														</div></div>                 

													</div>

													<?php if(!$editMode){ ?>

														<div class="m-form__group form-group row">

															<label class="col-form-label col-lg-3 col-sm-12"></label>

															<div class="col-lg-4">

																<div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">

																	<span>

																		<i class="la la-plus"></i>

																		<span>Agregar otro</span>

																	</span>

																</div>

															</div>                                        

														</div>

													<?php } ?>

												</div>

												<!-- Campos tags on edit mode... -->

												<?php

													if($editMode){

													?>

														<div class="form-group m-form__group row pt-4">

														<label class="control-label col-md-3"></label>

														<div class="col-md-9">
															
															<div class="campos-tags">
																
																<?php

																	foreach($camposList as $key => $campoData){

																		
																		?>

																		<div class="btn-group">
												                            <a  href="#" role="button" class="btn btn-default btn-xs disabled" aria-disabled="true"><?php echo $campoData['name']; ?></a>
												                            <a id="<?php echo $campoData['idRelation']; ?>" data-item-id="<?php echo $campoData['idCampo']; ?>" class="btn btn-xs delete-campo-relation-tag" data-toggle="tooltip" title="Eliminar">
												                                <i class="fas fa-times-circle"></i>
												                            </a>
												                        </div>

																		<?php


																	}

																?>

															</div>

														</div>
														
													</div>

													<?php
													
													}

												?>

												<!-- /.Campos tags on edit mode -->

											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">

													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															Marchas de la muerte

														</h3>


													</div>

												</div>

											</div>


												<!-- Death Marches -->
											<div class="form-group m-form__group row">

												<label for="death_marches" class="col-form-label col-lg-3 col-sm-12">¿Participó de alguna?</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="death_marches" name="death_marches" class="highlighted toggler-info death-marches" type="checkbox" <?php echo $deathMarches;?> data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<!-- /.Death Marches -->

											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">

													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															Escondites

														</h3>


													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label for="hide_at_family_houses" class="col-form-label col-lg-3 col-sm-12">¿Se escondió en casas de familias?</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="hide_at_family_houses" name="hide_at_family_houses" class="highlighted toggler-info hide_at_family_houses" type="checkbox" <?php echo $hideAtFamilyHouses;?> data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label for="hide_on_woods" class="col-form-label col-lg-3 col-sm-12">¿Se escondió en bosques?</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="hide_on_woods" name="hide_on_woods" class="highlighted toggler-info hide_on_woods-marches" type="checkbox" <?php echo $hideOnWoods;?> data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label for="hide_on_institutions" class="col-form-label col-lg-3 col-sm-12">¿Se escondió en instituciones?</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="hide_on_institutions" name="hide_on_institutions" class="highlighted toggler-info hide_on_institutions" type="checkbox" <?php echo $hideOnInstitutions;?> data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>


											<div class="form-group m-form__group row">

												<label for="use_false_identity" class="col-form-label col-lg-3 col-sm-12">¿Usó una identidad falsa?</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="use_false_identity" name="use_false_identity" class="highlighted toggler-info use_false_identity" type="checkbox" <?php echo $useFalseIdentity;?> data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">

													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															Datos Varios

														</h3>


													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label for="was_deported" class="col-form-label col-lg-3 col-sm-12">¿Fue deportado a Siberia?</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="was_deported" name="was_deported" class="highlighted toggler-info was_deported" type="checkbox" <?php echo $wasDeported;?> data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label for="was_rescued" class="col-form-label col-lg-3 col-sm-12">¿Fue rescatado?</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="was_rescued" name="was_rescued" class="highlighted toggler-info was_rescued" type="checkbox" <?php echo $wasRescued;?> data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<!-- rescuer -->			
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Rescatador:<br>

													<small data-toggle="tooltip" data-placement="bottom" title="Para que se habiliten estos campos debe haber sido rescatado (switch ¿Fue rescatado? activo)">importante!</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<select class="col-12 rescuer-selector selector-for-update" name="states[]" multiple="multiple" <?php echo ($wasRescued != "checked" ) ? 'disabled' : ''; ?> >

														<?php

														
														
														foreach($rescuers as $rescuer){


															if(!isset($rescuersAssignedToSurvivor[$rescuer->id])){



																//echo $relation->name . ' ' . $relation->surname . '<br />';

																?>

																<option value="<?php echo $rescuer->id;?>"><?php echo $rescuer->name . " " . $rescuer->surname; ?></option>

																<?php

															}

															
														}

														?>

													</select>

												</div>

												<?php if($editMode){  ?>

													<div class=""> 

														<button data-id="<?php echo $actualId;?>" data-action="rescuers" class="select-update-btn btn btn-default" type="button">actualizar</button>

													</div>

												<?php }?>

											</div>

											<!-- repeater! -->

											<div id="m_repeater_1"> 

												<div class="form-group  m-form__group row" id="m_repeater_1">

													<label class="col-form-label col-lg-3 col-sm-12">Añadir rescatador:

														<br><small>¿No está en la lista? agregalo</small>

													</label>

													<div data-repeater-list="" class="col-lg-8 col-md-9 col-sm-12">

														<div data-repeater-item="" class="rescuer-repeater-item form-group m-form__group row align-items-center" style="padding-left:0px !important; padding-right: 0px !important"> 

															<div class="col-md-3">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name="new_rescuer_name" type="text" class="form-control m-input new_rescuer_name" placeholder="nombre" holder="Nombre" <?php echo ($wasRescued != "checked" ) ? 'disabled' : ''; ?>> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															<div class="col-md-3">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name='new_rescuer_surname' type="text" class="form-control m-input new_rescuer_surname" placeholder="Apellido" <?php echo ($wasRescued != "checked" ) ? 'disabled' : ''; ?>> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															<div class="col-md-1">

																<?php if(!$editMode){?>

																	<div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">

																		<span>

																			<i class="la la-trash-o"></i>

																			<span>Eliminar</span>

																		</span>

																	</div>

																<?php }else{ ?>

																	<div class="btn-add-in-edition btn-add-in-edition-rescuer btn-sm btn btn-info m-btn m-btn--icon m-btn--pill">

																		<span>

																			<i class="la la-plus"></i>

																			<span>Agregar</span>

																		</span>

																	</div>


																<?php } ?>

															</div>

														</div></div>                 

													</div>

													<?php if(!$editMode){ ?>

														<div class="m-form__group form-group row">

															<label class="col-form-label col-lg-3 col-sm-12"></label>

															<div class="col-lg-4">

																<div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">

																	<span>

																		<i class="la la-plus"></i>

																		<span>Agregar otro</span>

																	</span>

																</div>

															</div>                                        

														</div>

													<?php } ?>

												</div>


												<!-- rescued by tags on edit mode... -->

												<?php

													if($editMode){

													?>

														<div class="form-group m-form__group row pt-4">

														<label class="control-label col-md-3"></label>

														<div class="col-md-9">
															
															<div class="rescuers-tags">
																
																<?php

																	
																	foreach($rescuerList as $key => $rescuerData){

																																			
																		?>

																		<div class="btn-group">
																			<a  href="#" role="button" class="btn btn-default btn-xs disabled" aria-disabled="true"><?php echo $rescuerData['name'] . " " . $rescuerData['surname']; ?></a>
																			<a id="<?php echo $rescuerData['idRelation']; ?>" data-item-id="<?php echo $rescuerData['idRescuer']; ?>" class="btn btn-xs delete-rescuer-relation-tag" data-toggle="tooltip" title="Eliminar">
																				<i class="fas fa-times-circle"></i>
																			</a>
																		</div>

																		<?php


																	}

																?>

															</div>

														</div>
														
													</div>

													<?php
													
													}

												?>

												<!-- /.rescued by tags on edit mode -->

											<div class="form-group m-form__group row">

												<label for="is_righteous" class="col-form-label col-lg-3 col-sm-12">¿Es un rescatador?</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="is_righteous" name="is_righteous" class="highlighted toggler-info is_righteous" type="checkbox" <?php echo $isRighteous;?> data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label for="participated" class="col-form-label col-lg-3 col-sm-12">¿Participó de levantamientos en guetos o campos?</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="participated" name="participated" class="highlighted toggler-info participated" type="checkbox" <?php echo $participated;?> data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label for="partisan" class="col-form-label col-lg-3 col-sm-12">¿Fue partisano o perteneció a la resistencia?</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="partisan" name="partisan" class="highlighted toggler-info partisan" type="checkbox" <?php echo $partisan;?> data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label for="ally" class="col-form-label col-lg-3 col-sm-12">¿Perteneció a los ejércitos aliados?</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="ally" name="ally" class="highlighted toggler-info ally" type="checkbox" <?php echo $ally;?> data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">

													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															Liberación

														</h3>


													</div>

												</div>

											</div>


											<div id="liberation-place" class="form-group m-form__group row pt-4">

												<label for="nationality" class="col-form-label col-lg-3 col-12">Lugar de liberación:

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input type="text" id="nationality" name="nationality" class="typeahead form-control m-input" placeholder="país" value="<?php echo  ($editMode) ? $liberationPlaceName : ''; ?>">

													<input type="hidden" id="hidden-liberation-place-id" name="hidden-liberation-place-id" value="<?php echo $liberationPlace; ?>">

												</div>
												
											</div>



											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Ejército de liberación:	
												</label>

												<div class="m-form__group form-group">

													<div class=" m-radio-list">

														<?php

														$alertsTypesQuery = DB::getInstance()->query('SELECT * FROM app_museum_survivors_project_liberation_army WHERE active = ? AND deleted = ?',[1, 0]);

														$alertsTypesResults = $alertsTypesQuery->results();

														$value = 0;

														foreach($alertsTypesResults as $queryResult){

															$value+=1;

															$checked = false;

															if($editMode){

																if($queryResult->id == $actualSurvivor->liberation_army) 

																	$checked = true;

															}
															

															?>

															<label class="m-radio m-radio--solid <?php echo $queryResult->class;?>">

																<input <?php if($checked == true) echo 'checked';?>

																type="radio" name="radio_group" value="<?php echo $value;?>"> <?php echo $queryResult->type;?>

															<span></span>


														</label>

													<?php } ?>

												</div>

												<span class="m-form__help">Elegí al menos una opción</span>
												
											</div>

										</div>

											<div class="form-group m-form__group row">

												<label for="dp_camp" class="col-form-label col-lg-3 col-sm-12">¿DP camp?</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="dp_camp" name="dp_camp" class="highlighted toggler-info dp_camp" type="checkbox" <?php echo $dpCamp;?> data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">

													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															Ingreso a la Argentina

														</h3>


													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label for="entry_year" class="col-form-label col-lg-3 col-sm-12">Año de ingreso a la argentina:</label>

												<div class="col-lg-4 col-12">

													<div class="input-group date">

														<input id="entry_year" name="entry_year" type="text" class="form-control" value="<?php if($editMode) echo $entryYear;?>">
														<div class="input-group-addon">
															<i class="pt-2 far fa-calendar-alt"></i>
														</div>

													</div>

													<small class="age-calculator"></small>

												</div>

								
											</div>

											<div class="form-group m-form__group row">

												<label for="legal" class="col-form-label col-lg-3 col-sm-12">¿Ingresó legalmente?</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="legal" name="legal" class="highlighted toggler-info legal" type="checkbox" <?php echo $legal;?> data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>


											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">

													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															Datos familiares

														</h3>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label for="marriage" class="col-form-label col-lg-3 col-sm-12">¿Contrajo matrimonio?</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input id="marriage" name="marriage" class="highlighted toggler-info marriage" type="checkbox" <?php echo $marriage;?> data-on="Enabled" data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<!-- esposa... -->



											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Conyugue/s:</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<select class="col-12 spouse-selector selector-for-update" name="states[]" multiple="multiple">

														<?php
														
														foreach($relations as $relation){


															if(!isset($familyAssignedToSurvivor[$relation->id])){



																//echo $relation->name . ' ' . $relation->surname . '<br />';

																?>

																<option value="<?php echo $relation->id;?>"><?php echo $relation->name . " " . $relation->surname; ?></option>

																<?php

															}

															
														}

														?>

													</select>

												</div>

												<?php if($editMode){  ?>

													<div class=""> 

														<button data-id="<?php echo $actualId;?>" data-action="spouses" class="select-update-btn btn btn-default" type="button">actualizar</button>

													</div>

												<?php }?>

											</div>

											<!-- repeater! -->

											<div id="m_repeater_1"> 

												<div class="form-group  m-form__group row" id="m_repeater_1">

													<label class="col-form-label col-lg-3 col-sm-12">Añadir cónyugue:

														<br><small>¿No está en la lista? agregalo</small>

													</label>

													<div data-repeater-list="" class="col-lg-8 col-md-9 col-sm-12">

														<div data-repeater-item="" class="spouse-repeater-item form-group m-form__group row align-items-center" style="padding-left:0px !important; padding-right: 0px !important"> 

															<div class="col-md-3">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name="new_spouse_name" type="text" class="form-control m-input new_spouse_name" placeholder="nombre" holder="Nombre"> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															<div class="col-md-3">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name='new_spouse_surname' type="text" class="form-control m-input new_spouse_surname" placeholder="Apellido"> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															<div class="col-md-1">

																<?php if(!$editMode){?>

																	<div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">

																		<span>

																			<i class="la la-trash-o"></i>

																			<span>Eliminar</span>

																		</span>

																	</div>

																<?php }else{ ?>

																	<div class="btn-add-in-edition btn-add-in-edition-family btn-sm btn btn-info m-btn m-btn--icon m-btn--pill" data-family-type="spouse">

																		<span>

																			<i class="la la-plus"></i>

																			<span>Agrregar</span>

																		</span>

																	</div>


																<?php } ?>

															</div>

														</div></div>                 

													</div>

													<?php if(!$editMode){ ?>

														<div class="m-form__group form-group row">

															<label class="col-form-label col-lg-3 col-sm-12"></label>

															<div class="col-lg-4">

																<div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">

																	<span>

																		<i class="la la-plus"></i>

																		<span>Agregar otro</span>

																	</span>

																</div>

															</div>                                        

														</div>

													<?php } ?>

												</div>


												<!-- Spouse tags on edit mode... -->

												<?php

													if($editMode){

													?>

														<div class="form-group m-form__group row pt-4">

														<label class="control-label col-md-3"></label>

														<div class="col-md-9">
															
															<div class="spouses-tags">
																
																<?php

																	
																	foreach($familyList as $key => $familyData){

																		if($familyData['id_relation_type'] == 1){

																			?>

																			<div class="btn-group">
																				
													                            <a  href="#" role="button" class="btn btn-default btn-xs disabled" aria-disabled="true"><?php echo $familyData['name'] . " " . $familyData["surname"]; ?></a>
													                            
													                            <a id="<?php echo $familyData['id_relation_table']; ?>" data-family-relation="spouse" class="btn btn-xs delete-family-relation-tag" data-toggle="tooltip" title="Eliminar">
													                                <i class="fas fa-times-circle"></i>
													                            </a>

													                        </div>

																			<?php

																		}

																	}

																?>

															</div>

														</div>
														
													</div>

													<?php
													
													}

												?>

											<!-- /.Spouse tags on edit mode -->

											<!-- number of sons -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Cantidad de hijo/s:</label>

												<div class="col-lg-4 col-md-9 col-8 input-group">

													<input type="number" min="0" class="form-control m-input" id="son_num" name="son_num" placeholder="Cantidad de hijo/s" value="<?php echo $son_num; ?>">

												</div>

												<?php 
													if($editMode){ 
														
													?>

													<div class="">

														<button data-db-value="<?php echo $son_num;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar</button>

													</div>

													<?php

													}

												?>

											</div>

											<!-- /.number of sons -->

											<!-- hijo/s -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Hijo/s:</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<select class="col-12 son-selector selector-for-update" name="states[]" multiple="multiple">

														<?php
															
															foreach($relations as $relation){


																if(!isset($familyAssignedToSurvivor[$relation->id])){

																	?>

																	<option value="<?php echo $relation->id;?>"><?php echo $relation->name . " " . $relation->surname; ?></option>

																	<?php

																}

																
															}

														?>

													</select>

												</div>

												<?php if($editMode){  ?>

													<div class=""> 

														<button data-id="<?php echo $actualId;?>" data-action="sons" class="select-update-btn btn btn-default" type="button">actualizar</button>

													</div>

												<?php }?>

											</div>

											<!-- repeater! -->

											<div id="m_repeater_2"> 

												<div class="form-group  m-form__group row" id="m_repeater_2">

													<label class="col-form-label col-lg-3 col-sm-12">Añadir hijo:

														<br><small>¿No está en la lista? agregalo</small>

													</label>

													<div data-repeater-list="" class="col-lg-8 col-md-9 col-sm-12">

														<div data-repeater-item="" class="son-repeater-item form-group m-form__group row align-items-center" style="padding-left:0px !important; padding-right: 0px !important"> 

															<div class="col-md-3">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name="new_son_name" type="text" class="form-control m-input new_son_name" placeholder="nombre" holder="Nombre"> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															<div class="col-md-3">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name='new_son_surname' type="text" class="form-control m-input new_son_surname" placeholder="Apellido"> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															<div class="col-md-1">

																<?php if(!$editMode){?>

																	<div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">

																		<span>

																			<i class="la la-trash-o"></i>

																			<span>Eliminar</span>

																		</span>

																	</div>

																<?php }else{ ?>

																	<div class="btn-add-in-edition btn-add-in-edition-family btn-sm btn btn-info m-btn m-btn--icon m-btn--pill" data-family-type="son">

																		<span>

																			<i class="la la-plus"></i>

																			<span>Agrregar</span>

																		</span>

																	</div>


																<?php } ?>

															</div>

														</div></div>                 

													</div>

													<?php if(!$editMode){ ?>

														<div class="m-form__group form-group row">

															<label class="col-form-label col-lg-3 col-sm-12"></label>

															<div class="col-lg-4">

																<div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">

																	<span>

																		<i class="la la-plus"></i>

																		<span>Agregar otro</span>

																	</span>

																</div>

															</div>                                        

														</div>

													<?php } ?>

												</div>


												<!-- Son tags on edit mode... -->

												<?php

													if($editMode){

													?>

														<div class="form-group m-form__group row pt-4">

														<label class="control-label col-md-3"></label>

														<div class="col-md-9">
															
															<div class="sons-tags">
																
																<?php

																	
																	foreach($familyList as $key => $familyData){

																		if($familyData['id_relation_type'] == 2){

																			?>

																			<div class="btn-group">
													                            <a  href="#" role="button" class="btn btn-default btn-xs disabled" aria-disabled="true"><?php echo $familyData['name'] . " " . $familyData["surname"]; ?></a>
													                            <a id="<?php echo $familyData['id_relation_table']; ?>" data-item-id="<?php echo $familyData['id_relation']; ?>" data-family-relation="son" class="btn btn-xs delete-family-relation-tag" data-toggle="tooltip" title="Eliminar">
													                                <i class="fas fa-times-circle"></i>
													                            </a>
													                        </div>

																			<?php

																		}

																	}

																?>

															</div>

														</div>
														
													</div>

													<?php
													
													}

												?>


											<!-- /.Son tags on edit mode -->



											<!-- end of -->

											<!-- number of Grnadsons -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Cantidad de nieto/s:</label>

												<div class="col-lg-4 col-md-9 col-8 input-group">

													<input type="number" min="0" class="form-control m-input" id="grandson_num" name="grandson_num" placeholder="Cantidad de nieto/s" value="<?php echo $grandson_num; ?>">

												</div>

												<?php 
													if($editMode){ 
														
													?>

													<div class="">

														<button data-db-value="<?php echo $grandson_num;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar</button>

													</div>

													<?php

													}

												?>

											</div>

											<!-- /.number of Grandsons -->

											<!-- nieto -->

											<div class="form-group m-form__group row">

											<label class="col-form-label col-lg-3 col-sm-12">Nieto/s:</label>

											<div class="col-lg-4 col-md-9 col-sm-12">

												<select class="col-12 grandson-selector selector-for-update" name="states[]" multiple="multiple">

													<?php
														
														foreach($relations as $relation){


															if(!isset($familyAssignedToSurvivor[$relation->id])){



																//echo $relation->name . ' ' . $relation->surname . '<br />';

																?>

																<option value="<?php echo $relation->id;?>"><?php echo $relation->name . " " . $relation->surname; ?></option>

																<?php

															}

															
														}

													?>

													</select>

												</div>

												<?php if($editMode){  ?>

													<div class=""> 

														<button data-id="<?php echo $actualId;?>" data-action="grandsons" class="select-update-btn btn btn-default" type="button">actualizar</button>

													</div>

												<?php }?>	

											</div>

											<!-- repeater! -->

											<div id="m_repeater_3"> 

												<div class="form-group  m-form__group row" id="m_repeater_3">

													<label class="col-form-label col-lg-3 col-sm-12">Añadir nieto:

														<br><small>¿No está en la lista? agregalo</small>

													</label>

													<div data-repeater-list="" class="col-lg-8 col-md-9 col-sm-12">

														<div data-repeater-item="" class="grandson-repeater-item form-group m-form__group row align-items-center" style="padding-left:0px !important; padding-right: 0px !important"> 

															<div class="col-md-3">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name="new_grandson_name" type="text" class="form-control m-input new_grandson_name" placeholder="nombre" holder="Nombre"> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															<div class="col-md-3">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name='new_grandson_surname' type="text" class="form-control m-input new_grandson_surname" placeholder="Apellido"> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															<div class="col-md-1">

																<?php if(!$editMode){?>

																	<div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">

																		<span>

																			<i class="la la-trash-o"></i>

																			<span>Eliminar</span>

																		</span>

																	</div>

																<?php }else{ ?>

																	<div class="btn-add-in-edition btn-add-in-edition-family btn-sm btn btn-info m-btn m-btn--icon m-btn--pill" data-family-type="grandson">

																		<span>

																			<i class="la la-plus"></i>

																			<span>Agrregar</span>

																		</span>

																	</div>


																<?php } ?>

															</div>

														</div></div>                 

													</div>

													<?php if(!$editMode){ ?>

														<div class="m-form__group form-group row">

															<label class="col-form-label col-lg-3 col-sm-12"></label>

															<div class="col-lg-4">

																<div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">

																	<span>

																		<i class="la la-plus"></i>

																		<span>Agregar otro</span>

																	</span>

																</div>

															</div>                                        

														</div>

													<?php } ?>

												</div>

												<!-- grandson tags on edit mode... -->

												<?php

													if($editMode){

													?>

														<div class="form-group m-form__group row pt-4">

														<label class="control-label col-md-3"></label>

														<div class="col-md-9">
															
															<div class="grandsons-tags">
																
																<?php

																	foreach($familyList as $key => $familyData){

																		if($familyData['id_relation_type'] == 3){

																			?>

																			<div class="btn-group">
													                            <a  href="#" role="button" class="btn btn-default btn-xs disabled" aria-disabled="true"><?php echo $familyData['name'] . " " . $familyData["surname"]; ?></a>
													                            <a id="<?php echo $familyData['id_relation_table']; ?>" data-item-id="<?php echo $familyData['id_relation']; ?>" data-family-relation="grandson" class="btn btn-xs delete-family-relation-tag" data-toggle="tooltip" title="Eliminar">
													                                <i class="fas fa-times-circle"></i>
													                            </a>
													                        </div>

																			<?php

																		}

																	}

																?>

															</div>

														</div>
														
													</div>

													<?php
													
													}

												?>

												<!-- /.Grandson tags on edit mode -->


											<!-- end of -->

											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">

													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															¿hay libros del sobreviviente?

														</h3>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Libro/s:</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<select class="col-12 book-selector selector-for-update" name="states[]" multiple="multiple">

														<?php 

															foreach($books as $key){

																if(!isset($booksAssignedToSurvivor[$key->id])){

																	?>

																	<option value="<?php echo $key->id; ?>"><?php echo $key->name; ?></option>

																	<?php

																}

															}

														?>

													</select>

												</div>

												<?php if($editMode){  ?>

													<div class=""> 

														<button data-id="<?php echo $actualId;?>" data-action="books" class="select-update-btn btn btn-default" type="button">actualizar</button>

													</div>

												<?php }?>	

											</div>



											<!-- repeater! -->

											<div id="m_repeater_4"> 

												<div class="form-group  m-form__group row" id="m_repeater_4">

													<label class="col-form-label col-lg-3 col-sm-12">Añadir libro:

														<br><small>¿No está en la lista? agregalo</small>

													</label>

													<div data-repeater-list="" class="col-lg-8 col-md-9 col-sm-12">

														<div data-repeater-item="" class="book-name-repeater my-repeater-item form-group m-form__group row align-items-center" style="padding-left:0px !important; padding-right: 0px !important"> 

															<div class="col-md-6">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name="new_book_name" type="text" class="form-control m-input new_book_name" placeholder="nombre" holder="Nombre"> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															
															<div class="col-md-1">

																<?php if(!$editMode){?>

																	<div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">

																		<span>

																			<i class="la la-trash-o"></i>

																			<span>Eliminar</span>

																		</span>

																	</div>

																<?php }else{ ?>

																	<div class="btn-add-in-edition btn-add-in-edition-book btn-sm btn btn-info m-btn m-btn--icon m-btn--pill">

																		<span>

																			<i class="la la-plus"></i>

																			<span>Agregar</span>

																		</span>

																	</div>


																<?php } ?>

															</div>

														</div></div>                 

													</div>

													<?php if(!$editMode){ ?>

														<div class="m-form__group form-group row">

															<label class="col-form-label col-lg-3 col-sm-12"></label>

															<div class="col-lg-4">

																<div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">

																	<span>

																		<i class="la la-plus"></i>

																		<span>Agregar otro</span>

																	</span>

																</div>

															</div>                                        

														</div>

													<?php } ?>

												</div>


												<!-- Books tags on edit mode... -->

												<?php

													if($editMode){

													?>

														<div class="form-group m-form__group row pt-4">

														<label class="control-label col-md-3"></label>

														<div class="col-md-9">
															
															<div class="books-tags">
																
																<?php

																	foreach($booksList as $key => $bookData){

																		
																		?>

																		<div class="btn-group">
												                            <a  href="#" role="button" class="btn btn-default btn-xs disabled" aria-disabled="true"><?php echo $bookData['name']; ?></a>
												                            <a id="<?php echo $bookData['idRelation']; ?>" data-item-id="<?php echo $bookData['idBook']; ?>" class="btn btn-xs delete-book-relation-tag" data-toggle="tooltip" title="Eliminar">
												                                <i class="fas fa-times-circle"></i>
												                            </a>
												                        </div>

																		<?php


																	}

																?>

															</div>

														</div>
														
													</div>

													<?php
													
													}

												?>

												<!-- /.Books tags on edit mode -->


											<!-- end of -->

											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">

													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															¿hay películas del sobreviviente?

														</h3>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Películas/s:</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<select class="col-12 film-selector selector-for-update" name="states[]" multiple="multiple">

														<?php

														foreach($films as $key){

															if(!isset($filmsAssignedToSurvivor[$key->id])){

																?>
																
																	<option value="<?php echo $key->id; ?>"><?php echo $key->name; ?></option>

																<?php 

															}

														}

														?>

													</select>

												</div>

												<?php if($editMode){  ?>

													<div class=""> 

														<button data-id="<?php echo $actualId;?>" data-action="films" class="select-update-btn btn btn-default" type="button">actualizar</button>

													</div>

												<?php }?>

											</div>

											<!-- repeater! -->

											<div id="m_repeater_5"> 

												<div class="form-group  m-form__group row" id="m_repeater_5">

													<label class="col-form-label col-lg-3 col-sm-12">Añadir película:

														<br><small>¿No está en la lista? agregalo</small>

													</label>

													<div data-repeater-list="" class="col-lg-8 col-md-9 col-sm-12">

														<div data-repeater-item="" class="film-repeater-item form-group m-form__group row align-items-center" style="padding-left:0px !important; padding-right: 0px !important"> 

															<div class="col-md-6">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name="new_film_name" type="text" class="form-control m-input new_film_name" placeholder="nombre" holder="Nombre"> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															
															<div class="col-md-1">

																<?php if(!$editMode){?>

																	<div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">

																		<span>

																			<i class="la la-trash-o"></i>

																			<span>Eliminar</span>

																		</span>

																	</div>

																<?php }else{ ?>

																	<div class="btn-add-in-edition btn-add-in-edition-film btn-sm btn btn-info m-btn m-btn--icon m-btn--pill">

																		<span>

																			<i class="la la-plus"></i>

																			<span>Agrregar</span>

																		</span>

																	</div>


																<?php } ?>

															</div>

														</div>
													</div>                 

													</div>

													<?php if(!$editMode){ ?>

														<div class="m-form__group form-group row">

															<label class="col-form-label col-lg-3 col-sm-12"></label>

															<div class="col-lg-4">

																<div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">

																	<span>

																		<i class="la la-plus"></i>

																		<span>Agregar otro</span>

																	</span>

																</div>

															</div>                                        

														</div>

													<?php } ?>

												</div>


												<!-- Films tags on edit mode... -->

												<?php

													if($editMode){

													?>

														<div class="form-group m-form__group row pt-4">

														<label class="control-label col-md-3"></label>

														<div class="col-md-9">
															
															<div class="films-tags">
																
																<?php

																	foreach($filmsList as $key => $filmData){

																		?>

																		<div class="btn-group">
												                            <a  href="#" role="button" class="btn btn-default btn-xs disabled" aria-disabled="true"><?php echo $filmData['name']; ?></a>
												                            <a id="<?php echo $filmData['idRelation']; ?>" data-item-id="<?php echo $filmData['idFilm']; ?>" class="btn btn-xs delete-film-relation-tag" data-toggle="tooltip" title="Eliminar">
												                                <i class="fas fa-times-circle"></i>
												                            </a>
												                        </div>

																		<?php

																	}

																?>


															</div>

														</div>
														
													</div>

													<?php
													
													}

												?>

												<!-- /.Films tags on edit mode -->


											<!-- end of -->


											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">

													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															¿Tiene reconocimientos?

														</h3>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

											<label class="col-form-label col-lg-3 col-sm-12">reconcimiento/s:</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<select class="col-12 honor-selector selector-for-update" name="states[]" multiple="multiple">

														
														<?php


														foreach($honors as $key){


															if(!isset($honorsAssignedToSurvivor[$key->id]))
															{

															?>

																<option value="<?php echo $key->id; ?>"><?php echo $key->name; ?></option>


															<?php

															}

														}


														?>


													</select>

												</div>

												<?php if($editMode){  ?>

												<div class=""> 

													<button data-id="<?php echo $actualId;?>" data-action="honors" class="select-update-btn btn btn-default" type="button">actualizar</button>

												</div>

												<?php }?>

											</div>

											<!-- repeater! -->

											<div id="m_repeater_6"> 

												<div class="form-group  m-form__group row" id="m_repeater_6">

													<label class="col-form-label col-lg-3 col-sm-12">Añadir reconocimiento:

														<br><small>¿No está en la lista? agregalo</small>

													</label>

													<div data-repeater-list="" class="col-lg-8 col-md-9 col-sm-12">

														<div data-repeater-item="" class="honor-repeater-item form-group m-form__group row align-items-center" style="padding-left:0px !important; padding-right: 0px !important"> 

															<div class="col-md-6">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name="new_honor_name" type="text" class="form-control m-input new_honor_name" placeholder="nombre" holder="Nombre"> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															
															<div class="col-md-1">

																<?php if(!$editMode){?>

																	<div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">

																		<span>

																			<i class="la la-trash-o"></i>

																			<span>Eliminar</span>

																		</span>

																	</div>

																<?php }else{ ?>

																	<div class="btn-add-in-edition btn-add-in-edition-honor btn-sm btn btn-info m-btn m-btn--icon m-btn--pill">

																		<span>

																			<i class="la la-plus"></i>

																			<span>Agregar</span>

																		</span>

																	</div>


																<?php } ?>

															</div>

														</div>

													</div>                 

													</div>


													<?php if(!$editMode){ ?>

														<div class="m-form__group form-group row">

															<label class="col-form-label col-lg-3 col-sm-12"></label>

															<div class="col-lg-4">

																<div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">

																	<span>

																		<i class="la la-plus"></i>

																		<span>Agregar otro</span>

																	</span>

																</div>

															</div>                                        

														</div>

													<?php } ?>

												</div>


												<!-- Honors tags on edit mode... -->

												<?php

													if($editMode){

													?>

														<div class="form-group m-form__group row pt-4">

														<label class="control-label col-md-3"></label>

														<div class="col-md-9">
															
															<div class="honors-tags">
																
																<?php

																	foreach($honorsList as $key => $honorData){

																		
																		?>

																		<div class="btn-group">
												                            <a  href="#" role="button" class="btn btn-default btn-xs disabled" aria-disabled="true">
												                            
												                            	<?php

																					echo $honorData['name'];

																				?>

												                        	</a>
												                            <a id="<?php echo $honorData['idRelation']; ?>"  class="btn btn-xs delete-honor-relation-tag" data-toggle="tooltip" title="Eliminar">
												                                <i class="fas fa-times-circle"></i>
												                            </a>
												                        </div>

																		<?php


																	}

																?>

															</div>

														</div>
														
													</div>

													<?php
													
													}

												?>

												<!-- /.Honors tags on edit mode -->

												<!-- end of -->


											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">

													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															Su BIO, notas, texto libre

														</h3>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Desarrollo de texto:*<br><small>Texto libre, sin máximo de caracteres.</small></label>

												<div class="col-lg-6 col-md-9 col-sm-12">	

													<textarea numlines="20" type="text" class="form-control m-input description" id="bio" name="bio" placeholder=""><?php echo trim($bio); ?></textarea>	


													<?php if($editMode){  ?>

														<br>

														<div class=""> 

															<button data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>	

												</div>

											</div>

											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">

													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															Su foto de perfil

														</h3>

													</div>

												</div>

											</div>

											
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Imagen de Perfil*:
													<br>
													<small>Es la imagen de perfil. Formatos válidos: jpg, jpeg, tiff, bmp, png<br></small>

													<?php if($editMode){ ?>
														<br />
														<small data-toggle="tooltip" data-placement="bottom" title="Una vez seleccionada la imagen, apretar en 'subir archivo' para guardarla correctamente">
															Como actualizar la imagen?
														</small>

													<?php }else{

														?>

														<small data-toggle="tooltip" data-placement="bottom" title="Porque al partir de una imagen grande, nuestros servidores pueden resamplearla a diversas resoluciones sin pérdida de calidad. El tamaño máximo es de 20 MB.">
															¿por qué este tamaño?
														</small>

														<?php
													}

													?>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input id="picture" name="picture" type="file">

												</div>

											</div>
											
											<!--
											<div class="tab-pane <?php if($actualTab =='images-tab') echo 'active';?>" id="images-tab">
												
											<div class="mt-5 mb-5 col-lg-12">

												<input type="file" id="images-batch2" name="images-batch[]" accept="image/*" multiple>

											</div>
											
											
											</div>
											-->

										

											<div class="m-portlet__foot m-portlet__foot--fit">

												<div class="m-form__actions">

												<div class="row">

													<div class="col-2"></div>

													<div class="col-7">

														<input type="hidden" name="token" value="<?php echo Token::generate()?>">

														<?php

															$hiddenClass = ($editMode) ? 'd-none' : '';

														?>

														<button id="add_new_survivor" type="submit" class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar Sobreviviente</button>
															
														&nbsp;&nbsp;

														<?php

															$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

															$buttonId =

															(!$editMode) ? 'exit_from_form' : 'back_to_list';

														?>

														<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

															<?php echo $buttonLabel;?>

														</button>

														</div>

													</div>

												</div>

											</div>

										</form>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="assets/app/js/museum/app_survivors_project/museum-app-survivors-project-add.js" type="text/javascript"></script>


</body>

</html>
