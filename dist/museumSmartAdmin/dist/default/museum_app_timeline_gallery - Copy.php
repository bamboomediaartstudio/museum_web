<?php

header("Cache-Control: private, must-revalidate, max-age=0, no-store, no-cache, must-revalidate, post-check=0, pre-check=0");

//-----------------------------------------------------------------------------
//init main includes...

$config = require_once('private/config.php');

require_once($config['general-urls']['DBConnect']);

//require_once($config['lib-lincludes']['aditivo-libs'] . 'KeepMeLoggedIn.php');

//require_once($config['url-includes']['keep-master-login']);

if(isset($_GET['idEvent']) && !empty($_GET['idEvent'])){

    $idEvent = $_GET['idEvent'];

    $editMode = true;

    $sql = "SELECT title FROM timeline_events WHERE id = ?";

    $stmt = $conn->prepare($sql);

    $stmt->bind_param('i', $idEvent);

    $stmt->execute();

    $stmt->store_result();

    $stmt->bind_result($eventName);

    $stmt->fetch();

}

//.....

$sql = "SELECT id, id_file, extension, item_type FROM timeline_event_images WHERE id_event = ? AND deleted = ? ORDER BY item_order";

$stmt = $conn->prepare($sql);

$deleted = 0;

$stmt->bind_param('ii', $idEvent, $deleted);

$stmt->execute();

$stmt->store_result();

$stmt->bind_result($idImage, $idFile, $extension, $itemType);

$total = $stmt->num_rows;

//...

$sql = "SELECT id, type FROM timeline_event_image_type WHERE active = ? AND deleted = ?";

$timelineTypeStmt = $conn->prepare($sql);

$active = 1;

$deleted = 0;

$timelineTypeStmt->bind_param('ii', $active, $deleted);

$timelineTypeStmt->execute();

$timelineTypeStmt->store_result();

$timelineTypeStmt->bind_result($timelineTypeId, $timelineTypeString);

?>



<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8" />

    <title>Museo de la Shoá | Timeline | Galería</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta content="width=device-width, initial-scale=1" name="viewport" />

    <meta content="admin para administradores..." name="description" />

    <meta content="Mariano Makedonsky" name="author" />

    <meta http-equiv="Cache-control" content="no-cache">

    <meta http-equiv="Cache-control" content="no-store">

    <meta http-equiv="Expires" content="-1">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet" type="text/css" />

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap2/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css" />
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" type="text/css" />

    <link href="compatibility/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

        <div class="clearfix"> </div>

        <div class="page-container">
            
            <div class="page-content-wrapper">

                <div class="page-content">

                    <div class="row users-list">

                        <div class="col-md-12">

                            <div class="portlet light bordered">

                                <div class="portlet-title">

                                 <div class="caption">

                                    <span class="caption-subject sbold uppercase">

                                        <?php 

                                        if($stmt->num_rows == 0){

                                            echo 'OPSS! AUN NO HAY IMÁGENES / VIDEOS.';

                                        }else{

                                            echo 'EDITAR IMÁGENES / VIDEOS';

                                        }

                                        ?>

                                    </span><br></br>

                                    <h5 class="small">

                                        <?php 

                                        if($stmt->num_rows == 0){

                                            echo 'Aun no hay imagenes.';

                                        }else{

                                            echo 'Este álbum pertenece al evento <b>' . $eventName . '.<br><br></b>Desde acá podés:<br><br> - Seleccionar la imagen principal.<br> - Eliminar imágenes/videos.<br> - Organizarla y asignar un orden al contenido.<br>-Escribir un pie/descripcion para cada imagen/video.';

                                        }

                                        ?>

                                    </h5>

                                    <h5 class="small">¿Necesitás agregar más imágenes/videos? hacé <b><a href='timeline_add.php?edit=&gotoGallery=true&true&id=<?php echo $idEvent;?>'

                                        >click acá.</a></b></h5>


                                    </div>


                                    <div class="tools"> </div>

                                </div>

                                <div id = "sortable" class="row">

                                    <?php

                                    if ($stmt->num_rows >= 1) {

                                        $counter = 1;

                                        while($stmt->fetch()){     

                                            $string = '<br>';

                                            $checkUniqueId = 'item_' . $counter;

                                            $counter+=1;

                                            $timelineTypeStmt->data_seek(0); 

                                            if($extension != 'mp4'){

                                                $image = '../timelineApp/bin/images/' . $idEvent . '/' . $idFile . '/' . $idFile . '_XSQ.' . $extension;

                                                $sourceType = '<img src="'. $image .'" />';


                                            }else{

                                                $image = '../timelineApp/bin/images/' . $idEvent . '/' . $idFile . '/' . $idFile . '.' . $extension;

                                                $sourceType = '<video  data-video-name='. $idFile .' id="player-'. $idImage  .'"  class="player" src="'. $image .'" width="250" height="200" controls></video>';
                                            }

                                            echo '<div id = "item_'. $idImage .'" class="general col-lg-3 col-md-4 col-sm-6 col-xs-12">

                                            <div class="portlet light portlet-fit bordered">

                                            <div class="portlet-body">

                                            <div class="mt-element-overlay">

                                            <div class="row">

                                            <div class="col-md-12">

                                            <div class="mt-overlay-1">' . $sourceType . ' 

                                            <div class="mt-overlay">

                                            <ul class="mt-info">

                                            <li class="do-delete">

                                            <a class="btn default btn-outline" href="javascript:;">

                                            <i class="icon-trash"></i>

                                            </a>

                                            </li>

                                            <li class="do-description">

                                            <a class="btn default btn-outline" href="javascript:;">

                                            <i class="icon-pencil"></i>

                                            </a>

                                            </li>';

                                            if($extension == 'mp4'){

                                                echo '<li class="do-play-video">

                                                <a class="btn default btn-outline" href="javascript:;">

                                                <i class="icon-control-play"></i>

                                                </a>

                                                </li>';

                                            }


                                            echo '</ul>

                                            </div>

                                            </div>

                                            </div>

                                            </div>

                                            </div>';

                                            if($extension == 'mp4'){ 

                                                echo '<button type="button" class="btn-get-thumb btn btn-success" style="width:100% !important">Capturar Miniatura</button>';

                                            }

                                            echo '<div class="mt-radio-list">';
                                            
                                            while($timelineTypeStmt->fetch()){

                                                $selectedString = '';

                                                $tempId = $idImage .  "_id_" . $timelineTypeId;

                                                ($itemType == $timelineTypeId) ? $selectedString = "checked" :  $selectedString = "";

                                                $string .= '<label for='.$tempId.'>

                                                <input type="radio" id="'.$tempId.'" name="'.$idImage.'" 

                                                value=" '. $timelineTypeId . ' " ' . $selectedString .'>'.$timelineTypeString.'</label>';
                                            }

                                            echo $string; 

                                            echo '</div></div></div></div>';

                                        }

                                    }

                                    ?>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <?php require_once($config['url-includes']['footer']); ?>

            <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>

            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js" type="text/javascript"></script>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js" type="text/javascript"></script>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" type="text/javascript"></script>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js" type="text/javascript"></script>

            <script type="text/javascript"> 

                var idEvent = <?php echo json_encode($idEvent); ?>; 

            </script>

            <script src="assets/app/js/museum/app_timeline/museum-app-timeline-gallery.js" type="text/javascript"></script>

        </body>

        </html>