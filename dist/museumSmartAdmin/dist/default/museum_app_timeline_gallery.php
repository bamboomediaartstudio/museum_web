<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

//$config = require_once('private/config.php');

require_once 'private/users/museum/apps/timeline/connect.php';

//require_once($config['general-urls']['DBConnect']);

//hacky...

if(isset($_GET['idEvent']) && !empty($_GET['idEvent'])){

    $idEvent = $_GET['idEvent'];

    $editMode = true;

    $sql = "SELECT title FROM timeline_events WHERE id = ?";

    $stmt = $conn->prepare($sql);

    $stmt->bind_param('i', $idEvent);

    $stmt->execute();

    $stmt->store_result();

    $stmt->bind_result($eventName);

    $stmt->fetch();

}

//...

$sql = "SELECT id, id_file, extension, item_type FROM timeline_event_images WHERE id_event = ? AND deleted = ? ORDER BY item_order";

$stmt = $conn->prepare($sql);

$deleted = 0;

$stmt->bind_param('ii', $idEvent, $deleted);

$stmt->execute();

$stmt->store_result();

$stmt->bind_result($idImage, $idFile, $extension, $itemType);

$total = $stmt->num_rows;

//...

$sql = "SELECT id, type FROM timeline_event_image_type WHERE active = ? AND deleted = ?";

$timelineTypeStmt = $conn->prepare($sql);

$active = 1;

$deleted = 0;

$timelineTypeStmt->bind_param('ii', $active, $deleted);

$timelineTypeStmt->execute();

$timelineTypeStmt->store_result();

$timelineTypeStmt->bind_result($timelineTypeId, $timelineTypeString);


/**
** Page General Data
** Set
*/

$page_title = "Museo de la Shoá | Timeline | Galería";

$page_subtitle_h3 = "Línea de tiempo";

$page_subtitle_li = "Listado de Eventos.";

$page_subtitle_i = "eventos";

$page_header_note = 'Esta página incluye los <strong>eventos</strong> de la linea de tiempo museo. Podés cambiar la visibilidad de los mismos a través del botón de <strong>Estado</strong>. También podés <i class="la la-edit"></i><strong>editar</strong> y <i class="la la-trash"></i><strong>eliminar</strong> cada Pelicula.<br><br>¿Querés agregar una pelicula nueva?<br><br>';

$page_header_href = 'museum_app_timeline_add.php';

$page_code_url = "assets/app/js/museum/app_timeline/museum-app-timeline-gallery.js";

?>


<!DOCTYPE html>

<html lang="en" >

<head>

    <meta charset="utf-8" />

    <title><?php echo $page_title; ?></title>

    <meta name="description" content="User profile example page">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

    <script>

        WebFont.load({

            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

            active: function() {

                sessionStorage.fonts = true;

            }

        });

    </script>

    <link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

    <link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />


    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css" />
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" type="text/css" />

    <link href="compatibility/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />


</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

    <div class="m-grid m-grid--hor m-grid--root m-page">

        <?php require_once 'private/includes/header.php'; ?>

        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

            <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

                <i class="la la-close"></i>

            </button>

            <?php require_once 'private/includes/sidebar.php'; ?>

            <div class="m-grid__item m-grid__item--fluid m-wrapper">

                <div class="m-content">

                    <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">

                        <div class="page-container">

                            <div class="portlet light bordered">

                                <div class="portlet-title">

                                    <div class="caption">

                                        <span class="caption-subject sbold uppercase">

                                            <?php 

                                            if($stmt->num_rows == 0){

                                                echo 'OPSS! AUN NO HAY IMÁGENES / VIDEOS.';

                                            }else{

                                                echo 'EDITAR IMÁGENES / VIDEOS';

                                            }

                                            ?>

                                        </span>

                                        <br>

                                    </br>

                                    <h5 class="small">

                                        <?php 

                                        if($stmt->num_rows == 0){

                                            echo 'Aun no hay imagenes.';

                                        }else{

                                            echo 'Este álbum pertenece al evento <b>' . $eventName . '.<br><br></b>Desde acá podés:<br><br> - Seleccionar la imagen principal.<br> - Eliminar imágenes/videos.<br> - Organizarla y asignar un orden al contenido.<br>-Escribir un pie/descripcion para cada imagen/video.';

                                        }

                                        ?>

                                    </h5>

                                    <h5 class="small">¿Necesitás agregar más imágenes/videos? hacé <b><a href='museum_app_timeline_add.php?edit=&gotoGallery=true&true&id=<?php echo $idEvent;?>'
                                        >click acá.</a></b></h5>

                                    </div>

                                    <div class="tools"> </div>

                                </div>

                                <div id = "sortable" class="row">

                                    <?php

                                    if ($stmt->num_rows >= 1) {

                                        $counter = 1;

                                        while($stmt->fetch()){     

                                            $string = '<br>';

                                            $checkUniqueId = 'item_' . $counter;

                                            $counter+=1;

                                            $timelineTypeStmt->data_seek(0); 

                                            if($extension != 'mp4'){

                                                //$image = '../timelineApp/bin/images/' . $idEvent . '/' . $idFile . '/' . $idFile . '_XSQ.' . $extension;
                                                $image = 'private/sources/images/timeline/' . $idEvent . '/' . $idFile . '/' . $idFile . '_XSQ.' . $extension;

                                                $sourceType = '<img src="'. $image .'" />';


                                            }else{

                                                $image = 'private/sources/images/timeline/' . $idEvent . '/' . $idFile . '/' . $idFile . '.' . $extension;

                                                $sourceType = '<video  data-video-name='. $idFile .' id="player-'. $idImage  .'"  class="player" src="'. $image .'" width="250" height="200" controls></video>';
                                            }

                                            echo '<div id = "item_'. $idImage .'" class="general col-lg-3 col-md-4 col-sm-6 col-xs-12">

                                            <div class="portlet light portlet-fit bordered">

                                            <div class="portlet-body">

                                            <div class="mt-element-overlay">

                                            <div class="row">

                                            <div class="col-md-12">

                                            <div class="mt-overlay-1">' . $sourceType . ' 

                                            <div class="mt-overlay">

                                            <ul class="mt-info">

                                            <li class="do-delete">

                                            <a class="btn default btn-outline" href="javascript:;">

                                            <i class="icon-trash"></i>

                                            </a>

                                            </li>

                                            <li class="do-description">

                                            <a class="btn default btn-outline" href="javascript:;">

                                            <i class="icon-pencil"></i>

                                            </a>

                                            </li>';

                                            if($extension == 'mp4'){

                                                echo '<li class="do-play-video">

                                                <a class="btn default btn-outline" href="javascript:;">

                                                <i class="icon-control-play"></i>

                                                </a>

                                                </li>';

                                            }


                                            echo '</ul>

                                            </div>

                                            </div>

                                            </div>

                                            </div>

                                            </div>';

                                            if($extension == 'mp4'){ 

                                                echo '<button type="button" class="btn-get-thumb btn btn-success" style="width:100% !important">Capturar Miniatura</button>';

                                            }

                                            echo '<div class="mt-radio-list">';
                                            
                                            while($timelineTypeStmt->fetch()){

                                                $selectedString = '';

                                                $tempId = $idImage .  "_id_" . $timelineTypeId;

                                                ($itemType == $timelineTypeId) ? $selectedString = "checked" :  $selectedString = "";

                                                $string .= '<label for='.$tempId.'>

                                                <input type="radio" id="'.$tempId.'" name="'.$idImage.'" 

                                                value=" '. $timelineTypeId . ' " ' . $selectedString .'>'.$timelineTypeString.'</label>';
                                            }

                                            echo $string; 

                                            echo '</div></div></div></div>';

                                        }

                                    }

                                    ?>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <?php require_once 'private/includes/footer.php'; ?>

        <div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">

            <i class="la la-arrow-up"></i>

        </div>

       <script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

        <script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js" type="text/javascript"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" type="text/javascript"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js" type="text/javascript"></script>

        <script type="text/javascript">  var idEvent = <?php echo json_encode($idEvent); ?>;  </script>

        <script src="assets/app/js/helper.js" type="text/javascript"></script>

        <script src="<?php echo $page_code_url;?>" type="text/javascript"></script>

    </body>

 </html>
