<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$actualId = Input::get('id');

$actualTab = 'general-tab';

if(Input::exists('get') && Input::get('tab')){

	$actualTab = Input::get('tab');

	//echo 'existe';

}

//echo $actualTab;

//return;

if(Input::exists("get") && Input::get('id')){

	$editMode = true;

	$mainQuery = DB::getInstance()->query('SELECT * FROM  museum_books  

		WHERE id=? AND deleted = ?', [Input::get('id'), 0]);

	$actualObject = $mainQuery->first();

	$objectTitle = $actualObject->title;
	
	$objectAuthor = $actualObject->author;

	$objectIsbn = $actualObject->isbn;

	$objectPublisher = $actualObject->publisher;

	$objectPublished = $actualObject->published;
	
	$objectPages = $actualObject->pages;
	
	$objectWebsite = $actualObject->website;

	$objectEdition = $actualObject->edition;

	$objectLanguage = $actualObject->language;

	$objectPrice = $actualObject->language;

	$objectUrl = $actualObject->url;

	$objectDescription = $actualObject->description;

	$allowShare = ($actualObject->allow_share == 1) ? 'checked' : '';

	$isHighlighted = ($actualObject->is_highlighted == 1) ? 'checked' : '';

	$isOnSale = ($actualObject->is_on_sale == 1) ? 'checked' : '';

	//image!

	$imgQuery = DB::getInstance()->query('SELECT * FROM 

		museum_images WHERE sid = ? AND source = ? AND deleted = ?', [$actualId, 'book', 0]);

	if($imgQuery->count()>0){

		$courseImage = $imgQuery->first();

		$mimeType = MimeTypes::getExtensionByMimeType($courseImage->mimetype);

		$imagePath = 'private/sources/images/books/' . $actualId . '/' . $courseImage->unique_id . '/' . $actualId . '_' . $courseImage->unique_id . '_original.' . $mimeType;

	}else{

		$imagePath = "";

	}

}else{

	$editMode = false;

	$objectPrice = '';

	$objectTitle = "";

	$objectAuthor = '';

	$objectUrl = '';

	$objectIsbn = '';

	$objectPublisher = '';

	$objectPublished = '';

	$objectPages = '';

	$objectWebsite = '';

	$objectEdition = '';

	$objectLanguage = '';

	$objectDescription = '';


	$isHighlighted = '';

	$isOnSale = 'checked';

	$allowShare = 'checked';
}

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Libros | Agregar Libro</title>

	<meta name="description" content="add new book">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{
		
		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	#map {

		height: 500px;

		width:100%;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}

</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div id="myModal" class="modal" tabindex="-1" role="dialog">
		
		<div class="modal-dialog" role="document">
			
			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title">Asignar descripción a la imagen</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body">

					<form>

						<div class="form-group">

							<label for="image-	" class="col-form-label">Descripción:</label>

							<input type="text" class="form-control" id="image-description">

						</div>
						
					</form>

				</div>

				<div class="modal-footer">

					<button type="button" class="save-description btn btn-primary">guardar</button>

					<button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<input type="hidden" id="image-path" name="image-path" value="<?php echo $imagePath;?>">

		<input type="hidden" id="book-id" name="book-id" value="<?php echo $actualId;?>">

		<input type="hidden" id="book-url" name="book-url" value="<?php echo $objectUrl;?>">
		
		<input type="hidden" id="book-name" name="book-name" value="<?php echo $objectTitle;?>">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Libros</h3>		
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>
								
								<li class="m-nav__separator">-</li>
								
								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text">Agregar Libro</span>

									</a>

								</li>
								
							</ul>

						</div>
						
					</div>

				</div>
				
				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link <?php if($actualTab =='general-tab') echo 'active';?>" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Información general

												</a>

											</li>

											<li class="nav-item m-tabs__item <?php if(!$editMode) echo 'disabled'; ?>"> 
												<a class="<?php if($actualTab =='images-tab') echo 'active';?> tab-navigation images-tab nav-link m-tabs__link" data-toggle="tab" href="#images-tab" role="tab">

													Imágenes

												</a>

											</li>

											<li class="nav-item m-tabs__item <?php if(!$editMode) echo 'disabled'; ?>"> 
												<a class="<?php if($actualTab =='videos-tab') echo 'active';?> tab-navigation videos-tab nav-link m-tabs__link" data-toggle="tab" href="#videos-tab" role="tab">

													Videos

												</a>

											</li>

										</ul>

									</div>

								</div>

								
								<div class="tab-content">

									<div class="tab-pane <?php if($actualTab =='videos-tab') echo 'active';?>" id="videos-tab">

										<div class="mt-5 mb-5 col-lg-12">

											<form id="youtube-form">

												<div class="form-group">


													<div class="form-group m-form__group row">

														<label class="col-form-label col-lg-3 col-sm-12">Link al video*:</label>

														<div class="col-lg-4 col-md-9 col-sm-12">

															<input id="youtube_id" type="text" class="form-control m-input" name="youtube_id" placeholder="URL del video" value="">

															<small class="yt-info m-form__help">Copiá y pegá acá la URL del video de <b>YouTube.</b></small>

															<div id="previews">

																<div id="preview-1"></div>

																<div id="preview-2"></div>

																<div id="preview-3"></div>


															</div>

															<input type="hidden" id="hidden-yt-id" name="hidden-yt-id" value="">

														</div>



														<div class=""> 

															<button data-db-value="" data-id="1" class="add-video-btn btn btn-default" type="submit">agregar

															</button>

														</div>


													</div>

												</div>

											</form>

											<input type="file" id="videos-batch" name="videos-batch[]" accept="image/*" multiple>

										</div>										
									</div>

									<div class="tab-pane <?php if($actualTab =='images-tab') echo 'active';?>" id="images-tab">

										<div class="mt-5 mb-5 col-lg-12">

											<input type="file" id="images-batch" name="images-batch[]" accept="image/*" multiple>

										</div>										
									</div>
									
									
									<div class="tab-pane <?php if($actualTab =='general-tab') echo 'active';?>" id="general-tab">

										<form id="add-data-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>	

												</div>

											</div>

											<!-- titulo de la noticia-->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Nombre del libro*:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="title" type="text" class="form-control m-input" name="title" placeholder="Título" value="<?php echo $objectTitle;?>">

													<?php if(!$editMode){ ?>

														<div class="">
															<button id="validate-title" class="btn  btn-info" type="button" disabled>Validar</button>
														</div>

													<?php } ?>

												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $objectTitle;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">URL*:<br>

													<small>Permalink. Así será la URL.
														<br>
														<small data-toggle="tooltip" data-placement="bottom" title="Por cuestiones de SEO (Search Engine Optimization) el permalink es el único campo que una vez que crees el contenido no vas a volver poder editar, por lo que te pedimos que le prestes especial atención al mismo.">
															¡Lee esto!
														</small>

													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-12">

													<input id="url" type="text" class="form-control m-input" name="url" placeholder="URL del libro" value="<?php echo $objectUrl;?>" readonly="readonly" disabled>

													<?php if($editMode){?>
														<small class='text-danger'>Este campo no se puede editar.</small>
													<?php }?>

												</div>

											</div>

											<!-- autor -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Autor:<br><small>Si conocés al autor del libro, podés agregarlo acá.</small></label>

												<div class="col-lg-4 col-12 input-group">

													<input id="author" type="text" class="form-control m-input" name="author" placeholder="Nombre del libro" value="<?php echo $objectAuthor;?>">

												
												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $objectAuthor;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<!-- isbn -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">ISBN:</small></label>

												<div class="col-lg-4 col-12 input-group">

													<input id="isbn" type="text" class="form-control m-input" name="isbn" placeholder="ISBN del libro" value="<?php echo $objectIsbn;?>">

												
												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $objectIsbn;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<!-- publisher -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Publisher / editorial:<br></label>

												<div class="col-lg-4 col-12 input-group">

													<input id="publisher" type="text" class="form-control m-input" name="publisher" placeholder="Editorial" value="<?php echo $objectPublisher;?>">

													
												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $objectPublisher;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<!-- publish year -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Año de publicación:<br></label>

												<div class="col-lg-4 col-12 input-group">

													<input id="published" type="number" class="form-control m-input" name="published" placeholder="ej: 1990" value="<?php echo $objectPublished;?>">

													
												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $objectPublished;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<!-- paginas -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Páginas:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="pages" type="number" class="form-control m-input" name="pages" placeholder="N de páginas" value="<?php echo $objectPages;?>">

													
												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $objectPages;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<!-- price -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Precio:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="price" type="number" class="form-control m-input" name="price" placeholder="Precio del libro" value="<?php echo $objectPrice;?>">

													
												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $objectPrice;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<!-- website -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">website:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="website" type="text" class="form-control m-input" name="website" placeholder="http://www" value="<?php echo $objectWebsite;?>">

													
												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $objectWebsite;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<!-- edicion -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Edición:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="edition" type="text" class="form-control m-input" name="edition" placeholder="ej: Primera edición" value="<?php echo $objectEdition;?>">

													
												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $objectEdition;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<!-- language -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Idioma:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="language" type="text" class="form-control m-input" name="language" placeholder="Ej: español / inglés" value="<?php echo $objectLanguage;?>">

													
												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $objectLanguage;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											
											<!-- description -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Descripción*:<br><small>Para insertar saltos de línea, hacelo presionando <b>Shift + Enter</b>.</small></label>

												<div class="col-lg-6 col-md-9 col-sm-12">	

													<textarea numlines="20" type="text" class="form-control m-input description" id="description" name="description" placeholder="Noticia"><?php echo trim($objectDescription); ?></textarea>	
													<span class="d-none summernote-description-error m-form__help">El cuerpo de la nota no puede quedar vacío.</span>

													<?php if($editMode){  ?>

														<br>

														<div class=""> 

															<button data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>	

												</div>

											</div>


											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Imagen de Portada*:
													<br><small>Es la imagen principal del libro y tiene que ser de 1000 x 1000 px.<br>

														<small data-toggle="tooltip" data-placement="bottom" title="Porque al partir de una imagen grande, nuestros servidores pueden resamplearla a diversas resoluciones sin pérdida de calidad. El tamaño máximo es de 20 MB.">
															¿por qué tan grande?
														</small>


													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input id="main-image" name="main-image" type="file">

												</div>

											</div>

											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">
													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															Generales

															<br>

														</h3>

													</div>

												</div>

											</div>

											<!-- allow share -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Permitir compartir:	

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input type="checkbox" <?php echo $allowShare;?>
																	class="allow_share toggler-info" 

																	name="allow_share" 

																	data-on="Enabled"

																	data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<!-- is highlighted? -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Libro destacado:

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Solamente destaca libros que tengan una trascendencia e importancia por fuera de lo común.">
														IMPORTANTE ¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input type="checkbox" <?php echo $isHighlighted;?>
																	class="highlighted toggler-info" 

																	name="is_highlighted" 

																	data-on="Enabled"

																	data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<!-- esta en el museo?? -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">¿Está a la venta?:
												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input type="checkbox" <?php echo $isOnSale;?>
																	class="highlighted toggler-info" 

																	name="is_on_sale" 

																	data-on="Enabled"

																	data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>



											<!-- ... -->

											<div class="m-portlet__foot m-portlet__foot--fit">

												<div class="m-form__actions">

													<div class="row">

														<div class="col-2"></div>

														<div class="col-7">

															<input type="hidden" name="token" value="<?php echo Token::generate()?>">

															<?php 

															$hiddenClass = ($editMode) ? 'd-none' : '';

															?>

															<button id="add_book" type="submit" 

															class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar Libro</button>
															&nbsp;&nbsp;

															<?php 

															$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

															$buttonId = 

															(!$editMode) ? 'exit_from_form' : 'back_to_list';

															?>

															<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																<?php echo $buttonLabel;?>

															</button>

														</div>

													</div>

												</div>

											</div>

										</form>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="assets/app/js/museum/books/museum-book-add.js" type="text/javascript"></script>

</body>

</html>
