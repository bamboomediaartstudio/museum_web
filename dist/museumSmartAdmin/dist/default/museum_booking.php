<?php

$origin = 'admin';

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Visitas | Editor de Visitas</title>

	<meta name="description" content="add new news">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{
		
		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}

	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	#map {

		height: 500px;

		width:100%;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}

	.see-event{
		color: white !important;
		background-color: green !important;
	}

	.delete-event{
		color: white !important;
		background-color: red !important;
	}

	.define-ammount{
		color: white  !important;
		background-color: #27c9cf !important;
	}

	.define-guide{
		color: white  !important;
		background-color: #8873a2 !important;
	}

	.change-event-place{
		color: white  !important;
		background-color: #8873a2 !important;
	}

	.change-event-visitors{

		color: white  !important;
		background-color: #27c9cf !important;

	}

	.change-tickets-per-visitors{

		color: white  !important;
		
		background-color: #a2c7b5 !important;
	}

	.is-online{

		color: white !important;

		background-color: #4ebfa9 !important;

	}

	.is-offline{

		color: white !important;

		background-color: #f35872 !important;

	}

	.change-notes{

		color: white !important;

		background-color: #f06200 !important;

	}


	.trash-for-events{

		background-color: #fff;

		height: 8em;

		box-shadow: 0px 1px 15px 1px rgba(69, 65, 78, 0.08);

		border: solid 1px #f5f5f5;

	}


	.date-selector-div{

		background-color: #999 !important;

		width: 100%;
	}


	.tickets-stepper{

		background-color: #999;

		color: white;
		
		width: 50px;
		
		border:1px solid #fff !important;
	}

	.selected-date{

		cursor: pointer !important;

		background-color: #274369 !important;
	}

	.fc-highlight{
		background-color: #ff0066 !important;
	}

	.fc-bgevent{
		background-color: #ff0066 !important;
	}



</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<?php include('../../../private/php/includes/modals/individual-reservation.php');?>

	<?php include('../../../private/php/includes/modals/group-reservation.php');?>


	<div id="myModal" class="modal" tabindex="-1" role="dialog">
		
		<div class="modal-dialog" role="document">
			
			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title">Asignar descripción a la imagen</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body">

					<form>

						<div class="form-group">

							<label for="image-	" class="col-form-label">Descripción:</label>

							<input type="text" class="form-control" id="image-description">

						</div>
						
					</form>

				</div>

				<div class="modal-footer">

					<button type="button" class="save-description btn btn-primary">guardar</button>

					<button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Visitas</h3>		
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>
								
								<li class="m-nav__separator">-</li>

								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text">Agregar y editar visitas</span>

									</a>

								</li>
								
							</ul>

						</div>
						
					</div>

				</div>
				
				<div class="m-content">
					<div class="row">
						<div class="col-lg-3">
							<!--begin::Portlet-->
							<div class="m-portlet" id="m_portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<span class="m-portlet__head-icon">
												<i class="flaticon-add"></i>
											</span>
											<h3 class="m-portlet__head-text">
												Eventos
											</h3>
										</div>
									</div>
								</div>
								<div class="m-portlet__body">

									<div id="m_calendar_external_events" class="fc-unthemed">

										<?php 

										$mainQuery = DB::getInstance()->query('SELECT * FROM museum_booking_event_type WHERE active = ? AND deleted = ?', [1, 0]);

										foreach($mainQuery->results() as $result){  ?>

											<div id="<?php echo $result->id;?>" 

												class='fc-event fc-event-external fc-start <?php echo $result->color;?> m--margin-bottom-15' 

												data-color="m-fc-event--primary" 

												date-name="<?php echo $result->type;?>" 

												data-id="<?php echo $result->id;?>"

												data-place="<?php echo $result->default_place;?>"

												data-start="<?php echo $result->defaut_date_start;?>" 

												data-end="<?php echo $result->defaut_date_end;?>"

												>

												<div class="fc-title">
													
													<div class="fc-content">
														<?php echo $result->type;?>

													</div>

												</div>

											</div>

										<?php } ?>

										<div class="m-separator m-separator--dashed m-separator--space"></div>

										<div class='trash-for-events m--margin-bottom-15' data-color="m-fc-event--danger">

											<div class="fc-title">

												<div class="fc-content text-center">

													<i class="text-danger fas fa-trash-alt mt-3"></i>
													
													<p class="ml-5 mr-5 text-center">Arrastrá y soltá acá el evento que quieras eliminar<p>

													</div>

												</div>

											</div>

											<div class="m-separator m-separator--dashed m-separator--space"></div>

											<div class="align-items-center">

												<button type="button" class="w-100 go-to-group-visit btn btn-success">añadir institución</button>

												<div class="m-separator m-separator--dashed m-separator--space"></div>

												<button type="button" class="w-100 go-to-individual-visit btn btn-danger">añadir visitanate</button>

											</div>

											
										</div>

									</div>

								</div>						

							</div>
							
							<div class="col-lg-9">

								<div class="m-portlet" id="m_portlet">

									<div class="m-portlet__head">

										<div class="m-portlet__head-caption">

											<div class="m-portlet__head-title">

												<span class="m-portlet__head-icon">

													<i class="flaticon-calendar-2"></i>

												</span>

												<h3 class="m-portlet__head-text">

													Calendario

												</h3>

											</div>

										</div>
										
									</div>

									<div class="m-portlet__body">

										<div id="m_calendar"></div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

			<?php require_once 'private/includes/footer.php'; ?>

		</div>

		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

		<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

		<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

		<script src="assets/app/js/helper.js" type="text/javascript"></script>

		<script src="assets/vendors/custom/jquery-ui/jquery-ui.bundle.js" type="text/javascript"></script>

		<!--<script src="assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>-->

		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/locale/es.js"></script>


		<script src="../../../private/vendor/gsap/src/minified/TweenMax.min.js"></script>

		<script src="../../../private/js/generalConf.min.js" type="text/javascript"></script>
		
		<script src="../../../private/js/helpers.min.js" type="text/javascript"></script>

		<script src="../../../private/js/booking.min.js" type="text/javascript"></script>

		<script src="assets/app/js/museum/booking/museum-booking.js" type="text/javascript"></script>


	</body>

	</html>
