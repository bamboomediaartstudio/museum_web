<?php

$origin = 'admin';

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

$yesterday = date('Y-m-d',strtotime("-1 days"));

$today = date('Y-m-d');

$tomorrow = date('Y-m-d',strtotime("+1 days"));

$dayAfterTomorrow = date('Y-m-d',strtotime("+2 days"));

if(Input::exists('get')){

	if(Input::get('tabId')){

		$tabId = Input::get('tabId');

	}else{

		$tabId = 1;

	}

}else{

	$tabId = 1;

}

//$tabId = 4;

$db = DB::getInstance();

$bookingTypeQuery = DB::getInstance()->query('SELECT * FROM museum_booking_event_type WHERE active = ? AND deleted = ?', [1, 0]);

if($bookingTypeQuery->count()>0) $bookingObject = $bookingTypeQuery->results();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Turnos | Lista</title>

	<meta name="description" content="add new news">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.css"/>


	<style>

		.summernote-description-error{

			color: red !important;

		}

		.museum-finder{

			cursor:pointer;

		}

		#editor {overflow:scroll; max-height:300px !important}

		input::-webkit-outer-spin-button,

		input::-webkit-inner-spin-button {

			-webkit-appearance: none;

			margin: 0;
		}

		.disabled {

			pointer-events: none !important;

			opacity: 0.4 !important;

		}

		#map {

			height: 500px;

			width:100%;

		}

		.tt-query {
			-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
			-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
			box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		}

		.tt-hint {
			color: #999
		}

		.tt-menu {    /* used to be tt-dropdown-menu in older versions */
			width: 422px;
			margin-top: 4px;
			padding: 4px 0;
			background-color: #fff;
			border: 1px solid #ccc;
			border: 1px solid rgba(0, 0, 0, 0.2);
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
			-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
			-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
			box-shadow: 0 5px 10px rgba(0,0,0,.2);
		}

		.tt-suggestion {
			padding: 3px 20px;
			line-height: 24px;
		}

		.tt-suggestion.tt-cursor,.tt-suggestion:hover {
			color: #fff;
			background-color: #0097cf;

		}

		.tt-suggestion p {
			margin: 0;
		}

		.twitter-typeahead{
			width: 100%;
		}

		.see-event{
			color: white !important;
			background-color: green !important;
		}

		.delete-event{
			color: white !important;
			background-color: red !important;
		}

		.define-ammount{
			color: white  !important;
			background-color: #27c9cf !important;
		}

		.define-guide{
			color: white  !important;
			background-color: #8873a2 !important;
		}

		.change-event-place{
			color: white  !important;
			background-color: #8873a2 !important;
		}

		.change-event-visitors{

			color: white  !important;
			background-color: #27c9cf !important;

		}

		.change-tickets-per-visitors{

			color: white  !important;

			background-color: #a2c7b5 !important;
		}

		.is-online{

			color: white !important;

			background-color: #4ebfa9 !important;

		}

		.is-offline{

			color: white !important;

			background-color: #f35872 !important;

		}

		.change-notes{

			color: white !important;

			background-color: #f06200 !important;

		}


		.trash-for-events{

			background-color: #fff;

			height: 8em;

			box-shadow: 0px 1px 15px 1px rgba(69, 65, 78, 0.08);

			border: solid 1px #f5f5f5;

		}


		.date-selector-div{

			background-color: #999 !important;

			width: 100%;
		}


		.tickets-stepper{

			background-color: #999;

			color: white;

			width: 50px;

			border:1px solid #fff !important;
		}

		.selected-date{

			cursor: pointer !important;

			background-color: #274369 !important;
		}

		.fc-highlight{
			background-color: #ff0066 !important;
		}

		.fc-bgevent{
			background-color: #ff0066 !important;
		}



	</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<input type="hidden" id="yesterday" name="yesterday" value="<?php echo $yesterday;?>">

	<input type="hidden" id="today" name="today" value="<?php echo $today;?>">
	
	<input type="hidden" id="tomorrow" name="tomorrow" value="<?php echo $tomorrow;?>">

	<input type="hidden" id="dayAfterTomorrow" name="dayAfterTomorrow" value="<?php echo $dayAfterTomorrow;?>">

	<div id="myModal" class="modal" tabindex="-1" role="dialog">
		
		<div class="modal-dialog" role="document">
			
			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title">Asignar descripción a la imagen</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body">

					<form>

						<div class="form-group">

							<label for="image-	" class="col-form-label">Descripción:</label>

							<input type="text" class="form-control" id="image-description">

						</div>
						
					</form>

				</div>

				<div class="modal-footer">

					<button type="button" class="save-description btn btn-primary">guardar</button>

					<button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Turnos del día</h3>		
							
						</div>
						
					</div>

				</div>
				
				<div class="m-content">
					
					<div class="row">

						<div class="col-lg-3">

							<div class="m-portlet" id="m_portlet">
								
								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">

										<div class="m-portlet__head-title">

											<span class="m-portlet__head-icon">

												<i class="flaticon-add"></i>

											</span>

											<h3 class="m-portlet__head-text">Filtar</h3>

										</div>

									</div>

								</div>
								
								<div class="m-portlet__body">

									<div id="m_calendar_external_events" class="fc-unthemed">

										<div class="form-group m-form__group row">

											<label class="col-form-label">Desde:

												<br>
											</label>

											<div class="col-12">

												<div id="date-from" class="input-group date">

													<input type="text" class="form-control" name="calendar-date">

													<div class="input-group-addon">

														<i class="pt-2 far fa-calendar-alt"></i>
														
													</div>
													
												</div>	

											</div>

										</div>
										
										<div class="m-separator m-separator--dashed m-separator--space"></div>

										<div class="align-items-center">

											<button type="button" class="w-100 go-to-today btn btn-success">hoy</button>

											<div class="m-separator m-separator--dashed m-separator--space"></div>

											<button type="button" class="w-100 go-to-tomorrow btn btn-danger">mañana</button>

											<div class="m-separator m-separator--dashed m-separator--space"></div>

											<button type="button" class="w-100 go-to-day-after-tomorrow btn btn-accent">pasado mañana</button>

										</div>


									</div>

								</div>

							</div>						

						</div>

						<div class="col-lg-9">

							<div class="m-portlet" id="m_portlet">

								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">

										<div class="m-portlet__head-title">

											<span class="m-portlet__head-icon">

												<i class="flaticon-calendar-2"></i>

											</span>

											<h3 class="m-portlet__head-text">

												Listados

											</h3>

										</div>

									</div>

									<div class="m-portlet__head-tools">

										<ul class="m-portlet__nav">
										
											<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
										
												<a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
													Acciones
												</a>
												<div class="m-dropdown__wrapper">
													
													<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 40.2222px;"></span>
													
													<div class="m-dropdown__inner">
														
														<div class="m-dropdown__body">
															
															<div class="m-dropdown__content">
																
																<ul class="m-nav">
																	
																	<li class="m-nav__item">
																		
																		<a href="museum_booking.php" class="m-nav__link">
																		
																			<i class="m-nav__link-icon flaticon-calendar"></i>
																			
																			<span class="m-nav__link-text">
																				
																				ir al calendario
																			
																			</span>
																		
																		</a>
																	
																	</li>
																	
																	<li class="m-nav__item">
																	
																		<a href="museum_booking_batch.php" class="m-nav__link">
																	
																			<i class="m-nav__link-icon flaticon-file"></i>
																	
																			<span class="m-nav__link-text">
																	
																				agregar en batch
																	
																			</span>
																	
																		</a>
																	
																	</li>
				
																</ul>
														
															</div>
												
														</div>
												
													</div>
									
												</div>
								
											</li>
								
										</ul>
								
									</div>

								</div>

								<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">

									<div class="m-portlet__head">

										<div class="m-portlet__head-tools">

											<ul id="myTab" class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--left m-tabs-line--primary" 
											role="tablist">

											<?php

											foreach($bookingObject as $categoryResult){

												$active = ($categoryResult->id == $tabId) ? 'active' : '';

												$divId = '#tab_' . $categoryResult->id;

												echo '<li class="nav-item m-tabs__item" data-id="'. $categoryResult->id .'">

												<a class="nav-link m-tabs__link '. $active .'" data-toggle="tab" href="'. $divId .'" role="tab">

												<i class="flaticon-share m--hide"></i>'.$categoryResult->type.'</a>

												</li>';

											}

											?>

										</ul>

									</div>

								</div>

								<div class="tab-content">

									<div class="tab-pane active" id="tab_1">

										<div class="m-portlet__body">

											<table id="tab-1" class="table display table-striped table-bordered responsive nowrap" style="width:100%">

												<thead>

													<tr>

														<th>turno</th>

														<th>Fecha</th>

														<th>Hora</th>

														<th>guía</th>

														<th>Acciones</th>

													</tr>

												</thead>

											</table>

										</div>

									</div>

									<div class="tab-pane" id="tab_2">

										<div class="m-portlet__body">

											<table id="tab-2" class="table display table-striped table-bordered responsive nowrap" style="width:100%" width="100%">

												<thead>

													<tr>

														<th>turno</th>

														<th>Fecha</th>

														<th>Hora</th>

														<th>guía</th>

														<th>Acciones</th>

													</tr>

												</thead>

											</table>

										</div>

									</div>

									<div class="tab-pane" id="tab_3">

										<div class="m-portlet__body">

											<table id="tab-3" class="table display table-striped table-bordered responsive nowrap" style="width:100%" width="100%">

												<thead>

													<tr>

														<th>turno</th>

														<th>Fecha</th>

														<th>Hora</th>

														<th>guía</th>

														<th>Acciones</th>

													</tr>

												</thead>

											</table>

										</div>

									</div>

									<div class="tab-pane" id="tab_4">

										<div class="m-portlet__body">

											<table id="tab-4" class="table display table-striped table-bordered responsive nowrap" style="width:100%" width="100%">

												<thead>

													<tr>

														<th>turno</th>

														<th>Fecha</th>

														<th>Hora</th>

														<th>guía</th>

														<th>Acciones</th>

													</tr>

												</thead>

											</table>

										</div>

									</div>

									<div class="tab-pane" id="tab_5">

										<div class="m-portlet__body">

											<table id="tab-5" class="table display table-striped table-bordered responsive nowrap" style="width:100%" width="100%">

												<thead>

													<tr>

														<th>turno</th>

														<th>Fecha</th>

														<th>Hora</th>

														<th>guía</th>

														<th>Acciones</th>

													</tr>

												</thead>

											</table>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/vendors/custom/jquery-ui/jquery-ui.bundle.js" type="text/javascript"></script>

<!--<script src="assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/locale/es.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.es.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>

<script src="../../../private/vendor/gsap/src/minified/TweenMax.min.js"></script>

<script src="../../../private/js/generalConf.min.js" type="text/javascript"></script>

<script src="../../../private/js/helpers.min.js" type="text/javascript"></script>

<script src="../../../private/js/booking.min.js" type="text/javascript"></script>

<script src="assets/app/js/museum/booking/museum-booking-list.js" type="text/javascript"></script>

</body>

</html>
