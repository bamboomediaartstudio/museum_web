<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$actualId = Input::get('id');

$actualTab = 'general-tab';

if(Input::exists('get') && Input::get('tab')){

	$actualTab = Input::get('tab');

}

$tutorsQuery = DB::getInstance()->query('SELECT * FROM museum_courses_tutors WHERE deleted = ?', [0]);

$tutors = $tutorsQuery->results();

$actualId = Input::get('id');

if(Input::exists("get") && Input::get('id')){

	$editMode = true;

	$actualTutorsQuery = DB::getInstance()->query('SELECT * FROM museum_courses_tutors_relations 

		WHERE id_course = ? AND deleted = ?', [Input::get('id'), 0]);

	$acualTutorsList = $actualTutorsQuery->results();



	$coursesQuery = DB::getInstance()->query('SELECT * FROM  museum_courses  

		WHERE id=? AND deleted = ?', [Input::get('id'), 0]);

	$actualCourse = $coursesQuery->first();

	$facebookGroup = $actualCourse->facebook_group;

	$whatsapp = $actualCourse->whatsapp;

	$courseName = $actualCourse->name;

	$email = $actualCourse->email;

	$observations = $actualCourse->observations;

	$phone = $actualCourse->phone;

	$courseUrl = $actualCourse->url;

	$courseDescription = $actualCourse->description;

	$duration = $actualCourse->duration;

	$allowProgramDownload = $actualCourse->allow_program_download;

	$showPrice = ($actualCourse->show_price == 1) ? 'checked' : '';

	$showDuration = ($actualCourse->show_duration == 1) ? 'checked' : '';

	$allowShare = ($actualCourse->allow_share == 1) ? 'checked' : '';

	$allowEnrollment = ($actualCourse->allow_enrollment == 1) ? 'checked' : '';

	$showAddress = ($actualCourse->show_address == 1) ? 'checked' : '';

	$coursePrice = $actualCourse->price;

	$classes = $actualCourse->number_of_classes;

	$startDate = $actualCourse->start_date;

	$endDate = $actualCourse->end_date;

	$address = $actualCourse->address;

	$lat = $actualCourse->lat;

	$long = $actualCourse->long;
	
	$priceInputStatus = ($showPrice != 'checked') ? 'disabled' : '';

	$durationInputStatus = ($showDuration != 'checked') ? 'disabled' : '';

	//course type relations...

	$modalitiesQuery = DB::getInstance()->query('SELECT * FROM 

		museum_courses_modalities_relations 

		WHERE id_course=? AND deleted = ?', [Input::get('id'), 0]);

	$courseModalitiesList = $modalitiesQuery->results();

	$courseModalitiesArray = [];

	foreach($courseModalitiesList as $cm){

		$courseModalitiesArray[] = $cm->id_modality;

	}

}else{

	$editMode = false;

	$allowProgramDownload = 0;

	$courseModalitiesArray = [];

	$observations = '';

	$email = '';

	$phone = '';

	$courseName = "";

	$courseUrl = '';

	$courseDescription = '';

	$showPrice = '';

	$coursePrice = '';

	$showDuration = '';

	$duration = '';

	$addressInputStatus = 'disabled';

	$priceInputStatus = 'disabled';

	$durationInputStatus = 'disabled';

	$startDate = '';

	$endDate = '';

	$facebookGroup = '';

	$allowEnrollment = 'checked';

	$allowShare = 'checked';

	$showAddress = 'checked';

	$address = ''; 

	$lat = '';
	
	$long = '';


}

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Cursos | Agregar Curso</title>

	<meta name="description" content="add new course">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{
		
		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	#map {

		height: 500px;

		width:100%;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}

</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div id="myModal" class="modal" tabindex="-1" role="dialog">
		
		<div class="modal-dialog" role="document">
			
			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title">Asignar descripción a la imagen</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body">

					<form>

						<div class="form-group">

							<label for="image-description" class="col-form-label">Descripción:</label>

							<input type="text" class="form-control" id="image-description">

						</div>
						
					</form>

				</div>

				<div class="modal-footer">

					<button type="button" class="save-description btn btn-primary">guardar</button>

					<button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<input type="hidden" id="course-id" name="course-id" value="<?php echo $actualId;?>">

		<input type="hidden" id="course-url" name="course-url" value="<?php echo $courseUrl;?>">
		
		<input type="hidden" id="course-name" name="course-name" value="<?php echo $courseName;?>">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">

								<?php echo ($editMode) ? 'editando: ' . $courseName : 'cursos';?>

							</h3>		
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>
								
								<li class="m-nav__separator">-</li>
								
								<li class="m-nav__item">

									<a href="museum_course_add.php" class="m-nav__link">

										<span class="m-nav__link-text">Agregar Curso</span>

									</a>

								</li> 

								<span> || </span>




								<li class="m-nav__item">

									<a href="museum_courses_list.php" class="m-nav__link">

										<span class="m-nav__link-text">Ver listado</span>

									</a>

								</li>
								
							</ul>

						</div>
						
					</div>

				</div>
				
				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link <?php if($actualTab =='general-tab') echo 'active';?>" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Información general

												</a>

											</li>

											<li class="nav-item m-tabs__item <?php if(!$editMode) echo 'disabled'; ?>"> 
												<a class="<?php if($actualTab =='images-tab') echo 'active';?> tab-navigation images-tab nav-link m-tabs__link" data-toggle="tab" href="#images-tab" role="tab">

													Imágenes

												</a>

											</li>

											<li class="nav-item m-tabs__item <?php if(!$editMode) echo 'disabled'; ?>"> 
												<a class="<?php if($actualTab =='videos-tab') echo 'active';?> tab-navigation videos-tab nav-link m-tabs__link" data-toggle="tab" href="#videos-tab" role="tab">

													Videos

												</a>

											</li>

										</ul>

									</div>

								</div>

								
								<div class="tab-content">

									<div class="tab-pane <?php if($actualTab =='videos-tab') echo 'active';?>" id="videos-tab">

										<div class="mt-5 mb-5 col-lg-12">

											<form id="youtube-form">

												<div class="form-group">


													<div class="form-group m-form__group row">

														<label class="col-form-label col-lg-3 col-sm-12">Link al video*:</label>

														<div class="col-lg-4 col-md-9 col-sm-12">

															<input id="youtube_id" type="text" class="form-control m-input" name="youtube_id" placeholder="URL del video" value="">

															<small class="yt-info m-form__help">Copiá y pegá acá la URL del video de <b>YouTube.</b></small>

															<div id="previews">

																<div id="preview-1"></div>

																<div id="preview-2"></div>

																<div id="preview-3"></div>


															</div>

															<input type="hidden" id="hidden-yt-id" name="hidden-yt-id" value="">

														</div>



														<div class=""> 

															<button data-db-value="" data-id="1" class="add-video-btn btn btn-default" type="submit">agregar

															</button>

														</div>


													</div>

												</div>

											</form>

											<input type="file" id="videos-batch" name="videos-batch[]" accept="image/*" multiple>

										</div>										
									</div>

									<div class="tab-pane <?php if($actualTab =='images-tab') echo 'active';?>" id="images-tab">

										<div class="mt-5 mb-5 col-lg-12">

											<input type="file" id="images-batch" name="images-batch[]" accept="image/*" multiple>

										</div>										
									</div>
									
									
									<div class="tab-pane <?php if($actualTab =='general-tab') echo 'active';?>" id="general-tab">

										<form id="add-data-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>	

												</div>

											</div>

											<!-- nombre del curso-->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Nombre del curso*:</label>

												<div class="col-lg-4 col-md-9 col-8 input-group">

													<input id="name" type="text" class="form-control m-input" name="name" placeholder="Nombre del curso" value="<?php echo $courseName;?>">

													<?php if(!$editMode){ ?>

														<div class="">
															<button id="validate-title" class="btn  btn-info" type="button" disabled>Validar</button>
														</div>

													<?php } ?>

												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $courseName;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<!-- URL del proyecto-->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">URL*:<br><small>Permalink. Así será la URL.
													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Por cuestiones de SEO (Search Engine Optimization) el permalink es el único campo que una vez que crees el curso no vas a volver poder editar, por lo que te pedimos que le prestes especial atención al mismo.">
														¡Lee esto!
													</small>

												</small>

											</label>

											<div class="col-lg-4 col-md-9 col-8">

												<input id="url" type="text" class="form-control m-input" name="url" placeholder="URL del curso" value="<?php echo $courseUrl;?>" readonly="readonly" disabled>

												<?php if($editMode){?>
													<small class='text-danger'>Este campo no se puede editar.</small>
												<?php }?>

											</div>

										</div>

										<!-- ¿en curso? -->

										<div class="form-group m-form__group row">

											<label class="col-form-label col-lg-3 col-sm-12">Descripción del curso:*</label>

											<div class="col-lg-6 col-md-9 col-sm-12">	

												<textarea numlines="20" type="text" class="form-control m-input description" id="description" name="description" placeholder="Descripción"><?php echo trim($courseDescription); ?></textarea>	
												<span class="d-none summernote-description-error m-form__help">La descripción no puede quedar vacía.</span>

												<?php if($editMode){  ?>

													<br>

													<div class=""> 

														<button data-id="<?php echo $actualId;?>"

															class="update-btn btn btn-default" type="button">actualizar

														</button>

													</div>

												<?php }?>	

											</div>

										</div>

										<div class="form-group m-form__group row">

											<label class="col-form-label col-lg-3 col-sm-12">Imagen de Portada*:
												<br><small>Es la imagen principal del curso y tiene que ser mayor a 2000 x 1000 px.<br>

													<small data-toggle="tooltip" data-placement="bottom" title="Porque al partir de una imagen grande, nuestros servidores pueden resamplearla a diversas resoluciones sin pérdida de calidad. El tamaño máximo es de 20 MB.">
														¿por qué tan grande?
													</small>


												</small>

											</label>

											<div class="col-lg-4 col-md-9 col-sm-12">

												<input id="main-image" name="main-image" type="file">

											</div>

										</div>

										<!-- programa -->

										<?php 

										$visibility = 'd-none';

										if($allowProgramDownload == 0){  $visibility = ''; }

										?>

										<div class="pdf-group form-group m-form__group row pt-4 

										<?php echo $visibility;?> ">

										<label class="col-form-label col-lg-3 col-sm-12">Programa del curso*:</label>

										<div class="col-lg-4 col-md-9 col-8">

											<input type="file" class="form-control-file" id="course-program" name="course-program" accept="application/pdf">

											<!--<button class="d-none delete-pdf-file update-btn btn btn-default" type="button">borrar archivo</button>-->

										</div>

									</div>

									<?php if($editMode && $allowProgramDownload == 1){ ?>

										<div class="form-group m-form__group row pt-4">

											<label class="col-form-label col-lg-3 col-sm-12">Programa:

											</label>

											<div class=""> 

												<button data-id="<?php echo $actualId;?>" class="delete-pdf-program btn btn-error" type="button">borrar programa actual</button>

											</div>

										</div>

									<?php } ?>

									<div class="form-group m-form__group row">

										<label class="col-form-label col-lg-3 col-sm-12">Categoría:*</label>

										<div class="col-lg-4 col-md-9 col-sm-12">

											<div class="m-checkbox-list">

												<?php 

												$categoriesQuery = DB::getInstance()->query('SELECT * FROM museum_courses_modalities');

												foreach($categoriesQuery->results() as $c){

													$checked = '';

													foreach($courseModalitiesArray as $item){

														if($c->id == $item) $checked = 'checked'; 
													}

													?>

													<label class="m-checkbox m-checkbox--solid m-checkbox--success">

														<input <?php echo $checked;?> value="<?php echo $c->id;?>" 

														name="checkboxes[]" 

														type="checkbox"

														class="<?php echo $c->modality_tag;?>"

														data-id="<?php echo $actualId;?>">

														<?php echo $c->course_modality;?>

														<span></span>

													</label>


												<?php } ?>

											</div>

											<span class="m-form__help"><b>Seleccioná</b> al menos una de las opciones.
											</span>

										</div>

									</div>

									<div class="m-portlet__head">

										<div class="m-portlet__head-caption">

											<div class="m-portlet__head-title">

												<h3 class="m-portlet__head-text">

													Sobre las fechas

												</h3>

											</div>

										</div>

									</div>

									<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Mostrar precio:	

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input type="checkbox" <?php echo $showPrice;?>
																	class="show_price toggler-info" 

																	name="show_price" 

																	data-on="Enabled"

																	data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

									<div class="form-group m-form__group row pt-4">

										<label class="col-form-label col-lg-3 col-sm-12">Precio:</label>

										<div class="col-lg-4 col-md-9 col-8">

											<div class="m-input-icon m-input-icon--left">

												<input type="number" min="0" class="form-control m-input" id="price" name="price" placeholder="Precio" value="<?php echo $coursePrice;?>" 

												<?php echo $priceInputStatus;?>>

												<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fas fa-dollar-sign"></i></span></span>

											</div>

										</div>

										<?php if($editMode){ ?>

											<div class=""> 

												<button data-db-value="<?php echo $coursePrice;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar</button>

											</div>

										<?php }?>

									</div>

									<div class="m-portlet__head">

										<div class="m-portlet__head-caption">

											<div class="m-portlet__head-title">

												<h3 class="m-portlet__head-text">

													Sobre las clases

												</h3>

											</div>

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label class="col-form-label col-lg-3 col-8">Clases:</label>

										<div class="col-lg-4 col-md-9 col-sm-12">

											<input type="number" min="0" class="form-control m-input" id = "number_of_classes" name="number_of_classes" placeholder="Número de clases" value="<?php echo $classes;?>">					
										</div>

										<?php if($editMode){ ?>

											<div class=""> 

												<button data-db-value="<?php echo $classes;?>" data-id="<?php echo $actualId;?>"

													class="update-btn btn btn-default" type="button">actualizar

												</button>

											</div>

										<?php }?>

									</div>

									<div class="form-group m-form__group row">

										<label class="col-form-label col-lg-3 col-sm-12">Mostrar Duración de las clases	
										</label>

										<div class="col-lg-4 col-md-9 col-sm-12">

											<div class="m-form__group form-group row">

												<div class="col-3">

													<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

														<label>
															<input type="checkbox" <?php echo $showDuration;?>
															class="show_duration toggler-info" 

															name="show_duration" 

															data-on="Enabled"

															data-id="<?php echo $actualId;?>">

															<span></span>

														</label>

													</span>

												</div>

											</div>

										</div>

									</div>

									<div class="form-group m-form__group row pt-4">

										<label class="col-form-label col-lg-3 col-sm-12">Duración:
											<br><small>Indicada en el formato <b>hh:mm:ss</b></small></label>


											<div class="col-lg-4 col-md-9 col-8">

												<div class="m-input-icon m-input-icon--left">

													<div class="input-group bootstrap-timepicker timepicker">

														<input name="duration" id="duration" type="text" class="form-control input-small" value="<?php echo $duration;?>" <?php echo $durationInputStatus;?>>

													</div>

													<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-clock"></i></span></span>

												</div>

											</div>

											<?php if($editMode){ ?>

												<div class=""> 

													<button data-db-value="<?php echo $duration;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar</button>

												</div>

											<?php }?>


										</div>

										<div class="form-group m-form__group row">

											<label class="col-form-label col-lg-3 col-sm-12">Fecha</label>

											<div class="col-lg-4 col-md-9 col-sm-12">

												<div class="input-daterange input-group" id="course-datepicker">

													<input type="text" class="start_date form-control m-input" name="start_date" value="<?php echo $startDate;?>">

													<div class=""> <span class="input-group-text">

														<i class="la la-ellipsis-h"></i> </span>

													</div>

													<input type="text" class="form-control end_date" name="end_date" value="<?php echo $endDate;?>">

												</div>

												<span class="m-form__help">Seleccioná la fecha de comienzo y la de fin.</span>

											</div>

										</div>

										<div class="m-portlet__head">

											<div class="m-portlet__head-caption">
												<div class="m-portlet__head-title">

													<h3 class="m-portlet__head-text">

														Tutores
														<br>
														<small>Aparecerán todos los tutores que alguna vez dictaron un curso. Si el tutor no aparece o es nuevo, simplemente agregalo escribiendo su nombre.</small>

													</h3>

												</div>

											</div>

										</div>

										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-3 col-sm-12">Lista de tutores:</label>

											<div class="col-lg-4 col-md-9 col-sm-12">

												<select class="col-12 tutor-selector" name="states[]" multiple="multiple">

													<?php foreach($tutors as $tutor){

														$selected = '';

														$dataRelation = '';

														foreach($acualTutorsList as $tutorInList){

															if($tutor->id == $tutorInList->id_tutor){

																$selected = 'selected';

																$dataRelation =  $tutorInList->id;

															}
														}echo $tutor->name . ' ' . $tutor->surname;

														?>

														<option data-relation="<?php echo $dataRelation;?> " <?php echo $selected;?> value="<?php echo $tutor->id;?>">

															<?php echo $tutor->name . ' ' . $tutor->surname;?></option>

														<?php } ?>

													</select>

												</div>

											</div>

											<div id="m_repeater_1"> 

												<div class="form-group  m-form__group row" id="m_repeater_1">

													<label class="col-form-label col-lg-3 col-sm-12">Nuevo tutor:

														<br><small>¿No está en la lista? agregalo</small>

													</label>

													<div data-repeater-list="" class="col-lg-8 col-md-9 col-sm-12">

														<div data-repeater-item="" class="my-repeater-item form-group m-form__group row align-items-center" style="padding-left:0px !important; padding-right: 0px !important"> 

															<div class="col-md-3">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name="new_tutor_name" type="text" class="form-control m-input new_tutor_name" placeholder="nombre" holder="Nombre"> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															<div class="col-md-3">

																<div class="m-form__group m-form__group--inline" style="padding-left:0px !important; padding-right: 0px !important">

																	<div class="m-form__control">

																		<input name='new_tutor_surname' type="text" class="form-control m-input new_tutor_surname" placeholder="Apellido"> 

																	</div>

																</div>

																<div class="d-md-none m--margin-bottom-10"></div>

															</div>

															<div class="col-md-1">

																<?php if(!$editMode){?>

																	<div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">

																		<span>

																			<i class="la la-trash-o"></i>

																			<span>Eliminar</span>

																		</span>

																	</div>

																<?php }else{ ?>

																	<div class="btn-add-in-edition btn-sm btn btn-info m-btn m-btn--icon m-btn--pill">

																		<span>

																			<i class="la la-plus"></i>

																			<span>Agrregar</span>

																		</span>

																	</div>


																<?php } ?>

															</div>

														</div></div>                 

													</div>

													<?php if(!$editMode){ ?>

														<div class="m-form__group form-group row">

															<label class="col-form-label col-lg-3 col-sm-12"></label>

															<div class="col-lg-4">

																<div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">

																	<span>

																		<i class="la la-plus"></i>

																		<span>Agregar otro</span>

																	</span>

																</div>

															</div>                                        

														</div>

													<?php } ?>

												</div>

												<div class="m-portlet__head">

													<div class="m-portlet__head-caption">

														<div class="m-portlet__head-title">

															<h3 class="m-portlet__head-text">

																Contacto

															</h3>

														</div>

													</div>

												</div>

												<!-- whatsapp -->

												<div class="form-group m-form__group row pt-4">

													<label class="col-form-label col-lg-3 col-sm-12">WhatsApp:</label>

													<div class="col-lg-4 col-md-9 col-8">

														<div class="m-input-icon m-input-icon--left">

															<input type="number" class="form-control m-input" id="whatsapp" name="whatsapp" placeholder="Una línea con whatsapp" value="<?php echo $whatsappw;?>">

															<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fab fa-whatsapp"></i></span></span>

														</div>

													</div>

													<?php if($editMode){ ?>

														<div class=""> 

															<button data-db-value="<?php echo $whatsapp;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar</button>

														</div>

													<?php }?>

												</div>

												<div class="form-group m-form__group row pt-4">

													<label class="col-form-label col-lg-3 col-sm-12">Email:</label>

													<div class="col-lg-4 col-md-9 col-8">

														<div class="m-input-icon m-input-icon--left">

															<input type="text" class="form-control m-input" id="email" name="email" placeholder="email" value="<?php echo $email;?>">

															<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-envelope"></i></span></span>

														</div>

													</div>

													<?php if($editMode){ ?>

														<div class=""> 

															<button data-db-value="<?php echo $email;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar</button>

														</div>

													<?php }?>


												</div>


												<div class="form-group m-form__group row pt-4">

													<label class="col-form-label col-lg-3 col-sm-12">Teléfono:</label>

													<div class="col-lg-4 col-md-9 col-8">

														<div class="m-input-icon m-input-icon--left">

															<input type="number" class="form-control m-input" id="phone" name="phone" placeholder="teléfono" value="<?php echo $phone;?>">

															<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-phone"></i></span></span>

														</div>

													</div>

													<?php if($editMode){ ?>

														<div class=""> 

															<button data-db-value="<?php echo $phone;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar</button>

														</div>

													<?php }?>

												</div>

												<div class="form-group m-form__group row pt-4">

													<label class="col-form-label col-lg-3 col-sm-12">Grupo en Facebook:</label>

													<div class="col-lg-4 col-md-9 col-8">

														<div class="m-input-icon m-input-icon--left">

															<input type="text" class="form-control m-input" id="facebook_group" name="facebook_group" placeholder="URL del grupo" value="<?php echo $facebookGroup;?>">

															<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fab fa-facebook"></i></span></span>

														</div>

													</div>

													<?php if($editMode){ ?>

														<div class=""> 

															<button data-db-value="<?php echo $facebook_group;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar</button>

														</div>

													<?php }?>

												</div>

												<!-- observaciones -->

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-sm-12">Observaciones<br><small>Usá este campo para info relevante que no hayas ingresado en el form.</small></label>

													<div class="col-lg-6 col-md-9 col-sm-12">	

														<textarea numlines="20" type="text" class="form-control m-input observations" id="observations" name="observations" placeholder="Ovsercaciones"><?php echo trim($observations);?></textarea>	

														<?php if($editMode){  ?>

															<br>

															<div class=""> 

																<button data-id="<?php echo $actualId;?>"

																	class="update-btn btn btn-default" type="button">actualizar

																</button>

															</div>

														<?php }?>	

													</div>

												</div>

												<div class="m-portlet__head">

													<div class="m-portlet__head-caption">
														<div class="m-portlet__head-title">

															<h3 class="m-portlet__head-text">

																Generales
																<br>

															</h3>

														</div>

													</div>

												</div>


												<div class="m-portlet__head">

													<div class="m-portlet__head-caption">
														<div class="m-portlet__head-title">

															<h3 class="m-portlet__head-text">

																Lugar

																<br>

															</h3>

														</div>

													</div>

												</div>

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-sm-12">¿Mostrar dirección?	

													</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<div class="m-form__group form-group row">

															<div class="col-3">

																<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																	<label>
																		<input type="checkbox" <?php echo $showAddress;?>
																		class="show_address toggler-info" 

																		name="show_address" 

																		data-on="Enabled"

																		data-id="<?php echo $actualId;?>">

																		<span></span>

																	</label>

																</span>

															</div>

														</div>

													</div>

												</div>

												<!-- va -->

												<div class="form-group m-form__group row pt-4 museum_map_address_block <?php if($showAddress == '') echo 'disabled';?>">

													<label class="col-form-label col-lg-3 col-sm-12">Lugar:</label>

													<div class="col-lg-4 col-md-9 col-8">

														<div class="m-input-icon m-input-icon--left">

															<input type="text" class="form-control m-input" id="address" name="address" placeholder="Driección" value="<?php echo $address;?>">

															<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fas fa-map-marker-alt"></i></i></span></span>

														</div>

														<span class="m-form__help">

															<p class="museum-finder">hace click acá para localizar al museo.</p></span>

														</div>

														<div class=""> 	

															<button data-db-value="<?php echo $whatsapp;?>" data-id="<?php echo $actualId;?>" class="search-btn btn btn-default" type="button">Buscar</button>

														</div>

													</div>



													<div class="form-group m-form__group row pt-4">


														<div class="offset-lg-3 col-lg-6 col-md-9 col-12 input-group">

															<div class="<?php if($showAddress == '') echo 'disabled';?>" id="map"></div>	

															<input type="hidden" id="lat" name="lat" value="<?php echo $lat;?>">

															<input type="hidden" id="long" name="long" value="<?php echo $long;?>">

														</div>

													</div>


													<div class="m-portlet__head">

														<div class="m-portlet__head-caption">
															<div class="m-portlet__head-title">

																<h3 class="m-portlet__head-text">

																	Generales
																	<br>

																</h3>

															</div>

														</div>

													</div>

													<!-- allow inscriptions -->

													<div class="form-group m-form__group row">

														<label class="col-form-label col-lg-3 col-sm-12">Permitir inscripciones:	

														</label>

														<div class="col-lg-4 col-md-9 col-sm-12">

															<div class="m-form__group form-group row">

																<div class="col-3">

																	<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																		<label>
																			<input type="checkbox" <?php echo $allowEnrollment;?>
																			class="allow_enrollment toggler-info" 

																			name="allow_enrollment" 

																			data-on="Enabled"

																			data-id="<?php echo $actualId;?>">

																			<span></span>

																		</label>

																	</span>

																</div>

															</div>

														</div>

													</div>

													<!-- allow share -->

													<div class="form-group m-form__group row">

														<label class="col-form-label col-lg-3 col-sm-12">Permitir compartir:	

														</label>

														<div class="col-lg-4 col-md-9 col-sm-12">

															<div class="m-form__group form-group row">

																<div class="col-3">

																	<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																		<label>
																			<input type="checkbox" <?php echo $allowShare;?>
																			class="allow_share toggler-info" 

																			name="allow_share" 

																			data-on="Enabled"

																			data-id="<?php echo $actualId;?>">

																			<span></span>

																		</label>

																	</span>

																</div>

															</div>

														</div>

													</div>

													<!-- ... -->

													<div class="m-portlet__foot m-portlet__foot--fit">

														<div class="m-form__actions">

															<div class="row">

																<div class="col-2"></div>

																<div class="col-7">

																	<input type="hidden" name="token" value="<?php echo Token::generate()?>">

																	<?php 

																	$hiddenClass = ($editMode) ? 'd-none' : '';

																	?>

																	<button id="add_new_course" type="submit" 

																	class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar Curso</button>
																	&nbsp;&nbsp;

																	<?php 

																	$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

																	$buttonId = 

																	(!$editMode) ? 'exit_from_form' : 'back_to_list';

																	?>

																	<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																		<?php echo $buttonLabel;?>

																	</button>

																</div>

															</div>

														</div>

													</div>

												</form>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

				<?php require_once 'private/includes/footer.php'; ?>

			</div>

			<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

			<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

			<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

			<script src="assets/app/js/helper.js" type="text/javascript"></script>

			<script src="assets/app/js/museum/courses/museum-course-add.js" type="text/javascript"></script>

			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtjbRNPncz0LJT5WPKwuTS4tPoOWPVU5U&callback=initMap" async defer></script>


		</body>

		</html>
