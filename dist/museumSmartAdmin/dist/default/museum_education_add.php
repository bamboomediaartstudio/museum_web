<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

if(Input::exists("get") && Input::get('id')){

	$actualId = Input::get('id');

	$editMode = true;

	$modulesQuery = DB::getInstance()->query('SELECT * FROM  museum_education_modules  

		WHERE id = ? AND deleted = ?', [Input::get('id'), 0]);

	$actualModule = $modulesQuery->first();

	$moduleTitle = $actualModule->title;

	$moduleLink = $actualModule->link;
	
	$moduleButton = $actualModule->button_label;

	$alertCaption = $actualModule->caption;

	$isBlank = ($actualModule->is_blank == 1) ? 'checked' : '';
	
	$isMdh = ($actualModule->is_mdh == 1) ? 'checked' : '';
	
	$isYoutube = ($actualModule->is_youtube == 1) ? 'checked' : '';

	$isInstagram = ($actualModule->is_instagram == 1) ? 'checked' : '';

	$isFacebook = ($actualModule->is_facebook == 1) ? 'checked' : '';

	$isZoom = ($actualModule->is_zoom == 1) ? 'checked' : '';

	$isOther = ($actualModule->is_other == 1) ? 'checked' : '';

	$fileName = $actualId . '_' . $actualModule->uid . '_uploaded.' . $actualModule->extension;

	$imagePath = 'private/sources/mix/' . $actualId . '/' . $actualModule->uid . '/' . $fileName;


}else{

	$editMode = false;

	$moduleTitle = "";

	$moduleLink = "";

	$alertCaption = '';

	$moduleButton = '';

	$isBlank = '';

	$isMdh = '';
	
	$isYoutube = '';

	$isInstagram = '';

	$isFacebook = '';

	$isZoom = '';

	$isOther = '';

	$imagePath = '';
}




?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Educación | Agregar Módulo</title>

	<meta name="description" content="add new alert">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{
		
		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	#map {

		height: 500px;

		width:100%;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}

	/* Tags styles */
	div.tagsinput { background: #FFF; padding:5px; width:300px; height:100px; overflow-y: auto;}
	div.tagsinput span.tag { border: 1px solid #a5d24a; -moz-border-radius:2px; -webkit-border-radius:2px; display: block; float: left; padding: 5px; text-decoration:none; background: #cde69c; color: #638421; margin-right: 5px; margin-bottom:5px;font-family: helvetica;  font-size:13px;}
	div.tagsinput span.tag a { font-weight: bold; color: #82ad2b; text-decoration:none; font-size: 11px;  }
	div.tagsinput input { width:80px; margin:0px; font-family: helvetica; font-size: 13px; border:1px solid transparent; padding:5px; background: transparent; color: #000; outline:0px;  margin-right:5px; margin-bottom:5px; }
	div.tagsinput div { display:block; float: left; }
	.tags_clear { clear: both; width: 100%; height: 0px; }
	.not_valid {background: #FBD8DB !important; color: #90111A !important;}
	/* Tags styles Ends*/

</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<input type="hidden" id="image-path" name="image-path" value="<?php echo $imagePath;?>">

		<input type="hidden" id="module-id" name="alert-id" value="<?php echo $actualId;?>">
		
		<input type="hidden" id="module-title" name="module-title" value="<?php echo $moduleTitle;?>">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Educación</h3>		
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>
								
								<li class="m-nav__separator">-</li>
								
								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text">Agregar módulo</span>

									</a>

								</li>
								
							</ul>

						</div>
						
					</div>

				</div>
				
				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Datos módulo

												</a>

											</li>

										</ul>

									</div>

								</div>

								
								<div class="tab-content">

									
									<div class="tab-pane active" id="general-tab">

										<form id="add-data-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>	

												</div>

											</div>

											<!-- titulo de la alarta -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Título*:<br><small>corto y conciso</small></label>

												<div class="col-lg-4 col-md-9 col-8">

													<input id="title" type="text" class="form-control m-input" name="title" placeholder="Título del módulo" value="<?php echo $moduleTitle;?>">

													
												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $moduleTitle;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<div id="key-list" class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Keywords:
													<br><small>Al agregar, se listan debajo<br>

														<small data-toggle="tooltip" data-placement="bottom" title="Los keywords o tags te permiten vincular cada módulo que subas con una temática específica. Es un sistema de relaciones que nos va a permitir hacer crecer la plataforma de educación de una forma sana, prolija, escalable y ordenada :)">
															lee esto!
														</small>


													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input type="text" id="keyword" name="keyword" class="typeahead form-control m-input" placeholder="Keyword / tag" value="">

													<input type="hidden" id="hidden-key-id" name="hidden-key-id" >

												</div>

												<?php

												if(!$editMode){

													?>

													<div class="input-group-append">

														<button id="add-keyword" class="btn btn-default" name="add-keyword" type="button">Agregar</button>

													</div>

													<?php

												}else{

													?>

													<div class="input-group-append">

														<button id="add-keyword-direct" class="btn btn-default " type="button">Agregar</button>

													</div>

													<?php

												}

												?>

											</div>

											<div class="form-group m-form__group row pt-4">

												<label class="control-label col-md-3"></label>

												<div class="col-md-9">

													<div id="" class="tagsinput" style="width: auto; height: 100px;">

														<!-- If we are in editMode, load actual tags for this item -->
														<?php

															if($editMode){


																//LEVANTAR TODOS LOS TAGS DE UN DIARIO

																//Para que se muestre la relación, tiene que estar delted = 0 la kewyord y la relacion keyword/diario

																$getTagsQuery = DB::getInstance()->query("SELECT ampk.name, ampk.deleted, ampkr.id as id_relation, ampkr.id_module, ampkr.id_keyword, ampkr.deleted FROM museum_education_keywords as ampk, museum_education_modules_keywords_relations as ampkr WHERE ampkr.id_keyword = ampk.id AND ampkr.id_module = ? AND ampk.deleted = ? AND ampkr.deleted = ?", array($actualId, 0, 0));

																$tagsList = $getTagsQuery->results(true);

																foreach($tagsList as $idTag => $dataTag){

																?>

																	<span id="<?php echo $dataTag["id_relation"]; ?>" class="tag deleteTag"><span><span class="tag-value"><?php echo $dataTag["name"]; ?></span>&nbsp;&nbsp;</span><a href="#" title="Eliminar Keyword">x</a></span>

																<?php

																}

															}

														?>

													</div>

												</div>

											</div>


											<!-- cuepo -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">contenido:*<br><small>No más de 255 caracteres :)</small></label>

												<div class="col-lg-6 col-md-9 col-sm-12">	

													<textarea numlines="20" type="text" class="form-control m-input description" id="caption" name="caption" placeholder="El mensaje de la alerta"><?php echo trim($alertCaption); ?></textarea>	


													<?php if($editMode){  ?>

														<br>

														<div class=""> 

															<button data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>	

												</div>

											</div>

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Botón:<br>

													<small>
													<small data-toggle="tooltip" data-placement="bottom" title="Por defecto, la plataforma va a customizar siempre los botones de forma automática, analizando el contenido subido. Si se sube una imagen, el default es 'ABRIR IMAGEN', y así con todos los tipos de assets. Aún así, en algunos casos, les puede ser de utilidad customizar el botón. Por ejemplo, si publican un link de inscripción, el default es 'ABRIR LINK', pero les puede ser más útil customizarlo a 'CLICK PARA INSCRIBIRSE'.">
															¡Lee esto!
														</small>

													</small>
													
												</label>
												
												<div class="col-lg-4 col-md-9 col-8">

													<input id="button_label" type="text" class="form-control m-input" name="button_label" placeholder="Texto del botón" value="<?php echo $moduleButton;?>">

												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $moduleButton;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Link:<br>

													<small>
													<small data-toggle="tooltip" data-placement="bottom" title="Podés usar cualquier tipo de link! La plataforma va a analizar automáticamente la URL y si detecta que es uno de los sitios más usados (YouTube, Facebook, Instagram, Zoom, Museo Del Holocausto, etc), se marcará el checkbox. Esto nos permite brindarle una visual más customizada al visitante del sitio.">
															¡Lee esto!
														</small>

													</small>

												</label>


												<div class="col-lg-4 col-md-9 col-8">

													<input id="link" type="text" class="form-control m-input" name="link" placeholder="Link" value="<?php echo $moduleLink;?>">

												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $moduleLink;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<!-- link type... -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">¿es link de youtube?	

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input type="checkbox" <?php echo $isYoutube;?>

																	class="is_youtube toggler-info" name="is_youtube" data-on="Enabled" data-id="">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">¿es link de Instagram?	

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input type="checkbox" <?php echo $isInstagram;?>

																	class="is_instagram toggler-info" name="is_instagram" data-on="Enabled" data-id="">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">¿es link de Facebook?	

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input type="checkbox" <?php echo $isFacebook;?>

																	class="is_facebook toggler-info" name="is_facebook" data-on="Enabled" data-id="">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">¿es link propio?	

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input type="checkbox" <?php echo $isMdh;?>

																	class="is_mdh toggler-info" name="is_mdh" data-on="Enabled" data-id="">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">¿es link de zoom?	

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input type="checkbox" <?php echo $isZoom;?>

																	class="is_zoom toggler-info" name="is_zoom" data-on="Enabled" data-id="">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">¿es otro?	

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input type="checkbox" <?php echo $isOther;?>

																	class="is_other toggler-info" name="is_other" data-on="Enabled" data-id="">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<!-- ... -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">¿Abrir en otro browser?	

													<small>
														<br>
														<small data-toggle="tooltip" data-placement="bottom" title="si activás esta opción, el sitio redirigá al link indicado. Si lo dejás sin activar, se va a abrir un iframe (ventana) dentro de la misma página. Si bien no hay una mejor opción que otra, la decisión va de la mano con lo que se quiere lograr. La opción por defecto (botón desactivada - iframe) tiene la ventaja que mantiene al usuario en el mismo sitio.">
															¡Lee esto que es importante!
														</small>

													</small>


												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input type="checkbox" <?php echo $isBlank;?>

																	class="is_blank toggler-info-blank" name="is_blank" data-on="Enabled" data-id="">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Archivo:
													<small>
													<br>
														<small data-toggle="tooltip" data-placement="bottom" title="Podés subir imágenes en cualquier formato (.jpg, .jpeg, .gif, .png, etc), videos (.mp4 o .webm), audios (.mp3), archivos .pdf, archivos comprimidos (.rar y .zip), archivos de Word (.doc y .docx), archivos de Excel (.xls y .xlsx), presentaciones de Power Point (.ppt y .pptx). La plataforma detectará automáticamente el formato para brindarle una visual acorde al visitante.">
															Lee sobre tipos de archivo!
														</small>


													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input id="main-image" name="main-image" type="file">

												</div>

											</div>


										<div class="m-portlet__foot m-portlet__foot--fit">

											<div class="m-form__actions">

												<div class="row">

													<div class="col-2"></div>

													<div class="col-7">

														<input type="hidden" name="token" value="<?php echo Token::generate()?>">

														<?php 

														$hiddenClass = ($editMode) ? 'd-none' : '';

														?>

														<button id="add_new_alert" type="submit" 

														class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar módulo</button>
														&nbsp;&nbsp;

														<?php 

														$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

														$buttonId = 

														(!$editMode) ? 'exit_from_form' : 'back_to_list';

														?>

														<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

															<?php echo $buttonLabel;?>

														</button>

													</div>

												</div>

											</div>

										</div>

									</form>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

	<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/museum/education/museum-education-add.js" type="text/javascript"></script>

</body>

</html>
