<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$url = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);

$errorReportQuery = DB::getInstance()->query("SELECT id FROM museum_bugs_subjects WHERE url = ?", array($url));

$errorReportId =  $errorReportQuery->first()->id;

?>


<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Eventos | Lista</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/af-2.3.0/r-2.2.2/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>

	<link href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.1/cropper.min.css" rel="stylesheet" type="text/css" />


	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.flex-mk{
		overflow: hidden;
		white-space: nowrap;
		text-overflow: ellipsis;
		width: 200px;

	}

	.table-cell-edit{

		background-color: #efefef !important;

		cursor:move;

		border: 1px solid #efefef;

		-webkit-box-shadow: 5px 0 5px -2px #ddd;

		box-shadow: 5px 0 5px -2px #ddd;

	}

	.modal-lg {

		min-width: 80%;

		/*margin: auto;*/

	}

	.temp-test{

		max-width: 100%;

		max-height:550px;

		overflow: hidden !important;

	}

	#result{

		width: 600px;

		height:600px;

		overflow: hidden !important;

	}

	.preview-container, .real-container{

		overflow: hidden !important;

	}

	
</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="modal" tabindex="-1" role="dialog">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title"></h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body">

					<p>Código QR con la URL del evento:</p>

					<div class='text-center' id="qrcode"></div>

					<div class="form-group m-form__group row pt-4">

						<div class="col-12 input-group">

							<input id="qr-generated-url" type="text" class="form-control m-input" name="name" placeholder="Nombre del evento">

							<div class="">
								<button class="btn btn-info copy-to-clipboard" type="button">Copiar</button>
							</div>

						</div> 
					</div>


				</div>

				<div class="modal-footer">

					<button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>

				</div>

			</div>

		</div>

	</div>

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
				<i class="la la-close"></i>
			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">

								Eventos

							</h3>

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon">

										<i class="m-nav__link-icon la la-home"></i>

									</a>

								</li>

								<li class="m-nav__separator">-</li>

								<li class="m-nav__item"> <span class="m-nav__link-text">Listado de eventos.</span></li>

							</ul>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">

						<div class="m-alert__icon"><i class="flaticon-exclamation m--font-brand"></i></div>

						<div class="m-alert__text">

							Esta página incluye los <strong>eventos</strong> del museo. Podés cambiar la visibilidad de las mismas a través del botón de <strong>Estado</strong>. También podés <i class="la la-edit"></i><strong>editar</strong> y <i class="la la-trash"></i><strong>eliminar</strong> cada muestra.<br><br>¿Querés subir un evento nuevo?<br><br>

							<a class="btn btn-xs btn-success" href="museum_event_add.php">Agregar</a><br><br>

							¿Algo no funciona? <a class="" href="museum_bug_report.php?id=<?php echo $errorReportId;?>">reportar error.</a><br><br>
							
						</div>

					</div>

					<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">

						<div class="m-portlet__head">

							<div class="m-portlet__head-tools">

								<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" 
								role="tablist">

								<li class="nav-item m-tabs__item" data-id="">

									<a class="nav-link m-tabs__link  active" data-toggle="tab_1" href="" role="tab">

										<i class="flaticon-share m--hide"></i>MUESTRAS

									</a>

								</li>

							</ul>

						</div>

					</div>

					<div class="tab-content">

						<div class="tab-pane active" id="tab_1">

							<div class="m-portlet__body">

								<table id="tab-1" class="table display table-striped table-bordered responsive nowrap">

									<thead>

										<tr>

											<th>id</th>

											<th>Muestra</th>

											<th>Fecha</th>
											
											<th>estado</th>

											<th>Acciones</th>

										</tr>

									</thead>

									<tbody>

										<?php

										
										$eventsResultsQuery = DB::getInstance()->query("SELECT * 

											FROM museum_events as mc WHERE mc.deleted = ? 

											ORDER BY mc.id DESC ", array(0));

										$eventsList = $eventsResultsQuery->results();

										$counter = 0;

										foreach($eventsList as $eventResult){

											setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

											$initTime = utf8_encode(strftime("%A, %d de %B del %Y a las %H:%m hs", strtotime($eventResult->start_date)));

											$endTime = utf8_encode(strftime("%A, %d de %B del %Y", strtotime($eventResult->end_date)));

											$finalTime = 'Del ' . $initTime . '<br> al ' . $endTime;


											$counter+=1;

											$checked = ($eventResult->active) ? 'checked' : '';

											?>

											<tr class="row-<?php echo $counter;?>">

												<td style='width:20px;'><?php echo $eventResult->id;?></td>
												
												<td class='pomex'><div class='flex-mk'><?php echo $eventResult->name;?></div></td>

												<td><?php echo $finalTime;?></td>

												<td>

													<input <?php echo $checked;?> type="checkbox" class="my-checkbox" name="my-checkbox" data-size="mini" data-on-color="success" data-on-text="online" data-off-text="offline">
												</td>

												

												<td>

													<span>	

														<button class="edit_row m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-id="<?php echo $eventResult->id;?>" title="Editar Detalles"> 

															<i class="la la-edit"></i>

														</button>

														<button class="add_images m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-id="<?php echo $eventResult->id;?>" title="Agregar imágenes">

															<i class="la la-photo"></i>

														</button>

														<button class="add_videos m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"  data-id="<?php echo $eventResult->id;?>" title="Agregar videos">

															<i class="fab fa-youtube"></i>

														</button>

														<button class="qr_generator m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-id="<?php echo $eventResult->id;?>" data-name="<?php echo $eventResult->name;?>" data-url="<?php echo $eventResult->url;?>" title="Generar código QR">

															<i class="fas fa-qrcode"></i>

														</button>

														<button class="go-to-inscription-list m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="inscriptos">

															<i class="la la-user"></i>

														</button>

														<button class="open-link m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Abrir link del evento" data-url="<?php echo $eventResult->url;?>"><i class="fas fa-globe-americas"></i></button>

														<button class="delete_row m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" 

														data-id="<?php echo $eventResult->id;?>"

														data-name="<?php echo $eventResult->name;?>"

														title="Eliminar"> <i class="la la-trash"></i>

													</button>

												</span>


											</td>

										</tr>

									<?php } ?>

								</tbody>

							</table>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">

	<i class="la la-arrow-up"></i>

</div>

<input type="hidden" id="token" name="token" value="<?php echo Token::generate();?>">


<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/ScrollToPlugin.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/af-2.3.0/r-2.2.2/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>


<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/museum/events/museum-events-list.js" type="text/javascript"></script>

</body>

</html>
