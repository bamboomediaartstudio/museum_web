<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$actualId = Input::get('id');

$actualTab = 'general-tab';

if(Input::exists('get') && Input::get('tab')){

	$actualTab = Input::get('tab');

	//echo 'existe';

}

//echo $actualTab;

//return;

if(Input::exists("get") && Input::get('id')){

	$editMode = true;

	$exhibitionQuery = DB::getInstance()->query('SELECT * FROM  museum_exhibitions  

		WHERE id=? AND deleted = ?', [Input::get('id'), 0]);

	$actualExhbition = $exhibitionQuery->first();

	$whatsapp = $actualExhbition->whatsapp;

	$exhibitionName = $actualExhbition->name;

	$email = $actualExhbition->email;

	$observations = $actualExhbition->observations;

	$phone = $actualExhbition->phone;

	$exhibitionUrl = $actualExhbition->url;

	$lat = $actualExhbition->lat;

	$long = $actualExhbition->long;

	$exhibitionDescription = $actualExhbition->description;

	$allowShare = ($actualExhbition->allow_share == 1) ? 'checked' : '';

	$allowContract = ($actualExhbition->allow_contract == 1) ? 'checked' : '';

	$isCurrent = ($actualExhbition->is_current == 1) ? 'checked' : '';
	
	$showAddress = ($actualExhbition->show_address == 1) ? 'checked' : '';

	$address = $actualExhbition->address;

	$startDate = $actualExhbition->start_date;

	$endDate = $actualExhbition->end_date;

	//image!

	$courseImageQuery = DB::getInstance()->query('SELECT * FROM 

		museum_images WHERE sid = ? AND source = ? AND deleted = ?', [$actualId, 'exhibitions', 0]);

	if($courseImageQuery->count()>0){

		$courseImage = $courseImageQuery->first();

		$mimeType = MimeTypes::getExtensionByMimeType($courseImage->mimetype);

		$imagePath = 'private/sources/images/exhibitions/' . $actualId . '/' . $courseImage->unique_id . '/' . $actualId . '_' . $courseImage->unique_id . '_original.' . $mimeType;

	}else{

		$imagePath = "";

	}

	//course type relations...

	$modalitiesQuery = DB::getInstance()->query('SELECT * FROM 

		museum_exhibitions_types_relations 

		WHERE id_exhibition = ? AND deleted = ?', [Input::get('id'), 0]);

	$exhibitionsModalitiesList = $modalitiesQuery->results();

	$exhibitionsArray = [];

	foreach($exhibitionsModalitiesList as $cm){

		$exhibitionsArray[] = $cm->id_exhibition_type;

	}

}else{

	$exhibitionsArray = [];

	$lat = '';

	$long = '';

	$isCurrent = '';

	$observations = '';

	$email = '';

	$phone = '';

	$editMode = false;

	$exhibitionName = "";

	$exhibitionUrl = '';

	$exhibitionDescription = '';

	$address = '';

	$startDate = '';

	$endDate = '';

	$facebookGroup = '';

	$whatsapp = '';

	$allowContract = 'checked';

	$allowShare = 'checked';

	$showAddress = 'checked';
}

//museum data...

$museumData = DB::getInstance();

$museumDataQ = $museumData->query('SELECT * FROM museum_data');

if($museumDataQ->count()>0) $museumDataObject = $museumDataQ->first();

//museum social media data...

$museumSocialMediaData = DB::getInstance();

$museumSocialMediaDataQ = $museumSocialMediaData->query('SELECT * FROM museum_social_media');

if($museumSocialMediaDataQ->count()>0) $museumSocialMediaObject = $museumSocialMediaDataQ->first();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Muestras | Agregar Muestra</title>

	<meta name="description" content="add new exhibition">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{
		
		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	#map {

		height: 500px;

		width:100%;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}

</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div id="myModal" class="modal" tabindex="-1" role="dialog">
		
		<div class="modal-dialog" role="document">
			
			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title">Asignar descripción a la imagen</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body">

					<form>

						<div class="form-group">

							<label for="image-	" class="col-form-label">Descripción:</label>

							<input type="text" class="form-control" id="image-description">

						</div>
						
					</form>

				</div>

				<div class="modal-footer">

					<button type="button" class="save-description btn btn-primary">guardar</button>

					<button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<input type="hidden" id="image-path" name="image-path" value="<?php echo $imagePath;?>">

		<input type="hidden" id="exhibition-id" name="exhibition-id" value="<?php echo $actualId;?>">

		<input type="hidden" id="exhibition-url" name="exhibition-url" value="<?php echo $exhibitionUrl;?>">
		
		<input type="hidden" id="exhibition-name" name="exhibition-name" value="<?php echo $exhibitionName;?>">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Muestras</h3>		
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>
								
								<li class="m-nav__separator">-</li>
								
								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text">Agregar Muestra</span>

									</a>

								</li>
								
							</ul>

						</div>
						
					</div>

				</div>
				
				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link <?php if($actualTab =='general-tab') echo 'active';?>" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Información general

												</a>

											</li>

											<li class="nav-item m-tabs__item <?php if(!$editMode) echo 'disabled'; ?>"> 
												<a class="<?php if($actualTab =='images-tab') echo 'active';?> tab-navigation images-tab nav-link m-tabs__link" data-toggle="tab" href="#images-tab" role="tab">

													Imágenes

												</a>

											</li>

											<li class="nav-item m-tabs__item <?php if(!$editMode) echo 'disabled'; ?>"> 
												<a class="<?php if($actualTab =='videos-tab') echo 'active';?> tab-navigation videos-tab nav-link m-tabs__link" data-toggle="tab" href="#videos-tab" role="tab">

													Videos

												</a>

											</li>

										</ul>

									</div>

								</div>

								
								<div class="tab-content">

									<div class="tab-pane <?php if($actualTab =='videos-tab') echo 'active';?>" id="videos-tab">

										<div class="mt-5 mb-5 col-lg-12">

											<form id="youtube-form">

												<div class="form-group">


													<div class="form-group m-form__group row">

														<label class="col-form-label col-lg-3 col-sm-12">Link al video*:</label>

														<div class="col-lg-4 col-md-9 col-sm-12">

															<input id="youtube_id" type="text" class="form-control m-input" name="youtube_id" placeholder="URL del video" value="">

															<small class="yt-info m-form__help">Copiá y pegá acá la URL del video de <b>YouTube.</b></small>

															<div id="previews">

																<div id="preview-1"></div>

																<div id="preview-2"></div>

																<div id="preview-3"></div>


															</div>

															<input type="hidden" id="hidden-yt-id" name="hidden-yt-id" value="">

														</div>



														<div class=""> 

															<button data-db-value="" data-id="1" class="add-video-btn btn btn-default" type="submit">agregar

															</button>

														</div>


													</div>

												</div>

											</form>

											<input type="file" id="videos-batch" name="videos-batch[]" accept="image/*" multiple>

										</div>										
									</div>

									<div class="tab-pane <?php if($actualTab =='images-tab') echo 'active';?>" id="images-tab">

										<div class="mt-5 mb-5 col-lg-12">

											<input type="file" id="images-batch" name="images-batch[]" accept="image/*" multiple>

										</div>										
									</div>
									
									
									<div class="tab-pane <?php if($actualTab =='general-tab') echo 'active';?>" id="general-tab">

										<form id="add-data-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>	

												</div>

											</div>

											<!-- nombre del la muestra-->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Nombre de la muestra*:</label>

												<div class="col-lg-4 col-md-9 col-8 input-group">

													<input id="name" type="text" class="form-control m-input" name="name" placeholder="Nombre de la muestra" value="<?php echo $exhibitionName;?>">

													<?php if(!$editMode){ ?>

														<div class="">
															<button id="validate-title" class="btn  btn-info" type="button" disabled>Validar</button>
														</div>

													<?php } ?>

												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $exhibitionName;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<!-- URL del proyecto-->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">URL*:<br>

													<small>Permalink. Así será la URL.
														<br>
														<small data-toggle="tooltip" data-placement="bottom" title="Por cuestiones de SEO (Search Engine Optimization) el permalink es el único campo que una vez que crees la muestra no vas a volver poder editar, por lo que te pedimos que le prestes especial atención al mismo.">
															¡Lee esto!
														</small>

													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-8">

													<input id="url" type="text" class="form-control m-input" name="url" placeholder="URL de la muestra" value="<?php echo $exhibitionUrl;?>" readonly="readonly" disabled>

													<?php if($editMode){?>
														<small class='text-danger'>Este campo no se puede editar.</small>
													<?php }?>

												</div>

											</div>

											<!-- ¿en curso? -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">¿Muestra actualmente en curso?:	

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input type="checkbox" <?php echo $isCurrent;?>
																	class="is_current toggler-info" 

																	name="is_current" 

																	data-on="Enabled"

																	data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Descripción de la muestra:*</label>

												<div class="col-lg-6 col-md-9 col-sm-12">	

													<textarea numlines="20" type="text" class="form-control m-input description" id="description" name="description" placeholder="Descripción"><?php echo trim($exhibitionDescription); ?></textarea>	
													<span class="d-none summernote-description-error m-form__help">La descripción no puede quedar vacía.</span>

													<?php if($editMode){  ?>

														<br>

														<div class=""> 

															<button data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>	

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Imagen de Portada*:
													<br><small>Es la imagen principal de la muestra y tiene que ser de 2000 x 1000 px.<br>

														<small data-toggle="tooltip" data-placement="bottom" title="Porque al partir de una imagen grande, nuestros servidores pueden resamplearla a diversas resoluciones sin pérdida de calidad. El tamaño máximo es de 20 MB.">
															¿por qué tan grande?
														</small>


													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input id="main-image" name="main-image" type="file">

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Tipo de muestra:*</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-checkbox-list">

														<?php 

														$categoriesQuery = DB::getInstance()->query('SELECT * FROM museum_exhibitions_types');

														foreach($categoriesQuery->results() as $c){

															$checked = '';

															foreach($exhibitionsArray as $item){

																if($c->id == $item) $checked = 'checked'; 
															}

															?>

															<label class="m-checkbox m-checkbox--solid m-checkbox--success">

																<input <?php echo $checked;?> value="<?php echo $c->id;?>" 

																name="checkboxes[]" 

																type="checkbox"

																class="<?php echo $c->type_tag;?>"

																data-id="<?php echo $actualId;?>">

																<?php echo $c->type;?>

																<span></span>

															</label>


														<?php } ?>

													</div>

													<span class="m-form__help"><b>Seleccioná</b> al menos una de las opciones.
													</span>

												</div>

											</div>

											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">

													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															Sobre las fechas

														</h3>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Fecha</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="input-daterange input-group" id="exhibition-datepicker">

														<input type="text" class="start_date form-control m-input" name="start_date" value="<?php echo $startDate;?>">

														<div class=""> <span class="input-group-text">

															<i class="la la-ellipsis-h"></i> </span>

														</div>

														<input type="text" class="form-control end_date" name="end_date" value="<?php echo $endDate;?>">

													</div>

													<span class="m-form__help">Seleccioná la fecha de comienzo y la de fin.</span>

												</div>

											</div>

											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">

													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															Contacto

														</h3>

													</div>

												</div>

											</div>

											<!-- whatsapp -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">WhatsApp:</label>

												<div class="col-lg-4 col-md-9 col-8">

													<div class="m-input-icon m-input-icon--left">

														<input type="number" class="form-control m-input" id="whatsapp" name="whatsapp" placeholder="Una línea con whatsapp" value="<?php echo $whatsapp;?>">

														<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fab fa-whatsapp"></i></span></span>

													</div>

													<span class="m-form__help">Sólo números.</span>

												</div>

												<?php if($editMode){ ?>

													<div class=""> 

														<button data-db-value="<?php echo $whatsapp;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar</button>

													</div>

												<?php }?>

											</div>

											<!-- email -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Email:</label>

												<div class="col-lg-4 col-md-9 col-8">

													<div class="m-input-icon m-input-icon--left">

														<input type="text" class="form-control m-input" id="email" name="email" placeholder="email" value="<?php echo $email;?>">

														<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-envelope"></i></span></span>

													</div>

												</div>

												<?php if($editMode){ ?>

													<div class=""> 

														<button data-db-value="<?php echo $email;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar</button>

													</div>

												<?php }?>

											</div>

											<!-- telefono -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Teléfono:</label>

												<div class="col-lg-4 col-md-9 col-8">

													<div class="m-input-icon m-input-icon--left">

														<input type="number" class="form-control m-input" id="phone" name="phone" placeholder="teléfono" value="<?php echo $phone;?>">

														<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-phone"></i></span></span>

													</div>

													<span class="m-form__help">Sólo números.</span>

												</div>

												<?php if($editMode){ ?>

													<div class=""> 

														<button data-db-value="<?php echo $phone;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar</button>

													</div>

												<?php }?>

											</div>

											<!-- observaciones -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Observaciones<br><small>Usá este campo para info relevante que no hayas ingresado en el form.</small></label>

												<div class="col-lg-6 col-md-9 col-sm-12">	

													<textarea numlines="20" type="text" class="form-control m-input observations" id="observations" name="observations" placeholder="Ovsercaciones"><?php echo trim($observations); ?></textarea>	

													<?php if($editMode){  ?>

														<br>

														<div class=""> 

															<button data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>	

												</div>

											</div>



											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">
													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															Lugar
															
															<br>

														</h3>

													</div>

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">¿Mostrar dirección?	

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input type="checkbox" <?php echo $showAddress;?>
																	class="show_address toggler-info" 

																	name="show_address" 

																	data-on="Enabled"

																	data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<!-- va -->

											<div class="form-group m-form__group row pt-4 museum_map_address_block <?php if($showAddress == '') echo 'disabled';?>">

												<label class="col-form-label col-lg-3 col-sm-12">Lugar:</label>

												<div class="col-lg-4 col-md-9 col-8">

													<div class="m-input-icon m-input-icon--left">

														<input type="text" class="form-control m-input" id="address" name="address" placeholder="Driección" value="<?php echo $address;?>">

														<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fas fa-map-marker-alt"></i></i></span></span>

													</div>

													<span class="m-form__help">

														<p class="museum-finder">hace click acá para localizar al museo.</p></span>

													</div>

													<div class=""> 	

														<button data-db-value="<?php echo $whatsapp;?>" data-id="<?php echo $actualId;?>" class="search-btn btn btn-default" type="button">Buscar</button>

													</div>

												</div>

												<!-- buscar! -->

											<!--<div class="form-group m-form__group row pt-4 museum_map_address_block <?php if($showAddress == '') echo 'disabled';?>" >

												<label class="col-form-label col-lg-3 col-sm-12">Lugar  de la muestra*:</label>

												<div class="col-lg-4 col-md-9 col-8 input-group">

													<input id="address" type="text" class="form-control m-input" name="address" placeholder="Lugar" value="<?php echo $address;?>">				

												</div>

												<div class="">

													<button class="search-btn btn btn-default" type="button">buscar</button>

												</div>


											</div>

											<p class="offset-lg-3 pl-4">Click para encontrar el museo.</p>-->


											<div class="form-group m-form__group row pt-4">


												<div class="offset-lg-3 col-lg-6 col-md-9 col-12 input-group">

													<div class="<?php if($showAddress == '') echo 'disabled';?>" id="map"></div>	

													<input type="hidden" id="lat" name="lat" value="<?php echo $lat;?>">

													<input type="hidden" id="long" name="long" value="<?php echo $long;?>">

												</div>

											</div>


											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">
													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															Generales
															<br>

														</h3>

													</div>

												</div>

											</div>

											<!-- allow contracts -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Permitir solicitudes de la muestra:	

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input type="checkbox" <?php echo $allowContract;?>
																	class="allow_contract toggler-info" 

																	name="allow_contract" 

																	data-on="Enabled"

																	data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<!-- allow share -->

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Permitir compartir:	

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-form__group form-group row">

														<div class="col-3">

															<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																<label>
																	<input type="checkbox" <?php echo $allowShare;?>
																	class="allow_share toggler-info" 

																	name="allow_share" 

																	data-on="Enabled"

																	data-id="<?php echo $actualId;?>">

																	<span></span>

																</label>

															</span>

														</div>

													</div>

												</div>

											</div>

											<!-- ... -->

											<div class="m-portlet__foot m-portlet__foot--fit">

												<div class="m-form__actions">

													<div class="row">

														<div class="col-2"></div>

														<div class="col-7">

															<input type="hidden" name="token" value="<?php echo Token::generate()?>">

															<?php 

															$hiddenClass = ($editMode) ? 'd-none' : '';

															?>

															<button id="add_new_exhibition" type="submit" 

															class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar Muestra</button>
															&nbsp;&nbsp;

															<?php 

															$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

															$buttonId = 

															(!$editMode) ? 'exit_from_form' : 'back_to_list';

															?>

															<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																<?php echo $buttonLabel;?>

															</button>

														</div>

													</div>

												</div>

											</div>

										</form>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="assets/app/js/museum/exhibitions/museum-exhibition-add.js" type="text/javascript"></script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtjbRNPncz0LJT5WPKwuTS4tPoOWPVU5U&callback=initMap" async defer></script>


</body>

</html>
