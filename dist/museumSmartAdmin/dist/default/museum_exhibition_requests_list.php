<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

if(Input::exists('get')){

	if(Input::get('id')){

		$id = Input::get('id');

	}else{

		//exit? el curso no existe...?

	}

}


//about the course...

$exhibitionDataQuery = DB::getInstance()->query(

	"SELECT * FROM museum_exhibitions as mc WHERE mc.id = ? ", array($id));

$exhibitionData = $exhibitionDataQuery->first();

$exhibitionName =  $exhibitionData->name;

$exhibitionURL = $exhibitionData->url;

//about the inscriptions...

$finalPath = 'http://' . $_SERVER['SERVER_NAME'] . '/cursos/' . $exhibitionURL;


?>


<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Muestras | Solicitudes</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />


	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>



	<link href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.1/cropper.min.css" rel="stylesheet" type="text/css" />


	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.table-cell-edit{

		background-color: #efefef !important;

		cursor:move;

		border: 1px solid #efefef;

		-webkit-box-shadow: 5px 0 5px -2px #ddd;

		box-shadow: 5px 0 5px -2px #ddd;

	}

	.modal-lg {

		min-width: 80%;

		/*margin: auto;*/

	}

	.temp-test{

		max-width: 100%;

		max-height:550px;

		overflow: hidden !important;

	}

	#result{

		width: 600px;

		height:600px;

		overflow: hidden !important;

	}

	.preview-container, .real-container{

		overflow: hidden !important;

	}

	
</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >


	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title" id="exampleModalLabel"></h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>
				
				<div class="modal-body">
					...
				</div>
				
				<div class="modal-footer">

					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
				<i class="la la-close"></i>
			</button>
			<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

				<?php require_once 'private/includes/sidebar.php'; ?>

			</div>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">



								SOLICITUDES DE LA MUESTRA <a target="_blank" href="<?php echo $finalPath;?>"><?php echo $exhibitionName;?></a>

							</h3>

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon">

										<i class="m-nav__link-icon la la-home"></i>

									</a>

								</li>

							</ul>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">

						<div class="m-alert__icon"><i class="flaticon-exclamation m--font-brand"></i></div>

						<div class="m-alert__text">

							Esta página incluye todas las <strong>solicitudes</strong> que se realizaron por parte de instituciones para la muestra <strong><?php echo $exhibitionName;?></strong>
							<br>

							
							
						</div>

					</div>

					<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">

						<div class="m-portlet__head">

							<div class="m-portlet__head-tools">

								<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" 
								role="tablist">

								<li class="nav-item m-tabs__item" data-id="">

									<a class="nav-link m-tabs__link  active" data-toggle="tab_1" href="" role="tab">

										<i class="flaticon-share m--hide"></i>SOLICITUDES A <?php echo $exhibitionName;?>

									</a>

								</li>

							</ul>

						</div>

					</div>

					<div class="tab-content">

						<div class="tab-pane active" id="tab_1">

							<div class="m-portlet__body">

								<p>PODES EXPORTAR ESTA LISTA!</p>


								<table id="tab-1" class="table display table-striped table-bordered responsive nowrap" style="width:100%">

									<thead>

										<tr>

											<th>id</th>

											<th>Solicitante</th>

											<th>Institución</th>
											
											<th>Tipo de institución</th>

											<th>Acciones</th>

										</tr>

									</thead>

									<tbody>

										<?php

										$counter = 0;

										
										$museum_exhibitions_requests = DB::getInstance()->query("SELECT * 

											FROM museum_exhibitions_requests as mci WHERE mci.deleted = ? AND mci.id_exhibition = ?  

											ORDER BY mci.id DESC ", array(0, $id));

										$exhibitionsRequestsSList = $museum_exhibitions_requests->results();

										foreach($exhibitionsRequestsSList as $requestResult){

											$counter+=1;

											$checked = ($requestResult->deleted) ? 'checked' : '';

											$paymentDone = '';

											?>

											<tr class="row-<?php echo $counter;?>">

												<td><?php echo $requestResult->id;?></td>
												
												<td><?php echo $requestResult->name . ' ' . $requestResult->surname;?></td>

												<!--<td><a href="mailto:<?php echo $requestResult->email;?>"><?php echo $requestResult->email;?></a></td>

													<td><a href="tel:<?php echo $requestResult->phone;?>"><?php echo $requestResult->phone;?></a></td>-->

													<td class="text-truncate"><?php echo $requestResult->institution_name;?></td>

													<td>

														<?php echo $requestResult->institution_type;?>

													</td>

													<td>

														<span>	

															<button class="aditional_data m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="información del inscripto" 

															data-city="<?php echo $requestResult->city;?>" 

															data-address="<?php echo $requestResult->address;?>" 

															data-medium="<?php echo $requestResult->medium;?>"

															data-first-time="<?php echo $requestResult->first_time;?>"

															data-payment-done="<?php echo $requestResult->payment_done;?>">



															<i class="fas fa-info-circle"></i>

														</button>

														<button class="delete_row m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Eliminar a <?php echo $requestResult->name;?>">

															<i class="fas fa-trash-alt"></i>

														</button>

													</span>


												</td>

											</tr>

										<?php } ?>

									</tbody>

								</table>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

	<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">

	<i class="la la-arrow-up"></i>

</div>

<input type="hidden" id="token" name="token" value="<?php echo Token::generate();?>">

<input type="hidden" id="course-name" name="course-name" value="<?php echo 'inscriptos-a-' . $exhibitionURL;?>">

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/ScrollToPlugin.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js" type="text/javascript"></script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>

<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/museum/exhibitions/museum-exhibitions-requests.js" type="text/javascript"></script>

</body>

</html>
