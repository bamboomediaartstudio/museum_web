<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$url = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);

$errorReportQuery = DB::getInstance()->query("SELECT id FROM museum_bugs_subjects WHERE url = ?", array($url));

$errorReportId =  $errorReportQuery->first()->id;


$uniqueToken = Token::generate();

//museum data...

$museumData = DB::getInstance();

$museumDataQ = $museumData->query('SELECT * FROM museum_data');

if($museumDataQ->count()>0) $museumDataObject = $museumDataQ->first();

//museum social media data...

$museumSocialMediaData = DB::getInstance();

$museumSocialMediaDataQ = $museumSocialMediaData->query('SELECT * FROM museum_social_media');

if($museumSocialMediaDataQ->count()>0) $museumSocialMediaObject = $museumSocialMediaDataQ->first();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Sobre El Museo | Información General</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});
		
	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
	
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>


			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Sobre El Museo</h3>			

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>
								
								<li class="m-nav__separator">-</li>
								
								<li class="m-nav__item">
									<a href="" class="m-nav__link">
										<span class="m-nav__link-text">Información general</span>
									</a>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
				
				<div class="m-content">

					<div class="row">

						<div class="col-12">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" 
										role="tablist">

										<li class="nav-item m-tabs__item">
											
											<a class="nav-link m-tabs__link active" data-toggle="tab" href="
											#m_museum_general" role="tab">

											<i class="flaticon-share m--hide"></i>

											Información general

										</a>
									</li>

									<!--<li class="nav-item m-tabs__item">

										<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_museum_sm_tab" role="tab">

											Social Media

										</a>
									</li>-->

									<li class="nav-item m-tabs__item">

										<a class="nav-link m-tabs__link" data-toggle="tab" href="#map_tab"
										role="tab">

										Mapa

									</a>

								</li>

							</ul>

						</div>
						
					</div>

					<div class="tab-content">

						<div class="tab-pane active" id="m_museum_general">

							<form class="m-form m-form--fit m-form--label-align-right">

								<div class="m-portlet__body">

									<div class="form-group m-form__group m--margin-top-10">

										<div class="show alert m-alert m-alert--default" role="alert">
											¿Está faltando algún <strong>campo</strong> que crees que debería estar en el administrador?<a class="" href="museum_bug_report.php?id=<?php echo $errorReportId;?>"><br><br>avisanos!</a><br><br>

										</div>

									</div>

									<div class="form-group m-form__group row">

										<div class="col-10 ml-auto">
											
											<h3 class="m-form__section">1. Datos Generales</h3>

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="dates_and_times" class="col-2 col-form-label">días y horarios:</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" value="<?php echo $museumDataObject->dates_and_times; ?>" name="dates_and_times" id="dates_and_times">

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="te" class="col-2 col-form-label">Teléfono 1:</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" value="<?php echo $museumDataObject->te;?>" name="te" id="te">

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="te_2" class="col-2 col-form-label">Teléfono 2:</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" value="<?php echo $museumDataObject->te_2;?>" name="te_2" id="te_2">

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="te_3" class="col-2 col-form-label">Teléfono 3:</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" value="<?php echo $museumDataObject->te_3;?>" name="te_3" id="te_3">

										</div>

									</div>

								
									<div class="form-group m-form__group row">

										<label for="email" class="col-2 col-form-label">email 1:</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" value="<?php echo $museumDataObject->email; ?>" name="email" id="email">

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="email_2" class="col-2 col-form-label">email 2:</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" value="<?php echo $museumDataObject->email_2; ?>" name="email_2" id="email_2">

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="email_2" class="col-2 col-form-label">email 3:</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" value="<?php echo $museumDataObject->email_3; ?>" name="email_3" id="email_3">

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="address" class="col-2 col-form-label">calle:</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" value="<?php echo $museumDataObject->address; ?>" name="address" id="address">

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="address_number" class="col-2 col-form-label">Nro:</label>

										<div class="col-7">

											<input class="form-control m-input" type="number" value="<?php echo $museumDataObject->address_number; ?>" name="address_number" id="address_number">

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="zip_code" class="col-2 col-form-label">CP:</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" value="<?php echo $museumDataObject->zip_code; ?>" name="zip_code" id="zip_code">

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="city" class="col-2 col-form-label">ciudad:</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" value="<?php echo $museumDataObject->city; ?>" name="city" id="city">

										</div>

									</div>

								</div>

								<div class="m-portlet__foot m-portlet__foot--fit">

									<div class="m-form__actions">
										
										<div class="row">

											<div class="col-2"></div>

											<div class="col-7">

												<input type="hidden" name="token" value="<?php echo $uniqueToken;?>">

												<button id="update_general_info" type="reset" class="btn btn-accent m-btn m-btn--air m-btn--custom">Guardar Cambios</button>
												&nbsp;&nbsp;
												
												<button id="exit_from_general" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancelar</button>

											</div>

										</div>

									</div>

								</div>

							</form>

						</div>

						<div class="tab-pane" id="m_museum_sm_tab">

							<form id="change-password-form" class="m-form m-form--fit m-form--label-align-right">

								<div class="m-portlet__body">

									<div class="form-group m-form__group m--margin-top-10">

										<div class="show alert m-alert m-alert--default" role="alert">
											¿Está faltando alguna <strong>red social</strong> que el museo usa y no la encontrás en la lista?<br><br>

											<a class="btn btn-xs btn-success" href="mailto:info@aditivointeractivegroup.com">Avisanos</a>

										</div>

									</div>


									<div class="form-group m-form__group row">

										<div class="col-10 ml-auto">
											
											<h3 class="m-form__section">1 . Redes Sociales</h3>

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="linkedin" class="col-2 col-form-label">Linkedin:</label>

										<div class="m-input-icon m-input-icon--left col-7">

											<input class="form-control m-input" type="text" name="linkedin" id="linkedin" value="<?php echo $museumSocialMediaObject->linked_in;?>">

											<span class="m-input-icon__icon m-input-icon__icon--left">

												<span><i class="fab fa-linkedin-in"></i></span>

											</span>

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="facebook" class="col-2 col-form-label">Facebook:</label>

										<div class="m-input-icon m-input-icon--left col-7">

											<input class="form-control m-input" type="text" name="facebook" id="facebook" value="<?php echo $museumSocialMediaObject->facebook;?>">

											<span class="m-input-icon__icon m-input-icon__icon--left">

												<span><i class="fab fa-facebook"></i></span>

											</span>

										</div>
										
									</div>

									<div class="form-group m-form__group row">
										
										<label for="twitter" class="col-2 col-form-label">Twitter:</label>
										
										<div class="m-input-icon m-input-icon--left col-7">

											<input class="form-control m-input" type="text" name="twitter" 
											id="twitter" value="<?php echo $museumSocialMediaObject->twitter;?>">

											<span class="m-input-icon__icon m-input-icon__icon--left">

												<span><i class="fab fa-twitter"></i></span>

											</span>

										</div>

									</div>

									<div class="form-group m-form__group row">
										
										<label for="instagram" class="col-2 col-form-label">Instagram:</label>
										
										<div class="m-input-icon m-input-icon--left col-7">

											<input class="form-control m-input" type="text" name="instagram" id="instagram" value="<?php echo $museumSocialMediaObject->instagram;?>">

											<span class="m-input-icon__icon m-input-icon__icon--left">

												<span><i class="fab fa-instagram"></i></span>

											</span>

										</div>

									</div>

									<div class="form-group m-form__group row">
										
										<label for="youtube" class="col-2 col-form-label">YouTube:</label>
										
										<div class="m-input-icon m-input-icon--left col-7">

											<input class="form-control m-input" type="text" name="youtube" id="youtube" value="<?php echo $museumSocialMediaObject->youtube;?>">

											<span class="m-input-icon__icon m-input-icon__icon--left">

												<span><i class="fab fa-youtube"></i></span>

											</span>

										</div>

									</div>

									<div class="form-group m-form__group row">
										
										<label for="vimeo" class="col-2 col-form-label">Vimeo:</label>
										
										<div class="m-input-icon m-input-icon--left col-7">

											<input class="form-control m-input" type="text" name="vimeo" id="vimeo" value="<?php echo $museumSocialMediaObject->vimeo;?>">

											<span class="m-input-icon__icon m-input-icon__icon--left">

												<span><i class="fab fa-vimeo"></i></span>

											</span>

										</div>

									</div>

									<div class="form-group m-form__group row">
										
										<label for="flickr" class="col-2 col-form-label">Flickr:</label>
										
										<div class="m-input-icon m-input-icon--left col-7">

											<input class="form-control m-input" type="text" name="flickr" id="flickr" value="<?php echo $museumSocialMediaObject->flickr;?>">

											<span class="m-input-icon__icon m-input-icon__icon--left">

												<span><i class="fab fa-flickr"></i></span>

											</span>

										</div>

									</div>
									

									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>

								</div>

								<div class="m-portlet__foot m-portlet__foot--fit">

									<div class="m-form__actions">
										
										<div class="row">

											<div class="col-2"></div>

											<div class="col-7">

												<input type="hidden" name="token" value="<?php echo $uniqueToken;?>">

												<button id="update_social_media" type="reset" class="btn btn-accent m-btn m-btn--air m-btn--custom">Guardar Cambios</button>
												&nbsp;&nbsp;
												<button id="exit_from_social_media" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancelar</button>

											</div>

										</div>

									</div>

								</div>

							</form>

						</div>

						<div class="tab-pane" id="map_tab">

							<form class="m-form m-form--fit m-form--label-align-right">

								<div class="m-portlet__body">

									<div class="form-group m-form__group m--margin-top-10">

										<div class="show alert m-alert m-alert--default" role="alert">
											<strong>¿Qué es esto?</strong><br><br>La geolocación ayuda a los visitantes a poder ubicar al museo de forma <strong>rápida y sencilla</strong>.<br>

											Actualmente se está calculando la dirección exacta en función de la dirección suministrada:<br><br>

											<strong><?php echo $museumDataObject->address . ' ' . $museumDataObject->address_number . ', ' . $museumDataObject->zip_code . ' ' . $museumDataObject->city ;?></strong>

											<br><br>Si encontrás algún error, avisanos a través del siguiente link: </strong><br><br>

											<a class="btn btn-xs btn-success" href="mailto:info@aditivointeractivegroup.com">informar error</a><br><br>

											Si querés editar la <strong>latitud</strong> y <strong>longitud</strong>, presioná el siguiente botón:<br><br>

											<a id="edit-map" class="btn-red btn btn-xs btn-danger" href="mailto:info@aditivointeractivegroup.com">editar</a><br><br>


										</div>

									</div>

									<div class="form-group m-form__group row">

										<div class="col-10 ml-auto">
											
											<h3 class="m-form__section">1. Geolocacion</h3>

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="latitud" class="col-2 col-form-label">Latitud:</label>

										<div class="m-input-icon m-input-icon--left col-7">

											<input disabled  class="form-control m-input" type="number" name="latitud" id="latitud" value="<?php echo $museumDataObject->lat;?>">

											<span class="m-input-icon__icon m-input-icon__icon--left">

												<span><i class="fa fa-map-marker"></i></span>

											</span>

										</div>
										
									</div>

									<div class="form-group m-form__group row">

										<label for="longitud" class="col-2 col-form-label">longitud:</label>

										<div class="m-input-icon m-input-icon--left col-7">

											<input disabled class="form-control m-input" type="number" name="longitud" id="longitud" value="<?php echo $museumDataObject->lon;?>">

											<span class="m-input-icon__icon m-input-icon__icon--left">

												<span><i class="fa fa-map-marker"></i></span>

											</span>

										</div>
										
									</div>

									<div class="form-group m-form__group row">

										<div class="offset-2 col-7" id="map"></div>

									</div>

								</div>

								<div class="m-portlet__foot m-portlet__foot--fit">

									<div class="m-form__actions">
										
										<div class="row">

											<div class="col-2"></div>

											<div class="col-7">

												<input type="hidden" name="token" value="<?php echo $uniqueToken;?>">

												<button id="update_general_info" type="reset" class="btn btn-accent m-btn m-btn--air m-btn--custom">Guardar Cambios</button>
												&nbsp;&nbsp;
												
												<button id="exit_from_map" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancelar</button>

											</div>

										</div>

									</div>

								</div>

							</form>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

</div>

<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
	<i class="la la-arrow-up"></i>
</div>

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="//maps.google.com/maps/api/js?key=AIzaSyDBGVDv5fOFgfW4ixNZL_2krgkriGu6vvc" type="text/javascript"></script>

<script src="./assets/vendors/custom/gmaps/gmaps.js" type="text/javascript"></script>

<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/museum/general-info.js" type="text/javascript"></script>


</body>

</html>
