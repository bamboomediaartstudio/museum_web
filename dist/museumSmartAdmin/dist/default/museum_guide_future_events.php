<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$today = date('Y-m-d');

if($user->data()->group_id == 1 || $user->data()->group_id == 2 || $user->data()->group_id == 3){

	if(Input::exists('get')){

		if(Input::get('email')){

			$email = Input::get('email');

			$finalName = Input::get('name') . ' ' . Input::get('surname');

		}

	}

}else if($user->data()->group_id == 4){

	$email = $user->data()->email;

	$finalName = $user->data()->name . ' ' . $user->data()->surname;

}

$query = DB::getInstance()->query(

	"SELECT * FROM museum_booking_guides as mbg

	INNER JOIN museum_guides as mg ON

	mbg.id_guide = mg.id

	INNER JOIN users as u ON

	mg.email = u.email 

	INNER JOIN museum_booking as mb ON

	mbg.id_booking = mb.id
	
	LEFT JOIN museum_booking_institutions_relations AS mbir
	
	ON mbir.id_booking = mb.id
	
	LEFT JOIN museum_booking_institutions as mbi
	
	ON mbi.id = mbir.id_institution

	WHERE u.email = ? AND mb.sid = ? AND mb.date_start >= ? AND mb.booked_places = ? ORDER BY mb.date_start ASC", [$email, 1, $today, 1]);

$results = $query->results();

//query 2...

$query2 = DB::getInstance()->query(

	"SELECT * FROM museum_booking_guides as mbg

	INNER JOIN museum_guides as mg ON

	mbg.id_guide = mg.id

	INNER JOIN users as u ON

	mg.email = u.email 

	LEFT JOIN museum_booking as mb ON

	mbg.id_booking = mb.id
	
	LEFT JOIN museum_booking_institutions_relations AS mbir
	
	ON mbir.id_booking = mb.id
	
	LEFT JOIN museum_booking_institutions as mbi
	
	ON mbi.id = mbir.id_institution

	WHERE u.email = ? AND mb.sid = ? AND mb.date_start >= ? ORDER BY mb.date_start ASC", [$email, 2, $today]);

$results2 = $query2->results();

$query3 = DB::getInstance()->query(

	"SELECT * FROM museum_booking_guides as mbg

	INNER JOIN museum_guides as mg ON

	mbg.id_guide = mg.id

	INNER JOIN users as u ON

	mg.email = u.email 

	LEFT JOIN museum_booking as mb ON

	mbg.id_booking = mb.id
	
	LEFT JOIN museum_booking_institutions_relations AS mbir
	
	ON mbir.id_booking = mb.id
	
	LEFT JOIN museum_booking_institutions as mbi
	
	ON mbi.id = mbir.id_institution

	WHERE u.email = ? AND mb.sid = ? AND mb.date_start >= ? ORDER BY mb.date_start ASC", [$email, 3, $today]);

$results3 = $query3->results();

?>


<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Historial de guía | Lista</title>

	<meta name="description" content="online booking">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.css"/>



	<link href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.1/cropper.min.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

		.table-cell-edit{

			background-color: #efefef !important;

			cursor:move;

			border: 1px solid #efefef;

			-webkit-box-shadow: 5px 0 5px -2px #ddd;

			box-shadow: 5px 0 5px -2px #ddd;

		}

		.modal-lg {

			min-width: 80%;

			/*margin: auto;*/

		}

		.temp-test{

			max-width: 100%;

			max-height:550px;

			overflow: hidden !important;

		}

		#result{

			width: 600px;

			height:600px;

			overflow: hidden !important;

		}

		.preview-container, .real-container{

			overflow: hidden !important;

		}


	</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >


	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title" id="exampleModalLabel"></h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>
				
				<div class="modal-body">
					...
				</div>
				
				<div class="modal-footer">

					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
				<i class="la la-close"></i>
			</button>
			<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

				<?php require_once 'private/includes/sidebar.php'; ?>

			</div>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">

								Historial de turno asignados a <?php echo $finalName;?>


							</h3>

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon">

										<i class="m-nav__link-icon la la-home"></i>

									</a>

								</li>

							</ul>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">

						<div class="m-alert__icon"><i class="flaticon-exclamation m--font-brand"></i></div>

						<div class="m-alert__text">

							Tus próximos turnos! :)<br><br>

						</div>

					</div>

					<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">

						<div class="m-portlet__head">

							<div class="m-portlet__head-tools">

								<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" 
								role="tablist">

								<li class="nav-item m-tabs__item" data-id="tab_1">

									<a class="nav-link m-tabs__link active" data-toggle="tab" href="#tab_1" role="tab">

										<i class="flaticon-share m--hide"></i>VISITAS GRUPALES

									</a>

								</li>

								<li class="nav-item m-tabs__item" data-id="tab_2">

									<a class="nav-link m-tabs__link" data-toggle="tab" href="#tab_2" role="tab">

										<i class="flaticon-share m--hide"></i>VISITAS INDIVIDUALES

									</a>

								</li>

								<li class="nav-item m-tabs__item" data-id="tab_3">

									<a class="nav-link m-tabs__link" data-toggle="tab" href="#tab_3" role="tab">

										<i class="flaticon-share m--hide"></i>VISITAS INSTITUCIONALES

									</a>

								</li>

							</ul>

						</div>

					</div>

					<div class="tab-content">

						<div class="tab-pane active" id="tab_1">

							<div class="m-portlet__body">

								<p>PODES EXPORTAR ESTA LISTA EN LOS SIGUIENTES FORMATOS:</p>


								<table id="tab-1" class="table display table-striped table-bordered responsive nowrap" style="width:100%">

									<thead>

										<tr>

											<th>N#</th>

											<th>Institución</th>

											<th>Fecha</th>

											<th>Hora</th>

											<th>estado</th>
																						
										</tr>

									</thead>

									<tbody>

										<?php

										$counter = 0;

										foreach($results as $result){

											$counter+=1;

											$dt = new DateTime($result->date_start);

									        $date = $dt->format('m/d/Y');

									        $time = $dt->format('H:i:s');

											 ?>

											<tr class="row-<?php echo $counter;?>">

												<td><?php echo $counter;?></td>
												
												<td><?php echo $result->institution_name;?></td>
												
												<td><?php echo $date;?></td>

												<td><?php echo $time;?></td>

												<td>
													<?php if($result->booked_places == 1){ ?>

													<button class="btn-success btn btn-sm disable">confirmada</button>

													<?php } else{ ?>

														<button class="text-white btn-warning btn btn-sm disable">pendiente</button>

													<?php } ?>
												
												</td>
																																																
											</tr>

										<?php } ?>

									</tbody>

								</table>

							</div>

						</div>

						<div class="tab-pane" id="tab_2">

							<div class="m-portlet__body">

								<p>PODES EXPORTAR ESTA LISTA EN LOS SIGUIENTES FORMATOS:</p>


								<table id="tab-2" class="table display2 table-striped table-bordered responsive nowrap" style="width:100%">

									<thead>

										<tr>

											<th>N#</th>

											<th>Fecha</th>

											<th>Hora</th>

											<th>estado</th>
																						
										</tr>

									</thead>

									<tbody>

										<?php

										$counter = 0;

										foreach($results2 as $result2){

											$counter+=1;

											$dt = new DateTime($result2->date_start);

									        $date = $dt->format('m/d/Y');

									        $time = $dt->format('H:i:s');

											 ?>

											<tr class="row-<?php echo $counter;?>">

												<td><?php echo $counter;?></td>
																								
												<td><?php echo $date;?></td>

												<td><?php echo $time;?></td>

												<td>

													<button class="text-white btn-danger btn btn-sm disable"><?php echo 'reservas: ' . $result2->booked_places . ' de ' . $result2->available_places;?></button>

												
												</td>
																																																
											</tr>

										<?php } ?>

									</tbody>

								</table>

							</div>

						</div>

						<div class="tab-pane" id="tab_3">

							<div class="m-portlet__body">

								<p>PODES EXPORTAR ESTA LISTA EN LOS SIGUIENTES FORMATOS:</p>


								<table id="tab-3" class="table display3 table-striped table-bordered responsive nowrap" style="width:100%">

									<thead>

										<tr>

											<th>N#</th>

											<th>Fecha</th>

											<th>Hora</th>

											<th>estado</th>
																						
										</tr>

									</thead>

									<tbody>

										<?php

										$counter = 0;

										foreach($results3 as $result3){

											$counter+=1;

											$dt = new DateTime($result3->date_start);

									        $date = $dt->format('m/d/Y');

									        $time = $dt->format('H:i:s');

											 ?>

											<tr class="row-<?php echo $counter;?>">

												<td><?php echo $counter;?></td>
																								
												<td><?php echo $date;?></td>

												<td><?php echo $time;?></td>

												<td>
													<?php echo $result3->notes;?>
												</td>
																																																
											</tr>

										<?php } ?>

									</tbody>

								</table>

							</div>

						</div>

						

					</div>

				</div>

			</div>

		</div>

	</div>

	<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">

	<i class="la la-arrow-up"></i>

</div>

<input type="hidden" id="token" name="token" value="<?php echo Token::generate();?>">

<input type="hidden" id="historical-list" name="historical-list" value="visitas-individuales-historicas">

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/ScrollToPlugin.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>



<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/museum/guides/museum-guide-historic-view.js" type="text/javascript"></script>

</body>

</html>
