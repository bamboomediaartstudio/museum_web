<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$isHistorial;

$inscriptionsList;

if(Input::exists('get')){

	if(Input::get('id')){

		$id = Input::get('id');

		//booking query...

		setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

		$bookingQuery = DB::getInstance()->query(

			"SELECT * FROM museum_booking as mb WHERE mb.id = ? ", array($id));

		$booking = $bookingQuery->first();

		$dateStart =  $booking->date_start;

		$dateEnd =  $booking->date_end;

		$stringDate = utf8_encode(strftime("%A, %d de %B del %Y", strtotime($booking->date_start)));

		$stringStartTime = strftime("%H:%M:%S", strtotime($booking->date_start));

		$strirngEndTime = strftime("%H:%M:%S", strtotime($booking->date_end));

		//...

		//institutional query

		$institutionQuery = DB::getInstance()->query('SELECT * FROM museum_booking_institutions_relations WHERE id_booking = ?', [$id]);

		$institutionId = $institutionQuery->first()->id_institution;

		$participantsQuery = DB::getInstance()->query(

			"SELECT * FROM museum_booking_institutions as mbi WHERE active = ? AND deleted = ? AND id = ? ORDER BY mbi.id DESC", [1, 0, $institutionId]);

		$inscriptionsList = $participantsQuery->results();

		//...

		//all the reservations...

		$allTheReservations = DB::getInstance()->query('SELECT * FROM museum_booking_institutions_relations as mbir LEFT JOIN 

			museum_booking as mb ON mbir.id_booking = mb.id 

			WHERE mbir.id_institution = ? AND mbir.id_booking != ? ', [$institutionId, $id]);

		$allResultsCount = $allTheReservations->count();

		$allResults = $allTheReservations->results();

		$isHistorial = false;

	}else if(Input::get('from') && Input::get('to')){


		$participantsQuery = DB::getInstance()->query('SELECT *, mbi.id as id FROM museum_booking_institutions as mbi inner join museum_booking_institutions_relations as mbir on mbi.id = mbir.id_institution inner join museum_booking as mb on mbir.id_booking = mb.id WHERE mb.date_start >= ? AND mb.date_end <= ? AND mbi.active = ? AND mbi.deleted = ? GROUP BY mbi.id ORDER BY mbi.id DESC', [Input::get('from'), Input::get('to'), 1, 0]);

		$inscriptionsList = $participantsQuery->results();

		$isHistorial = true;

	}

}else{

	$participantsQuery = DB::getInstance()->query(

		"SELECT * FROM museum_booking_institutions as mbi WHERE active = ? AND deleted = ? GROUP BY mbi.id ORDER BY mbi.id DESC", [1, 0]);

	$inscriptionsList = $participantsQuery->results();

	$isHistorial = true;

}


?>


<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<?php if($isHistorial == true){ ?>

		<title>Museo de la Shoá | Historial de Visitas | Instituciones</title>

	<?php }else{ ?>

		<title>Museo de la Shoá | Institución registrada</title>

	<?php } ?>

	<meta name="description" content="online booking">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.css"/>



	<link href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.1/cropper.min.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

		.table-cell-edit{

			background-color: #efefef !important;

			cursor:move;

			border: 1px solid #efefef;

			-webkit-box-shadow: 5px 0 5px -2px #ddd;

			box-shadow: 5px 0 5px -2px #ddd;

		}

		.modal-lg {

			min-width: 80%;

			/*margin: auto;*/

		}

		.temp-test{

			max-width: 100%;

			max-height:550px;

			overflow: hidden !important;

		}

		#result{

			width: 600px;

			height:600px;

			overflow: hidden !important;

		}

		.preview-container, .real-container{

			overflow: hidden !important;

		}


	</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >


	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title" id="exampleModalLabel"></h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>
				
				<div class="modal-body">
					...
				</div>
				
				<div class="modal-footer">

					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
				<i class="la la-close"></i>
			</button>
			<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

				<?php require_once 'private/includes/sidebar.php'; ?>

			</div>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">

								<?php if($isHistorial == true){ ?>

									Historial de Instituciones registradas

								<?php }else{ ?>

									Institución registrada

								<?php } ?>


							</h3>

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon">

										<i class="m-nav__link-icon la la-home"></i>

									</a>

								</li>

							</ul>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">

						<div class="m-alert__icon"><i class="flaticon-exclamation m--font-brand"></i></div>

						<div class="m-alert__text">

							<?php if($isHistorial == true){ ?>

								Esta página incluye registros históricos de las <strong>instituciones</strong> que tramitaron visitas.<br><br>

							<?php }else{ ?>

								Esta página incluye información respecto a la institución que solicitó este turno.<br><br>

								Día de la visita guiada: <strong><?php echo $stringDate;?></strong>	
								<br>
								<b>Hora de inicio: </b> <?php echo $stringStartTime;?><br>

								<b>Hora de cierre: </b> <?php echo $strirngEndTime;?><br>

								<?php

								if($allResultsCount > 0){  ?>

									<br><p>La institución también solicitó los siguientes turnos:</p>

									<?php foreach($allResults as $other){

										$stringDate = utf8_encode(strftime("%A, %d de %B del %Y", strtotime($other->date_start)));

										$stringStartTime = strftime("%H:%M", strtotime($other->date_start));

										$strirngEndTime = strftime("%H:%M", strtotime($other->date_end));

										echo '<a href="museum_booking_event_list.php?id=' . $other->id_booking .'">' .  $stringDate . ' de ' . $stringStartTime . ' a ' . $strirngEndTime . '</a><br>';

									}

								}

								?>

							<?php } ?>

							<?php if($isHistorial == false){ ?>


								<br><p>Podés añadir y quitar turnos o editar información de la institución presionando el botón de editar.</p>

								<?php $finalURL = 'museum_institution_add.php?id=' . $institutionId;?>

								<a class="btn btn-xs btn-success" href="<?php echo $finalURL;?>">Editar</a>

							<?php } ?>

						</div>

					</div>

					<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">

						<div class="m-portlet__head">

							<div class="m-portlet__head-tools">

								<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" 
								role="tablist">

								<li class="nav-item m-tabs__item" data-id="">

									<a class="nav-link m-tabs__link  active" data-toggle="tab_1" href="" role="tab">

										<i class="flaticon-share m--hide"></i>Listado

									</a>

								</li>

							</ul>

						</div>

					</div>

					<div class="tab-content">

						<div class="tab-pane active" id="tab_1">

							<div class="m-portlet__body">


								<table id="tab-1" class="table display table-striped table-bordered responsive" style="width:100%">

									<thead>

										<tr>

											<th>Institución</th>

											<th>Email</th>

											<th>Teléfono</th>

											<th>beca</th>

											<th>abonado</th>
											
											<th>subido</th>

											<th>asistió</th>
											
											<th>Acciones</th>

										</tr>

									</thead>

								</table>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

	<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">

	<i class="la la-arrow-up"></i>

</div>

<input type="hidden" id="token" name="token" value="<?php echo Token::generate();?>">

<input type="hidden" id="historical-list" name="historical-list" value="visitas-individuales-historicas">

<input type="hidden" id="table-type" name="table-type" value="<?php echo $isHistorial;?>">

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/ScrollToPlugin.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.5.0/r-2.2.2/rg-1.1.0/rr-1.2.4/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>

<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/museum/booking/museum-historical-institutions.js" type="text/javascript"></script>

</body>

</html>
