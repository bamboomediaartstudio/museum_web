<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$actualId = Input::get('id');

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

$states = ['Buenos Aires', 'Catamarca', 'Chaco', 'Chubut', 'Cordoba', 'Corrientes', 'Entre Rios', 'Formosa', 'Jujuy', 'La Pampa', 'La Rioja', 'Mendoza', 'Misiones', 'Neuquen',  'Rio Negro', 'Salta', 'San Juan', 'San Luis', 'Santa Cruz', 'Santa Fe, Sgo. del Estero', 'Tierra del Fuego', 'Tucuman'];

$iva = ['Consumidor final', 'Exento', 'Responsable monotributo', 'No respsonsable', 'Cliente del exterior', 'Proveedor del exterior', 'Responsable inscripto', 
'Sujeto no categorizado', 'Otro'];

$level = ['Primario', 'Secundario', 'Terciario', 'Universitario', 'Educación No Formal'];

$institutionsTypes = ['publica', 'privada'];

$needs = ['si', 'no'];

if(Input::exists("get") && Input::get('id')){

	$editMode = true;

	$mainQuery = DB::getInstance()->query('SELECT * FROM  museum_booking_institutions  

		WHERE id=? AND deleted = ?', [Input::get('id'), 0]);

	$actualInstitution = $mainQuery->first();

	$institutionName = $actualInstitution->institution_name;
	
	$institutionStudents = $actualInstitution->institution_students;

	$institutionCuit = $actualInstitution->institution_cuit;

	$institutionAddress = $actualInstitution->institution_address;

	$institutionPhone = $actualInstitution->institution_phone;

	$institutionEmail = $actualInstitution->institution_email;

	$institutionState = $actualInstitution->institution_state;
	
	$institutionIva = $actualInstitution->iva;

	$educationLevel = $actualInstitution->eduction_level;
	
	$institutionType = $actualInstitution->institution_type;

	$specialNeeds = $actualInstitution->special_need;

	$contactName = $actualInstitution->inscription_contact_name;

	$contactEmail = $actualInstitution->inscription_contact_email;

	$contactPhone = $actualInstitution->inscription_contact_phone;


	
}else{

	$editMode = false;

	$institutionName = '';

	$institutionStudents = '';

	$institutionCuit = '';

	$institutionAddress = '';
	
	$institutionPhone = '';
	
	$institutionEmail = '';

	$institutionState = '';

	$institutionIva = '';

	$educationLevel = '';

	$institutionType = '';

	$specialNeeds = '';

	$contactName = '';

	$contactEmail = '';

	$contactPhone = '';
}

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Guías | Agregar guía</title>

	<meta name="description" content="add new guide">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{
		
		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	#map {

		height: 500px;

		width:100%;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}

	.is-not-selectable{

		opacity: .5;
		text-decoration:line-through
	}

	.date-div{

		cursor:pointer;
	
	}

</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div id="myModal" class="modal" tabindex="-1" role="dialog">
		
		<div class="modal-dialog" role="document">
			
			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title">Asignar descripción a la imagen</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body">

					<form>

						<div class="form-group">

							<label for="image-	" class="col-form-label">Descripción:</label>

							<input type="text" class="form-control" id="image-description">

						</div>
						
					</form>

				</div>

				<div class="modal-footer">

					<button type="button" class="save-description btn btn-primary">guardar</button>

					<button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<input type="hidden" id="image-path" name="image-path" value="<?php echo $imagePath;?>">

		<input type="hidden" id="institution-id" name="institution-id" value="<?php echo $actualId;?>">
		
		<input type="hidden" id="institution-name" name="institution-name" value="<?php echo $objectTitle;?>">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Guías</h3>		
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>
								
								<li class="m-nav__separator">-</li>
								
								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text">Agregar guía</span>

									</a>

								</li>
								
							</ul>

						</div>
						
					</div>

				</div>
				
				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link active" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Información general

												</a>

											</li>


										</ul>

									</div>

								</div>

								
								<div class="tab-content">

									<div class="tab-pane active" id="general-tab">

										<form id="add-data-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>	

												</div>

											</div>

											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">
													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															INFORMACIÓN INSTITUCIONAL
															<br>

														</h3>

													</div>

												</div>

											</div>

											<!-- nombre -->

											<div class="form-group m-form__group row pt-4">

												<label for= "institution_phone" class="col-form-label col-lg-3 col-12">Nombre:</label>

												<div class="col-lg-4 col-md-9 col-8">

													<input id="institution_name" type="text" class="form-control m-input" name="institution_name" placeholder="Nombre de la institución" value="<?php echo $institutionName;?>">

												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $institutionName;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<!-- students! -->

											<div class="form-group m-form__group row pt-4">

												<label for = "institution_students" class="col-form-label col-lg-3 col-12">Cantidad de alumnos:</label>

												<div class="col-lg-4 col-md-9 col-8">

													<select class="form-control" id="institution_students" name="institution_students">

														<?php for($i = 0; $i<100; $i++){ 

															if($i == $institutionStudents){

																$selected = 'selected';

															}else{

																$selected = '';

															}

															?>

															<option value="<?php echo $i;?>" <?php echo $selected;?>><?php echo $i;?></option>

														<?php } ?>

													</select>

												</div>

											</div>

											<!-- booking! -->

											<div class="form-group m-form__group row pt-4">

												<label for = "students-selector" class="col-form-label col-lg-3 col-12">turnos asignados:<br>	<small>Podés eliminar o agregar turnos</small>
													

												</label>

												<div class="col-lg-4 col-md-9 col-8">

													<?php  

													$bookings = DB::getInstance()->query('SELECT * FROM museum_booking_institutions_relations as mbir

														LEFT JOIN museum_booking as mb ON mbir.id_booking = mb.id WHERE mbir.id_institution = ?', [Input::get('id')]);

													foreach($bookings->results() as $res){ 

														$isSelectable = '';

														if(date($res->date_start) < date("Y-m-d H:i:s")){

															$isSelectable = 'is-not-selectable';

														}else{

															$isSelectable = '';
														}

														?>

														<div <?php if($isSelectable != '') echo 'data-toggle="tooltip" data-placement="top" title="Este turno ya pasó y no se lo puede eliminar."';?> class="date-div <?php echo $isSelectable;?>">

															<p><?php echo utf8_encode(strftime("%A, %d de %B del %Y", strtotime($res->date_start))) . ' de ' . strftime("%H:%M:", strtotime($res->date_start)) . ' a ' . strftime("%H:%M", strtotime($res->date_end)) 

															?>

															<button type = "button" class="delete_row m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete booking"

															data-booking-id = '<?php echo $res->id;?>'

															>

															<i class="la la-trash"></i>

														</button>

													</p>

												</div>

											<?php  } ?>


										</div>

									</div>

									<!-- nuevos turnos! -->

									<div class="form-group m-form__group row pt-4">

										<label for = "booking_add" class="col-form-label col-lg-3 col-12">Agregar turno:</label>

										<div class="col-lg-4 col-md-9 col-8">

											<select class="form-control" id="booking_add" name="booking_add">

												<?php 

												$availableBookings = DB::getInstance()->query('SELECT * FROM museum_booking WHERE booked_places = ? AND date_start > ? ORDER BY date_start', [0, date('Y-m-d')]);?>

												    <option value="" disabled selected>Selccioná un turno</option>

												    <?php

												foreach($availableBookings->results() as $result){ ?>

													<option value="<?php echo $result->id;?>"><?php echo utf8_encode(strftime("%A, %d de %B del %Y", strtotime($result->date_start))) . ' de ' . strftime("%H:%M:", strtotime($result->date_start)) . ' a ' . strftime("%H:%M", strtotime($result->date_end)) 
													;?>
														

													</option>

												<?php } ?>

											</select>

										</div>

									</div>



									<!-- cuit -->

									<div class="form-group m-form__group row pt-4">

										<label for = "institution_cuit" class="col-form-label col-lg-3 col-12">Cuit:</label>

										<div class="col-lg-4 col-md-9 col-8">

											<input id="institution_cuit" type="text" class="form-control m-input" name="institution_cuit" placeholder="Cuit de la institución" value="<?php echo $institutionCuit;?>">


										</div> 

										<?php if($editMode){ ?>

											<div class="">

												<button data-db-value="<?php echo $institutionCuit;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
												</button>

											</div>

										<?php }?>

									</div>

									<!-- IVA -->

									<div class="form-group m-form__group row pt-4">

										<label for = "iva" class="col-form-label col-lg-3 col-12">Situación frente al IVA:</label>

										<div class="col-lg-4 col-md-9 col-8">

											<select class="form-control" id="iva" name="iva">

												<?php foreach($iva as $situation){ 

													if($institutionIva == $situation){

														$selected = 'selected';

													}else{

														$selected = '';

													}

													?>

													<option value="<?php echo $situation;?>" <?php echo $selected;?>><?php echo $situation;?></option>

												<?php } ?>

											</select>

										</div>

									</div>

									<!-- address -->

									<div class="form-group m-form__group row pt-4">

										<label for = "institution_address" class="col-form-label col-lg-3 col-12">Dirección:</label>

										<div class="col-lg-4 col-md-9 col-8">

											<input id="institution_address" type="text" class="form-control m-input" name="institution_address" placeholder="dirección de la institución" value="<?php echo $institutionAddress;?>">


										</div> 

										<?php if($editMode){ ?>

											<div class="">

												<button data-db-value="<?php echo $institutionAddress;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
												</button>

											</div>

										<?php }?>

									</div>

									<!-- Teléfono -->

									<div class="form-group m-form__group row pt-4">

										<label for = "institution_phone" class="col-form-label col-lg-3 col-12">Teléfono:</label>

										<div class="col-lg-4 col-md-9 col-8">

											<input id="institution_phone" type="text" class="form-control m-input" name="institution_phone" placeholder="teléfono de la institución" value="<?php echo $institutionPhone;?>">


										</div> 

										<?php if($editMode){ ?>

											<div class="">

												<button data-db-value="<?php echo $institutionPhone;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
												</button>

											</div>

										<?php }?>

									</div>

									<!-- email -->

									<div class="form-group m-form__group row pt-4">

										<label for = "institution_email" class="col-form-label col-lg-3 col-12">Email:</label>

										<div class="col-lg-4 col-md-9 col-8">

											<input id="institution_email" type="text" class="form-control m-input" name="institution_email" placeholder="email de la institución" value="<?php echo $institutionEmail;?>">


										</div> 

										<?php if($editMode){ ?>

											<div class="">

												<button data-db-value="<?php echo $institutionEmail;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
												</button>

											</div>

										<?php }?>

									</div>

									<!-- provincia -->

									<div class="form-group m-form__group row pt-4">

										<label for = "institution_state" class="col-form-label col-lg-3 col-12">Provincia:</label>

										<div class="col-lg-4 col-md-9 col-8">

											<select class="form-control" id="institution_state" name="institution_state">

												<?php foreach($states as $state){ 

													if($institutionState == $state){

														$selected = 'selected';

													}else{

														$selected = '';

													}

													?>

													<option value="<?php echo $state;?>" <?php echo $selected;?>><?php echo $state;?></option>

												<?php } ?>

											</select>

										</div>

									</div>

									<!-- education level -->

									<div class="form-group m-form__group row pt-4">

										<label for = "eduction_level" class="col-form-label col-lg-3 col-12">Nivel de educación:</label>

										<div class="col-lg-4 col-md-9 col-8">

											<select class="form-control" id="eduction_level" name="eduction_level">

												<?php foreach($level as $lv){ 

													if(strtolower($educationLevel) == strtolower ($lv)){

														$selected = 'selected';

													}else{

														$selected = '';

													}

													?>

													<option value="<?php echo $lv;?>" <?php echo $selected;?>><?php echo $lv;?></option>

												<?php } ?>

											</select>

										</div>

									</div>

									<!-- institution type -->

									<div class="form-group m-form__group row pt-4">

										<label for = "institution_type" class="col-form-label col-lg-3 col-12">Tipo:</label>

										<div class="col-lg-4 col-md-9 col-8">

											<select class="form-control" id="institution_type" name="institution_type">

												<?php foreach($institutionsTypes as $tp){ 

													if(strtolower($institutionType) == strtolower ($tp)){

														$selected = 'selected';

													}else{

														$selected = '';

													}

													?>

													<option value="<?php echo $tp;?>" <?php echo $selected;?>><?php echo $tp;?></option>

												<?php } ?>

											</select>

										</div>

									</div>

									<!-- special neeeds -->

									<div class="form-group m-form__group row pt-4">

										<label for = "special_need" class="col-form-label col-lg-3 col-12">Necesidades especiales:</label>

										<div class="col-lg-4 col-md-9 col-8">

											<select class="form-control" id="special_need" name="special_need">

												<?php foreach($needs as $nd){ 

													if(strtolower($specialNeeds) == strtolower ($nd)){

														$selected = 'selected';

													}else{

														$selected = '';

													}

													?>

													<option value="<?php echo $nd;?>" <?php echo $selected;?>><?php echo $nd;?></option>

												<?php } ?>

											</select>

										</div>

									</div>

									<div class="m-portlet__head">

										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">

												<h3 class="m-portlet__head-text">

													PERSONA DE CONTACTO
													<br>

												</h3>

											</div>

										</div>

									</div>

									<!-- nombre -->

									<div class="form-group m-form__group row pt-4">

										<label for= "inscription_contact_name" class="col-form-label col-lg-3 col-12">Nombre:</label>

										<div class="col-lg-4 col-md-9 col-8">

											<input id="inscription_contact_name" type="text" class="form-control m-input" name="inscription_contact_name" placeholder="Nombre la persona de contacto" value="<?php echo $contactName;?>">

										</div> 

										<?php if($editMode){ ?>

											<div class="">

												<button data-db-value="<?php echo $contactName;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
												</button>

											</div>

										<?php }?>

									</div>

									<!-- email! -->

									<div class="form-group m-form__group row pt-4">

										<label for= "inscription_contact_email" class="col-form-label col-lg-3 col-12">Email:</label>

										<div class="col-lg-4 col-md-9 col-8">

											<input id="inscription_contact_email" type="text" class="form-control m-input" name="inscription_contact_email" placeholder="Email la persona de contacto" value="<?php echo $contactEmail;?>">

										</div> 

										<?php if($editMode){ ?>

											<div class="">

												<button data-db-value="<?php echo $contactEmail;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
												</button>

											</div>

										<?php }?>

									</div>

									<!-- phone -->

									<div class="form-group m-form__group row pt-4">

										<label for= "inscription_contact_phone" class="col-form-label col-lg-3 col-12">Teléfono:</label>

										<div class="col-lg-4 col-md-9 col-8">

											<input id="inscription_contact_phone" type="text" class="form-control m-input" name="inscription_contact_phone" placeholder="Teléfono de la persona de contacto" value="<?php echo $contactPhone;?>">

										</div> 

										<?php if($editMode){ ?>

											<div class="">

												<button data-db-value="<?php echo $contactPhone;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
												</button>

											</div>

										<?php }?>

									</div>


									<div class="m-portlet__foot m-portlet__foot--fit">

										<div class="m-form__actions">

											<div class="row">

												<div class="col-2"></div>

												<div class="col-7">

													<input type="hidden" name="token" value="<?php echo Token::generate()?>">

													<?php 

													$hiddenClass = ($editMode) ? 'd-none' : '';

													?>

													<button id="add_guide" type="submit" 

													class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar guía</button>
													&nbsp;&nbsp;

													<?php 

													$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

													$buttonId = 

													(!$editMode) ? 'exit_from_form' : 'back_to_list';

													?>

													<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

														<?php echo $buttonLabel;?>

													</button>

												</div>

											</div>

										</div>

									</div>

								</form>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/museum/booking/museum-institution-add.js" type="text/javascript"></script>

</body>

</html>
