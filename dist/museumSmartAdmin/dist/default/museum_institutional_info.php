<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$uniqueToken = Token::generate();

//museum data...

$url = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);

$errorReportQuery = DB::getInstance()->query("SELECT id FROM museum_bugs_subjects WHERE url = ?", array($url));

$errorReportId =  $errorReportQuery->first()->id;

$data = DB::getInstance();

$dataQ = $data->query('SELECT * FROM museum_institutional');

if($dataQ->count()>0) $instObject = $dataQ->first();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Sobre El Museo | Información Institucional</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});
		
	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
	
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>


			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Sobre El Museo</h3>			

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>
								
								<li class="m-nav__separator">-</li>
								
								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text">Información Institucional</span>

									</a>

								</li>
								
							</ul>
						</div>
						<div>

						</div>
					</div>
				</div>
				
				<div class="m-content">

					<div class="row">

						<div class="col-12">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" 
										role="tablist">

										<li class="nav-item m-tabs__item">

											<a class="nav-link m-tabs__link active" data-toggle="tab" href="
											#m_museum_general" role="tab">

											<i class="flaticon-share m--hide"></i>

											Información Institucional
											
										</a>
										
									</li>


								</ul>

							</div>

						</div>

						<div class="tab-content">

							<div class="tab-pane active" id="m_museum_general">

								<form id="institutional-form" class="m-form m-form--fit m-form--label-align-right">

									<div class="m-portlet__body">

										<div class="form-group m-form__group m--margin-top-10">

											<div class="show alert m-alert m-alert--default" role="alert">
												Si crees que está faltando algún campo de importancia, <a class="" href="museum_bug_report.php?id=<?php echo $errorReportId;?>">reportalo</a> :)

											</div>

										</div>

										<div class="form-group m-form__group row">

											<label for="about" class="col-2 col-form-label">Sobre el museo:</label>


											<div class="col-10">

												<textarea class = "form-control m-input" 

												name="about" id="about"><?php echo $instObject->about;?></textarea>
												
											</div>

										</div>


										<div class="form-group m-form__group row">

											<label for="mission" class="col-2 col-form-label">Misión:</label>


											<div class="col-10">

												<textarea class = "form-control m-input" 

												name="mission" id="mission"><?php echo $instObject->mission;?></textarea>
												
											</div>

										</div>

										<div class="form-group m-form__group row">

											<label for="vision" class="col-2 col-form-label">Visión:</label>

											<div class="col-10">

												<textarea class = "form-control m-input" 

												name="vision" id="vision"><?php echo $instObject->vision;?></textarea>
												
											</div>

										</div>

										<div class="form-group m-form__group row">

											<label for="values" class="col-2 col-form-label">Valores:</label>

											<div class="col-10">

												<textarea class = "form-control m-input" 

												name="values" id="values"><?php echo $instObject->values;?></textarea>
												
											</div>

										</div>

									</div>

									<div class="m-portlet__foot m-portlet__foot--fit">

										<div class="m-form__actions">

											<div class="row">

												<div class="col-2"></div>

												<div class="col-7">

													<input type="hidden" name="token" value="<?php echo $uniqueToken;?>">

													<button id="update_institutional_info" type="reset" class="btn btn-accent m-btn m-btn--air m-btn--custom">Guardar Cambios</button>
													&nbsp;&nbsp;

													<button id="exit_from_general" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancelar</button>

												</div>

											</div>

										</div>

									</div>

								</form>

							</div>


						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
	<i class="la la-arrow-up"></i>
</div>

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/museum/institutional-info.js" type="text/javascript"></script>


</body>

</html>
