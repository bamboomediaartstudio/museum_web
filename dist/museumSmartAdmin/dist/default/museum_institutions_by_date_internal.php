<?php

$origin = 'admin';

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$query = DB::getInstance()->query("SELECT * FROM museum_booking WHERE sid = ? AND  date(date_start) = ?", [2, date(date('y-m-d'))]);

$count = $query->count();

$results = $query->results();

$bookedPlacesCounter = 0;

foreach($results as $tester) if($tester->booked_places) $bookedPlacesCounter++;

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

$yesterday = date('Y-m-d',strtotime("-1 days"));

$today = date('Y-m-d');

//$today = strtotime(date('Y-m-d H:00:00'));

$tomorrow = date('Y-m-d',strtotime("+1 days"));

$dayAfterTomorrow = date('Y-m-d',strtotime("+2 days"));

$inThreeDays = date('Y-m-d',strtotime("+3 days"));

$first = date('Y-m-01');

$last = date('Y-m-t');

$mondayThisWeek = date("Y-m-d", strtotime('monday this week')); 

$sundayThisWeek = date("Y-m-d", strtotime('sunday this week'));


?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Filtrar instituciones</title>

	<meta name="description" content="add new news">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

		.take-assistance{
			color:white !important;
		}

		.summernote-description-error{

			color: red !important;

		}

		.museum-finder{

			cursor:pointer;

		}

		#editor {overflow:scroll; max-height:300px !important}

		input::-webkit-outer-spin-button,

		input::-webkit-inner-spin-button {

			-webkit-appearance: none;

			margin: 0;
		}

		.disabled {

			pointer-events: none !important;

			opacity: 0.4 !important;

		}

		#map {

			height: 500px;

			width:100%;

		}

		.tt-query {
			-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
			-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
			box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		}

		.tt-hint {
			color: #999
		}

		.tt-menu {    /* used to be tt-dropdown-menu in older versions */
			width: 422px;
			margin-top: 4px;
			padding: 4px 0;
			background-color: #fff;
			border: 1px solid #ccc;
			border: 1px solid rgba(0, 0, 0, 0.2);
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
			-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
			-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
			box-shadow: 0 5px 10px rgba(0,0,0,.2);
		}

		.tt-suggestion {
			padding: 3px 20px;
			line-height: 24px;
		}

		.tt-suggestion.tt-cursor,.tt-suggestion:hover {
			color: #fff;
			background-color: #0097cf;

		}

		.tt-suggestion p {
			margin: 0;
		}

		.twitter-typeahead{
			width: 100%;
		}

		.see-event{
			color: white !important;
			background-color: green !important;
		}

		.delete-event{
			color: white !important;
			background-color: red !important;
		}

		.define-ammount{
			color: white  !important;
			background-color: #27c9cf !important;
		}

		.define-guide{
			color: white  !important;
			background-color: #8873a2 !important;
		}

		.change-event-place{
			color: white  !important;
			background-color: #8873a2 !important;
		}

		.change-event-visitors{

			color: white  !important;
			background-color: #27c9cf !important;

		}

		.change-tickets-per-visitors{

			color: white  !important;

			background-color: #a2c7b5 !important;
		}

		.is-online{

			color: white !important;

			background-color: #4ebfa9 !important;

		}

		.is-offline{

			color: white !important;

			background-color: #f35872 !important;

		}

		.change-notes{

			color: white !important;

			background-color: #f06200 !important;

		}


		.trash-for-events{

			background-color: #fff;

			height: 8em;

			box-shadow: 0px 1px 15px 1px rgba(69, 65, 78, 0.08);

			border: solid 1px #f5f5f5;

		}


		.date-selector-div{

			background-color: #999 !important;

			width: 100%;
		}


		.tickets-stepper{

			background-color: #999;

			color: white;

			width: 50px;

			border:1px solid #fff !important;
		}

		.selected-date{

			cursor: pointer !important;

			background-color: #274369 !important;
		}

		.fc-highlight{
			background-color: #ff0066 !important;
		}

		.fc-bgevent{
			background-color: #ff0066 !important;
		}



	</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<?php include('../../../private/php/includes/modals/individual-reservation.php');?>

	<?php include('../../../private/php/includes/modals/group-reservation.php');?>

	<input type="hidden" id="today" name="today" value="<?php echo $today;?>">
	
	<input type="hidden" id="yesterday" name="yesterday" value="<?php echo $yesterday;?>">
	
	<input type="hidden" id="tomorrow" name="tomorrow" value="<?php echo $tomorrow;?>">

	<input type="hidden" id="first" name="first" value="<?php echo $first;?>">

	<input type="hidden" id="last" name="last" value="<?php echo $last;?>">

	<input type="hidden" id="dayAfterTomorrow" name="dayAfterTomorrow" value="<?php echo $dayAfterTomorrow;?>">
	
	<input type="hidden" id="inThreeDays" name="inThreeDays" value="<?php echo $inThreeDays;?>">
	
	<input type="hidden" id="mondayThisWeek" name="mondayThisWeek" value="<?php echo $mondayThisWeek;?>">

	<input type="hidden" id="sundayThisWeek" name="sundayThisWeek" value="<?php echo $sundayThisWeek;?>">

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title">Tomar asistencia a instituciones</h3>	
							<p>Utilizá los filtros para poder ver las instituciones que deberían presentarse en determado día/s.<br>Empleá el botón <b>HOY</b> para ver el listado de instituciones que se van a acercar hoy al museo.</p>	
							
						</div>
						
					</div>

				</div>
				
				<div class="m-content">
					
					<div class="row">

						<div class="col-lg-4">

							<div class="m-portlet" id="m_portlet">

								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">

										<div class="m-portlet__head-title">

											<span class="m-portlet__head-icon">

												<i class="flaticon-add"></i>

											</span>

											<h3 class="m-portlet__head-text">

												Seleccionar Fechas

											</h3>

										</div>

									</div>

								</div>

								<div class="m-portlet__body">

									<div id="m_calendar_external_events" class="fc-unthemed">

										<div class="align-items-center">

											<div class="form-group m-form__group row">

												<label class="col-form-label">Desde:

													<br>
													

												</label>

												<div class="col-12">

													<div id="date-from" class="input-group date">
														<input type="text" class="form-control" name="calendar-date">
														<div class="input-group-addon">
															<i class="pt-2 far fa-calendar-alt"></i>
														</div>
													</div>	


												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label">Hasta:

													<br>
													

												</label>

												<div class="col-12">

													<div id="date-to" class="input-group date">
														<input type="text" class="form-control" name="calendar-date">
														<div class="input-group-addon">
															<i class="pt-2 far fa-calendar-alt"></i>
														</div>
													</div>	


												</div>

											</div>


											
											<div class="m-separator m-separator--dashed m-separator--space"></div>


											<button type="button" class="w-100 export-excel btn btn-success" data-toggle="tooltip" data-placement="bottom" title="ir!">Ver Visitas</button>

											<br><br><p>También podés utilizar los siguientes filtros / accesos directos para simplificar y optimizar el criterio de búsqueda</p><br>

											<button type="button" class="w-100  btn btn-info go-to-yesterday" data-toggle="tooltip" data-placement="bottom" title="ver las visitas de ayer">ayer</button>

											<div class="m-separator m-separator--dashed m-separator--space"></div>

											<button type="button" class="w-100  btn btn-info go-to-today" data-toggle="tooltip" data-placement="bottom" title="ver las visitas de hoy">hoy</button>

											<div class="m-separator m-separator--dashed m-separator--space"></div>

											<button type="button" class="w-100  btn btn-info go-to-tomorrow" data-toggle="tooltip" data-placement="bottom" title="ver las visitas de mañana">mañana</button>

											<div class="m-separator m-separator--dashed m-separator--space"></div>

											<button type="button" class="w-100  btn btn-info go-to-two-days" data-toggle="tooltip" data-placement="bottom" title="ver las visitas de pasado mañana">pasado mañana</button>

											<div class="m-separator m-separator--dashed m-separator--space"></div>

											<button type="button" class="w-100  btn btn-warning text-white go-to-this-week" data-toggle="tooltip" data-placement="bottom" title="ver las visitas de esta semana">esta semana</button>

											<div class="m-separator m-separator--dashed m-separator--space"></div>

											<button type="button" class="w-100  btn btn-danger go-to-month" data-toggle="tooltip" data-placement="bottom" title="ver todas las visitas de este mes">este mes</button>

										</div>

									</div>

								</div>

							</div>		

							<!-- helpers -->				

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.es.min.js" type="text/javascript"></script>

	<script src="assets/vendors/custom/jquery-ui/jquery-ui.bundle.js" type="text/javascript"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/locale/es.js"></script>

	<script src="../../../private/vendor/gsap/src/minified/TweenMax.min.js"></script>

	<script src="../../../private/js/generalConf.min.js" type="text/javascript"></script>

	<script src="../../../private/js/helpers.min.js" type="text/javascript"></script>

	<script src="assets/app/js/museum/booking/museum-institutions-by-date-internal.js" type="text/javascript"></script>


</body>

</html>
