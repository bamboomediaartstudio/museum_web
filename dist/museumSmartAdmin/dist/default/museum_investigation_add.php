<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$actualId = Input::get('id');

if(Input::exists("get") && Input::get('id')){

	$editMode = true;

	$mainQuery = DB::getInstance()->query('SELECT * FROM  museum_investigations  

		WHERE id = ? AND deleted = ?', [Input::get('id'), 0]);

	$actualInvestigation = $mainQuery->first();

	$investigationTitle = $actualInvestigation->title;

	$description = $actualInvestigation->description;

	$investigationUrl = $actualInvestigation->url;

	$pdfPath = 'private/sources/pdf/investigations/' . $actualId . '/pdf/' . $investigationUrl .  '.pdf';




}else{

	$description='';

	$editMode = false;

	$investigationTitle = "";

	$investigationUrl = '';

	$pdfPath = '';

}

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Investigaciones | Agregar Investigación</title>

	<meta name="description" content="add new investigation">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{
		
		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	#map {

		height: 500px;

		width:100%;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}

</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<input type="hidden" id="pdf-path" name="pdf-path" value="<?php echo $pdfPath;?>">

		<input type="hidden" id="investigation-id" name="investigation-id" value="<?php echo $actualId;?>">

		<input type="hidden" id="investigation-url" name="investigation-url" value="<?php echo $investigationUrl;?>">
		
		<input type="hidden" id="investigation-name" name="investigation-name" value="<?php echo $investigationTitle;?>">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Investigaciones</h3>		
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>
								
								<li class="m-nav__separator">-</li>
								
								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text">Agregar Investigación</span>

									</a>

								</li>
								
							</ul>

						</div>
						
					</div>

				</div>
				
				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link active" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Información general

												</a>

											</li>

										</ul>

									</div>

								</div>

								
								<div class="tab-content">

									
									<div class="tab-pane active" id="general-tab">

										<form id="add-data-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>	

												</div>

											</div>

											<!-- titulo -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Título de la investigación*:</label>

												<div class="col-lg-4 col-md-9 col-8">

													<input id="title" type="text" class="form-control m-input" name="title" placeholder="Ej: el nombre del PDF" value="<?php echo $investigationTitle;?>">
												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $investigationTitle;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											 <input type="hidden" id="url" name="url" value="">


												<!-- contenido -->

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-sm-12">Breve descripción:<br><small>Para insertar saltos de línea, hacelo presionando <b>Shift + Enter</b>.</small></label>

													<div class="col-lg-6 col-md-9 col-sm-12">	

														<textarea numlines="20" type="text" class="form-control m-input description" id="description" name="description" placeholder="Descripción"><?php echo trim($description);?></textarea>	
														
														<?php if($editMode){  ?>

															<br>

															<div class=""> 

																<button data-id="<?php echo $actualId;?>"

																	class="update-btn btn btn-default" type="button">actualizar

																</button>

															</div>

														<?php }?>	

													</div>

												</div>


												<!-- body -->

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-sm-12">PDF de lal investigación*:<br><small>Solamente archivos PDF.</small>

													</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<input id="investigation-pdf" name="investigation-pdf" type="file" accept="application/pdf" required>

													</div>

												</div>

												<!-- ... -->

												<div class="m-portlet__foot m-portlet__foot--fit">

													<div class="m-form__actions">

														<div class="row">

															<div class="col-2"></div>

															<div class="col-7">

																<input type="hidden" name="token" value="<?php echo Token::generate()?>">

																<?php 

																$hiddenClass = ($editMode) ? 'd-none' : '';

																?>

																<button id="add_news" type="submit" 

																class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar Investigación</button>
																&nbsp;&nbsp;

																<?php 

																$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

																$buttonId = 

																(!$editMode) ? 'exit_from_form' : 'back_to_list';

																?>

																<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																	<?php echo $buttonLabel;?>

																</button>

															</div>

														</div>

													</div>

												</div>

											</form>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

			<?php require_once 'private/includes/footer.php'; ?>

		</div>

		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

		<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

		<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

		<script src="assets/app/js/helper.js" type="text/javascript"></script>

		<script src="assets/app/js/museum/investigations/museum-investigation-add.js" type="text/javascript"></script>

	</body>

	</html>
