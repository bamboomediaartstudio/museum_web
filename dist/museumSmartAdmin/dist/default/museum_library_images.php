<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';


?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Biblioteca | Subir imágenes</title>

	<meta name="description" content="library">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{
		
		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	#map {

		height: 500px;

		width:100%;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}

</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div id="myModal" class="modal" tabindex="-1" role="dialog">
		
		<div class="modal-dialog" role="document">
			
			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title">Asignar descripción a la imagen</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body">

					<form>

						<div class="form-group">

							<label for="image-	" class="col-form-label">Descripción:</label>

							<input type="text" class="form-control" id="image-description">

						</div>
						
					</form>

				</div>

				<div class="modal-footer">

					<button type="button" class="save-description btn btn-primary">guardar</button>

					<button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>

				</div>

			</div>

		</div>

	</div>

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Biblioteca popular judía</h3>		
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>
								
								<li class="m-nav__separator">-</li>
								
								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text">Listado de imágenes</span>

									</a>

								</li>
								
							</ul>

						</div>
						
					</div>

				</div>
				
				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--left m-tabs-line--primary" role="tablist">

											

											<li class="nav-item m-tabs__item"> 
												<a class="active tab-navigation images-tab nav-link m-tabs__link" data-toggle="tab" href="#images-tab" role="tab">

													Imágenes

												</a>

											</li>

											

										</ul>

									</div>

								</div>

								
								<div class="tab-content">

									

									<div class="tab-pane active" id="images-tab">

										<div class="mt-5 mb-5 col-lg-12">

											<input type="file" id="images-batch" name="images-batch[]" accept="image/*" multiple>

										</div>										
									</div>
									

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="assets/app/js/museum/library/library-images.js" type="text/javascript"></script>

</body>

</html>
