<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';


$actualId = Input::get('id');

if(Input::exists("get") && Input::get('id')){

	$editMode = true;

	$opinionQuery = DB::getInstance()->query(

		'SELECT * FROM museum_opinions as mo WHERE mo.id=?', [$actualId]);

	$opinionRow = $opinionQuery->first();

	$name = $opinionRow->name;

	$surname = $opinionRow->surname;

	$role = $opinionRow->role;

	$company = $opinionRow->company;

	$quote = $opinionRow->quote;

	$opinionRowImageQuery = DB::getInstance()->query('SELECT * FROM 

		museum_images WHERE sid=? AND deleted = ? AND source = ?', [$actualId, 0, 'opinions']);

	if($opinionRowImageQuery->count()>0){

		$opinionRowImage = $opinionRowImageQuery->first();

		$mimeType = MimeTypes::getExtensionByMimeType($opinionRowImage->mimetype);

		$imagePath = 'private/sources/images/opinions/' . $actualId . '/' . $opinionRowImage->unique_id . '/' .

		$actualId . '_' . $opinionRowImage->unique_id . '_sq.' . $mimeType;

	}else{

		$imagePath = "";

	}


}else{

	$editMode = false;

	$name = "";

	$surname = "";

	$role = "";
	
	$company = "";

	$quote = "";

}

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Opiniones | Agregar Opinión</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});
		
	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<input type="hidden" id="image-path" name="image-path" value="<?php echo $imagePath;?>">

		<input type="hidden" id="opinion-id" name="opinion-id" value="<?php echo $actualId;?>">

		<input type="hidden" id="opinion-person-name" name="opinion-person-name" value="<?php echo $name . ' ' . $surname;?>">


		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Opiniones</h3>			

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>
								
								<li class="m-nav__separator">-</li>
								
								<li class="m-nav__item">
									
									<a href="" class="m-nav__link">
									
										<span class="m-nav__link-text">Agregar Nueva Opinión</span>									
									</a>
								
								</li>
								
							</ul>
					
						</div>
										
					</div>
				
				</div>
				
				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">

											<h3 class="m-portlet__head-text">

												Agregar una nueva opinión del museo 

												<small>¿Querés ver cómo se va a visualizar una opinión en la página o en la app? <span class="ops"><strong>hacé click acá</span></strong>.</small>

											</h3>


										</div>

									</div>

								</div>

								<div class="tab-content">

									<div class="tab-pane active" id="m_museum_general">

										<form id="my-awesome-dropzone" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>	

												</div>

											</div>

											
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Nombre*:</label>

												<div class="col-lg-4 col-md-9 col-8">

													<input id="name" type="text" class="form-control m-input" name="name" placeholder="Nombre del nuevo miembro" value="<?php echo $name;?>">
													
												</div>

												<?php if($editMode){ ?>

													<div class="input-group-append"> 

														<button data-db-value="<?php echo $name;?>" data-id="<?php echo $actualId;?>"

															class="update-btn btn btn-default" type="button">actualizar</button>

														</div>

													<?php }?>

												</div>

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-8">Apellido*:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<input type="text" class="form-control m-input" id = "surname" name="surname" placeholder="apellido" value="<?php echo $surname;?>">					
													</div>

													<?php if($editMode){ ?>

														<div class="input-group-append"> 

															<button data-db-value="<?php echo $surname;?>" data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>

												</div>

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-sm-12">Frase:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<textarea type="text" class="form-control m-input" id="quote" name="quote" placeholder="frase"><?php echo $quote;?></textarea>		

													</div>

													<?php if($editMode){ ?>

														<div class="input-group-append"> 

															<button data-db-value="<?php echo $quote;?>" data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>

												</div>

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-8">Rol*:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<input type="text" class="form-control m-input" id = "role" name="role" placeholder="apellido" value="<?php echo $role;?>">					
													</div>

													<?php if($editMode){ ?>

														<div class="input-group-append"> 

															<button data-db-value="<?php echo $role;?>" data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>

												</div>

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-8">Empresa*:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<input type="text" class="form-control m-input" id = "company" name="company" placeholder="Empresa/organización" value="<?php echo $company;?>">					
													</div>

													<?php if($editMode){ ?>

														<div class="input-group-append"> 

															<button data-db-value="<?php echo $company;?>" data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>

												</div>

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-sm-12">Imagen:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<div class="m-dropzone dropzone dropzone-previews" id="m-dropzone-one">

															<div class="m-dropzone__msg dz-message needsclick">

																<h3 class="m-dropzone__msg-title">

																	Arrastrá la imagen acá

																</h3>

																<span class="m-dropzone__msg-desc">

																	O cliqueá para <strong>subirla!</strong>

																</span>

															</div>

														</div>

													</div>

												</div>




												<div class="m-portlet__foot m-portlet__foot--fit">

													<div class="m-form__actions">

														<div class="row">

															<div class="col-2"></div>

															<div class="col-7">

																<input type="hidden" name="token" value="<?php echo Token::generate()?>">

																<?php 

																$hiddenClass = ($editMode) ? 'd-none' : '';

																?>

																<button id="add_new_opinion" type="submit" 

																class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar Opinión</button>
																&nbsp;&nbsp;

																<?php 

																$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

																$buttonId = 

																(!$editMode) ? 'exit_from_form' : 'back_to_list';

																?>

																<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																	<?php echo $buttonLabel;?>

																</button>

															</div>

														</div>

													</div>

												</div>

											</form>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

			<?php require_once 'private/includes/footer.php'; ?>

		</div>

		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

		<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

		<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

		<script src="assets/app/js/helper.js" type="text/javascript"></script>

		<script src="assets/app/js/museum/opinions/museum-opinion-add.js" type="text/javascript"></script>

	</body>

	</html>
