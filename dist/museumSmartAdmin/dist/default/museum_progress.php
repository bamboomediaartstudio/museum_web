<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$query = DB::getInstance()->query( 'SELECT progress FROM museum_progress as mp');

$queryRow = $query->first();

?>


<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Sobre El Museo | Progreso</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});
		
	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<input type="hidden" id="progress" name="progress" value="<?php echo $queryRow->progress;?>">

	<div class="m-grid m-grid--hor m-grid--root m-page">
		
		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Progreso de la obra</h3>			

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>
								
								<li class="m-nav__separator">-</li>
								
								<li class="m-nav__item">
									
									<a href="" class="m-nav__link">

										<span class="m-nav__link-text">progreso</span>									
									</a>

								</li>
								
							</ul>

						</div>

					</div>

				</div>
				
				<div class="m-content">

					<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">

						<div class="m-alert__icon"><i class="flaticon-exclamation m--font-brand"></i></div>

						<div class="m-alert__text">

							Esta página contiene un slider que te va a permitir indicar a través de un porcentaje (%) el progreso de la obra que se está llevando a cabo en el edificio de la calle Uruguay.<br><br>

							El porcentaje que indiques a través del mismo, lo empleamos en la home / página principal de <a href="http://www.museodelholocausto.org.ar">http://www.museodelholocausto.org.ar</a> para que los visitantes estén al corriente de los progresos e ir generando expectativa conforme se acerque la fecha de inauguración.

						</div>

					</div>

					<div class="row">

						<div class="col-xl-12 col-lg-12">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">

											<h3 class="m-portlet__head-text">

												Progreso: 

											</h3>

										</div>

									</div>

								</div>

								<div class="tab-content">

									<div class="tab-pane active" id="m_museum_general">

										<form id="faq-form" class="m-form m-form--fit m-form--label-align-right" action="">

											

											<div class="form-group m-form__group row">


												<div class="mt-5 mb-5 col-10 offset-1">

													<input type="text" id="progress_slider" name="progress_slider" value=""/>

												</div>

											</div>



										</form>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="assets/app/js/museum/progress.js" type="text/javascript"></script>

</body>

</html>
