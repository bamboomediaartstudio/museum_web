<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

//$uniqueToken = Token::generate();

$actualId = Input::get('id');

if(Input::exists("get") && Input::get('id')){

	$editMode = true;

	$userCategoriesQuery = DB::getInstance()->query('SELECT * FROM

		museum_staff_categories_relations

		WHERE id_museum_staff_user=? AND deleted = ?', [Input::get('id'), 0]);

	$userCategoriesItems = $userCategoriesQuery->results();

	$userCategoriesArray = [];

	foreach($userCategoriesItems as $c){

		$userCategoriesArray[] = $c->id_museum_staff_category;

	}

	$staffMemberQuery = DB::getInstance()->query(

		'SELECT * FROM museum_staff as ms

		LEFT JOIN museum_staff_roles as msr

		ON ms.role = msr.id WHERE ms.id=?', [$actualId]);

	$staffMember = $staffMemberQuery->first();

	$name = $staffMember->name;

	$surname = $staffMember->surname;

	$role = $staffMember->role;

	$nickname = $staffMember->nickname;

	$email = $staffMember->email;

	$phone = $staffMember->phone;

	$bio = $staffMember->bio;

	$showEmailOnApp = ($staffMember->show_email_on_app == 1) ? 'checked' : '';

	$showEmailOnWeb = ($staffMember->show_email_on_web == 1) ? 'checked' : '';

	$showPhoneOnApp = ($staffMember->show_phone_on_app == 1) ? 'checked' : '';

	$showPhoneOnWeb = ($staffMember->show_phone_on_web == 1) ? 'checked' : '';

	$staffMemberImageQuery = DB::getInstance()->query('SELECT * FROM

		museum_images WHERE sid = ? AND source = ? AND deleted = ?', [$actualId, 'staff', 0]);

	if($staffMemberImageQuery->count()>0){

		$staffMemberImage = $staffMemberImageQuery->first();

		$mimeType = MimeTypes::getExtensionByMimeType($staffMemberImage->mimetype);

		$imagePath = 'private/sources/images/staff/' . $actualId . '/' . $staffMemberImage->unique_id . '/' .

		$actualId . '_' . $staffMemberImage->unique_id . '_sq.' . $mimeType;

	}else{

		$imagePath = "";

	}


}else{

	$editMode = false;

	$userCategoriesArray = [];

	$name = "";

	$surname = "";

	$role = "";

	$nickname = "";

	$email = "";

	$phone = "";

	$bio = "";

	$showEmailOnApp = "";

	$showEmailOnWeb = "";

	$showPhoneOnApp = "";

	$showPhoneOnWeb = "";
}

//museum data...

$museumData = DB::getInstance();

$museumDataQ = $museumData->query('SELECT * FROM museum_data');

if($museumDataQ->count()>0) $museumDataObject = $museumDataQ->first();

//museum social media data...

$museumSocialMediaData = DB::getInstance();

$museumSocialMediaDataQ = $museumSocialMediaData->query('SELECT * FROM museum_social_media');

if($museumSocialMediaDataQ->count()>0) $museumSocialMediaObject = $museumSocialMediaDataQ->first();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Staff | Agregar Personal</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>



	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<!--<link href="assets/css/typeahead.css" rel="stylesheet" type="text/css" />-->

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}


</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<input type="hidden" id="image-path" name="image-path" value="<?php echo $imagePath;?>">

		<input type="hidden" id="staff-member-id" name="staff-member-id" value="<?php echo $actualId;?>">

		<input type="hidden" id="staff-member-name" name="staff-member-name" value="<?php echo $name . ' ' . $surname;?>">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>


			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Staff</h3>

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>

								<li class="m-nav__separator">-</li>

								<li class="m-nav__item">
									<a href="" class="m-nav__link">
										<span class="m-nav__link-text">Agregar Personal</span>
									</a>
								</li>

							</ul>
						</div>
						<div>

						</div>
					</div>
				</div>

				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">

							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">

											<h3 class="m-portlet__head-text">

												Agregar nuevo miembro del staff

											</h3>

										</div>

									</div>

								</div>

								<div class="tab-content">

									<div class="tab-pane active" id="m_museum_general">

										<form id="my-awesome-dropzone" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>

												</div>

											</div>


											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Nombre*:</label>

												<div class="col-lg-4 col-md-9 col-8">

													<input id="name" type="text" class="form-control m-input" name="name" placeholder="Nombre del nuevo miembro" value="<?php echo $name;?>">

												</div>

												<?php if($editMode){ ?>

													<div class="input-group-append">

														<button data-db-value="<?php echo $name;?>" data-id="<?php echo $actualId;?>"

															class="update-btn btn btn-default" type="button">actualizar</button>

														</div>

													<?php }?>

												</div>

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-8">Apellido*:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<input type="text" class="form-control m-input" id = "surname" name="surname" placeholder="apellido" value="<?php echo $surname;?>">
													</div>

													<?php if($editMode){ ?>

														<div class="input-group-append">

															<button data-db-value="<?php echo $surname;?>" data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>

												</div>

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-sm-12">Categoría:*</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<div class="m-checkbox-list">

															<?php

															$categoriesQuery = DB::getInstance()->query('SELECT * FROM museum_staff_categories');

															foreach($categoriesQuery->results() as $c){

																$checked = '';

																foreach($userCategoriesArray as $item){

																	if($c->id == $item){ $checked = 'checked'; }

																}

																?>

																<label class="m-checkbox m-checkbox--solid m-checkbox--success">

																	<input <?php echo $checked;?> value="<?php echo $c->id;?>"

																	name="checkboxes[]"

																	type="checkbox"

																	data-id="<?php echo $actualId;?>">

																	<?php echo $c->category_name;?>

																	<span></span>

																</label>


															<?php } ?>

														</div>

														<span class="m-form__help"><b>Seleccioná</b> al menos una de las opciones.
														</span>

													</div>

												</div>

												<!--

													<div class="form-group m-form__group row pt-4">

														<label class="col-form-label col-lg-3 col-sm-12">Email:</label>

														<div class="col-lg-4 col-md-9 col-8">

															<div class="m-input-icon m-input-icon--left">

																<input type="text" class="form-control m-input" id="email" name="email" placeholder="email" value="<?php echo $email;?>">

																<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-envelope"></i></span></span>

															</div>

														</div>

														<?php if($editMode){ ?>

															<div class="input-group-append">

																<button data-db-value="<?php echo $email;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar</button>

															</div>

														<?php }?>


													</div>


												-->

												<div id="roles-list" class="form-group m-form__group row pt-4">

													<label class="col-form-label col-lg-3 col-sm-12">Rol:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<input type="text" class="typeahead form-control m-input" name="role" placeholder="rol que desempeña" value="<?php echo $role;?>">

														<input type="hidden" id="hidden-role-id" name="hidden-role-id" >

														<!--<span class="m-form__help"><b>ejemplos:</b> vocal, guía, investigación, museología, etc.</span>-->

													</div>

													<?php if($editMode){ ?>

														<div class="input-group-append">

															<button data-db-value="<?php echo $role;?>" data-id="<?php echo $actualId;?>"

																class="btn btn-default upate-for-typeahead" type="button">actualizar</button>

															</div>

														<?php }?>

													</div>

													<div class="form-group m-form__group row">

														<label class="col-form-label col-lg-3 col-sm-12">Imagen de perfil:</label>

														<div class="col-lg-4 col-md-9 col-sm-12">

															<div class="m-dropzone dropzone dropzone-previews" id="m-dropzone-one">

																<div class="m-dropzone__msg dz-message needsclick">

																	<h3 class="m-dropzone__msg-title">

																		Arrastrá la imagen acá

																	</h3>

																	<span class="m-dropzone__msg-desc">

																		O cliqueá para <strong>subirla!</strong>

																	</span>

																</div>

															</div>

														</div>

													</div>

													<div class="form-group m-form__group row">

														<label class="col-form-label col-lg-3 col-sm-12">Apodo:</label>

														<div class="col-lg-4 col-md-9 col-sm-12">

															<input type="text" class="form-control m-input" id="nickname" name="nickname" placeholder="nickname" value="<?php echo $nickname;?>">
														</div>

														<?php if($editMode){ ?>

															<div class="input-group-append">

																<button data-db-value="<?php echo $nickname;?>" data-id="<?php echo $actualId;?>"

																	class="update-btn btn btn-default" type="button">actualizar</button>

																</div>

															<?php }?>

														</div>

														<div class="form-group m-form__group row">

															<label class="col-form-label col-lg-3 col-sm-12">bio:</label>

															<div class="col-lg-4 col-md-9 col-sm-12">

																<textarea type="text" class="form-control m-input" id="bio" name="bio" placeholder="bio"><?php echo $bio;?></textarea>

															</div>

															<?php if($editMode){ ?>

																<div class="input-group-append">

																	<button data-db-value="<?php echo $bio;?>" data-id="<?php echo $actualId;?>"

																		class="update-btn btn btn-default" type="button">actualizar

																	</button>

																</div>

															<?php }?>

														</div>

														<div class="form-group m-form__group row pt-4">

															<label class="col-form-label col-lg-3 col-sm-12">Email:</label>

															<div class="col-lg-4 col-md-9 col-8">

																<div class="m-input-icon m-input-icon--left">

																	<input type="text" class="form-control m-input" id="email" name="email" placeholder="email" value="<?php echo $email;?>">

																	<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-envelope"></i></span></span>

																</div>

															</div>

															<?php if($editMode){ ?>

																<div class="input-group-append">

																	<button data-db-value="<?php echo $email;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar</button>

																</div>

															<?php }?>


														</div>

														<div class="form-group m-form__group row">

															<label class="col-form-label col-lg-3 col-sm-12">Mostrar email:</label>

															<div class="col-lg-4 col-md-9 col-sm-12">

																<div class="m-form__group form-group row">

																	<label class="col-form-label">en la web:</label>

																	<div class="col-3">

																		<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																			<label>
																				<input type="checkbox" <?php echo $showEmailOnWeb;?>
																				class="show-email-on-web toggler-info"

																				name="show-email-on-web"

																				data-id="<?php echo $actualId;?>">

																				<span></span>

																			</label>

																		</span>

																	</div>

																	<label class="col-form-label">en la app:</label>

																	<div class="col-3">

																		<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																			<label>
																				<input <?php echo $showEmailOnApp;?> class="show-email-on-app toggler-info"

																				type="checkbox"

																				data-id="<?php echo $actualId;?>"

																				name="show-email-on-app">

																				<span></span>

																			</label>

																		</span>

																	</div>

																</div>

															</div>

														</div>

														<div class="form-group m-form__group row pt-4">

															<label class="col-form-label col-lg-3 col-sm-12">Teléfono:</label>

															<div class="col-lg-4 col-md-9 col-8">

																<div class="m-input-icon m-input-icon--left">

																	<input type="text" class="form-control m-input" id="phone" name="phone" placeholder="teléfono" value="<?php echo $phone;?>">

																	<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-phone"></i></span></span>

																</div>

															</div>

															<?php if($editMode){ ?>

																<div class="input-group-append">

																	<button data-db-value="<?php echo $phone;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar</button>

																</div>

															<?php }?>

														</div>

														<div class="form-group m-form__group row">

															<label class="col-form-label col-lg-3 col-sm-12">Mostrar teléfono:</label>

															<div class="col-lg-4 col-md-9 col-sm-12">

																<div class="m-form__group form-group row">

																	<label class="col-form-label">en la web:</label>

																	<div class="col-3">

																		<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																			<label>

																				<input type="checkbox" <?php echo $showPhoneOnWeb;?>

																				class="show-phone-on-web toggler-info"

																				data-id="<?php echo $actualId;?>"

																				name="show-phone-on-web">

																				<span></span>

																			</label>

																		</span>

																	</div>

																	<label class="col-form-label">en la app:</label>

																	<div class="col-3">

																		<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																			<label>

																				<input <?php echo $showPhoneOnApp;?>

																				class="show-phone-on-app toggler-info"

																				data-id="<?php echo $actualId;?>"

																				type="checkbox"

																				name="show-phone-on-app">

																				<span></span>

																			</label>

																		</span>

																	</div>

																</div>

															</div>

														</div>


														<div class="m-portlet__foot m-portlet__foot--fit">

															<div class="m-form__actions">

																<div class="row">

																	<div class="col-2"></div>

																	<div class="col-7">

																		<input type="hidden" name="token" value="<?php echo Token::generate()?>">

																		<?php

																		$hiddenClass = ($editMode) ? 'd-none' : '';

																		?>

																		<button id="add_staff_member" type="submit"

																		class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar Miembro</button>
																		&nbsp;&nbsp;

																		<?php

																		$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

																		$buttonId =

																		(!$editMode) ? 'exit_from_form' : 'back_to_list';

																		?>

																		<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																			<?php echo $buttonLabel;?>

																		</button>

																	</div>

																</div>

															</div>

														</div>

													</form>

												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

					<?php require_once 'private/includes/footer.php'; ?>

				</div>

				<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

				<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

				<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

				<script src="assets/app/js/helper.js" type="text/javascript"></script>

				<script src="assets/app/js/museum/staff/museum-staff-add.js" type="text/javascript"></script>

			</body>

			</html>
