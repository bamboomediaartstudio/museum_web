<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

setlocale(LC_ALL, "es_ES", 'Spanish_Spain', 'Spanish');

//---------------------------------------------------------------------------------------------------------------
//interactions hoy...

$todayInteractions =  DB::getInstance()->query("SELECT COUNT(*) as today FROM app_stats as stats INNER JOIN app_stats_interactions as interactions  ON stats.id = interactions.id_app WHERE interactions.date = CURDATE()", [])->first()->today;

$yesterdayInteractions =  DB::getInstance()->query("SELECT COUNT(*) as today FROM app_stats as stats INNER JOIN app_stats_interactions as interactions  ON stats.id = interactions.id_app WHERE interactions.date = CURDATE() - 1", [])->first()->today;

$todayInteractionsPercent = 100;

$todayTooltip = 'Contempla el total de interacciones de todas las pantallas durante todo el día de hoy.';

$dayTooltip = '';

$diff;

$diffString;

if($yesterdayInteractions > $todayInteractions){

	$todayInteractionsPercent = round(($todayInteractions * 100) / $yesterdayInteractions, 0);

	$diff = ($yesterdayInteractions - $todayInteractions);

	$todayInteractionsQuote = $diff . ' interacciones menos que ayer'; 

	$diffString = $diff . ' menos respecto a ayer';

}else if($yesterdayInteractions == $todayInteractions){

	$todayInteractionsQuote = $todayInteractions . ' interacciones: misma cantidad de interacciones que ayer'; 

	$diffString = 'respecto a ayer';

}else{

	$diff = ($todayInteractions - $yesterdayInteractions);

	$todayInteractionsQuote = '¡Objetivo cumplido! ' .  $diff . ' interacciones más que ayer!'; 

	$diffString = $diff . ' más respecto a ayer';
}

//---------------------------------------------------------------------------------------------------------------
//end interactions hoy...

//---------------------------------------------------------------------------------------------------------------
//interactions week...

$weekInteractions =  DB::getInstance()->query("SELECT COUNT(*) as week FROM app_stats as stats INNER JOIN app_stats_interactions as interactions ON stats.id = interactions.id_app WHERE  YEARWEEK(interactions.date, 1) = YEARWEEK(CURDATE(), 1)", [])->first()->week;

$lastWeekInteractions =  DB::getInstance()->query("SELECT COUNT(*) as week FROM app_stats as stats INNER JOIN app_stats_interactions as interactions ON stats.id = interactions.id_app WHERE  YEARWEEK(interactions.date, 1) = YEARWEEK(CURDATE(), 1) - 1", [])->first()->week;

$weekInteractionsPercent = 100;

$diffWeekStreet;

if($lastWeekInteractions > $weekInteractions ){

	$weekInteractionsPercent = round(($weekInteractions * 100) / $lastWeekInteractions , 0);

	$diff = ($lastWeekInteractions - $weekInteractions);

	$weekInteractionsQuote = $diff . ' interacciones menos que ayer'; 

	$diffWeekStreet = '(' . $diff . ' menos respecto a la semana pasada)';

}else if($weekInteractions == $lastWeekInteractions){

	$weekInteractionsQuote = $lastWeekInteractions . ' interacciones: misma cantidad de interacciones que la semana pasada'; 

	$diffWeekStreet = '(respecto a ayer)';

}else{

	$diff = ($weekInteractions - $lastWeekInteractions);

	$weekInteractionsQuote = $diff . ' interacciones más que la semana pasada!'; 

	$diffWeekStreet = '(' . $diff . ' más respecto a ayer)';
}

$weekTooltip = "Contempla el total de interacciones desde el comienzo de la semana hasta el día de hoy. Se contabiliza de lunes a viernes.";

//---------------------------------------------------------------------------------------------------------------
//interactions month...

$monthTooltip = "Contempla el total de interacciones desde el primer día del mes al último.";

$monthInteractions =  DB::getInstance()->query("SELECT COUNT(*) as today FROM app_stats as stats INNER JOIN app_stats_interactions as interactions  ON stats.id = interactions.id_app WHERE  (interactions.date between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW() )", [])->first()->today;

//---------------------------------------------------------------------------------------------------------------
//interactions year...

$yearInteractions =  DB::getInstance()->query("SELECT COUNT(*) as year FROM app_stats as stats INNER JOIN app_stats_interactions as interactions ON stats.id = interactions.id_app WHERE YEAR (interactions.date) = YEAR(CURDATE())", [])->first()->year;

$mostOfTheDay = DB::getInstance()->query("SELECT *, count(interactions.id) as interactions_value, stats.id as app_id FROM app_stats as stats LEFT JOIN app_stats_interactions as interactions ON stats.id = interactions.id_app AND interactions.date = CURDATE() GROUP by stats.id ORDER BY interactions_value DESC LIMIT 1")->first();

$minusOfTheDay = DB::getInstance()->query("SELECT *, count(interactions.id) as interactions_value, stats.id as app_id FROM app_stats as stats LEFT JOIN app_stats_interactions as interactions ON stats.id = interactions.id_app AND interactions.date = CURDATE() GROUP by stats.id ORDER BY interactions_value ASC LIMIT 1")->first();

$mostOfTheWeek = DB::getInstance()->query("SELECT *, count(interactions.id) as interactions_value, stats.id as app_id FROM app_stats as stats LEFT JOIN app_stats_interactions as interactions ON stats.id = interactions.id_app AND YEARWEEK(interactions.date, 1) = YEARWEEK(CURDATE(), 1)GROUP by stats.id ORDER BY interactions_value DESC LIMIT 1")->first();

$minusOfTheWeek = DB::getInstance()->query("SELECT *, count(interactions.id) as interactions_value, stats.id as app_id FROM app_stats as stats LEFT JOIN app_stats_interactions as interactions ON stats.id = interactions.id_app AND YEARWEEK(interactions.date, 1) = YEARWEEK(CURDATE(), 1)GROUP by stats.id ORDER BY interactions_value ASC LIMIT 1")->first();


$mostOFTheMonth = DB::getInstance()->query("SELECT *, count(interactions.id) as  interactions_value, stats.id as app_id FROM app_stats as stats LEFT JOIN  app_stats_interactions as interactions ON stats.id = interactions.id_app  AND MONTH(interactions.date) = MONTH(CURRENT_DATE()) AND  YEAR(interactions.date) =  YEAR(CURRENT_DATE()) GROUP by stats.id ORDER BY interactions_value DESC LIMIT 1")->first();

$minusOfTheMonth = DB::getInstance()->query("SELECT *, count(interactions.id) as  interactions_value, stats.id as app_id FROM app_stats as stats LEFT JOIN  app_stats_interactions as interactions ON stats.id = interactions.id_app  AND MONTH(interactions.date) = MONTH(CURRENT_DATE()) AND  YEAR(interactions.date) =  YEAR(CURRENT_DATE()) GROUP by stats.id ORDER BY interactions_value ASC LIMIT 1")->first();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Estadísticas | Pantallas Touch</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() { sessionStorage.fonts = true; }

		});
		
	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
	
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<input type="hidden" name="user-id" id="user-id" value="<?php echo $actualUser;?>">

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-content">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title ">ETADÍSTICAS</h3>

							<p>Generales por día, semana, mes, año e históricas.</p>

						</div>

					</div>

					<div class="m-portlet ">

						<div class="m-portlet__head">

							<div class="m-portlet__head-caption">
								
								<div class="m-portlet__head-title">

									<h3 class="m-portlet__head-text">
										
										INTERACCIONES<br><small>Cuanta gente usó las pantallas en el día, la semana y el mes.</small>
										
									</h3>
									
									<br><br>

								</div>

							</div>

						</div>

						<div class="m-portlet__body  m-portlet__body--no-padding">

							<div class="row m-row--no-padding m-row--col-separator-xl">

								<div class="col-md-12 col-lg-6 col-xl-4">

									<div class="m-widget24">

										<div class="m-widget24__item">

											<h4 class="m-widget24__title">Interacciones de hoy: </h4>

											<br>

											<span class="m-widget24__desc"><?php echo date("d/m/Y");?></span>

											<span class="m-widget24__stats m--font-brand" data-skin="dark" data-toggle="m-tooltip" 

											data-placement="right" title="<?php echo $todayTooltip;?>" 

											data-original-title="Contempla el total de interacciones de todas las pantallas durante todo el día de hoy."><?php echo $todayInteractions;?> </span>

											<div class="m--space-10"></div>

											<div class="progress m-progress--sm">

												<div class="progress-bar m--bg-brand" role="progressbar" style="width: <?php echo $todayInteractionsPercent;?>%;" data-skin="dark" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="<?php echo $todayInteractionsQuote;?>"></div>
											</div>

											<span class="m-widget24__change"><?php echo $diffString;?></span>

											<span class="m-widget24__number"><?php echo $todayInteractionsPercent;?>%</span>

										</div>

									</div>

								</div>

								<div class="col-md-12 col-lg-6 col-xl-4">

									<div class="m-widget24">

										<div class="m-widget24__item">

											<h4 class="m-widget24__title">Interacciones de la semana:</h4>
											<br>
											<span class="m-widget24__desc">de lunes a viernes</span>

											<span class="m-widget24__stats m--font-info" data-skin="dark" data-toggle="m-tooltip" 

											data-placement="right" title="" 

											data-original-title="<?php echo $weekTooltip;?>"><?php echo $weekInteractions;?></span>

											<div class="m--space-10"></div>

											<div class="progress m-progress--sm">

												<div class="progress-bar m--bg-info" role="progressbar" style="width: <?php echo $weekInteractionsPercent;?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>

											</div>

											<span class="m-widget24__change">respecto a la semana pasada</span>

											<span class="m-widget24__number"><?php echo $weekInteractionsPercent;?>%</span>

										</div>

									</div>

								</div>

								<div class="col-md-12 col-lg-6 col-xl-4">

									<div class="m-widget24">

										<div class="m-widget24__item">

											<h4 class="m-widget24__title">Ineracciones del mes:</h4>

											<br>

											<span class="m-widget24__desc">durante <?php echo strftime("%B");?></span>

											<span class="m-widget24__stats m--font-danger" data-skin="dark" data-toggle="m-tooltip" 

											data-placement="right" title="" 

											data-original-title="<?php echo $monthTooltip;?>"><?php echo $monthInteractions;?></span>

											<div class="m--space-10"></div>

											<div class="progress m-progress--sm">
												<div class="progress-bar m--bg-danger" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
											</div>
											<span class="m-widget24__change">Desde el primer día del mes</span>

											<span class="m-widget24__number">100%</span>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

					<div class="m-portlet">

						<div class="m-portlet__head">

							<div class="m-portlet__head-caption">
								
								<div class="m-portlet__head-title">

									<h3 class="m-portlet__head-text">
										
										LO MÁS Y MENOS VISTO<br><small>del día, de la semana y del mes</small>
										
									</h3>
									
									<br><br>

								</div>

							</div>

						</div>

						<div class="m-portlet__body m-portlet__body--no-padding">

							<div class="row m-row--no-padding m-row--col-separator-xl">

								<div class="col-md-12 col-lg-12 col-xl-4">

									<div class="m-widget1">

										<div class="m-widget1__item">

											<div class="row m-row--no-padding align-items-center">

												<div class="col">

													<h3 class="m-widget1__title">Lo más visto del día:</h3>
													
													<span class="m-widget1__desc"><?php echo strtoupper($mostOfTheDay->name);?></span>
													
												</div>
												<div class="col m--align-right">

													<span class="m-widget1__number m--font-brand"><?php echo strtoupper($mostOfTheDay->interactions_value);?></span>
													
												</div>
												
											</div>
											
										</div>

										<div class="m-widget1__item">

											<div class="row m-row--no-padding align-items-center">
												
												<div class="col">

													<h3 class="m-widget1__title">Lo menos visto del día:</h3>

													<span class="m-widget1__desc"><?php echo strtoupper($minusOfTheDay	->name);?></span>
													
												</div>

												<div class="col m--align-right">

													<span class="m-widget1__number m--font-danger"><?php echo strtoupper($minusOfTheDay	->interactions_value);?></span>
													
												</div>
												
											</div>
											
										</div>


									</div>

								</div>

								<div class="col-md-12 col-lg-12 col-xl-4">

									<div class="m-widget1">

										<div class="m-widget1__item">

											<div class="row m-row--no-padding align-items-center">
												
												<div class="col">
													
													<h3 class="m-widget1__title">
														+ visto de la semana:
													</h3>
													<span class="m-widget1__desc"><?php echo strtoupper($mostOfTheWeek->name);?></span>
												</div>

												<div class="col m--align-right">

													<span class="m-widget1__number m--font-accent">
														
														<?php echo strtoupper($mostOfTheWeek->interactions_value);?>
														
													</span>
													
												</div>
												
											</div>
											
										</div>

										<div class="m-widget1__item">

											<div class="row m-row--no-padding align-items-center">

												<div class="col">

													<h3 class="m-widget1__title">
														+ visto de la semana:
													</h3>

													<span class="m-widget1__desc"><?php echo strtoupper($minusOfTheWeek->name);?></span>
													
												</div>

												<div class="col m--align-right">
													
													<span class="m-widget1__number m--font-info">
														<?php echo strtoupper($minusOfTheWeek->interactions_value);?>

													</span>

												</div>
												
											</div>
											
										</div>

									</div>

								</div>

								<div class="col-md-12 col-lg-12 col-xl-4">

									<div class="m-widget1">

										<div class="m-widget1__item">
											
											<div class="row m-row--no-padding align-items-center">
												
												<div class="col">

													<h3 class="m-widget1__title">+ visto del mes </h3>
													
													<span class="m-widget1__desc"><?php echo strtoupper($mostOFTheMonth->name);?></span>
													
												</div>
												<div class="col m--align-right">

													<span class="m-widget1__number m--font-success"><?php echo strtoupper($mostOFTheMonth->interactions_value);?></span>
													
												</div>
												
											</div>
											
										</div>

										<div class="m-widget1__item">
											
											<div class="row m-row--no-padding align-items-center">

												<div class="col">

													<h3 class="m-widget1__title"> - visto del año </h3>

													<span class="m-widget1__desc"><?php echo strtoupper($minusOfTheMonth->name);?></span>
													
												</div>

												<div class="col m--align-right">

													<span class="m-widget1__number m--font-danger"><?php echo strtoupper($minusOfTheMonth->interactions_value);?></span>
													
												</div>
												
											</div>
											
										</div>

									</div>
									
								</div>

							</div>

						</div>

					</div>

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title ">RANKINGS</h3>

							<p>La posición de cada app en el día, el mes y el año, y su posición respecto a ayer, a la semana pasada y al mes pasado.</p>

						</div>

					</div>

					<div class="row">

						<div class="col-xl-4">

							<div class="m-portlet m-portlet--full-height ">
								
								<div class="m-portlet__head">
									
									<div class="m-portlet__head-caption">
										
										<div class="m-portlet__head-title">
											
											<h3 class="m-portlet__head-text">Ranking del dia:</h3>
											
										</div>
										
									</div>

								</div>

								<div class="m-portlet__body">

									<?php

									$yesterdayDailyRanking = DB::getInstance()->query("SELECT *, count(interactions.id) as interactions_value, stats.id as app_id FROM app_stats as stats LEFT JOIN app_stats_interactions as interactions ON stats.id = interactions.id_app AND interactions.date = CURDATE()-1 GROUP by stats.id ORDER BY interactions_value DESC");

									$yesterdayArray = array();

									foreach($yesterdayDailyRanking->results() as $yesterdayResults){

										$object = (object) ['id' => $yesterdayResults->app_id, 'interactions_value' => $yesterdayResults->interactions_value];

										array_push($yesterdayArray, $object);
									}

									$dailyRanking = DB::getInstance()->query("SELECT *, count(interactions.id) as interactions_value, stats.id as app_id FROM app_stats as stats LEFT JOIN app_stats_interactions as interactions ON stats.id = interactions.id_app AND interactions.date = CURDATE() GROUP by stats.id ORDER BY interactions_value DESC");

									$counter = 0;

									$class;

									$label;

									foreach($dailyRanking->results() as $results){

										$mainId = $results->app_id;

										foreach ($yesterdayArray as $key => $value) {

											if($value->id == $mainId){

												if($results->interactions_value < $value->interactions_value){

													$class = 'fas fa-arrow-down text-danger';

													$label = $results->name . ' bajó en el ranking respecto ayer.';

												}else if($results->interactions_value == $value->interactions_value){

													$class = 'fas fa-grip-lines m--font-info';

													$label = $results->name . ' está igual en el ranking respecto a ayer.';										

												}else{

													$label =  $results->name . 'mejoró en el ranking respecto ayer.';

													$class = 'fas fa-arrow-up text-success';
												}

											}

											continue;

										}

										$counter+=1;

										echo '<div class="m-widget4">

										<div class="m-widget4__item">

										<div class="m-widget4__info">

										<span class="m-widget4__text">' . $counter . ' - ' . $results->name . ' </span>

										</div>

										<div class="m-widget4__ext" data-skin="dark" data-toggle="m-tooltip" 

										data-placement="right" title="' . $label .  '"> <i class="' . $class . '"></i></div>

										</div>

										</div>';

									}?>

								</div>
								
							</div>

						</div>

						<div class="col-xl-4">

							<div class="m-portlet m-portlet--full-height ">
								
								<div class="m-portlet__head">
									
									<div class="m-portlet__head-caption">
										
										<div class="m-portlet__head-title">
											
											<h3 class="m-portlet__head-text"> Ranking de la semana:</h3>
											
										</div>
										
									</div>

								</div>

								<div class="m-portlet__body">

									<?php

									$lastWeekRanking = DB::getInstance()->query("SELECT *, count(interactions.id) as interactions_value, stats.id as app_id FROM app_stats as stats LEFT JOIN app_stats_interactions as interactions ON stats.id = interactions.id_app AND YEARWEEK(interactions.date, 1) = YEARWEEK(CURDATE(), 1) - 1 GROUP by stats.id ORDER BY interactions_value DESC");

									$lastWeekArray = array();

									foreach($lastWeekRanking->results() as $lastWeekResults){

										$object = (object) ['id' => $lastWeekResults->app_id, 'interactions_value' => $lastWeekResults->interactions_value];

										array_push($lastWeekArray, $object);
									}


									$weekRanking = DB::getInstance()->query("SELECT *, count(interactions.id) as interactions_value, stats.id as app_id FROM app_stats as stats LEFT JOIN app_stats_interactions as interactions ON stats.id = interactions.id_app AND YEARWEEK(interactions.date, 1) = YEARWEEK(CURDATE(), 1) GROUP by stats.id ORDER BY interactions_value DESC");

									$weekCounter = 0;

									$class;

									$label;

									foreach($weekRanking->results() as $results){

										$mainId = $results->app_id;

										foreach ($lastWeekArray as $key => $value) {

											if($value->id == $mainId){

												if($results->interactions_value < $value->interactions_value){

													$class = 'fas fa-arrow-down text-danger';

													$label = $results->name . ' bajó en el ranking respecto a la semana pasada.';

												}else if($results->interactions_value == $value->interactions_value){

													$class = 'fas fa-grip-lines m--font-info';

													$label = $results->name . ' está igual en el ranking respecto a la semana pasada.';										
												}else{

													$label =  $results->name . ' mejoró en el ranking respecto la semana pasada.';

													$class = 'fas fa-arrow-up text-success';
												}

												continue;

											}

										}

										$weekCounter+=1;

										echo '<div class="m-widget4">

										<div class="m-widget4__item">

										<div class="m-widget4__info">

										<span class="m-widget4__text">' . $weekCounter . ' - ' . $results->name . ' </span>

										</div>

										<div class="m-widget4__ext" data-skin="dark" data-toggle="m-tooltip" 

										data-placement="right" title="' . $label .  '"> <i class="' . $class . '"></i></div>

										</div>

										</div>';

									}?>

								</div>

							</div>

						</div>

						<div class="col-xl-4">

							<div class="m-portlet m-portlet--full-height ">
								
								<div class="m-portlet__head">
									
									<div class="m-portlet__head-caption">
										
										<div class="m-portlet__head-title">
											
											<h3 class="m-portlet__head-text"> Ranking del mes:</h3>
											
										</div>
										
									</div>

								</div>

								<div class="m-portlet__body">

									<?php

									$previousMonthDate =  date("Y-m",strtotime("-1 month"));

									$month = date("m",strtotime($previousMonthDate));
									
									$year = date("Y",strtotime($previousMonthDate));
									
									$lastMonthRanking = DB::getInstance()->query("SELECT *, count(interactions.id) as interactions_value, stats.id as app_id 

										FROM app_stats as stats LEFT JOIN 

										app_stats_interactions as interactions 

										ON stats.id = interactions.id_app 

										AND month(interactions.date) = $month AND year (interactions.date) = $year

										GROUP by stats.id ORDER BY interactions_value DESC");

									$lastMonthArray = array();

									foreach($lastMonthRanking->results() as $lastMonthResults){

										$object = (object) ['id' => $lastMonthResults->app_id, 'interactions_value' => $lastMonthResults->interactions_value];

										array_push($lastMonthArray, $object);
									}

									
									$monthRanking = DB::getInstance()->query("SELECT *, count(interactions.id) as 

										interactions_value, stats.id as app_id FROM app_stats as stats LEFT JOIN 

										app_stats_interactions as interactions ON stats.id = interactions.id_app 

										AND MONTH(interactions.date) = MONTH(CURRENT_DATE()) AND 

										YEAR(interactions.date) =  YEAR(CURRENT_DATE())

										GROUP by stats.id ORDER BY interactions_value DESC");

									$monthCounter = 0;

									$class;

									$label;

									foreach($monthRanking->results() as $results){

										$mainId = $results->app_id;

										foreach ($lastMonthArray as $key => $value) {

											if($value->id == $mainId){

												if($results->interactions_value < $value->interactions_value){

													$class = 'fas fa-arrow-down text-danger';

													$label = $results->name . ' bajó en el ranking respecto a la semana pasada.';

												}else if($results->interactions_value == $value->interactions_value){

													$class = 'fas fa-grip-lines m--font-info';

													$label = $results->name . ' está igual en el ranking respecto a la semana pasada.';										
												}else{

													$label =  $results->name . 'mejoró en el ranking respecto la semana pasada.';

													$class = 'fas fa-arrow-up text-success';
												}

												continue;

											}

										}

										$monthCounter+=1;

										echo '<div class="m-widget4">

										<div class="m-widget4__item">

										<div class="m-widget4__info">

										<span class="m-widget4__text">' . $monthCounter . ' - ' . $results->name . ' </span>

										</div>

										<div class="m-widget4__ext" data-skin="dark" data-toggle="m-tooltip" 

										data-placement="right" title="' . $label .  '"> <i class="' . $class . '"></i></div>

										</div>

										</div>';

									}?>

								</div>

							</div>

						</div>

					</div>

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title">GRÁFICOS Y ESTADÍSTICAS</h3><br>

						</div>

					</div>

					<div class="row">

						<div class="col-xl-6">

							<div class="">

								<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">

									<div class="m-portlet__head">

										<div class="m-portlet__head-caption">

											<div class="m-portlet__head-title">

												<h3 class="m-portlet__head-text">

													Estadística histórica promedio de usuarios por hora:

												</h3>

											</div>

										</div>

									</div>

									<div class="m-portlet__body">

										<div class="m-widget15">

											<div class="m-widget15__chart" style="height:180px;">

												<canvas  id="chart_activity_by_hour_today"></canvas>

											</div>

											<div class="m-widget15__desc">
												* Se toman en cuenta todos los usuarios desde el inicio del museo.
											</div>
										
										</div>

									</div>
								
								</div>

							</div>
						
						</div>

						<div class="col-xl-6">

							<div class="">

								<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">

									<div class="m-portlet__head">

										<div class="m-portlet__head-caption">

											<div class="m-portlet__head-title">

												<h3 class="m-portlet__head-text">

													Estadística histórica promedio de usuarios por día

												</h3>

											</div>

										</div>

									</div>

									<div class="m-portlet__body">

										<div class="m-widget15">

											<div class="m-widget15__chart" style="height:180px;">

												<canvas  id="chart_activity_by_day_in_week"></canvas>

											</div>

											<div class="m-widget15__desc">
												
												* Se toman en cuenta todos los usuarios desde el inicio del museo.
											
											</div>
										
										</div>

									</div>
								</div>

							</div>

						</div>

					</div>

					<div class="row">

						<div class="col-xl-4">

							<div class="m-widget14 m-portlet m-portlet--bordered-semi m-portlet--full-height">

								<div class="m-widget14__header m--margin-bottom-30">

									<h3 class="m-widget14__title"> Interacciones por día en el mes de <?php echo strftime("%B");?> </h3>

									<span class="m-widget14__desc">Comparado con máximos históricos</span>

								</div>

								<div class="m-widget14__chart" style="height:120px;">

									<canvas  id="chart_average_by_days_in_month"></canvas>

								</div>

							</div>

						</div>

						<div class="col-xl-4">

							<div class="m-widget14 m-widget14 m-portlet m-portlet--bordered-semi m-portlet--full-height">

								<div class="m-widget14__header m--margin-bottom-30">

									<h3 class="m-widget14__title">

										Interacciones por mes para <?php echo strftime("%Y");?>

									</h3>

									<span class="m-widget14__desc">

										de enero a diciembre

									</span>

								</div>

								<div class="m-widget14__chart" style="height:120px;">
									<canvas  id="chart_activity_by_month_in_year"></canvas>

								</div>

							</div>

						</div>

						<div class="col-xl-4">

							<div class="m-widget14 m-widget14 m-portlet m-portlet--bordered-semi m-portlet--full-height">

								<div class="m-widget14__header">

									<h3 class="m-widget14__title">Distribución </h3>

									<span class="m-widget14__desc"> Uso según ubicación de la pantalla. Permite saber donde se produce la mayor cantidad de interacciones. </span>

								</div>

								<div class="row  align-items-center">

									<div class="col">

										<div id="chart_stats_by_level" class="m-widget14__chart1" style="height: 180px"></div>

									</div>

									<div class="col">

										<div class="m-widget14__legends">

											<div class="m-widget14__legend">

												<span class="m-widget14__legend-bullet m--bg-accent"></span>

												<span class="sub"> - </span>

											</div>

											<div class="m-widget14__legend">

												<span class="m-widget14__legend-bullet m--bg-brand"></span>

												<span class="pp"> - </span>

											</div>

											<div class="m-widget14__legend">

												<span class="m-widget14__legend-bullet m--bg-danger"></span>

												<span class="pb"> - </span>

											</div>
											
										</div>
										
									</div>
									
								</div>
								
							</div>

						</div>

					</div>

					<div class="button-group filters-button-group mb-5">

						<button class="mt-3 btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-brand m-btn--gradient-to-accent btn green button is-checked" data-filter="*">Mostrar todo</button>

						<button class="mt-3 btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent btn green button" data-filter=".subsuelo">Subsuelo</button>

						<button class="mt-3 btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent btn green button" data-filter=".planta_baja">Planta Baja</button>

						<button class="mt-3 btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent btn green button" data-filter=".primer_piso">Primer Piso</button>

					</div>

					<div class="grid row">					
						<?php

						$query = DB::getInstance()->query(

							"SELECT *, count(interactions.id) as interactions_value, apps.id as main_id FROM 

							app_stats as apps LEFT JOIN app_stats_levels as lv ON apps.app_level = lv.level_id 

							LEFT JOIN app_stats_interactions as interactions ON apps.id = interactions.id_app

							WHERE apps.active = ? AND apps.deleted = ? GROUP by apps.id ORDER BY apps.id ", 

							array(1, 0));

						$usersList = $query->results();

						$print = '';

						$now = new DateTime();
						
						$endTime = new DateTime('20:00');

						$openTime = new DateTime('08:00');

						if ($now >= $openTime && $now <= $endTime) {

						}


						if( (date('D') != 'Sat' && date('D') != 'Sun') &&  ($now >= $openTime && $now <= $endTime))  { 
							
							$nowClass = 'btn-success';

							$nowStatus = 'Ahora online';

						}else{

							$nowStatus = 'Ahora offline';

							$nowClass = 'btn-danger';

						}

						foreach($usersList as $actualUser){

							$appId = $actualUser->main_id;

							$print .= '<div class="col-xl-4 element-item ' . $actualUser->level_class . ' ' . $editableLabel . '">

							<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">

							<div class="m-portlet__head m-portlet__head--fit">

							<div class="m-portlet__head-caption">

							<div class="m-portlet__head-action">';

							$print .= '

							</div>

							</div>

							</div>

							<div class="m-portlet__body">

							<div class="m-widget19">

							<div class="m-widget19__pic m-portlet-fit--top m-portlet-fit--sides" style="min-height-: 286px">

							<img src="private/sources/images/screens/' . $appId . '.jpg" alt="">

							<h3 class="m-widget19__title m--font-light">' . strtoupper($actualUser->name) . '</h3>

							<div class="m-widget19__shadow"></div>

							</div>

							<div class="m-widget19__content">

							<div class="m-widget19__header">

							<span class="m-widget21__icon">
							
							<a href="#" class="btn '. $nowClass . ' m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="bottom" title="Las estadísticas se generan en los días y horarios indicados: de lunes a viernes, de 08:00 a 20:00">
							
							<i class="fas fa-bullseye"></i>
							
							</a>
							
							</span>

							<div class="m-widget19__info">

							
							<span class="m-widget19__time">' . $nowStatus . '</span>

							</div>

							<div class="m-widget19__stats" data-skin="dark" data-toggle="m-tooltip" data-placement="bottom" title="Contempla las interaccione históricas, desde que se instaló la app.">

							<span class="m-widget19__number m--font-brand">' . $actualUser->interactions_value . '</span>

							<span class="m-widget19__comment">Interacciones</span>

							</div>

							</div>

							<div class="m-widget19__body">' . $actualUser->description . '</div>
							
							<div class="m-widget15__items">

							</div>

							</div>';

							$print .= '

							<div class="m-widget19__action">

							<button type="button" data-id = "'. $appId . '" class="go-to-app-data btn m-btn--pill btn-secondary m-btn m-btn--hover-brand m-btn--custom">
							
							ver estadísticas
							
							</button>

							</div>

							</div>

							</div>

							</div>

							</div>';	

							?>

						<?php } 

						echo $print;

						?>	

					</div>

				</div>

			</div>

		</div>

	</div>

	<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i> </div>

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="assets/vendors/custom/jquery-ui/jquery-ui.bundle.js" type="text/javascript"></script>

<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>

<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/museum/stats/museum-stats-touch-screens.js" type="text/javascript"></script>

</body>

</html>
