<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$subjectId = '';

if(Input::exists("get") && Input::get('id')){

	$subjectId = Input::get('id');

}

$query = DB::getInstance()->query( 'SELECT * FROM support_settings as mp');

$support = $query->first();


$supportsSubjects = DB::getInstance()->query( 'SELECT * FROM museum_support_subjects as mbs');

$supportsResults = $supportsSubjects->results();

$user = new User();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Soporte | Soporte Técnico</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});
		
	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<input type="hidden" id="user" name="user" value="<?php echo $user->data()->name;?>">

	<input type="hidden" id="user-id" name="user-id" value="<?php echo $user->data()->id;?>">

	<div class="m-grid m-grid--hor m-grid--root m-page">
		
		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Soporte Técnico</h3>			

						</div>

					</div>

				</div>
				
				<div class="m-content">

					<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">

						<div class="m-alert__icon"><i class="flaticon-exclamation m--font-brand"></i></div>

						<div class="m-alert__text">

							Para soporte técnico podés escribirnos a <a href='mailto:<?php echo $support->support_email;?>'><?php echo $support->support_email;?></a> o llamarnoso al <a href='mailto:<?php echo $support->help_phone;?>'><?php echo $support->help_phone;?></a>.<br><br>Si preferís hacerlo por este medio, usá el siguiente formulario.

						</div>

					</div>

					<div class="row">

						<div class="col-xl-12 col-lg-12">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">

											<h3 class="m-portlet__head-text">

												Contacto: 

											</h3>

										</div>

									</div>

								</div>

								<div class="tab-content">

									<div class="tab-pane active" id="m_museum_general">

										<form id="report-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">motivo*:</label>

												<div class="col-lg-6 col-12">

													<select class="custom-select my-1 mr-sm-2" id="subjects">

														<?php  foreach($supportsResults as $result){ 

															$active = ($result->id == $subjectId)  ? 'selected' : '';

															?>
														
														<option value="<?php echo $result->id;?>" <?php echo $active;?>><?php echo $result->subject;?></option>
														
													<?php }?>

													</select>
													
												</div>

											</div>

											<div class="form-group m-form__group row">


												<label class="col-form-label col-lg-3 col-sm-12">Mensaje*:</label>


												<div class="col-lg-6 col-1-2">

													<textarea class = "form-control m-input" 

													name="report" id="report"></textarea>

												</div>

											</div>

											<div class="m-portlet__foot m-portlet__foot--fit">

												<div class="m-form__actions">

													<div class="row">

														<div class="col-2"></div>

														<div class="col-lg-2 col-12 btn-group">

														
															<button id="send-report" type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom mr-3">Enviar</button>

															<button id="back" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">volver</button>

														</div>

													</div>

												</div>

											</div>

										</form>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="assets/app/js/museum/support/museum-support.js" type="text/javascript"></script>

</body>

</html>
