<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

setlocale(LC_ALL, "es_ES", 'Spanish_Spain', 'Spanish');

//---------------------------------------------------------------------------------------------------------------
//interactions hoy...

$survey =  DB::getInstance()->query(

	"SELECT COUNT(*) as all_rows, sum(content_rate) content_rate, sum(experience_qualification) as experience, 

	sum(time_in_museum) as time_in_museum, sum(museum_attention) as attention, sum(app_rate) as app_rate, sum(guided) as guided, 

	sum(recomend) as recomend, sum(museum_rate) as rate, sum(visit_completed) as completed FROM app_survey", [])->first();

$allRows = $survey->all_rows;

//content...

$contentRate = ($survey->content_rate / $allRows);

$contentRate = number_format((float)$contentRate, 2, '.', '');  

$contentRatePercent = $contentRate * 10;


$contentTooltip = "La valoración promedio respecto al contenido general del museo (entre 1 y 10) es de " . $contentRate . " basado en un total de " . $allRows . " votantes";

//experience...

$experienceRate = ($survey->experience  / $allRows);

$experiencePercent = ($experienceRate * 2) * 10;

$experienceRate = number_format((float)$experienceRate, 2, '.', '');  

$experienceTooltip = "La valoración promedio respecto a la experiencia en el museo (entre 1 y 5) es de " . $experienceRate . " basado en un total de " . $allRows . " votantes";

//attention...

$attentionRate = ($survey->attention  / $allRows);

$attentionPercent = ($attentionRate * 2) * 10;

$attentionRate = number_format((float)$attentionRate, 2, '.', '');  

$attentionTooltip = "La valoración promedio respecto a la atención en el museo (entre 1 y 5) es de " . $attentionRate . " basado en un total de " . $allRows . " votantes";

//time...

$timeRate = ($survey->time_in_museum  / $allRows);

$timeRatePercent = ($timeRate * 2)  * 10;

$timeRate = number_format((float)$timeRate, 2, '.', '');  

$timeRateTooltip = "El promedio de permanencia en el museo es de  " . $timeRate . "hs. basado en un total de " . $allRows . " votantes";

$stToTime = gmdate('H:i', floor($timeRate * 3600));

//app...

$appRate = ($survey->app_rate  / $allRows);

$appRate = number_format((float)$appRate, 2, '.', '');  

$appRatePercent = ($appRate * 2)  * 10;

$appRateTooltip = "La valoración de la app (entre 1 y 5) es de " . $appRate . " basado en un total de " . $allRows . " votantes";


$guided = $survey->guided;

$notGuided = $allRows - $guided;

$recomend = $survey->recomend;

$notRecomend  = $allRows - $recomend; 

$completed = $survey->completed;

$notCompleted = $allRows  - $completed;




?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Estadísticas | Encuesta</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() { sessionStorage.fonts = true; }

		});
		
	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
	
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="modal" tabindex="-1" role="dialog">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title"></h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body">

					<div class="m-widget24 add-data">

				</div>

			</div>

			<div class="modal-footer">

				<button type="button" class="btn btn-secondary" data-dismiss="modal">cerrar</button>

			</div>

		</div>

	</div>

</div>

<input type="hidden" name="user-id" id="user-id" value="<?php echo $actualUser;?>">

<input type="hidden" name="recomend" id="recomend" value="<?php echo $recomend;?>">

<input type="hidden" name="not-recomend" id="not-recomend" value="<?php echo $notRecomend;?>">

<input type="hidden" name="guided" id="guided" value="<?php echo $guided;?>">

<input type="hidden" name="not-guided" id="not-guided" value="<?php echo $notGuided;?>">

<input type="hidden" name="completed" id="completed" value="<?php echo $completed;?>">

<input type="hidden" name="not-completed" id="not-completed" value="<?php echo $notCompleted;?>">

<input type="hidden" name="all-rows" id="all-rows" value="<?php echo $allRows;?>">

<div class="m-grid m-grid--hor m-grid--root m-page">

	<?php require_once 'private/includes/header.php'; ?>

	<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

		<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

			<i class="la la-close"></i>

		</button>

		<?php require_once 'private/includes/sidebar.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-wrapper">

			<div class="m-content">

				<div class="d-flex align-items-center">

					<div class="mr-auto">

						<h3 class="m-subheader__title ">ENCUESTAS</h3>

						<p>Info general recopilada desde la encuesta de la app.</p>

					</div>

				</div>

				<div class="m-portlet ">

					<div class="m-portlet__body  m-portlet__body--no-padding">

						<div class="row m-row--no-padding m-row--col-separator-xl">

							<div class="col-md-12 col-lg-6 col-xl-4">

								<div class="m-widget24" data-skin="dark" data-toggle="m-tooltip" 

								data-placement="bottom" title="<?php echo $contentTooltip;?>">

								<div class="m-widget24__item">

									<h4 class="m-widget24__title">Calificación del contenido: </h4>

									<br>

									<span class="m-widget24__desc">El promedio es <strong><?php echo $contentRate;?> / 10</strong> sobre un total de <strong><?php echo $allRows;?> valoraciones.</strong></span>

									<span class="m-widget24__stats m--font-brand" data-skin="dark" data-toggle="m-tooltip" 

									data-placement="bottom" title="" 

									data-original-title="Contempla el total de interacciones de todas las pantallas durante todo el día de hoy."> </span>

									<div class="m--space-10"></div>

									<div class="progress m-progress--sm">

										<div class="progress-bar m--bg-brand" role="progressbar" style="width: <?php echo (int)$contentRatePercent;?>%;"></div>
									</div>

									<span class="m-widget24__change">1</span>

									<span class="m-widget24__number">10</span>

								</div>

							</div>

							<button type="button" class="get-content-data btn m-btn--pill btn-secondary m-btn m-btn--hover-brand m-btn--custom mb-3 ml-3">ver detalles</button>

						</div>

						<div class="col-md-12 col-lg-6 col-xl-4">

							<div class="m-widget24" data-skin="dark" data-toggle="m-tooltip" 

							data-placement="bottom" title="<?php echo $experienceTooltip;?>">

							<div class="m-widget24__item">

								<h4 class="m-widget24__title">Calificación de la experiencia: </h4>

								<br>

								<span class="m-widget24__desc">El promedio es <strong><?php echo $experienceRate;?>/5</strong> sobre un total de <strong><?php echo $allRows;?> valoraciones.</strong></span>

								<span class="m-widget24__stats m--font-success" data-skin="dark" data-toggle="m-tooltip" 

								data-placement="bottom" title="" 

								> </span>

								<div class="m--space-10"></div>

								<div class="progress m-progress--sm">												

									<div class="progress-bar m--bg-success" role="progressbar" style="width: <?php echo (int)$experiencePercent;?>%;"></div>
								</div>

								<span class="m-widget24__change">1</span>

								<span class="m-widget24__number">5</span>

							</div>

						</div>

						<button type="button" class="get-experience btn m-btn--pill btn-secondary m-btn m-btn--hover-brand m-btn--custom mb-3 ml-3">ver detalles</button>

					</div>

					<div class="col-md-12 col-lg-6 col-xl-4">

						<div class="m-widget24" data-skin="dark" data-toggle="m-tooltip" 

						data-placement="bottom" title="<?php echo $attentionTooltip;?>">

						<div class="m-widget24__item">

							<h4 class="m-widget24__title">Calificación de la atención: </h4>

							<br>

							<span class="m-widget24__desc">El promedio es <strong><?php echo $attentionRate;?>/5</strong> sobre un total de <strong><?php echo $allRows;?> valoraciones.</strong></span>

							<span class="m-widget24__stats m--font-danger" data-skin="dark" data-toggle="m-tooltip" 

							data-placement="bottom" title="" 

							data-original-title="Contempla el total de interacciones de todas las pantallas durante todo el día de hoy."> </span>

							<div class="m--space-10"></div>

							<div class="progress m-progress--sm">

								<div class="progress-bar m--bg-danger" role="progressbar" style="width: <?php echo (int)$attentionPercent;?>%;"></div>
							</div>

							<span class="m-widget24__change">1</span>

							<span class="m-widget24__number">5</span>

						</div>

					</div>

					<button type="button" class="get-attention btn m-btn--pill btn-secondary m-btn m-btn--hover-brand m-btn--custom mb-3 ml-3">ver detalles</button>

				</div>							

			</div>

		</div>

	</div>

	<div class="m-portlet ">

		<div class="m-portlet__body  m-portlet__body--no-padding">

			<div class="row m-row--no-padding m-row--col-separator-xl">

				<div class="col-md-12 col-lg-6 col-xl-6">

					<div class="m-widget24" data-skin="dark" data-toggle="m-tooltip" 

					data-placement="bottom" title="<?php echo $timeRateTooltip;?>">

					<div class="m-widget24__item">

						<h4 class="m-widget24__title">Tiempo en el museo: </h4>

						<br>

						<span class="m-widget24__desc">El promedio de tiempo en el museo es de <strong><?php echo $stToTime;?></strong> hs sobre un total de <strong><?php echo $allRows;?> valoraciones.</strong></span>

						<span class="m-widget24__stats m--font-warning" data-skin="dark" data-toggle="m-tooltip" 

						data-placement="bottom" title="" 

						data-original-title="Contempla el total de interacciones de todas las pantallas durante todo el día de hoy."> </span>

						<div class="m--space-10"></div>

						<div class="progress m-progress--sm">

							<div class="progress-bar m--bg-warning" role="progressbar" style="width: <?php echo (int)$timeRatePercent;?>%;"></div>
						</div>

						<span class="m-widget24__change">1 hora o menos</span>

						<span class="m-widget24__number">5 hs o más</span>

					</div>

				</div>

				<button type="button" class="get-time btn m-btn--pill btn-secondary m-btn m-btn--hover-brand m-btn--custom mb-3 ml-3">ver detalles</button>

			</div>

			<div class="col-md-12 col-lg-6 col-xl-6">

				<div class="m-widget24" data-skin="dark" data-toggle="m-tooltip" 

				data-placement="bottom" title="<?php echo $appRateTooltip;?>">

				<div class="m-widget24__item">

					<h4 class="m-widget24__title">App Rate: </h4>

					<br>

					<span class="m-widget24__desc">El promedio es <strong><?php echo $appRate;?></strong> sobre un total de <strong><?php echo $allRows;?> valoraciones.</strong></span>

					<span class="m-widget24__stats m--font-accent" data-skin="dark" data-toggle="m-tooltip" 

					data-placement="bottom" title="" 

					data-original-title="Contempla el total de interacciones de todas las pantallas durante todo el día de hoy."> </span>

					<div class="m--space-10"></div>

					<div class="progress m-progress--sm">

						<div class="progress-bar m--bg-accent" role="progressbar" style="width: <?php echo (int)$appRatePercent;?>%;"></div>
					</div>

					<span class="m-widget24__change">1</span>

					<span class="m-widget24__number">5</span>

				</div>

			</div>

			<button type="button" class="get-app-rate btn m-btn--pill btn-secondary m-btn m-btn--hover-brand m-btn--custom mb-3 ml-3">ver detalles</button>

			<button type="button" class="go-to-app-data btn m-btn--pill btn-secondary m-btn m-btn--hover-brand m-btn--custom mb-3 ml-3"><i class="fab fa-android"></i> ver stats Android</button>

			<button type="button" class="go-to-app-data btn m-btn--pill btn-secondary m-btn m-btn--hover-brand m-btn--custom mb-3 ml-3"><i class="fab fa-apple"></i> ver stats IOS</button>

		</div>

	</div>

</div>

</div>

<div class="d-flex align-items-center">

	<div class="mr-auto">

		<h3 class="m-subheader__title ">GRÁFICOS</h3>

		<p>Sobre visitas guiadas, recomendaciones y recorrido.</p>

	</div>

</div>


<div class="row">

	<div class="col-xl-4">

		<div class="m-widget14 m-widget14 m-portlet m-portlet--bordered-semi m-portlet--full-height">

			<div class="m-widget14__header">

				<h3 class="m-widget14__title">Visitas guiadas </h3>

				<span class="m-widget14__desc">Del total de <?php echo $allRows;?> visitantes, <?php echo $guided;?> indicaron que tomarían una visita guiada.</span>

			</div>

			<div class="row  align-items-center">

				<div class="col col-12">

					<div id="chart_stats_by_level" class="m-widget14__chart1"></div>

				</div>


				<div class="col">

					<div class="m-widget14__legends">

						<div class="m-widget14__legend">

							<span class="m-widget14__legend-bullet m--bg-warning"></span>

							<span class="op1"> - </span>

						</div>


						<div class="m-widget14__legend">

							<span class="m-widget14__legend-bullet m--bg-danger"></span>

							<span class="op2"> - </span>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

	<div class="col-xl-4">

		<div class="m-widget14 m-widget14 m-portlet m-portlet--bordered-semi m-portlet--full-height">

			<div class="m-widget14__header">

				<h3 class="m-widget14__title">Recomendaciones </h3>

				<span class="m-widget14__desc">Del total de <?php echo $allRows;?> visitantes, <?php echo $recomend;?> recomiendan la visita al museo.</span>

			</div>

			<div class="row  align-items-center">

				<div class="col col-12">

					<div id="chart_stats_by_level_2" class="m-widget14__chart1"></div>

				</div>


				<div class="col">

					<div class="m-widget14__legends">

						<div class="m-widget14__legend">

							<span class="m-widget14__legend-bullet m--bg-accent"></span>

							<span class="op3"> - </span>

						</div>


						<div class="m-widget14__legend">

							<span class="m-widget14__legend-bullet m--bg-success"></span>

							<span class="op4"> - </span>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

	<div class="col-xl-4">

		<div class="m-widget14 m-widget14 m-portlet m-portlet--bordered-semi m-portlet--full-height">

			<div class="m-widget14__header">

				<h3 class="m-widget14__title">Recorrido </h3>

				<span class="m-widget14__desc">Del total de <?php echo $allRows;?> visitantes, <?php echo $completed;?> completaron el recorrido.</span>

			</div>

			<div class="row  align-items-center">

				<div class="col col-12">

					<div id="chart_stats_by_level_3" class="m-widget14__chart1"></div>

				</div>


				<div class="col">

					<div class="m-widget14__legends">

						<div class="m-widget14__legend">

							<span class="m-widget14__legend-bullet m--bg-brand"></span>

							<span class="op5"> - </span>

						</div>


						<div class="m-widget14__legend">

							<span class="m-widget14__legend-bullet m--bg-accent"></span>

							<span class="op6"> - </span>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

</div>

</div>

</div>

</div>

<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i> </div>

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="assets/vendors/custom/jquery-ui/jquery-ui.bundle.js" type="text/javascript"></script>

<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>

<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/museum/stats/museum-survey-stats.js" type="text/javascript"></script>

</body>

</html>


