<?php

//require_once 'private/users/core/init.php';

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$actualId = Input::get('id');

if(Input::exists("get") && Input::get('id')){

	$editMode = true;

	$testimonialsCategories = DB::getInstance()->query('SELECT * FROM 

		museum_testimonials_categories_relations 

		WHERE id_museum_testimonial=? AND deleted = ?', [Input::get('id'), 0]);

	$testimonialCategoriesItems = $testimonialsCategories->results();

	$testimonialsCategoriesArray = [];

	foreach($testimonialCategoriesItems as $c){

		$testimonialsCategoriesArray[] = $c->id_museum_testimonial_category;

	}

	$testimonialQuery = DB::getInstance()->query(

		'SELECT *, location.id as location_id, videos.id as video_id FROM museum_testimonials as mt 

		LEFT JOIN world_location_content as location

		ON mt.id = location.sid AND location.source = ?

		LEFT JOIN museum_youtube_videos as videos

		ON mt.id = videos.sid AND videos.source = ?

		WHERE mt.id=? ', 

		['testimonials', 'testimonials', $actualId]);

	$testimonialObject = $testimonialQuery->first();

	if(isset($testimonialObject->youtube_id)){

		$videoId = $testimonialObject->video_id;

		$hiddenYTId = $testimonialObject->youtube_id;

		$url = 'https://youtu.be/' . $hiddenYTId;

	}else{

		$videoId = "";

		$hiddenYTId = "";

		$url = '';


	}

	if(isset($testimonialObject->location_id)){

		$locationId = $testimonialObject->location_id;

	}else{

		$locationId = "";
	}

	$name = $testimonialObject->name;

	$surname = $testimonialObject->surname;

	$birthdate = ($testimonialObject->birthdate == "0000-00-00") ? "" : $testimonialObject->birthdate;
	
	$quote = $testimonialObject->quote;

	$countryId = $testimonialObject->country_id;

	$hiddenYTId = '';

	if($countryId != 0){

		$countryQuery = DB::getInstance()->query('SELECT name_es FROM 

			world_location_countries WHERE id = ?', [$countryId]);

		$country = $countryQuery->first()->name_es;

	}else{

		$country = "";
	}

	$regionId = $testimonialObject->region_id;

	if($regionId != 0){

		$regionQuery = DB::getInstance()->query('SELECT name FROM 

			world_location_regions WHERE id = ?', [$regionId]);

		$region = $regionQuery->first()->name;

	}else{

		$region = "";
	}

	$cityId = $testimonialObject->city_id;

	if($cityId != 0){

		$cityQuery = DB::getInstance()->query('SELECT name FROM 

			world_location_cities WHERE id = ?', [$cityId]);

		$city = $cityQuery->first()->name;

	}else{

		$city = "";
	}

	$zl = ($testimonialObject->zl == 1) ? 'checked' : '';

	$testimonialObjectImageQuery = DB::getInstance()->query('SELECT * FROM 

		museum_images WHERE sid = ? AND source = ? AND deleted = ?', [$actualId, 'testimonials', 0]);

	if($testimonialObjectImageQuery->count()>0){

		$testimonialObjectImage = $testimonialObjectImageQuery->first();

		$mimeType = MimeTypes::getExtensionByMimeType($testimonialObjectImage->mimetype);

		$imagePath = 'private/sources/images/testimonials/' . $actualId . '/' . $testimonialObjectImage->unique_id . '/' .

		$actualId . '_' . $testimonialObjectImage->unique_id . '_sq.' . $mimeType;

	}else{

		$imagePath = "";

	}


}else{

	$videoId = '';

	$hiddenYTId = '';

	$url = '';

	$editMode = false;

	$testimonialsCategoriesArray = [];

	$name = "";

	$surname = "";

	$countryId = "";
	
	$country = "";

	$regionId = "";

	$region = "";

	$cityId = "";
	
	$city = "";

	$zl = "";

	$quote = "";

	$birthdate = "";

	$locationId = "";
}

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Testimonios | Agregar Testimonio</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});
		
	</script>

	<!--<link href="assets/css/typeahead.css" rel="stylesheet" type="text/css" />-->

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}

	#previews div {

		display: inline-block;

		/*margin-left:10px;*/

	}


</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<input type="hidden" id="image-path" name="image-path" value="<?php echo $imagePath;?>">

		<input type="hidden" id="testimonial-id" name="testimonial-id" value="<?php echo $actualId;?>">
		
		<input type="hidden" id="testimonial-name" name="testimonial-name" value="<?php echo $name . ' ' . $surname;?>">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Testimonios</h3>			

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>
								
								<li class="m-nav__separator">-</li>
								
								<li class="m-nav__item">
									<a href="" class="m-nav__link">
										<span class="m-nav__link-text">Agregar nuevo testimonio.</span>
									</a>
								</li>
								
							</ul>
						</div>
						<div>

						</div>
					</div>
				</div>
				
				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">

											<h3 class="m-portlet__head-text">

												Agregar nuevo testimonio

											</h3>

										</div>

									</div>

								</div>

								<div class="tab-content">

									<div class="tab-pane active" id="m_museum_general">

										<form id="my-awesome-dropzone" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>	

												</div>

											</div>

											
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Nombre*:</label>

												<div class="col-lg-4 col-md-9 col-8">

													<input id="name" type="text" class="form-control m-input" name="name" placeholder="Nombre de la persona" value="<?php echo $name;?>">
													
												</div>

												<?php if($editMode){ ?>

													<div class="input-group-append"> 

														<button data-db-value="<?php echo $name;?>" data-id="<?php echo $actualId;?>"

															class="update-btn btn btn-default" type="button">actualizar</button>

														</div>

													<?php }?>

												</div>

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-8">Apellido*:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<input type="text" class="form-control m-input" id = "surname" name="surname" placeholder="apellido" value="<?php echo $surname;?>">	

													</div>

													<?php if($editMode){ ?>

														<div class="input-group-append"> 

															<button data-db-value="<?php echo $surname;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>

												</div>

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-8">Nacimiento:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<div class="input-group date" id="birthdate-picker">

															<input type="text" class="form-control m-input" readonly="" 
															value="<?php echo $birthdate;?>" id="birthdate" name="birthdate">

															<span class="input-group-addon"><i class="la la-calendar"></i></span>

														</div>

													</div>

												</div>

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-sm-12">Filtro:*</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<div class="m-checkbox-list">

															<?php 

															$categoriesQuery = DB::getInstance()->query('SELECT * FROM museum_testimonials_categories');

															foreach($categoriesQuery->results() as $c){

																$checked = '';

																foreach($testimonialsCategoriesArray as $item){

																	if($c->id == $item){ $checked = 'checked'; }

																}

																?>

																<label class="m-checkbox m-checkbox--solid m-checkbox--success">

																	<input <?php echo $checked;?> value="<?php echo $c->id;?>" 

																	name="checkboxes[]" 

																	type="checkbox"

																	data-id="<?php echo $actualId;?>">

																	<?php echo $c->category_name;?>

																	<span></span>

																</label>

															<?php } ?>

														</div>

														<span class="m-form__help"><b>Seleccioná</b> al menos una de las opciones.
														</span>

													</div>

												</div>

												<div id="countries-list" class="form-group m-form__group row pt-4">

													<label class="col-form-label col-lg-3 col-sm-12">País:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<input type="text" class="typeahead form-control m-input" name="country" id="country-input" placeholder="país de nacimiento" value="<?php echo $country;?>" autocomplete="off" data-db-reference="<?php echo $locationId?>">

														<input type="hidden" id="hidden-country-id" name="hidden-country-id" value="<?php echo $countryId;?>">

														<span class="m-form__help">Si no conocés este dato dejalo vacío.</span>

													</div>

													<?php if($editMode){ ?>

														<div class="input-group-append"> 

															<button name="country" data-db-reference="<?php echo $locationId?>" class="delete-btn btn btn-default" type="button">Borrar

															</button>

														</div>

													<?php }?>


												</div>

												<div id="regions-list" class="form-group m-form__group row pt-4">

													<label class="col-form-label col-lg-3 col-sm-12">Provincia:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<input type="text" class="typeahead form-control m-input" id="region-input" name="region" placeholder="" value="<?php echo $region;?>" data-db-reference="<?php echo $locationId?>">

														<input type="hidden" id="hidden-region-id" name="hidden-region-id" value="<?php echo $regionId;?>">

														<span class="m-form__help">Si no conocés este dato dejalo vacío.</span>

													</div>

													<?php if($editMode){ ?>

														<div class="input-group-append"> 

															<button name="region" data-db-reference="<?php echo $locationId?>" class="delete-btn btn btn-default" type="button">Borrar

															</button>

														</div>

													<?php }?>



												</div>

												<div id="cities-list" class="form-group m-form__group row pt-4"  >

													<label class="col-form-label col-lg-3 col-sm-12">Ciudad:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<input id="city-input" type="text" class="typeahead form-control m-input" name="city" placeholder="" value="<?php echo $city;?>" data-db-reference="<?php echo $locationId?>">

														<input type="hidden" id="hidden-city-id" name="hidden-city-id" value="<?php echo $cityId;?>">

														<span class="m-form__help">Si no conocés este dato dejalo vacío.</span>

													</div>

													<?php if($editMode){ ?>

														<div class="input-group-append"> 

															<button name="city" data-db-reference="<?php echo $locationId?>" class="delete-btn btn btn-default" type="button">Borrar

															</button>

														</div>

													<?php }?>

												</div>


												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-sm-12">Imagen de perfil:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<div class="m-dropzone dropzone dropzone-previews" id="m-dropzone-one">

															<div class="m-dropzone__msg dz-message needsclick">

																<h3 class="m-dropzone__msg-title">

																	Arrastrá la imagen acá

																</h3>

																<span class="m-dropzone__msg-desc">

																	O cliqueá para <strong>subirla!</strong>

																</span>

															</div>

														</div>

														<span class="m-form__help">Si no subís una imagen, se utilizará una silueta en su reemplazo.</span>

													</div>

												</div>

												<!-- videoo! -->

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-sm-12">Link al video*:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<input id="youtube_id" type="text" class="form-control m-input" name="youtube_id" placeholder="URL del video" value="<?php echo $url;?>">

														<span class="yt-info m-form__help">Copiá y pegá acá la URL del video de <b>YouTube.</b></span>

														<div id="previews">

															<div id="preview-1"></div>
															
															<div id="preview-2"></div>
															
															<div id="preview-3"></div>
															

														</div>

														<input type="hidden" id="hidden-yt-id" name="hidden-yt-id" value="<?php echo $hiddenYTId;?>">

													</div>


													<?php if($editMode){ ?>

														<div class="input-group-append"> 

															<button data-db-value="<?php echo $quote;?>" data-id="<?php echo $videoId?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>

												</div>

												<!-- -->

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-sm-12">Frase:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<textarea type="text" class="form-control m-input" id="quote" name="quote" placeholder="frase"><?php echo $quote;?></textarea>

														<span class="m-form__help">Puede ser una frase que diga en el video, o alguna frase inspiradora que esta persona haya dicho.</span>

													</div>

													<?php if($editMode){ ?>

														<div class="input-group-append"> 

															<button data-db-value="<?php echo $quote;?>" data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>

												</div>

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-sm-12">Zijronó LiBerajá:</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<div class="m-form__group form-group row">

															<div class="col-6">

																<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																	<label>

																		<input <?php echo $zl;?> class="zl toggler-info" 
																		type="checkbox" 

																		data-id="<?php echo $actualId;?>" name="zl">

																		<span></span>

																	</label>

																</span>

															</div>

														</div>

													</div>

												</div>

												<div class="m-portlet__foot m-portlet__foot--fit">

													<div class="m-form__actions">

														<div class="row">

															<div class="col-2"></div>

															<div class="col-7">

																<input type="hidden" name="token" value="<?php echo Token::generate()?>">

																<?php 

																$hiddenClass = ($editMode) ? 'd-none' : '';

																?>

																<button id="add_testomony" type="submit" 

																class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar Testimonio</button>
																&nbsp;&nbsp;

																<?php 

																$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

																$buttonId = 

																(!$editMode) ? 'exit_from_form' : 'back_to_list';

																?>

																<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																	<?php echo $buttonLabel;?>

																</button>

															</div>

														</div>

													</div>

												</div>

											</form>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

			<?php require_once 'private/includes/footer.php'; ?>

		</div>

		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

		<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

		<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.es.min.js" type="text/javascript"></script>

		<script src="assets/app/js/helper.js" type="text/javascript"></script>

		<script src="assets/app/js/museum/testimonials/museum-testimonial-add.js" type="text/javascript"></script>

	</body>

	</html>
