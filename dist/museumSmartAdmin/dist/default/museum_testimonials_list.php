<?php

require_once 'private/users/core/init.php';

if(Input::exists('get')){

	if(Input::get('tabId')){

		$tabId = Input::get('tabId');

	}else{

		$tabId = 1;

	}

}else{

	$tabId = 1;

}

$db = DB::getInstance();

$museumDataQ = $db->query('SELECT * FROM museum_testimonials_categories');

if($museumDataQ->count()>0) $museumDataObject = $museumDataQ->results();

?>


<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Testimonios | Lista</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>



	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/af-2.3.0/r-2.2.2/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>

	<link href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.1/cropper.min.css" rel="stylesheet" type="text/css" />


	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.table-cell-edit{

		background-color: #efefef !important;

		cursor:move;

		border: 1px solid #efefef;

		-webkit-box-shadow: 5px 0 5px -2px #ddd;

		box-shadow: 5px 0 5px -2px #ddd;

	}

	.modal-lg {

		min-width: 80%;

		/*margin: auto;*/

	}

	.temp-test{

		max-width: 100%;

		max-height:550px;

		overflow: hidden !important;

	}

	#result{

		width: 600px;

		height:600px;

		overflow: hidden !important;

	}

	.preview-container, .real-container{

		overflow: hidden !important;

	}

	
</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
				<i class="la la-close"></i>
			</button>
			<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

				<?php require_once 'private/includes/sidebar.php'; ?>

			</div>

			<!-- video modal -->

			<div class="modal fade" id="video_modal" tabindex="-1" role="dialog">

				<div class="modal-dialog" role="document">

					<div class="modal-content">

						<div class="modal-header">

							<h5 class="modal-title">Video</h5>

							<button type="button" class="close" data-dismiss="modal" aria-label="Close">

								<span aria-hidden="true">&times;</span>

							</button>

						</div>

						<div class="modal-body">

							<iframe class= "video-content d-none" width="420" height="315" src="https://www.youtube.com/embed/tgbNymZ7vqY">

							</iframe>

					</div>
					
					<div class="modal-footer">

						<button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>

					</div>

				</div>

			</div>

		</div>

		<!-- end video modal -->
		
		<!-- image modal -->

		<div class="modal fade" id="image_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true" data-keyboard="false" data-backdrop="static">

			<div class="modal-dialog modal-lg" role="document">

				<div class="modal-content">

					<div class="modal-header">

						<h5 class="modal-title" id="image_modal_title"></h5>

						<button type="button" class="close" data-dismiss="modal" aria-label="Close">

							<span aria-hidden="true">×</span>

						</button>


					</div>

					<div class="modal-body">

						<p class='text-alert d-none'>Aún no subiste una imagen para el usuario. Podés agregarla desde aquí.</p>

						<label class="btn btn-primary btn-success" for="inputImage" title="Upload image file">

							<input type="file" class="sr-only" id="inputImage" name="croppResult" accept="image/*">

							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="Import image with Blob URLs">

								<span class="fa fa-upload"></span> <span class="button-label"> Cambiar imagen</span>

							</span>

						</label>

						<p></p>	

						<div class="row images-containers d-none">

							<div class="real-container col-md-6 col-sm-12 col-xs-12" style="overflow: hidden;">

								<p><strong>IMAGEN REAL</strong></p>

								<div id="wo">

								</div>

								<div class="mt-3">

									<!--<div class="btn-group">

										<button type="button" class="flip-horizontal btn btn-default">

											<span class="fa fa-arrows-h" style="color:grey"></span>

										</button>

										<button type="button" class="flip-vertical btn btn-default">

											<span class="fa fa-arrows-v" style="color:grey"></span>

										</button>

									</div>-->

									<div class="btn-group">

										<button type="button" class="zoom-out btn btn-secondary">

											<span class="fa fa-search-minus" style="color:grey"></span>

										</button>

										<button type="button" class="zoom-in btn btn-secondary">

											<span class="fa fa-search-plus" style="color:grey"></span>

										</button>

									</div>

									<div class="btn-group">

										<button type="button" class="move-left btn btn-secondary">

											<span class="fa fa-arrow-left" style="color:grey"></span>

										</button>

										<button type="button" class="move-right btn btn-secondary">

											<span class="fa fa-arrow-right" style="color:grey"></span>

										</button>

										<button type="button" class="move-up btn btn-secondary">

											<span class="fa fa-arrow-up" style="color:grey"></span>

										</button>

										<button type="button" class="move-down btn btn-secondary">

											<span class="fa fa-arrow-down" style="color:grey"></span>

										</button>

									</div>

								</div>

							</div>

							<div class="preview-container col-md-6 col-sm-12 col-xs-12">

								<p><strong>PREVIEW</strong></p>

								<canvas id="preview-canvas" width=550 height=550></canvas>

								<div class="mt-2">

									<div class="btn-group">

										<button type="button" class="convert-to-grey-scale btn btn-secondary">

											<i class="fa fa-tint"></i>

										</button>

										<button type="button" class="reset-to-default btn btn-secondary">

											<i class="fa fa-power-off"></i>


										</button>

										<button type="button" class="custom-edition btn btn-secondary">

											<i class="fa fa-sliders"></i>


										</button>

									</div>

								</div>

								<div class="controllers-group d-none mt-5">

									<div class="form-group m-form__group row">

										<label for="img_saturation" class="col-form-label col-lg-3 col-sm-12">Saturación:</label>

										<div class="col-lg-9 col-md-9 col-sm-12">

											<div class="m-ion-range-slider">

												<input type="text" id="img_saturation" name="img_saturation" value="" />

											</div>

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="img_brightness" class="col-form-label col-lg-3 col-sm-12">Brillo:</label>

										<div class="col-lg-9 col-md-9 col-sm-12">

											<div class="m-ion-range-slider">

												<input type="text" id="img_brightness" name="img_brightness" value="" />

											</div>

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="img_contrast" class="col-form-label col-lg-3 col-sm-12">Contraste:</label>

										<div class="col-lg-9 col-md-9 col-sm-12">

											<div class="m-ion-range-slider">

												<input type="text" id="img_contrast" name="img_contrast" value="" />

											</div>

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="img_vibrance" class="col-form-label col-lg-3 col-sm-12">Vibrance:</label>

										<div class="col-lg-9 col-md-9 col-sm-12">

											<div class="m-ion-range-slider">

												<input type="text" id="img_vibrance" name="img_vibrance" value="" />

											</div>

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="img_exposure" class="col-form-label col-lg-3 col-sm-12">Exposición:</label>

										<div class="col-lg-9 col-md-9 col-sm-12">

											<div class="m-ion-range-slider">

												<input type="text" id="img_exposure" name="img_exposure" value="" />

											</div>

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="img_sharpen" class="col-form-label col-lg-3 col-sm-12">Sharpen:</label>

										<div class="col-lg-9 col-md-9 col-sm-12">

											<div class="m-ion-range-slider">

												<input type="text" id="img_sharpen" name="img_sharpen" value="" />

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

					<div class="modal-footer">

						<button type="button" class="save-img-changes btn btn-success d-none" data-dismiss="modal">Guardar Cambios</button>

						<button type="button" class="reset-img-content btn btn-danger" data-dismiss="modal">Cerrar</button>

					</div>

				</div>

			</div>

		</div>

		<div class="m-grid__item m-grid__item--fluid m-wrapper">

			<div class="m-subheader ">

				<div class="d-flex align-items-center">

					<div class="mr-auto">
						<h3 class="m-subheader__title m-subheader__title--separator">

							Testimonios

						</h3>

						<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

							<li class="m-nav__item m-nav__item--home">

								<a href="index.php" class="m-nav__link m-nav__link--icon">

									<i class="m-nav__link-icon la la-home"></i>

								</a>

							</li>

							<li class="m-nav__separator">-</li>

							<li class="m-nav__item"> <span class="m-nav__link-text">Listado del testimonios</span></li>

						</ul>

					</div>

				</div>

			</div>

			<div class="m-content">

				<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">

					<div class="m-alert__icon"><i class="flaticon-exclamation m--font-brand"></i></div>

					<div class="m-alert__text">

						Esta página incluye todos los testimonios, segmentados en distintas categorías. Recordá que un testimonio puede pertenecer a más de una categoría. Podés cambiar la visibilidad de un testimonio a través del botón <strong>Estado</strong>. También podés <i class="la la-edit"></i><strong>editar</strong> y <i class="la la-trash"></i><strong>eliminar</strong> cada entrada.<br><br>¿Querés subir otro testimonio?<br><br>

						<a class="btn btn-xs btn-success" href="museum_testimonial_add.php">Agregar</a><br><br>

						¿Algo no funciona? <a class="" href="#">reportar error.</a><br><br>
					</div>

				</div>

				<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">

					<div class="m-portlet__head">

						<div class="m-portlet__head-tools">

							<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" 
							role="tablist">

							<?php

							foreach($museumDataObject as $categoryResult){

								$active = ($categoryResult->id == $tabId) ? 'active' : '';

								$divId = '#tab_' . $categoryResult->id;

								echo '<li class="nav-item m-tabs__item" data-id="'. $categoryResult->id .'">

								<a class="nav-link m-tabs__link '. $active .'" data-toggle="tab" href="'. $divId .'" role="tab">

								<i class="flaticon-share m--hide"></i>'.$categoryResult->category_name.'</a>

								</li>';

							}

							?>

						</ul>

					</div>

				</div>

				<div class="tab-content">

					<?php

					foreach($museumDataObject as $categoryResult){ 

						$active = ($categoryResult->id == $tabId) ? 'active' : '';

						?>

						<div class="tab-pane <?php echo $active;?>" id="tab_<?php echo $categoryResult->id;?>">

							<div class="m-portlet__body">

								<table id="tab-<?php echo $categoryResult->id;?>" class="table display table-striped table-bordered responsive nowrap" style="width:100%">

									<thead>

										<tr>

											<th>Orden</th>

											<th>id</th>

											<th>Nombre</th>

											<th>Pais</th>

											<th>Visibilidad</th>

											<th>Zijronó LiBerajá</th>

											<th>Acciones</th>

											<th>staff member id</th>

											<th>Category Id</th>

											<th>Category name</th>

										</tr>

									</thead>

									<tbody>

										<?php

										$testimonialQuery = $db->query("SELECT mspi.mimetype, mspi.unique_id, mtcr.id as listId, mt.name, mt.surname, mtcr.internal_order, mtcr.active, mt.id as mainId, mspi.deleted as image_deleted, wc.name_es as countryName, reg.name as regionName, cit.name as cityName, mt.zl, yt.youtube_id as yt_id

											FROM museum_testimonials as mt 

											LEFT JOIN museum_testimonials_categories_relations as mtcr 

											ON mt.id = mtcr.id_museum_testimonial

											LEFT JOIN museum_images as mspi
											
											ON mt.id = mspi.sid AND mspi.source = ?

											LEFT JOIN world_location_content as wlc

											ON mt.id = wlc.sid AND wlc.source = ?

											LEFT JOIN world_location_countries as wc

											ON wlc.country_id = wc.id

											LEFT JOIN world_location_regions as reg

											ON wlc.region_id = reg.id

											LEFT JOIN world_location_cities as cit

											ON wlc.city_id = cit.id

											LEFT JOIN museum_youtube_videos as yt

											ON mt.id = yt.sid and yt.source = ?

											WHERE mtcr.id_museum_testimonial_category = ? 

											AND mtcr.deleted = ?

											ORDER BY mtcr.internal_order DESC ", array(

												'testimonials', 

												'testimonials', 

												'testimonials', 

												$categoryResult->id, 0));

										$testimonial = $testimonialQuery->results();

										$counter = 0;

										foreach($testimonial as $testimonialResult){

											$counter+=1;

											$checked = ($testimonialResult->active) ? 'checked' : '';

											$zlCheck = ($testimonialResult->zl) ? 'checked' : '';

											?>

											<tr class="row-<?php echo $counter;?>">

												<td>

													<?php echo $testimonialResult->internal_order;?>

												</td>

												<td><?php echo $testimonialResult->listId;?></td>

												<td><?php echo $testimonialResult->name . ' ' . $testimonialResult->surname;?></td>

												<td><?php 

												$regionSeparator =  ($testimonialResult->cityName) ? ', ' : ' ';

												$countrySeparator =  ($testimonialResult->regionName) ? ', ' : ' ';

												echo $testimonialResult->cityName . $regionSeparator . $testimonialResult->regionName . $countrySeparator . $testimonialResult->countryName;?></td>


												<td><input <?php echo $checked;?> type="checkbox" class="my-checkbox" name="my-checkbox" data-size="mini" data-on-color="success" data-on-text="online" data-off-text="offline"></td>

												<td><input <?php echo $zlCheck;?> type="checkbox" class="my-checkbox" name="my-checkbox" data-size="mini" data-on-color="success" data-on-text="si" data-off-text="no"></td>

												<td>

													<span>	

														<button class="edit_row m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">

															<i class="la la-edit"></i>

														</button>

														<button class="delete_row m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">

															<i class="la la-trash"></i>

														</button>

														<button 

														data-image-deleted="<?php if($testimonialResult->image_deleted) 

														echo $testimonialResult->image_deleted;?>" 

														data-image-id="<?php if($testimonialResult->unique_id) 

														echo $testimonialResult->unique_id;?>"

														data-image-mimetype="<?php if($testimonialResult->mimetype)  

														echo MimeTypes::getExtensionByMimeType($testimonialResult->mimetype);?>

														" class="edit_image m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">

														<i class="la la-image"></i>

													</button>	

													<button data-yt = '<?php echo $testimonialResult->yt_id;?>' class="play_video m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="view video">

														<i class="fa fa-youtube-play"></i>

													</button>								

												</span>


											</td>

											<td><?php echo $testimonialResult->mainId;?></td>

											<td><?php echo $categoryResult->id;?></td>

											<td><?php echo $categoryResult->category_name;?></td>

										</tr>

										<?php

									}

									?>

								</tbody>

							</table>

						</div>

					</div>

				<?php } ?>

			</div>


		</div>
	</div>

</div>

</div>

<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">

	<i class="la la-arrow-up"></i>

</div>

<input type="hidden" id="token" name="token" value="<?php echo Token::generate();?>">


<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/ScrollToPlugin.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/camanjs/4.1.2/caman.full.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.1/cropper.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/af-2.3.0/r-2.2.2/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>


<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/museum/testimonials/museum-testimonials-list.js" type="text/javascript"></script>

</body>

</html>
