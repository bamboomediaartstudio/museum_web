<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';


/**
** Page General Data
** Set
*/

$page_title = "Museo de la Shoá | Trivias | Agregar Trivia";
$page_subtitle_h3 = "Trivias";
$page_subtitle_li = "Agregar Trivias";
$page_subtitle_i = "Trivias";

$page_code_url = "assets/app/js/museum/trivias/museum-trivias-add.js";

/* End Set general data */


$actualId = Input::get('id');

$actualTab = 'general-tab';

if(Input::exists('get') && Input::get('tab')){

	$actualTab = Input::get('tab');

}

$editMode = false;

//List of categories relation for one random fact

$actualRelationsList = [];

$actualOptionToShowRelations = [];

if(Input::exists("get") && Input::get('id')){

	$editMode = true;

	$mainQuery = DB::getInstance()->query('SELECT * FROM  museum_trivias

		WHERE id=? AND deleted = ?', [Input::get('id'), 0]);

	$actualTrivia				    = $mainQuery->first();

	$trivia 								= $actualTrivia->trivia;


	//Get catgories relations for this trivia

	$triviaRelationQuery = DB::getInstance()->query('SELECT id, id_trivia, id_category, active, deleted FROM museum_trivias_categories_relations WHERE id_trivia = ? AND active = ? AND deleted = ?', [Input::get('id'), 1, 0]);

	$actualRelationsList = $triviaRelationQuery->results(true);

	//Get feedback options to show to the final user in app (for this random fact)

	$triviaFeedbackOptionQuery = DB::getInstance()->query('SELECT id, id_trivia, id_feedback_option, active, deleted FROM museum_trivias_feedback_show_relations WHERE id_trivia = ? AND active = ? AND deleted = ?', [Input::get('id'), 1, 0]);

	$actualOptionToShowRelations = $triviaFeedbackOptionQuery->results(true);


	//Get custom replies  for this trivia
	$repliesSql = 'SELECT id, id_trivia, correct, reply, active, deleted FROM museum_trivias_custom_replies WHERE id_trivia = ? AND active = ? AND deleted = ?';
	$triviaRepliesQuery = DB::getInstance()->query($repliesSql, [Input::get('id'), 1, 0]);

	$actualRepliesList = $triviaRepliesQuery->results(true);

	/*
	echo "<pre>";
	print_r($actualRepliesList);
	echo "</pre>";*/


}else{

  $trivia  = '';


}

//museum data...

$museumData = DB::getInstance();

$museumDataQ = $museumData->query('SELECT * FROM museum_data');

if($museumDataQ->count()>0) $museumDataObject = $museumDataQ->first();

//museum social media data...

$museumSocialMediaData = DB::getInstance();

$museumSocialMediaDataQ = $museumSocialMediaData->query('SELECT * FROM museum_social_media');

if($museumSocialMediaDataQ->count()>0) $museumSocialMediaObject = $museumSocialMediaDataQ->first();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title><?php echo $page_title; ?></title>

	<meta name="description" content="add new Survivor">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{

		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}

	/*.note-editing-area {padding-top: 10px !important;}*/


</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">


		<input type="hidden" id="trivia-id" name="trivia-id" value="<?php echo $actualId;?>">


		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator"><?php echo $page_subtitle_h3; ?></h3>
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>

								<li class="m-nav__separator">-</li>

								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text"><?php echo $page_subtitle_li; ?></span>

									</a>

								</li>

							</ul>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">

							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link <?php if($actualTab =='general-tab') echo 'active';?>" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Información general

												</a>

											</li>

											<li class="nav-item m-tabs__item <?php if(!$editMode) echo 'disabled'; ?>">
<<<<<<< HEAD
												
=======
>>>>>>> e0b157755a5f16e0c10cd2dba9a0139208e88f34
												<a class="<?php if($actualTab =='reply-tab') echo 'active';?> tab-navigation images-tab nav-link m-tabs__link" data-toggle="tab" href="#reply-tab" role="tab">

													Respuestas a esta trivia

												</a>

											</li>

										</ul>

									</div>

								</div>


								<div class="tab-content">

									<!-- Respuestas a trivias -->
<<<<<<< HEAD

									<div class="tab-pane <?php if($actualTab =='reply-tab') echo 'active';?>" id="reply-tab">
=======
									<div class="tab-pane <?php if($actualTab =='images-tab') echo 'active';?>" id="reply-tab">
>>>>>>> e0b157755a5f16e0c10cd2dba9a0139208e88f34

										<div class="m-portlet__body">

											<!-- Respuesta a trivia -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Respuestas:*

												<br />

												<small data-toggle="tooltip" data-placement="bottom" title="Máximo 255 caracteres">
													¡Lee esto!
												</small>

												</label>

												<div class="col-lg-6 col-md-9 col-sm-12">

													<textarea numlines="20" type="text" class="form-control m-input description" id="reply" name="reply" placeholder="Respuestas"></textarea>

													<?php if($editMode){  ?>

														<br>

														<div class="">

															<button data-id="<?php echo $actualId;?>"

																class="add-new-reply btn btn-info" type="button">Agregar

															</button>

														</div>

													<?php }?>

												</div>

											</div>
											<!-- /. Respuesta a trivia -->

												<table id="replies" class="table display table-striped table-bordered responsive nowrap" style="width:100%">

													<thead>

														<tr>
															<th>Orden</th>

															<th>id</th>

															<th>Trivia</th>

															<th>Respuesta
																<small data-toggle="tooltip" data-placement="bottom" title="Sólo una respuesta puede ser correcta. Al seleccionar una, se desactivan las demas">
																	¡Lee esto!
																</small>
															</th>

															<th>Acciones</th>

														</tr>

													</thead>

													<tbody>

													<?php

														//If we are in editMode ... check for saved replies

														if($editMode){

															if(count($actualRepliesList)){

																$counter = 0;

																	foreach($actualRepliesList as $id => $replyData){

																		$counter ++;

																		$correctChecked = ($replyData["correct"] == 1) ? 'checked' : '';

																		?>

																		<tr>
																			<td><?php echo $counter; ?></td>

																			<td><?php echo $replyData["id"]; ?></td>

																			<td><?php echo $replyData["reply"]; ?></td>

																			<td><input type="checkbox" id="<?php echo $replyData['id']; ?>" class="editor-active my-checkbox" <?php echo $correctChecked; ?> name="my-checkbox" data-size="mini" data-on-color="success" data-on-text="Correcta" data-off-text="Incorrecta"></td>

																			<td>

																				<span>

																					<button class="delete_row_reply m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">

																						<i class="la la-trash"></i>

																					</button>

																				</span>


																			</td>

																		</tr>

																		<?php
																	}

																}else{

																	?>

																	<tr>
																		<td>Sin respuestas para esta trivia</td>
																		<td> </td>
<<<<<<< HEAD
																		<td> </td>
																		<td> </td>
																		<td> </td>
=======
>>>>>>> e0b157755a5f16e0c10cd2dba9a0139208e88f34
																	</tr>

																	<?php

																}

														}

													?>


													</tbody>

												</table>

										</div>

									</div>

									<!-- general tab -->

									<div class="tab-pane <?php if($actualTab =='general-tab') echo 'active';?>" id="general-tab">

										<form id="add-trivia-data-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>

												</div>

											</div>

											<!-- Trivia -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Trivia:*

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Máximo 255 caracteres">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-6 col-md-9 col-sm-12">

													<textarea numlines="20" type="text" class="form-control m-input description" id="trivia" name="trivia" placeholder="Trivia"><?php echo trim($trivia); ?></textarea>
<<<<<<< HEAD
													<span class="d-none summernote-description-error m-form__help">Este contenido no puede estar vacío.</span>
=======
>>>>>>> e0b157755a5f16e0c10cd2dba9a0139208e88f34

													<?php if($editMode){  ?>

														<br>

														<div class="">

															<button data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>

												</div>

											</div>
											<!-- /.Trivia -->

											<!-- Categories -->
											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Categorias:*

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="No olvides seleccionar al menos una cateogoría">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<div class="m-checkbox-list">

														<?php

														//Get all categories (apps)

														$categoriesQuery = DB::getInstance()->query('SELECT * FROM app_museum_categories');

														foreach($categoriesQuery->results() as $c){

															$checked = '';


															foreach($actualRelationsList as $idRel => $item){

																if($c->id == $item["id_category"]) $checked = 'checked';
															}

															?>

															<label class="m-checkbox m-checkbox--solid m-checkbox--success">

																<input <?php echo $checked;?> value="<?php echo $c->id;?>" name="checkboxes[]" type="checkbox" class="" data-id="<?php echo $actualId;?>">

																<?php echo $c->name;?>

																<span></span>

															</label>


														<?php } ?>

													</div>

													<span class="m-form__help"><b>Seleccioná</b> al menos una de las opciones.
													</span>

													<?php if($editMode){  ?>

														<br>

														<div class="">

															<button data-db-value="categoriesFlag" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
															</button>

														</div>

													<?php }?>

												</div>

											</div>
											<!-- /.categories -->

											<!-- ... -->

											<div class="m-portlet__foot m-portlet__foot--fit">

												<div class="m-form__actions">

													<div class="row">

														<div class="col-2"></div>

														<div class="col-7">

															<input type="hidden" name="token" value="<?php echo Token::generate()?>">

															<?php

															$hiddenClass = ($editMode) ? 'd-none' : '';

															?>

															<button id="add_trivia_reply" type="submit"

															class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar Respueta</button>
															&nbsp;&nbsp;

															<?php

															$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

															$buttonId =

															(!$editMode) ? 'exit_from_form' : 'back_to_list';

															?>

															<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																<?php echo $buttonLabel;?>

															</button>

														</div>

													</div>

												</div>

											</div>

										</form>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/af-2.3.0/r-2.2.2/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="<?php echo $page_code_url; ?>" type="text/javascript"></script>

</body>

</html>
