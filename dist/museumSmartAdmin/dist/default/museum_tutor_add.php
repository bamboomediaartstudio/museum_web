<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

if(Input::exists("get") && Input::get('id')){

	$actualId = Input::get('id');

	$editMode = true;

	$tutorQuery = DB::getInstance()->query('SELECT * FROM  museum_courses_tutors  

		WHERE id = ? AND deleted = ?', [Input::get('id'), 0]);

	$actualTutor = $tutorQuery->first();

	$tutorName = $actualTutor->name;

	$tutorSurname = $actualTutor->surname;

	$whatsapp = $actualTutor->whatsapp;

	$email = $actualTutor->email;

	$phone = $actualTutor->phone;

	$tutorBio = $actualTutor->bio;
	
	//image!

	$courseImageQuery = DB::getInstance()->query('SELECT * FROM 

		museum_images WHERE sid = ? AND source = ? AND deleted = ?', [$actualId, 'tutors', 0]);

	if($courseImageQuery->count()>0){

		$courseImage = $courseImageQuery->first();

		$mimeType = MimeTypes::getExtensionByMimeType($courseImage->mimetype);

		$imagePath = 'private/sources/images/tutors/' . $actualId . '/' . $courseImage->unique_id . '/' . $actualId . '_' . $courseImage->unique_id . '_original.' . $mimeType;

	}else{

		$imagePath = "";

	}

}else{

	$editMode = false;

	$tutorName = "";

	$tutorSurname = "";

	$whatsapp = '';

	$email = '';

	$phone = '';

	$tutorBio = '';
}


?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Tutores | Agregar tutor</title>

	<meta name="description" content="add new tutor">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{
		
		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	#map {

		height: 500px;

		width:100%;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}

</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<input type="hidden" id="image-path" name="image-path" value="<?php echo $imagePath;?>">

		<input type="hidden" id="tutor-id" name="tutor-id" value="<?php echo $actualId;?>">
		
		<input type="hidden" id="tutor-name" name="tutor-name" value="<?php echo $tutorName;?>">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Tutores</h3>		
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>
								
								<li class="m-nav__separator">-</li>
								
								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text">Agregar Tutor</span>

									</a>

								</li>
								
							</ul>

						</div>
						
					</div>

				</div>
				
				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Información general

												</a>

											</li>

										</ul>

									</div>

								</div>

								
								<div class="tab-content">

									
									<div class="tab-pane active" id="general-tab">

										<form id="add-data-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>	

												</div>

											</div>

											<!-- nombre del tutor-->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Nombre del tutor*:</label>

												<div class="col-lg-4 col-md-9 col-8">

													<input id="name" type="text" class="form-control m-input" name="name" placeholder="Nombre del tutor" value="<?php echo $tutorName;?>">

												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $tutorName;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Apellido del tutor*:</label>

												<div class="col-lg-4 col-md-9 col-8">

													<input id="surname" type="text" class="form-control m-input" name="surname" placeholder="Apellido del tutor" value="<?php echo $tutorSurname;?>">

												</div> 

												<?php if($editMode){ ?>

													<div class="">

														<button data-db-value="<?php echo $tutorSurname;?>" data-id="<?php echo $actualId;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												<?php }?>

											</div>
											

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Bio:*</label>

												<div class="col-lg-6 col-md-9 col-sm-12">	

													<textarea numlines="20" type="text" class="form-control m-input description" id="bio" name="bio" placeholder="Descripción"><?php echo trim($tutorBio); ?></textarea>	
												

													<?php if($editMode){  ?>

														<br>

														<div class=""> 

															<button data-id="<?php echo $actualId;?>"

																class="update-btn btn btn-default" type="button">actualizar

															</button>

														</div>

													<?php }?>	

												</div>

											</div>

											<div class="form-group m-form__group row">

												<label class="col-form-label col-lg-3 col-sm-12">Imagen de Portada*:
													<br><small>Es la imagen del tutor y tiene que ser de 500 x 500 px o superior.<br>

													</small>

												</label>

												<div class="col-lg-4 col-md-9 col-sm-12">

													<input id="main-image" name="main-image" type="file">

												</div>

											</div>

											<div class="m-portlet__head">

												<div class="m-portlet__head-caption">

													<div class="m-portlet__head-title">

														<h3 class="m-portlet__head-text">

															Contacto

														</h3>

													</div>

												</div>

											</div>

											<!-- whatsapp -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">WhatsApp:</label>

												<div class="col-lg-4 col-md-9 col-8">

													<div class="m-input-icon m-input-icon--left">

														<input type="number" class="form-control m-input" id="whatsapp" name="whatsapp" placeholder="Una línea con whatsapp" value="<?php echo $whatsapp;?>">

														<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fab fa-whatsapp"></i></span></span>

													</div>

													<span class="m-form__help">Sólo números.</span>

												</div>

												<?php if($editMode){ ?>

													<div class=""> 

														<button data-db-value="<?php echo $whatsapp;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar</button>

													</div>

												<?php }?>

											</div>

											<!-- email -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Email:</label>

												<div class="col-lg-4 col-md-9 col-8">

													<div class="m-input-icon m-input-icon--left">

														<input type="text" class="form-control m-input" id="email" name="email" placeholder="email" value="<?php echo $email;?>">

														<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-envelope"></i></span></span>

													</div>

												</div>

												<?php if($editMode){ ?>

													<div class=""> 

														<button data-db-value="<?php echo $email;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar</button>

													</div>

												<?php }?>

											</div>

											<!-- telefono -->

											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-sm-12">Teléfono:</label>

												<div class="col-lg-4 col-md-9 col-8">

													<div class="m-input-icon m-input-icon--left">

														<input type="number" class="form-control m-input" id="phone" name="phone" placeholder="teléfono" value="<?php echo $phone;?>">

														<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="fa fa-phone"></i></span></span>

													</div>

													<span class="m-form__help">Sólo números.</span>

												</div>

												<?php if($editMode){ ?>

													<div class=""> 

														<button data-db-value="<?php echo $phone;?>" data-id="<?php echo $actualId;?>" class="update-btn btn btn-default" type="button">actualizar</button>

													</div>

												<?php }?>

											</div>

											<div class="m-portlet__foot m-portlet__foot--fit">

												<div class="m-form__actions">

													<div class="row">

														<div class="col-2"></div>

														<div class="col-7">

															<input type="hidden" name="token" value="<?php echo Token::generate()?>">

															<?php 

															$hiddenClass = ($editMode) ? 'd-none' : '';

															?>

															<button id="add_new_tutor" type="submit" 

															class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Agregar Tutor</button>
															&nbsp;&nbsp;

															<?php 

															$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

															$buttonId = 

															(!$editMode) ? 'exit_from_form' : 'back_to_list';

															?>

															<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																<?php echo $buttonLabel;?>

															</button>

														</div>

													</div>

												</div>

											</div>

										</form>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="assets/app/js/museum/courses/museum-tutor-add.js" type="text/javascript"></script>

</body>

</html>
