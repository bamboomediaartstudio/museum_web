<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$url = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);

$errorReportQuery = DB::getInstance()->query("SELECT id FROM museum_bugs_subjects WHERE url = ?", array($url));

$errorReportId =  $errorReportQuery->first()->id;


$uniqueToken = Token::generate();

//museum data...

$museumData = DB::getInstance();

$museumDataQ = $museumData->query('SELECT * FROM museum_data');

if($museumDataQ->count()>0) $museumDataObject = $museumDataQ->first();

//museum social media data...

$museumSocialMediaData = DB::getInstance();

$museumSocialMediaDataQ = $museumSocialMediaData->query('SELECT * FROM museum_social_media');

if($museumSocialMediaDataQ->count()>0) $museumSocialMediaObject = $museumSocialMediaDataQ->first();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Wording | Información General</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});
		
	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
	
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>


			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator">Wording</h3>			

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>
								
								<li class="m-nav__separator">-</li>
								
								<li class="m-nav__item">
									<a href="" class="m-nav__link">
										<span class="m-nav__link-text">Frases y palabras del sitio</span>
									</a>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
				
				<div class="m-content">

					<div class="row">

						<div class="col-12">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" 
										role="tablist">

										<li class="nav-item m-tabs__item">
											
											<a class="nav-link m-tabs__link active" data-toggle="tab" href="
											#m_museum_general" role="tab">

											<i class="flaticon-share m--hide"></i>

											Información general

										</a>
									</li>

									

								</ul>

							</div>

						</div>

						<div class="tab-content">

							<div class="tab-pane active" id="m_museum_general">

								<form class="m-form m-form--fit m-form--label-align-right">

									<div class="m-portlet__body">

										<div class="form-group m-form__group m--margin-top-10">

											<div class="show alert m-alert m-alert--default" role="alert">
												Desde esta página vas a poder editar todo lo relativo a palabras y frases dele sitio.<br>

												¿Está faltando algún <strong>campo</strong> que crees que debería estar en el administrador?<a class="" href="museum_bug_report.php?id=<?php echo $errorReportId;?>"><br><br>avisanos!</a><br><br>

											</div>

										</div>

										<?php

										$wordingQuery = DB::getInstance()->query("SELECT * FROM museum_wording ORDER BY sid");

										$counter = 1;


										$title = '';

										foreach($wordingQuery->results() as $result){
											
											if($result->subcategory != $title) {

												?>

												<div class="form-group m-form__group row">

													<div class="col-10">

														<h3 class="col-6 m-form__section"><?php echo $counter . ' - ' . $result->subcategory;?>

														<br><br>

														<small>los cambios que hagas dentro de la categoría <b><?php echo $result->category . ' > ' . $result->subcategory ;?></b> los verás refljados en: 

															<a target="_blank" href="<?php echo $result->url;?>"><?php echo $result->url;?></a>
														</small>

														</h3>
														

													</div>

												</div>

												<?php

												$title = $result->subcategory;

												$counter +=1;

											}?>

											<div class="form-group m-form__group row pt-4">

												<label for="<?php echo $result->item_key;?>" class="col-form-label col-lg-3 col-12"><?php echo $result->description;?></label>

												<div class="col-lg-7 col-12 input-group">

													<input class="form-control m-input" type="text" value="<?php echo $result->value;?>" name="<?php echo $result->item_key;?>" id="<?php echo $result->item_key;?>">

													<div class="">

														<button data-db-value="<?php echo $result->value;?>" data-id="<?php echo $result->id;?>"class="update-btn btn btn-default" type="button">actualizar
														</button>

													</div>

												</div>

											</div>

											<?php

										}

										?>

									</div>



								</form>

							</div>


						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
	<i class="la la-arrow-up"></i>
</div>

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="//maps.google.com/maps/api/js?key=AIzaSyDBGVDv5fOFgfW4ixNZL_2krgkriGu6vvc" type="text/javascript"></script>

<script src="./assets/vendors/custom/gmaps/gmaps.js" type="text/javascript"></script>

<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/museum/wording.js" type="text/javascript"></script>


</body>

</html>
