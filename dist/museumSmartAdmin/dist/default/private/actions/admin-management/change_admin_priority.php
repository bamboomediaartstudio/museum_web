<?php

require_once('../../connect/connect.php');

if (!isset($_POST['id']) || empty($_POST['id']) || !isset($_POST['priority']) || empty($_POST['priority'])){

	$status['code'] = -1;

	$status['msg'] = 'missing get variables...';

	echo json_encode($status);

	return;

}else{

	$id = $_POST['id'];

	$priority = $_POST['priority'];

}

$sql = "UPDATE users_master SET priority = ? WHERE id=?";
 
$stmt = $conn->prepare($sql);

$stmt->bind_param('ii', $priority, $id);

$stmt->execute();

if ($stmt->errno) { 

	$status['code'] = -1;

	$status['msg'] = 'db error';

}else{

	$status['code'] = 1;

	$status['msg'] = 'se cambio la pripridad!';

}

echo json_encode($status);

$stmt->close();


?>