<?php

require_once('../../connect/connect.php');

if (!isset($_POST['id']) || empty($_POST['id'])){

	$status['code'] = -1;

	$status['msg'] = 'missing get variables...';

	echo json_encode($status);

	return;

}else{

	$id = $_POST['id'];

}

$sql = "UPDATE users_master SET deleted = ? WHERE id=?";
 
$stmt = $conn->prepare($sql);

$deleted = 1;

$stmt->bind_param('ii', $deleted, $id);

$stmt->execute();

if ($stmt->errno) { 

	$status['code'] = -1;

	$status['msg'] = 'db error';

}else{

	$status['code'] = 1;

	$status['msg'] = 'tod ok...se elimino de la DB! :)';

}

echo json_encode($status);

$stmt->close();


?>