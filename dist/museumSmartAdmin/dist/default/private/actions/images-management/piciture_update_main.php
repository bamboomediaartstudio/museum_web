<?php

require_once('../../connect/connect.php');

if (!isset($_POST['id']) || empty($_POST['id']) || !isset($_POST['idPropertie']) || empty($_POST['idPropertie'])){

	$status['code'] = -1;

	$status['msg'] = 'missing get variables...';

	echo json_encode($status);

	return;

}else{
	
	$id = $_POST['id'];

	$idPropertie = $_POST['idPropertie'];

}

//...primero quitar todas...

$sql = "UPDATE properties_images SET is_main = ? WHERE id_propertie = ?";

$stmt = $conn->prepare($sql);

$active = 0;

$stmt->bind_param('ii', $active, $idPropertie);

$stmt->execute();

if ($stmt->errno) { 

	$status['code'] = -1;

	$status['msg'] = 'db error';

}else{

	$status['code'] = 1;

	$status['msg'] = 'tod ok :)';

}

//...luego set

$sql = "UPDATE properties_images SET is_main = ? WHERE id_propertie = ? AND id = ?";

$stmt = $conn->prepare($sql);

$active = 1;

$stmt->bind_param('iii', $active, $idPropertie, $id);

$stmt->execute();

if ($stmt->errno) { 

	$status['code'] = -1;

	$status['msg'] = 'db error';

}else{

	$status['code'] = 1;

	$status['msg'] = 'tod ok :)';

}

echo json_encode($status);

$stmt->close();

?>