<?php

require_once('../connect.php');

if (!isset($_POST['item']) || empty($_POST['item'])){

	$status['code'] = -1;

	$status['msg'] = 'missing get variables...';

	echo json_encode($status);

	return;

}else{

	$i = 0;

	$sql = "UPDATE timeline_event_images SET item_order = ? WHERE id = ?";

	foreach ($_POST['item'] as $value) {

		$stmt = $conn->prepare($sql);

		$stmt->bind_param('ii', $i, $value);

		$stmt->execute();
    
    	$i++;
	}

			$stmt->close();


	$status['code'] = 1;

	$status['msg'] = 'tod ok...modifico el orden en la DB... :)';

}

echo json_encode($status);


?>