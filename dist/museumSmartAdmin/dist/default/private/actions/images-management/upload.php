<?php

error_reporting(E_ALL | E_STRICT);

require('UploadHandler.php');

$options = array('image_versions' => array());  

class CustomUploadHandler extends UploadHandler {

    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error, $index = null, $content_range = null) {

      	$file = parent::handle_file_upload($uploaded_file, $name, $size, $type, $error, $index, $content_range);
        
        if (empty($file->error)) {
			
			$projectPath = '../../../../timelineApp/bin/images/' . $GLOBALS['id'] . '/';

			if (!file_exists($projectPath)) { mkdir($projectPath, 0777, true); }

			$supported_image = array('gif', 'jpg', 'jpeg', 'png', 'mp4');

			$imageId = uniqid();

			$imagePath = $projectPath . $imageId;

			if (!file_exists($imagePath)) { mkdir($imagePath, 0777, true); }

			$ext = strtolower(pathinfo($name, PATHINFO_EXTENSION));

			$moveFrom = 'files/' . $name;

			$moveTo = $imagePath . '/' . $imageId . '.' . $ext;

			rename($moveFrom, $moveTo);

			include 'smart_resize_image.function.php';

			if($ext != 'mp4'){

				$hd = $imagePath . '/' . $imageId . '_HD_SCALED.' . $ext;

				$hdp = $imagePath . '/' . $imageId . '_HD_PROPORTIONAL.' . $ext;

				$sq = $imagePath . '/' . $imageId . '_SQ.' . $ext;

				$xsq = $imagePath . '/' . $imageId . '_XSQ.' . $ext;

				$thumb = $imagePath . '/' . $imageId . '_16_9.' . $ext;

			}else{

				$moveTo = '../../../img/default-video-thumbnail/image.png';

				$hd = $imagePath . '/' . $imageId . '_HD_SCALED.png';// . $ext;

				$hdp = $imagePath . '/' . $imageId . '_HD_PROPORTIONAL.png';// . $ext;

				$sq = $imagePath . '/' . $imageId . '_SQ.png';// . $ext;

				$xsq = $imagePath . '/' . $imageId . '_XSQ.png';// . $ext;

				$thumb = $imagePath . '/' . $imageId . '_16_9.png';// . $ext;

			}

			smart_resize_image($moveTo, null, 1920, 1080, true, $hd, false, false, 100);

			smart_resize_image($moveTo, null, 1920, 1080, false, $hdp, false, false, 100);

			smart_resize_image($moveTo, null, 500, 500, false, $sq, false, false, 100);

			smart_resize_image($moveTo, null, 150, 150, false, $xsq, false, false, 100);

			smart_resize_image($moveTo, null, 150, 85, false, $thumb, false, false, 100);

				
			include '../connect.php';

			//$id = $GLOBALS['id'];

			//....................

			$sql = "INSERT INTO timeline_event_images (id_event, id_file, extension, active, deleted) VALUES (?, ?, ?, ?, ?)";

			$stmt = $conn->prepare($sql);

			$active = 1;

			$deleted = 0;

			//$id = 20;

			$stmt->bind_param('issii', $GLOBALS['id'], $imageId, $ext, $active, $deleted);

			$stmt->execute();

			$stmt->close();

        }

        return $file;
    }

	protected function handle_form_data($file, $index) {
        
        $GLOBALS['id'] = @$_REQUEST['id'];
        
    }

}

$upload_handler = new CustomUploadHandler($options);

?>