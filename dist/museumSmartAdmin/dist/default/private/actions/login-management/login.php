<?php

session_start();

require_once('../../connect/connect.php');

$email = $_POST['email'];

$password = $_POST['password'];

$rememberUser = $_POST['rememberUser'];

$status;

$sql = "SELECT id, password, name, surname, nickname FROM users_master WHERE email = ?";

$stmt = $conn->prepare($sql);

$stmt->bind_param('s', $email);

$stmt->execute();

$stmt->store_result();

$stmt->bind_result($id, $DBpassword, $name, $surname, $nickname);

if ($stmt->errno) { 

	$status['code'] = -1;

	$status['msg'] = 'error en la DB...intentalo mas tarde';

}else{

	if ($stmt->num_rows >= 1) {

		$stmt->fetch();

		$isPasswordCorrect = password_verify($password, $DBpassword);

		if($isPasswordCorrect){

			$status['code'] = 1;

			$status['msg'] = 'logged';

			$_SESSION["email"] = $email;

			$_SESSION["id"] = $id;

			if($rememberUser === 'true'){

				$stmt->close();

				$token = bin2hex(openssl_random_pseudo_bytes(32));

				$sql = "INSERT INTO users_auth_tokens (token, email) VALUES (?, ?)";

				$stmt = $conn->prepare($sql);

				$stmt->bind_param('ss', $token, $email);

				$stmt->execute();

				$stmt->store_result();

				if ($stmt->errno) { 

					$status['code'] = -1;

					$status['msg'] = 'error en la DB...intentalo mas tarde';

				}else{

					$cookie = $email . ':' . $token;

					$mac = hash_hmac('sha256', $cookie, 123);

					$cookie .= ':' . $mac;

					setcookie('keepMeLoggedIn', $cookie, time()+31536000,'/');

				}

			}else{

				$status['code'] = 1;

				$status['msg'] = 'El nombre de usuario o la contraseña son incorrectos.';
			}
		
		}else{

			$status['code'] = -1;

			$status['msg'] = 'La contraseña es incorrecta.';
		}

	}else{

		$status['code'] = -1;

		$status['msg'] = 'No se encontró el email en la base de datos.';
	}
}

echo json_encode($status);

?>