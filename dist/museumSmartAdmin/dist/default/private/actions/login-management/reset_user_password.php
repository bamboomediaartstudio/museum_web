<?php

//-----------------------------------------------------------------------------------
//includes y variables...

include('../../connect/connect.php');

include('../../libs/aditivo/SOHelper.php');

require '../../libs/others/Carbon/Carbon.php';

include('../../libs/aditivo/Mailer.php');

use Carbon\Carbon;

$status;

$email;

$mailer = new Mailer();

//-----------------------------------------------------------------------------------
//validacion POST. Si no hay form, salir...

if (empty($_POST)){

	$status['code'] = -1;

	$status['msg'] = 'missing variables...';

	echo json_encode($status);

	return;

}else{

	$email = $_POST['email'];

	//-----------------------------------------------------------------------------------
	//si no es un mail valido, salir...


	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {

		$status['code'] = -1;

		$status['msg'] = 'invalid email';

		echo json_encode($status);

		return;

	}else{

		//-----------------------------------------------------------------------------------
		//si hay data POST y el dato es un mail, consultar DB y ver si existe un user
		//con esa direccion...

		$status['code'] = 1;

		$status['msg'] = 'valid';

		$sql = "SELECT name, surname, nickname, id FROM users_master WHERE email=?";

		global $conn;

		$stmt = $conn->prepare($sql);

		$stmt->bind_param('s', $email);

		$stmt->execute();

		$stmt->bind_result($name, $surname, $nickname, $id);

		//-----------------------------------------------------------------------------------
		//detener si hay error en db...
		
		if ($stmt->errno) { 

			$status['code'] = 2;

			$status['msg'] = 'error en la DB...intentalo mas tarde';

		}else{

			$fetch = $stmt->fetch();

			//-----------------------------------------------------------------------------------
			//si encontro un registro, existe el usuario...
		
			if($fetch==1){

				$status['code'] = 3;

				$status['msg'] = 'el suario existe en la DB';

				$stmt->close();

				//-----------------------------------------------------------------------------------
				//si hay pedidos previos de recuperacion, eliminarlos...

				$sql = "DELETE from users_reset_password WHERE email=?";

				global $conn;

				$stmt = $conn->prepare($sql);

				$stmt->bind_param('s', $email);

				$stmt->execute();

				$stmt->close();

				//-----------------------------------------------------------------------------------
				//insertar la data en DB:
				//
				//id - el id del usuario que solicita el cambio
				//email - el email del usuario al cual se envia el link de recuperacion...
				//el token con hash almacenado para previa recuperacion...
				//la fecha en la que expira el pedido...
				//el IP desde donde se solicito el cambio...
				//luego:
				//enviar mail para recuperacion...


				$sql = "INSERT INTO users_reset_password (id, email, token, expire, ip) VALUES (?,?,?,?,?)";

				global $conn;

				$token = bin2hex(openssl_random_pseudo_bytes(32));

				$hash = password_hash($token, PASSWORD_DEFAULT);

				//$verify = password_verify ($token, $hash);

				//echo $hash;

				//$date = new DateTime();
				
				//$dateResult = $date->format('Y-m-d H:i:s');

				$now = Carbon::now('America/Argentina/Buenos_Aires');

				$helper = new SOHelper();

				$ip = $helper->getClientIp();

				$stmt = $conn->prepare($sql);

				$stmt->bind_param('issss', $id, $email, $hash, $now, $ip);

				$stmt->execute();

				$stmt->close();

				$mailer->sendRecoverEmail($email, $token);

			//-----------------------------------------------------------------------------------
			//se indico un mail correcto pero no existe en la DB. informar a dicha direccion...

			}else{

			 	$status['code'] = 4;

				$status['msg'] = 'el suario NNNOO existe en la DB';

				$stmt->close();

				$mailer->sendFailEmail($email);
			}
		}
	}
}

echo json_encode($status);

?>