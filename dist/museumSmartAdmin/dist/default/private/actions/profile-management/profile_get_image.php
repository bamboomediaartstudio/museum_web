<?php

require '../../connect/connect.php';

if(isset($_POST['id'])){

	$id = $_POST['id'];

	$sql = "SELECT unique_id, extension, x1, y1, x2, y2, crop_width, crop_height FROM users_profile_picture WHERE user_id = ?";

	global $conn;

	$stmt = $conn->prepare($sql);

	$stmt->bind_param('i', $id);

	$stmt->execute();

	$stmt->store_result();

	$stmt->bind_result($unique_id, $extension, $x1, $y1, $x2, $y2, $crop_width, $crop_height);

	if ($stmt->errno) { 

		$status['code'] = -1;

		$status['msg'] = 'db error';

		echo json_encode($status);

		return;

	}else{

		$stmt->fetch();

		if($stmt->num_rows == 1){

			$status['code'] = 1;

			$status['msg'] = 'todo ok...';

			$status['unique_id'] = $unique_id;
			
			$status['extension'] = $extension;

			$status['x1'] = $x1;

			$status['y1'] = $y1;

			$status['x2'] = $x2;

			$status['y2'] = $y2;
			
			$status['crop_width'] = $crop_width;

			$status['crop_height'] = $crop_height;
			
			$status['crop_picture'] = 'images/admin/profile/' . $id .'/'. $unique_id . '/'. $unique_id . '_crop.' . $extension;

			$status['original_picture'] = 'images/admin/profile/' . $id .'/'. $unique_id . '/'. $unique_id . '_original.' . $extension;

		}else{

			$status['code'] = 2;

			$status['msg'] = 'no hay imagen...';

		}
	}
	
}else{

	$status['code'] = -1;

	$status['msg'] = 'no post variables';

	echo json_encode($status);

	return;
}


echo json_encode($status);

?>