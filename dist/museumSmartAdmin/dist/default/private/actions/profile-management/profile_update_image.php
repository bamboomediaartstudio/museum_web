<?php

namespace abeautifulsite;

use Exception;

require '../../libs/others/SimpleImage.php';

require '../../connect/connect.php';

$status;

$uId = uniqid();

$fileName = '';

$fileExtension = 'jpg';

$original = 'original';

$crop = 'crop';

//------------------------------------------------------------------------
//--------------------------------------------------------- post variables

if(isset($_POST['id']) && isset($_POST['x']) && isset($_POST['y']) && isset($_POST['x2']) && isset($_POST['y2']) && isset($_POST['width']) 

	&& isset($_POST['height']) && isset($_POST['imgBase64'])){

	$id = $_POST['id'];

	$x = $_POST['x'];

	$y = $_POST['y'];

	$x2 = $_POST['x2'];

	$y2 = $_POST['y2'];

	$crop_width = $_POST['width'];

	$crop_height = $_POST['height'];

	$originalFileName = $uId . '_' . $original . '.' . $fileExtension;
	
	$cropFileName = $uId . '_' . $crop . '.' . $fileExtension;

}else{

	$status['code'] = -1;

	$status['msg'] = 'no post variables';

	echo json_encode($status);

	return;
}

//------------------------------------------------------------------------
//------------------------------------------------------------ SimpleImage

$path = '../../../images/admin/profile/'. $id;

$newPath = $path . '/' .  $uId . '/';

mkdir($newPath, 0777, true);

try {

	$img = new SimpleImage();

	$img->load_base64($_POST['imgBase64'])->save($newPath . $originalFileName);

	$img->crop($x, $y, $x2, $y2)->resize(150, 150)->save($newPath . $cropFileName);

	$status['code'] = 1;

	$status['msg'] = 'imagen ok';

	$sql = "UPDATE users_profile_picture set unique_id = ?, extension = ?, x1 = ?, y1 = ?, x2 = ?, y2 = ?, crop_width = ?, crop_height = ? WHERE user_id = ?";

	$stmt = $conn->prepare($sql);

	$stmt->bind_param('ssiiiiiii', $uId, $fileExtension, $x, $y, $x2, $y2, $crop_width, $crop_height, $id);

	$stmt->execute();

	if ($stmt->errno) { 

		$status['code'] = -1;

		$status['msg'] = 'error encoding image';

		echo json_encode($status);

		return false;

	}else{

		$status['code'] = 2;

		$status['msg'] = 'db ok';

	}


} catch (Exception $e) {

	$status['code'] = -1;

	$status['msg'] = 'error encoding image';    

}


echo json_encode($status);

?>