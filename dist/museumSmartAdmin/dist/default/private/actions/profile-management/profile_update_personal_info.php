<?php

require_once('../../connect/connect.php');

if (empty($_POST)){

	$status['code'] = -1;

	echo json_encode($status);

	return;
}

$status['code'] = 0;

$name = $_POST['name'];

$surname = $_POST['surname'];

$nickname = $_POST['nickname'];

$bio = $_POST['bio'];

$id = $_POST['id'];

$rol = $_POST['rol'];

$sql = "UPDATE users_master SET name=?, surname=?, nickname=?, bio=?, rol = ? WHERE id=?";

$stmt = $conn->prepare($sql);

$stmt->bind_param('sssssi', $name, $surname, $nickname, $bio, $rol, $id);

$stmt->execute();

if ($stmt->errno) { 

	$status['code'] = 1;

	$status['msg'] = 'error';

}else{
	
	$status['code'] = 2;

	$status['msg'] = 'Información actualizada exitosamente!';
}

echo json_encode($status);

$stmt->close();

?>