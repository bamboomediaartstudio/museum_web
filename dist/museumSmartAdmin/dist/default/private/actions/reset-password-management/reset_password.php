<?php

require_once('../../connect/connect.php');

require_once('../../libs/aditivo/SOHelper.php');

require_once('../../libs/aditivo/Mailer.php');

if (empty($_POST)){

	$status['code'] = -1;

	echo json_encode($status);

	return;
}

$email = $_POST['email'];

$newPassword = $_POST['newpassword'];

$sql = "SELECT password, name FROM users_master WHERE email=?";

$stmt = $conn->prepare($sql);

$stmt->bind_param('s', $email);

$stmt->execute();

$stmt->bind_result($password, $name);

$stmt->fetch();

if ($stmt->errno) { 

	$status['code'] = 1;

	$status['msg'] = 'error en la DB...intentalo mas tarde';

}else{

	$stmt->close();

	$newSql = "UPDATE users_master SET password=? WHERE email=?";

	$stmt = $conn->prepare($newSql);

	$hashAndSalt = password_hash($newPassword, PASSWORD_BCRYPT);

	$stmt->bind_param('ss', $hashAndSalt, $email);

	$stmt->execute();

	if ($stmt->errno) { 

 		$status['code'] = 3;

 		$status['msg'] = 'error en la DB...intentalo mas tarde';

 	}else{

 		$status['code'] = 4;

 		$status['name'] = $name;

 		$status['msg'] = '...La contraseña se cambió exitosamente!';

 		$mailer = new Mailer();

 		$mailer->sendUpdatePasswordEmail($name, $email);

	 }

}

echo json_encode($status);

$stmt->close();

?>