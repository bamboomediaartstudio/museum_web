<?php

//-----------------------------------------------------------------------------------
//includes y variables...

include('../../connect/connect.php');

//require_once '../../users/core/init.php';


$status;

//-----------------------------------------------------------------------------------
//validacion POST. Si no hay form, salir...

if(!isset($_POST['eventName']) || empty($_POST['eventName']) || !isset($_POST['eventDescription']) || empty($_POST['eventDescription']) || !isset($_POST['date'])|| empty($_POST['date'])){

	$status['code'] = 1;

	$status['msg'] = 'missing variables';

}else{

	$eventName = $_POST['eventName'];

	$eventDescription = $_POST['eventDescription'];

	$date = $_POST['date'];

	$sql = "INSERT INTO timeline_events (title, description, date_start) VALUES (?, ?, ?)";

	$createEventStmt = $conn->prepare($sql);

	$createEventStmt->bind_param('sss', $eventName, $eventDescription, $date);

	$createEventStmt->execute();
	
	$status['eventId'] = $createEventStmt->insert_id;

	//...

	//$sql2 = "INSERT INTO timeline_event_context (id_event) VALUES (?)";

	//$createEventContextStmt = $conn->prepare($sql2);

	//$createEventContextStmt->bind_param('i', $status['eventId']);

	//$createEventContextStmt->execute();

	//...

	//...

	$sql3 = "INSERT INTO timeline_event_international (id_event) VALUES (?)";

	$createEventInternational = $conn->prepare($sql3);

	$createEventInternational->bind_param('i', $status['eventId']);

	$createEventInternational->execute();

	//...

	$sql4 = "INSERT INTO timeline_event_national (id_event) VALUES (?)";

	$createEventNational = $conn->prepare($sql4);

	$createEventNational->bind_param('i', $status['eventId']);

	$createEventNational->execute();


	$status['code'] = 2;

	$status['msg'] = 'se creo el evento...';

	$status['eventTitle'] = $eventName;


}

echo json_encode($status);

?>