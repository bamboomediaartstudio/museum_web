<?php

require_once('../../connect/connect.php');

if (!isset($_POST['id']) || empty($_POST['id'])){

	$status['code'] = -1;

	$status['msg'] = 'missing get variables...';

	echo json_encode($status);

	return;

}else{

	$id = $_POST['id'];

	$sql = "UPDATE timeline_events SET deleted = ?, active = ? WHERE id = ?";

	$stmt = $conn->prepare($sql);

	$deleted = 1;

	$active = 0;

	$stmt->bind_param('iii', $deleted, $active, $id);

	$stmt->execute();

	$status['code'] = 1;

	$status['msg'] = 'tod ok :)';

}

echo json_encode($status);

$stmt->close();

?>