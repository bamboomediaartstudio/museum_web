<?php

require_once('../../connect/connect.php');

if (!isset($_POST['id']) || empty($_POST['id']) || !isset($_POST['idEvent']) || empty($_POST['idEvent'])){

	$status['code'] = -1;

	$status['msg'] = 'missing get variables...';

	echo json_encode($status);

	return;

}else{

	$id = $_POST['id'];

	$idEvent = $_POST['idEvent'];

}

$sql = "UPDATE timeline_event_images SET deleted = ? WHERE id=?";
 
$stmt = $conn->prepare($sql);

$deleted = 1;

$stmt->bind_param('ii', $deleted, $id);

$stmt->execute();

if ($stmt->errno) { 

	$status['code'] = -1;

	$status['msg'] = 'db error';

}else{

	$status['code'] = 1;

	$status['msg'] = 'tod ok...se elimino la imagen de la DB! :)';

	$status['query'] = $sql;

	$status['id'] = $id;

	$status['idEvent'] = $idEvent;

}

echo json_encode($status);

$stmt->close();


?>