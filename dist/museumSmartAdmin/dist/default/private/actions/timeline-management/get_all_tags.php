<?php

require_once('../../connect/connect.php');

$sql = "SELECT tags.id, tags.tag FROM tags as tags WHERE tags.active = ? AND tags.deleted = ?";

$tagsStmt = $conn->prepare($sql);

$active = 1;

$deleted = 0;

$tagsStmt->bind_param('ii', $active, $deleted);

$tagsStmt->execute();

$tagsStmt->store_result();

$tagsStmt->bind_result($id, $tag);

if ($tagsStmt->num_rows >= 1) {

	while($tagsStmt->fetch()){

		$item = array();

		//$item['idTag'] = $id;

		$item['tag'] = $tag;

		 $result[] = $item;

	}

}

echo json_encode($result, JSON_PRETTY_PRINT);

die();

?>