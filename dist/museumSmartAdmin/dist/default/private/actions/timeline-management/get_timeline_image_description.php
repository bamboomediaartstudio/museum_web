<?php

require_once('../../connect/connect.php');

if (!isset($_POST['id']) || empty($_POST['id'])){

	$status['code'] = -1;

	$status['msg'] = 'missing get variables...';

	echo json_encode($status);

	return;

}else{
	
	$id = $_POST['id'];

}


$sql = "SELECT description FROM timeline_event_images WHERE id = ?";

$descriptionStmt = $conn->prepare($sql);

$descriptionStmt->bind_param('i', $id);

$descriptionStmt->execute();

$descriptionStmt->store_result();

$descriptionStmt->bind_result($description);

$descriptionStmt->fetch();

$status['code'] = 1;

$status['msg'] = 'tod ok :)';

$status['description'] = $description;

$status['id'] = $id;

echo json_encode($status);

$descriptionStmt->close();

?>