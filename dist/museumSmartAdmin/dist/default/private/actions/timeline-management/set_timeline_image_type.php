<?php

require_once('../../connect/connect.php');

if (!isset($_POST['id']) || empty($_POST['id']) || !isset($_POST['type']) || empty($_POST['type'])){

	$status['code'] = -1;

	$status['msg'] = 'missing get variables...';

	echo json_encode($status);

	return;

}else{
	
	$id = $_POST['id'];

	$type = $_POST['type'];

}

//...primero quitar todas...

$sql = "UPDATE timeline_event_images SET item_type = ? WHERE id = ?";

$stmt = $conn->prepare($sql);

$active = 0;

$stmt->bind_param('ii', $type, $id);

$stmt->execute();

if ($stmt->errno) { 

	$status['code'] = -1;

	$status['msg'] = 'db error';

}else{

	$status['code'] = 1;

	$status['msg'] = 'tod ok :)';

}

echo json_encode($status);

$stmt->close();

?>