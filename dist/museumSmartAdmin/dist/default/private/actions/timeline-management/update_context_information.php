<?php

require_once('../../connect/connect.php');

if (!isset($_POST['id']) || empty($_POST['id'])){

	$status['code'] = -1;

	$status['msg'] = 'missing get variables...';

	echo json_encode($status);

	return;

}else{

	$id = $_POST['id'];
	
	$eventSubtitle = $_POST['eventSubtitle'];
	
	$eventDescription = nl2br($_POST['eventDescription']);

	$sql = "UPDATE timeline_event_context SET title = ?, description = ? WHERE id_event = ?";

	$stmt = $conn->prepare($sql);

	$stmt->bind_param('ssi', $eventSubtitle, $eventDescription, $id);

	$stmt->execute();

	$status['code'] = 1;

	$status['msg'] = 'tod ok :)';

}

echo json_encode($status);

$stmt->close();

?>