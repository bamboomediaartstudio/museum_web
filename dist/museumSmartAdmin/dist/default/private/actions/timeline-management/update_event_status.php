<?php

require_once('../../connect/connect.php');

if (!isset($_POST['active']) || !isset($_POST['id']) || empty($_POST['active']) || empty($_POST['id'])){

	$status['code'] = -1;

	$status['msg'] = 'missing get variables...';

	echo json_encode($status);

	return;

}else{

	$active = ($_POST['active'] == 'true') ? 1 : 0;
	
	$id = $_POST['id'];

}

$sql = "UPDATE timeline_events SET active = ? WHERE id = ?";

$stmt = $conn->prepare($sql);

$stmt->bind_param('ii', $active, $id);

$stmt->execute();

if ($stmt->errno) { 

	$status['code'] = -1;

	$status['msg'] = 'db error';

}else{

	$status['code'] = 1;

	$status['msg'] = 'tod ok :)';

}

echo json_encode($status);

$stmt->close();

?>