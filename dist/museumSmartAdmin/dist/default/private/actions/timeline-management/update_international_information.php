<?php

require_once('../../connect/connect.php');

//id:id, internationalTitle:internationalTitle, internationalDescription:internationalDescription, date:date},
            

if (!isset($_POST['id']) || empty($_POST['id']) || !isset($_POST['title']) || 

	!isset($_POST['description']) || !isset($_POST['date']) || !isset($_POST['type'])){

	$status['code'] = -1;

	$status['msg'] = 'missing get variables...';

	echo json_encode($status);

	return;

}else{

	$id = $_POST['id'];
	
	$title = $_POST['title'];
	
	$description = nl2br($_POST['description']);

	$date = $_POST['date'];

	$type = $_POST['type'];

	if($type == 'international'){

		$sql = "UPDATE timeline_event_international SET title = ?, description = ?, date_start = ? WHERE id_event = ?";

	}else{

		$sql = "UPDATE timeline_event_national SET title = ?, description = ?, date_start = ? WHERE id_event = ?";

	}

	$stmt = $conn->prepare($sql);

	$stmt->bind_param('sssi', $title, $description, $date, $id);

	$stmt->execute();

	$status['code'] = 1;

	$status['msg'] = 'tod ok :)';

}

echo json_encode($status);

$stmt->close();

?>