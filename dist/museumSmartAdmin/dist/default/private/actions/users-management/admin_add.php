<?php

//-----------------------------------------------------------------------------------
//includes y variables...

include('../../connect/connect.php');

include('../../libs/aditivo/SOHelper.php');

include('../../libs/aditivo/Mailer.php');

include('../../libs/others/Carbon/Carbon.php');

use Carbon\Carbon;

$mailer = new Mailer();

$status;

$email;

$status;

$refererFullName;

$referedFullName;

//-----------------------------------------------------------------------------------
//validacion POST. Si no hay form, salir...

if(!isset($_POST['priority']) || empty($_POST['priority']) || !isset($_POST['refererId']) || empty($_POST['refererId']) || !isset($_POST['name']) || empty($_POST['name']) || !isset($_POST['surname']) || empty($_POST['surname']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){

	$status['code'] = 1;

	$status['msg'] = 'missing variables';

}else{

	$email = $_POST['email'];

	$refererId = $_POST['refererId'];

	$name = $_POST['name'];

	$surname = $_POST['surname'];

	$priority = $_POST['priority'];

	//-----------------------------------------------------------------------------------
	//primero chequear si es un usuario REGISTRADO...

	$sql = "SELECT * FROM users_master WHERE email=?";

	$checkRegisterStmt = $conn->prepare($sql);

	$checkRegisterStmt->bind_param('s', $email);

	$checkRegisterStmt->execute();

	$fetch = $checkRegisterStmt->fetch();

	if($fetch==1){

		$status['code'] = 2;

		$status['msg'] = 'el usuario ya esta registrado.';

		$checkRegisterStmt->close();

	}else{

		//-----------------------------------------------------------------------------------
		//en caso de no estar REGISTRADO, chequear que no sea PENDING... 

		$sql = "SELECT * FROM users_pending_list WHERE email=?";

		$checkPendingStmt = $conn->prepare($sql);

		$checkPendingStmt->bind_param('s', $email);

		$checkPendingStmt->execute();

		$fetch = $checkPendingStmt->fetch();

		if($fetch==1){

			$status['code'] = 3;

			$status['msg'] = 'there is a pending user with this email...';

			$checkPendingStmt->close();

			//add pending...


			$sql = "UPDATE users_pending_list SET attempts = attempts + 1 WHERE email = ?";

			$stmtUpdate = $conn->prepare($sql);

			$stmtUpdate->bind_param('s', $email);

			$stmtUpdate->execute();

			$stmtUpdate->close();

		}else{

			$checkPendingStmt->close();

			//-----------------------------------------------------------------------------------
			//get inviter data...

			$sql = "SELECT name, surname FROM users_master WHERE id=?";

			$refererStmt = $conn->prepare($sql);

			$refererStmt->bind_param('i', $refererId);

			$refererStmt->bind_result($inviterName, $inviterSurname);

			$refererStmt->execute();

			$refererStmt->fetch();

			$refererFullName  = $status['refererFullName'] = $inviterName . ' ' . $inviterSurname;

			$referedFullName = $status['referedFullName'] = $name . ' ' . $surname;

			$refererStmt->close();

			//-----------------------------------------------------------------------------------
			//asigna el registro en lista de pendientes...

			$sql = "INSERT INTO users_pending_list (referer, email, name, surname, priority, token, code, expire) VALUES (?,?,?,?,?,?,?,?)";

			$addPendingUserStmt = $conn->prepare($sql);

			$token = bin2hex(openssl_random_pseudo_bytes(32));

			$hash = password_hash($token, PASSWORD_DEFAULT);

			$code = strtoupper(substr(md5(microtime()),rand(0,26),4));

			$now = Carbon::now('America/Argentina/Buenos_Aires');

			$addPendingUserStmt->bind_param('isssisss', $refererId, $email, $name, $surname, $priority, $hash, $code, $now);

			$addPendingUserStmt->execute();

			$mailer->sendInviteNewUser($email, $token, $code, $refererFullName, $referedFullName);

			$status['code'] = 4;

			$status['msg'] = 'se dio de alta al nuevo usuario...';

		}

	}

}

echo json_encode($status);

?>