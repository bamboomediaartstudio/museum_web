<?php

session_start();

session_unset();

session_destroy();

session_write_close();

setcookie(session_name(),'',0,'/');

session_regenerate_id(true);

if (isset($_COOKIE['keepMeLoggedIn'])) {

	unset($_COOKIE['keepMeLoggedIn']);
    
    setcookie('keepMeLoggedIn', '', time() - 3600, '/');
}

$config = require_once('../../config.php');

$login = $config['general-urls']['master_login'];

header("Location: " . $login);

die();

?>