<?php

require_once('../../connect/connect.php');

if (!isset($_POST['userId']) || empty($_POST['userId'])){

	$status['code'] = -1;

	$status['msg'] = 'missing get variables...';

	echo json_encode($status);

	return;

}else{

	$userId = $_POST['userId'];

}


$sql = "SELECT name, surname, email, company, linked_in, position, facebook, url, country, notes, te, youtube FROM users WHERE id = ?";

$stmt = $conn->prepare($sql);

$stmt->bind_param('i', $userId);

$stmt->execute();

$stmt->store_result();

$stmt->bind_result($name, $surname, $email, $company, $linked_in, $position, $facebook, $url, $country, $notes, $te, $youtube);

$stmt->fetch();

if ($stmt->errno) { 

	$status['code'] = -1;

	$status['msg'] = 'db error';

}else{

	$status['code'] = 1;

	$status['name'] = ($name == null) ?  $name = '-' : $name;

	$status['surname'] = ($surname == null) ?  $surname = '-' : $surname;

	$status['email'] = ($email == null) ?  $email = '-' : $email;

	$status['company'] = ($company == null) ?  $company = '-' : $company;

	$status['position'] = ($position == null) ?  $position = '-' : $position;

	$status['linkedIn'] = ($linked_in == null) ?  $linked_in = '-' : $linked_in;

	$status['facebook'] = ($facebook == null) ?  $facebook = '-' : $facebook;

	$status['url'] = ($url == null) ?  $url = '-' : $url;

	$status['country'] = ($country == null) ?  $country = '-' : $country;

	$status['notes'] = ($notes == null) ?  $notes = '-' : $notes;

	$status['te'] = ($te == null) ?  $te = '-' : $te;

	$status['youtube'] = ($youtube == null) ?  $youtube = '-' : $youtube;

}

echo json_encode($status);

$stmt->close();

?>
