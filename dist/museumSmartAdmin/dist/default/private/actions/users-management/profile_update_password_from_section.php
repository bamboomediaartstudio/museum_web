<?php

require_once('../../connect/connect.php');

require_once('../../libs/aditivo/SOHelper.php');

require_once('../../libs/aditivo/Mailer.php');

if (empty($_POST)){

	$status['code'] = -1;

	echo json_encode($status);

	return;
}

$oldPassword = $_POST['oldPassword'];

$id = $_POST['id'];

$newPassword = $_POST['newPassword'];

$userEmail;

$userName;

$sql = "SELECT password, email, name FROM users_master WHERE id=?";

$stmt = $conn->prepare($sql);

$stmt->bind_param('i', $id);

$stmt->execute();

$stmt->bind_result($password, $userEmail, $userName);

if ($stmt->errno) { 

	$status['code'] = 1;

	$status['msg'] = 'error en la DB...intentalo mas tarde';

}else{

	 $stmt->fetch();

	 $isPasswordCorrect = password_verify($oldPassword, $password);

	 if($isPasswordCorrect){

	 	$stmt->close();

	 	$newSql = "UPDATE users_master SET password=? WHERE id=?";

	 	$stmt = $conn->prepare($newSql);

	 	$hashAndSalt = password_hash($newPassword, PASSWORD_BCRYPT);

	 	$stmt->bind_param('si', $hashAndSalt, $id);

	 	$stmt->execute();

	 	if ($stmt->errno) { 

	 		$status['code'] = 3;

	 		$status['msg'] = 'error en la DB...intentalo mas tarde';

	 	}else{

	 		$status['code'] = 4;

	 		$status['msg'] = 'La contraseña se cambió exitosamente!';

	 		$mailer = new Mailer();

	 		global $userName;

	 		global $userEmail;

	 		$mailer->sendUpdatePasswordEmail($userName, $userEmail);

	 	}

	}else{

		$status['code'] = 2;

		$status['msg'] = 'La contraseña actual que indicaste es incorrecta.';

	}	
}
echo json_encode($status);

$stmt->close();

?>