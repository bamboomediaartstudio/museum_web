<?php

//-----------------------------------------------------------------------------------
//includes y variables...

require_once('../../connect/connect.php');

include('../../libs/aditivo/Mailer.php');

if (!isset($_POST['email']) || empty($_POST['email']) || !isset($_POST['pass']) || empty($_POST['pass']) || !isset($_POST['code']) || empty($_POST['code'])){

	$status['code'] = 1;

	$status['msg'] = 'missing get variables...';

	echo json_encode($status);

	return;

}else{

	//-----------------------------------------------------------------------------------
	//variables y select...

	$pendingId;

	$mailer = new Mailer();
	
	$email = $_POST['email'];

	$pass = $_POST['pass'];

	$code = $_POST['code'];

	$lastId;

	$sql = "SELECT id, code, name, surname, referer, priority FROM users_pending_list WHERE email = ?";

	$stmt = $conn->prepare($sql);

	$stmt->bind_param('s', $email);

	$stmt->execute();

	$stmt->store_result();

	$stmt->bind_result($pendingId, $DBCode, $DBName, $DBSurname, $DBReferer, $DBPriority);

	$stmt->fetch();

	//-----------------------------------------------------------------------------------
	//existen un pending con este mail...

	if ($stmt->num_rows >= 1) {

		//-----------------------------------------------------------------------------------
		//el codigo es incorrecto. Volver a pedirle...

		if($DBCode != $code){

			$status['code'] = 1;

			$status['msg'] = 'El codigo es incorrecto. Por favor volvé a intentarlo';

			return;

		}else{

			//-----------------------------------------------------------------------------------
			//si llegamos aca el user existe y el code es correcto. Darlo de alta...

			$stmt->close();

			$hashAndSalt = password_hash($pass, PASSWORD_BCRYPT);

			$sql = "INSERT INTO users_master (email, password, name, surname, nickname, active, priority) VALUES (?, ?, ?, ?, ?, ?, ?)";

			$addNewUserStmt = $conn->prepare($sql);

			$active = 1;

			$pending = 0;

			$nick = 'oli';

			$addNewUserStmt->bind_param('sssssii', $email , $hashAndSalt, $DBName, $DBSurname, $DBName, $active, $DBPriority);

			$addNewUserStmt->execute();

			$lastId = $addNewUserStmt->insert_id;

			$addNewUserStmt->close();

			$status['code'] = 2;

			$status['msg'] = 'ok...alta';

			//-----------------------------------------------------------------------------------
			//asignar el espacio para la foto y mover los archivos...


			$sql = "INSERT INTO users_profile_picture (user_id, unique_id) VALUES (?, ?)";

			$addNewImage = $conn->prepare($sql);

			$active = 1;

			$pending = 0;

			$nick = 'oli';

			$uniqueId = uniqid();

			$addNewImage->bind_param('is', $lastId, $uniqueId);

			$addNewImage->execute();

			$addNewImage->close();

			//$status['code'] = 3;

			$status['msg'] = 'ok...alta IMAGEN';

			$path = '../../../images/admin/profile/' . $lastId . '/' . $uniqueId;

			mkdir($path, 0777, true);

			$imagePath = "../../../images/admin/default/default.jpg";

			$newPathCrop = $path . '/' . $uniqueId . '_crop.jpg';
			
			$newPathOriginal = $path . '/' . $uniqueId . '_original.jpg';

			$copy = copy($imagePath , $newPathCrop);

			$copy = copy($imagePath , $newPathOriginal);

			if(!$copy){

				//error

			}else{
				
				//ok...

			}


			/*$status['query'] = $sql;

			$status['nick'] = $nick;
			
			$status['hashAndSalt'] = $hashAndSalt;
			
			$status['DBName'] = $DBName;
			
			$status['DBSurname'] = $DBSurname;*/

			//$status['active'] = $active;

			//$status['pending'] = $pending;

			//-----------------------------------------------------------------------------------
			//eliminar el pending...


			/*$sql = "DELETE FROM users_pending_list WHERE email = ? AND id = ?";

			$stmt = $conn->prepare($sql);

			$stmt->bind_param('si', $email, $pendingId);

			$stmt->execute();

			if ($stmt->errno) { 

				$status['code'] = -1;

				$status['msg'] = 'db error';

			}else{

				$status['code'] = 2;

				$status['msg'] = 'Los codigos coinciden y se updateo DB..';

			}*/

		}

	}else{

		$status['code'] = -1;

		$status['msg'] = 'No se encontró el email';

	}

}
echo json_encode($status);