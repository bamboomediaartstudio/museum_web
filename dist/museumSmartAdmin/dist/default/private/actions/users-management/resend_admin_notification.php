<?php

//-----------------------------------------------------------------------------------
//includes y variables...

include('../../connect/connect.php');

include('../../libs/aditivo/SOHelper.php');

include('../../libs/aditivo/Mailer.php');

include('../../libs/others/Carbon/Carbon.php');

use Carbon\Carbon;

$mailer = new Mailer();

$status;

$email;

$status;

$refererFullName;

$referedFullName;

//-----------------------------------------------------------------------------------
//validacion POST. Si no hay form, salir...

if(!isset($_POST['priority']) || empty($_POST['priority']) || !isset($_POST['refererId']) || empty($_POST['refererId']) || !isset($_POST['name'])

	|| empty($_POST['name']) || !isset($_POST['surname']) || empty($_POST['surname']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){

	$status['code'] = 1;

	$status['msg'] = 'missing variables';

}else{

	$email = $_POST['email'];

	$name = $_POST['name'];

	$surname = $_POST['surname'];

	$refererId = $_POST['refererId'];

	$priority = $_POST['priority'];

	$refererFullName;

	$referedFullName;

	//-----------------------------------------------------------------------------------
	//get inviter data...

	$sql = "SELECT name, surname FROM users_master WHERE id=?";

	$refererStmt = $conn->prepare($sql);

	$refererStmt->bind_param('i', $refererId);

	$refererStmt->bind_result($inviterName, $inviterSurname);

	$refererStmt->execute();

	$refererStmt->fetch();

	$refererFullName  = $status['refererFullName'] = $inviterName . ' ' . $inviterSurname;

	$referedFullName = $status['referedFullName'] = $name . ' ' . $surname;

	$refererStmt->close();

	//-----------------------------------------------------------------------------------
	//updatear pending...

	$sql = "UPDATE users_pending_list SET token = ?, code = ?, expire = ?, priority = ? WHERE email = ?";

	$stmtUpdate = $conn->prepare($sql);

	$token = bin2hex(openssl_random_pseudo_bytes(32));

	$hash = password_hash($token, PASSWORD_DEFAULT);

	$code = strtoupper(substr(md5(microtime()),rand(0,26),4));

	$now = Carbon::now('America/Argentina/Buenos_Aires');

	$stmtUpdate->bind_param('sssis', $hash, $code, $now, $priority, $email);

	$stmtUpdate->execute();

	$stmtUpdate->close();

	$mailer->sendInviteNewUser($email, $token, $code, $refererFullName, $referedFullName);

	$status['code'] = 2;

	$status['msg'] = 'ok...';
}

echo json_encode($status);


?>