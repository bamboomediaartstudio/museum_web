<?php

//------------------------------------------------------------------------
//--------------------------------------------------------------- defaults

date_default_timezone_set('America/Argentina/Buenos_Aires');

//------------------------------------------------------------------------
//--------------------------------------------------------------- requires

require_once('../../connect/connect.php');

//------------------------------------------------------------------------
//estan las variables?

/*if (!isset($_POST['email']) || empty($_POST['email'])){

	$status['code'] = -1;

	$status['msg'] = 'missing variables...';

	echo json_encode($status);

	return;

}else{*/

	$name = $_POST['name'];

	$surname = $_POST['surname'];
	
	$email = $_POST['email'];
	
	$company = $_POST['company'];

	$country = $_POST['country'];

	$te = $_POST['te'];

	$position = $_POST['position'];

	$linkedIn = $_POST['linkedIn'];

	$userNotes = $_POST['userNotes'];

	$url = $_POST['url'];

	$facebook = $_POST['facebook'];

	$youtube = $_POST['youtube'];

//}

//------------------------------------------------------------------------
//do query...

$sql = "INSERT INTO users (name, surname, email, company, country, te, position, linked_in, notes, url, facebook, youtube, active, deleted) 

VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

$stmt = $conn->prepare($sql);

$active = 1;

$deleted = 0;

$stmt->bind_param('ssssssssssssii', $name, $surname, $email, $company, $country, $te, $position, $linkedIn, $userNotes, $url, $facebook, $youtube, $active, $deleted);

$stmt->execute();

if ($stmt->errno) { 

	$status['code'] = -1;

	$status['msg'] = 'db error';

}else{

	$status['code'] = 1;

	$status['msg'] = 'tod ok :)';

	$status['lasrInsertId'] = $stmt->insert_id;


}

echo json_encode($status);

$stmt->close();

?>