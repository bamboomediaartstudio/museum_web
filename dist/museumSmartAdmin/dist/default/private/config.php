<?php

define("PRIVATE_PATH",  "private/"); 

define("DB_PATH",  "private/connect/");

define("LIBS",  "private/libs/");

define("ADITIVO_LIBS", "private/libs/aditivo/");

define("OTHER_LIBS", "private/libs/others/");

define("MASTER_LOGOUT", "private/actions/users-management/do_user_logout.php");

define("MASTER_LOGIN", "../../../login.php");


return array(

	"general-urls"=>array(

		'favicons' => PRIVATE_PATH . 'includes/favicons.php',

		'DBConnect' => DB_PATH . 'connect.php',

		'master_logout' => MASTER_LOGOUT,

		'master_login' => MASTER_LOGIN

	),

	"url-includes" => array(

		'header' => PRIVATE_PATH . 'includes/header.php',	

		'footer' => PRIVATE_PATH . 'includes/footer.php',

		'sidebar' => PRIVATE_PATH . 'includes/sidebar.php',

		'keep-master-login' => PRIVATE_PATH . 'includes/master_login_data.php',

		'keep-admin-login' => PRIVATE_PATH . 'includes/admin_login_data.php'
	),

	"lib-lincludes" => array(

		'aditivo-libs' => ADITIVO_LIBS,

		'other-libs' => OTHER_LIBS,

		'libs' => LIBS

	)
    
);
?>
