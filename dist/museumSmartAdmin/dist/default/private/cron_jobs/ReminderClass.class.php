<?php

class ReminderClass{

  private $db;
  public $tomorrow;
  public $afterTomorrow;
  public $afterThreeDays;

  public function __construct(){

    $this->db = DB::getInstance();

  }


  private function queryExecution($dateToCheck){

    $db = $this->db;

    $qs = 'SELECT mvce.id as id_mvce, mvce.zoom_url, mvce.date_start, mvce.date_end, mvce.added, mvce.active, mvce.deleted,
  			 	mvc.id AS id_class, name AS class_name,
  			 	mvciir.id_institution,
  			 	mbi.institution_name, mbi.inscription_contact_name, mbi.inscription_contact_email
  			  FROM museum_virtual_classes_events AS mvce
  				INNER JOIN museum_virtual_classes_institutions_inscriptions_relations AS mvciir
  				ON mvce.id = mvciir.id_event
  				INNER JOIN museum_virtual_classes AS mvc
  				ON mvciir.id_class = mvc.id
  				INNER JOIN museum_booking_institutions AS mbi
  				ON mvciir.id_institution = mbi.id
  				WHERE DATE(mvce.date_start) = ?
  				AND mvce.active = ?
  				AND mvce.deleted = ?';


  	$q = $db->query($qs, [$dateToCheck, 1, 0]);
  	$results = $q->results();

  	return $results;

  }


  public function getTomorrowResults(){

    $dtTomorrow = new DateTime('tomorrow');
    $tomorrow = $dtTomorrow->format('Y-m-d');
    $this->tomorrow = $tomorrow;

    return $this->queryExecution($tomorrow);

  }


  public function getAfterTomorrowResults(){

    $dtAfterTomorrow = new DateTime('tomorrow + 1day');
    $afterTomorrow = $dtAfterTomorrow->format('Y-m-d');
    $this->afterTomorrow = $afterTomorrow;

    return $this->queryExecution($afterTomorrow);

  }

  public function getAfterThreeDaysResults(){

    $dtPassedThreeDays = new DateTime('tomorrow + 2day');
    $afterThreeDays = $dtPassedThreeDays->format('Y-m-d');
    $this->afterThreeDays = $afterThreeDays;

    return $this->queryExecution($afterThreeDays);

  }

  public function getCustomDateResults($dateToCheck){

    return $this->queryExecution($dateToCheck);

  }

  private function isWeekend($dateToCheck){

    return (date('N', strtotime($dateToCheck)) >= 6);

  }

  public function getCantIncompleteZoomUrl($arrData){

    $cantFound = 0;

    if(sizeof($arrData)>0){

      foreach($arrData as $result){

        if($result->zoom_url == NULL || $result->zoom_url == ''){

          $cantFound++;

        }

      }

    }


    return $cantFound;

  }

  //Pass date and cant of events without url zoom
  //General use outside class.
  //Si del array con los eventos de tomorrow, afterTomorrow o 72 hsdespues tenemos cantidad de urls incomepltas de zooom y (&&) no es finde, da true
  public function sendEmail($date, $cantIncompleteFound){

    return (!$this->isWeekend($date) && $cantIncompleteFound > 0) ? true : false;

  }

  //Insert log into db
  public function dbLog($arrData, $daysBefore=1){

    $db = DB::getInstance();

    $arrEmailDebug = array();

    $today = date('Y-m-d H:i:s');

    $isTodayWeekend = $this->isWeekend($today);


    if(count($arrData)>0){

      foreach($arrData as $result){

        $isZoomIncomplete = ($result->zoom_url == NULL || $result->zoom_url == '') ? 1 : 0;


        $arrEmailDebug = array(

      			'id_event'								=> $result->id_mvce,
      			'institution_name'				=> $result->institution_name,
      			'was_zoom_data_completed'	=> ($isZoomIncomplete==1) ? 0 : 1,
      			'email_sent_to'						=> ($this->sendEmail($today, $isZoomIncomplete)) ? 'Send email' : 'No send email',
      			'checked_days_before'			=> $daysBefore,
      			'class_start_date'				=> $result->date_start,
      			'last_modification'				=> $result->added,
      			'reminder_executed_date'	=> $today,
      			'isExecutedDateWeekend'	  => ($isTodayWeekend) ? 1 : 0,
      			'from'										=> basename(__FILE__, '.php'),
      			'active'									=> 1,
      			'deleted'									=> 0

      	);

        $db->insert('cron_emails_sent_log', $arrEmailDebug);

      }

    }else{

      $arrEmailDebug = array(

          'id_event'								=> 0,
          'institution_name'				=> 'no results found, nothing sent',
          'was_zoom_data_completed'	=> 0,
          'email_sent_to'						=> 'no results found, nothing sent',
          'checked_days_before'			=> $daysBefore,
          'class_start_date'				=> '0000-00-00',
          'last_modification'				=> '0000-00-00',
          'reminder_executed_date'	=> $today,
          'from'										=> basename(__FILE__, '.php'),
          'active'									=> 1,
          'deleted'									=> 0

      );

      $db->insert('cron_emails_sent_log', $arrEmailDebug);

    }

  }

  public function logDayReminder($mailingRun, $emailsSent, $emailsFailed, $bookingType){

    $db = DB::getInstance();

    $insertDate = date("Y-m-d H:i:s");

    $fields = array(

    	'mailing_run'=>$mailingRun,

    	'emails_sent'=>$emailsSent,

    	'emails_failed'=>$emailsFailed,

    	'run_datetime' => $insertDate,

    	'booking_type' => $bookingType,

    	'category' => 'virtual class'

    );

    $db->insert('cron_visit_previous_day_reminder', $fields);

  }



  /************************************************************************************/
  /************************************************************************************/
  /*                  // TESTS DEBUG                                                  */
  /************************************************************************************/
  /************************************************************************************/

  public function classDebug(){

    $tomorrowResults = $this->getTomorrowResults();
    $tomorrow = $this->tomorrow;
    $tomorrowIncompleteFound = $this->getCantIncompleteZoomUrl($tomorrowResults);
    $today = date('Y-m-d');

    echo '*** TOMORROW - ('.$tomorrow.') *** <br>';
    echo '*** Cant results: '.count($tomorrowResults).' *** <br>';
    echo ($this->isWeekend($today)) ? '*** Today is Weekend?: Yes *** <br>': '*** Today is Weekend?: No *** <br>';
    echo '*** Zoom incomplete found:  '.$tomorrowIncompleteFound.' *** <br>';
    echo ($this->sendEmail($today, $tomorrowIncompleteFound)) ? 'Send email' : 'No send email';


    echo '<pre>';
    print_r($tomorrowResults);
    echo '</pre>';

    echo '<br> *********************************************************************************** <br>';

    $afterTomorrowResults = $this->getAfterTomorrowResults();
    $afterTomorrow = $this->afterTomorrow;
    $afterTomorrowIncompleteFound = $this->getCantIncompleteZoomUrl($afterTomorrowResults);

    echo '<br>*** AFTER TOMORROW - ('.$afterTomorrow.') *** <br>';
    echo '*** Cant results: '.count($afterTomorrowResults).' *** <br>';
    echo ($this->isWeekend($today)) ? '*** Today is Weekend?: Yes *** <br>': '*** Today is Weekend?: No *** <br>';
    echo '*** Zoom incomplete found:  '.$afterTomorrowIncompleteFound.' *** <br>';
    echo ($this->sendEmail($today, $afterTomorrowIncompleteFound)) ? 'Send email' : 'No send email';


    echo '<pre>';
    print_r($afterTomorrowResults);
    echo '</pre>';



    echo '<br> *********************************************************************************** <br>';


    $afterThreeDaysResults = $this->getAfterThreeDaysResults();
    $afterThreeDays = $this->afterThreeDays;
    $threeDaysIncompleteFound = $this->getCantIncompleteZoomUrl($afterThreeDaysResults);

    echo '<br>*** AFTER THREE DAYS - ('.$afterThreeDays.') *** <br>';
    echo '*** Cant results: '.count($afterThreeDaysResults).' *** <br>';
    echo ($this->isWeekend($today)) ? '*** Today is Weekend?: Yes *** <br>': '*** Today is Weekend?: No *** <br>';
    echo '*** Zoom incomplete found:  '.$threeDaysIncompleteFound.' *** <br>';
    echo ($this->sendEmail($today, $threeDaysIncompleteFound)) ? 'Send email' : 'No send email';


    echo '<pre>';
    print_r($afterThreeDaysResults);
    echo '</pre>';

    echo '<br> *********************************************************************************** <br>';


    $customDate = '2020-11-02';
    $customDateResults = $this->getCustomDateResults($customDate);

    echo '<br>*** CUSTOM DATE: - ('.$customDate.') *** <br>';
    echo '*** Cant results: '.count($customDateResults).' *** <br>';
    echo ($this->isWeekend($customDate)) ? '*** Is this date Weekend?: Yes *** <br>': '*** Is this date Weekend?: No *** <br>';
    echo ($this->sendEmail($customDate, $customDateResults)) ? 'Send email' : 'No send email';

    echo '<pre>';
    print_r($customDateResults);
    echo '</pre>';

    echo '*** Zoom incomplete found:  '.$this->getCantIncompleteZoomUrl($customDateResults).' *** <br>';

    echo '<br> ********************************* END *********************************************<br>';
    echo '*********************************************************************************** <br>';


  }



}

?>
