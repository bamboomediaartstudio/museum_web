<?php

require_once '../users/core/init.php';

require_once('../framework/mailer/Emailer.php');

require_once('../framework/mailer/EmailTemplate.php');


setlocale(LC_TIME, 'es_ES', 'esp_esp');

$db = DB::getInstance();

$status['init'] = true;

$queryrString = 'SELECT *, mbi.id as main_id, mvc.name as class_name FROM museum_booking_institutions as mbi

INNER JOIN museum_virtual_classes_institutions_inscriptions_relations as mvciir

ON mbi.id = mvciir.id_institution

INNER JOIN museum_virtual_classes_events as mvce

ON mvciir.id_event = mvce.id

INNER JOIN museum_virtual_classes as mvc

ON mvce.sid = mvc.id

WHERE mvce.active = ? AND mvce.deleted = ? AND DATEDIFF(mvce.date_start, DATE_SUB(NOW(), INTERVAL 1 DAY)) = ?';

$results = $db->query($queryrString, [1, 0, 0]);

$emailsSent = 0;

$emailsFailed = 0;

$mailingRun = 0;

$bookingType = 0;

foreach($results->results() as $result){

	$mailingRun = 1;

	$dtStart = new DateTime($result->date_start);

	$d = $dtStart->format('m/d/Y');

	$date = strftime("%A %d de %B del %Y", strtotime($result->date_start));

	$dtEnd = new DateTime($result->date_end);

	$timeStart = $dtStart->format('H:i');

	$timeEnd = $dtEnd->format('H:i');

	$otherFormat = $dtStart->format('Y-m-d');


	$st =  'Hola <b>' . $result->inscription_contact_name . '</b>.<br><br>';

	//$st .= 'Muchas gracias por haber participado de la clase virtual <b>' . $result->class_name . '</b>. Para poder seguir mejorando esta propuesta educativa, nos gustaría conocer más sobre tu experiencia. ¿Podrías completar esta breve encuesta? Hacé click acá para ingresar: <a href="https://bit.ly/EncuestaClasesVirtuales2020">https://bit.ly/EncuestaClasesVirtuales2020</a><br><br>';
	$st .= 'Muchas gracias por haber participado de la clase virtual <b>' . $result->class_name . '</b>. Para poder seguir mejorando esta propuesta educativa, nos gustaría conocer más sobre tu experiencia. ¿Podrías completar esta breve encuesta? Hacé click acá para ingresar: <a href="'.$result->url_survey_institutions.'">'.$result->url_survey_institutions.'</a><br><br>';

	$st .= 'Gracias a tu devolución, podremos seguir mejorando nuestra labor educativa.<br><br>';

	$st .= 'Muchas gracias';

	$email = $result->inscription_contact_email;

	$emailer = new Emailer([$email], 'encuesta sobre clase virtual');

	$template = new EmailTemplate('../framework/mailer/templates/custom.php');

	$template->title = 'encuesta sobre clase virtual';

	$template->bodyContent = $st;

	$emailer->SetTemplate($template);

	$mailWasSent = $emailer->send();

	$emailsSent++;

}

$insertDate = date("Y-m-d H:i:s");

$fields = array(

	'mailing_run'=>$mailingRun,

	'emails_sent'=>$emailsSent,

	'emails_failed'=>$emailsFailed,

	'run_datetime' => $insertDate,

	'booking_type' => $bookingType,

	'category' => 'virtual class'

);

$db->insert('cron_next_day_survey', $fields);

?>
