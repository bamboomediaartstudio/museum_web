<?php

require_once '../users/core/init.php';

require_once('../framework/mailer/Emailer.php');

require_once('../framework/mailer/EmailTemplate.php');


setlocale(LC_TIME, 'es_ES', 'esp_esp');

$db = DB::getInstance();

$status['init'] = true;

$queryrString = 'SELECT mvcei.id AS inscription_id, mvcei.id_event AS inscription_id_event, mvcei.id_class, mvcei.name, mvcei.surname, mvcei.email, mvcei.added AS mvcei_added,

       									mvce.id AS mvce_id, mvce.date_start, mvce.date_end, mvce.added AS mvce_added, mvce.available_places, mvce.booked_places,

		 										mvce.booking_type,

												mvc.name AS class_name, mvc.url_survey_individuals

       					 FROM museum_virtual_classes_events_inscriptions AS mvcei

								 INNER JOIN museum_virtual_classes_events AS mvce

								 ON mvcei.id_event = mvce.id

								 INNER JOIN museum_virtual_classes as mvc

								 ON mvcei.id_class = mvc.id

								 WHERE mvce.active = ? AND mvce.deleted = ? AND DATEDIFF(mvce.date_start, DATE_SUB(NOW(), INTERVAL 1 DAY)) = ?';

$results = $db->query($queryrString, [1, 0, 0]);

$emailsSent = 0;

$emailsFailed = 0;

$mailingRun = 0;

$bookingType = 0;

foreach($results->results() as $result){

	$mailingRun = 1;

	$dtStart = new DateTime($result->date_start);

	$d = $dtStart->format('m/d/Y');

	$date = strftime("%A %d de %B del %Y", strtotime($result->date_start));

	$dtEnd = new DateTime($result->date_end);

	$timeStart = $dtStart->format('H:i');

	$timeEnd = $dtEnd->format('H:i');

	$otherFormat = $dtStart->format('Y-m-d');


	$st =  'Hola <b>' . $result->name . '</b>.<br><br>';

	//$st .= 'Muchas gracias por haber participado de la clase virtual <b>' . $result->class_name . '</b>. Para poder seguir mejorando esta propuesta educativa, nos gustaría conocer más sobre tu experiencia. ¿Podrías completar esta breve encuesta? Hacé click acá para ingresar: <a href="https://bit.ly/EncuestaClasesVirtuales2020">https://bit.ly/EncuestaClasesVirtuales2020</a><br><br>';
	$st .= 'Muchas gracias por haber participado de la clase virtual <b>' . $result->class_name . '</b>. Para poder seguir mejorando esta propuesta educativa, nos gustaría conocer más sobre tu experiencia. ¿Podrías completar esta breve encuesta? Hacé click acá para ingresar: <a href="'.$result->url_survey_individuals.'">'.$result->url_survey_individuals.'</a><br><br>';

	$st .= 'Gracias a tu devolución, podremos seguir mejorando nuestra labor educativa.<br><br>';

	$st .= 'Muchas gracias';


	$email = $result->email;

	$emailer = new Emailer([$email], 'Encuesta sobre clase virtual');
	//$emailer = new Emailer('bugio89@gmail.com', 'encuesta sobre clase virtual');

	$template = new EmailTemplate('../framework/mailer/templates/custom.php');

	$template->title = 'Encuesta sobre clase virtual';

	$template->bodyContent = $st;

	$emailer->SetTemplate($template);

	$mailWasSent = $emailer->send();

  $emailsSent++;

}


$insertDate = date("Y-m-d H:i:s");

$fields = array(

	'mailing_run'=>$mailingRun,

	'emails_sent'=>$emailsSent,

	'emails_failed'=>$emailsFailed,

	'run_datetime' => $insertDate,

	'booking_type' => $bookingType,

	'category' => 'virtual class for individuals'

);

$db->insert('cron_next_day_survey', $fields);

?>
