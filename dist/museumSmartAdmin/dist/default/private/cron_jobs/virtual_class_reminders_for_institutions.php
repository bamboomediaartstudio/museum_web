<?php

//Solo funciona este reminder. Graba en db. Simula los 3 dias. Ver db para saber que grabo. Los otros reminders estan comentados.
/********************* test reminders ************************************* */

require_once '../users/core/init.php';

require_once('../framework/mailer/Emailer.php');

require_once('../framework/mailer/EmailTemplate.php');

require_once('ReminderClass.class.php');

setlocale(LC_TIME, 'es_ES', 'esp_esp');

$status['init'] = true;


$reminderObj = new ReminderClass();
$today = date('Y-m-d');


/************************************** Tomorrow (24hs) *********************************************/

$tomorrowData = $reminderObj->getTomorrowResults();
$cantZoomIncompleteFoundTomorrow = $reminderObj->getCantIncompleteZoomUrl($tomorrowData);
//Check if we have zoom_urls incomplte and if we are not in weekend
if($reminderObj->sendEmail($today, $cantZoomIncompleteFoundTomorrow)){

  $title = '[24hs] - opss! a una institución le falta el link de zoom';

  sendEmailReminderToAdmin($tomorrowData, $title);

}else{

  $reminderObj->logDayReminder(0, 0, 0, 1);

}

$reminderObj->dbLog($tomorrowData, 1);



/************************************** After Tomorrow (48hs) *********************************************/


//Check and send email after before 48hs (event after tomorrow)

$afterTomorrowData = $reminderObj->getAfterTomorrowResults();

$cantZoomIncompleteFoundAfterTomorrow = $reminderObj->getCantIncompleteZoomUrl($afterTomorrowData);

//Check if we have zoom_urls incomplte and if we are not in weekend

if($reminderObj->sendEmail($today, $cantZoomIncompleteFoundAfterTomorrow)){

  $title = '[48hs] - opss! a una institución le falta el link de zoom';

  sendEmailReminderToAdmin($afterTomorrowData, $title);

}else{

  $reminderObj->logDayReminder(0, 0, 0, 1);

}

//Save db log

$reminderObj->dbLog($afterTomorrowData, 2);


/************************************** After 72hs Three days *********************************************/


$afterThreeDaysData = $reminderObj->getAfterThreeDaysResults();

$cantZoomIncompleteFoundAfterThreeDays = $reminderObj->getCantIncompleteZoomUrl($afterThreeDaysData);

//Check if we have zoom_urls incomplte and if we are not in weekend

if($reminderObj->sendEmail($today, $cantZoomIncompleteFoundAfterThreeDays)){

  $title = '72hs reminder: falta un zoom - Institución.';

  sendEmailReminderToAdmin($afterThreeDaysData, $title);

}else{

  $reminderObj->logDayReminder(0, 0, 0, 1);

}

$reminderObj->dbLog($afterThreeDaysData, 3);




function sendEmailReminderToAdmin($arrEventData=array(), $emailTitle){

//  echo ' en funcion send email reminder<br>';

  $emailsSent = 0;

  $emailsFailed = 0;

  $mailingRun = 0;

  $bookingType = 0;

  foreach($arrEventData as $result){

    if($result->zoom_url == NULL || $result->zoom_url == ""){

      $mailingRun = 1;

      $dtStart = new DateTime($result->date_start);

      $d = $dtStart->format('m/d/Y');

      $date = strftime("%A %d de %B del %Y", strtotime($result->date_start));

      $dtEnd = new DateTime($result->date_end);

      $timeStart = $dtStart->format('H:i');

      $timeEnd = $dtEnd->format('H:i');

      $otherFormat = $dtStart->format('Y-m-d');


      $st =  'Hola! La institución <b>' . $result->institution_name . '</b> tendrá la clase virtual el ' . $d . ' a las ' . $timeStart . ' y aún no se definió la URL para Zoom.<br><br>';

      $st .= 'Accedé a https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/museum_virtual_classes_dates_list.php?date=' . $otherFormat . ' y asigná los datos faltantes.<br><br>';

      $st .= 'Gracias! <br>';

      $email = 'visitasguiadas@museodelholocausto.org.ar';
      //$email = 'info@museodelholocausto.org.ar';
      //$email = 'bugio89@gmail.com';

      $email2 = 'gabriel.merlo@museodelholocausto.org.ar';
      //$email2 = 'colo@aditivointeractivegroup.com';


      $emailer = new Emailer([$email, $email2], $emailTitle);

      $template = new EmailTemplate('../framework/mailer/templates/custom.php');

      $template->title = $emailTitle;

      $template->bodyContent = $st;

      $emailer->SetTemplate($template);

      $mailWasSent = $emailer->send();

      //echo $st;
      //echo 'enviar mail a administraticvo, falta zoom url <br>';

    }else{

      //echo 'enviar mail a persona, ya estan los datos de zoom<br>';

      $mailingRun = 1;

			$dtStart = new DateTime($result->date_start);

			$d = $dtStart->format('m/d/Y');

			$date = strftime("%A %d de %B del %Y", strtotime($result->date_start));

			$dtEnd = new DateTime($result->date_end);

			$timeStart = $dtStart->format('H:i');

			$timeEnd = $dtEnd->format('H:i');



			$st =  'Hola <b>' . $result->inscription_contact_name . '</b>.<br><br>';

			$st .= 'Te recordamos que el próximo ' . $date . ' desde las  ' . $timeStart . ' tendremos la clase virtual <b>' . $result->class_name . '</b><br><br>';

			$st .= 'Estos son los datos del zoom:<br><br>';

			$st .= '<b>URL:</b> ' . $result->zoom_url . ' <br>';

			$st .= '<b>ID:</b> ' . $result->zoom_id . ' <br>';

			$st .= '<b>password:</b> ' . openssl_decrypt($result->zoom_password_encrypt, "AES-128-ECB", 'Muse@2021!') . ' <br><br>';

			$st .= 'Cualquier duda, escribinos.<br><br>';

			$st .= 'Gracias';

			$emailer = new Emailer([$result->inscription_contact_email], 'Recordatorio clase virtual');
			//$emailer = new Emailer(['bugio89@gmail.com'], 'Recordatorio clase virtual');

			$template = new EmailTemplate('../framework/mailer/templates/custom.php');

			$template->title = 'Recordatorio de clase virtual';

			$template->bodyContent = $st;

			$emailer->SetTemplate($template);

			$mailWasSent = $emailer->send();



    }

    $emailsSent++;

  }

  $reminderObj = new ReminderClass();
  $reminderObj->logDayReminder($mailingRun, $emailsSent, $emailsFailed, $bookingType);


}


/********************* / test reminders *********************************** */





?>
