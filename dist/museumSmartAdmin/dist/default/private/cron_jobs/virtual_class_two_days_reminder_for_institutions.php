<?php

require_once '../users/core/init.php';

require_once('../framework/mailer/Emailer.php');

require_once('../framework/mailer/EmailTemplate.php');


setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

$db = DB::getInstance();

$status['init'] = true;

$queryrString = 'SELECT *, mbi.id as main_id, mvc.name as class_name FROM museum_booking_institutions as mbi 

INNER JOIN museum_virtual_classes_institutions_inscriptions_relations as mvciir

ON mbi.id = mvciir.id_institution

INNER JOIN museum_virtual_classes_events as mvce

ON mvciir.id_event = mvce.id

INNER JOIN museum_virtual_classes as mvc

ON mvce.sid = mvc.id

WHERE mvce.active = ? AND mvce.deleted = ? AND DATEDIFF(mvce.date_start, DATE_ADD(NOW(), INTERVAL 2 DAY)) = ?';

$results = $db->query($queryrString, [1, 0, 0]);

$emailsSent = 0;

$emailsFailed = 0;

$mailingRun = 0;

$bookingType = 0;

foreach($results->results() as $result){

	if($result->zoom_url == null){

		$mailingRun = 1;

		$dtStart = new DateTime($result->date_start);

		$d = $dtStart->format('m/d/Y');

		$date = strftime("%A %d de %B del %Y", strtotime($result->date_start));

		$dtEnd = new DateTime($result->date_end);

		$timeStart = $dtStart->format('H:i');
		
		$timeEnd = $dtEnd->format('H:i');

		$otherFormat = $dtStart->format('Y-m-d');

		$st =  'Hola! La institución <b>' . $result->institution_name . '</b> tendrá la clase virtual el ' . $date . ' a las ' . $timeStart . ' y aún no se definió la URL para Zoom.<br><br>';

		$st .= 'Accedé a https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/museum_virtual_classes_dates_list.php?date=' . $otherFormat . ' y asigná los datos faltantes.<br><br>';

		$st .= 'Gracias!';

		$email = 'visitasguiadas@museodelholocausto.org.ar';

		$email2 = 'gabriel.merlo@museodelholocausto.org.ar';

		$emailer = new Emailer([$email, $email2], 'opss! a una institución le falta el link de zoom');

		$template = new EmailTemplate('../framework/mailer/templates/custom.php');

		$template->title = 'opss! a una institución le falta el link de zoom';

		$template->bodyContent = $st;

		$emailer->SetTemplate($template);

		$mailWasSent = $emailer->send();
		
	}else{


		$mailingRun = 1;

		$dtStart = new DateTime($result->date_start);

		$d = $dtStart->format('m/d/Y');

		$date = strftime("%A %d de %B del %Y", strtotime($result->date_start));

		$dtEnd = new DateTime($result->date_end);

		$timeStart = $dtStart->format('H:i');
		
		$timeEnd = $dtEnd->format('H:i');

		$st =  'Hola <b>' . $result->inscription_contact_name . '</b>.<br><br>';

		$st .= 'Te recordamos que el próximo ' . $date . ' desde las  ' . $timeStart . ' tendremos la clase virtual <b>' . $result->class_name . '</b><br><br>';

		$st .= 'Te enviamos el link para que se lo hagas llegar a tus alumnos: <a href="' . $result->zoom_url . '">' . $result->zoom_url . '</a><br><br>';

		$st .= 'Si no podés abrir el link, copiá y compartí la siguiente URL: ' . $result->zoom_url . '<br><br>';

		$st .= 'Cualquier duda, escribinos.<br><br>';

		$st .= 'Gracias';

		$emailer = new Emailer([$result->inscription_contact_email], 'Recordatorio clase virtual');

		$template = new EmailTemplate('../framework/mailer/templates/custom.php');

		$template->title = 'Recordatorio de clase virtual';

		$template->bodyContent = $st;

		$emailer->SetTemplate($template);

		$mailWasSent = $emailer->send();

	}

}

$insertDate = date("Y-m-d H:i:s");

$fields = array(

	'mailing_run'=>$mailingRun, 

	'emails_sent'=>$emailsSent, 

	'emails_failed'=>$emailsFailed,

	'run_datetime' => $insertDate,

	'booking_type' => $bookingType,

	'category' => 'virtual class'

);

$db->insert('cron_visit_previous_day_reminder', $fields);

?>