<?php

require_once '../users/core/init.php';

require_once('../framework/mailer/Emailer.php');

require_once('../framework/mailer/EmailTemplate.php');

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

$db = DB::getInstance();

$status['init'] = true;

$query = 'SELECT *,mvc.name AS class_name, users.name AS guide_name, mvce.id as main_id FROM museum_virtual_classes_events AS `mvce` 

  INNER JOIN `museum_virtual_classes` AS `mvc` ON (`mvce`.`sid` = `mvc`.`id`) 

  LEFT JOIN `museum_virtual_classes_events_guides_relations` as `guides` ON  (`mvce`.`id` = `guides`.`id_event`)

  LEFT JOIN `users` as `users` ON (`users`.`id` = `guides`.`id_guide`)

  WHERE `mvce`.active = ? AND `mvce`.deleted = ? AND DATEDIFF(`mvce`.date_start, DATE_ADD(NOW(), INTERVAL 1 DAY)) = ? AND `mvce`.booking_type = ?';

  $results = $db->query($query, [1, 0, 0, 2]);

  $emailsSent = 0;

  $emailsFailed = 0;

  $mailingRun = 0;

  $bookingType = 0;

  foreach($results->results() as $result){

  	$dtStart = new DateTime($result->date_start);

	$d = $dtStart->format('m/d/Y');

	$date = strftime("%A %d de %B del %Y", strtotime($result->date_start));

	$dtEnd = new DateTime($result->date_end);

	$timeStart = $dtStart->format('H:i');

	$timeEnd = $dtEnd->format('H:i');

	//...

	$individualsQuery = 'SELECT * FROM museum_virtual_classes_events_inscriptions WHERE active = ? AND deleted = ? AND id_event = ?';

	$individualsResults = $db->query($individualsQuery, [1, 0, $result->main_id]);

	$emails = array();

	foreach($individualsResults->results() as $individualsResult){ array_push($emails, $individualsResult->email); }

	//echo var_dump($emails) . '<br><br><br>';

	echo '<pre>' , var_dump($emails) , '</pre>';

	$emailer = new Emailer($emails, 'testing');

	$template = new EmailTemplate('../framework/mailer/templates/custom.php');

	$template->title = 'testing';

	$st = '¡Hola!<br><br>Este email es un recordatorio de la la clase virtual <b>' . $result->class_name . '</b> del próximo ' . $date . ' desde las ' . $timeStart  . ' <br>';
	
	$st .= 'Podés ver los datos de acceso al Zoom desde la siguiente URL:<br><br>';

	$st .= 'https://museodelholocausto.org.ar/proximoEvento/index.php?id=' . $result->main_id . '<br><br>';

	$st .= 'Si los datos aún no fueron publicados, tené a bien volver a revisar la URL a lo largo del día';

	$template->bodyContent = $st;
 
	$emailer->SetTemplate($template);

	//$mailWasSent = $emailer->send();

	if($result->zoom_url != null){
		
	}else{

	}

}

/*$insertDate = date("Y-m-d H:i:s");

$fields = array(

	'mailing_run'=>$mailingRun, 

	'emails_sent'=>$emailsSent, 

	'emails_failed'=>$emailsFailed,

	'run_datetime' => $insertDate,

	'booking_type' => $bookingType,

	'category' => 'virtual class'

);*/

//$db->insert('cron_visit_previous_day_reminder', $fields);

?>