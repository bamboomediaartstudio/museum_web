<?php

require_once '../users/core/init.php';

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 


$db = DB::getInstance();

$status['init'] = true;

$settingsQ = $db->query("SELECT * FROM settings");

$settings = $GLOBALS['settings'] = $settingsQ->first();

$supportSettingsQ = $db->query("SELECT * FROM support_settings");

$GLOBALS['supportSettings'] = $supportSettingsQ->first();

$museumSettingsQ = $db->query("SELECT * FROM museum_data");

$museumSettings = $museumSettingsQ->first();

//social media...

$museumSocialMediaQ = $db->query("SELECT * FROM museum_sm WHERE active = ? AND deleted = ? ORDER BY internal_order", [1, 0]);

$smArray = array();

foreach($museumSocialMediaQ->results() as $result){ $smArray[$result->name] = $result->url; }

$year = date("Y");

$address = $museumSettings->address . ' ' . $museumSettings->address_number . ', ' . $museumSettings->zip_code . ' ' . $museumSettings->city;

//for museum

$queryrString = 'SELECT *, mb.sid as save_sid FROM museum_booking_assistants as mba LEFT JOIN museum_booking as mb ON mba.booking_id = mb.id WHERE DATEDIFF(mb.date_start, DATE_ADD(CURDATE(), INTERVAL 1 DAY)) = ? AND mb.active = ? AND mb.deleted = ?';

$results = $db->query($queryrString, [0, 1, 0]);

$emailsSent = 0;

$emailsFailed = 0;

$mailingRun = 0;

foreach($results->results() as $result){

	$mailingRun = 1;

	$bookingType = $result->save_sid;

	//date start...

	$dtStart = new DateTime($result->date_start);

	$d = $dtStart->format('m/d/Y');

	$date = strftime("%A %d de %B del %Y", strtotime($result->date_start));
	
	$timeStart = $dtStart->format('H:i');

	//date end...

	$dtEnd = new DateTime($result->date_end);
	
	$timeEnd = $dtEnd->format('H:i');

	$place = $result->place;

	//cron_template

	$msg = file_get_contents('../email_templates/visit_previous_day_reminder_template.html');	

	$msg = str_replace('%projectTitle%', $museumSettings->title, $msg);

	$msg = str_replace('%name%', utf8_encode($result->name), $msg);

	$msg = str_replace('%surname%', utf8_encode($result->surname), $msg);

	$msg = str_replace('%date%', utf8_encode($date), $msg);

	$msg = str_replace('%timeStart%', $timeStart, $msg);

	$msg = str_replace('%timeEnd%', $timeEnd, $msg);

	$msg = str_replace('%place%', $place, $msg);

	$msg = str_replace('%ip%', Helpers::getIP(), $msg);

	$msg = str_replace('%os%', Helpers::getOS(), $msg);

	$msg = str_replace('%browser%', Helpers::getBrowser(), $msg);

	$msg = str_replace('%email%', $GLOBALS['supportSettings']->support_email, $msg);

	$msg = str_replace('%address%', $address, $msg);

	$msg = str_replace('%te%', $museumSettings->te, $msg);

	$msg = str_replace('%museumEmail%', $museumSettings->email, $msg);

	$msg = str_replace('%lat%', $museumSettings->lat, $msg);

	$msg = str_replace('%lon%', $museumSettings->lon, $msg);

	$msg = str_replace('%facebook%', $smArray['Facebook'], $msg);

	$msg = str_replace('%instagram%', $smArray['Instagram'], $msg);

	$msg = str_replace('%twitter%', $smArray['Twitter'], $msg);

	$msg = str_replace('%youtube%', $smArray['Youtube'], $msg);

	$msg = str_replace('%year%', $year, $msg);

	$sent = Helpers::email($result->email, utf8_decode('Recordatorio de visita'), $msg);

	if($sent){

		$status['envio'] = 'sent OK';
		
		$emailsSent = $emailsSent + 1;

	}else{

		$emailsFailed = $emailsFailed + 1;

		$status['envio'] = 'ops!';

	}

}

$insertDate = date("Y-m-d H:i:s");

$fields = array(

	'mailing_run'=>$mailingRun, 

	'emails_sent'=>$emailsSent, 

	'emails_failed'=>$emailsFailed,

	'run_datetime' => $insertDate,

	'booking_type' => $bookingType

);

$db->insert('cron_visit_previous_day_reminder', $fields);

?>