<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/


/**
* Config();
* @description 									-						load config...
*/

class Config {

	/**
	* get($path = null);
	* @description 								-				 		get...
	*/

	public static function get($path = null){

		if($path){

			$config = $GLOBALS['config'];

			$path = explode('/', $path);

			foreach($path as $bit){ if(isset($config[$bit])){ $config = $config[$bit]; } }

			return $config;
		}

		return false;
	}
}

?>
