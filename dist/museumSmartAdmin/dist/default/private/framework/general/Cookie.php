<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/


/**
* Cookie();
* @description 									-						manage all the cookies...
*/

class Cookie{

	/**
	*  exists();
	* @description 									-						check if a cookie exists.
	* @param $name									-						the name of the cookie.
	* @return $boolean							-						true / false...
	*/

	public static function exists($name){ return (isset($_COOKIE[$name])) ? true : false; }

	/**
	*  get();
	* @description 									-						get a cookie
	* @param $name									-						the name of the cookie.
	* @return $_COOKIE							-						the cookie...
	*/

	public static function get($name){ return $_COOKIE[$name]; }

	/**
	*  set();
	* @description 									-						create a new cookie...
	* @param $name									-						the name of the cookie.
	* @param $value									-						the content of the cookie.
	* @return $expiry								-						the expiration date.
	*/

	public static function put($name, $value, $expiry){ return (setcookie($name, $value, time() + $expiry, '/')) ? true : false; }

	/**
	*  delete();
	* @description 									-						delete a cookie...
	* @param $name									-						the name of the cookie.
	*/

	public static function delete($name){ self::put($name, '', time()-1); }
}
