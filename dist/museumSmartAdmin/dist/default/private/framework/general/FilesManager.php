<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

/**
* FilesManager
* @description									-						check images before confirm upload...
*/

class FilesManager{

	private $_mtype;

	private $_fileSize;

	/**
	* __construct
	* @description									-					 constructor...
	*/

	public function __construct(){ }

	/**
	* checkDirectory();
	* @description									-					 check if a directory exists
	* @param $directory 						-					 the name of the directory...
	*/

	public function checkDirectory($directory){ return (!file_exists($directory)) ? false : true; }

	/**
	* makeDirectory();
	* @description									-					 create a new directory...
	* @param $directory 						-					 the name of the directory...
	* @param $mode 									-					 set 0777 as default...
	* @param $recursive 						-					 recursive to true...
	*/

	public function makeDirectory($directory, $mode = 0777, $recursive = true){ return mkdir($directory, $mode, $recursive); }

	/**
	* createHtaccess();
	* @description									-					 create a htaccess for the folder...
	* @param $folder 								-					 the name of the directory...
	*/

	public function createHtaccess($folder){

		if (!file_exists($folder."/.htaccess")){

			try {

				$file = fopen($folder."/.htaccess","w");

				$txt = "order deny,allow\n";

				$txt .= "deny from all\n";

				$txt .="allow from 127.0.0.1";

				fwrite($file, $txt);

				fclose($file);

				return true;

			} catch (Exception $e) { return false; }

		} else { return true; }

	}

	/**
	* checkPHPExtensions();
	* @description									-					 check the PHP extensions?...
	*/

	public function checkPHPExtensions(){

		if (!extension_loaded('fileinfo')) {

			if(function_exists('dl')){

				if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {

					if (!dl('fileinfo.dll')) { return false; } else { return true; }

				} else {

					if (!dl('fileinfo.so')) { return false; } else { return true; }

				}

			} else { return false; }

		} else { return true; }

	}

	/**
	* checkImageMimeType();
	* @description									-					 check the image mimetype
	*/


	public function checkImageMimeType($tmpname){

		$finfo = finfo_open(FILEINFO_MIME_TYPE);

		$mtype = finfo_file($finfo, $tmpname);

		$this->_mtype = $mtype;

		if(strpos($mtype, 'image/') === 0){ return true; } else { return false; }

		finfo_close($finfo);
	}

	/**
	* getMimeType();
	* @description									-					 get the image mimetype
	*/

	public function getMimeType(){ return $this->_mtype; }

	/**
	* getFileExtensionByMimeType();
	* @description									-					 get the image mimetype
	* @param $mimeType 							-					 the mimetype...
	* @return $string 							-					 the extension acorting to the mimetype...
	*/

	public function getFileExtensionByMimeType($mimeType){

		 $extensions = array('image/jpeg' => 'jpeg',

		 	'text/xml' => 'xml',

		 	'image/png' => 'png',

		 	'image/gif' => 'gif'
		 	 );

		 return $extensions[$mimeType];
	}

	/**
	* checkImageDimensions();
	* @description									-					 check the image width and height...
	* @param $file    							-					 the file to check
	* @param $width    							-					 the width of the image...
	* @param $height    						-					 the height of the image...
	* @param $strict 				   			-					 if is strict, the dimensions must match. Else we can check for greatter or equal to: >=
	* @return $boolean 							-					 the condition...
	*/

	public function checkImageDimensions($file, $imgWidth, $imgHeight, $strict){

		//return true;

		list($width, $height) = getimagesize($file);

		$status;

		if($strict == true){

			if($width == $imgWidth && $height == $imgHeight){

				$status = true;

			}else{

				$status = false;
			}

		} else {

			if($width >= $imgWidth && $height >= $imgHeight){

				$status = true;

			}else{

				$status = false;

		}

	}

	return $status;

}

	/**
	* checkFileSize();
	* @description									-					 check the file of the size...
	* @param $file    							-					 the file
	* @param $size    							-					 the size
	* @return $boolean 							-					 the condition...
	*/

	public function checkFileSize($file, $size){

		$this->_fileSize = filesize($file);

		if($this->_fileSize > $size){ return false; }else{ return true; }

	}

	/**
	* getFileSize();
	* @description									-					 get the size of the file...
	* @return $int    							-					 the size...
	*/

	public function getFileSize(){

		return $this->_fileSize;
	}
}
