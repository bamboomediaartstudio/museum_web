<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/


/**
*  Hash();
* @description 									-						Manage hashses...
*/

class Hash{

	/**
	*  make();
	* @description 									-						create a new hash...
	* @param $string 								-						the string
	* @param $salt 									-						the salt
	* @return $hash									-						the hash
	*/

	public static function make($string, $salt = ''){ return hash('sha256', $string . $salt); }

	/**
	*  salt();
	* @description 									-						create a salt
	* @param $length 								-						the length
	* @return $number								-						the salt
	*/

public static function salt($length){ return random_bytes($length); }

	/**
	*  unique();
	* @description 									-						create an unique
	* @return $number								-						unique...
	*/

	public static function unique(){ return self::make(uniqid()); }

}
