<?php

/**
* @summary All the helpers for the PHP side.
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}
*
* @todo Complete documentation.
*/


/**
* @class Helpers
* @description all the helpers that we ll need.
*/

class Helpers{

	/**
	* @method getRandomString
	* @description generate a random considering the param lenght
	*
	* @param {int} $len             		- The length of the random string.
	* @return {string} $string             	- The generated string.
	*/

	public static function getRandomString($len){

		$len = $len++;

		$string = "";

		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for($i=0;$i<$len;$i++)

			$string.=substr($chars,rand(0,strlen($chars)),1);

		return $string;
	}

	/**
	* @method getIP();
	* @description get the user IP
	*
	* @return {string} $ip             		- String with the IP address of the user.
	*/

	public static function getIP(){

		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {

			$ip = $_SERVER['HTTP_CLIENT_IP'];

		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {

			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

		} else {

			$ip = $_SERVER['REMOTE_ADDR'];

		}

		return $ip;

	}

	/**
	* @method getOS();
	* @description get the user OS
	*
	* @return {string} $os_platform         - String with the OS.
	*/

	public static function getOS() {

		$user_agent = $_SERVER['HTTP_USER_AGENT'];

		$os_platform  = "Unknown OS Platform";

		$os_array     = array(
			'/windows nt 10/i'      =>  'Windows 10',
			'/windows nt 6.3/i'     =>  'Windows 8.1',
			'/windows nt 6.2/i'     =>  'Windows 8',
			'/windows nt 6.1/i'     =>  'Windows 7',
			'/windows nt 6.0/i'     =>  'Windows Vista',
			'/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
			'/windows nt 5.1/i'     =>  'Windows XP',
			'/windows xp/i'         =>  'Windows XP',
			'/windows nt 5.0/i'     =>  'Windows 2000',
			'/windows me/i'         =>  'Windows ME',
			'/win98/i'              =>  'Windows 98',
			'/win95/i'              =>  'Windows 95',
			'/win16/i'              =>  'Windows 3.11',
			'/macintosh|mac os x/i' =>  'Mac OS X',
			'/mac_powerpc/i'        =>  'Mac OS 9',
			'/linux/i'              =>  'Linux',
			'/ubuntu/i'             =>  'Ubuntu',
			'/iphone/i'             =>  'iPhone',
			'/ipod/i'               =>  'iPod',
			'/ipad/i'               =>  'iPad',
			'/android/i'            =>  'Android',
			'/blackberry/i'         =>  'BlackBerry',
			'/webos/i'              =>  'Mobile'
		);

		foreach ($os_array as $regex => $value){

			if (preg_match($regex, $user_agent)){ $os_platform = $value; }

		}

		return $os_platform;
	}

	/**
	* @method isLocalhost();
	* @description Simple check to know if we are in localhost.
	*
	* @return {bool} true/false             - Boolean to indicate if we are in localhost.
	*/

	public static function isLocalhost() {

		if($_SERVER["REMOTE_ADDR"]=="127.0.0.1" || $_SERVER["REMOTE_ADDR"]=="::1"  || $_SERVER["REMOTE_ADDR"]=="localhost"){

			return true;

		}else{

			return false;

		}

	}

	/**
	* @method getBrowser();
	* @description get the user Browser.
	*
	* @return {string} $browser             - String with the browser name.
	*/

	public static function getBrowser() {

		$user_agent = $_SERVER['HTTP_USER_AGENT'];

		$browser = "Unknown Browser";

		$browser_array = array(

			'/msie/i'      => 'Internet Explorer',

			'/firefox/i'   => 'Firefox',

			'/safari/i'    => 'Safari',

			'/chrome/i'    => 'Chrome',

			'/edge/i'      => 'Edge',

			'/opera/i'     => 'Opera',

			'/netscape/i'  => 'Netscape',

			'/maxthon/i'   => 'Maxthon',

			'/konqueror/i' => 'Konqueror',

			'/mobile/i'    => 'Handheld Browser'

		);

		foreach ($browser_array as $regex => $value){

			if (preg_match($regex, $user_agent)){ $browser = $value; }

		}

		return $browser;

	}

	/**
	* @method getDataByIp
	* @description check the geoplugin api to get data by ip
	*
	* @param {String} $ip 							- 							the IP...if null it will get it through remote addr.
	*
	* @param {string} $purpose        	- 							what we need?
	*
	* @param {boolean} deep_detect     	- 							deep...
	*
	* @return {object} $output					-								the data...
	*/

	public static function getDataByIp($ip = NULL, $purpose = "location", $deep_detect = TRUE) {

		$output = NULL;

		if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {

				$ip = $_SERVER["REMOTE_ADDR"];

				if ($deep_detect) {

						if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))

							  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

					  if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))

						    $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }

	  $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));

	  $support    = array("country", "countrycode", "state", "region", "city", "location", "address", "latitude", "longitude", "timezone");

		$continents = array( "AF" => "Africa", "AN" => "Antarctica", "AS" => "Asia", "EU" => "Europe", "OC" => "Australia (Oceania)", "NA" => "North America", "SA" => "South America");

	  if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {

				$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));

				if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {

						switch ($purpose) {

							  case "location":

										$output = array(

												"city"           => @$ipdat->geoplugin_city,

											  "state"          => @$ipdat->geoplugin_regionName,

											  "country"        => @$ipdat->geoplugin_countryName,

												"country_code"   => @$ipdat->geoplugin_countryCode,

											  "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],

												"continent_code" => @$ipdat->geoplugin_continentCode,

                    );

									  break;

							  case "address":

									  $address = array($ipdat->geoplugin_countryName);

										if (@strlen($ipdat->geoplugin_regionName) >= 1)

										    $address[] = $ipdat->geoplugin_regionName;

									  if (@strlen($ipdat->geoplugin_city) >= 1)

									      $address[] = $ipdat->geoplugin_city;

									  $output = implode(", ", array_reverse($address));

									  break;

							  case "city":

									  $output = @$ipdat->geoplugin_city;

							      break;

							  case "state":

								    $output = @$ipdat->geoplugin_regionName;

								    break;

							  case "region":

									  $output = @$ipdat->geoplugin_regionName;

								    break;

							  case "country":

									  $output = @$ipdat->geoplugin_countryName;

								    break;

							  case "countrycode":

										$output = @$ipdat->geoplugin_countryCode;

										break;

									case "latitude":

										$output = @$ipdat->geoplugin_latitude;

										break;

									case "longitude":

										$output = @$ipdat->geoplugin_longitude;

										break;

										case "timezone":

											$output = @$ipdat->geoplugin_timezone;

											break;

										}

									}

								}

								return $output;
							}





	public static function timeElapsedString($datetime, $full = false) {

		$now = new DateTime;

		$ago = new DateTime($datetime);

		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);

		$diff->d -= $diff->w * 7;

		$string = array(

			'y' => 'year',

			'm' => 'month',

			'w' => 'week',

			'd' => 'day',

			'h' => 'hour',

			'i' => 'minute',

			's' => 'second'

		);

		foreach ($string as $k => &$v) {

			if ($diff->$k) {

				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');

			} else {

				unset($string[$k]);

			}

		}

		if (!$full) $string = array_slice($string, 0, 1);

		return $string ? implode(', ', $string) . ' ago' : 'just now';
	}

}

?>
