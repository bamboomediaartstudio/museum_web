<?php

/**
* Input();
* @description 									-						manage all the inputs...
*/

class Input{

	/**
	* exists();
	* @description 								-					check if an input exists...
	* @param $type							  -					post o get...
	* @return $boolean					  -					true / false...
	*/

	public static function exists($type = 'post'){

		switch ($type) {

			case 'post':

			return (!empty(	$_POST)) ? true : false;

				break;

			case 'get':

				return (!empty(	$_GET)) ? true : false;

				break;

			default:

				return false;

				break;
		}
	}

	/**
	* get();
	* @description 								-					obtain a get or a post variable...
	* @param $item							  -					the variable name...
	* @return $string					  	-					the variable value...
	*/

	public static function get($item){

		if(isset($_POST[$item])){

			return $_POST[$item];

		}else if(isset($_GET[$item])){

			return $_GET[$item];
		}

		return '';

	}


}
