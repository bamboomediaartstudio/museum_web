<?php 

class MimeTypes{

	public static function getExtensionByMimeType($mimeType){

		 $extensions = array(

		 	'image/jpeg' => 'jpeg',

		 	'text/xml' => 'xml',

		 	'image/png' => 'png'

		 );

    return $extensions[$mimeType];

	
	}

}

?>