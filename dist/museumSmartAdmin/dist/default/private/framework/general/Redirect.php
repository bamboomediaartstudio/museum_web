<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

/**
* @class Redirect
* @description manage all the redirect from here.
*/

class Redirect{

	/**
	* @method to - the redirect location...
	* @description manage all the redirections from here.
	*
	* @param {$string} $location 			- the PATH where it should redirect.
	*/

	public static function to($location = null){

		if($location){

			header('Location: ' . $location);

			exit();
		}
	}
}
