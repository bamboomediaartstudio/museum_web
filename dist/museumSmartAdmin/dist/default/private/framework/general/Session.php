<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

class Session{

	/**
	* put();
	* @description 									-						add...
	* @param $name									-						the name of the session.
	* @return $boolean							-						true / false...
	*/

	public static function put($name, $value){ return $_SESSION[$name] = $value; }

	/**
	*  exists();
	* @description 									-						check if a session exists.
	* @param $name									-						the name of the session.
	* @return $boolean							-						true / false...
	*/

	public static function exists($name){ return (isset($_SESSION[$name])) ? true : false; }

	/**
	*  delete();
	* @description 									-						delete a session...
	*/

	public static function delete($name){  if(self::exists($name)) unset($_SESSION[$name]); }

	/**
	*  get();
	* @description 									-						get a session
	* @param $name									-						the name of the session.
	* @return $_SESSION							-						the cookie...
	*/
	public static function get($name){ return $_SESSION[$name]; }

	/**
	*  flash();
	* @description 									-						...
	* @param $name									-						the name to flash...
	* @return $string								-						...
	*/

	public static function flash($name, $string = ''){

		if(self::exists($name)){

			$session = self::get($name);

			self::delete($name);

			return $session;

		}else{

			self::put($name, $string);
		}

	}

}
