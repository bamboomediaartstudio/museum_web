<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

class TFAChecker {

  private $_db;

  private $_user;

  private $use2faAuth;

  private $_data;

  /**
	*  __construct									-						constructor...
	* @description 									-						manage all about users...
	* @param $user									-						receive the user. If null, check session...
	*/

	public function __construct($user = null){

    $this->_user = $user;

  }

  /**
	* create();
	* @description 									-						create a new user...
	* @param $fields								-						array with all th data...
	*/

	public function check2FAStatus(){

    $query = DB::getInstance()->query('select * from dev_general_config')->first();

    //first we need to know if the system is using 2FA...

    $systemIsUsing2FA = ($query->use_2fa == 1) ? true : false;

    $this->_data['remember_2fa_in_device'] =

    $system2FADays = $query->get_2fa_days;

    $systemRememberInDevice = ($system2FADays == 0) ? false : true;

    //if the system is not using it,  exit from here.

    $this->_data['status'] = false;

    if($systemIsUsing2FA == false){

      $this->_data['status'] = false;

      $this->_data['msg'] = 'the system is not using 2FA';

    }else{

      //otherwhise, we have to check if the user is using it...

      $userQuery = DB::getInstance()->query('select * from users WHERE id = ?', [$this->_user->data()->id])->first();

      $userIsUsing2FA = ($userQuery->is_using_2fa == 1) ? true : false;

      //if system is using it but not the user, we just exit. We must evalute the cases with 2FA as mandatory...

      if($userIsUsing2FA == false){

        $this->_data['status'] = false;

        $this->_data['msg'] = 'the system is using 2FAs but the user do not allow it';

      }else{

        //from here, user is using it...

        if($systemRememberInDevice == false){

          //in this case, is mandatory, since there is no remember politics. Prompt must be show to user.

          $this->_data['status'] = true;

          $this->_data['msg'] = 'El sistema requiere acceso seguro con 2FA cada vez que inicies sesión. Por favor, ingresá los seis dígitos que aparecen en Google Authenticator:';

        }else{

          //if the system is uing it and the user too, we have to check if this is a new device.
          //if we are using a new device, no matter the time expiry option. We just have to show the 2FA for
          //the new device...

          if(!Cookie::exists('confirmed-device') || Cookie::get('confirmed-device') != 1){

            $this->_data['status'] = true;

            $this->_data['msg'] = '¿Estás accediendo desde un equipo nuevo?<br><br> Por favor, ingresá los seis dígitos que aparecen en Google Authenticator:';

          }else{

            $today = new DateTime("now");

            $expiryDate = new DateTime($userQuery->last_2fa_auth_setted);

            $diff = $expiryDate->diff($today)->format("%r%a");

            $this->_data['diff']  = $diff;

            if($diff >= 0){

              $this->_data['status'] = true;

              $this->_data['msg'] = 'Pasaron más de ' . $system2FADays . ' días desde tu útlima autenticación.<br><br> Por favor, ingresá los seis dígitos que aparecen en Google Authenticator:';

            }else{

              $this->_data['status'] = false;

              $this->_data['msg'] = 'sesión guardada.';

            }

          }

        }

      }

    }

      return json_encode($this->_data);

  }

}
