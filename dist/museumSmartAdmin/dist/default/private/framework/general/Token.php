<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

/**
*  Token();
* @description 									-						Manage all the tokens stuff...
*/

Class Token{

	/**
	*  generate();
	* @description 									-						create a Token...
	* @return $_SESSION							-						true / false...
	*/

	public static function generate(){ return Session::put(Config::get('session/token_name'), md5(uniqid())); }

	/**
	*  check();
	* @description 									-						check a token
	* @return $boolean							-						true / false...
	*/

	public static function check($token){

		$tokenName = Config::get('session/token_name');

		return (Session::exists($tokenName) && $token === Session::get($tokenName)) ?  true : false;

	}

	/**
	* delete();
	* @description 									-						delete a token...
	* @return $boolean							-						true / false...
	*/

	public static function delete($token){

		$tokenName = Config::get('session/token_name');

		if(Session::exists($tokenName) && $token === Session::get($tokenName)) Session::delete($tokenName);

	}
}
