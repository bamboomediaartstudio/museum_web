<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

class User {

	private $_db;

	private $_data;

	private $_socialMedia;

	private $_profileImage;

	private $_sessionName;

	private $_cookieName;

	private $_isLoggedIn;

	private $_errorsAreDetailed = false;

	private $_sessionLogginId;

	private $_loginErrors = array();

	/**
	*  __construct									-						constructor...
	* @description 									-						manage all about users...
	* @param $user									-						receive the user. If null, check session...
	*/

	public function __construct($user = null){

		$this->_db = DB::getInstance();

		$this->_sessionName = Config::get('session/session_name');

		$this->_cookieName = Config::get('remember/cookie_name');

		if(!$user){

			if(Session::exists($this->_sessionName)){

				$user = Session::get($this->_sessionName);

				if($this->find($user)) $this->_isLoggedIn = true;
			}

		}else{

			$this->find($user);

		}

	}

	/**
	* create();
	* @description 									-						create a new user...
	* @param $fields								-						array with all th data...
	*/

	public function create($fields = array()){

		if(!$this->_db->insert('users', $fields)){ throw new exception('there was a problem creating the user'); }

	}

	/**
	* update();
	* @description 									-						update user data...
	* @param $fields								-						array with all th data to update...
	*/

	public function update($fields = array(), $id = null){

		if(!$id && $this->isLoggedIn()){ $id = $this->data()->id; }

		if(!$this->_db->update('users', $id, $fields)){ throw new exception('there was a problem creating the user'); }

	}

	/**
	* find();
	* @description 									-						find an user by id or mail...
	* @param $user									-						the user :)
	* @return $boolean							-						true or false, depending on search...
	*/

	public function find($user = null){

		if($user){

			if(is_numeric($user)){

				$query = 'SELECT * FROM users WHERE id = ? AND deleted = ?';

			}else{

				$query = 'SELECT * FROM users WHERE email = ? AND deleted = ?';
			}

			$data = $this->_db->query($query, [$user , 0]);

			if($data->count()){

				$this->_data = $data->first();

				return true;
			}
		}
		return false;
	}

	/**
	* isMasterUser();
	* @description 									-						check if user is master...
	* @return $boolean							-						true or false....
	*/

	public function isMasterUser(){ return ($this->data()->group_id == 1 || $this->data()->group_id == 2 || $this->data()->group_id == 5) ? true : false; }

	/**
	* isHierarchyUser();
	* @description 									-						check if user is heararchy...
	* @return $boolean							-						true or false....
	*/

	public function isHierarchyUser(){ return ($this->data()->group_id == 5) ? true : false; }

	/**
	* hasAccessTo();
	* @description 									-						 check if user has access to section...
	* @return $boolean							-						 true / false...
	*/

	public function hasAccessTo($section){

	  if($this->data()->group_id == 1  || $this->data()->group_id == 2) return true;

	  $groupId = $this->_db->query('SELECT id FROM museum_admin_sections_groups WHERE group_key = ?', [$section]);

	  $hasAccess = $this->_db->query('SELECT * from museum_admin_sections_users_relations WHERE id_user = ? AND id_group = ?', [$this->data()->id, $groupId->first()->id]);

	  if($hasAccess->count() == 0){

	    $value = 0;

	  }else{

	    $value = $hasAccess->first()->status;
	  }

	  return $value;

	}

	/**
	* canSeeAbmOption();
	* @description 									-						 dont know?
	* @return ...										-						 ...
	*/

	public function canSeeAbmOption(){

		$userRole = $this->data()->group_id;

		return ($userRole == 1 || $userRole == 2) ? true : false;

	}

	/**
	* login();
	* @description 									-						 important function that will try to make the login.
	* @param $email									-						 user email.
	* @param $password							-						 from form...
	* @param $remember							-						 remember the login_ false by default...
	* @return $boolean							-						 true / false
	*/

	public function login($email = null, $password = null, $remember = false){

		if(!$email && !$password  && $this->exists()){

			Session::put($this->_sessionName, $this->data()->id);

			return true;

		}else{

			$user = $this->find($email);

			if($user){

				if($this->data()->password === Hash::make($password, $this->data()->salt)){

					Session::put($this->_sessionName, $this->data()->id);

					if($remember){

						$hash = Hash::unique();

						$hashCheck = $this->_db->get('users_sessions', array('user_id', '=', $this->data()->id));

						if(!$hashCheck->count()){

							$this->_db->insert('users_sessions', array(

								'user_id' => $this->data()->id,

								'hash' => $hash
							));

						}else{

							$hash = $hashCheck->first()->hash;

						}

						Cookie::put($this->_cookieName, $hash, Config::get('remember/cookie_expiry'));
					}

					$date = date("Y-m-d H:i:s");

					//last login...

					$this->_db->query("UPDATE users SET last_login = ?, logins = logins + 1 WHERE id = ?",

					[$date,$this->data()->id]);

					//add log data...

					$this->_db->insert('logs',

					['log_date' => $date,

					'user_id' => $this->data()->id,

					'log_type' => "user",'log_note' => "User logged in."]);

					//add ip...

					$ip = $_SERVER['REMOTE_ADDR'];

					$q = $this->_db->query("SELECT id FROM users_ip_list WHERE ip = ?",array($ip));

					$c = $q->count();

					if($c < 1){

						$this->_db->insert('users_ip_list', array('user_id' => $this->data()->id, 'ip' => $ip, ));

					}else{

						$f = $q->first();

						$this->_db->update('users_ip_list',$f->id, array('user_id' => $this->data()->id, 'ip' => $ip,

							'timestamp'=>date('Y-m-d H:i:s')));

					}

					return true;

				}else{

					$this->addLoginError("El password que ingresaste para el usuario <strong>{$email}</strong> es incorrecto.");

					return false;

				}

			}else{

				$this->addLoginError("el email <strong>{$email}</strong> no se encuentra en nuestra base de datos.");

				return false;
			}

		}

		return false;

	}

	/**
	* exists();
	* @description 								-						 check if exists...?
	* @return $boolean							-						 true / false
	*/

	public function exists(){ return (!empty($this->_data)) ? true : false; }

	/**
	* logout();
	* @description 									-						 logout from ABM...
	*/

	public function logout(){

		$this->_db->delete('users_sessions', array('user_id', '=', $this->data()->id));

		$this->addLogData('Cerraste sesión.', 'cerró sesión.', 'danger', 'fas fa-power-off');

		Session::delete($this->_sessionName);

		Cookie::delete($this->_cookieName);

	}

	//group_ids (roles) de usuarios que el usuario loggeado puede eliminar, segun su group_id

	/**
	* canDeleteUsers($idGroupLoggedUser);
	* @description 									-						 ...
	*/

	public function canDeleteUsers($idGroupLoggedUser){

		$devArr = [2,3,4,5]; //Todopoeroso, usuario developer puede eliminar estos ids...

		$hierArr = [2,3,4]; //Jerarquia

		$masterArr = [3,4]; // Master

		//El indice de este array representa cada rol (buscar en db group_id para agregar mas)

		$arrRolTypes = array(

				1	=> $devArr,

				2	=> $masterArr,

				5 => $hierArr			//5-> user jerarquia. Caso en el que este loggeado sea un id 5, uer jerarquico

		);

		return $arrRolTypes[$idGroupLoggedUser];

	}

	/**
	* getRecoverPassData();
	* @description 										-						 get verycode and verycode expiry...
	*/

	public function getRecoverPassData(){

		$idUser = $this->data()->id;

		$sql = "SELECT vericode, vericode_expiry FROM users_recover_password WHERE user_id = ?";

		$queryRecover = $this->_db->query($sql, [$idUser]);

		$arrRecover = $queryRecover->results(true);

		return ["vericode" => $arrRecover[0]["vericode"], "vericode_expiry" => $arrRecover[0]["vericode_expiry"]];
	}

	//getters and setters...

	public function data(){ return $this->_data; }

	public function isLoggedIn(){ return $this->_isLoggedIn; }

	/**
	* addLoginError($error);
	* @description 										-						 add a new error to the array.
	*@param {string} $errors					-						 string with the error :)
	*/

	private function addLoginError($error){ $this->_loginErrors[] = $error; }

	/**
	* loginErrors();
	* @description 										-						 if the login went wrong, get the errors lits in an array.
	*/

	public function loginErrors(){

		$errorsList = array();

		if(!$this->_errorsAreDetailed){

			$errorsList[0] = 'El <b>email</b> o la <b>contraseña</b> son incorrectos. Por favor, volvé a intentarlo';

		}else{

			$errorsList = $this->_loginErrors;

		}

		return $errorsList;

	}

	public function getLastSession(){

		$session = $this->_db->query('SELECT * FROM users_login_data WHERE id = ?', [$this->data()->id])->first();

		return $session;


	}

	/**
	* changePassword();
	* @description 										-						 change the password for the user...
	* @param $actualPassword					-						 the user actual password...
	* @param $newPassword							-						 the user new password...
	*/

	public function changePassword($actualPassword, $newPassword){

		if(Hash::make($actualPassword, $this->data()->salt) !== $this->data()->password){

			throw new Exception("La contraseña es incorrecta", 3);

		}else{

			$passData = DB::getInstance()->query('SELECT *  from dev_general_config')->first();

			$resetPass = $passData->reset_password_periodically;

			if($resetPass == 1){

				$resetPassOffset = $passData->reset_password_time_offset;

			}else{

				$resetPassOffset = 365 * 100;

			}

			$salt = Hash::salt(32);

			$today = new DateTime('now');

			$today->modify('+' . $resetPassOffset . ' day');

			$this->update(array('password' => Hash::make($newPassword, $salt), 'salt' => $salt, 'password_expiry'=> $today->format('Y-m-d H:i:s')));

		}

	}

	public function addLogData($personalMsg, $externalMsg, $brand, $icon, $isPublic = 1){

		$this->_db->insert('users_logs', array(

			'id_user' => $this->data()->id,

			'session_login_id' => Session::get('lastLogginSessionData'),

			'personal_message' => $personalMsg,

			'external_message' => $externalMsg,

			'brand' => $brand,

			'is_public' => $isPublic,

			'icon' => $icon));
	}

	/**
	* addLoginData();
	* @description 										-						 save login access data...
	*/

	public function addLoginData(){

		$whitelist = array('127.0.0.1', '::1');

		$userIp = (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '181.167.224.102' : 'Visitor';

		$logginSessionData = $this->_db->insert('users_login_data', array(

			'user_id' => $this->data()->id,

			'ip' => Helpers::getIP(),

			'date_time' => date('Y/m/d H:i:s'),

			'browser_name' => Helpers::getBrowser(),

			'country' => Helpers::getDataByIp($userIp, "Country"),

			'country_code' => Helpers::getDataByIp($userIp, "Country Code"),

			'state' => Helpers::getDataByIp($userIp, "state"),

			'city' => Helpers::getDataByIp($userIp, "city"),

			'latitude' => Helpers::getDataByIp($userIp, "latitude"),

			'longitude' => Helpers::getDataByIp($userIp, "longitude"),

			'timezone' => Helpers::getDataByIp($userIp, "timezone"),

			'os' => Helpers::getOS()

		));

		$this->_sessionLogginId = DB::getInstance()->lastId();

		Session::put('lastLogginSessionData', DB::getInstance()->lastId());

	}

}

?>
