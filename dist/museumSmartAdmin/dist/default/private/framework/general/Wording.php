<?php

/**
* @function Wording
* @description All the related things to wording...
*/

class Wording{

	public $wordingArray = array();

	/**
	* @function Constructor
	* @description All the related things to wording...
	*/

	public function __construct(){ $this->wordingArray = array(); }

	/**
	* @function load
	* @param $category 		- the actual category
	* @param $subcategory 	- the actual subcategory
	* @description the function receive the values and load the data from mysql
	*/

	public function load($category, $subcategory){

		$values = DB::getInstance()->query('SELECT * from museum_wording WHERE category = ?', [$category]);

		foreach($values->results() as $result){

			$itemKey = $result->item_key;

			$value = $result->value;

			$this->wordingArray[$itemKey] = $value;

		}

	}

	/**
	* @function get
	* @param $value			- receive the key value
	* @param $return 		- the DB value according to the key value...
	* @description manage all the wording under this key - value criteria
	*/

	public function get($value){

		return $this->wordingArray[$value];
	} 

}