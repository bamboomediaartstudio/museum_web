<?php  ?>

<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

	<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper"></div>

	<div class="kt-header__topbar">

		<div class="kt-header__topbar-item kt-header__topbar-item--quick-panel" data-toggle="kt-tooltip" title="" data-placement="right" data-original-title="Config">

				<span class="kt-header__topbar-icon" id="kt_quick_panel_toggler_btn">

					<i class="fas fa-cogs kt-font-info"></i> 

				</span>
			</div>

		<div class="kt-header__topbar-item kt-header__topbar-item--user">

			<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">

				<div class="kt-header__topbar-user">

					<span class="kt-header__topbar-welcome kt-hidden-mobile">Hola,</span>

					<span class="kt-header__topbar-username kt-hidden-mobile"><?php echo $user->data()->name . ' ' . $user->data()->surname;?></span>

					<img class="kt-hidden" alt="Pic" src="assets/media/users/300_25.jpg" />

					<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?php echo $user->data()->name[0];?></span>

				</div>

			</div>

			<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

				<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url(assets/media/misc/bg-1.jpg)">

					<div class="kt-user-card__avatar">

						<img class="kt-hidden" alt="Pic" src="assets/media/users/300_25.jpg" />

						<span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success"><?php echo $user->data()->name[0];?></span>

					</div>

					<div class="kt-user-card__name"> <?php echo $user->data()->name . ' ' . $user->data()->surname;?> </div>

				</div>

				<div class="kt-notification">

					<a href="profile.php#personal" class="kt-notification__item">

						<div class="kt-notification__item-icon">

							<i class="flaticon2-calendar-3 kt-font-success"></i>

						</div>

						<div class="kt-notification__item-details">

							<div class="kt-notification__item-title kt-font-bold"> Mi Perfil </div>

							<div class="kt-notification__item-time">Configuración de cuenta y datos personales.</div>

						</div>

					</a>

					<a href="profile.php#picture" class="kt-notification__item">

						<div class="kt-notification__item-icon">

							<i class="flaticon-photo-camera kt-font-danger"></i>

						</div>

						<div class="kt-notification__item-details">

							<div class="kt-notification__item-title kt-font-bold"> Mi Foto </div>

							<div class="kt-notification__item-time">Tu foto de perfil. </div>

						</div>

					</a>

					<a href="profile.php#password" class="kt-notification__item">

						<div class="kt-notification__item-icon">

							<i class="fas fa-key kt-font-warning"></i>

						</div>

						<div class="kt-notification__item-details">

							<div class="kt-notification__item-title kt-font-bold"> Seguridad </div>

							<div class="kt-notification__item-time">Tu contraseña y assets de seguridad.</div>

						</div>

					</a>

					<a href="profile.php#2fa" class="kt-notification__item">

						<div class="kt-notification__item-icon">

							<i class="fas fa-shield-alt kt-font-brand"></i>

						</div>

						<div class="kt-notification__item-details">

							<div class="kt-notification__item-title kt-font-bold"> 2FA </div>

							<div class="kt-notification__item-time">Factor de Doble Autenticación.</div>

						</div>

					</a>

					<div class="kt-notification__custom kt-space-between">

						<a href="private/framework/users/logout.php" target="_blank" class="btn btn-label btn-label-brand btn-sm btn-bold">Salir</a>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>
