<?php
 ?>

 <div id="kt_quick_panel" class="kt-quick-panel">

   <a href="#" class="kt-quick-panel__close" id="kt_quick_panel_close_btn"><i class="flaticon2-delete"></i></a>

   <div class="kt-quick-panel__nav">

     <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand  kt-notification-item-padding-x" role="tablist">

       <li class="nav-item active">

         <a class="nav-link active" data-toggle="tab" href="#kt_quick_panel_tab_logs" role="tab">Logs de actividad</a>

       </li>

       <li class="nav-item">

         <a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_settings" role="tab">Seettings</a>

       </li>

     </ul>

   </div>

   <div class="kt-quick-panel__content">

     <div class="tab-content">

       <div class="tab-pane  kt-scroll active" id="kt_quick_panel_tab_logs" role="tabpanel">

         <div class="kt-notification-v2">

           <?php

           $notificationsQuery = DB::getInstance()->query('SELECT * from users_logs WHERE id_user = ? order by id DESC limit 100', [$user->data()->id]);

           foreach($notificationsQuery->results() as $result){ ?>

           <a href = "#" class="kt-notification-v2__item">

             <div class="kt-notification-v2__item-icon">

               <i class="<?php echo $result->icon;?> kt-font-<?php echo $result->brand;?>"></i>

             </div>

             <div class="kt-notification-v2__itek-wrapper">

               <div class="kt-notification-v2__item-title"> <?php echo $result->personal_message;?> </div>

               <div class="kt-notification-v2__item-desc">

                 <time class="timeago" datetime="<?php echo $result->date;?>"></time>

               </div>

             </div>

           </a>

          <?php } ?>

        </div>

      </div>

       <!-- settings -->

       <div class="tab-pane kt-quick-panel__content-padding-x fade kt-scroll" id="kt_quick_panel_tab_settings" role="tabpanel">

         <form class="kt-form">

         <?php

         //first we have to loop through options...

         $categoriesQuery = DB::getInstance()->query('SELECT * from users_settings_categories WHERE active = ? AND deleted = ?', [1, 0]);

         foreach($categoriesQuery->results() as $result){ ?>

           <div class="kt-heading kt-heading--sm kt-heading--space-sm"><?php echo $result->category;?></div>

           <?php

           //then through subcategories related to user...
           //important: reverse check for actions in code:
           //SELECT value from users_settings_subcategories_options as usso INNER JOIN users_settings_subcategories as uss ON usso.id_subcategory = uss.id WHERE usso.id_user = 3 AND uss.identifier = 'on_new_user_notification_sent'

           $subcategoriesQuery = DB::getInstance()->query(

             'SELECT * from users_settings_subcategories AS uss

              INNER JOIN users_settings_subcategories_options as usso

              ON uss.id = usso.id_subcategory WHERE usso.id_user = ? AND uss.id_category = ? ', [$user->data()->id, $result->id]);

              foreach($subcategoriesQuery->results() as $catResult){ ?>

                <div class="form-group form-group-xs row">

                 <label class="col-8 col-form-label"><?php echo $catResult->option;?></label>

                 <div class="col-4 kt-align-right">

                   <span class="kt-switch kt-switch--<?php echo $result->brand;?> kt-switch--sm">

                     <label>

                       <input type="checkbox" <?php if($catResult->value ==1) echo 'checked="checked"';?> name="quick_panel_notifications_1">

                       <span></span>

                     </label>

                   </span>

                 </div>

               </div>

          <?php } ?>

           <div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

        <?php } ?>

       </form>

     </div>

   </div>

 </div>

</div>
