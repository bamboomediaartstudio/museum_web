<?php
?>

<meta name="description" content="Updates and statistics">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />

<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />

<link href="assets/css/skins/header/base/light.css" rel="stylesheet" type="text/css" />

<link href="assets/css/skins/header/menu/light.css" rel="stylesheet" type="text/css" />

<link href="assets/css/skins/brand/dark.css" rel="stylesheet" type="text/css" />

<link href="assets/css/skins/aside/dark.css" rel="stylesheet" type="text/css" />

<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
