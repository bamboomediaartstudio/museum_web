<?php

/**
* @summary Manage all the mailing stuff...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

class EmailTemplate{

  var $variables = array();

  var $path_to_file= array();

  /**
  *  __construct									-						constructor...
  * @description 									-						the template to use...
  */

  function __construct($path_to_file){

    if(!file_exists($path_to_file)){

      trigger_error('Template File not found!',E_USER_ERROR);

      return;

    }

    $this->path_to_file = $path_to_file;

  }

  /**
  *  __set($key,$val);
  * @description 									-						receive the key pais values...
  * @param $key 									-						the name...
  * @param $value 								-						the value...
  */

  public function __set($key,$val) { $this->variables[$key] = $val; }

  /**
  *  compile();
  * @description 									-						use the ob_stat to extrat and compile.
  * @return $content 							-						the template...
  */

  public function compile(){

    ob_start();

    extract($this->variables);

    include $this->path_to_file;

    $content = ob_get_contents();

    ob_end_clean();

    return $content;

  }
}

 ?>
