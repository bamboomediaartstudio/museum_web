<?php

/**
* @summary Manage all the mailing stuff...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

use PHPMailer\PHPMailer\PHPMailer;

use PHPMailer\PHPMailer\Exception;

//require 'phpmailer/Exception.php';

//require 'phpmailer/PHPMailer.php';

//require 'phpmailer/SMTP.php';

//...

class Emailer{

  var $recipients = array();

  var $subject = "";

  var $EmailTemplate;

  var $EmailContents;

  /**
  *  __construct									-						constructor...
  * @description 									-						manage all about users...
  * @param $to			  						-						the recipients, as string for single or array...
  */

  public function __construct($to = false, $subject = "")
  {
      if($to !== false)
      {
          if(is_array($to)) {

              foreach($to as $_to){ $this->recipients[$_to] = $_to; }

          }else{

              $this->recipients[$to] = $to;
          }
      }

      $this->subject = $subject;
  }

  /**
  *  SetTemplate(EmailTemplate $EmailTemplate);
  * @description 									-						the template for this mail...
  */

  function SetTemplate(EmailTemplate $EmailTemplate){ $this->EmailTemplate = $EmailTemplate; }

  /**
  *  send();
  * @description 									-						send thre message...
  */

  function send(){

    //museodelholocausto.norepply@gmail.com

    //museodelholocausto.recovery@gmail.com

      $email = 'museodelholocausto.recovery@gmail.com';

      $this->EmailTemplate->compile();

      $mail = new PHPMailer(true);

      $mail->CharSet = "UTF-8";

    	$mail->IsSMTP();

    	$mail->SMTPDebug = false;

    	$mail->SMTPAuth = true;

  		$mail->SMTPSecure = 'tls';

  		$mail->SMTPAutoTLS = false;

  		$mail->Host = 'smtp.gmail.com';

  		$mail->Port = 465;

  		$mail->Username = $email;

  		$mail->Password = 'antonietaFrancesca';

  		$mail->From = $email;

  		$mail->FromName = 'Museo';

      reset($this->recipients);

  		$mail->AddReplyTo(key($this->recipients),'reply');

  		$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';

      foreach($this->recipients as $to){ $mail->addAddress(rawurldecode($to)); }

  		$mail->addBCC($email);

  		$mail->isHTML(true);

  		$mail->Subject = $this->subject;

  		$mail->Body = $this->EmailTemplate->compile();

  		$result = $mail->send();

  		return $result;

    }
}

 ?>
