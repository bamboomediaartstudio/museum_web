<?php

  $sarlanga = 'outside bar';

  //require_once '../../../users/core/init.php';

  $getData = DB::getInstance()->query("SELECT * FROM museum_data");

  $institutionSettings = $getData->first();

  $address = $institutionSettings->address . ' ' . $institutionSettings->address_number . ', ' . $institutionSettings->zip_code . ' ' . $institutionSettings->city;

  $lat = $institutionSettings->lat;

  $lon = $institutionSettings->lon;

  $te = $institutionSettings->te;

  $email = $institutionSettings->email;

  $projectTitle = $institutionSettings->title;

  $securityString = '<small>Este mensaje fue enviado desde un sistema operativo <strong>' . Helpers::getOS() . '</strong>, usando el browser <strong>' . Helpers::getBrowser() . '</strong> desde la dirección IP <strong>' . Helpers::getIP() . '</strong>. Si no completaste ningún formulario relacionado a <strong>' . $projectTitle . '</strong>, ignorá este mensaje, o contactanos a <a href="mailto:info@museodelholocausto.org.ar">info@museodelholocausto.org.ar</a></small>';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

  <head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title><?php echo $title;?></title>

    <style type="text/css" rel="stylesheet" media="all">

    <?php echo file_get_contents("includes/styles.css", FILE_USE_INCLUDE_PATH); ?>

    </style>

  </head>

  <body>

    <span class="preheader"><?php echo $title;?></span>

    <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0">

      <tr>

        <td align="center">

          <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">

            <tr>

              <td class="email-masthead logo">

              <a href="https://www.museodelholocausto.org.ar" class="email-masthead_name">

                <img src="https://www.museodelholocausto.org.ar/private/img/sm/logo.png" alt="Museo Del Holocausto" class="logo" width="100px">

              </a>

            </td>

            </tr>

            <tr>

              <td class="email-body" width="100%" cellpadding="0" cellspacing="0">

                <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">

                  <tr>

                    <td class="content-cell">

                      <?php echo $bodyContent;?>

                      <br><br>

                      <?php echo $securityString;?>

                    </td>

                  </tr>

                </table>

              </td>

            </tr>

            <tr>

              <td>

                <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0">

                  <tr>

                    <td class="content-cell" align="center">

                      <table class="social" align="center" width="570" cellpadding="0" cellspacing="0">

                        <tr>

                          <?php

                          $query = DB::getInstance()->query('SELECT * from museum_sm');

                          foreach($query->results() as $result){ ?>

                            <td class="align-center">

                              <a target='_blank' href="<?php echo $result->url;?>">

                                <img src="https://www.museodelholocausto.org.ar/private/img/sm/<?php echo strtolower($result->name);?>.png" height="20" alt="<?php echo $result->name;?>" class="social_icon">

                              </a>

                            </td>

                          <?php } ?>

                        </table>

                        <p class="sub align-center">&copy; <?php echo date('Y');?> <?php echo $projectTitle;?>. Todos los derechos reservados.</p>

                        <p class="sub align-center">

                          <br><a href="https://maps.google.com/?q=<?php echo $lat;?>,<?php echo $lon;?>"><?php echo $address;?></a><br>

                          <br><a href="tel:<?php echo $te;?>"><?php echo $te;?></a><br>

                          <br><a href="mailto:<?php echo $email;?>"><?php echo $email;?></a><br>

                        </p>

                      </td>

                    </tr>

                  </table>

                </td>

              </tr>

            </table>

          </td>

        </tr>

      </table>

    </body>

  </html>
