<?php

/**
* @summary change password...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

setlocale(LC_TIME, 'es_ES', 'esp_esp');

require('../../core/init.php');

require('../../../libraries/datatables/ssp.customized.class.php');

if(!Input::exists('get')) exit(); 

$user = new User();

if(!$user->isLoggedIn()) exit();

$table = 'users_logs';

$userId = $_GET['userId'];

$primaryKey = 'id';

$columns = array(

	array( 'db' => '`u`.`id`', 'dt' => 'id', 'field'=>'id'),

	array( 'db' => '`u`.`external_message`', 'dt' => 'external_message', 'field'=>'external_message'),

	array( 'db' => '`u`.`date`', 'dt' => 'date', 'field' =>'date', 'formatter' => function($d, $row) { return utf8_encode(strftime("%A, %d de %B de %Y", strtotime($d)));}),

	array( 'db' => '`u`.`brand`', 'dt' => 'brand', 'field'=>'brand'),

	array( 'db' => '`u`.`icon`', 'dt' => 'icon', 'field'=>'icon')
);

$whitelist = array('127.0.0.1', '::1');

$sql_details = array(

	'host'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '127.0.0.1' : '108.179.242.98',

	'user'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? 'root' : 'brpxmzm5_make',

	'pass'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '' : 'n},iT0aJTqnF',

	'db'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? 'lasalle_museum' : 'brpxmzm5_museum_smart_admin'

);

$joinQuery = "FROM `users_logs` AS `u`";

$extraWhere = "`u`.`id_user` = " . $userId;

header('Content-type: text/javascript');

echo json_encode( SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere), JSON_PRETTY_PRINT );
?>