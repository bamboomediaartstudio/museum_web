<?php

/**
* @summary change password...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

setlocale(LC_TIME, 'es_ES', 'esp_esp');

require('../../core/init.php');

require('../../../libraries/datatables/ssp.customized.class.php');

if(!Input::exists('get')) exit(); 

$user = new User();

if(!$user->isLoggedIn()) exit();

$table = 'users_login_data';

$userId = $_GET['userId'];

$primaryKey = 'id';

$columns = array(

	array( 'db' => '`u`.`id`', 'dt' => 'id', 'field'=>'id'),

	array( 'db' => '`u`.`ip`', 'dt' => 'ip', 'field'=>'ip'),

	array( 'db' => '`u`.`os`', 'dt' => 'os', 'field'=>'os'),

	array( 'db' => '`u`.`country`', 'dt' => 'country', 'field'=>'country'),

	array( 'db' => '`u`.`state`', 'dt' => 'state', 'field'=>'state'),

	array( 'db' => '`u`.`city`', 'dt' => 'city', 'field'=>'city'),

	array( 'db' => '`u`.`timezone`', 'dt' => 'timezone', 'field'=>'timezone'),

	array( 'db' => '`u`.`latitude`', 'dt' => 'latitude', 'field'=>'latitude'),

	array( 'db' => '`u`.`longitude`', 'dt' => 'longitude', 'field'=>'longitude'),

	array( 'db' => '`u`.`date_time`', 'dt' => 'date_time', 'field' =>'date_time', 'formatter' => function($d, $row) { return utf8_encode(strftime("%d de %B de %Y", strtotime($d)));}),
	
	array( 'db' => '`u`.`browser_name`', 'dt' => 'browser_name', 'field'=>'browser_name' )

);

$whitelist = array('127.0.0.1', '::1');

$sql_details = array(

	'host'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '127.0.0.1' : '108.179.242.98',

	'user'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? 'root' : 'brpxmzm5_make',

	'pass'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '' : 'n},iT0aJTqnF',

	'db'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? 'lasalle_museum' : 'brpxmzm5_museum_smart_admin'

);

$joinQuery = "FROM `users_login_data` AS `u`";

$extraWhere = "`u`.`user_id` = " . $userId;

header('Content-type: text/javascript');

echo json_encode( SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere), JSON_PRETTY_PRINT );
?>