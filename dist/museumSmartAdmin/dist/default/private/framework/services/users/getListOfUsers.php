<?php

/**
* @summary change password...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

setlocale(LC_TIME, 'es_ES', 'esp_esp');

require('../../core/init.php');

require('../../../libraries/datatables/ssp.customized.class.php');

if(!Input::exists('get')) exit();

$user = new User();

if(!$user->isLoggedIn()) exit();

$table = 'users';

$userId = $_GET['userId'];

$primaryKey = 'id';

$columns = array(

	array( 'db' => '`u`.`id`', 'dt' => 'main_id', 'field'=>'id',  'as' => 'id' ),

	array( 'db' => '`u`.`name`', 'dt' => 'first_name', 'field'=>'name' ),

	array( 'db' => '`uc`.`label`', 'dt' => 'label', 'field'=>'label' ),

	array( 'db' => '`uc`.`category_description`', 'dt' => 'category_description', 'field'=>'category_description' ),

	array( 'db' => '`u`.`surname`', 'dt' => 'last_name', 'field'=>'surname' ),

	array( 'db' => '`u`.`last_login`', 'dt' => 'last_login', 'field'=>'last_login', 'formatter' => function($d, $row) { return utf8_encode(strftime("%A, %d de %B de %Y", strtotime($d)));} ),

	array( 'db' => '`u`.`logins`', 'dt' => 'logins', 'field'=>'logins' ),

	array( 'db' => '`u`.`id_referer`', 'dt' => 'id_referer', 'field'=>'id_referer' ),

	array( 'db' => '`u`.`email`', 'dt' => 'email', 'field'=>'email' ),

	array( 'db' => '`u`.`phone`', 'dt' => 'phone', 'field'=>'phone' ),

	array( 'db' => '`u`.`bio`', 'dt' => 'bio', 'field'=>'bio' ),

	array( 'db' => '`u`.`active`', 'dt' => 'active', 'field'=>'active' ),

	array( 'db' => '`u`.`deleted`', 'dt' => 'deleted', 'field'=>'deleted' ),

	array( 'db' => '`u`.`group_id`', 'dt' => 'label_id', 'field'=>'group_id'),

	array( 'db' => '`u`.`joined`', 'dt' => 'joined', 'field' =>'joined', 'formatter' => function($d, $row) { return utf8_encode(strftime("%A, %d de %B de %Y", strtotime($d)));}
	)
);

$whitelist = array('127.0.0.1', '::1');

$sql_details = array(

	'host'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '127.0.0.1' : '108.179.242.98',

	'user'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? 'root' : 'brpxmzm5_make',

	'pass'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '' : 'n},iT0aJTqnF',

	'db'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? 'lasalle_museum' : 'brpxmzm5_museum_smart_admin'

);


$joinQuery = "FROM `users` AS `u` INNER JOIN `users_categories` AS `uc` ON (`u`.`group_id` = `uc`.`id`) ";

$extraWhere = "`u`.`id` != " . $userId;

//$groupBy = "`u`.`office`";

//$having = "`u`.`salary` >= 140000";


header('Content-type: text/javascript');

echo json_encode( SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere), JSON_PRETTY_PRINT );
?>
