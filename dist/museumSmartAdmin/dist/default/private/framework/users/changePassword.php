<?php

/**
* @summary change password...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

require_once '../core/init.php';

$status['status'] = 0;

$status['title'] = "";

$status['msg'] = "";

$status['type'] = "";

$user = new User();

if(!$user->isLoggedIn()){ Redirect::to('index.php'); }

if(Input::exists()){

	if(Token::check(Input::get('token'))){

		$validate = new Validate();

		$validation = $validate->check($_POST, array(

			'actualpassword' => array('display'=>'actualpass', 'required' => true, 'min' => 4),

			'newpassword' => array('display'=>'newpass', 'required' => true, 'min' => 4, 'different' => 'actualpassword'),

			'repeatnewpassword' => array('display'=>'repeatnewpass', 'required' => true, 'min' => 4, 'matches' => 'newpassword')

		));

		if($validation->passed()){

			try{

				$user->changepassword(Input::get('actualpassword'), Input::get('newpassword'));

				$status['status'] = 1;

				$status['title'] = "Se cambió la contraseña exitosamente.";

				$status['msg'] = "No te olvides de cambiar tu contraseña periódicamente para mayor seguridad.";

				$status['type'] = "success";

				$user->addLogData('Cambiaste tu contraseña.', 'cambió su contraseña.', 'warning', 'fas fa-key');

			}catch(Exception $e) {

				$status['status'] = 2;

				$status['title'] = "La contraseña es incorrecta.";

				$status['msg'] = "Tu contraseña actual no es correcta. Por favor, volvé a intentarlo";

				$status['type'] = "error";

			}

		}else{

			$errors = array();

			foreach($validation->errors() as $error){ $errors[] = $error; }

			$status['status'] = 3;

			$status['title'] = "Error de validación.";

			$status['msg'] = $errors[0];

			$status['type'] = "error";

		}

	}else{

		$status['status'] = 4;

		$status['title'] = "Fallo de seguridad.";

		$status['msg'] = "Error de token. Consultar a Aditivo";

		$status['type'] = "error";

	}

}else{

	$status['status'] = 5;

	$status['title'] = "Falta input.";

	$status['msg'] = "Revisar los campos";

	$status['type'] = "error";

}

echo json_encode($status);


?>
