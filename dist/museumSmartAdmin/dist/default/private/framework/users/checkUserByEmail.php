<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/


$status['status'] = 0;

$status['msg'] = "";

require_once '../core/init.php';

if(Input::exists()){

  if(Token::check(Input::get('token'))){

    $validate = new Validate();

		$validation = $validate->check($_POST, array('email' => array('display'=>'email', 'valid_email'=>true, 'required' => true)));

    if($validation->passed()){

      $query = DB::getInstance()->query('SELECT * from users WHERE email = ?',[Input::get('email')]);

      $status['count'] = $query->count();

      if($query->count() == 0){

        $status['valid'] = true;

      }else{

        $status['valid'] = false;

        $status['name'] = $query->first()->name;

        $status['surname'] = $query->first()->surname;

        $status['logins'] = $query->first()->logins;
      }

    }else{

      $status['valid'] = false;

    }

  }else{

    $status['valid'] = false;

    $status['msg'] = 'token';

  }

}else{

  $status['valid'] = false;

  $status['msg'] = 'form';

}

echo json_encode($status);

?>
