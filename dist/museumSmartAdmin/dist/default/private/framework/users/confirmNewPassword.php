<?php

/**
* @summary confirm password stuff...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

//error # 1010 - status: 0 - fallo el input.                - Error #1010 - Fallo el formulario. Por favor, volvé a intentarlo.
//error # 1011 - status: 1 - fallo el token.                - Error #1011 - Error en el formulario. Recargá la página (f5) y volvé a intentarlo. Si el problema persiste, comunicate a info@aditivointeractivegroup.com.
//error # 1012 - status: 2 - fallo en validator             - Error propio de clase Validate.php junto a #
//error # 1013 - status: 3 - no existe el email.            - Error #1013 - La dirección de correo electrónico que indicaste no existe en nuestra base de datos.
//error # 1014 - status: 4 - no existe el email pending.    - Error #1014 - No hay un procesod de recuperación en curso para el mail que indicaste.
//error # 1015 - status: 5 - error codigo                   - Error #1015 - El código de 6 dígitos que ingresaste es incorrecto.
//error # 1016 - status: 6 - error de db al crear           - Error #1016 - Error de base de datos. Recargá la página (f5) y volvelo a intentar.
//error # 1017 - status: 7 - error al borrar                - Error #1017 - Error de base de datos. Recargá la página (f5) y volvelo a intentar.
//error # 1018 - status: 8 - error al borrar                - Error #1018 - Error al llogear al usuario - pasa.
//warning # 1019 - status 9 - error en el mailing.          - Warning #1019 - Error al enviar el email. - pasa.
//success # 1020 - status 10 - ok.                          - Success  #1020 - Error al enviar el email. - pasa.

//status 8, 9, 10 - pasan. El resto, warning.

$status['status'] = 0;

$status['msg'] = "";

require_once '../core/init.php';

if(Input::exists()){

  if(Token::check(Input::get('securityToken'))){

    $validate = new Validate();

		$validation = $validate->check($_POST, array(

      'email' => array('display'=>'email', 'valid_email'=>true, 'required' => true),

			'code' => array('display'=>'code','required' => true, 'min'=>6, 'max'=>6),

			'password' => array('display'=>'password', 'required' => true, 'min' => 8),

			'repeatpassword' => array('display'=>'repeatpassword', 'required' => true, 'min' => 8, 'matches' => 'password')

		));

    if($validation->passed()){

      $status['status'] = 1;

      $status['msg'] = "por ahora bien";

      $confirmUser = DB::getInstance()->query('SELECT * from users WHERE email = ?', [Input::get('email')]);

      if($confirmUser->count() == 1){

        $saveId = $confirmUser->first()->id;

        $confirmRecover = DB::getInstance()->query('SELECT * from users_recover_password wHERE email = ?', [Input::get('email')]);

        if($confirmRecover->count()==1){

          if($confirmRecover->first()->unique_token == Hash::make(Input::get('code'), $confirmRecover->first()->unique_salt)){

            $saveSalt = Hash::salt(33);

            $savePass = Hash::make(Input::get('password'), $saveSalt);

            $updatePass = DB::getInstance()->update('users', $saveId, ['password' => $savePass, 'salt'=>$saveSalt]);

            if($updatePass){

              $delete = DB::getInstance()->delete("users_recover_password", ["email","=",Input::get('email')]);

              if($delete){

                $saveUser = new User();

                $saveUser->login(Input::get('email'), Input::get('password'));

                if($saveUser->isLoggedIn()){

                  $saveUser->addLogData('Recuperaste tu contraseña.', 'recuperó su contraseña.', 'warning', 'fas fa-user-lock', 0);

                  $emailer = new Emailer([$saveUser->data()->email], 'Cambiaste tu contraseña!');

                  $template = new EmailTemplate('../mailer/templates/custom.php');

                  $template->title = 'Cambiaste tu contraseña!';

                  $inviteString = '<h1>Hola, <b>' . $saveUser->data()->name . ' ' . $saveUser->data()->surname . '</b>!</h1><br>';

                  $inviteString .= '<p>Acabás de recuperar tu contraseña exitosamente!<p><br>';

                  $inviteString .= '<p>Recordá no compartir tu contraseña con nadie, ni anotarla en ningún lugar.</p><br>';

                  $inviteString .= '<p>Te recomendamos que cambies tu password con cierta regularidad.</p>';

                  $template->bodyContent = $inviteString;

                  $emailer->SetTemplate($template);

                  $mailWasSent = $emailer->send();

                  if($mailWasSent){

                    $status['status'] = 10;

                    $status['type'] = 'success';

                    $status['msg'] = "Success #1020 - Proceso validado. Redirigiendo...";

                  }else{

                    $status['status'] = 9;

                    $status['type'] = 'success';

                    $status['msg'] = "Warning #1019 - Error al enviar el email.";

                  }

                }else{

                  $status['status'] = 8;

                  $status['type'] = 'success';

                  $status['msg'] = "Warning #1018 - Error al llogear al usuario.";

                }

              }else{

                $status['status'] = 7;

                $status['type'] = 'danger';

                $status['msg'] = "Error #1017 - Error de base de datos. Recargá la página (f5) y volvelo a intentar.";

              }

            }else{

              $status['status'] = 6;

              $status['type'] = 'danger';

              $status['msg'] = "Error #1016 - Error de base de datos. Recargá la página (f5) y volvelo a intentar.";

            }


          }else{

            $status['status'] = 5;

            $status['type'] = 'danger';

            $status['msg'] = "Error #1015 - El código de 6 dígitos que ingresaste es incorrecto.";

          }

        }else{

          $status['status'] = 4;

          $status['type'] = 'danger';

          $status['msg'] = "Error #1014 - No hay un proceso de recuperación en curso para el mail que indicaste.";


        }

      }else{

        $status['status'] = 3;

        $status['type'] = 'danger';

        $status['msg'] = "Error #1013 - La dirección de correo electrónico que indicaste no existe en nuestra base de datos.";
      }

    }else{

      $errors = array();

			foreach($validation->errors() as $error){ $errors[] = $error; }

			$status['status'] = 2;

			$status['msg'] = "Error #1012: " . $errors[0][0];

			$status['type'] = "error";

    }

  }else{

    $status['status'] = 1;

    $status['type'] = 'danger';

    $status['msg'] = "Error #1011: Error en el formulario. Recargá la página (f5) y volvé a intentarlo. Si el problema persiste, comunicate a info@aditivointeractivegroup.com";
  }

}else{

  $status['status'] = 0;

  $status['type'] = 'danger';

  $status['msg'] = "Error #1010: Fallo el formulario. Por favor, volvé a intentarlo.";

}

echo json_encode($status);

?>
