<?php

/**
* @summary forget password stuff...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

//error # 1001 - fallo el input. - error #1001, Comunicate con Aditivo Interactive Group S.A o quien te haya dado de alta en el sistema.
//error # 1002 - fallo de token - error #1002, error #1002, actualizá la página (F5) y volvé a intentarlo.
//error # 1003 - error de vlaidación.
//
// forget 100
//confirm 101

require_once '../core/init.php';

$status['status'] = 0;

$status['msg'] = "";

$errorIdentifier = 100;

if(Input::exists()){

  if(Token::check(Input::get('token'))){

    $validate = new Validate();

		$validation = $validate->check($_POST, array( 'email' => array('display'=>'email', 'required' => true, 'email'=>true)));

		if($validation->passed()){

      $check = DB::getInstance()->query('SELECT * from users WHERE email = ?', [Input::get('email')]);

      //nonexistent email...

      if($check->count() == 0){

        $status['status'] = 4;

        $status['msg'] = 'Enviamos un email a <b>' . Input::get('email') . '</b> con las instrucciones para recuperar tu contraseña.';

        $checkNonExistentEmail = DB::getInstance()->query('SELECT * from users_failed_recovery_attempts WHERE email = ?', [Input::get('email')]);

        $whitelist = array('127.0.0.1', '::1');

        $userIp = (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '181.167.224.102' : 'Visitor';

        if($checkNonExistentEmail->count() == 0){

          $addAdvice = DB::getInstance()->insert('users_failed_recovery_attempts', array('email'=>Input::get('email')));

          $saveId = DB::getInstance()->lastId();

        }else{

          $saveId = $checkNonExistentEmail->first()->id;

        }

        $addAdviceLog = DB::getInstance()->insert('users_failed_recovery_attempts_logs', array(

          'id_email' => $saveId, 'ip' => Helpers::getIP(), 'date_time' => date('Y/m/d H:i:s'), 'browser_name' => Helpers::getBrowser(), 'country' => Helpers::getDataByIp($userIp, "Country"),

          'country_code' => Helpers::getDataByIp($userIp, "Country Code"), 'state' => Helpers::getDataByIp($userIp, "state"), 'city' => Helpers::getDataByIp($userIp, "city"),

          'latitude' => Helpers::getDataByIp($userIp, "latitude"), 'longitude' => Helpers::getDataByIp($userIp, "longitude"), 'timezone' => Helpers::getDataByIp($userIp, "timezone"),

          'os' => Helpers::getOS()));

      }else{

        $item = $check->first();

        $hoursToExpire = '+72 hour';

        //mailing token stuff...

        $mailingToken = Token::generate();

        $mailingSalt = Hash::salt(33);

        $hashedMailingToken = Hash::make($mailingToken, $mailingSalt);

        //unique token stuff...

        $uniqueCodeToken = substr(md5(uniqid(rand(), true)), -6);

        $uniqueCodeSalt = Hash::salt(33);

        $hashedUniqueToken = Hash::make($uniqueCodeToken, $uniqueCodeSalt);

        //check if exists...

        $pendingRegister = DB::getInstance()->query('SELECT * from users_recover_password WHERE email = ? AND active = ? AND deleted = ? AND verified = ? ', [$item->email, 1, 0, 0]);

        $arrayQuery = ['email' => $item->email, 'token' => $hashedMailingToken, 'salt' => $mailingSalt,

        'unique_token' => $hashedUniqueToken, 'unique_salt'=>$uniqueCodeSalt];

        if($pendingRegister->count() == 0){

          $addData = DB::getInstance()->insert('users_recover_password', $arrayQuery);

        }else{

          $arrayQuery["attempts"] = $pendingRegister->first()->attempts + 1;

          $addData = DB::getInstance()->update('users_recover_password', $pendingRegister->first()->id, $arrayQuery);

        }

        if(!$arrayQuery){

        }else{

          $status['status'] = 5;

          $status['msg'] = 'Enviamos un email a <b>' . Input::get('email') . '</b> con las instrucciones para recuperar tu contraseña.';

          $finalURL = 'http://localhost/lasalle/web/dist/admin/dist/recover.php?token=' . $mailingToken;

          //mailing...

          $emailer = new Emailer([$item->email], 'Recuperar contraseña');

          $template = new EmailTemplate('../mailer/templates/custom.php');

          $template->title = 'Recuperar contraseña';

          $inviteString = '<h1>Hola, <b>' . $item->name . ' ' . $item->surname . '</b>!</h1><br>';

          $inviteString .= '<p>Este es tu código de recuperación: <b>' . $uniqueCodeToken . '</b> <p><br>';

          $inviteString .= '<p>Para recuperar tu contraseña, cliqueá el siguiente botón y seguí las instrucciones:<p><br>';

          $inviteString .= '<a class="button-link button-color button button--green" target="_blank" style="color: #FFFFFF; text-decoration: none" href="'. $finalURL .'"><span style="color: #FFFFFF; text- decoration: none"><font color="#FFFFFF">IR AL LINK</font></span></a><br><br>';

          $inviteString .= '¿No podés abrir el link? copiá y pegá la siguiente URL en tu navegador:<br><br>';

          $inviteString .= $finalURL . '<br>';

          $template->bodyContent = $inviteString;

          $emailer->SetTemplate($template);

          $mailWasSent = $emailer->send();

        }

      }

    }else{

      $errors = array();

			foreach($validation->errors() as $error){ $errors[] = $error; }

			$status['status'] = 3;

			$status['msg'] = 'Error #1003: ' . $errors[0][0];

    }

  }else{

    $status['status'] = 2;

    $status['msg'] = "Error #1002: actualizá la página (F5) y volvé a intentarlo.";

  }

}else{

  $status['status'] = 1;

  $status['msg'] = "Error #1001: Comunicate con Aditivo Interactive Group S.A o quien te haya dado de alta en el sistema.";

}

echo json_encode($status);


?>
