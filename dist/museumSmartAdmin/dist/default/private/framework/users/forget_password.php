<?php

require_once 'core/init.php';

$status['status'] = null;

$db = DB::getInstance();

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='recover_password'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

$GLOBALS['testing'] = "mi testing";

if(Input::exists()){

    if(!Token::check(Input::get('token'))){ printData(1); }
}

if (Input::get('email_reset')) {

	$GLOBALS['user'] = new User(Input::get('email_reset'));

	$validate = new Validate();

	$validation = $validate->check($_POST,array('email_reset' => array('display' => 'Email', 'email' => true,

		'required' => true)));

	if($validation->passed()){

		//general settings...

		$settingsQ = $db->query("SELECT * FROM settings");

		$settings = $GLOBALS['settings'] = $settingsQ->first();

		//support settings...

		$supportSettingsQ = $db->query("SELECT * FROM support_settings");

		$GLOBALS['supportSettings'] = $supportSettingsQ->first();

		//museum settings...

		$museumSettingsQ = $db->query("SELECT * FROM museum_data");

		$museumSettings = $museumSettingsQ->first();

		//museum social media...

		$museumSocialMediaQ = $db->query("SELECT * FROM museum_social_media");

		$museumSocialMediaSettings = $museumSocialMediaQ->first();






		if($GLOBALS['user']->exists()){

			$db = DB::getInstance();

			$vericode = Helpers::getRandomString($GLOBALS['settings']->vericode_length);

			$vericode_expiry = date("Y-m-d H:i:s",

								strtotime("+$settings->reset_vericode_expiry minutes",

								strtotime(date("Y-m-d H:i:s"))));

			//$db->query("SELECT email FROM users WHERE username = ? AND logins > ?", [$name, $logins]);

			$getCode = $db->query("SELECT * FROM users_recover_password WHERE user_id = ?", 

				[$GLOBALS['user']->data()->id]);

			$count = $getCode->count();

			if($count > 0){

				$GLOBALS['testing'] = 'mayor a 0';

				$firstCode = $getCode->first();

				if($firstCode){

					$db->update('users_recover_password',

					$firstCode->id,

					['vericode' => $vericode,'vericode_expiry' => $vericode_expiry]);

				}

			}else{

				$insertCode = $db->insert("users_recover_password", 

					["user_id"=>$GLOBALS['user']->data()->id,

					"vericode"=>$vericode,

					"vericode_expiry"=>$vericode_expiry]);

			}

			//get URL for verifying process...

			$address = $museumSettings->address . ' ' . $museumSettings->address_number . ', ' . $museumSettings->zip_code . ' ' . $museumSettings->city;

			$db = DB::getInstance();

            $query = $db->query("SELECT verify_url FROM email WHERE label = 'recovery'");

            $results = $query->first();

            $recoverURL = $results->verify_url . '?vericode=' . $vericode;

            //make template replacements...

            $message = file_get_contents('../email_templates/password_reset.html');

            $message = str_replace('%projectTitle%', $museumSettings->title, $message);
            
            $message = str_replace('%expiry_time%',$GLOBALS['settings']->reset_vericode_expiry, $message);
			
			$message = str_replace('%username%', $GLOBALS['user']->data()->name, $message);
									
			$message = str_replace('%recoveryurl%', $recoverURL, $message);

			$message = str_replace('%ip%', Helpers::getIP(), $message);

			$message = str_replace('%os%', Helpers::getOS(), $message);
			
			$message = str_replace('%browser%', Helpers::getBrowser(), $message);
			
			$message = str_replace('%email%', $GLOBALS['supportSettings']->support_email, $message);
			
			$message = str_replace('%address%', $address, $message);

			$message = str_replace('%te%', $museumSettings->te, $message);

			$message = str_replace('%museumEmail%', $museumSettings->email, $message);

			$message = str_replace('%lat%', $museumSettings->lat, $message);

			$message = str_replace('%lon%', $museumSettings->lon, $message);

			$message = str_replace('%facebook%', $museumSocialMediaSettings->facebook, $message);

			$message = str_replace('%instagram%', $museumSocialMediaSettings->instagram, $message);

			$message = str_replace('%twitter%', $museumSocialMediaSettings->twitter, $message);

			$sent = Helpers::email($GLOBALS['user']->data()->email, utf8_decode('cambio de contraseña'), $message);

            if($sent){

            	UserHelpers::logger(

            		$GLOBALS['user']->data()->id, "User","Solicitó el cambio de contraseña.", 

            		"Solicistaste el cambio de contraseña", Helpers::getIP(), Helpers::getOS(), Helpers::getBrowser()
            	);

            	printData(4);
            
            }else{

            	printData(5);

            }

		}else{

			$message = file_get_contents('../email_templates/recover_missing_email.html');

			$message = str_replace('%ip%', Helpers::getIP(), $message);

			$message = str_replace('%os%', Helpers::getOS(), $message);
			
			$message = str_replace('%browser%', Helpers::getBrowser(), $message);

			$sent = Helpers::email(Input::get('email_reset'), 'solicitud de cambio de password', $message);

			($sent) ? printData(4) : printData(5);

		}

	}else{

		printData(3);

	}

}else{

	printData(2);
}

function printData($dataId){

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;
        	
	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	if($dataId == 4 || $dataId == 5){

		$changeEmail = ($dataId == 5) ? $GLOBALS['settings']->support_email :  Input::get('email_reset');

		$status['msg'] = str_replace('%email%', $changeEmail, 

			$GLOBALS['wordingArray'][$dataId]->string_value);
	
	}else{

		$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	}

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	echo json_encode($status);

	return;
}

?>