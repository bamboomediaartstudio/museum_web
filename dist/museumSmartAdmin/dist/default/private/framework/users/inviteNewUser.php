<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

require_once '../core/init.php';

$status['errors'] = false;

if(Input::exists()){

	if(Token::check(Input::get('token'))){

		$validate = new Validate();

		$validation = $validate->check($_POST, array('email' => array('display'=>'email', 'required' => true), 'name' => array('display'=>'nombre', 'required' => true, 'min'=>3),

    'surname' => array('display'=>'apellido', 'required' => true, 'min'=>3), 'group' => array('display'=>'grupo', 'required' => true)));

		if($validation->passed()){

      $user = new User();

      if($user->isLoggedin()){

        //ojo. Esta validación debería venir controlada desde cliente.
        //De todos modos nunca está de más...

        $count = DB::getInstance()->query('SELECT * from users WHERE email = ?', [Input::get('email')])->count();

        if($count == 0){

          $pendingCount = DB::getInstance()->query('SELECT * from users_pending_list WHERE email = ?', [Input::get('email')])->count();

          if($pendingCount == 0){

            $actualDate = date('Y-m-d H:i:s');

            $hoursToExpire = '+72 hour';

						$tokenToSend = Token::generate();

						$newSalt = Hash::salt(33);

						$hashedToken = Hash::make($tokenToSend, $newSalt);

						$unique = substr(md5(uniqid(rand(), true)), -6);

						$uniqueSalt = Hash::salt(33);

						$hashedUnique = Hash::make($unique, $uniqueSalt);

            $arrayQuery = ['name' => Input::get('name'), 'surname' => Input::get('surname'),'email' => Input::get('email'),

            'group_id' => Input::get('group'), 'hashed_token'=>$hashedToken, 'salt'=> $newSalt, 'hashed_unique'=>$hashedUnique, 'unique_salt'=>$uniqueSalt, 'invited_by'=>$user->data()->id];

            $insertNewUser = DB::getInstance()->insert('users_pending_list', $arrayQuery);

						//...

						if (!empty(Input::get('permissions'))) {

							$userIdArray = array();

							$permissionArray = array();

							$groupArray = array();

							foreach(Input::get('permissions') as $permission){

								$id = $permission[0];

								if(!empty($permission[1])){

									foreach($permission[1] as $detail){

										$userIdArray[] = DB::getInstance()->lastId();

										$groupArray[] = $id;

										$permissionArray[] = $detail;

									}

								}

							}

							DB::getInstance()->insert("users_pending_permissions", ["id_user"=>$userIdArray, "id_group"=>$groupArray, "id_permission"=>$permissionArray]);

						}

						//...

						$finalURL = 'http://localhost/lasalle/web/dist/admin/dist/register.php?token=' . $tokenToSend;

						if($insertNewUser){

							$emailer = new Emailer([Input::get('email')], 'Invitación');

							$template = new EmailTemplate('../mailer/templates/custom.php');

							$template->title = 'Invitación a administrador';

							$inviteString = '<h1>Hola, ' . Input::get('name') . ' ' . Input::get('surname') . '!</h1><br><p>' . $user->data()->name . ' ' . $user->data()->surname . ' te envió una invitación a unirte al administrador de contenidos del Museo LaSalle!<p>';

							$inviteString .= '<p>Para completar tu registro, hacé click en el siguiente botón.<p><br>';

							$inviteString .= '<a href="' . $finalURL . '"  class="button-link button-color button button--green" target="_blank"><span style="color: #FFFFFF; text- decoration: none"> Click aquí para confirmar</span></a><br><br>';

							$inviteString .= '¿No podés abrir el link? copiá y pegá la siguiente URL en tu navegador:<br>';

							$inviteString .= $finalURL . '<br>';

							$inviteString .= 'Este es tu código de seguridad de 6 caracteres que vas a tener que usar:<b>' . $unique .'</b>';

							$template->bodyContent = $inviteString;

							$emailer->SetTemplate($template);

							$mailWasSent = $emailer->send();

							if($mailWasSent){

								$user->addLogData('Invitaste a <b>' . Input::get('name') . ' ' . Input::get('surname') . '</b>.',

								'invitó a <b>' . Input::get('name') . ' ' . Input::get('surname') . '</b>.', 'brand', 'fas fa-users');

								$status['errors'] = false;

	          		$status['errorsList'][0] = 'correcto...';

							}else{

								$status['errors'] = false;

	          		$status['errorsList'][0] = 'fallo el envio....';

							}



            }else{

              $status['errors'] = true;

          		$status['errorsList'][0] = 'Ops...problema agregando nuevo usuario...';

            }

          }else{

            $status['errors'] = true;

        		$status['errorsList'][0] = 'Ya existe un usuario registrado con el email ' . Input::get('email') . ' en lista de espera. Tiene 72 hs. para complear el registro';

          }

        }else{

          $status['errors'] = true;

      		$status['errorsList'][0] = 'Ya existe un usuario registrado con el email ' . Input::get('email');

        }

      }else{

        $status['errors'] = true;

    		$status['errorsList'][0] = 'El usuario no está loggeado.';

      }


    }else{

    }

  }else{

    $status['errors'] = true;

    $status['errorsList'][0] = 'Fallo en el token de seguridad';


  }

}else{

  $errors = array();

  foreach($user->loginErrors() as $error){ $errors[] = $error; }

  $status['errors'] = true;

  $status['errorsList'] = $errors[0];

}

echo json_encode($status);


 ?>
