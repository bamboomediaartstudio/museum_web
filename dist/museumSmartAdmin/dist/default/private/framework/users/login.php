<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

require_once '../core/init.php';

$status['errors'] = false;

$query = DB::getInstance()->query('select * from dev_general_config')->first();

if(Input::exists()){

	if(Token::check(Input::get('token'))){

		$validate = new Validate();

		$validation = $validate->check($_POST, array( 'email' => array('display'=>'email', 'required' => true), 'password' => array('display'=>'contraseña', 'required' => true, 'min'=>3) ));

		if($validation->passed()){

			$user = new User();

			$remember = (Input::get('remember') === 'on') ? true : false;

			$login = $user->login(Input::get('email'), Input::get('password'), $remember);

			if($login){

				Cookie::put('confirmed-device', 1, time() + (10 * 365 * 24 * 60 * 60));

				$checker = new TFAChecker($user);

				$status['auth'] = $checker->check2FAStatus();

				if(json_decode($status['auth'])->status == false){

					$user->addLoginData();

					$user->addLogData('Iniciaste sesión.', 'inició sesión.', 'success', 'fas fa-user-shield');

				}

				Token::delete(Input::get('token'));

			}else{

				$errors = array();

				foreach($user->loginErrors() as $error){ $errors[] = $error; }

				$status['errors'] = true;

				$status['errorsList'] = $errors;

			}

		}else{

			$errors = array();

			foreach($validation->errors() as $error){ $errors[] = $error; }

			$status['errors'] = true;

			$status['errorsList'] = $errors;

		}

	}else{

		$status['errors'] = true;

		$status['errorsList'][0] = 'Error 0x0001.<br><br>Algo salió mal. Por favor, comunicate a <b><a href="info@aditivoestudio.org.ar">info@aditivoestudio.org.ar</a></b>';

	}
}

//echo...

echo json_encode($status);

?>
