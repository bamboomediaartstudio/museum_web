<?php

/**
* @summary change password...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

//check a toda la lsita de eventos...
//0 - error de formulario -> "Fallo el formulario. Por favor, volvé a intentarlo.";
//1 - Error en el token. -> Comunicate a info@aditivointeractivegroup.com
//2 - Error de validacion -> Error de validación. Por favor, revisá los campos.
//3 - Error de correo -> No encontramos la dirección de email en la base de datos. Por favor, corregí el correo y volvé a intentarlo.
//4 - Error en el codigo de seguridad -> El código de seguridad que ingresaste es incorrecto. Por favor, volvé a intentarlo.
//5 - Error en el mailing -> Falló el sistema de mailing
//6 - Error en el sistema de mailing -> Falló el sistema de mailing al referer
//7 - ok...


$status['status'] = 0;

$status['title'] = "";

$status['msg'] = "";

$deletePending = true;

require_once '../core/init.php';

if(Input::exists()){

  if(Token::check(Input::get('securityToken'))){

    $validate = new Validate();

		$validation = $validate->check($_POST, array(

      'email' => array('display'=>'email', 'valid_email'=>true, 'required' => true),

			'code' => array('display'=>'code','required' => true, 'min'=>6, 'max'=>6),

			'password' => array('display'=>'password', 'required' => true, 'min' => 8),

			'repeatpassword' => array('display'=>'repeatpassword', 'required' => true, 'min' => 8, 'matches' => 'password')

		));

    if($validation->passed()){

      $status['msg'] = 'paso la valideta...';

      $checkEmail = DB::getInstance()->query('SELECT * from users_pending_list WHERE email = ? AND active = ? AND deleted = ?', [Input::get('email'), 1, 0])->count();

      if($checkEmail == 0){

        $status['status'] = 3;

        $status['msg'] = 'No encontramos la dirección de email en la base de datos. Por favor, corregí el correo y volvé a intentarlo.';

      }else{

        $checkUnique = DB::getInstance()->query('SELECT * from users_pending_list WHERE email = ? AND active = ? AND deleted = ?', [Input::get('email'), 1, 0])->first();

        if($checkUnique->hashed_unique  == Hash::make(Input::get('code'), $checkUnique->unique_salt)){

          $saveSalt = Hash::salt(33);

          $pass = Hash::make(Input::get('password'), $saveSalt);

          $userquery = ['name' => $checkUnique->name, 'surname' => $checkUnique->surname,'email' => $checkUnique->email,

          'group_id' => $checkUnique->group_id, 'password'=>$pass, 'salt'=> $saveSalt, 'id_referer'=> $checkUnique->invited_by];

          $insertNewUser = DB::getInstance()->insert('users', $userquery);

          if($deletePending){

            $completeProcess = DB::getInstance()->deleteById('users_pending_list', $checkUnique->id);

            if($completeProcess){

              $emailer = new Emailer([Input::get('email')], 'Bienvenido! :)');

							$template = new EmailTemplate('../mailer/templates/custom.php');

              $template->title = 'Bienvenido!';

              $inviteString = '<h1>Hola, ' . $checkUnique->name . ' ' . $checkUnique->surname . '! :)</h1><br><p>';

              $inviteString .= 'Bienvenido al administrador. Recordá iniciar sesión siempre desde la siguiente  URL';

							$inviteString .= 'http://localhost/lasalle/web/dist/admin/dist/login.php <br>';

							$inviteString .= 'No compartas tu contraseña ni tus datos personales.';

							$template->bodyContent = $inviteString;

							$emailer->SetTemplate($template);

							$mailWasSent = $emailer->send();

              if(!$mailWasSent){

                $status['status'] = 5;

                $status['msg'] = 'Fallo el sistema de mailing. Por favor, volvé a intentarlo.';

              }else{

                $refererUser = DB::getInstance()->query('SELECT * from users WHERE id = ? AND active = ? AND deleted = ?', [$checkUnique->invited_by, 1, 0])->first();

                //...

                $sendEmailToReferer = DB::getInstance()->query('SELECT value from users_settings_subcategories_options as usso INNER JOIN users_settings_subcategories as uss ON usso.id_subcategory = uss.id WHERE usso.id_user = ? AND uss.identifier = ?', [$checkUnique->invited_by, 'on_new_user_notification_sent'])->first();

                if($sendEmailToReferer->value == 1){

                  $user = new User($refererUser->email);

                  $emailer = new Emailer([$refererUser->email], $checkUnique->name . ' se dio de alta! ');

    							$template = new EmailTemplate('../mailer/templates/custom.php');

                  $template->title = $checkUnique->name . ' se dio de alta!';

                  $inviteString = '<h1>Hola, ' . $refererUser->name . '! :)</h1><br><p>';

                  $inviteString .= 'El usuario ' . $checkUnique->name . ' ' . $checkUnique->surname . ' a quien invitaste al ABM, se acaba de dar de alta.<br>';

                  $inviteString .= 'Está atento y preparado por si te solicita ayuda. Definir contenido del email.';

    							$template->bodyContent = $inviteString;

    							$emailer->SetTemplate($template);

    							$emailToRefererWasSent = $emailer->send();

                  if($emailToRefererWasSent){

                    $user->addLogData($checkUnique->name . ' ' . $checkUnique->surname . 'se unió.', $checkUnique->name . ' ' . $checkUnique->surname . 'se unió.', 'success', 'fas fa-hat-wizard');

                    $newUser = new User();

                    $login = $newUser->login(Input::get('email'), Input::get('password'));

                    if($login){

                      Cookie::put('confirmed-device', 1, time() + (10 * 365 * 24 * 60 * 60));

                      $newUser->addLoginData();

                      $newUser->addLogData('Iniciaste sesión.', 'inició sesión.', 'success', 'fas fa-user-shield');

                      $status['status'] = 7;

                      $status['msg'] = 'Registro exitoso! :) redirigiendo...';

                    }


                  }else {

                    $status['status'] = 6;

                    $status['msg'] = 'Falló el sistema de mailing al referer. Por favor, volvé a intentarlo.';

                  }

                }else{

                  $newUser = new User();

                  $login = $newUser->login(Input::get('email'), Input::get('password'));

                  if($login){

                    Cookie::put('confirmed-device', 1, time() + (10 * 365 * 24 * 60 * 60));

                    $newUser->addLoginData();

                    $newUser->addLogData('Iniciaste sesión.', 'inició sesión.', 'success', 'fas fa-user-shield');

                    $status['status'] = 7;

                    $status['msg'] = 'Registro exitoso! redirigiendo...';

                  }


                }

              }

            }

          }

        }else{

          $status['status'] = 4;

          $status['msg'] = 'El código de seguridad que ingresaste es incorrecto. Por favor, volvé a intentarlo.';

        }

      }

    }else{

      $errors = array();

			foreach($validation->errors() as $error){ $errors[] = $error; }

			$status['status'] = 2;

			$status['title'] = "Error de validación. Por favor, revisá los campos.";

			$status['msg'] = $errors[0][0];

			$status['type'] = "error";

    }

  }else{

    $status['status'] = 1;

    $status['msg'] = "Error en el token. Comunicate a info@aditivointeractivegroup.com";
  }

}else{

  $status['status'] = 0;

  $status['msg'] = "Fallo el formulario. Por favor, volvé a intentarlo.";

}

echo json_encode($status);

 ?>
