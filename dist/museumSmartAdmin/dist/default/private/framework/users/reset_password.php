<?php

require_once 'core/init.php';

$status['0'] = 0;

$db = DB::getInstance();

$vericode = Input::get('vericode');

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='reset_password'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

//settings...

$settingsQ = $db->query("Select * FROM settings");

$settings = $GLOBALS['settings'] = $settingsQ->first();

//...


if(!Input::exists()) printData(0);


if(!Token::check(Input::get('token'))) printData(1);

if(!$vericode || !isset($vericode) || strlen($vericode) !=$settings->vericode_length) printData(3);

if (!Input::get('email')) printData(2);

else{

	$GLOBALS['user'] = new User(Input::get('email'));

	$validate = new Validate();

	$validation = $validate->check($_POST,array(

		'email' => array('display' => 'Email', 'email' => true,'required' => true),

		'newpassword' => array('display' => 'Contraseña', 'required' => true),
		
		'repeatnewpassword' => array('display' => 'Contraseña', 'required' => true, 'matches' => 'newpassword')

	));

	if(!$validation->passed()) printData(5);

	else{

		if($GLOBALS['user']->data()->vericode != $vericode) printData(6);

		if(strtotime($GLOBALS['user']->data()->vericode_expiry) - strtotime(date("Y-m-d H:i:s")) <= 0) printData(7);

		else{

			$GLOBALS['user']->update(array(

				'password' => password_hash(Input::get('newpassword'), PASSWORD_BCRYPT, array('cost' => 12)),

				'vericode' => helpers::getRandomString($GLOBALS['settings']->vericode_length),

				'vericode_expiry' => date("Y-m-d H:i:s")

			), $GLOBALS['user']->data()->id);

		//logger($ruser->$GLOBALS['user']->id, "User", "Reset password.", "reseteaste tu contraseña.");
			
			printData(4);

		/*if($GLOBALS['user']->data()->vericode != $vericode || (strtotime($ruser->data()->vericode_expiry) - strtotime(date("Y-m-d H:i:s")) <= 0)){
				Redirect::to($us_url_root.'users/forgot_password_reset.php?err=Something+went+wrong.+Please+try+again.');
			}


			printData(4);*/


		}

	}

}



/*if (Input::get('email')) {

	$GLOBALS['user'] = new User(Input::get('email'));

	$validate = new Validate();

	$validation = $validate->check($_POST,array(

		'email' => array('display' => 'Email', 'email' => true,'required' => true),

		'newpassword' => array('display' => 'Contraseña', 'required' => true),
		
		'repeatnewpassword' => array('display' => 'Contraseña', 'required' => true, 'matches' => 'newpassword')

	));

	if($validation->passed()){

		printData(4);
//	
	}else{

		printData(2);


	}

}*/

function printData($dataId){

	$status['token'] = Input::get('token');
	
	//$status['veryverygoodverycode'] = Input::get('vericode');

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	echo json_encode($status);

	exit();
}



/*



if (Input::get('email_reset')) {

	$GLOBALS['user'] = new User(Input::get('email_reset'));

	$validate = new Validate();

	$validation = $validate->check($_POST,array('email_reset' => array('display' => 'Email', 'email' => true,

		'required' => true)));

	if($validation->passed()){

		$settingsQ = $db->query("Select * FROM settings");

		$settings = $GLOBALS['settings'] = $settingsQ->first();

		if($GLOBALS['user']->exists()){

			$db = DB::getInstance();

			$vericode = Helpers::getRandomString(15);

			$vericode_expiry = date("Y-m-d H:i:s",

								strtotime("+$settings->reset_vericode_expiry minutes",

								strtotime(date("Y-m-d H:i:s"))));

			$db->update('users',$GLOBALS['user']->data()->id,['vericode' => $vericode,'vericode_expiry' => $vericode_expiry]);

			//get URL for verifying process...

			$db = DB::getInstance();

            $query = $db->query("SELECT verify_url FROM email WHERE label = 'recovery'");

            $results = $query->first();

            $recoverURL = $results->verify_url . '/?tonyToken=' . $vericode;

            //make template replacements...

            $message = file_get_contents('../email_templates/recover.html');
			
			$message = str_replace('%username%', $GLOBALS['user']->data()->name, $message);
									
			$message = str_replace('%recoveryurl%', $recoverURL, $message);

			$message = str_replace('%ip%', Helpers::getIP(), $message);

			$message = str_replace('%os%', Helpers::getOS(), $message);
			
			$message = str_replace('%browser%', Helpers::getBrowser(), $message);
			
			$message = str_replace('%email%', $GLOBALS['settings']->support_email, $message);

            $sent = Helpers::email($GLOBALS['user']->data()->email, 'cambio de password', $message);

            if($sent){

            	UserHelpers::logger(

            		$GLOBALS['user']->data()->id, "User","Solicitó el cambio de contraseña.", 

            		"Solicistaste el cambio de contraseña", Helpers::getIP(), Helpers::getOS(), Helpers::getBrowser()
            	);

            	printData(4);
            
            }else{

            	printData(5);

            }

		}else{

			$message = file_get_contents('../email_templates/recover_missing_email.html');

			$message = str_replace('%ip%', Helpers::getIP(), $message);

			$message = str_replace('%os%', Helpers::getOS(), $message);
			
			$message = str_replace('%browser%', Helpers::getBrowser(), $message);

			$sent = Helpers::email(Input::get('email_reset'), 'solicitud de cambio de password', $message);

			($sent) ? printData(4) : printData(5);

		}

	}else{

		printData(3);

	}

}else{

	printData(2);
}
*/

?>
