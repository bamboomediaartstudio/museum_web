<?php

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

require_once 'core/init.php';

require '../libraries/claviska/SimpleImage.php';

$status['status'] = 0;

//$_db;

//$this->_db = DB::getInstance();

$user = new User();

$filesManager = new FilesManager();

$ds = DIRECTORY_SEPARATOR;

$uniqueId = uniqid();

$image = new \claviska\SimpleImage();

$originalPrefix = '_original';

$squarePrefix = '_sq';

$defaultPrefix = '_default';

if(!$user->isLoggedIn()){

	Redirect::to('index.php');

}else{

	$userId = $user->data()->id;

}

$fileSize = Config::get('users/users_profile_picture_max_size');

$userFolder = Config::get('users/users_profile_picture_folder') .  $userId;

$deleteFolder = Config::get('users/users_profile_picture_folder') .  $userId . '/delete';

$fileFolder = $userFolder . '/' . $uniqueId;

//check for input...

if(Input::exists()){

	$status['status'] = 1;

	//check for token...

	if(Token::check(Input::get('token'))){

		$status['status'] = 2;

		$status['token'] = Input::get('token');

		//check for USER directory...

		$userFolderExists = $filesManager->checkDirectory($userFolder);

		if(!$userFolderExists){

			$userFolderCreated = $filesManager->makeDirectory($userFolder);

			$deleteFolder = $filesManager->makeDirectory($deleteFolder);

		}

		$folderExists = $filesManager->checkDirectory($fileFolder);

		$status['folderExists'] = $folderExists;

		if(!$folderExists){

			$folderCreated = $filesManager->makeDirectory($fileFolder);

		}else{

			$folderCreated = true;

		}

		$status['folderCreated'] = $folderCreated;

		if($folderCreated){

			$checkPHPExtensions = $filesManager->checkPHPExtensions();

			$status['checkPHPExtensions'] = $checkPHPExtensions;
		}

		//si llego hasta aca, estan hechas las validaciones de caracter general. Ahora vienen las individuales.

		if($checkPHPExtensions){

			if (!empty($_FILES)) {

				$tempFile = $_FILES['file']['tmp_name'];

				if(is_file($tempFile)){

					$checkFileSize = $filesManager->checkFileSize($tempFile, $fileSize);

					$status['checkFileSize'] = $checkFileSize;

					if($checkFileSize){

						$status['checkFileSize'] = $checkFileSize;

						$status['fileSize'] = $filesManager->getFileSize();

						$targetPath = dirname( __FILE__ ) . $ds . $fileFolder . $ds;

						$name = $userId . '_' . $uniqueId;

						$checkMimeType = $filesManager->checkImageMimeType($_FILES['file']['tmp_name']);

						if($checkMimeType){

							$status['checkMimeType'] = $checkMimeType;

							$status['mimeType'] = $filesManager->getMimeType();

							$extension = $filesManager->getFileExtensionByMimeType($filesManager->getMimeType());

							$status['extension'] = $extension;

							$targetFile =  $targetPath . $name . $originalPrefix  . '.' . $extension;

							$targetFileSQ =  $targetPath . $name . $squarePrefix . '.' . $extension;

							$targetFileDefault =  $targetPath . $name . $defaultPrefix . '.' . $extension;

							move_uploaded_file($tempFile, $targetFile);

							$image->fromFile($targetFile)->thumbnail(200, 200, 100)

							->toFile($targetFileSQ, $filesManager->getMimeType(), 100);

							$image->fromFile($targetFile)->bestFit(800, 800)

							->toFile($targetFileDefault, $filesManager->getMimeType(), 100);

							//....

							$q = DB::getInstance()->query("SELECT unique_id, id FROM users_profile_image WHERE user_id = ?",

								array($userId));

							$c = $q->count();

							if($c < 1){

								DB::getInstance()->insert('users_profile_image', array('user_id' => $userId,

									'unique_id' => $uniqueId, 'mimetype'=> $filesManager->getMimeType()));

							}else{

								$previous = $q->first()->unique_id;

								$status['temp'] = $q->first();

								$status['previous'] = $pre = $userFolder . '/' . $previous;

								$status['delete'] = $new = $deleteFolder . '/' . $previous;

								rename($pre, $new);


								DB::getInstance()->update('users_profile_image', $q->first()->id,

									array('unique_id' => $uniqueId,

										'mimetype' => $filesManager->getMimeType(),

										'uploaded'=>date('Y-m-d H:i:s')));

							}

							$date = date("Y-m-d H:i:s");

							DB::getInstance()->insert('logs',['log_date' => $date,'user_id' => $userId,

						'log_type' => "user",'log_note' => "User uploaded a new image."]);

							$status['jsonstatus'] = "uploaded";

						}
					}
				}
			}
		}
	}
}

echo json_encode($status);

die();

?>
