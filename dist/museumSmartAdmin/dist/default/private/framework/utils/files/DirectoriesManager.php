<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

/**
* DirectoriesManager
* @description									-						management for directories...
*/

class DirectoriesManager{

	/**
	* checkDirectory();
	* @description									-					 check if a directory exists
	* @param $directory 						-					 the name of the directory...
	*/

	public static function checkDirectory($directory){ return (!file_exists($directory)) ? false : true; }

	/**
	* makeDirectory();
	* @description									-					 create a new directory...
	* @param $directory 						-					 the name of the directory...
	* @param $mode 									-					 set 0777 as default...
	* @param $recursive 						-					 recursive to true...
	*/

	public static function makeDirectory($directory, $mode = 0777, $recursive = true){ return mkdir($directory, $mode, $recursive); }

	/**
	* checkIfFileExists();
	* @description									-					 check if file exists....
	* @param $fileName	 						-					 the file to check...
	* @return {boolean} 						-					 true / false...
	*/

	public static function checkIfFileExists($file){ return file_exists($file); }

	/**
	* moveFile($from, $to);
	* @description									-					 move a file from one directory to another...
	* @param $from			 						-					 origin...
	* @param $to				 						-					 destiny...
	* @return {boolean} 						-					 true / false...
	*/

	public static function moveFile($from, $to){ return rename($from, $to); }

	/**
	* uploadFile($file, $to)
	* @description									-					 upload file to server...
	* @param $file			 						-					 the file to upload...
	* @param $to				 						-					 the dir...
	* @return {boolean} 						-					 true / false...
	*/

	public static function uploadFile($file, $to){

		return move_uploaded_file($file, $to);

	}


}
