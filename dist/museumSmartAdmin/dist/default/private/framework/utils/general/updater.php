<?php

/**
* @summary Manage all the updates for the ABM.
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

//error # 2001 - status: 1 - type: danger - fallo el input.                     - Error #2001 - Fallo en el formulario. Por favor, volvé a intentarlo.
//error # 2002 - status: 2 - type: danger - fallo el token.                     - Error #2002 - Error en el formulario. Recargá la página (f5) y volvé a intentarlo.
//error # 2003 - status: 3 - type: danger - fallo el token.                     - Error #2003 - No estás loggeado en el sistema.
//error # 2004 - status: 4 - type: danger - fallo el variables.                 - Error #2004 - Fallo de validación.
//error # 2005 - status: 5 - type: danger - fallo en el query.                  - Error #2005 - Fallo la acción. Por favor, volvé a intentarlo.
//success # 2006 - status: 6 - type: success - todo ok...                       - Se actualizaron los datos.

require_once '../../../users/core/init.php';

$status['status'] = 0;

$status['errors'] = true;

$status['type'] = 'danger';

$status['msg'] = '';

$status['title'] = '';

//...

if(Input::exists()){

  if(Token::check(Input::get('token'))){

    $user = new User();

    if($user->isLoggedin()){

      $validate = new Validate();

      $validation = $validate->check($_POST, array(

        'id' => array('display'=> 'id', 'required' => true),

        'table' => array('display'=> 'table', 'required' => true),

        'row' => array('display'=> 'source', 'required' => true),

        'value' => array('display'=> 'value')));

      if($validation->passed()){

        $updated = DB::getInstance()->update(Input::get('table'),Input::get('id'), array(Input::get('row') => Input::get('value')));

        if($updated){

          $status['status'] = 6;

          $status['errors'] = false;

          $status['type'] = 'success';

          $status['title'] = 'Datos actualizados.';

          $status['msg'] = 'Se actualizaron los datos correctamente.';

        }else{

          $status['status'] = 5;

          $status['errors'] = true;

          $status['type'] = 'danger';

          $status['title'] = 'Error #2005';

          $status['msg'] = 'Fallo la acción. Por favor, volvé a intentarlo.';

        }

      }else{

        $status['status'] = 4;

        $status['errors'] = true;

        $status['type'] = 'danger';

        foreach($validation->errors() as $error){ $errors[] = $error; }

        $status['title'] = 'Error #2004';

        $status['msg'] = 'Error de validación.' . $errors[0];

      }

    }else{

      $status['status'] = 3;

      $status['errors'] = true;

      $status['type'] = 'danger';

      $status['title'] = 'Error #2003';

      $status['msg'] = 'No estás loggeado en el sistema.';

    }

  }else{

    $status['status'] = 2;

    $status['errors'] = true;

    $status['type'] = 'danger';

    $status['title'] = 'Error #2002';

    $status['msg'] = 'Error en el formulario. Recargá la página (f5) y volvé a intentarlo.';

  }

}else{

  $status['status'] = 1;

  $status['errors'] = true;

  $status['type'] = 'danger';

  $status['title'] = 'Error #2000';

  $status['msg'] = 'Fallo en el formulario.';

}

echo json_encode($status);

die();

?>
