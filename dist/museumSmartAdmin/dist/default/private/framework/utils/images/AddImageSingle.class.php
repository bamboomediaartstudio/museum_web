<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

require '../../../libraries/claviska/SimpleImage.php';

require_once '../files/DirectoriesManager.php';


/**
* AddImageSingle								-
* @param $this->_user									-						receive the user. If null, check session...
*/

class AddImageSingle{

  private $ds = DIRECTORY_SEPARATOR;

  private $_imgInputName = '';

  private $_resizeImage = '';

  private $_userDirectory = '';

  private $_maxFileSize;

  private $_minWidth;

  private $_minHeight;

  private $_sizeIsStrict;

  private $_user;

  /**
  *  __construct									-						constructor...
  * @description 									-						manage all about users...
  * @param $imgInputName					-						the image to processs...
  */

public function __construct($imgInputName, $maxFileSize, $minWidth, $minHeight, $sizeIsStrict) {

    $this->_imgInputName = $imgInputName;

    $this->_maxFileSize = $maxFileSize;

    $this->_minWidth = $minWidth;

    $this->_minHeight = $minHeight;

    $this->_sizeIsStrict = ($sizeIsStrict == 1) ? true : false;

    $this->_user = new User();


  }

  /**
  *  uploadImage
  * @description 									-						upload a new image to the server...
  * @param $type									-						the type of image...
  */

  public function uploadImage($_mainDirectory = null, $source = "Dashboard"){

    $clientImage = $this->_imgInputName;

    $file_path = $clientImage['tmp_name'];

    $mainDirectory = $_mainDirectory;

    $this->_userDirectory = $mainDirectory . $this->_user->data()->id;

    $this->_userDeleteDirectory = $mainDirectory . $this->_user->data()->id . '/deleted';

    $fileName = $this->_user->data()->id  . '.jpeg';

    if(!$this->_user->isLoggedIn()) throw new Exception("Ops..! Ocurrió un error de autenticación de usuario. Por favor, volvé a intentarlo. Si el error persiste, cerrá sesión en el admin y loggeate nuevamente", 1);

    if(DirectoriesManager::checkDirectory($mainDirectory) == false) throw new Exception("No existe el directorio principal. Algo salió mal. Comunicate.", 2);

    if(DirectoriesManager::checkDirectory($this->_userDirectory) == false){

      if(DirectoriesManager::makeDirectory($this->_userDirectory) == false) throw new Exception("Ocurrió un error durante la creación de directorios", 3);

      if(DirectoriesManager::makeDirectory($this->_userDeleteDirectory) == false) throw new Exception("Ocurrió un error durante la creación de directorios", 3);

    }

    $filesManager = new FilesManager();

    if(!$filesManager->checkPHPExtensions()) throw new Exception("checkPHPExtensions???", 4);

    if(empty($_FILES)) throw new Exception("FILES is empty", 5);

    if(!is_file($file_path)) throw new Exception("it is not a file", 6);

     if($source != "Webcam") if(!$filesManager->checkImageDimensions($file_path, $this->_minWidth, $this->_minHeight, $this->_sizeIsStrict)) throw new Exception("Aspect ratio problem.", 7);

    if(!$filesManager->checkFileSize($file_path, $this->_maxFileSize)) throw new Exception("El peso de la imagen es mayor al permitido.", 7);

    if(!$filesManager->checkImageMimeType($file_path)) throw new Exception("hay un problema con el mimetype", 8);

    $this->_resizeImage = $this->_userDirectory . DIRECTORY_SEPARATOR . $fileName;

    if (DirectoriesManager::checkIfFileExists($this->_resizeImage)) DirectoriesManager::moveFile($this->_resizeImage, $this->_userDeleteDirectory . DIRECTORY_SEPARATOR . uniqid() . '_' . $this->_user->data()->id  . '.jpeg');

    if (DirectoriesManager::uploadFile($file_path, $this->_resizeImage) == false) throw new Exception("problem uploading file!", 9);
  }

  /**
  * resizeImage
  * @description 									-						resize the original image...
  * @param $type									-						the type of image...
  */

  public function resizeImage($width, $height, $isSquare = true, $prefix = '_sq', $extension = "jpeg", $mimeType = "image/jpeg", $quality = 100){

    $image = new \claviska\SimpleImage();

    $img = $this->_userDirectory . DIRECTORY_SEPARATOR . $this->_user->data()->id .  '_' . $prefix . '.' . $extension;

    $msg;

    if($isSquare == true){

      $saveImage = $image->fromFile($this->_resizeImage)->thumbnail($width, $height, $width/2)->toFile($img, $mimeType, $quality);

      $msg = 'Falló la creación del thumbnail.';

    }else{

      $saveImage = $image->fromFile($this->_resizeImage)->resize($width)->toFile($img, $mimeType, $quality);

      $msg = 'Falló la creación de una miniatura.';

    }

    if(!$saveImage) throw new Exception($msg, 0);

  }
}

?>
