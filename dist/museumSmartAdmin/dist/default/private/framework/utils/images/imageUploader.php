<?php

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

require_once '../../core/init.php';

require_once 'AddImageSingle.class.php';

$status['status'] = 0;

$status['server_data_type'] = Input::get('server_data_type');

$status['tester'] = 'test';

$status['test'] = $_FILES['my_file']['tmp_name'];

$status['file'] = $_FILES['my_file'];

$status['source'] = Input::get('source');

//get all the image properties...

$user = new User();

if($user->isLoggedin()){

  $pictureData = DB::getInstance()->query('SELECT * from dev_pictures_data WHERE identifier = ?', [Input::get('server_data_type')])->first();

  $status['data'] = $pictureData->path;

  //create a new image...

  $image = new AddImageSingle($_FILES['my_file'], $pictureData->max_size, $pictureData->min_width, $pictureData->min_height, $pictureData->size_is_strict);

  try {

    $image->uploadImage($pictureData->path, Input::get('source'));

    $status['msg'] = "fue bien";

  }catch(Exception $e) {

    $status['msg'] =  $e->getMessage();

    $status['status']+=1;

  }

  if($pictureData->use_thumbnail == 1){

    try { $image->resizeImage($pictureData->thumbnail_size, $pictureData->thumbnail_size, true, 'sq'); }catch(Exception $e){ $status['msg'] =  $e->getMessage(); $status['status']+=1; }

  }

  if($pictureData->use_small == 1){

    try { $image->resizeImage($pictureData->small_width, $pictureData->small_width, false, 'sm'); }catch(Exception $e){ $status['msg'] =  $e->getMessage();  $status['status']+=1; }

  }

}

$user->addLogData('Cambiaste tu imagen de perfil.', 'cambió su imagen de perfil.', 'danger', 'fas fa-camera');

echo json_encode($status);

die();


?>
