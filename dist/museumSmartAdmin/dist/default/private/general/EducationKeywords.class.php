<?php

class EducationKeywordsClass{

  private $id;
  
  private $name;
  
  private $active;
  
  private $deleted;

  public function __construct(){ }

  /**
  * @function checkExist
  * @description Check with name if keyword exists
  *
  * @param {string} $name 	- 	 keyword name
  * @return 0 not exist / 1 exist
  */

  public function checkExist($name){

    $finded = 0;

    $getExistsQuery = DB::getInstance()->query("SELECT count(*) as cantFinded, id as idKeyword FROM museum_education_keywords WHERE name = ? AND deleted = ?", array($name, 0));

    $keywordsResult = $getExistsQuery->results(true);

    if($keywordsResult[0]["cantFinded"] > 0){

      $finded = $keywordsResult[0]["idKeyword"];

    }else{

      $finded = 0;

    }

    return $finded;

  }

  /**
  * @function addKeyword
  * @description add keyword
  *
  * @param {string} $name 	- 	 keyword name
  * @return 0 Error / lastId added
  */

  public function addKeyword($name){

    $inserted = 0;

    $arrayKeywordsQuery = [

      'name'    => $name,

      'active'  => 1,

      'deleted' => 0
    ];

    $db = DB::getInstance();

    $insertKeywordQuery = $db->insert('museum_education_keywords', $arrayKeywordsQuery);

    if($insertKeywordQuery){

      return $db->lastId();

    }else{

      throw new Exception("Error al agregar keyword a db", 0);

    }

  }

  public function addRelation($idModule, $idKeyword){

    $inserted = 0;

    $arrayKeywordsQuery = [

      'id_module'   => $idModule,

      'id_keyword'  => $idKeyword,

      'active'      => 1,

      'deleted'     => 0

    ];

    $db = DB::getInstance();

    $insertRelationQuery = $db->insert('museum_education_modules_keywords_relations', $arrayKeywordsQuery);

    return ($insertRelationQuery) ? 1 : 0;

  }

  public function hideKeywordRelation($idRelation){
    
  }

}

?>
