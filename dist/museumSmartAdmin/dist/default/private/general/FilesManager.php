<?php

class FilesManager{

	private $_mtype;

	private $_fileSize;

	public function __construct(){

	}

	public function checkDirectory($directory){

		return (!file_exists($directory)) ? false : true;
	}

	public function makeDirectory($directory){

		return mkdir($directory, 0777, true);

	}

	public function createHtaccess($folder){

		if (!file_exists($folder."/.htaccess")){

			try {
				
				$file = fopen($folder."/.htaccess","w");
				
				$txt = "order deny,allow\n";
				
				$txt .= "deny from all\n";
				
				$txt .="allow from 127.0.0.1";
				
				fwrite($file, $txt);
				
				fclose($file);
				
				return true;
			
			} catch (Exception $e) {
			
				return false;
			
			}
		
		} else {
		
			return true;
	
		}
	}

	/* Checks if required PHP extensions are loaded. Tries to load them if not */

	public function checkPHPExtensions(){

		if (!extension_loaded('fileinfo')) {
		
			if(function_exists('dl')){
		
				if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
		
					if (!dl('fileinfo.dll')) {
		
						return false;
		
					} else {
		
						return true;
		
					}
		
				} else {
		
					if (!dl('fileinfo.so')) {
		
						return false;
		
					} else {
		
						return true;
		
					}
		
				}
		
			} else {
		
				return false;
		
			}
		
		} else {
		
			return true;
		
		}
	
	}

	public function checkImageMimeType($tmpname){

		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		
		$mtype = finfo_file($finfo, $tmpname);

		$this->_mtype = $mtype;
		
		if(strpos($mtype, 'image/') === 0){

			return true;
		} else {
			
			return false;
		
		}
		
		finfo_close($finfo);
	}

	public function getMimeType(){ return $this->_mtype; }

	public function getFileExtensionByMimeType($mimeType){

		 $extensions = array('image/jpeg' => 'jpeg',

		 	'text/xml' => 'xml',

		 	'image/png' => 'png', 

		 	'image/gif' => 'gif'
		 	 );

		 return $extensions[$mimeType];
	}

	public function checkFileSize($file, $size){

		$this->_fileSize = filesize($file);

		if($this->_fileSize > $size){
			
			return false;
		
		}else{ 

			return true;

		}

	}

	public function getFileSize(){

		return $this->_fileSize;
	}
}
