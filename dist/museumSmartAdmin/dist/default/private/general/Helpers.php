<?php

/**
* @summary All the helpers for the PHP side.
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}
*
* @todo Complete documentation.
*/

use PHPMailer\PHPMailer\PHPMailer;

use PHPMailer\PHPMailer\Exception;

require 'phpmailer/Exception.php';

require 'phpmailer/PHPMailer.php';

require 'phpmailer/SMTP.php';

/**
* @class Helpers
* @description all the helpers that we ll need.
*/

class Helpers{

	/**
	* @method getRandomString
	* @description generate a random considering the param lenght
	*
	* @param {int} $len             		- The length of the random string.
	* @return {string} $string             	- The generated string.
	*/

	public static function getRandomString($len){

		$len = $len++;

		$string = "";

		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for($i=0;$i<$len;$i++)

			$string.=substr($chars,rand(0,strlen($chars)),1);

		return $string;
	}

	/**
	* @method getIP();
	* @description get the user IP
	*
	* @return {string} $ip             		- String with the IP address of the user.
	*/

	public static function getIP(){

		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {

			$ip = $_SERVER['HTTP_CLIENT_IP'];

		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {

			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

		} else {

			$ip = $_SERVER['REMOTE_ADDR'];

		}

		return $ip;

	}

	/**
	* @method getOS();
	* @description get the user OS
	*
	* @return {string} $os_platform         - String with the OS.
	*/

	public static function getOS() {

		$user_agent = $_SERVER['HTTP_USER_AGENT'];

		$os_platform  = "Unknown OS Platform";

		$os_array     = array(
			'/windows nt 10/i'      =>  'Windows 10',
			'/windows nt 6.3/i'     =>  'Windows 8.1',
			'/windows nt 6.2/i'     =>  'Windows 8',
			'/windows nt 6.1/i'     =>  'Windows 7',
			'/windows nt 6.0/i'     =>  'Windows Vista',
			'/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
			'/windows nt 5.1/i'     =>  'Windows XP',
			'/windows xp/i'         =>  'Windows XP',
			'/windows nt 5.0/i'     =>  'Windows 2000',
			'/windows me/i'         =>  'Windows ME',
			'/win98/i'              =>  'Windows 98',
			'/win95/i'              =>  'Windows 95',
			'/win16/i'              =>  'Windows 3.11',
			'/macintosh|mac os x/i' =>  'Mac OS X',
			'/mac_powerpc/i'        =>  'Mac OS 9',
			'/linux/i'              =>  'Linux',
			'/ubuntu/i'             =>  'Ubuntu',
			'/iphone/i'             =>  'iPhone',
			'/ipod/i'               =>  'iPod',
			'/ipad/i'               =>  'iPad',
			'/android/i'            =>  'Android',
			'/blackberry/i'         =>  'BlackBerry',
			'/webos/i'              =>  'Mobile'
		);

		foreach ($os_array as $regex => $value){

			if (preg_match($regex, $user_agent)){ $os_platform = $value; }

		}

		return $os_platform;
	}

	/**
	* @method isLocalhost();
	* @description Simple check to know if we are in localhost.
	*
	* @return {bool} true/false             - Boolean to indicate if we are in localhost.
	*/

	public static function isLocalhost() {

		if($_SERVER["REMOTE_ADDR"]=="127.0.0.1" || $_SERVER["REMOTE_ADDR"]=="::1"  || $_SERVER["REMOTE_ADDR"]=="localhost"){

			return true;

		}else{

			return false;

		}

	}

	/**
	* @method getBrowser();
	* @description get the user Browser.
	*
	* @return {string} $browser             - String with the browser name.
	*/

	public static function getBrowser() {

		$user_agent = $_SERVER['HTTP_USER_AGENT'];

		$browser = "Unknown Browser";

		$browser_array = array(

			'/msie/i'      => 'Internet Explorer',

			'/firefox/i'   => 'Firefox',

			'/safari/i'    => 'Safari',

			'/chrome/i'    => 'Chrome',

			'/edge/i'      => 'Edge',

			'/opera/i'     => 'Opera',

			'/netscape/i'  => 'Netscape',

			'/maxthon/i'   => 'Maxthon',

			'/konqueror/i' => 'Konqueror',

			'/mobile/i'    => 'Handheld Browser'

		);

		foreach ($browser_array as $regex => $value){

			if (preg_match($regex, $user_agent)){ $browser = $value; }

		}

		return $browser;

	}

	/**
	* @method email
	* @description send the emails from here...
	*
	* @param {string} $to             		- The recipient.
	* @param {string} $subject             	- The subject of the email.
	* @param {string} $body             	- The body.
	* @param {!object} $opts             	- Option to send the data in an object.
	* @param {!string} $attachment          - The attachments.
	*
	* @return {boolean} $result 			- Boolean indicating if the email was sent.
 	*/

	public static function email($to, $subject, $body, $opts=[], $attachment=false){

		$mail = new PHPMailer(true);

		$mail->CharSet = "UTF-8";

		$mail->IsSMTP();

		$mail->SMTPDebug = 0;

		$mail->SMTPAuth = true;

		$mail->SMTPSecure = 'tls';

		$mail->SMTPAutoTLS = false;

		$mail->Host = 'smtp.gmail.com';

		$mail->Port = 587;

		$mail->Username = 'museodelholocausto.recovery@gmail.com';

		$mail->Password = 'antonietaFrancesca';

		$mail->From = 'info@museodelholocausto.org.ar';

		$mail->FromName = 'Museo Del Holocausto';

		$mail->AddReplyTo($to,'reply');

		$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';

		$mail->addAddress(rawurldecode($to));

		$mail->addBCC('museodelholocausto.recovery@gmail.com');

		$mail->isHTML(true);

		$mail->Subject = $subject;

		$mail->Body = $body;

		$result = $mail->send();

		return $result;
	}

	public static function timeElapsedString($datetime, $full = false) {

		$now = new DateTime;

		$ago = new DateTime($datetime);

		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);

		$diff->d -= $diff->w * 7;

		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {

			if ($diff->$k) {

				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');

			} else {

				unset($string[$k]);

			}

		}

		if (!$full) $string = array_slice($string, 0, 1);

		return $string ? implode(', ', $string) . ' ago' : 'just now';
	}

}

?>
