<?php

/**
* @class Logger
* @description manage all the log data for the platform.
*/

class Logger{

	/**
	* @method addLogData - add the log data...
	* @description manage all the log data for the platform.
	*
	* @param {int} $user             		- the id of the user.
	* @param {string} $logType       		- the typep of log.
	* @param {string} $logNote       		- not for the system.
	* @param {string} $logPersonal   		- not for the user.
	* @param {?string} $logCategory   		- filter?.
	*/

	public static function addLogData($user, $logType, $logNote, $logPersonal, $logCategory = null, $logSubCategory = null){

		$db = DB::getInstance();
		
		$fields = array(
			  
			  'user_id' => $user,
			  
			  'log_date' => date("Y-m-d H:i:s"),
			  
			  'log_type' => $logType,

			  'log_category' => $logCategory,

			  'log_subcategory' => $logSubCategory,
			  
			  'log_note' => utf8_decode($logNote),
			  
			  'log_personal' => utf8_decode($logPersonal),

			  'ip' => Helpers::getIP(),

			  'os' => Helpers::getOS(),

			  'browser' => Helpers::getBrowser()
			);

		$db->insert('logs',$fields);
		
		$lastId = $db->lastId();
		
		return $lastId;
		
	}
}

?>