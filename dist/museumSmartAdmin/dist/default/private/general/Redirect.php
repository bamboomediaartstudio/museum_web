<?php

/**
* @class Redirect
* @description manage all the redirect from here.
*/

class Redirect{

	/**
	* @method to - the redirect location...
	* @description manage all the redirections from here.
	*
	* @param {?string} $location 			- the PATH where it should redirect.
	*/

	public static function to($location = null){
		
		if($location){
		
			//echo $location;
		
			header('Location: ' . $location);
		
			exit();
		}
	}
}