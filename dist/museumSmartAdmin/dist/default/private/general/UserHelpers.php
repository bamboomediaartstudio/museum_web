<?php

class UserHelpers{

	public static function logger($user_id, $logtype, $lognote, $logPersonal, $ip, $os, $browser) {

		$db = DB::getInstance();
		
		$fields = array(
			  
			  'user_id' => $user_id,
			  
			  'log_date' => date("Y-m-d H:i:s"),
			  
			  'log_type' => $logtype,
			  
			  'log_note' => utf8_decode($lognote),
			  
			  'log_personal' => utf8_decode($logPersonal),

			  'ip' => $ip,

			  'os' => $os,

			  'browser' => $browser


			);

		$db->insert('logs',$fields);
		
		$lastId = $db->lastId();
		
		return $lastId;
	}
}

?>