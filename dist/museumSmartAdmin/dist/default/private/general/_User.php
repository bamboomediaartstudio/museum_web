<?php

class User {

	private $_db;

	private $_data;

	private $_socialMedia;

	private $_profileImage;

	private $_sessionName;

	private $_cookieName;

	private $_isLoggedIn;

	private $_loginErrors = array();

	public function __construct($user = null){

		$this->_db = DB::getInstance();

		$this->_sessionName = Config::get('session/session_name');

		$this->_cookieName = Config::get('remember/cookie_name');

		if(!$user){

			if(Session::exists($this->_sessionName)){

				$user = Session::get($this->_sessionName);

				if($this->find($user)){

					$this->_isLoggedIn = true;

				}else{

					//logout...
				}
			}

		}else{

			$this->find($user);

		}

	}

	public function create($fields = array()){

		if(!$this->_db->insert('users', $fields)){

			throw new exception('there was a problem creating the user');

		}
	} 

	public function update($fields = array(), $id = null){

		if(!$id && $this->isLoggedIn()){

			$id = $this->data()->id;
		}

		if(!$this->_db->update('users', $id, $fields)){

			throw new exception('there was a problem creating the user');

		}

	}

	public function find($user = null){

		if($user){

			//$field = (is_numeric($user)) ? 'id' : 'email';

			if(is_numeric($user)){

				$query = 'SELECT * FROM users WHERE id = ? AND deleted = ?';
			
			}else{

				$query = 'SELECT * FROM users WHERE email = ? AND deleted = ?';
			}

			//$data = $this->_db->get('users', array($field, '=', $user));
			
			$data = $this->_db->query($query, [$user , 0]);
			
			if($data->count()){

				$this->_data = $data->first();

				return true;
			}
		}

		return false;
	}

	public function getSocialMedia($user){

		$socialMedia = $this->_db->get('users_social_media', array('user_id', '=', $user));

		if($socialMedia->count()){

			$this->_socialMedia = $socialMedia->first();

			return true;

		}

		return false;
		
	}

	public function isMasterUser(){

		return ($this->data()->group_id == 1 || $this->data()->group_id == 2) ? true : false;

	}

	public function getWording($value){
		
	}

	public function hasAccessTo($section){

		//if user is master...

		if($this->data()->group_id == 1  || $this->data()->group_id == 2) return true;

		//else...

		$groupId = $this->_db->query('SELECT id FROM museum_admin_sections_groups WHERE group_key = ?', [$section]);

		$hasAccess = $this->_db->query('SELECT * from museum_admin_sections_users_relations WHERE id_user = ? AND id_group = ?', [$this->data()->id, $groupId->first()->id]);

		if($hasAccess->count() == 0){

			$value = 0;
		}else{

			$value = $hasAccess->first()->status;
		}

		return $value;
	}

	public function getProfileImage($user){

		$image = $this->_db->query('SELECT * from museum_images WHERE sid = ? AND source = ? AND deleted = ?', 

			[$user, 'profile', 0]);
		if($image->count()){

			$this->_profileImage = $image->first()->unique_id;

			return true;
		}

		return false;


	}

	public function login($email = null, $password = null, $remember = false){

		if(!$email && !$password  && $this->exists()){

			Session::put($this->_sessionName, $this->data()->id);

			return true;

		}else{

			$user = $this->find($email);

			if($user){

				if($this->data()->password === Hash::make($password, $this->data()->salt)){

					Session::put($this->_sessionName, $this->data()->id);

					if($remember){

						$hash = Hash::unique();

						$hashCheck = $this->_db->get('users_sessions', array('user_id', '=', $this->data()->id));

						if(!$hashCheck->count()){

							$this->_db->insert('users_sessions', array(

								'user_id' => $this->data()->id,

								'hash' => $hash
							));

						}else{

							$hash = $hashCheck->first()->hash;
						}

						Cookie::put($this->_cookieName, $hash, Config::get('remember/cookie_expiry'));
					}

					$date = date("Y-m-d H:i:s");

					//last login...

					$this->_db->query("UPDATE users SET last_login = ?, logins = logins + 1 WHERE id = ?",

					[$date,$this->data()->id]);

					//add log data...

					$this->_db->insert('logs',['log_date' => $date,'user_id' => $this->data()->id,

						'log_type' => "user",'log_note' => "User logged in."]);

					//add ip...

					$ip = $_SERVER['REMOTE_ADDR'];

					$q = $this->_db->query("SELECT id FROM users_ip_list WHERE ip = ?",array($ip));
					
					$c = $q->count();
					
					if($c < 1){
					
						$this->_db->insert('users_ip_list', array('user_id' => $this->data()->id, 'ip' => $ip, ));
					
					}else{
						
						$f = $q->first();
						
						$this->_db->update('users_ip_list',$f->id, array('user_id' => $this->data()->id, 'ip' => $ip, 

							'timestamp'=>date('Y-m-d H:i:s')));

					}

					return true;

				}else{

					$this->addLoginError("El password que ingresaste para el usuario <strong>{$email}</strong> es incorrecto.");

					return false;

				}

			}else{

				$this->addLoginError("el email <strong>{$email}</strong> no se encuentra en nuestra base de datos.");

				return false;
			}

		}

		return false;

	}


	public function uploadProfileImage(){

	}

	public function exists(){ return (!empty($this->_data)) ? true : false; }

	public function logout(){

		$this->_db->delete('users_sessions', array('user_id', '=', $this->data()->id));

		Session::delete($this->_sessionName);

		Cookie::delete($this->_cookieName);

	}

	public function data(){ return $this->_data; }

	public function socialMedia(){ return $this->_socialMedia; }

	public function profileImage(){ return $this->_profileImage; }

	public function isLoggedIn(){ return $this->_isLoggedIn; }

	private function addLoginError($error){ $this->_loginErrors[] = $error; }

	public function loginErrors(){ return $this->_loginErrors; }

}

?>