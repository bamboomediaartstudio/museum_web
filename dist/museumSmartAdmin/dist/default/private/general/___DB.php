<?php


class DB{

	private static $_instance  = null;

	private $_pdo, 

	$_query, 

	$_error = false, 

	$_results = [], 

	$_count = 0,

	$_resultsArray=[],

	$_lastId;

	private function __construct(){

		try{

			$this->_pdo = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname='. Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'));

		}catch(PDOException $e){

			die($e->getMessage());
		}
	}

	public static function getInstance(){

		if(!isset(self::$_instance)){

			self::$_instance = new DB();
		}

		return self::$_instance;
	}

	public function query($sql, $params = array()){
		#echo "DEBUG: query(sql=$sql, params=".print_r($params,true).")<br />\n";
		$this->_queryCount++;
		$this->_error = false;
		$this->_errorInfo = array(0, null, null); $this->_resultsArray=[]; $this->_count=0; $this->_lastId=0;
		if ($this->_query = $this->_pdo->prepare($sql)) {
			$x = 1;
			if (count($params)) {
				foreach ($params as $param) {
					$this->_query->bindValue($x, $param);
					$x++;
				}
			}

			if ($this->_query->execute()) {
				if ($this->_query->columnCount() > 0) {
					$this->_results = $this->_query->fetchALL(PDO::FETCH_OBJ);
					$this->_resultsArray = json_decode(json_encode($this->_results),true);
				}
				$this->_count = $this->_query->rowCount();
				$this->_lastId = $this->_pdo->lastInsertId();
			} else{
				$this->_error = true;
				$this->_errorInfo = $this->_query->errorInfo();
			}
		}
		return $this;
	}

	/*public function query($sql, $params = array()){

		$this->_error = false;

		$this->_lastId = 0;

		$this->_resultsArray=[];

		if($this->_query = $this->_pdo->prepare($sql)){

			$x = 1;

			if(count($params)){

				foreach($params as $param){

					$this->_query->bindValue($x, $param);

					$x++;


				}
			}

			if($this->_query->execute()){

				$this->_results = $this->_query->fetchALL(PDO::FETCH_OBJ);

				$this->_resultsArray = json_decode(json_encode($this->_results),true);

				//$this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);

				//$this->_resultsArray = json_decode(json_encode($this->_results),true);

				$this->_count = $this->_query->rowCount();

				$this->_lastId = $this->_pdo->lastInsertId();

			}else{

				$this->_error = true;
			}

		}

		return $this;
	}*/

	public function action($action, $table, $where = array()){

		if(count($where) === 3){

			$operators = array('=', '>', '<', '>=', '<=');

			$field = $where[0];

			$operator = $where[1];

			$value = $where[2];

			if(in_array($operator, $operators)){

				$sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";

				if(!$this->query($sql, array($value))->error()){

					return $this;

				}
			}
		}

		return false;
	}

	public function insert($table, $fields = array()){
		
		if(count($fields)){

			$keys = array_keys($fields);

			$values = null;

			$x = 1;

			$sql = "INSERT INTO `{$table}` (`".implode('`, `', $keys)."`) VALUES (".str_repeat ( "?, " , count($keys)-1 )."?)";

			if(!$this->query($sql, $fields)->error()){

				return true;
			}

		}
		
		return false;
	}

	public function update($table, $id, $fields){

		$sql = "UPDATE {$table} SET " . (empty($fields) ? "" : "`") . implode("` = ? , `", array_keys($fields)) . (empty($fields) ? "" : "` = ? ");
		
		$is_ok = true;

		if (!is_array($id)) {

			$sql     .= "WHERE id = ?";
			
			$fields[] = $id;
		
		} else {
		
			if (empty($id))
		
				return false;

			if ($where_text = $this->_calcWhere($id, $fields, "and", $is_ok))

				$sql .= "WHERE $where_text";
		}

		if ($is_ok)
			
			if (!$this->query($sql, $fields)->error())
			
				return true;

		return false;
	}

	private function _calcWhere($w, &$vals, $comboparg='and', &$is_ok=NULL) {
	
		#echo "DEBUG: Entering _calcwhere(w=".print_r($w,true).",...)<br />\n";
	
		if (is_array($w)) {
				#echo "DEBUG: is_array - check<br />\n";
			$comb_ops   = ['and', 'or', 'and not', 'or not'];
			
			$valid_ops  = ['=', '<', '>', '<=', '>=', '<>', '!=', 'LIKE', 'NOT LIKE', 'ALIKE', 'NOT ALIKE', 'REGEXP', 'NOT REGEXP'];
			
			$two_args   = ['IS NULL', 'IS NOT NULL'];
			
			$four_args  = ['BETWEEN', 'NOT BETWEEN'];
			
			$arr_arg    = ['IN', 'NOT IN'];
			
			$nested_arg = ['ANY', 'ALL', 'SOME'];
			
			$nested     = ['EXISTS', 'NOT EXISTS'];
			
			$nestedIN   = ['IN SELECT', 'NOT IN SELECT'];
			
			$wcount     = count($w);

			if ($wcount == 0)
			
				return "";

			# believe it or not, this appears to be the fastest way to check
			# sequential vs associative. Particularly with our expected short
			# arrays it shouldn't impact memory usage
			# https://gist.github.com/Thinkscape/1965669
			
			if (array_values($w) === $w) { // sequential array
			
						#echo "DEBUG: Sequential array - check!<br />\n";
			
				if (in_array(strtolower($w[0]), $comb_ops)) {
			
							#echo "DEBUG: w=".print_r($w,true)."<br />\n";
			
					$sql = '';
			
					$combop = '';
			
					for ($i = 1; $i < $wcount; $i++) {
			
						$sql .= ' '. $combop . ' ' . $this->_calcWhere($w[$i], $vals, "and", $is_ok);
			
						$combop = $w[0];
			
					}
			
					return '('.$sql.')';


				} elseif ($wcount==3  &&  in_array($w[1],$valid_ops)) {

					#echo "DEBUG: normal condition w=".print_r($w,true)."<br />\n";

					$vals[] = $w[2];

					return "{$w[0]} {$w[1]} ?";

				} elseif ($wcount==2  &&  in_array($w[1],$two_args)) {

					return "{$w[0]} {$w[1]}";

				} elseif ($wcount==4  &&  in_array($w[1],$four_args)) {

					$vals[] = $w[2];

					$vals[] = $w[3];

					return "{$w[0]} {$w[1]} ? AND ?";

				} elseif ($wcount==3  &&  in_array($w[1],$arr_arg)  &&  is_array($w[2])) {

					$vals = array_merge($vals,$w[2]);

					return "{$w[0]} {$w[1]} (" . substr( str_repeat(",?",count($w[2])), 1) . ")";

				} elseif (($wcount==5 || $wcount==6 && is_array($w[5]))  &&  in_array($w[1],$valid_ops)  &&  in_array($w[2],$nested_arg)) {

					return  "{$w[0]} {$w[1]} {$w[2]}" . $this->get_subquery_sql($w[4],$w[3],$w[5],$vals,$is_ok);

				} elseif (($wcount==3 || $wcount==4 && is_array($w[3]))  &&  in_array($w[0],$nested)) {

					return $w[0] . $this->get_subquery_sql($w[2],$w[1],$w[3],$vals,$is_ok);

				} elseif (($wcount==4 || $wcount==5 && is_array($w[4]))  &&  in_array($w[1],$nestedIN)) {

					return "{$w[0]} " . substr($w[1],0,-7) . $this->get_subquery_sql($w[3],$w[2],$w[4],$vals,$is_ok);

				} else {

					echo "ERROR: w=".print_r($w,true)."<br />\n";

					$is_ok = false;

				}

			} else { // associative array ['field' => 'value']

				#echo "DEBUG: Associative<br />\n";

				$sql = '';

				$combop = '';

				foreach ($w as $k=>$v) {

					if (in_array(strtolower($k), $comb_ops)) {

						#echo "DEBUG: A<br />\n";

						#echo "A: k=$k, v=".print_r($v,true)."<br />\n";

						$sql .= $combop . ' (' . $this->_calcWhere($v, $vals, $k, $is_ok) . ') ';

						$combop = $comboparg;

					} else {

						#echo "DEBUG: B<br />\n";

						#echo "B: k=$k, v=".print_r($v,true)."<br />\n";

						$vals[] = $v;

						if (in_array(substr($k,-1,1), array('=', '<', '>'))) // 'field !='=>'value'

							$sql .= $combop . ' ' . $k . ' ? ';

						else // 'field'=>'value'

							$sql .= $combop . ' ' . $k . ' = ? ';

						$combop = $comboparg;

					}

				}

				return ' ('.$sql.') ';

			}

		} else {

			echo "ERROR: No array in $w<br />\n";

			$is_ok = false;

		}

	}

	private function get_subquery_sql($action, $table, $where, &$values, &$is_ok) {

		if (is_array($where))
		
			if ($where_text = $this->_calcWhere($where, $values, "and", $is_ok))
		
				$where_text = " WHERE $where_text";

		return " (SELECT $action FROM $table$where_text)";

	}

	/*public function update($table, $id, $fields, $idField = null){

		$applyField = ($idField == null) ? 'id' : $idField; 

		$set = '';

		$x = 1;

		foreach($fields as $name=>$value){

			$set .= "{$name} = ?";

			if($x < count($fields)){

				$set .= ', ';

			}

			$x++;
		}

		$sql = "UPDATE {$table} SET {$set} WHERE {$applyField} = {$id}";

		if(!$this->query($sql, $fields)->error()){

			return $this;

		}

		return false;
	}*/

	public function results($assoc = false){

		if($assoc) return ($this->_resultsArray) ? $this->_resultsArray : [];
		
		return ($this->_results) ? $this->_results : [];
	}

	//public function results(){ return $this->_results; }

	public function get($table, $where){ return $this->action('SELECT *', $table, $where); }

	public function delete($table, $where){ return $this->action('DELETE', $table, $where); }

	public function lastId(){ return $this->_lastId; }

	public function first(){ return $this->results()[0]; }

	public function error(){ return $this->_error; }

	public function count(){ return $this->_count; }
}


?>