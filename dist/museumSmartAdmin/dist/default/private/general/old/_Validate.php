<?php

class Validate{

	private $_passed = false;

	private $_errors = array();

	private $_db = null;

	public function __construct(){

		$this->_db = DB::getInstance();


	}

	public function check($source, $items = array()){

		foreach($items as $item => $rules){

			foreach($rules as $rule=>$rule_value){

				$display = $rules['display'];

				$value = trim($source[$item]);

				if($rule == 'get_required_files()' && empty($value)){

					$this->addError("{$item} es requerido");
				
				}else if(!empty($value)){

					switch ($rule) {
						
						case 'min':

							if(strlen($value) < $rule_value){

								$this->addError("{$item} tiene que tener por lo menos 2 caracteres");
							}

							break;

						case 'max':

						if(strlen($value) > $rule_value){

								$this->addError("{$item} tiene que tener menos de 20");
							}

							break;

						case 'matches':

							if($value != $source[$rule_value]){

								$this->AddError("{$rule_value} debe coincidir con {$item}");

							}

							break;

						case 'different':

							if($value === $source[$rule_value]){

								$this->AddError("{$rule_value} debe ser distinto a {$item}");

							}

							break;

						case 'unique':

							$check = $this->_db->get($rule_value, array($item, '=', $value));

							if($check->count()){

								$this->AddError("{$item} existe en la DB");


							}

							break;

						case 'email':
						case 'valid_email':

							if(!filter_var($value, FILTER_VALIDATE_EMAIL)){

								$this->addError("{$item} tiene que ser un email");

							}

							break;

						case 'url':

							if(!filter_var($value, FILTER_VALIDATE_URL)){

								$this->addError("{$item} tiene que ser una URL válida");

							}


							break;
						
						default:
							# code...
							break;
					}
				}


			}
		}

        if(empty($this->_errors)){ $this->_passed = true; }

        return $this;

    }

	private function addError($error){ $this->_errors[] = $error; }

	public function errors(){ return $this->_errors; }

	public function passed(){ return $this->_passed; }
}

?>