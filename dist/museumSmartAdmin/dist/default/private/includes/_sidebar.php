<?php

$url = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);

?>

<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

	<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">

		<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

			<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true">

				<a href="index.php" class="m-menu__link m-menu__toggle">

					<i class="m-menu__link-icon fas fa-chart-bar"></i>

					<span class="m-menu__link-title">

						<span class="m-menu__link-wrap">

							<span class="m-menu__link-text">Dashboard</span>

							<span class="m-menu__link-badge"></span>

						</span>

					</span>

				</a>

			</li>

			<!-- -->

			<li class="m-menu__section ">

				<h4 class="m-menu__section-text">STATS</h4>

				<i class="m-menu__section-icon flaticon-more-v2"></i>

			</li>

			<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true">

				<a href="museum_stats_touch_screens.php" class="m-menu__link m-menu__toggle">

					<i class="m-menu__link-icon fas fa-tv"></i>

					<span class="m-menu__link-title">

						<span class="m-menu__link-wrap">

							<span class="m-menu__link-text">Pantallas Touch</span>

							<span class="m-menu__link-badge"></span>

						</span>

					</span>

				</a>

			</li>

			<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true">

				<a href="museum_survey_stats.php" class="m-menu__link m-menu__toggle">

					<i class="m-menu__link-icon fas fa-poll-h"></i>


					<span class="m-menu__link-title">

						<span class="m-menu__link-wrap">

							<span class="m-menu__link-text">Encuesta App</span>

							<span class="m-menu__link-badge"></span>

						</span>

					</span>

				</a>

			</li>

			<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true">

				<a href="app_stats.php" class="m-menu__link m-menu__toggle">

					<i class="m-menu__link-icon fas fa-mobile-alt"></i>


					<span class="m-menu__link-title">

						<span class="m-menu__link-wrap">

							<span class="m-menu__link-text">App</span>

							<span class="m-menu__link-badge"></span>

						</span>

					</span>

				</a>

			</li>

			<!-- -->



			<!---->

			<?php if ($user->hasAccessTo('booking') == 1){ ?>

				<li class="m-menu__section ">

					<h4 class="m-menu__section-text">VISITAS GUIADAS</h4>

					<i class="m-menu__section-icon flaticon-more-v2"></i>

				</li>

				<!--<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true">

					<a href="museum_booking.php" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-users"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Turnos</span>

								<span class="m-menu__link-badge"></span>

							</span>

						</span>

					</a>

				</li>-->

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-users"></i>

						<span class="m-menu__link-text">Turnos</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_booking.php" class="m-menu__link">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Calendario</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_booking_batch.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Batch</span>

								</a>

							</li>

						</ul>

					</div>

				</li>



				<!-- -->

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true">

					<a href="museum_export_to_plataforma.php" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-cloud-download-alt"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Export Plataforma</span>

								<span class="m-menu__link-badge"></span>

							</span>

						</span>

					</a>

				</li>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true">

					<a href="museum_institutions_by_date.php" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-calendar-alt"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Filtrar por fecha</span>

								<span class="m-menu__link-badge"></span>

							</span>

						</span>

					</a>

				</li>


				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-person-booth"></i>

						<span class="m-menu__link-text">Guías</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_guide_add.php" class="m-menu__link">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Guía</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_guides_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-clipboard-list"></i>

						<span class="m-menu__link-text">Historial</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_historical_institutions.php" class="m-menu__link">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">V. grupales</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_historical_individual_visits.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">V. Individuales</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_historical_institutional_visits.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">V. Instituciones</span>

								</a>

							</li>

						</ul>


					</div>

				</li>


			<?php }?>

			<?php if ($user->hasAccessTo('assistance') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-apple-alt"></i>

						<span class="m-menu__link-text">Tomar Asistencia</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_take_assistance.php" class="m-menu__link">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Visitas Individuales</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_institutions_by_date_internal.php" class="m-menu__link">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Visitas Grupales</span>

								</a>

							</li>


						</ul>

					</div>

				</li>

			<?php } ?>


			<?php if(!$user->isHierarchyUser()){ ?>

			<li class="m-menu__section ">

				<h4 class="m-menu__section-text">MUSEUM WEB</h4>

				<i class="m-menu__section-icon flaticon-more-v2"></i>

			</li>

		<?php } ?>

			<?php if ($user->hasAccessTo('guide') == 1 && $user->data()->group_id == 4){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-user-shield"></i>

						<span class="m-menu__link-text">Guía</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style="">

						<span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_guide_historic_view.php" class="m-menu__link">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Listado Histórico</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_guide_past_events.php" class="m-menu__link">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Visitas pasadas</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_guide_future_events.php" class="m-menu__link">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Proximas visitas!</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>


			<?php if ($user->hasAccessTo('home') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-home	"></i>

						<span class="m-menu__link-text">HOME</span>

						<i class="m-menu__ver-arrow la la-angle-right">

						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_wording.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">wording</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_home_modules.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Módulos home</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_menu_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Menú</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_social_media.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Social Media</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_sponsors.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Sponsors</span>

								</a>

							</li>


						</ul>

					</div>

				</li>

			<?php } ?>

			<!-- mariano -->

			<?php if ($user->hasAccessTo('education') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-pen	"></i>

						<span class="m-menu__link-text">EDUCACIÓN</span>

						<i class="m-menu__ver-arrow la la-angle-right">

						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_education_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">añadir módulo</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_education_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">ver lista</span>

								</a>

							</li>


						</ul>

					</div>

				</li>

			<?php } ?>

			<!-- mariano -->


			<?php if ($user->hasAccessTo('about') == 1){ ?>

				<li class="active m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fa fa-university"></i>

						<span class="m-menu__link-text">Sobre el Museo</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu" m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_general_info.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Info. General</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_institutional_info.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Info. Institucional</span>

								</a>

							</li>

							<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

								<a href="javascript:;" class="m-menu__link m-menu__toggle">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Staff</span>

									<i class="m-menu__ver-arrow la la-angle-right"></i>

								</a>

								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>

									<ul class="m-menu__subnav">

										<li class="m-menu__item " aria-haspopup="true">

											<a href="museum_staff_add.php" class="m-menu__link ">
												<i class="m-menu__link-bullet">
													<span></span>

												</i>

												<span class="m-menu__link-text">Agregar miembro</span>

											</a>

										</li>

										<li class="m-menu__item " aria-haspopup="true">

											<a href="museum_staff_list.php" class="m-menu__link ">

												<i class="m-menu__link-bullet"><span></span></i>

												<span class="m-menu__link-text">Ver Lista</span>

											</a>

										</li>

									</ul>

								</div>

							</li>

							<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

								<a href="javascript:;" class="m-menu__link m-menu__toggle">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Investigaciones</span>

									<i class="m-menu__ver-arrow la la-angle-right"></i>

								</a>

								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>

									<ul class="m-menu__subnav">

										<li class="m-menu__item " aria-haspopup="true">

											<a href="museum_investigation_add.php" class="m-menu__link ">
												<i class="m-menu__link-bullet">
													<span></span>

												</i>

												<span class="m-menu__link-text">Agregar investigación</span>

											</a>

										</li>

										<li class="m-menu__item " aria-haspopup="true">

											<a href="museum_investigations_list.php" class="m-menu__link ">

												<i class="m-menu__link-bullet"><span></span></i>

												<span class="m-menu__link-text">Ver Lista</span>

											</a>

										</li>

									</ul>

								</div>

							</li>

							<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

								<a href="javascript:;" class="m-menu__link m-menu__toggle">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Objetivos</span>

									<i class="m-menu__ver-arrow la la-angle-right"></i>

								</a>

								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>

									<ul class="m-menu__subnav">

										<li class="m-menu__item " aria-haspopup="true">

											<a href="museum_objective_add.php" class="m-menu__link ">
												<i class="m-menu__link-bullet">
													<span></span>

												</i>

												<span class="m-menu__link-text">Agregar objetivo</span>

											</a>

										</li>

										<li class="m-menu__item " aria-haspopup="true">

											<a href="museum_objectives_list.php" class="m-menu__link ">

												<i class="m-menu__link-bullet"><span></span></i>

												<span class="m-menu__link-text">Ver Lista</span>

											</a>

										</li>

									</ul>

								</div>

							</li>

							<!--<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_progress.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Progreso</span>

								</a>

							</li>-->


						</ul>

					</div>

				</li>

			<?php } ?>

			<?php if ($user->hasAccessTo('testimonials') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fa fa-user-circle"></i>

						<span class="m-menu__link-text">Testimonios</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_testimonial_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar testimonio</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_testimonials_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista </span>

								</a>

							</li>


						</ul>

					</div>

				</li>

			<?php } ?>

			<?php if ($user->hasAccessTo('opinions') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fa fa-comments"></i>

						<span class="m-menu__link-text">Opiniones</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_opinion_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar una opinión</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_opinions_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>


						</ul>

					</div>

				</li>

			<?php }?>

			<?php if ($user->hasAccessTo('shoa') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-leaf"></i>

						<span class="m-menu__link-text">La Shoá</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu" m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_shoa_description.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Descripción</span>

								</a>

							</li>

							<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

								<a href="javascript:;" class="m-menu__link m-menu__toggle">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">F.A.Q</span>

									<i class="m-menu__ver-arrow la la-angle-right"></i>

								</a>

								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>

									<ul class="m-menu__subnav">

										<li class="m-menu__item " aria-haspopup="true">

											<a href="museum_shoa_faq_add.php" class="m-menu__link ">
												<i class="m-menu__link-bullet">
													<span></span>

												</i>

												<span class="m-menu__link-text">Agregar nueva</span>

											</a>

										</li>

										<li class="m-menu__item " aria-haspopup="true">

											<a href="museum_shoa_faq_list.php" class="m-menu__link ">

												<i class="m-menu__link-bullet"><span></span></i>

												<span class="m-menu__link-text">Ver Lista</span>

											</a>

										</li>

									</ul>

								</div>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>

			<?php if ($user->hasAccessTo('faq') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-question-circle"></i>

						<span class="m-menu__link-text">F.A.Q</span>

						<i class="m-menu__ver-arrow la la-angle-right">

						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_faq_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar F.A.Q</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_faq_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>


						</ul>

					</div>

				</li>

			<?php } ?>

			<?php if ($user->hasAccessTo('virtual_classes') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-laptop"></i>

						<span class="m-menu__link-text">Clases Virtuales</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<?php if($user->data()->group_id != 4 && $user->data()->group_id != 6){ ?>

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_virtual_class_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar clase virtual</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_virtual_classes_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista </span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_virtual_classes_batch.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar días y horarios </span>

								</a>

							</li>

							<?php } ?>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_virtual_classes_dates_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver isncripciones por día y horario</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_virtual_classes_inscriptions.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver isncripciones</span>

								</a>

							</li>

							
						</ul>

					</div>

				</li>



			<?php } ?>

			<?php if ($user->hasAccessTo('courses') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-graduation-cap"></i>

						<span class="m-menu__link-text">Cursos</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_course_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Curso</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_courses_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista </span>

								</a>

							</li>

							<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

								<a href="javascript:;" class="m-menu__link m-menu__toggle">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Tutores</span>

									<i class="m-menu__ver-arrow la la-angle-right"></i>

								</a>

								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>

									<ul class="m-menu__subnav">

										<li class="m-menu__item " aria-haspopup="true">

											<a href="museum_tutor_add.php" class="m-menu__link ">
												<i class="m-menu__link-bullet">
													<span></span>

												</i>

												<span class="m-menu__link-text">Agregar tutor</span>

											</a>

										</li>

										<li class="m-menu__item " aria-haspopup="true">

											<a href="museum_tutors_list.php" class="m-menu__link ">

												<i class="m-menu__link-bullet"><span></span></i>

												<span class="m-menu__link-text">Ver Lista</span>

											</a>

										</li>

									</ul>

								</div>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>

			<?php if ($user->hasAccessTo('exhibitions') == 1){ ?>

				<!-- muestras -->

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-box"></i>

						<span class="m-menu__link-text">Muestras</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_exhibition_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Muestra</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_exhibitions_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>

			<!-- noticias -->

			<?php if ($user->hasAccessTo('news') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-newspaper"></i>

						<span class="m-menu__link-text">Prensa</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_news_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Noticia</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_news_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a target="_blank" href="http://museodelholocausto.local/prensa/rss.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">RSS</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php }?>

			<!-- eventos -->

			<?php if ($user->hasAccessTo('events') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-calendar"></i>

						<span class="m-menu__link-text">Eventos</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_event_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Evento</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_events_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>

			<?php if ($user->hasAccessTo('alerts') == 1){ ?>

				<!-- alertas! -->

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-exclamation-triangle"></i>

						<span class="m-menu__link-text">Alertas</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_alert_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Crear Alerta</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_alerts_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>

			<!-- quotes -->

			<?php if ($user->hasAccessTo('quotes') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-quote-right"></i>

						<span class="m-menu__link-text">Frases</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_quote_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Frase</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_quotes_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>

			<?php if ($user->hasAccessTo('heritage') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-menorah"></i>

						<span class="m-menu__link-text">Patrimonio</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_heritage_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Objeto</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_heritage_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>

			<!-- enf of heritage -->

			<?php if ($user->hasAccessTo('books') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-book"></i>

						<span class="m-menu__link-text">Libros</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_book_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Libro</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_books_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>

			<?php if ($user->hasAccessTo('images') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon far fa-images"></i>

						<span class="m-menu__link-text">Galerías</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_library_images.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Biblioteca</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_visits_images.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Visitas</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_education_images.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Educación</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>



			<!-- Ends Museum Web-->

			<?php if(!$user->isHierarchyUser()){ ?>
			<li class="m-menu__section ">

				<h4 class="m-menu__section-text">MUSEUM APP</h4>

				<i class="m-menu__section-icon flaticon-more-v2"></i>

			</li>
		<?php } ?>

			<!-- Push Notifications -->
			<?php if ($user->hasAccessTo('push_notifications') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<!--<i class="m-menu__link-icon fas fa-book"></i>-->
						<i class="m-menu__link-icon fas fa-bell"></i>

						<span class="m-menu__link-text">Notificaciones Push</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_push_notifications_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Notificaciones</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_push_notifications_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>

			<!-- Criminals -->
			<?php if ($user->hasAccessTo('criminals') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<!--<i class="m-menu__link-icon fas fa-book"></i>-->
						<i class="m-menu__link-icon fas fa-skull-crossbones"></i>

						<span class="m-menu__link-text">Criminales</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_app_criminals_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Criminal</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_app_criminals_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>

			<!-- timeline -->

			<?php if ($user->hasAccessTo('timeline') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-stream"></i>

						<span class="m-menu__link-text">Linea de tiempo</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_app_timeline_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Evento</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_app_timeline_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>
			<!-- /. timeline --->

			<!-- Guetos -->

			<?php if ($user->hasAccessTo('guetos') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<!--<i class="m-menu__link-icon fas fa-book"></i>-->
						<i class="m-menu__link-icon fas fa-map-marker-alt"></i>

						<span class="m-menu__link-text">Guetos</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_app_guetos_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Gueto</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_app_guetos_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>
			<!-- /. Guetos --->

			<!-- Prensa -->
			<?php if ($user->hasAccessTo('prensa') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-newspaper"></i>

						<span class="m-menu__link-text">Prensa</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_app_prensa_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Prensa</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_app_prensa_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>


			<!-- /. Prensa --->

			<!-- Sobrevivientes -->
			<?php if ($user->hasAccessTo('survivors') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-blind"></i>

						<span class="m-menu__link-text">Sobrevivientes</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_app_survivors_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Sobreviviente</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_app_survivors_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>
			<!-- /. Sobrevivientes --->

			<!-- Justos entre las naciones -->
			<?php if ($user->hasAccessTo('righteous_among_nations') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<!--<i class="m-menu__link-icon fas fa-book"></i>-->
						<i class="m-menu__link-icon fas fa-heart"></i>

						<span class="m-menu__link-text">Justos entre las Naciones</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_app_righteousNations_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Justo</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_app_righteousNations_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_app_righteousNations_countries_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Justos por paises</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_app_righteousNations_countries_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Listar Justos por paises</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>
			<!-- /. Justos entre las naciones --->

			<!-- Films -->

			<?php if ($user->hasAccessTo('films') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-film"></i>

						<span class="m-menu__link-text">Peliculas</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<!-- TODO:: CONTINUAR CON SECCION DE FILMS AGREGAR Y LISTAR! -->

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_app_films_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Pelicula</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true" >

								<a  href="museum_app_films_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>
			<!-- /. Films --->

			<!-- Museum random facts -->

			<?php if ($user->hasAccessTo('random_facts') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-random"></i>

						<span class="m-menu__link-text">¿Sabías Qué?</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_random_facts_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_random_facts_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Listar</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_random_facts_feedback.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver feedback</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>


			<!-- Museum Trivias -->

			<?php if ($user->hasAccessTo('trivias') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon far fa-images"></i>

						<span class="m-menu__link-text">Trivias</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_trivias_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Trivia</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_trivias_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Listar Trivias</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_trivias_feedback.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver feedback</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>


			<!-- Museum Mapping -->
			<?php if ($user->hasAccessTo('mapping') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon far fa-images"></i>

						<span class="m-menu__link-text">Mapping</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_app_mapping_names_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Datos</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_app_mapping_names_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Listar Datos</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>


			<!-- Museum Audioguide -->
			
			<?php if ($user->hasAccessTo('audioguide') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-podcast"></i>

						<span class="m-menu__link-text">Audioguía</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_app_audioguide_add.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Agregar Audioguía</span>

								</a>

							</li>

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_app_audioguide_list.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Listar Audioguías</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>

			<!-- cripto -->

			<?php if ($user->hasAccessTo('crypto') == 1){ ?>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-coins"></i>

						<span class="m-menu__link-text">Criptoactivos</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="crypto.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver Wallets</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>

			<!-- en of cripto -->


			<!-- /. Museum App Menu -->


			<?php if ($user->hasAccessTo('support') == 1){ ?>

				<li class="m-menu__section ">

					<h4 class="m-menu__section-text">SOPORTE</h4>

					<i class="m-menu__section-icon flaticon-more-v2"></i>

				</li>

				<!-- bugs? -->

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-bug"></i>

						<span class="m-menu__link-text">Bugs</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_bug_report.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Reportar error</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-life-ring"></i>

						<span class="m-menu__link-text">Soporte</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_support.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Solicitar soporte técnico</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-question-circle"></i>

						<span class="m-menu__link-text">General F.A.Q</span>

						<i class="m-menu__ver-arrow la la-angle-right"> </i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="faq.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Preguntas frecuentes</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>

			<?php if ($user->hasAccessTo('backups') == 1){ ?>

				<li class="m-menu__section ">

					<h4 class="m-menu__section-text">BACK UPS</h4>

					<i class="m-menu__section-icon flaticon-more-v2"></i>

				</li>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-envelope"></i>

						<span class="m-menu__link-text">Emails</span>

						<i class="m-menu__ver-arrow la la-angle-right">


						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_emails_backups.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fab fa-mailchimp"></i>

						<span class="m-menu__link-text">Newsletter</span>

						<i class="m-menu__ver-arrow la la-angle-right">

						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_newsletter_backups.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-pen"></i>

						<span class="m-menu__link-text">Solicitudes</span>

						<i class="m-menu__ver-arrow la la-angle-right">

						</i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="840" style=""><span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item " aria-haspopup="true">

								<a  href="museum_applicants_backups.php" class="m-menu__link ">

									<i class="m-menu__link-bullet"><span></span></i>

									<span class="m-menu__link-text">Ver lista</span>

								</a>

							</li>

						</ul>

					</div>

				</li>

			<?php } ?>

			<?php if ($user->isMasterUser() == true){ ?>

				<li class="m-menu__section ">

					<h4 class="m-menu__section-text">ADMIN</h4>

					<i class="m-menu__section-icon flaticon-more-v2"></i>

				</li>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true">

					<a href="users.php" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-users"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Listado de Usuarios</span>

								<span class="m-menu__link-badge"></span>

							</span>

						</span>

					</a>

				</li>

				<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true">

					<a href="users_invitation.php" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fas fa-users"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Invitar Usuario</span>

								<span class="m-menu__link-badge"></span>

							</span>

						</span>

					</a>

				</li>

			<?php } ?>

		</ul>

	</div>

</div>
