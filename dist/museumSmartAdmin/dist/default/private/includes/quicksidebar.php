<?php

require_once 'private/users/core/init.php';

$logs = DB::getInstance()->query('SELECT * FROM logs where user_id = ? ORDER BY log_category', (array($user->data()->id)));

$items = [];

foreach($logs->results() as $result){

	$items[] = array('personal'=>$result->log_personal, 'category' =>$result->log_category, 'date'=>$result->log_date);

};

/*usort($items, function($a, $b) {

	return $a['date'] - $b['date'];

});*/

$lastCategory = null;

?>

<div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light">

	<div class="m-quick-sidebar__content m--hide">

		<span id="m_quick_sidebar_close" class="m-quick-sidebar__close">

			<i class="la la-close"></i>

		</span>

		<ul id="m_quick_sidebar_tabs" class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">

			<li class="nav-item m-tabs__item">

				<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_quick_sidebar_tabs_logs" role="tab">

					Logs / tu actividad

				</a>

			</li>

		</ul>

		<div class="tab-content">

			<div class="tab-pane m-scrollable active" id="m_quick_sidebar_tabs_logs" role="tabpanel">

				<div class="m-list-timeline">

					<?php 

					foreach($items as $item){

						$actualCategory = ($item['category']);

						$personal = ($item['personal']);

						$date = $item['date'];

						if($actualCategory != $lastCategory){

							?>

							<div class="m-list-timeline__group">

								<div class="m-list-timeline__heading">

									<?php echo $actualCategory;?>

								</div>

								<div class="m-list-timeline__items">

									<?php

								}else{

									?>

									<div class="m-list-timeline__item">

										<span class="m-list-timeline__badge m-list-timeline__badge--state-success"></span>

										<a href="" class="m-list-timeline__text">

											<?php echo $personal;?>

										</a>

										<span class="m-list-timeline__time">

											<?php

											$helper = new Helpers();

											echo $helper->timeElapsedString($date);

											?>

										</span>

									</div>

									<?php 

								}

								$lastCategory = $actualCategory;
							}

							?>

						</div>

					</div>

				</div>

			</div>

		</div>