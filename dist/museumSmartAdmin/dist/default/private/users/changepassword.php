<?php

require_once 'core/init.php';

$status['status'] = 0;

$user = new User();

if(!$user->isLoggedIn()){

	Redirect::to('index.php');
}

if(Input::exists()){

	if(Token::check(Input::get('token'))){

		$validate = new Validate();

		$validation = $validate->check($_POST, array(

			'actualpassword' => array('required' => true, 'min' => 4),
			
			'newpassword' => array('required' => true, 'min' => 4, 'different' => 'actualpassword'),

			'repeatnewpassword' => array('required' => true, 'min' => 4, 'matches' => 'newpassword')
	
		));

		if($validation->passed()){

			$status['hashed'] = Hash::make(Input::get('actualpassword'), $user->data()->salt);

			if(Hash::make(Input::get('actualpassword'), $user->data()->salt) !== $user->data()->password){
				
				$status['status'] = 1;

			}else{

				$salt = Hash::salt(32);

				$user->update(array(

					'password' => Hash::make(Input::get('newpassword'), $salt),

					'salt' => $salt

				));

				$status['status'] = 2;
			}
			

		}else{

			$status['status'] = 3;
			
		}
	
	}else{

		$status['notoken'] = Input::get('token');

	}

}else{

	$status['status'] = 4;

}


echo json_encode($status);


?>