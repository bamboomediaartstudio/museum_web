<?php

//just to understand the logic of this checker:
//...
//...
//1 - if the session exists, then we have to make the login considering the session value:
//$user = new User(Session::get(Config::get('session/session_name')));
//if this login is not successful, then we sent the user to the login page.
//...
//2 - if there is no session, then we have to evaluete the cookie alternative for the remember me.
// - so we get the hash.
// - then we made the hashCheck 
// - if user exists, everything is fine... :)
// - if the user does not exists, then we redirect to te login page.
// - if there is no remember cookie, then we go to login too.
//
//
// there are three alternatives to go back to login:
// if there is a session but cannot login, then go to login page.
// if there is a remember me cookie but it is not in the DB, then go to login page.
// if there is a remember me cookie and a valid hash, but cannot login, then go to login page. 
// (is the last alternative possible? perhaps with a deleted user? check thisi).
//
//3 - now there is one more alternative to check: and it´s the situation when an admin user
//is trying to access to section that it is not allowed: this could happen if the user is copy and pasting
//a link with an URL. We should think about two alternatives:
//
// - redirect to index.php
// - add an error message in the admin
// - finally we will send the user to a redirect.php page with a get variable indicating that he is not allowed
// to visit this section...
//
// then there should be a whitelist:
//
// - all the users can access to index.php / login.php / redirect.php
//
// and also there is some kind of blacklist, since just master admins can access to users.php and users_access.php

//so first we have to know the URL...

$prev = strtok($_SERVER['REQUEST_URI'], '?');

$url = basename($prev, '?' . $_SERVER['QUERY_STRING']);


//then we have to create the whitelist...

$whiteList = array('index.php', 'login.php', 'redirect.php', 'users.php', 'user_access.php', 'profile.php', 'guide-index.php', 'museum_institutions_by_date.php', 'museum_stats_touch_screens.php', 'museum_app_stats.php', 'museum_survey_stats.php', 'app_stats.php');

//exit;

//and we will use this boolean to flag the white listed status.

$isWhitelisted = false;

//then we select the id_gorup frorm the museum_admin_sections_pages using the actual URL...

$idGroupQuery = DB::getInstance()->query('SELECT id_group FROM museum_admin_sections_pages WHERE page = ?', [$url]);



//if the link does not exists we have to send the user to a 404 page or something like that...
//else we have to select the group_key since we use it for validations though the method 'hasAccessTo'

if($idGroupQuery->count() >0){

	$idGroup = $idGroupQuery->first()->id_group;

	$groupNameQuery = DB::getInstance()->query('SELECT group_key FROM museum_admin_sections_groups WHERE id = ?', [$idGroup]);

	$groupKey = $groupNameQuery->first()->group_key;

}else if(in_array($url, $whiteList)){

	$isWhitelisted = true;

}else{

	Redirect::to('login.php');
	
}


if(!Session::exists(Config::get('session/session_name'))){

	if(Cookie::exists(Config::get('remember/cookie_name'))){

		$hash = Cookie::get(Config::get('remember/cookie_name'));

		$hashCheck = DB::getInstance()->get('users_sessions', array('hash', '=', $hash));

		if($hashCheck->count()){

			$user = new User($hashCheck->first()->user_id);

			$login = $user->login();

			//if the user is not looged in, redirect to login.php

			if(!$login){

				Redirect::to('login.php?errorType=nologin');

			}else{

				//if the user is logged, we have to evaluate if:

				//is master user trying to go to users.php or user_access.php? if the user is not master,
				//redirect to redirect.php indicating the conflict with this URL...

				if(!$user->isMasterUser() && ($url == 'users.php' || $url =='user_access.php')) Redirect::to('redirect.php?prevUrl=' . $url);

				//then we have to check: if the user is trying to access to a non whitelisted URL, then we 
				//have to evaluate if the user has access. For this we have the metod hasAccessTo.

				if(!$isWhitelisted){

					$hasAccessTo = $user->hasAccessTo($groupKey);

					//is the user has no access, then rredirect to redirect.php indicating the URL...

					if($hasAccessTo == 0) Redirect::to('redirect.php?prevUrl=' . $url);

				}

			}

		}

	}else{

		Redirect::to('login.php?errorType=nocookie');
	}

}else{

	$user = new User(Session::get(Config::get('session/session_name')));

	$login = $user->login();

	if(!$login){

		Redirect::to('login.php');

	}else{

		//the same logic...

		if(!$user->isMasterUser() && ($url == 'users.php' || $url =='user_access.php')) Redirect::to('redirect.php?prevUrl=' . $url);

		if(!$isWhitelisted){

			$hasAccessTo = $user->hasAccessTo($groupKey);

			if($hasAccessTo == 0) Redirect::to('redirect.php?prevUrl=' . $url);

		}

	}

}

?>