<?php

session_start();

date_default_timezone_set('America/Argentina/Buenos_Aires');

$GLOBALS['config'] = array(

		'mysql'=>array(

		'host'=> '127.0.0.1',

		'username'=> 'root',

		'password'=> '',

		'db'=> 'museum_smart_admin'
	),

	'remember'=>array(

		'cookie_name'=> 'hash',

		'cookie_expiry'=> 604800

	),

	'session'=>array(

		'session_name' => 'user',

		'token_name' => 'token'
	), 

	'users'=>array(

		'users_profile_picture_folder' => '../sources/images/users/',

		'users_profile_picture_max_size' => 20971520 //20 MB...

	),

	'staff'=>array(

		'staff_profile_picture_folder' => '../../../sources/images/staff/',

		'staff_profile_picture_max_size' => 20971520 //20 MB...

	),

	'opinions'=>array(

		'opinions_picture_folder' => '../../../sources/images/opinions/',

		'opinions_picture_max_size' => 20971520 //20 MB...

	),

	'testimonials'=>array(

		'testimonials_picture_folder' => '../../../sources/images/testimonials/',

		'testimonials_picture_max_size' => 20971520 //20 MB...

	),

	'courses'=>array(

		'courses_picture_folder' => '../../../sources/images/courses/',

		'courses_picture_max_size' => 20971520 //20 MB...

	),

	'exhibitions'=>array(

		'exhibitions_picture_folder' => '../../../sources/images/exhibitions/',

		'exhibitions_picture_max_size' => 20971520 //20 MB...

	),

	'tutors'=>array(

		'tutors_picture_folder' => '../../../sources/images/tutors/',

		'tutors_picture_max_size' => 20971520 //20 MB...

	),

	'news'=>array(

		'news_picture_folder' => '../../../sources/images/news/',

		'news_picture_max_size' => 20971520 //20 MB...

	),

	'events'=>array(

		'events_picture_folder' => '../../../sources/images/events/',

		'events_picture_max_size' => 20971520 //20 MB...

	)

);


spl_autoload_register(function($class){

	require_once($_SERVER['DOCUMENT_ROOT'] . '/museumSmartAdmin/dist/default' .  '/private/general/' . $class . '.php');

});

require_once($_SERVER['DOCUMENT_ROOT'] . '/museumSmartAdmin/dist/default' .  '/private/users/functions/sanitize.php');

$path =  basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);

if(!Session::exists(Config::get('session/session_name')) || $path === 'login.php'){

	if(Cookie::exists(Config::get('remember/cookie_name'))){

		$hash = Cookie::get(Config::get('remember/cookie_name'));

		$hashCheck = DB::getInstance()->get('users_sessions', array('hash', '=', $hash));

		if($hashCheck->count()){

			$user = new User($hashCheck->first()->user_id);

			$login = $user->login();

			if(!$login){

				//Redirect::to('index.php');

				Redirect::to('login.php');

			}/*else{

				Redirect::to('login.php');
			}*/
		}
	}
}