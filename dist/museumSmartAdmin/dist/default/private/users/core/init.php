<?php session_start();

date_default_timezone_set('America/Argentina/Buenos_Aires');

$whitelist = array('127.0.0.1', '::1');

$GLOBALS['config'] = array(

		'mysql'=>array(

		'host'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '127.0.0.1' : '108.179.242.98',

		'username'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? 'root' : 'brpxmzm5_make',

		'password'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '' : 'n},iT0aJTqnF',

		'db'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? 'museum_smart_admin' : 'brpxmzm5_museum_smart_admin'
	),

	'remember'=>array(

		'cookie_name'=> 'hash',

		'cookie_expiry'=> 604800

	),

	'session'=>array(

		'session_name' => 'user',

		'token_name' => 'token'
	),

	'users'=>array(

		'users_profile_picture_folder' => '../sources/images/users/',

		'users_profile_picture_max_size' => 20971520 //20 MB...

	),

	'staff'=>array(

		'staff_profile_picture_folder' => '../../../sources/images/staff/',

		'staff_profile_picture_max_size' => 20971520 //20 MB...

	),

	'opinions'=>array(

		'opinions_picture_folder' => '../../../sources/images/opinions/',

		'opinions_picture_max_size' => 20971520 //20 MB...

	),

	'testimonials'=>array(

		'testimonials_picture_folder' => '../../../sources/images/testimonials/',

		'testimonials_picture_max_size' => 20971520 //20 MB...

	),

	'courses'=>array(

		'courses_picture_folder' => '../../../sources/images/courses/',

		'courses_picture_max_size' => 20971520 //20 MB...

	),

	'exhibitions'=>array(

		'exhibitions_picture_folder' => '../../../sources/images/exhibitions/',

		'exhibitions_picture_max_size' => 20971520 //20 MB...

	),

	'tutors'=>array(

		'tutors_picture_folder' => '../../../sources/images/tutors/',

		'tutors_picture_max_size' => 20971520 //20 MB...

	),

	'news'=>array(

		'news_picture_folder' => '../../../sources/images/news/',

		'news_picture_max_size' => 20971520 //20 MB...

	),

	'events'=>array(

		'events_picture_folder' => '../../../sources/images/events/',

		'events_picture_max_size' => 20971520 //20 MB...

	),

	'profile'=>array(

		'profile_picture_folder' => '../../../sources/images/profile/',

		'profile_picture_max_size' => 20971520 //20 MB...

	),

	'sponsors'=>array(

		'sponsors_picture_folder' => '../../../sources/images/sponsors/',

		'sponsors_picture_max_size' => 20971520 //20 MB...

	),

	'heritage'=>array(

		'heritage_picture_folder' => '../../../sources/images/heritage/',

		'heritage_picture_max_size' => 20971520 //20 MB...

	),

	'library'=>array(

		'library_picture_folder' => '../../../sources/images/library/',

		'library_picture_max_size' => 20971520 //20 MB...

	),

	'visits'=>array(

		'visits_picture_folder' => '../../../sources/images/visits/',

		'visits_picture_max_size' => 20971520 //20 MB...

	),

	'education'=>array(

		'education_picture_folder' => '../../../sources/images/education/',

		'education_picture_max_size' => 20971520 //20 MB...

	),

	'books'=>array(

		'books_picture_folder' => '../../../sources/images/books/',

		'books_picture_max_size' => 20971520 //20 MB...

	),

	'survivors'=>array(

		'news_picture_folder' => '../../../sources/images/survivors/',

		'news_picture_max_size' => 20971520 //20 MB...

	),

	'classes'=>array(

		'classes_picture_folder' => '../../../sources/images/classes/',

		'classes__picture_max_size' => 20971520 //20 MB...

	)




);

spl_autoload_register(function($class){

	$privateBase = dirname(dirname(dirname(__FILE__)));

	require_once($privateBase .  '/general/' . $class . '.php');

});

$privateBase = dirname(dirname(dirname(__FILE__)));

require_once($privateBase . '/users/functions/sanitize.php');


//...

//$privateBase = dirname(dirname(dirname(__FILE__)));

//require_once($privateBase .  '/framework/mailer/Emailer.php');

//require_once($privateBase .  '/framework/mailer/EmailTemplate.php');
