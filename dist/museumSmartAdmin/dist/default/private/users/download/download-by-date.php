<?php

use PhpOffice\PhpSpreadsheet\Helper\Sample;

use PhpOffice\PhpSpreadsheet\IOFactory;

use PhpOffice\PhpSpreadsheet\Spreadsheet;

require 'vendor/autoload.php';

require_once '../core/init.php';

$status['init'] = true;

$status['id'] = Input::get('id');

$validate = new Validate();

$validation = $validate->check($_GET, array(

    'from' => array('display'=>'from', 'required' => true),

    'to' => array('display'=>'to', 'required' => true)

));

if($validation->passed()){

    $fileName = 'plataforma_from_' . Input::get('from') . '_to_' . Input::get('to');

    $spreadsheet = new Spreadsheet();

    $spreadsheet->getProperties()->setCreator('Museo Del Holocausto de Buenos Aires')

    ->setLastModifiedBy('Museo Del Holocausto de Buenos Aires')
    
    ->setTitle('Office 2007 XLSX Test Document')
    
    ->setSubject('Office 2007 XLSX Test Document')
    
    ->setDescription('Documento creado dinámicamente para upload de instituciones a Plataforma - Museo Del Holocausto')
    
    ->setKeywords('export de institiuciones')
    
    ->setCategory('export de instituciones');

    $helper = new Sample();
    
    if ($helper->isCli()) {

        $helper->log('opsss!' . PHP_EOL);

        return;

    }

    $query = DB::getInstance()->query('

        SELECT *, mb.id as booking_main_id, mb.deleted as institution_deleted FROM museum_booking_institutions AS mbi 

        LEFT JOIN museum_booking_institutions_relations as mbir

        ON mbi.id = mbir.id_institution

        LEFT join museum_booking AS mb

        ON mbir.id_booking = mb.id

        LEFT JOIN museum_booking_guides as mbg

        ON mb.id = mbg.id_booking

        LEFT JOIN museum_guides as mg

        ON mg.id = mbg.id_guide

        WHERE mb.date_start >= ? AND mb.date_end <=? ORDER BY mb.date_start ASC', [Input::get('from'), Input::get('to')]); 

    $styleArray = [

        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
        ]
    ];


    $counter = 1; 

    $results = $query->results();

    $printHeader = true;

    $positionA = 'A' . (string)$counter;

    $positionB = 'B' . (string)$counter;

    $positionC = 'C' . (string)$counter;

    $positionD = 'D' . (string)$counter;

    $positionE = 'E' . (string)$counter;

    $positionF = 'F' . (string)$counter;

    $positionG = 'G' . (string)$counter;

    $positionH = 'H' . (string)$counter;

    $positionI = 'I' . (string)$counter;

    $positionJ = 'J' . (string)$counter;

    $positionK = 'K' . (string)$counter;

    $positionL = 'L' . (string)$counter;

    $positionM = 'M' . (string)$counter;

    $positionN = 'N' . (string)$counter;

    $positionO = 'O' . (string)$counter;

    $positionP = 'P' . (string)$counter;

    $positionQ = 'Q' . (string)$counter;

    $positionR = 'R' . (string)$counter;
    
    $positionS = 'S' . (string)$counter;

    $positionT = 'T' . (string)$counter;


    if($printHeader){

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionA, 'ID unico de visita');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionB, 'Fecha');
        
        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionC, 'Horario');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionD, 'Nombre legal del visitante');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionE, 'CUIT');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionF, 'Condición frente al IVA');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionG, 'Teléfono');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionH, 'Dirección');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionI, 'Proincia');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionJ, 'Email');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionK, 'Datos d contacto');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionL, 'Guía');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionM, 'Guía secundaria');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionN, 'Cantidad de visitantes');
        
        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionO, 'Tipo de actividad');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionP, 'Tipo de institución');
        
        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionQ, 'Beca');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionR, 'Discapacidad');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionS, '¿Canceló?');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionT, '¿Asistió?');

        $spreadsheet->getActiveSheet()->getStyle('A1:R1')->getFont()->setBold(true);

        //$spreadsheet->insertNewRowBefore($counter + 1, 1);
    }

    $counter = 3;

    foreach($results as $result){

        $positionA = 'A' . (string)$counter;

        $positionB = 'B' . (string)$counter;

        $positionC = 'C' . (string)$counter;

        $positionD = 'D' . (string)$counter;

        $positionE = 'E' . (string)$counter;

        $positionF = 'F' . (string)$counter;

        $positionG = 'G' . (string)$counter;

        $positionH = 'H' . (string)$counter;

        $positionI = 'I' . (string)$counter;

        $positionJ = 'J' . (string)$counter;

        $positionK = 'K' . (string)$counter;

        $positionL = 'L' . (string)$counter;

        $positionM = 'M' . (string)$counter;

        $positionN = 'N' . (string)$counter;

        $positionO = 'O' . (string)$counter;

        $positionP = 'P' . (string)$counter;

        $positionQ = 'Q' . (string)$counter;

        $positionR = 'R' . (string)$counter;

        $positionS = 'S' . (string)$counter;

        $positionT = 'T' . (string)$counter;

        $bookingId = $result->booking_main_id;

        $name = $result->institution_name;

        $cuit = $result->institution_cuit;

        $phone = $result->institution_phone;

        $address = $result->institution_address;

        $email = $result->institution_email;

        $state = $result->institution_state;

        $dt = new DateTime($result->date_start);

        $date = $dt->format('d/m/Y');
        
        $time = $dt->format('H:i:s');

        $iva = strtolower($result->iva);

        $finalIVA = '';

        $inscriptionContactName = $result->inscription_contact_name;

        $inscriptionContactPhone = $result->inscription_contact_phone;

        $inscriptionContactEmail = $result->inscription_contact_email;

        $contactData = $inscriptionContactName . ' -  te: ' . $inscriptionContactPhone . ' - email: ' . $inscriptionContactEmail;

        if (strlen($contactData) > 255){

            $contactData = substr($contactData, 0, 252) . '...';

        }

        if($iva == 'consumidor final'){

            $finalIVA = 'CF';

        }else if($iva == 'exento'){

            $finalIVA = 'EX';

        }else if($iva == 'responsable monotributo'){

            $finalIVA = 'MO';

        }else if($iva == 'responsable inscripto' || $iva == 'pesponsable inscripto'){

            $finalIVA = 'RI';

        }else if($iva == 'otro'){

            $finalIVA = 'OT';

        }else if($iva == 'sujeto no categorizado'){

            $finalIVA = 'NC';

        }else{

            $finalIVA = '';
        }

        $guideName = $result->name . ' ' . $result->surname;

        $level = $result->eduction_level;

        $scholarship = ($result->scholarship_asigned == 0) ? 'NO' : 'SI';
        
        $cancel = ($result->institution_deleted == 0) ? 'NO' : 'SI';

        if($cancel == 1){

            $assisted = 'NO';

        }else{

            $assisted = ($result->assisted == 0) ? 'NO' : 'SI';
        
        }

        $specialNeed = strtoupper($result->special_need);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionA, $bookingId);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionB, $date);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionC, $time);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionD, $name);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionE, $cuit);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionF, $finalIVA);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionG, $phone);;

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionH, $address);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionI, $state);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionJ, $email);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionK, $contactData);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionL, $guideName);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionM, '');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionN, '25');
        
        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionO, 'VG');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionP, $level);
        
        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionQ, $scholarship);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionR, $specialNeed);
        
        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionS, $cancel);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionT, $assisted);
        
        $counter+=1;
    } 

    foreach(range('A','Z') as $columnID) {

        $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);

    }

    $spreadsheet->getActiveSheet()->setTitle('Museo Del Holocausto');

    $spreadsheet->setActiveSheetIndex(0);

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    
    header('Content-Disposition: attachment;filename="'. $fileName . '.xlsx"');
    
    header('Cache-Control: max-age=0');
    
    header('Cache-Control: max-age=1');
    
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    
    header('Cache-Control: cache, must-revalidate');
    
    header('Pragma: public');

    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

    $writer->save('php://output');

    die();

}