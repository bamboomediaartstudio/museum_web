<?php

use PhpOffice\PhpSpreadsheet\Helper\Sample;

use PhpOffice\PhpSpreadsheet\IOFactory;

use PhpOffice\PhpSpreadsheet\Spreadsheet;

require 'vendor/autoload.php';

require_once '../core/init.php';

$status['init'] = true;

$status['id'] = Input::get('id');

$validate = new Validate();

$validation = $validate->check($_GET, array(

    'from' => array('display'=>'from', 'required' => true),

    'to' => array('display'=>'to', 'required' => true)

));

if($validation->passed()){

    $fileName = 'plataforma_individuals_from_' . Input::get('from') . '_to_' . Input::get('to');

    $spreadsheet = new Spreadsheet();

    $spreadsheet->getProperties()->setCreator('Museo Del Holocausto de Buenos Aires')

    ->setLastModifiedBy('Museo Del Holocausto de Buenos Aires')
    
    ->setTitle('Office 2007 XLSX Test Document')
    
    ->setSubject('Office 2007 XLSX Test Document')
    
    ->setDescription('Documento creado dinámicamente para upload de instituciones a Plataforma - Museo Del Holocausto')
    
    ->setKeywords('export de institiuciones')
    
    ->setCategory('export de instituciones');

    $helper = new Sample();
    
    if ($helper->isCli()) {

        $helper->log('opsss!' . PHP_EOL);

        return;

    }

    $query = DB::getInstance()->query('

        SELECT *, mb.id as booking_main_id FROM museum_booking_assistants AS mba

        INNER JOIN museum_booking as mb  ON mba.booking_id = mb.id

        WHERE mba.active = ? AND mba.deleted = ? AND mb.date_start >= ? and mb.date_end <= ? ORDER BY mb.date_start ASC', 

        [1, 0, Input::get('from'), Input::get('to')]); 

    $styleArray = [

        'alignment' => [
            
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
        
        ]
    ];


    $counter = 1; 

    $results = $query->results();

    $printHeader = true;

    $positionA = 'A' . (string)$counter;

    $positionB = 'B' . (string)$counter;

    $positionC = 'C' . (string)$counter;

    $positionD = 'D' . (string)$counter;

    $positionE = 'E' . (string)$counter;

    $positionF = 'F' . (string)$counter;

    $positionG = 'G' . (string)$counter;

    $positionH = 'H' . (string)$counter;

    $positionI = 'I' . (string)$counter;

    $positionJ = 'J' . (string)$counter;


    if($printHeader){

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionA, 'ID unico de visita');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionB, 'Fecha');
        
        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionC, 'Horario');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionD, 'Nombre');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionE, 'Apellido');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionF, 'email');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionG, 'teléfono');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionH, 'DNI / Pasaporte');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionI, '¿Cuál es el motivo de la visita?');

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionJ, '¿Cómo conociste el Museo del Holocausto?');

        $spreadsheet->getActiveSheet()->getStyle('A1:R1')->getFont()->setBold(true);

    }

    $counter = 3;

    foreach($results as $result){

        $positionA = 'A' . (string)$counter;

        $positionB = 'B' . (string)$counter;

        $positionC = 'C' . (string)$counter;

        $positionD = 'D' . (string)$counter;

        $positionE = 'E' . (string)$counter;

        $positionF = 'F' . (string)$counter;

        $positionG = 'G' . (string)$counter;

        $positionH = 'H' . (string)$counter;

        $positionI = 'I' . (string)$counter;

        $positionJ = 'J' . (string)$counter;

        $bookingId = $result->booking_main_id;

        $dt = new DateTime($result->date_start);

        $date = $dt->format('d/m/Y');

        $time = $dt->format('H:i:s');

        $name = $result->name;

        $surname = $result->surname;

        $email = $result->email;

        $phone = $result->phone;

        $dni = $result->dni;

        $visitReason = $result->visit_reason;

        $aboutMuseum = $result->about_museum;

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionA, $bookingId);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionB, $date);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionC, $time);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionD, $name);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionE, $surname);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionF, $email);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionG, $phone);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionH, $dni);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionI, $visitReason);

        $spreadsheet->setActiveSheetIndex(0)->setCellValue($positionJ, $aboutMuseum);

        $counter+=1;
    } 

    foreach(range('A','Z') as $columnID) {

        $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);

    }

    $spreadsheet->getActiveSheet()->setTitle('Museo Del Holocausto');

    $spreadsheet->setActiveSheetIndex(0);

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    
    header('Content-Disposition: attachment;filename="'. $fileName . '.xlsx"');
    
    header('Cache-Control: max-age=0');
    
    header('Cache-Control: max-age=1');
    
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    
    header('Cache-Control: cache, must-revalidate');
    
    header('Pragma: public');

    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

    $writer->save('php://output');

    die();

}