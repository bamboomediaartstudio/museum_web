<?php


use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

require 'vendor/autoload.php';

require_once '../core/init.php';

$status['init'] = true;

$status['id'] = Input::get('id');

$validate = new Validate();

$validation = $validate->check($_GET, array(

    'id' => array('display'=>'id', 'required' => true)

    
));

if($validation->passed()){

    $query = DB::getInstance()->query('SELECT * FROM museum_booking_institutions WHERE id = ?', [Input::get('id')]);    

    $result = $query->first();

    $name = $result->institution_name;

    $cuit = $result->institution_cuit;

    $address = $result->institution_address;

    $phone = $result->institution_phone;

    $email = $result->institution_email;

    $students = $result->institution_students;

    $level = $result->eduction_level;

    $iva = strtolower($result->iva);

    $finalIVA = '';

    if($iva == 'consumidor final'){

        $finalIVA = 'CF';

    }else if($iva == 'exento'){

        $finalIVA = 'EX';

    }else if($iva == 'responsable monotributo'){

        $finalIVA = 'MO';

    }else if($iva == 'responsable inscripto'){

        $finalIVA = 'RI';

    }else{

        $finalIVA = '';
    }

    $scholarship = ($result->scholarship_asigned == 0) ? 'SI' : 'NO';

    $helper = new Sample();
    if ($helper->isCli()) {
        $helper->log('opsss!' . PHP_EOL);

        return;
    }
    $spreadsheet = new Spreadsheet();

    $spreadsheet->getProperties()->setCreator('Museo Del Holocausto de Buenos Aires')
    ->setLastModifiedBy('Museo Del Holocausto de Buenos Aires')
    ->setTitle('Office 2007 XLSX Test Document')
    ->setSubject('Office 2007 XLSX Test Document')
    ->setDescription('Documento creado dinámicamente para upload de instituciones a Plataforma - Museo Del Holocausto')
    ->setKeywords('export de institiucion')
    ->setCategory('export de institucion');

    $spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A1', 'institución')
    ->setCellValue('B1', $name)
    ->setCellValue('A2', 'cuit')
    ->setCellValue('B2', $cuit)
    ->setCellValue('A3', 'dirección')
    ->setCellValue('B3', $address)
    ->setCellValue('A4', 'teléfono')
    ->setCellValue('B4', $phone)
    ->setCellValue('A5', 'email')
    ->setCellValue('B5', $email)
    ->setCellValue('A6', 'cantidad de alumnos')
    ->setCellValue('B6', $students)
    ->setCellValue('A7', 'tipo de institución')
    ->setCellValue('B7', $level)
    ->setCellValue('A8', 'beca')
    ->setCellValue('B8', $scholarship)
    ->setCellValue('A9', 'situación frente al IVA')
    ->setCellValue('B9', $finalIVA)

    ;


    $spreadsheet->getActiveSheet()->setTitle('Simple');

    $spreadsheet->setActiveSheetIndex(0);

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'. $name . '.xlsx"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: cache, must-revalidate');
    header('Pragma: public');

    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

    $writer->save('php://output');

    die();

}