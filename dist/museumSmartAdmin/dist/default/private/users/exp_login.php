<?php

header('Access-Control-Allow-Origin: *'); //allow everybody

require_once 'core/init.php';

$status['init'] = true;

$db = DB::getInstance();

if(Input::exists()){

	$status['input'] = true;

	//if(Token::check(Input::get('token'))){

		$status['valid'] = 'valid token';

		$validate = new Validate();

		$validation = $validate->check($_POST, array(

			'email' => array('display'=>'email', 'required' => true),

			'password' => array('display'=>'contraseña', 'required' => true, 'min'=>3)

		));

		if($validation->passed()){

			$user = new User();

			$remember = (Input::get('remember') === 'on') ? true : false;

			$login = $user->login(Input::get('email'), Input::get('password'), $remember);

			if($login){

				$status['success'] = true;

				$db->insert('users_login_data', array(

					'user_id' => $user->data()->id,

					'ip' => $_SERVER['REMOTE_ADDR'],

					'date_time' => date('Y/m/d H:i:s'),

					'referer' => $_SERVER["HTTP_REFERER"],

					'browser_name' => Input::get('browserName'),

					'browser_engine' => Input::get('browserEngine'),

				));

				$status['login-success'] = 'el login fue exitoso!';

				Token::delete(Input::get('token'));

			}else{


				$accessQuery = $db->query('SELECT * FROM museum_experience_access WHERE email = ?', [Input::get('email')])->first();

				if($accessQuery->password === Hash::make(Input::get('password'), $accessQuery->salt)){

					$status['login-success'] = 'el login fue exitoso! full access';

					$status['timestamp'] = $accessQuery->expire;

				}else{

					$status['errors'] = true;

					$status['errorsList'][0] = 'El usuario/email o la contraseña no coinciden.';

				}				

			}

		}else{

			$status['errors'] = true;

			$status['errorsList'][0] = 'El usuario/email o la contraseña no coinciden.';


		}
	//}

	$status['token'] = 'invalid';
}

echo json_encode($status);

?>
