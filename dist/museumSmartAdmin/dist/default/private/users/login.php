<?php

require_once 'core/init.php';

$status['init'] = true;

if(Input::exists()){

	$status['input'] = true;

	if(Token::check(Input::get('token'))){

		$status['valid'] = 'valid token';

		$validate = new Validate();

		$validation = $validate->check($_POST, array(

			'email' => array('display'=>'email', 'required' => true),

			'password' => array('display'=>'contraseña', 'required' => true, 'min'=>3)

		));

		if($validation->passed()){

			$user = new User();

			$remember = (Input::get('remember') === 'on') ? true : false;

			$login = $user->login(Input::get('email'), Input::get('password'), $remember);

			if($login){

				$status['success'] = true;

				$db = DB::getInstance();

				$db->insert('users_login_data', array(

					'user_id' => $user->data()->id,

					'ip' => $_SERVER['REMOTE_ADDR'],

					'date_time' => date('Y/m/d H:i:s'),

					'referer' => $_SERVER["HTTP_REFERER"],

					'browser_name' => Input::get('browserName'),

					'browser_engine' => Input::get('browserEngine'),

				));

				$status['login-success'] = 'el login fue exitoso!';

				Token::delete(Input::get('token'));

			}else{

				$errors = array();

				foreach($user->loginErrors() as $error){

					$errors[] = $error;

				}

				$status['errors'] = true;

				$status['errorsList'] = $errors;

			}

		}else{

			$errors = array();

			foreach($validation->errors() as $error){

				$errors[] = $error;
			}

			$status['errors'] = true;

			$status['errorsList'] = $errors;

		}
	}

	$status['token'] = 'invalid';
}

echo json_encode($status);

?>