<?php

/**
 * @summary Add new alert to the DB
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0	
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the includes */

require_once '../../core/init.php';

/**all the variables */

$status['init'] = true;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_alert_add'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'error');

/**if there is no token, print and out */

//if(!Token::check(Input::get('token'))) printData(0, 'token problem');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'title' => array('display'=> 'title', 'required' => true),

	'caption' => array('display'=> 'caption', 'required' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	if($user->isLoggedIn()){

		$db->insert('museum_alerts',[

			'title'=> Input::get('title'),

			'caption'=> strip_tags(Input::get('caption'), '<br><b><strong><i>'),

			'button_label'=> Input::get('button_label'),

			'link'=> Input::get('link'),

			'alert_type'=> Input::get('radio_group')
		]);

		$lastId = $db->lastId();

		addLogData($user);

		printData(1, 'ok');
		
	}

}




/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The alert ' . Input::get('title') .  ' was added.';

	$userString = 'Agregaste una nueva alerta.';

	Logger::addLogData($user->data()->id, 

		'user',

		$systemString,

		$userString,

		'alerts',

		'alerts');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>