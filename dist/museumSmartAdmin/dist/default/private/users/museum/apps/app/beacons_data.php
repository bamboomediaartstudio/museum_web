<?php

/**
 * @summary Delete image from batch.
 *
 * @description -
 *
 * @author Mariano Makedonsky / Colo Baggins <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the includes */

require_once '../../../core/init.php';


setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

header("Content-type: text/xml");

$query = DB::getInstance()->query('SELECT * FROM beacons_tracking_info');

$xml = '<data>';

$data = $query->first();




$xml .= '<UUID>' . $data->UUID . '</UUID>';

$xml .= '<family_major>' . $data->family_major . '</family_major>';

$xml .= '<family_reading_distance>' . $data->family_reading_distance . '</family_reading_distance>';

$xml .= '<family_proximity>' . $data->family_proximity . '</family_proximity>';

$xml .= '<lock_proximity>' . $data->lock_proximity . '</lock_proximity>';

$xml .= '<lock_reading_distance>' . $data->lock_reading_distance . '</lock_reading_distance>';

$xml .= '<lock_major>' . $data->lock_major . '</lock_major>';

$xml .= '</data>';

echo $xml;


//foreach($query->results() as $category){ 


?>