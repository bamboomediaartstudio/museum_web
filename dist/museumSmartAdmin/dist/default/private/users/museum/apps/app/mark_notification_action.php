<?php

/**
 * @summary Delete image from batch.
 *
 * @description -
 *
 * @author Mariano Makedonsky / Colo Baggins <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the includes */

require_once '../../../core/init.php';

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'id', 'required' => true), 

	'action' => array('display'=> 'action', 'required' => true)
	
	)
);

$status = 0;

if($validation->passed()){

	if(Input::get('action') == 'fav'){

		DB::getInstance()->query('UPDATE push_notifications SET marked_as_favourite = marked_as_favourite + 1 WHERE id = ?', [Input::get('id')]);

	}else if(Input::get('action') == 'deleted'){

		DB::getInstance()->query('UPDATE push_notifications SET marked_as_deleted = marked_as_deleted + 1 WHERE id = ?', [Input::get('id')]);

	}else if(Input::get('action') == 'read'){

		DB::getInstance()->query('UPDATE push_notifications SET marked_as_read = marked_as_read + 1 WHERE id = ?', [Input::get('id')]);

	}

	$status = "status=1";

}else{

	$status = "status=0";

}


echo $status;



?>
