<?php

/**
 * @summary Delete image from batch.
 *
 * @description -
 *
 * @author Mariano Makedonsky / Colo Baggins <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the includes */

require_once '../../../core/init.php';

$status = "sts =0";

$db = DB::getInstance();

//validar si nos llega un unique_id

$validate = new Validate();

$validation = $validate->check($_POST, array('uniqueId' => array('display' => 'uniqueId', 'required' => true)));

//validar si pasa...

if($validation->passed()){

	//chequear si existe...

	$query = DB::getInstance()->query("SELECT * FROM app_device_users WHERE unique_id = ?", [Input::get('uniqueId')]);

	$results = $query->results();

	//si no existe, agregarlo a app_device_users

	if($results == null){

		$insertUserData = $db->insert('app_device_users',  ['unique_Id' => Input::get('uniqueId')] );

		//luego añadir la sesion a app_device_user_sessions...

		if($insertUserData){

			$insertSessionData = $db->insert('app_device_user_sessions', ['unique_Id' => Input::get('uniqueId')]);

			//y por ultimo almacenar info del device...

			if($insertSessionData){

				$insertDeviceInfo = $db->insert('app_device_data',
					[

						'av_hardware_disable'=> (Input::get('avHardwareDisable') == true) ? 1 : 0,

						'has_accessibility'=> (Input::get('hasAccessibility') == true) ? 1 : 0,

						'has_audio'=> (Input::get('hasAudio') == true) ? 1 : 0,

						'has_audio_encoder'=> (Input::get('hasAudioEncoder') == true) ? 1 : 0, 

						'has_embedded_video'=> (Input::get('hasEmbeddedVideo') == true) ? 1 : 0,

						'has_ime'=> (Input::get('hasIME') == true) ? 1 : 0,

						'has_mp3'=> (Input::get('hasMP3') == true) ? 1 : 0,

						'has_screen_broadcast'=> (Input::get('hasScreenBroadcast') == true) ? 1 : 0,

						'has_screen_playback'=> (Input::get('hasScreenPlayback') == true) ? 1 : 0,

						'has_streaming_audio'=> (Input::get('hasStreamingAudio') == true) ? 1 : 0,

						'has_tls'=> (Input::get('hasTLS') == true) ? 1 : 0,

						'has_video_encoder'=> (Input::get('hasVideoEncoder') == true) ? 1 : 0,

						'adobe_language'=> Input::get('adobeLanguage'),

						'adobe_manufacturer'=> Input::get('adobeManufacturer'),

						'adobe_os'=> Input::get('adobeOs'),

						'pixel_aspect_ratio'=> Input::get('pixelAspectRatio'),

						'player_type'=> Input::get('playerType'),

						'screen_color'=> Input::get('screenColor'),

						'screen_dpi'=> Input::get('screenDPI'),

						'screen_resolution_x'=> Input::get('screenResolutionX'),

						'screen_resolution_y'=> Input::get('screenResolutionY'),

						'brand'=> Input::get('brand'),

						'language'=> Input::get('language'),

						'country'=> Input::get('country'),

						'manufacturer'=> Input::get('manufacturer'),

						'model'=> Input::get('model'),

						'name'=> Input::get('name'),

						'os_name'=> Input::get('osName'),

						'os_api_level'=> Input::get('osApiLevel'),

						'os_type'=> Input::get('osType'),

						'os_version'=> Input::get('osVersion'),

						'time_zone_abbreviation'=> Input::get('timeZoneAbbreviation'),

						'time_zone_description'=> Input::get('timeZoneDescription'),

						'time_zone_id'=> Input::get('timeZoneId'),

						'time_zone_offset'=> Input::get('timeZoneOffset'),

						'unique_id'=> Input::get('uniqueId'),

						'year_class'=> Input::get('yearClass')]

					);

				if($insertDeviceInfo){

					$status = "sts =1";

				}else{

					$status = "sts =0";

				}
			}else{

				$status = "sts =0";

			}
	
		}else{

			$status = "sts =0";

		}

	}else{

		$insertSessionData = $db->insert('app_device_user_sessions', ['unique_Id' =>Input::get('uniqueId')]);

		if($insertSessionData){

			$status = "sts =1";
	
		}else{

			$status = "sts =0";

		}

	}

}

echo $status;

?>