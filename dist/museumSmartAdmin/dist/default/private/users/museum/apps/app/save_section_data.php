<?php

/**
 * @summary Delete image from batch.
 *
 * @description -
 *
 * @author Mariano Makedonsky / Colo Baggins <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the includes */

require_once '../../../core/init.php';

$status = "sts=0";

$db = DB::getInstance();

//validar si nos llega un unique_id

$validate = new Validate();

$validation = $validate->check($_POST, 

	array(

		'uniqueId' => array('display' => 'uniqueId', 'required' => true),

		'appId' => array('display' => 'appId', 'required' => true)

));

//validar si pasa...

if($validation->passed()){

	$insertSessionData = $db->insert('app_device_menu_clicks', 

		['unique_Id' =>Input::get('uniqueId'),

		'app_id' =>Input::get('appId')

	]);

		if($insertSessionData){

			$status = "sts=1";
	
		}else{

			$status = "sts=0";

		}

}

echo $status;

?>