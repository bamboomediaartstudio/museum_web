<?PHP

/**
 * @summary Delete image from batch.
 *
 * @description -
 *
 * @author Mariano Makedonsky / Colo Baggins <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the includes */

require_once '../../../core/init.php';

set_time_limit(60 * 5);

$validate = new Validate();

$validation = $validate->check($_GET, array('title' => array('display'=> 'content', 'required' => true)));

$status['init'] = true;

$db = DB::getInstance();

if($validation->passed()){

    $date = date('Y-m-d H:i:s');

    $arrayQuery = [

      'title' => Input::get('title'),

      'content' => Input::get('content'),

      'is_ios' => (Input::get('is_ios') == true) ? 1 : 0,
      
      'is_android' => (Input::get('is_android') == true) ? 1 : 0,

      'priority' => Input::get('priority'),
      
      'added' => $date,

      'sent' => $date,

      'active' => 1,

      'deleted'  => 0
    ];

    $insertDbQuery = $db->insert('push_notifications', $arrayQuery);

    if($insertDbQuery){

        $lastId = $db->lastId();

        $response = sendMessage($lastId, $date);

        $return["allresponses"] = $response;

        $return = json_encode($return);

        $data = json_decode($response, true);

        print_r($data);

        $id = $data['id'];

        print_r($id);

        $db->update("push_notifications", $lastId, ["internal_id"=>$id]);

        print("\n\nJSON received:\n");

        print($return);

        print("\n");
    }

}else{

    echo "falta dato";

}

function sendMessage($lastId, $date) {

    $headings = array( "en" => Input::get('title') );

    $subtitle = array( "en" => '' );
    
    $content = array( "en" => Input::get('content') );

    $fields = array(
       
        'app_id' => "a5b314e3-5ef8-4bea-b91d-091db949204b",
        
        'included_segments' => array('All'),

        'isAndroid'=> Input::get('is_android'),

        'isIos' => Input::get('is_ios'),

        'priority' => Input::get('priority'),
        
        'data' => array("notification_id" => $lastId, "date"=>$date),
        
        'headings' => $headings,

        'subtitle' => $subtitle,
        
        'contents' => $content
    );
    
    $fields = json_encode($fields);
   
    print("\nJSON sent:\n");
    
    print($fields);
    
    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json; charset=utf-8',
        'Authorization: Basic YzM5MWY3ZDUtZjM3MS00ZGQ3LThlNGQtMDJmYWM2MzFmOWIy'
    ));
    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    
    curl_setopt($ch, CURLOPT_POST, TRUE);
    
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    
    $response = curl_exec($ch);
    
    curl_close($ch);
    
    return $response;
}

?>