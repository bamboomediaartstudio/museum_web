<?php

/**
 * @summary Delete image from batch.
 *
 * @description -
 *
 * @author Mariano Makedonsky / Colo Baggins <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the includes */

require_once '../../../core/init.php';

$validate = new Validate();

$validation = $validate->check($_POST, array('descriptor' => array('display'=> 'descriptor', 'required' => true)));

$status = 0;

if($validation->passed()){

	DB::getInstance()->query('UPDATE app_museum_categories SET unlocked_times = unlocked_times + 1 WHERE alt_source = ?', [Input::get('descriptor')]);

	$status = "status=1";

}else{

	$status = "status=0";

}


echo $status;



?>
