<?php

/**
 * @summary Delete image from batch.
 *
 * @description -
 *
 * @author Mariano Makedonsky / Colo Baggins <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the includes */

require_once '../../../core/init.php';

$validate = new Validate();

$validation = $validate->check($_POST, array(

  'idFamily' => array('display' => 'idFamily', 'required' => true),

  'idInstance' => array('display' => 'idInstance', 'required' => true)));

$status = "status=0";

if($validation->passed()){

	$query = DB::getInstance()->query("SELECT * FROM beacons_families_relations WHERE id_family = ? AND id_instance = ?", [Input::get('idFamily'), Input::get('idInstance')]);

	$results = $query->results();

	if($results == null){

		$data = [

			'id_family' => Input::get('idFamily'),

			'id_instance' => Input::get('idInstance'),

			'unlocked_times' => 1];

		$insert = DB::getInstance()->insert('beacons_families_relations', $data);

		if($insert){

			$status = "status=1";
		}

	}else{

		$update = DB::getInstance()->query('UPDATE beacons_families_relations SET unlocked_times = unlocked_times + 1 WHERE id_family = ? AND id_instance = ?', [Input::get('idFamily'), Input::get('idInstance')]);

		if($update){

			$status = "status=1";
		}
	}

}

echo $status;



?>
