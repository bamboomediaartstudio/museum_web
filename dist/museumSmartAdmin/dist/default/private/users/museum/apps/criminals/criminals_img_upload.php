<?php

/**
 * @summary Add new Surivor data
 *
 * @description -
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Adds Survivor data to the DB. For App use
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../../core/init.php';

require '../../images_general/AddImageSingle.class.php';

include '../../general/UpdateCategoriesDates.class.php';

/**all the variables */


$allowProgramDownload = 0;

$status['init'] = true;

$db = DB::getInstance();


/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_add_criminal_img'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');


/**initialize validation process */


//change img url and upload

$criminalId = Input::get('id');

$objAddSingleImage = new AddImageSingle("picture");

try{

  //upload image (Edit mode)
  $uploadPath = $objAddSingleImage->loadImage($criminalId, "criminals");

  //Update db with image path (picture field)
  $updateDbImgPath = $objAddSingleImage->updateDb('app_museum_criminals', $criminalId, ['picture'=> $uploadPath]); // (picture field in db)

  if($updateDbImgPath == 1) {

  	$objUpdateCatDate = new UpdateCategoriesDates("criminales");

	$objUpdateCatDate->updateModifiedDate();

    printData(0, 'Imagen actualizada.');

  }else{

    printData(0, "Error updating db.");

  }


}catch(Exception $e){

  printData(0, $e->getMessage()." - ".$e->getCode());

}


/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The Criminals App ' . Input::get('name') .  ' was added.';

	$userString = 'Agregaste datos a Criminales <b>' . ' ' . '</b> al sistema.';

	Logger::addLogData($user->data()->id,

		'user',

		$systemString,

		$userString,

		'criminals',

		'criminals');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}


?>
