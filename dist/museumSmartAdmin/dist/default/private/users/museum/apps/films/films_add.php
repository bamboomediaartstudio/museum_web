<?php

/**
 * @summary Add new films to the DB
 *
 * @description -
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Adds Films to the DB.
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../../core/init.php';

//require '../../images_general/AddImageSingle.class.php';
require '../../images_general/AddImageSingleFilm.class.php';

include '../../general/UpdateCategoriesDates.class.php';


/**all the variables */

$allowProgramDownload = 0;

$status['init'] = true;

$db = DB::getInstance();

$retinaOriginalWidth = 2000;

$retinaOriginalHeight = 1000;


/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_add_films'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');


/**initialize validation process */

$validate = new Validate();


$validation = $validate->check($_POST, array(

  'title' => array('display' => 'title', 'required' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

  $user = new User();

  if($user->isLoggedIn()){

    $arrayQuery = [
      'title' => Input::get('title'),

      'director' => Input::get('director'),

      'year' => Input::get('year'),

      'id_country' => Input::get('hidden-country-id'),

      'synopsis' => Input::get('synopsis'),

      'description' => Input::get('description'),

      'link_netflix' => Input::get('link_netflix'),

      'link_youtube' => Input::get('link_youtube'),

      'link_imdb' => Input::get('link_imdb'),

      'link_other' => Input::get('link_other'),

      'active' => 1,

      'deleted'  => 0
    ];


    $insertDbQuery = $db->insert('app_museum_films', $arrayQuery);

    if($insertDbQuery){

        $lastId = $db->lastId();


        $totalCategoriesToAdd = count(Input::get('checkboxes'));

        $totalCategoriesAdded = 0;

    		foreach(Input::get('checkboxes') as $selected){

    			$insert = $db->insert('app_museum_films_relations',
    				[
    					'id_film'     => $lastId,

    					'id_category' => $selected,

              'active'      => 1,

              'deleted'     => 0

    				]

    			);

          //...

          $selectData = $db->query('SELECT * from app_museum_categories WHERE id = ?', [$selected]);

          $objUpdateCatDate = new UpdateCategoriesDates($selectData->name);

          $objUpdateCatDate->updateModifiedDate();


          $totalCategoriesAdded++;

    		}

        //Checkear que todas las categorias se hayan agregado a la db

        if($totalCategoriesToAdd == $totalCategoriesAdded){

          checkAndAddImages($lastId);

        }else{

          printData(1, 'Error al cargar categrias de film');

        }




    }else{

      //No se cargaron datos del formulario de manera exitosa. No cargar imagenes.
      printData(0, 'Error, no se cargaron datos del formulario, no cargamos tampoco la imagen.');

    }


  }


}

function checkAndAddImages($lastId){

  //Si la carga de datos del formulario fue exitosa, proceder a ver si se adjunto imagen

  //Check si se carga imgen

  if(!file_exists($_FILES['picture']['tmp_name']) || !is_uploaded_file($_FILES['picture']['tmp_name'])){
    global $user;
    addLogData($user);

    //printData(1, 'ok without image...por que?');

    printData(0, 'Subido correctamente - Pelicula sin imagen.');

  }else{

    //Ir a crear img y cargarla en db

    $objAddSingleImage = new AddImageSingleFilm("picture");

    try{

      //upload image multiple sizes
      $uploadPath = $objAddSingleImage->loadImage($lastId, "films");

      //Update db with image path (picture field)
      $updateDbImgPath = $objAddSingleImage->updateDb('app_museum_films', $lastId, ['picture'=> $uploadPath]);

      if($updateDbImgPath == 1) {

        printData(0, 'Imagen subida y en DB.');

      }else{

        printData(0, "Error updating db.");

      }


    }catch(Exception $e){

      printData(0, $e->getMessage()." - ".$e->getCode());

    }

  }

}


/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The Gueto (Gueto App) ' . Input::get('name') .  ' was added.';

	$userString = 'Agregaste el Gueto <b>' . ' ' . '</b> al sistema.';

	Logger::addLogData($user->data()->id,

		'user',

		$systemString,

		$userString,

		'guetos',

		'guetos');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>
