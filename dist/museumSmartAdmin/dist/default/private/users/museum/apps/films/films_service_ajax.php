<?php

/**
 * @summary
 *
 * @description -
 *
 * @author Mariano Makedonsky / Colo Baggins <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the includes */

require_once '../../../core/init.php';

/**validate.. */

$user = new User();

$db = DB::getInstance();

if($user->isLoggedIn()){

	$validate = new Validate();

	$validation = $validate->check($_POST, array(

			'id' => array('display'=> 'id', 'required' => true),

			'action' => array('display'=> 'action', 'required' => true)
		)
	);


}



if($validation->passed()){

	if(Input::get('action') == 'updateCategories'){

    $arrCategories = Input::get('arrCategories');

    //Primero borrar las relaciones con este id Film
    $deleteOlderRelations = $db->delete('app_museum_films_relations', ['id_film'=> Input::get('id')]);

    //Luego agregar los valores de del arrCategories en columna id_category
    if($arrCategories){

      $totalCatsToAdd = count($arrCategories);

      $totalAdded = 0;

      foreach($arrCategories as $selected){

        $insert = $db->insert('app_museum_films_relations',
          [
            'id_film'     => Input::get('id'),

            'id_category' => $selected,

            'active'      => 1,

            'deleted'     => 0

          ]

        );

        $totalAdded++;

      }

      if($totalCatsToAdd == $totalAdded){

        $status['msg'] = 'Se actualizaron las categorias';

  			$status['button'] = 'Entendido';

      }else{

        $status['msg'] = 'Error al actualizar categorias. Una o mas categorias no se actualizaron.';

  			$status['button'] = 'Ok';

      }

    }

		echo json_encode($status);

	}

}



?>
