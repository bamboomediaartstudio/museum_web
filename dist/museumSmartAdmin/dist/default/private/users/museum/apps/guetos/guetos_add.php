<?php

/**
 * @summary Add new Guetos to the DB - APP
 *
 * @description -
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Adds Guetos to the DB. For App use
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../../core/init.php';

require '../../../../libraries/claviska/SimpleImage.php';

include '../../general/UpdateCategoriesDates.class.php';

/**all the variables */

$filesManager = new FilesManager();

$ds = DIRECTORY_SEPARATOR;

$image = new \claviska\SimpleImage();

$allowProgramDownload = 0;

$status['init'] = true;

$db = DB::getInstance();

$retinaOriginalWidth = 2000;

$retinaOriginalHeight = 1000;


/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_add_gueto'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');


/**initialize validation process */

$validate = new Validate();


$validation = $validate->check($_POST, array(

  'name' => array('display' => 'name', 'required' => true),

  'country' => array('display' => 'country', 'required' => true),

  'year_start' => array('display' => 'year_start', 'required' => true),

  'year_end' => array('display' => 'year_end', 'required' => true),

  'guetoLat' => array('display' => 'guetoLat', 'required' => true),

  'guetoLng' => array('display' => 'guetoLng', 'required' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

  $user = new User();

  if($user->isLoggedIn()){

    $arrayTest = [
      'name' => Input::get('name'),

      'id_country' => Input::get('hidden-country-id'),

      'year_start' => Input::get('year_start'),

      'year_end' => Input::get('year_end'),

      'lat' => Input::get('guetoLat'),

      'lng' => Input::get('guetoLng'),

      'description' => Input::get('description'),

      'active' => 1,

      'deleted'  => 0
    ];


    $testDbQuery = $db->insert('app_museum_guetos', $arrayTest);
    
    if($testDbQuery){

      $objUpdateCatDate = new UpdateCategoriesDates("guetos");

      $objUpdateCatDate->updateModifiedDate();

       printData(0, 'Gueto agregado correctamente');
   
    }else{
     
      printData(1, 'Error al agregar Gueto. Consulte con el administrador');
   
    }


  }


}



/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The Gueto (Gueto App) ' . Input::get('name') .  ' was added.';

	$userString = 'Agregaste el Gueto <b>' . ' ' . '</b> al sistema.';

	Logger::addLogData($user->data()->id,

		'user',

		$systemString,

		$userString,

		'guetos',

		'guetos');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>
