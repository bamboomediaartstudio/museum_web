<?php

/**
 * @summary Delete image from batch.
 *
 * @description -
 *
 * @author Mariano Makedonsky / Colo Baggins <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the includes */

require_once '../../../core/init.php';

include '../../general/UpdateCategoriesDates.class.php';

/**validate.. */

$user = new User();

if($user->isLoggedIn()){

	$validate = new Validate();

	$validation = $validate->check($_POST, array(

			'key' => array('display'=> 'key', 'required' => true)
		)
	);


}


if($validation->passed()){

	$objUpdateCatDate = new UpdateCategoriesDates("guetos");

	$objUpdateCatDate->updateModifiedDate();

	$fields = array('deleted'=>1);

	DB::getInstance()->update('app_museum_guetos_images', Input::get('key'), $fields);

	$status['msg'] = 'Se elimino la imagen';

	echo json_encode($status);

	return;
}

/*
if($validation->passed()){

	$fields = array('deleted'=>1);

	DB::getInstance()->update('museum_images', Input::get('key'), $fields);

	$status['msg'] = 'Se elimino la imagen';

	echo json_encode($status);

	return;
}
*/

?>
