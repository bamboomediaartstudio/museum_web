<?php

class Keywords{

  private $id;
  private $name;
  private $active;
  private $deleted;

  public function __construct(){

  }

  /**
  * @function checkExist
  * @description Check with name if keyword exists
  *
  * @param {string} $name 	- 	 keyword name
  * @return 0 not exist / 1 exist
  */

  public function checkExist($name){

    $finded = 0;

    $getExistsQuery = DB::getInstance()->query("SELECT count(*) as cantFinded, id as idKeyword FROM app_museum_prensa_keywords WHERE name = ? AND deleted = ?", array($name, 0));

    $keywordsResult = $getExistsQuery->results(true);     // if returns 0 doesn't exists


    if($keywordsResult[0]["cantFinded"] > 0){ //Finded!

      //$finded = 1;

      //Si lo encontró devuelvo directamente el id del key;
      $finded = $keywordsResult[0]["idKeyword"];

    }else{

      $finded = 0;

    }

    return $finded;

  }

  /**
  * @function addKeyword
  * @description add keyword
  *
  * @param {string} $name 	- 	 keyword name
  * @return 0 Error / lastId added
  */

  public function addKeyword($name){

    $inserted = 0;

    $arrayKeywordsQuery = [

      'name'    => $name,

      'active'  => 1,

      'deleted' => 0
    ];

    $db = DB::getInstance();

    $insertKeywordQuery = $db->insert('app_museum_prensa_keywords', $arrayKeywordsQuery);

    if($insertKeywordQuery){

      return $db->lastId();

    }else{

      throw new Exception("Error al agregar keyword a db", 0);

    }

  }

  public function addKeywordPrensaRelation($idPrensa, $idKeyword){

    $inserted = 0;

    $arrayKeywordsQuery = [

      'id_prensa'   => $idPrensa,

      'id_keyword'  => $idKeyword,

      'active'      => 1,

      'deleted'     => 0

    ];

    $db = DB::getInstance();

    $insertRelationQuery = $db->insert('app_museum_prensa_keywords_relations', $arrayKeywordsQuery);

    return ($insertRelationQuery) ? 1 : 0;

  }

  public function hideKeywordRelation($idRelation){
    
  }

}

?>
