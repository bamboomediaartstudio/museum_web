<?php

/**
 * @summary Add new Prensa data
 *
 * @description -
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Adds Prensa data to the DB. For App use
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../../core/init.php';

require 'Keywords.class.php';

require '../../images_general/AddImageSingle.class.php';

include '../../general/UpdateCategoriesDates.class.php';

/**all the variables */


$allowProgramDownload = 0;

$status['init'] = true;

$db = DB::getInstance();

$user = new User();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_add_prensa'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');


/**initialize validation process */

$validate = new Validate();


$validation = $validate->check($_POST, array(

  'title' => array('display' => 'title', 'required' => true),

  'id_newspaper' => array('display' => 'id_newspaper', 'required' => true),

  'date' => array('display' => 'date', 'required' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

//  $user = new User();

  if($user->isLoggedIn()){

    $arrayQuery = [
      'title' => Input::get('title'),

      'id_newspaper' => Input::get('id_newspaper'),

      'date' => Input::get('date'),

      'active' => 1,

      'deleted'  => 0
    ];



    $insertDbQuery = $db->insert('app_museum_prensa', $arrayQuery);

    if($insertDbQuery){

      $lastId = $db->lastId();

      $objUpdateCatDate = new UpdateCategoriesDates("prensa");

        $objUpdateCatDate->updateModifiedDate();

        $addPrensaTags = addPrensaTags($lastId, Input::get('keywords'));

        if($addPrensaTags == 1){

          checkAndAddImages($lastId, "app_museum_prensa", "prensa");

        }else{

          printData(1, 'Error al cargar tags');

        }

    }else{

      printData(1, 'Error al cargar prensa');

    }


  }


}

/**
* @function checkAndAddImages
* @description Add keywords to prensa - json decodes, parse and add
*
* @param {int} $lastId 	-
* @param {string} $table 	-         Nombre de la tabla
* @param {string} $folderName 	- 	Carpeta de imgs, dentro de source
*/
function checkAndAddImages($lastId, $table, $folderName){

  //Si la carga de datos del formulario fue exitosa, proceder a ver si se adjunto imagen

  //Check si se carga imgen

  if(!file_exists($_FILES['picture']['tmp_name']) || !is_uploaded_file($_FILES['picture']['tmp_name'])){
    global $user;
    addLogData($user);

    //printData(1, 'ok without image...por que?');

    printData(0, 'Subido correctamente - Prensa sin imagen.');

  }else{

    //Ir a crear img y cargarla en db

    $objAddSingleImage = new AddImageSingle("picture");

    try{

      //upload image multiple sizes
      $uploadPath = $objAddSingleImage->loadImage($lastId, $folderName);

      //Update db with image path (picture field)
      $updateDbImgPath = $objAddSingleImage->updateDb($table, $lastId, ['picture'=> $uploadPath]);

      if($updateDbImgPath == 1) {

        printData(0, 'Imagen subida y en DB.');

      }else{

        printData(0, "Error updating db.");

      }


    }catch(Exception $e){

      printData(0, $e->getMessage()." - ".$e->getCode());

    }

  }

}


/**
* @function addPrensaTags
* @description Add keywords to prensa - json decodes, parse and add
*
* @param {json} $keywods 	- 	 stringyfy jquery array.
*/

function addPrensaTags($idPrensa, $keywords){

  //Comes from stringify jquery array, decode json

  $keywordsDecoded = json_decode($keywords);

  $arrLength = count($keywordsDecoded);

  $db = DB::getInstance();

  $realCantKeywordsToAdd = 0;

  $keywordsAdded = 0;

  $relationsAdded = 0;

  foreach($keywordsDecoded as $keywordResult){

    $objKeywords = new Keywords();

    $keywordsExist = $objKeywords->checkExist($keywordResult);


    if($keywordsExist==0){    //Only add to db id keyword name doesnt exist already

      $realCantKeywordsToAdd++;  //Count only from origin array haw many keys to add

      try{

          $addKeywordLastId = $objKeywords->addKeyword($keywordResult);

          if($addKeywordLastId!=0){

            $keywordsAdded++; //Keyword added to db

            //Si ya se agrego la keyword a la db, guardamos la relación -
            //La relación debe contemplar key nuevas como ENCONTRADOS de la db DE KEYS

            $doRelation= $objKeywords->addKeywordPrensaRelation($idPrensa, $addKeywordLastId);

            if($doRelation == 1){

              $relationsAdded++;    //relation saved

            }

          }


      }catch(Exception $e){

        echo "Error al agregar keyword a db: ".$e->getMessage() . " - ".$e->getCode();

      }

    }else{

      //La relación debe contemplar key nuevas como ENCONTRADOS de la db DE KEYS. Este caso son keys encontrados en la db

      $addKeywordLastId = $keywordsExist;

      $doRelation= $objKeywords->addKeywordPrensaRelation($idPrensa, $addKeywordLastId);

      if($doRelation == 1){

        $relationsAdded++;    //relation saved

      }

    }

  }

  if(($realCantKeywordsToAdd == $keywordsAdded) && ($relationsAdded == $arrLength)) { //La cantidad de relaciones guardas tiene que ser igual al array de tags que envia el user.

    //Todo insertado en la db
    return 1;

  }else{

    //Keys no insertadas
    return 0;

  }


}


/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The Prensa App ' . Input::get('name') .  ' was added.';

	$userString = 'Agregaste datos a Prensa <b>' . ' ' . '</b> al sistema.';

	Logger::addLogData($user->data()->id,

		'user',

		$systemString,

		$userString,

		'prensa',

		'prensa');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

  $status['from'] = $from;

	$status['test'] = "ok";

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>
