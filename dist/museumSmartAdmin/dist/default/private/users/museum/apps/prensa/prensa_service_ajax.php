<?php

/**
 * @summary
 *
 * @description -
 *
 * @author Mariano Makedonsky / Colo Baggins <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the includes */

require_once '../../../core/init.php';

require 'Keywords.class.php';

include '../../general/UpdateCategoriesDates.class.php';

/**validate.. */

$user = new User();

$db = DB::getInstance();

if($user->isLoggedIn()){

	$validate = new Validate();

	$validation = $validate->check($_POST, array(

		'id' => array('display'=> 'id', 'required' => true),

		'action' => array('display'=> 'action', 'required' => true)
	)
);


}



if($validation->passed()){

	//...
	//cualquiera que sea la accion, indicar cambio....

	$objUpdateCatDate = new UpdateCategoriesDates("prensa");

	$objUpdateCatDate->updateModifiedDate();

	//...

	if(Input::get('action') == 'deleteTag'){

		$fields = array(

			'id'			=> Input::get('id'),

			'deleted' => 1

		);

		$db->update('app_museum_prensa_keywords_relations', Input::get('id'), $fields);

		$status['msg'] = 'Se elimino la relación keyword/Diario';
		$status['button'] = 'Entendido';

		echo json_encode($status);

		return;

	}else if(Input::get('action') == 'addDirectTag'){



		// (Input::get('id')) es el id de prensa en este caso

		$arrayQuery = [
			'id_prensa'   => Input::get('id'),

			'name'				=> Input::get('tagName')

		];

		$objKeywords = new Keywords();

		$keywordsExist = $objKeywords->checkExist(Input::get('tagName'));

		if($keywordsExist==0){    //Only add to db id keyword name doesnt exist already

			try{

				$addKeywordLastId = $objKeywords->addKeyword(Input::get('tagName'));

				if($addKeywordLastId!=0){

						//Si ya se agrego la keyword a la db, guardamos la relación -
						//La relación debe contemplar key nuevas como ENCONTRADOS de la db DE KEYS

					$doRelation= $objKeywords->addKeywordPrensaRelation(Input::get('id'), $addKeywordLastId);

				}

				$status['msg'] = 'Se agregó nuevo keyword';

				$status['button'] = 'Entendido';


			}catch(Exception $e){

				echo "Error al agregar keyword a db: ".$e->getMessage() . " - ".$e->getCode();

				$status['msg'] = 'Error al agregar keyword';

				$status['button'] = 'Ok';

			}

		}else{

			//Se vincula keyword a noticia, la keyword ya estaba agregadad de antes.
			//La relación debe contemplar key nuevas como ENCONTRADOS de la db DE KEYS. Este caso son keys encontrados en la db

			$addKeywordLastId = $keywordsExist;

			$doRelation= $objKeywords->addKeywordPrensaRelation(Input::get('id'), $addKeywordLastId);

			$status['msg'] = 'Se vinculo keyword a esta noticia... :)';

			$status['button'] = 'Ok';

		}

		echo json_encode($status);

	}

}



?>
