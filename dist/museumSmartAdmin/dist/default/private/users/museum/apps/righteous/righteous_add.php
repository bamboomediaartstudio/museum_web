<?php

/**
 * @summary Add new Surivor data
 *
 * @description -
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Adds Survivor data to the DB. For App use
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../../core/init.php';

require '../../images_general/AddImageSingle.class.php';

include '../../general/UpdateCategoriesDates.class.php';

/**all the variables */





$allowProgramDownload = 0;

$status['init'] = true;

$db = DB::getInstance();


/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_add_righteous_among_nations'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');


/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

  'name' => array('display' => 'name', 'required' => true),

  'surname' => array('display' => 'surname', 'required' => true),

  'gender' => array('display' => 'surname', 'required' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

  $user = new User();

  if($user->isLoggedIn()){

    $arrayQuery = [
      'name'              => Input::get('name'),

      'surname'           => Input::get('surname'),

      'nickname'          => Input::get('nickname'),

      'birth_date'        => Input::get('birth_date'),

      'death_date'        => (Input::get('zl') != 'on') ? "0000-00-00" : Input::get('death_date'),    //Check death_date / with zl checkbox

      'nationality'       => Input::get('hidden-nationality-id'),

      'religion'          => Input::get('religion'),

      'gender'            => Input::get('gender'),

      'profession'        => Input::get('profession'),

      'recognition_date'  => (Input::get('is_righteous') == 'on') ? Input::get('recognition_date') : "0000-00-00",  //Check recognition date / with is_righteous checkbox

      'bio'               => Input::get('bio'),

      'zl'                => (Input::get('zl') == 'on') ? 1 : 0,

      'is_righteous'      => (Input::get('is_righteous') == 'on') ? 1 : 0,

      'picture'           => '',

      'active'            => 1,

      'deleted'           => 0
    ];


    $insertDbQuery = $db->insert('app_museum_righteous_among_nations', $arrayQuery);



    if($insertDbQuery){

      $lastId = $db->lastId();

      $objUpdateCatDate = new UpdateCategoriesDates("justos");

      $objUpdateCatDate->updateModifiedDate();


      //Si la carga de datos del formulario fue exitosa, proceder a ver si se adjunto imagen

      checkAndAddImages($lastId, "app_museum_righteous_among_nations", "righteous");


    }else{

      //No se cargaron datos del formulario de manera exitosa. No cargar imagenes.
      printData(0, 'Error, no se cargaron datos del formulario, no cargamos tampoco la imagen.');

    }

  }// /.isLoogedIn

}// Validation passed


/**
* @function checkAndAddImages
* @description Add keywords to prensa - json decodes, parse and add
*
* @param {int} $lastId 	-
* @param {string} $table 	-         Nombre de la tabla
* @param {string} $folderName 	- 	Carpeta de imgs, dentro de source
*/
function checkAndAddImages($lastId, $table, $folderName){

  //Si la carga de datos del formulario fue exitosa, proceder a ver si se adjunto imagen

  //Check si se carga imgen

  if(!file_exists($_FILES['picture']['tmp_name']) || !is_uploaded_file($_FILES['picture']['tmp_name'])){
    global $user;
    addLogData($user);

    //printData(1, 'ok without image...por que?');

    printData(0, 'Subido correctamente - Prensa sin imagen.');

  }else{

    //Ir a crear img y cargarla en db

    $objAddSingleImage = new AddImageSingle("picture");

    try{

      //upload image multiple sizes
      $uploadPath = $objAddSingleImage->loadImage($lastId, $folderName);

      //Update db with image path (picture field)
      $updateDbImgPath = $objAddSingleImage->updateDb($table, $lastId, ['picture'=> $uploadPath]);

      if($updateDbImgPath == 1) {

        printData(0, 'Imagen subida y en DB.');

      }else{

        printData(0, "Error updating db.");

      }


    }catch(Exception $e){

      printData(0, $e->getMessage()." - ".$e->getCode());

    }

  }

}

/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The Survivors App ' . Input::get('name') .  ' was added.';

	$userString = 'Agregaste datos a Sobrevivientes <b>' . ' ' . '</b> al sistema.';

	Logger::addLogData($user->data()->id,

		'user',

		$systemString,

		$userString,

		'survivors',

		'survivors');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}


?>
