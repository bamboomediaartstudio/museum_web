<?php

/**
 * @summary Add new Surivor data
 *
 * @description -
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Adds Survivor data to the DB. For App use
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../../core/init.php';

include '../../general/UpdateCategoriesDates.class.php';



/**all the variables */


$allowProgramDownload = 0;

$status['init'] = true;

$db = DB::getInstance();


/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_add_righteous_per_country'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');


/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

  'hidden-country-id'         => array('display' => 'hidden-country-id', 'required' => true),

  'total_righteous'    => array('display' => 'total_righteous', 'required' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

  $user = new User();

  if($user->isLoggedIn()){

    $arrayQuery = [
      'id_country'        => Input::get('hidden-country-id'),

      'total_righteous'   => Input::get('total_righteous'),

      'bio'               => Input::get('bio'),

      'active'            => 1,

      'deleted'           => 0
    ];


    $insertDbQuery = $db->insert('app_museum_righteous_among_nations_countries', $arrayQuery);



    if($insertDbQuery){

      $lastId = $db->lastId();

      $objUpdateCatDate = new UpdateCategoriesDates("justos");

      $objUpdateCatDate->updateModifiedDate();

      printData(0, 'Se agregó justo a pais');


    }else{

      //No se cargaron datos del formulario de manera exitosa.
      printData(0, 'Error, no se cargaron datos del formulario');

    }

  }// /.isLoogedIn

}// Validation passed



/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The Survivors App ' . Input::get('name') .  ' was added.';

	$userString = 'Agregaste datos a Sobrevivientes <b>' . ' ' . '</b> al sistema.';

	Logger::addLogData($user->data()->id,

		'user',

		$systemString,

		$userString,

		'survivors',

		'survivors');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}


?>
