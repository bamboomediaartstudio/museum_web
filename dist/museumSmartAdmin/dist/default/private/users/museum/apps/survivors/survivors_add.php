<?php

/**
 * @summary Add new Surivor data
 *
 * @description -
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Adds Survivor data to the DB. For App use
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../../core/init.php';

require '../../images_general/AddImageSingle.class.php';

include '../../general/UpdateCategoriesDates.class.php';


/**all the variables */



$allowProgramDownload = 0;

$status['init'] = true;

$db = DB::getInstance();


/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_add_survivor'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');


/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

  'name' => array('display' => 'name', 'required' => true),

  'surname' => array('display' => 'surname', 'required' => true))

);




/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) {

  printData(0, 'validation not passed');

}else{

  $user = new User();

  if($user->isLoggedIn()){

    $arrayQuery = [
      'name'              => Input::get('name'),

      'surname'           => Input::get('surname'),

      'nick_name'         => Input::get('nick_name'),

      'birth_date'        => Input::get('birth_date'),

      'id_birth_country'  => Input::get('hidden-birth-place-id'),

      'zl'                => (Input::get('zl') == 'on') ? 1 : 0,

      'picture'           => '',

      'bio'               => Input::get('bio'),

      'active'            => 1,

      'deleted'           => 0
    ];


    $insertDbQuery = $db->insert('app_museum_survivors', $arrayQuery);



    if($insertDbQuery){

      $lastId = $db->lastId();

      $objUpdateCatDate = new UpdateCategoriesDates("Sobrevivientes");

      $objUpdateCatDate->updateModifiedDate();


      //Si la carga de datos del formulario fue exitosa, proceder a ver si se adjunto imagen

      //Check si se carga imgen

      if(!file_exists($_FILES['picture']['tmp_name']) || !is_uploaded_file($_FILES['picture']['tmp_name'])){
        addLogData($user);

  			//printData(1, 'ok without image...por que?');

        printData(0, 'Subido correctamente - Sobreviviente sin imagen.');

      }else{

        //Ir a crear img y cargarla en db

        $objAddSingleImage = new AddImageSingle("picture");

        try{

          //upload image multiple sizes
          $uploadPath = $objAddSingleImage->loadImage($lastId, "survivors");

          //Update db with image path (picture field)
          $updateDbImgPath = $objAddSingleImage->updateDb('app_museum_survivors', $lastId, ['picture'=> $uploadPath]);

          if($updateDbImgPath == 1) {

            printData(0, 'Imagen subida y en DB.');

          }else{

            printData(0, "Error updating db.");

          }


        }catch(Exception $e){

          printData(0, $e->getMessage()." - ".$e->getCode());

        }

      }

    }else{

      //No se cargaron datos del formulario de manera exitosa. No cargar imagenes.
      printData(0, 'Error, no se cargaron datos del formulario, no cargamos tampoco la imagen.');

    }

  }// /.isLoogedIn

}// Validation passed





/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The Survivors App ' . Input::get('name') .  ' was added.';

	$userString = 'Agregaste datos a Sobrevivientes <b>' . ' ' . '</b> al sistema.';

	Logger::addLogData($user->data()->id,

		'user',

		$systemString,

		$userString,

		'survivors',

		'survivors');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}


?>
