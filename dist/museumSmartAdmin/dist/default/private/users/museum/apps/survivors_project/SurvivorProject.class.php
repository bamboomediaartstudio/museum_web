<?php

class SurvivorProject{

    private $db;
    private $idSurvivor;

    public function __construct($idSurvivor)
    {
        $this->idSurvivor = $idSurvivor;

        $this->db = DB::getInstance();
    }



    public function addAlternativeName($name, $surname)
    {
        
        $insertNames = $this->db->insert('app_museum_survivors_project_alternative_name', ['sid'=>$this->idSurvivor, 'alternative_name'=>$name, 'alternative_surname'=>$surname]);
        
        if($insertNames)
        {

            return $this->db->lastId();
        
        }else{
        
            throw new Exception('Error on insert alternatives names', 0);
        
        }


    }

    public function addCampo($name)
    {

        $queryCampo = $this->db->insert('app_museum_survivors_campos', ['name'=> $name]);

        if($queryCampo)
        {

            return ['status' => 'success', 'lastId' => $this->db->lastId()];

        }else{

            throw new Exception('Error on insert campos', 0);

        }

    }

    public function assignCampo($idCampo)
    {
        

        $queryAssign = $this->db->insert('app_museum_survivors_project_campos_relations', ['id_survivor'=>$this->idSurvivor, 'id_campo'=>$idCampo]);

        if($queryAssign)
        {

            return ['status' => 'success', 'lastId' => $this->db->lastId()];

        }else{

            throw new Exception('Error on campo assign..', 0);

        }

    }

    public function addBook($name)
    {

        $queryBook = $this->db->insert('app_museum_survivors_project_books', ['name'=>$name]);

        if($queryBook)
        {

            return ['status' => 'success', 'lastId' => $this->db->lastId()];

        }else{

            throw new Exception('Error adding book', 0);

        }

    }


    public function assignBook($idBook)
    {

        $queryAssign = $this->db->insert('app_museum_survivors_project_books_relations', ['id_survivor'=>$this->idSurvivor, 'id_book'=>$idBook]);

        if($queryAssign)
        {

            return ['status' => 'success', 'lastId' => $this->db->lastId()];

        }else{

            throw new Exception('Error on book assign', 0);

        }

    }

    public function addFilm($name)
    {

        $queryFilm = $this->db->insert('app_museum_survivors_project_films', ['name'=>$name]);

        if($queryFilm)
        {

            return ['status' => 'success', 'lastId' => $this->db->lastId()];

        }else{

            throw new Exception('Error adding film', 0);

        }

    }

    public function assignFilm($idFilm)
    {

        $queryAssign = $this->db->insert('app_museum_survivors_project_films_relations', ['id_survivor'=>$this->idSurvivor, 'id_film'=>$idFilm]);

        if($queryAssign)
        {

            return ['status' => 'success', 'lastId' => $this->db->lastId()];

        }else{

            throw new Exception('Error on film assign', 0);

        }

    }

    public function addHonor($name)
    {

        $queryHonor = $this->db->insert('app_museum_survivors_project_honors', ['name'=>$name]);

        if($queryHonor)
        {

            return ['status' => 'success', 'lastId' => $this->db->lastId()];

        }else{

            throw new Exception('Error adding honor', 0);

        }

    }

    public function assignHonor($idHonor)
    {

        $queryAssign = $this->db->insert('app_museum_survivors_project_honors_relations', ['id_survivor'=>$this->idSurvivor, 'id_honor'=>$idHonor]);

        if($queryAssign)
        {

            return ['status' => 'success', 'lastId' => $this->db->lastId()];

        }else{

            throw new Exception('Error on honor assign', 0);

        }

    }

    public function addRescuer($name, $surname)
    {

        $queryRescuer = $this->db->insert('app_museum_survivors_project_rescuers', ['name'=>$name, 'surname'=>$surname]);

        if($queryRescuer)
        {

            return ['status' => 'success', 'lastId' => $this->db->lastId()];

        }else{

            throw new Exception('Error adding rescuer', 0);

        }

    }

    public function assignRescuer($idRescuer)
    {

        $queryAssign = $this->db->insert('app_museum_survivors_project_rescuers_relations', ['id_survivor'=> $this->idSurvivor, 'id_rescuer'=>$idRescuer]);

        if($queryAssign)
        {

            return ['status' => 'success', 'lastId' => $this->db->lastId()];

        }else{

            throw new Exception('Error on rescuer assign', 0);

        }

    }


    //Check si tengo img de perfil...

    public function cantProfileImage()
    {
        
        $strQuery = 'SELECT count(id) AS cant 
                        FROM app_museum_survivors_project_main_picture 
                        WHERE sid = ?';
        
        $queryCheck = $this->db->query($strQuery, ['sid'=> $this->idSurvivor]);

        $cantSelected = $queryCheck->first();

        return $cantSelected->cant;

    }

    public function addProfileImage($imgPath)
    {

        $addQuery = $this->db->insert('app_museum_survivors_project_main_picture', ['sid'=>$this->idSurvivor, 'picture'=>$imgPath, 'active'=>1, 'deleted'=>0]);

        if($addQuery)
        {

            return ['status'=>'success', 'lastId'=>$this->db->lastId()];

        }else{
            
            throw new Exception('Error adding profile image on db', 0);

        }
    }

}

