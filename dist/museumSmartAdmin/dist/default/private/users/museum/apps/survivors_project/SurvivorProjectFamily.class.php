<?php

class SurvivorProjectFamily{

    private $db;
    private $idSurvivor;

    public function __construct($idSurvivor){

        if($idSurvivor != null){

            $this->idSurvivor = $idSurvivor;

        }

        $this->db = DB::getInstance();

    }

    public function addFamilyRelationData($name, $surname)
    {

        $querySpouse = $this->db->insert('app_museum_survivors_projects_family_relations', ['name'=>$name, 'surname'=>$surname]);

        if($querySpouse)
        {

            return ['status' => 'success', 'lastId' => $this->db->lastId()];

        }else{

            throw new Exception('Error on add family relations..', 0);

        }

    }


    public function assignFamilyRelation($idFamilyRelation, $relationType)
    {

        $queryAssign = $this->db->insert('app_museum_survivors_projects_family_relations_relations_table', ['id_survivor'=>$this->idSurvivor, 'id_relation'=>$idFamilyRelation, 'id_relation_type'=>$relationType]);

        if($queryAssign)
        {

            return ['status' => 'success', 'lastId' => $this->db->lastId()];

        }else{

            throw new Exception('Error on assign family relations..', 0);

        }   

    }


    //TODO:: Delete method.. 

}