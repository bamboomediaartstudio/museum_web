<?php

/**
 * @summary Add new Surivor data
 *
 * @description -
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Adds Survivor data to the DB. For App use
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../../core/init.php';

require '../../images_general/AddImageSingle.class.php';

include '../../general/UpdateCategoriesDates.class.php';

//...

/*
echo '<pre>';
print_r($_POST);
echo '</pre>';
*/

$filesManager = new FilesManager();

$ds = DIRECTORY_SEPARATOR;

$image = new \claviska\SimpleImage();

$retinaOriginalWidth = 2000;

$retinaOriginalHeight = 1000; 

/**all the variables */

$allowProgramDownload = 0;

$status['init'] = true;

$status['year'] = "";

$db = DB::getInstance();



/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_add_survivor_project'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');

$validate = new Validate();

$validation = $validate->check($_POST, array(

  'name' => array('display' => 'name', 'required' => true),

  'surname' => array('display' => 'surname', 'required' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) {

  printData(0, 'validation not passed?');

}else{

  $user = new User();

  if($user->isLoggedIn()){

    //...

    $saveYear = (Input::get('year') == null) ? '0000' : Input::get('year');
    
    $saveMonth = (Input::get('month') == null) ? '00' : Input::get('month');

    $saveDay = (Input::get('day') == null) ? '00' : Input::get('day');


    $date = $saveYear . "-" . $saveMonth . "-" . $saveDay;
    
    $arrayQuery = [

      'name'                              => Input::get('name'),

      'surname'                           => Input::get('surname'),

      'alternative_surname'               => Input::get('alternative_surname'),
      
      'birthday'                          => date("Y-m-d",strtotime($date)),

      'nationality'                       => Input::get('countryId'),

      'city'                              => Input::get('cityId'),

      'nationality_at_that_moment'        => Input::get('atThatMomentId'),

      'nationality_at_the_beginning_of_war' => Input::get('atTheBeginningId'),
      
      'liberation_place'                  => Input::get('liberationId'),

      'bio'                               => Input::get('bio'),

      'liberation_army'                   => Input::get('radio_group'),

      'entry_year'                        => Input::get('entry_year'),

      'son_num'                           => Input::get('son_num'), 

      'grandson_num'                      => Input::get('grandson_num'), 

      'death_marches'                     => (Input::get('death_marches') == 'on') ? 1 : 0,

      'hide_at_family_houses'             => (Input::get('hide_at_family_houses') == 'on') ? 1 : 0, 

      'hide_on_woods'                     => (Input::get('hide_on_woods') == 'on') ? 1 : 0, 

      'hide_on_institutions'              => (Input::get('hide_on_institutions') == 'on') ? 1 : 0, 
      
      'use_false_identity'                => (Input::get('use_false_identity')  == 'on') ? 1 : 0, 

      'was_deported'                      => (Input::get('was_deported')  == 'on') ? 1 : 0, 

      'was_rescued'                       => (Input::get('was_rescued')  == 'on') ? 1 : 0, 

      'is_righteous'                      => (Input::get('is_righteous')  == 'on') ? 1 : 0, 

      'participated'                      => (Input::get('participated')  == 'on') ? 1 : 0, 
      
      'partisan'                          => (Input::get('partisan')  == 'on') ? 1 : 0, 

      'ally'                              => (Input::get('ally')  == 'on') ? 1 : 0, 
      
      'dp_camp'                           => (Input::get('dp_camp')  == 'on') ? 1 : 0, 

      'legal'                             => (Input::get('legal')  == 'on') ? 1 : 0, 

      'marriage'                          => (Input::get('marriage')  == 'on') ? 1 : 0, 

      'emigrated'                         => (Input::get('emigrated')  == 'on') ? 1 : 0, 

      'active'                            => 1,

      'deleted'                           => 0

    ];

    $insertDbQuery = $db->insert('app_museum_survivors_project', $arrayQuery);

    $lastId = $db->lastId();

    // Alternative names...

    if(!empty(Input::get('alternartiveNames'))){

      $jsonObject = json_decode(Input::get('alternartiveNames'));

      foreach ($jsonObject as $key => $value) {

        $db->insert('app_museum_survivors_project_alternative_name', 

          ['sid'=>$lastId, 'alternative_name'=>$value->name, 'alternative_surname'=>$value->surname]);
        
      }

    }

    // ****






     // * New Rescuers 

     if(!empty(Input::get('rescuerList'))){


      $jsonObject = json_decode(Input::get('rescuerList'));

      foreach ($jsonObject as $key => $value) {

        $db->insert('app_museum_survivors_project_rescuers', ['name'=>$value->name, 'surname'=>$value->surname]);

        
        $lastRescuerId = $db->lastId();

        $db->insert('app_museum_survivors_project_rescuers_relations',   //Agrego a la tabla de relaciones con el survivor

          ['id_survivor'=>$lastId, 'id_rescuer'=>$lastRescuerId]);
        
      }


    }

    // ****

    // * selected Rescuers! - 
    
    if(!empty(Input::get('selectedRescuers'))){

      $jsonObject = json_decode(Input::get('selectedRescuers'));

      foreach ($jsonObject as $key => $value) {

        $db->insert('app_museum_survivors_project_rescuers_relations', ['id_survivor'=>$lastId, 'id_rescuer'=>$value->id]);
        
      }

    }

    // ****


    // * New Guetos  Se agrega NUEVOS guetos

    if(!empty(Input::get('guetoList'))){


      $jsonObject = json_decode(Input::get('guetoList'));

      foreach ($jsonObject as $key => $value) {

        $db->insert('app_museum_guetos', 

          ['name'=>$value->name]);            //Se agrega a db guetos


        
        $lastGuetoId = $db->lastId();  //Obtengo id de cada unos insertado


        $db->insert('app_museum_survivors_project_guetos_relations',   //Agrego a la tabla de relaciones con el survivor

          ['id_survivor'=>$lastId, 'id_gueto'=>$lastGuetoId]);
        
      }


    }

    // ****


    // * selected Guetos! - 
    
    if(!empty(Input::get('selectedGuetos'))){

      $jsonObject = json_decode(Input::get('selectedGuetos'));

      foreach ($jsonObject as $key => $value) {

        $db->insert('app_museum_survivors_project_guetos_relations', 

          ['id_survivor'=>$lastId, 'id_gueto'=>$value->id]);
        
      }

    }

    // ****



    // * New Campos  Se agrega NUEVOS campos

    if(!empty(Input::get('campoList'))){


      $jsonObject = json_decode(Input::get('campoList'));

      foreach ($jsonObject as $key => $value) {

        $db->insert('app_museum_survivors_campos', 

          ['name'=>$value->name]);            //Se agrega a db guetos


        
        $lastGuetoId = $db->lastId();  //Obtengo id de cada unos insertado


        $db->insert('app_museum_survivors_project_campos_relations',   //Agrego a la tabla de relaciones con el survivor

          ['id_survivor'=>$lastId, 'id_campo'=>$lastGuetoId]);
        
      }

    }

    // ****


    // * selected campos! - 
    
    if(!empty(Input::get('selectedCampos'))){

      $jsonObject = json_decode(Input::get('selectedCampos'));

      foreach ($jsonObject as $key => $value) {

        $db->insert('app_museum_survivors_project_campos_relations', 

          ['id_survivor'=>$lastId, 'id_campo'=>$value->id]);
        
      }

    }

    // ****




    // * New Books  Se agrega NUEVOS libros (se insertan por primera vez en db y luego se agregan a tabla que los relacion) *

    if(!empty(Input::get('booksList'))){


      $jsonObject = json_decode(Input::get('booksList'));

      foreach ($jsonObject as $key => $value) {

        $db->insert('app_museum_survivors_project_books', 

          ['name'=>$value->name]);            //Se agrega a db books


        
        $lastBookId = $db->lastId();  //Obtengo id de cada unos insertado


        $db->insert('app_museum_survivors_project_books_relations',   //Agrego a la tabla de relaciones con el survivor

          ['id_survivor'=>$lastId, 'id_book'=>$lastBookId]);
        
      }


    }

    // ****


    // * selected Books! - Se agregan libros seleccionados del desplegable (ya estan en db, se agregan a tabla que los relacion) *
    
    if(!empty(Input::get('selectedBooks'))){

      $jsonObject = json_decode(Input::get('selectedBooks'));

      foreach ($jsonObject as $key => $value) {

        $db->insert('app_museum_survivors_project_books_relations', 

          ['id_survivor'=>$lastId, 'id_book'=>$value->id]);
        
      }

    }

    // ****
    

    //New Films - Se agregan nuevos films

    if(!empty(Input::get('filmsList'))){


      $jsonObject = json_decode(Input::get('filmsList'));

      foreach ($jsonObject as $key => $value) {

        $db->insert('app_museum_survivors_project_films', 

          ['name'=>$value->name]);            //Se agrega a db films


        
        $lastFilmId = $db->lastId();          //Obtengo id de cada uno insertado


        $db->insert('app_museum_survivors_project_films_relations',   //Agrego a la tabla de relaciones con el survivor

          ['id_survivor'=>$lastId, 'id_film'=>$lastFilmId]);
        
      }


    }

    // ****

    // * selected Films! - Se agregan films seleccionadas del desplegable (ya estan en db, se agregan a tabla que relaciona) *
    
    if(!empty(Input::get('selectedFilms'))){

      $jsonObject = json_decode(Input::get('selectedFilms'));

      foreach ($jsonObject as $key => $value) {

        $db->insert('app_museum_survivors_project_films_relations', 

          ['id_survivor'=>$lastId, 'id_film'=>$value->id]);
        
      }

    }

    // ****

    //New Honors - Se agregan nuevos reconocimientos

    if(!empty(Input::get('honorList'))){

      $jsonObject = json_decode(Input::get('honorList'));


      foreach($jsonObject as $key => $value){

        $db->insert('app_museum_survivors_project_honors', ['name'=>$value->name]);

        $lastHonorId = $db->lastId();

        $db->insert('app_museum_survivors_project_honors_relations',   //Agrego a la tabla de relaciones con el survivor

          ['id_survivor'=>$lastId, 'id_honor'=>$lastHonorId]);

      }


    }

    // ****

    // * selected Honors! - Se agregan reconocimientos seleccionadas del desplegable *
    
    if(!empty(Input::get('selectedHonors'))){

      $jsonObject = json_decode(Input::get('selectedHonors'));

      foreach($jsonObject as $key => $value){

        $db->insert('app_museum_survivors_project_honors_relations', ['id_survivor'=>$lastId, 'id_honor'=> $value->id]); 

      }

    }

    // ****



    //New Family - Add spouse, son and grandson (new register)
    
    if(!empty(Input::get('familyList'))){

      $jsonObject = json_decode(Input::get('familyList'));

      foreach($jsonObject as $key => $value){

        $db->insert('app_museum_survivors_projects_family_relations', ['name'=>$value->name, 'surname'=>$value->surname]);

        $lastSpouseId = $db->lastId();

        $db->insert('app_museum_survivors_projects_family_relations_relations_table', ['id_survivor'=>$lastId, 'id_relation'=>$lastSpouseId, 'id_relation_type'=> $value->relation_type]);

      }

    }

    // ****

   
    //selected family from multile select (From selects: spouse, son and grandson)

    if(!empty(Input::get('selectedFamily'))){

      $jsonObject = json_decode(Input::get('selectedFamily'));

      foreach($jsonObject as $key => $value){

        $db->insert('app_museum_survivors_projects_family_relations_relations_table', ['id_survivor'=>$lastId, 'id_relation'=> $value->id, 'id_relation_type'=> $value->relation_type]); 

      }

    }

    // ****
    

    // * selected reference country - Se agregan paises de referencia seleccionadas del desplegable *
    
    if(!empty(Input::get('selectedReferenceCountries'))){

      $jsonObject = json_decode(Input::get('selectedReferenceCountries'));

      foreach($jsonObject as $key => $value){

        $db->insert('app_museum_survivors_project_referenced_countries_relations', ['id_survivor'=>$lastId, 'id_country'=> $value->id]); 

      }

    }

    // ****



    
    if($insertDbQuery){

       if(!file_exists($_FILES['picture']['tmp_name']) || !is_uploaded_file($_FILES['picture']['tmp_name'])){

        addLogData($user);

        printData(0, 'subido correctamente - sin imagen adjunta...');  //TODO:: porque si le mando uno no anda?

        //printData(0, 'Subido correctamente - Sobreviviente sin imagen.');

      }else{

        //echo 'bueno vamos por aca  que hay img...';
        
        $uniqueId = uniqid();

        $fileSize = 20971520;

        $itemFolder = '../../../../sources/images/survivors_project/' . $lastId;

        $deleteFolder = '../../../../sources/images/survivors_project/' . $lastId . '/delete';

        $fileFolder = $itemFolder . '/' . $uniqueId;

        $itemFolderExists = $filesManager->checkDirectory($itemFolder);

        if(!$itemFolderExists){

          $itemFolderCreated = $filesManager->makeDirectory($itemFolder);

          $deleteFolder = $filesManager->makeDirectory($deleteFolder);
        }

        $folderExists = $filesManager->checkDirectory($fileFolder);

        if(!$folderExists){

          $folderCreated = $filesManager->makeDirectory($fileFolder);

        }else{

          $folderCreated = true;
        }

        if($folderCreated){

          $checkPHPExtensions = $filesManager->checkPHPExtensions();
        }

        if($checkPHPExtensions){

          if (!empty($_FILES)) {

            $tempFile = $_FILES['picture']['tmp_name'];

            if(is_file($tempFile)){

              $checkFileSize = $filesManager->checkFileSize($tempFile, $fileSize);

              if($checkFileSize){

                $targetPath = dirname( __FILE__ ) . $ds . $fileFolder . $ds;

                $name = $lastId . '_' . $uniqueId;

                $checkMimeType = $filesManager->checkImageMimeType($_FILES['picture']['tmp_name']);

                if($checkMimeType){

                  //-----------------------------------------------------
                  //general image settings...

                  $newMime = 'image/jpeg';

                  $newExtension = 'jpeg';

                  //-----------------------------------------------------
                  //the default image will be the retina image...

                  $uploaded =  $targetPath . $name . '_uploaded.' . $newExtension;

                  move_uploaded_file($tempFile, $uploaded);


                  //-----------------------------------------------------
                  //the normal image is the half of the retina...

                  $originalFileRetina =  $targetPath . $name . '_original@2x.' . $newExtension;

                  $image->fromFile($uploaded)->thumbnail($retinaOriginalWidth, $retinaOriginalHeight, $retinaOriginalHeight/2)->toFile($originalFileRetina, $newMime, 70);


                  //-----------------------------------------------------
                  //this is a square from the retina image

                  $originalFileRetinaSq =  $targetPath . $name . '_original_sq@2x.' . $newExtension;

                  $image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight, $retinaOriginalHeight, $retinaOriginalHeight/2)->toFile($originalFileRetinaSq, $newMime, 70);

                  //-----------------------------------------------------
                  //the normal image is the half of the retina...

                  $originalFile =  $targetPath . $name . '_original.' . $newExtension;

                  $image->fromFile($originalFileRetina)->resize(null, $retinaOriginalHeight/2)->toFile($originalFile, $newMime, 70);

                  //-----------------------------------------------------
                  //the normal image sq version

                  $originalFileSq =  $targetPath . $name . '_original_sq.' . $newExtension;

                  $image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight/2, $retinaOriginalHeight/2, $retinaOriginalHeight/4)->toFile($originalFileSq, $newMime, 70);

                  //-----------------------------------------------------
                  //the medium version for the retina...

                  $mediumRetinaFile =  $targetPath . $name . '_medium@2x.' . $newExtension;

                  $image->fromFile($originalFileRetina)->resize(null, $retinaOriginalHeight/2)->toFile($mediumRetinaFile, $newMime, 70);

                  //-----------------------------------------------------
                  //the medium sq version for the retina...

                  $mediumRetinaFileSq =  $targetPath . $name . '_medium_sq@2x.' . $newExtension;

                  $image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight/2, $retinaOriginalHeight/2, $retinaOriginalHeight/4)->toFile($mediumRetinaFileSq, $newMime, 70);

                  //-----------------------------------------------------
                  //the medium version for the normal...

                  $mediumFile =  $targetPath . $name . '_medium.' . $newExtension;

                  $image->fromFile($originalFileRetina)->resize(null, $retinaOriginalHeight/4)->toFile($mediumFile, $newMime, 70);

                  //-----------------------------------------------------
                  //the medium sq version for the retina...

                  $mediumFileSq =  $targetPath . $name . '_medium_sq.' . $newExtension;

                  $image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight/4, $retinaOriginalHeight/4, $retinaOriginalHeight/2)->toFile($mediumFileSq, $newMime, 70);

                  //-----------------------------------------------------
                  //small retina

                  $smallRetinaFile =  $targetPath . $name . '_small@2x.' . $newExtension;

                  $image->fromFile($originalFileRetina)->resize(null, $retinaOriginalHeight/4)->toFile($smallRetinaFile, $newMime, 70);

                  //-----------------------------------------------------
                  //the small sq version for the retina...

                  $smallRetinaSq =  $targetPath . $name . '_small_sq@2x.' . $newExtension;

                  $image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight/4, $retinaOriginalHeight/4, $retinaOriginalHeight/8)->toFile($smallRetinaSq, $newMime, 70);

                  //-----------------------------------------------------
                  //small

                  $smallFile =  $targetPath . $name . '_small.' . $newExtension;

                  $image->fromFile($originalFileRetina)->resize(null, $retinaOriginalHeight/8)->toFile($smallFile, $newMime, 70);

                  //-----------------------------------------------------
                  //the small sq version for the retina...

                  $smallFileSq =  $targetPath . $name . '_small_sq.' . $newExtension;

                  $image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight/8, $retinaOriginalHeight/8, $retinaOriginalHeight/16)->toFile($smallFileSq, $newMime, 70);

                  //......

                  $values = [

                    'sid'                              => $lastId,

                    'picture'                           => $uniqueId

                  ];

                  $insertImages = $db->insert('app_museum_survivors_project_main_picture', $values);

                 

                  printData(3, 'inserted with image');

                }else{

                  printData(4, 'error file mimetype');

                }

              }else{

                printData(5, 'error. filesize...');

              }

            }else{

              //printData(0, '2');
              printData(6, 'inserted without image. no file..');

            }

          }

        }
        
      }
    }
    

  }

}




/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The Survivors Project App ' . Input::get('name') .  ' was added.';

	$userString = 'Agregaste datos a Proyecto Sobrevivientes <b>' . ' ' . '</b> al sistema.';

	Logger::addLogData($user->data()->id,

		'user',

		$systemString,

		$userString,

		'survivors_proyect',

		'survivors_proyect');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

  $status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

  echo json_encode($status);

	exit();
}


?>
