<?php

/**
 * @summary Delete family relation from survivor project
 *
 * @description -
 *
 * @author Colo Baggins <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the includes */

require_once '../../../core/init.php';


/**validate.. */

$user = new User();


if($user->isLoggedIn()){

	$validate = new Validate();

	$validation = $validate->check($_POST, array(

			'action' => array('display'=> 'action', 'required' => true)
		)
	);


}

$db = DB::getInstance();

if($validation->passed()){


	$idRelation = Input::get('id');

	//if(Input::get('action') == "delete_family_relation"){	//Delete family relation
	if(Input::get('action') == "grandson" || Input::get('action') == "son" || Input::get('action') == "spouse"){	//Delete family relation


		$deleteQuery = $db->query("DELETE FROM app_museum_survivors_projects_family_relations_relations_table WHERE id = '$idRelation'");


		//TODO:: como chequear con userSpice 4 si realmente el query tuvo exito??

		$status['msg'] = 'Se elimino relacion familiar';

		$status['title'] = 'Eliminado';

		$status['button'] = 'Entendido';

		echo json_encode($status);

		return;

	//}else if(Input::get('action') == "delete_book_relation"){	//Delete book relation
	}else if(Input::get('action') == "book"){	//Delete book relation


		$deleteQuery = $db->query("DELETE FROM app_museum_survivors_project_books_relations WHERE id = '$idRelation'");


		//TODO:: como chequear con userSpice 4 si realmente el query tuvo exito??

		$status['msg'] = 'Libro eliminado';

		$status['title'] = 'Eliminado';

		$status['button'] = 'Entendido';

		echo json_encode($status);

		return;

	//}else if(Input::get('action') == "delete_film_relation"){	//Delete film relation
	}else if(Input::get('action') == "film"){	//Delete film relation


		$deleteQuery = $db->query("DELETE FROM app_museum_survivors_project_films_relations WHERE id = '$idRelation'");


		//TODO:: como chequear con userSpice 4 si realmente el query tuvo exito??

		$status['msg'] = 'Película eliminada';

		$status['title'] = 'Eliminada';

		$status['button'] = 'Entendido';

		echo json_encode($status);

		return;

	//}else if(Input::get('action') == "delete_honor_relation"){	//Delete honor relation
	}else if(Input::get('action') == "honor"){	//Delete honor relation


		$deleteQuery = $db->query("DELETE FROM app_museum_survivors_project_honors_relations WHERE id = '$idRelation'");


		//TODO:: como chequear con userSpice 4 si realmente el query tuvo exito??

		$status['msg'] = 'Reconocimiento eliminada';

		$status['title'] = 'Eliminado';

		$status['button'] = 'Entendido';

		echo json_encode($status);

		return;

	}else if(Input::get('action') == "gueto"){

		$deleteQuery = $db->query("DELETE FROM app_museum_survivors_project_guetos_relations WHERE id = '$idRelation'");


		//TODO:: como chequear con userSpice 4 si realmente el query tuvo exito??

		$status['msg'] = 'Gueto eliminado';

		$status['title'] = 'Eliminado';

		$status['button'] = 'Entendido';

		echo json_encode($status);

		return;

	}else if(Input::get('action') == "campo"){

		$deleteQuery = $db->query("DELETE FROM app_museum_survivors_project_campos_relations WHERE id = '$idRelation'");


		//TODO:: como chequear con userSpice 4 si realmente el query tuvo exito??

		$status['msg'] = 'Campo eliminado';

		$status['title'] = 'Eliminado';

		$status['button'] = 'Entendido';

		echo json_encode($status);

		return;

	}else if(Input::get('action') == "alternative_name"){

		$deleteQuery = $db->query("DELETE FROM app_museum_survivors_project_alternative_name WHERE id = '$idRelation'");


		//TODO:: como chequear con userSpice 4 si realmente el query tuvo exito??

		$status['msg'] = 'Nombre eliminado';

		$status['title'] = 'Eliminado';

		$status['button'] = 'Entendido';

		echo json_encode($status);

		return;

	}else if(Input::get('action') == "referenced"){	//referenced countries

		$deleteQuery = $db->query("DELETE FROM app_museum_survivors_project_referenced_countries_relations WHERE id = '$idRelation'");


		//TODO:: como chequear con userSpice 4 si realmente el query tuvo exito??

		$status['msg'] = 'Referencia eliminada';

		$status['title'] = 'Eliminado';

		$status['button'] = 'Entendido';

		echo json_encode($status);

		return;

	}elseif(Input::get('action') == "rescuer"){

		$deleteQuery = $db->query("DELETE FROM app_museum_survivors_project_rescuers_relations WHERE id = '$idRelation'");


		//TODO:: como chequear con userSpice 4 si realmente el query tuvo exito??

		$status['msg'] = 'Rescatador eliminado';

		$status['title'] = 'Eliminado';

		$status['button'] = 'Entendido';

		echo json_encode($status);

		return;

	}else{

		$status['msg'] = 'Error. what we are doing here?';

		$status['button'] = 'Salir';

		echo json_encode($status);

	}




}



?>
