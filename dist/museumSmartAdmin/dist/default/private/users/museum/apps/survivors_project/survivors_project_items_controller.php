<?php

/**
 * @summary Add new Surivor data
 *
 * @description -
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../../core/init.php';

require_once 'SurvivorProject.class.php';

require_once 'SurvivorProjectFamily.class.php';

//$db = DB:getInstance();

/*

echo '<pre>';
print_r($_POST);
echo '</pre>';

*/


/**validate.. */

$user = new User();


if($user->isLoggedIn()){

    $validate = new Validate();

    $validation = $validate->check($_POST, array(

            'action' => array('display'=> 'action', 'required' => true),

            'id'    => array('display'=> 'id', 'required'=> true)
        )
    );



}


if($validation->passed()){

    $action = Input::get('action');

    $idSurvivor = Input::get('id');

    
    

    if(!empty($action)){

        if($action == 'alternativeName'){

            $objSurvivor = new SurvivorProject($idSurvivor);


            $items = Input::get('items');   //(Array)

            $name = $items[0]['name'];

            $surname = $items[0]['surname'];


            try{

                $inserted = $objSurvivor->addAlternativeName($name, $surname);


            }catch(Exception $e){

                echo 'Error. ' . $e->getMessage() . '. Code: ' . $e->getCode();
                
                exit();

            }
            
            $arrResponse = array(

                'status'    => 'success',
                'title'     => 'Agregado',
                'desc'      => 'Nombre alternativo agregado correctamente',
                'lastId'    => $inserted

            );

            echo json_encode($arrResponse);

        } 

        
        if($action == 'addCampo'){

            $objSurvivor = new SurvivorProject($idSurvivor);

            $campoName = Input::get('items');

            //Add campo o table
            try{

                $campoResponse = $objSurvivor->addCampo($campoName, true);  


            }catch(Exception $e){

                //echo 'Error. '.$e->getMessage() . '. Code: ' . $e->getCode();

                onErrorException($e->getMessage(), $e->getCode());

            }


            //Assign campo to survivor
            try{

                $assignCampo = $objSurvivor->assignCampo($campoResponse['lastId']);

            }catch(Exception $e){

                onErrorException($e->getMessage(), $e->getCode());

            }


            $arrResponse = array(
                'status'        => 'success',
                'title'         => 'Agregado',
                'desc'          => 'Campo agregado correctamente',
                'lastCampoId'   => $campoResponse['lastId'],
                'lastRelationId'=> $assignCampo['lastId']
            );

            echo json_encode($arrResponse);
            
        }



        if($action == 'addFamilyData'){

            $items = Input::get('items');

            $name = $items[0]['name'];

            $surname = $items[0]['surname'];

            $relationType = $items[0]['relation_type'];

            $objSurvivorFamily = new SurvivorProjectFamily($idSurvivor);
            

            try{

                $familyResponse = $objSurvivorFamily->addFamilyRelationData($name, $surname);

                $familyLastId = $familyResponse['lastId'];

            }catch(Exception $e){

                onErrorException($e->getMessage(), $e->getCode());

            }


            try{

                $assignFamilyRelation = $objSurvivorFamily->assignFamilyRelation($familyLastId, $relationType);

            }catch(Exception $e){

                onErrorException($e->getMessage(), $e->getCode());

            }

            $arrResponse = array(
                'status'                => 'success',
                'title'                 => 'Agregado',
                'desc'                  => 'Relacion familiar agregada correctamente',
                'familyLastId'          => $familyResponse['lastId'],
                'familyRelationLastId'  => $assignFamilyRelation['lastId']
            );

            echo json_encode($arrResponse);

        }

        if($action == 'addBook'){

            $objSurvivor = new SurvivorProject($idSurvivor);
            $name = Input::get('items');

            //Add book
            try{

                $bookResponse = $objSurvivor->addBook($name);

            }catch(Exception $e){

                onErrorException($e->getMessage(), $e->getCode());

            }

            //Assign book
            try{

                $assignResponse = $objSurvivor->assignBook($bookResponse['lastId']);


            }catch(Exception $e){

                onErrorException($e->getMessage(), $e->getCode());

            }

            $arrResponse = array(
                'status'            => 'sucess',
                'title'             => 'Agregado',
                'desc'              => 'Libro agregado correctamente',
                'lastBookId'        => $bookResponse['lastId'],
                'lastBookRelation'  => $assignResponse['lastId']
            );

            echo json_encode($arrResponse);

        }

        if($action == 'addFilm'){

            $objSurvivor = new SurvivorProject($idSurvivor);

            $name = Input::get('items');

            //Add film
            try{

                $filmResponse = $objSurvivor->addFilm($name);

            }catch(Exception $e){

                onErrorException($e->getMessage(), $e->getCode());

            }

            //Assign film

            try{

                $assignResponse = $objSurvivor->assignFilm($filmResponse['lastId']);

            }catch(Exception $e){

                onErrorException($e->getMessage(), $e->getCode());
            }

            $arrResponse = array(
                'status'            => 'sucess',
                'title'             => 'Agregada',
                'desc'              => 'Película agregada correctamente',
                'lastFilmId'        => $filmResponse['lastId'],
                'lastFilmRelation'  => $assignResponse['lastId']
            );

            echo json_encode($arrResponse);

        }

        if($action == 'addHonor'){

            $objSurvivor = new SurvivorProject($idSurvivor);

            $name = Input::get('items');

            try{

                $honorResponse = $objSurvivor->addHonor($name);

            }catch(Exception $e){

                onErrorException($e->getMessage(), $e->getCode());

            }

            try{

                $assignResponse = $objSurvivor->assignHonor($honorResponse['lastId']);

            }catch(Exception $e){

                onErrorException($e->getMessage(), $e->getCode());

            }

            $arrResponse = array(
                'status'            => 'success',
                'title'             => 'Agregado',
                'desc'              => 'Reconocimiento agregado correctamente',
                'lastIdHonor'       => $honorResponse['lastId'],
                'lastHonorRelation' => $assignResponse['lastId']
            );

            echo json_encode($arrResponse);

        }

        if($action == 'addRescuer'){

            $objSurvivor = new SurvivorProject($idSurvivor);
            
            $items = Input::get('items');

            $name = $items[0]['name'];

            $surname = $items[0]['surname'];


            try{

                $rescuerResponse = $objSurvivor->addRescuer($name, $surname);

            }catch(Exception $e){

                onErrorException($e->getMessage(), $e->getCode());

            }

            try{

               $assignResponse = $objSurvivor->assignRescuer($rescuerResponse['lastId']);

            }catch(Exception $e){

                onErrorException($e->getMessage(), $e->getCode());

            }


            $arrResponse = array(
                'status'            => 'success',
                'title'             => 'Agregado',
                'desc'              => 'Rescatador agregado correctamente',
                'lastIdHonor'       => $rescuerResponse['lastId'],
                'lastHonorRelation' => $assignResponse['lastId']
            );

            echo json_encode($arrResponse);

        }
  
    }

}

function onErrorException($msg, $code){
    

    $arrResponse = array(
        'status'=> 'fail',
        'title' => 'Error',
        'desc'  => 'Error al agregar nuevo dato. Intento mas tarde nuevamente',
        'msg'   => $msg,
        'code'  => $code
    );

    echo json_encode($arrResponse);

    exit();

}