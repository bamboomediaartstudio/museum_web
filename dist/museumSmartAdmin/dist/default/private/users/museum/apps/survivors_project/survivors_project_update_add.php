<?php

/**
 * @summary Update Select Add new item in edit mode
 *
 * @description -
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo 
 */


/**all the includes */

require_once '../../../core/init.php';


/**validate.. */

$user = new User();


if($user->isLoggedIn()){

    $validate = new Validate();

    $validation = $validate->check($_POST, array(

            'action' => array('display'=> 'action', 'required' => true)
        )
    );


}



if($validation->passed()){


    $jsonObject = json_decode(Input::get('items'));

    $idSurvivor = Input::get('id_survivor');

    $action = Input::get('action');

    if($action == 'books')
    {

        $table = "app_museum_survivors_project_books_relations";

        $columnName = "id_book";

        addItems($jsonObject, $idSurvivor, $table, $columnName);

    }else if($action == 'films'){

        $table = "app_museum_survivors_project_films_relations";

        $columnName = "id_film";

        addItems($jsonObject, $idSurvivor, $table, $columnName);

    }else if($action == 'honors'){

        $table = "app_museum_survivors_project_honors_relations";

        $columnName = "id_honor";

        addItems($jsonObject, $idSurvivor, $table, $columnName);

    }else if($action == 'grandsons'){

        $table = "app_museum_survivors_projects_family_relations_relations_table";

        $columnName = "id_relation";

        $family_type = 3;

        addFamilyItems($jsonObject, $idSurvivor, $table, $columnName, $family_type);
    }else if($action == 'sons'){

        $table = "app_museum_survivors_projects_family_relations_relations_table";

        $columnName = "id_relation";

        $family_type = 2;

        addFamilyItems($jsonObject, $idSurvivor, $table, $columnName, $family_type);
    }else if($action == 'spouses'){

        $table = "app_museum_survivors_projects_family_relations_relations_table";

        $columnName = "id_relation";

        $family_type = 1;

        addFamilyItems($jsonObject, $idSurvivor, $table, $columnName, $family_type);
    }else if($action == 'guetos'){

        $table = "app_museum_survivors_project_guetos_relations";

        $columnName = "id_gueto";

        addItems($jsonObject, $idSurvivor, $table, $columnName);

    }else if($action == 'campos'){

        $table = "app_museum_survivors_project_campos_relations";

        $columnName = "id_campo";

        addItems($jsonObject, $idSurvivor, $table, $columnName);

    }else if($action == 'referenceds'){

        $table = "app_museum_survivors_project_referenced_countries_relations";

        $columnName = "id_country";

        addItems($jsonObject, $idSurvivor, $table, $columnName);

    }else if($action == 'rescuers'){

        $table = "app_museum_survivors_project_rescuers_relations";

        $columnName = "id_rescuer";

        addItems($jsonObject, $idSurvivor, $table, $columnName);

    }else{
        $status['msg'] = 'Error. what we are doing here?';

        $status['button'] = 'Salir';

        echo json_encode($status);
    }


   

}


function addItems($items, $idSurvivor, $table, $column_name){

    $db = DB::getInstance();


    $cantToAdd = count($items);
    $added = 0;

    $responseArr = array();

    foreach($items as $key){

        $db->insert($table, ["id_survivor"=>$idSurvivor, $column_name=>$key->id]);

        $added++;

        $lastInsertedId = $db->lastId();

        $responseArr[$lastInsertedId] = array(

            "lastId"    => $lastInsertedId,
            "id"        => $key->id,
            "name"      => $key->text

        );

    }


    $msg = ($added==$cantToAdd) ? "Actualizado correctamente" : "Error al actualizar uno o mas registros";

    $status['title'] = "Actualizado";

    $status['msg'] = $msg;

    $status['button'] = 'Entendido';

    $status['itemInsertedData'] = json_encode($responseArr);
    

    echo json_encode($status);

    return;

}


function addFamilyItems($items, $idSurvivor, $table, $column_name, $family_type){


    $db = DB::getInstance();


    $cantToAdd = count($items);
    $added = 0;

    $responseArr = array();

    foreach($items as $key){

        $db->insert($table, ["id_survivor"=>$idSurvivor, $column_name=>$key->id, "id_relation_type"=> $family_type]);

        $added++;

        $lastInsertedId = $db->lastId();

        $responseArr[$lastInsertedId] = array(

            "lastId"    => $lastInsertedId,
            "id"        => $key->id,
            "name"      => $key->text

        );

    }

    $msg = ($added==$cantToAdd) ? "Actualizado correctamente" : "Error al actualizar uno o mas registros";

    $status['title'] = "Actualizado";

    $status['msg'] = $msg;

    $status['button'] = 'Entendido';

    $status['itemInsertedData'] = json_encode($responseArr);
    

    echo json_encode($status);

    return;



}






?>