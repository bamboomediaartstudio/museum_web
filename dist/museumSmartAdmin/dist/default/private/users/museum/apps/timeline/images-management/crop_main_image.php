<?php

include 'smart_resize_image.function.php';

include '../connect.php';

define('UPLOAD_DIR', '../../../assets/img/mainGallery/');

$jsondata = array();

$iWidth = (int)$_POST['width'];

$iHeight = (int)$_POST['height'];

$x = (int)$_POST['x'];

$y = (int)$_POST['y'];

$img = $_POST['imgBase64'];

$img = str_replace('data:image/png;base64,', '', $img);

$img = str_replace(' ', '+', $img);

$data = base64_decode($img);

$imageId = uniqid();

$imageFolder = UPLOAD_DIR . $imageId . '/';

if (!file_exists($imageFolder)) { mkdir($imageFolder, 0777, true); }

$file = $imageFolder . $imageId . '_original.png';

$success = file_put_contents($file, $data);

$vImg = @imagecreatefrompng($file);

$iJpgQuality = 100;

$vDstImg = @imagecreatetruecolor($iWidth, $iHeight);


imagecopyresampled( $vDstImg,			//imagen de DESTINO 
					
					$vImg,				//imagen SOUCE 
					
					0,					//DESTINO x... 
					
					0,					//DESTINO y... 
					
					$x, 				//x de la imagen que copiamos SOURCE
					
					$y,					//y de la imagen que copiamos SOURCE
					
					$iWidth,			//el width de la imagen destino 
					
					$iHeight, 			//el height de la imagen de destino
					
					$iWidth,			//el width de la imagen SOURCE. 
					
					$iHeight);			//el height de la imagen SOURCE.

$sResultFileName = $imageFolder . $imageId . '_crop.png';

$medium = $imageFolder . $imageId . '_medium.png';

$xs = $imageFolder . $imageId . '_xs.png';

imagejpeg($vDstImg, $sResultFileName, $iJpgQuality);

smart_resize_image($sResultFileName, null, 1920/2 , 1080/2, true , $medium , false , false ,100 );

smart_resize_image($sResultFileName, null, 1920/4 , 1920/4, true , $xs , false , false ,100 );

$sql = "INSERT INTO maingallery (idFile, extension, isActive) VALUES ('$imageId', 'png', 1)";

$conn->query($sql);

$conn->close();

echo json_encode($jsondata);

?>