<?php

include('../connect.php');

if(!isset($_POST['id']) || empty($_POST['id']) || 

	!isset($_POST['isInWhiteList']) || empty($_POST['isInWhiteList']) || 

	!isset($_POST['tag'])|| empty($_POST['tag'])){

	$status['code'] = 1;

	$status['msg'] = 'missing variables';

}else{

	$isInWhiteList = $_POST['isInWhiteList'];
	
	$id = $_POST['id'];

	$tag = $_POST['tag'];

	if($isInWhiteList == "false"){

		//si no esta en la lista de tags, agregarlo a la lista general...

		$sql = "INSERT INTO tags (tag) VALUES (?)";

		$createTag = $conn->prepare($sql);

		$createTag->bind_param('s', $tag);

		$createTag->execute();

		$tagId = $createTag->insert_id;

		//luego agregarlo a la lista propia...

		$sql = "INSERT INTO timeline_tags (id_tag, id_timeline_event) VALUES (?, ?)";

		$eventTag = $conn->prepare($sql);

		$eventTag->bind_param('ii', $tagId, $id);

		$eventTag->execute();
		
		$status['eventId'] = $createTag->insert_id;

		$status['msg'] = "se agrego a la lista y luego al evento...";

	}else{

		//seleccionar el ID de la tabla de tags...

		$sql = "SELECT id FROM tags WHERE tag = ?";

		$selectTag = $conn->prepare($sql);

		$selectTag->bind_param('s', $tag);

		$selectTag->execute();

		$selectTag->store_result();

		$selectTag->bind_result($tagId);

		$selectTag->fetch();

		//asignarlo...

		$sql = "INSERT INTO timeline_tags (id_tag, id_timeline_event) VALUES (?, ?)";

		$eventTag = $conn->prepare($sql);

		$eventTag->bind_param('ii', $tagId, $id);

		$eventTag->execute();
		
		$status['eventId'] = $tagId;

		$status['msg'] = "listo...se agrego uno al evento...";

		$status['isInWhiteList'] = $isInWhiteList;
	}

}

echo json_encode($status);

?>