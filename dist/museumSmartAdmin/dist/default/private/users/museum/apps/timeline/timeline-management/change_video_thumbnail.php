<?php

	include 'smart_resize_image.function.php';

	$file = $_FILES['image']['tmp_name'];

	//move_uploaded_file($file, $toPath);

	$hd = '../../../../timelineApp/bin/images/' . $_POST['idEvent'] . '/' . $_POST['videoName'] . '/' . $_POST['videoName'] . '_HD_SCALED.png';
	
	$hdp = '../../../../timelineApp/bin/images/' . $_POST['idEvent'] . '/' . $_POST['videoName'] . '/' . $_POST['videoName'] . '_HD_PROPORTIONAL.png';
	
	$sq = '../../../../timelineApp/bin/images/' . $_POST['idEvent'] . '/' . $_POST['videoName'] . '/' . $_POST['videoName'] . '_SQ.png';
	
	$xsq = '../../../../timelineApp/bin/images/' . $_POST['idEvent'] . '/' . $_POST['videoName'] . '/' . $_POST['videoName'] . '_XSQ.png';

	$thumb = '../../../../timelineApp/bin/images/' . $_POST['idEvent'] . '/' . $_POST['videoName'] . '/' . $_POST['videoName'] . '_16_9.png';

	smart_resize_image($file, null, 1920, 1080, true, $hd, false, false, 100);

	smart_resize_image($file, null, 1920, 1080, false, $hdp, false, false, 100);

	smart_resize_image($file, null, 500, 500, false, $sq, false, false, 100);

	smart_resize_image($file, null, 150, 150, false, $xsq, false, false, 100);

	smart_resize_image($file, null, 150, 85, false, $thumb, false, false, 100);


	$status['msg'] = 'ok :)';

	echo json_encode($status);

?>