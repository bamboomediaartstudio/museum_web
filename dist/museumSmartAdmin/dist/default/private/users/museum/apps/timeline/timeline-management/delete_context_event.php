<?php

require_once('../connect.php');

if (!isset($_POST['id']) || empty($_POST['id']) || !isset($_POST['db']) || empty($_POST['db'])){

	$status['code'] = -1;

	$status['msg'] = 'missing get variables...';

	echo json_encode($status);

	return;

}else{

	$id = $_POST['id'];

	$db = $_POST['db'];

	$sql = "UPDATE " . $db ." SET date_start = ?, date_end = ?, title = ?, description = ? WHERE id_event = ?";

	$stmt = $conn->prepare($sql);

	$ds = NULL;

	$de = NULL;

	$t = NULL;

	$d = NULL;

	$stmt->bind_param('ssssi', $ds, $de, $t, $d, $id);

	$stmt->execute();

	$status['code'] = 1;

	$status['msg'] = 'tod ok :)';

}

echo json_encode($status);

$stmt->close();

?>