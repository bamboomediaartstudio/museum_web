<?php

include('../connect.php');

if(!isset($_POST['id']) || empty($_POST['id']) || !isset($_POST['tag'])|| empty($_POST['tag'])){

	$status['code'] = 1;

	$status['msg'] = 'missing variables';

}else{

	$id = $_POST['id'];
	
	$tag = $_POST['tag'];

	//primero obtener el ID del tag...

	$sql = "SELECT id FROM tags WHERE tag = ?";

	$selectTag = $conn->prepare($sql);

	$selectTag->bind_param('s', $tag);

	$selectTag->execute();

	$selectTag->store_result();

	$selectTag->bind_result($tagId);

	$selectTag->fetch();

	//luego quita....

	$sql = "UPDATE timeline_tags SET deleted = ?, active = ? WHERE id_timeline_event = ? AND id_tag = ?";

	$stmt = $conn->prepare($sql);

	$active = 0;

	$deleted = 1;

	$stmt->bind_param('iiis', $deleted, $active, $id, $tagId);

	$stmt->execute();

	$status['msg'] = "listo...se elimino el evento";

}

echo json_encode($status);

?>