<?php

require_once('../connect.php');

if (!isset($_POST['id']) || empty($_POST['id'])){

	$status['code'] = -1;

	$status['msg'] = 'missing get variables...';

	echo json_encode($status);

	return;

}else{
	
	$id = $_POST['id'];

	$result = array();
}


$sql = "SELECT timelineTags.id, timelineTags.id_tag, tags.tag FROM timeline_tags as timelineTags

INNER JOIN tags as tags ON timelineTags.id_tag = tags.id WHERE timelineTags.id_timeline_event = ?";

$tagsStmt = $conn->prepare($sql);

$tagsStmt->bind_param('i', $id);

$tagsStmt->execute();

$tagsStmt->store_result();

$tagsStmt->bind_result($idRelation, $idTag, $tag);

if ($tagsStmt->num_rows >= 1) {

	while($tagsStmt->fetch()){

		$item = array();

		$item['idRelation'] = $idRelation;

		$item['idTag'] = $idTag;

		$item['idEvent'] = $id;

		$item['tag'] = $tag;

		 $result[] = $item;

	}

}

echo json_encode($result, JSON_PRETTY_PRINT);

die();

?>