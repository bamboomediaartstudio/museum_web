<?php

/**
 * @summary delete booking
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the includes */

require_once '../../core/init.php';

/**all the variables */

$status['init'] = true;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_booking_update_max_visitors'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array( 

	'id' => array('display'=> 'id', 'required' => true),

	'institutionId' => array('display'=> 'institutionId', 'required' => true)

));

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	if($user->isLoggedIn()){

		//insert new one...

		$insert = $db->insert('museum_booking_institutions_relations',[

					'id_institution'=> Input::get('institutionId'),

					'id_booking'=> Input::get('id')]);
		
		$last = DB::getInstance()->query('UPDATE museum_booking set booked_places = ? WHERE id = ?', 

				[1, Input::get('id')]);

		
		$data = DB::getInstance()->query('SELECT * FROM museum_booking WHERE id = ?', 

			[Input::get('id')]);

		$mySuperData =  $data->first();

		setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

		$saveDate = ' día <b>' . utf8_encode(strftime("%A, %d de %B del %Y", strtotime($mySuperData->date_start))) . '</b>';

		$saveDate.= ' desde las ' . strftime("%H:%M:%S", strtotime($mySuperData->date_start));

		$saveDate.= ' hasta las ' . strftime("%H:%M:%S", strtotime($mySuperData->date_end));

		//pending...

		$pendings = DB::getInstance()->query('SELECT * FROM museum_booking_institutions_relations as mbir LEFT JOIN museum_booking as mb ON mbir.id_booking = mb.id WHERE mbir.id_institution = ?', [Input::get('institutionId')]);

		$counter = 0;

		if($pendings->count() >= 1){

			$active = '';

			foreach($pendings->results() as $pending){ 

				$counter+=1;

				$active.= '<b>turno' . $counter . ':</b><br><br>';

				$active.= '<b>día: </b>' . utf8_encode(strftime("%A, %d de %B del %Y", strtotime($pending->date_start))) . '<br>';

				$active.= '<b>comienzo del turno: </b>' . strftime("%H:%M:%S", strtotime($pending->date_start)) . '<br>';

				$active.= '<b>fin del turno: </b>' . strftime("%H:%M:%S", strtotime($pending->date_end)) . '<br><br>';

			}


		}else{

			$active = '';

		}

		//instiution data...

		$institutionDataQuery = DB::getInstance()->query('SELECT * FROM museum_booking_institutions WHERE id = ?', [Input::get('institutionId')]);

		$dataRow = $institutionDataQuery->first();


		//email...

		$settingsQ = $db->query("SELECT * FROM settings");

		$settings = $GLOBALS['settings'] = $settingsQ->first();

		$supportSettingsQ = $db->query("SELECT * FROM support_settings");

		$GLOBALS['supportSettings'] = $supportSettingsQ->first();

		$museumSettingsQ = $db->query("SELECT * FROM museum_data");

		$museumSettings = $museumSettingsQ->first();

		//social media...

		$museumSocialMediaQ = $db->query("SELECT * FROM museum_sm WHERE active = ? AND deleted = ? ORDER BY internal_order", [1, 0]);

		$smArray = array();

		foreach($museumSocialMediaQ->results() as $result){

		$smArray[$result->name] = $result->url;

		}

		//end of social media...

		$year = date("Y");

		$address = $museumSettings->address . ' ' . $museumSettings->address_number . ', ' . $museumSettings->zip_code . ' ' . $museumSettings->city;


		$msg = file_get_contents('../../../email_templates/add_institution_subscription.html');

		$msg = str_replace('%projectTitle%', $museumSettings->title, $msg);

		$msg = str_replace('%institution%', $dataRow->institution_name, $msg);
		
		$msg = str_replace('%contactName%', $dataRow->inscription_contact_name, $msg);

		$msg = str_replace('%saveDate%', $saveDate, $msg);

		$msg = str_replace('%active%', $active, $msg);

		$msg = str_replace('%ip%', Helpers::getIP(), $msg);

		$msg = str_replace('%os%', Helpers::getOS(), $msg);

		$msg = str_replace('%browser%', Helpers::getBrowser(), $msg);

		$msg = str_replace('%email%', $GLOBALS['supportSettings']->support_email, $msg);

		$msg = str_replace('%address%', $address, $msg);

		$msg = str_replace('%te%', $museumSettings->te, $msg);

		$msg = str_replace('%museumEmail%', $museumSettings->email, $msg);

		$msg = str_replace('%lat%', $museumSettings->lat, $msg);

		$msg = str_replace('%lon%', $museumSettings->lon, $msg);

		$msg = str_replace('%facebook%', $smArray['Facebook'], $msg);

		$msg = str_replace('%instagram%', $smArray['Instagram'], $msg);

		$msg = str_replace('%twitter%', $smArray['Twitter'], $msg);

		$msg = str_replace('%youtube%', $smArray['Youtube'], $msg);

		$msg = str_replace('%year%', $year, $msg);

		$sent = Helpers::email($dataRow->inscription_contact_email, utf8_decode('Se asignó un nuevo turno!'), $msg);

		$status['envio'] = 'done!';

		echo json_encode($status);

		exit();

	}

}


/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>