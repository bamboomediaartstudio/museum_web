<?php

require_once '../../core/init.php';

$db = DB::getInstance();

$status['init'] = true;

$validate = new Validate();

$data;

$validation = $validate->check($_POST, array(

	'inscription-institution-name' => array('display'=>'inscription-institution-name', 'required' => true),

	'inscription-institution-cuit' => array('display'=>'inscription-institution-cuit', 'required' => true),

	'inscription-institution-address' => array('display'=>'inscription-institution-address', 'required' => true),

	'inscription-institution-phone' => array('display'=>'inscription-institution-phone', 'required' => true),

	'inscription-institution-email' => array('display'=>'inscription-institution-email', 'required' => true),

	'inscription-institution-state' => array('display'=>'inscription-institution-state', 'required' => true),

	'education-level' => array('display'=>'education-level', 'required' => true),

	'institution-type' => array('display'=>'institution-type', 'required' => true),

	'iva' => array('display'=>'iva', 'required' => true),

	'special-need' => array('display'=>'special-need', 'required' => true),

	'inscription-contact-name' => array('display'=>'inscription-contact-name', 'required' => true),

	'inscription-contact-email' => array('display'=>'inscription-contact-email', 'required' => true),

	'inscription-contact-phone' => array('display'=>'inscription-contact-phone', 'required' => true),

	'first-time' => array('display'=>'first-time', 'required' => true),

	'institution_students' => array('display'=>'institution_students', 'required' => true),

	//'bookings_ids' => array('display'=>'bookings_ids', 'required' => true),

	'institution-first-time' => array('display'=>'institution-first-time', 'required' => true)));

if($validation->passed()){

	$status['validation'] = 'ok';

	$test = ip2long(Helpers::getIP());

	$resolve =  long2ip($test);

	$insert = $db->insert('museum_booking_institutions',[

		'institution_name'=> Input::get('inscription-institution-name'),

		'institution_cuit'=> Input::get('inscription-institution-cuit'),

		'institution_address'=> Input::get('inscription-institution-address'),

		'institution_phone'=> Input::get('inscription-institution-phone'),

		'institution_email'=> Input::get('inscription-institution-email'),

		'institution_state'=> Input::get('inscription-institution-state'),

		'institution_students'=> Input::get('institution_students'),

		'eduction_level'=> Input::get('education-level'),

		'education_grade'=> Input::get('education-grade'),

		'institution_type'=> Input::get('institution-type'),

		'special_need'=> Input::get('special-need'),

		'iva'=> Input::get('iva'),

		'origin'=> Input::get('origin'),

		'inscription_contact_name'=> Input::get('inscription-contact-name'),

		'inscription_contact_email'=> Input::get('inscription-contact-email'),

		'inscription_contact_phone'=> Input::get('inscription-contact-phone'),

		'first_time'=> Input::get('first-time'),

		'institution_first_time'=> Input::get('institution-first-time')]);

	$lastId = $db->lastId();

	//add relations...

	$data = Input::get('bookings_ids');

	foreach($data as $id){

		$insert = $db->insert('museum_booking_institutions_relations',

			['id_institution'=>$lastId, 'id_booking' =>$id[0]]);

	}

	//add booked places...

	foreach($data as $id){

		$query = $db->query("UPDATE museum_booking SET booked_places= ? WHERE id = ?", [1, $id[0]]); 

	}

	//select dates...

	$saveDate = '';

	setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

	$counter = 0;

	foreach ($data as $id) {

		$counter+=1;

		$query = $db->query('SELECT * from museum_booking WHERE id = ?', [$id[0]]);

		$res = $query->first();

		$saveDate.= '<b>turno ' . $counter . ':</b><br><br>';

		$saveDate.= '<b>día: </b>' . utf8_encode(strftime("%A, %d de %B del %Y", strtotime($res->date_start))) . '<br>';

		$saveDate.= '<b>comienzo del turno: </b>' . strftime("%H:%M:%S", strtotime($res->date_start)) . '<br>';

		$saveDate.= '<b>fin del turno: </b>' . strftime("%H:%M:%S", strtotime($res->date_end)) . '<br><br>';

	}

	//mailing...

	$settingsQ = $db->query("SELECT * FROM settings");

	$settings = $GLOBALS['settings'] = $settingsQ->first();

	$supportSettingsQ = $db->query("SELECT * FROM support_settings");

	$GLOBALS['supportSettings'] = $supportSettingsQ->first();

	$museumSettingsQ = $db->query("SELECT * FROM museum_data");

	$museumSettings = $museumSettingsQ->first();

	//social media...

	$museumSocialMediaQ = $db->query("SELECT * FROM museum_sm WHERE active = ? AND deleted = ? ORDER BY internal_order", [1, 0]);

	$smArray = array();

	foreach($museumSocialMediaQ->results() as $result){

		$smArray[$result->name] = $result->url;

	}

	//end of social media...

	$year = date("Y");

	$address = $museumSettings->address . ' ' . $museumSettings->address_number . ', ' . $museumSettings->zip_code . ' ' . $museumSettings->city;

	//for institution

	if(Input::get('institution-type') == 'privada'){

		$msg = file_get_contents('../../../email_templates/sender_group_subscription_private.html');

	}else{

		$msg = file_get_contents('../../../email_templates/sender_group_subscription_public.html');

	}

	$msg = str_replace('%projectTitle%', $museumSettings->title, $msg);

	$msg = str_replace('%name%', Input::get('inscription-contact-name'), $msg);
	
	$msg = str_replace('%saveDate%', $saveDate, $msg);

	$msg = str_replace('%students%', Input::get('institution_students'), $msg);

	$msg = str_replace('%te%', Input::get('inscription-contact-phone'), $msg);

	$msg = str_replace('%institution%', Input::get('inscription-institution-name'), $msg);
	
	$msg = str_replace('%ip%', Helpers::getIP(), $msg);

	$msg = str_replace('%os%', Helpers::getOS(), $msg);

	$msg = str_replace('%browser%', Helpers::getBrowser(), $msg);

	$msg = str_replace('%email%', $GLOBALS['supportSettings']->support_email, $msg);

	$msg = str_replace('%address%', $address, $msg);

	$msg = str_replace('%te%', $museumSettings->te, $msg);

	$msg = str_replace('%museumEmail%', $museumSettings->email, $msg);

	$msg = str_replace('%lat%', $museumSettings->lat, $msg);

	$msg = str_replace('%lon%', $museumSettings->lon, $msg);

	$msg = str_replace('%facebook%', $smArray['Facebook'], $msg);

	$msg = str_replace('%instagram%', $smArray['Instagram'], $msg);

	$msg = str_replace('%twitter%', $smArray['Twitter'], $msg);

	$msg = str_replace('%youtube%', $smArray['Youtube'], $msg);

	$msg = str_replace('%year%', $year, $msg);

	$sent = Helpers::email(Input::get('inscription-contact-email'), utf8_decode('Recepción de inscripción'), $msg);
	//$sent = Helpers::email('mariano.makedonsky@gmail.com', utf8_decode('Recepción de inscripción'), $msg);

	//...
	//for museum

	$msg2 = file_get_contents('../../../email_templates/group_subscription.html');

	$msg2 = str_replace('%projectTitle%', $museumSettings->title, $msg2);

	$msg2 = str_replace('%institution%', Input::get('inscription-institution-name'), $msg2);

	$msg2 = str_replace('%students%', Input::get('institution_students'), $msg2);

	$msg2 = str_replace('%saveDate%', $saveDate, $msg2);

	$msg2 = str_replace('%institutionName%', Input::get('inscription-institution-name'), $msg2);

	$msg2 = str_replace('%institutionCuit%', Input::get('inscription-institution-cuit'), $msg2);

	$msg2 = str_replace('%institutionIva%', Input::get('iva'), $msg2);

	$msg2 = str_replace('%address%', Input::get('inscription-institution-address'), $msg2);

	$msg2 = str_replace('%phone%', Input::get('inscription-institution-phone'), $msg2);

	$msg2 = str_replace('%email%', Input::get('inscription-institution-email'), $msg2);

	$msg2 = str_replace('%state%', Input::get('inscription-institution-state'), $msg2);

	$msg2 = str_replace('%city%', Input::get('inscription-institution-city'), $msg2);

	$msg2 = str_replace('%educationLevel%', Input::get('education-level'), $msg2);

	$msg2 = str_replace('%institutionType%', Input::get('institution-type'), $msg2);

	if(Input::get('special-need') == 'si'){

		$specialNeed = '<b>IMPORTANTE:</b> la institución ' . Input::get('inscription-institution-name') . ' indicó la participación de personas con capacidades especiales. Tené a bien contactarte con ellos al ' . Input::get('inscription-institution-phone') . ' o escribirles a ' . Input::get('inscription-institution-email') . ' para solicitar información al respecto. También podés contactar a ' . Input::get('inscription-contact-name') . ', quien será la persona a cargo. Podés llamarlo al ' . Input::get('inscription-contact-phone') . ' o escribirle al ' . Input::get('inscription-contact-email');
	}else{

		$specialNeed = '';

	}

	$msg2 = str_replace('%accesibility%', $specialNeed, $msg2);

	$msg2 = str_replace('%contactName%', Input::get('inscription-contact-name'), $msg2);

	$msg2 = str_replace('%contactEmail%', Input::get('inscription-contact-email'), $msg2);

	$msg2 = str_replace('%contactPhone%', Input::get('inscription-contact-phone'), $msg2);
	
	$msg2 = str_replace('%ip%', Helpers::getIP(), $msg2);

	$msg2 = str_replace('%os%', Helpers::getOS(), $msg2);

	$msg2 = str_replace('%browser%', Helpers::getBrowser(), $msg2);

	$msg2 = str_replace('%email%', $GLOBALS['supportSettings']->support_email, $msg2);

	$msg2 = str_replace('%address%', $address, $msg2);

	$msg2 = str_replace('%te%', $museumSettings->te, $msg2);

	$msg2 = str_replace('%museumEmail%', $museumSettings->email, $msg2);

	$msg2 = str_replace('%lat%', $museumSettings->lat, $msg2);

	$msg2 = str_replace('%lon%', $museumSettings->lon, $msg2);

	$msg2 = str_replace('%facebook%', $smArray['Facebook'], $msg2);

	$msg2 = str_replace('%instagram%', $smArray['Instagram'], $msg2);

	$msg2 = str_replace('%twitter%', $smArray['Twitter'], $msg2);

	$msg2 = str_replace('%youtube%', $smArray['Youtube'], $msg2);

	$msg2 = str_replace('%year%', $year, $msg2);

	$sent2 = Helpers::email('mariano.makedonsky@gmail.com', utf8_decode('Nueva inscripción de institución'), $msg2);
	
	//$sent2 = Helpers::email('visitasguiadas@museodelholocausto.org.ar', utf8_decode('Nueva inscripción de institución'), $msg2);

	if($sent && $sent2){

		$status['envio'] = 'sent';
		
		$status['resoslve'] = $resolve;

	}else{

		$status['envio'] = 'ops!';

	}

	
}else{

	$status['envio'] = 'no exitoso';

}

echo json_encode($status);

return;


?>