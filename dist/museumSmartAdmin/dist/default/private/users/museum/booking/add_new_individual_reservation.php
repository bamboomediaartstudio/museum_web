<?php

require_once '../../core/init.php';

$db = DB::getInstance();

$status['init'] = true;

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'inscription-name' => array('display'=>'inscription-name', 'required' => true),

	'inscription-surname' => array('display'=>'inscription-surname', 'required' => true),

	'inscription-email' => array('display'=>'inscription-email', 'required' => true),

	'inscription-dni' => array('display'=>'inscription-dni', 'required' => true),

	'inscription-phone' => array('display'=>'inscription-phone', 'required' => true),

	'booking-id' => array('display'=>'booking-id', 'required' => true),

	'tickets-per-person' => array('display'=>'tickets-per-person', 'required' => true)
));

if($validation->passed()){

	$status['validation'] = 'ok';

	$test = ip2long(Helpers::getIP());

	$resolve =  long2ip($test);

	$status['inscription-name'] = Input::get('inscription-name');

	$status['inscription-surname'] = Input::get('inscription-surname');

	$status['inscription-email'] = Input::get('inscription-email');

	$status['inscription-phone'] = Input::get('inscription-phone');

	$status['inscription-dni'] = Input::get('inscription-dni');

	$status['booking-id'] = Input::get('booking-id');

	DB::getInstance()->insert('museum_booking_assistants',[

		'booking_id'=> Input::get('booking-id'),

		'name'=> Input::get('inscription-name'),

		'surname'=> Input::get('inscription-surname'),
		
		'email'=> Input::get('inscription-email'),

		'phone' => Input::get('inscription-phone'),

		'about_museum' => Input::get('about-museum'),

		'about_museum' => Input::get('about-museum'),

		'visit_reason' => Input::get('visit-reason'),

		'origin' => Input::get('origin'),

		'tickets_per_person' => Input::get('tickets-per-person'),

		'dni' => Input::get('inscription-dni'),

		'ip' => ip2long(Helpers::getIP()),

		'os' => Helpers::getOS(),

		'browser' => Helpers::getBrowser(),

		'added' => date("Y-m-d H:i:s")]

	);


	DB::getInstance()->query(

		'UPDATE museum_booking SET booked_places = booked_places + ? WHERE id = ?', 

		[Input::get('tickets-per-person'), Input::get('booking-id')]

	);

	$status['readablePlace'] = Input::get('readablePlace');

	$status['readableHour'] = Input::get('readableHour');

	$status['readableDay'] = Input::get('readableDay');

	//mailing

	//general settings...

	$settingsQ = $db->query("SELECT * FROM settings");

	$settings = $GLOBALS['settings'] = $settingsQ->first();

	$supportSettingsQ = $db->query("SELECT * FROM support_settings");

	$GLOBALS['supportSettings'] = $supportSettingsQ->first();

	$museumSettingsQ = $db->query("SELECT * FROM museum_data");

	$museumSettings = $museumSettingsQ->first();

	//social media...

	$museumSocialMediaQ = $db->query("SELECT * FROM museum_sm WHERE active = ? AND deleted = ? ORDER BY internal_order", [1, 0]);

	$smArray = array();

	foreach($museumSocialMediaQ->results() as $result){

		$smArray[$result->name] = $result->url;

	}

	//end of social media...

	$year = date("Y");

	$address = $museumSettings->address . ' ' . $museumSettings->address_number . ', ' . $museumSettings->zip_code . ' ' . $museumSettings->city;

	//for museum

	$msg = file_get_contents('../../../email_templates/individual_subscription.html');

	$msg = str_replace('%projectTitle%', $museumSettings->title, $msg);

	$msg = str_replace('%name%', Input::get('inscription-name'), $msg);

	$msg = str_replace('%surname%', Input::get('inscription-surname'), $msg);
	
	$msg = str_replace('%subsEmail%', Input::get('inscription-email'), $msg);

	$msg = str_replace('%subsPhone%', Input::get('inscription-phone'), $msg);

	$msg = str_replace('%day%', Input::get('readableDay'), $msg);

	$msg = str_replace('%date%', Input::get('readableHour'), $msg);

	$msg = str_replace('%place%', Input::get('readablePlace'), $msg);
	
	$msg = str_replace('%ip%', Helpers::getIP(), $msg);

	$msg = str_replace('%os%', Helpers::getOS(), $msg);

	$msg = str_replace('%browser%', Helpers::getBrowser(), $msg);

	$msg = str_replace('%email%', $GLOBALS['supportSettings']->support_email, $msg);

	$msg = str_replace('%address%', $address, $msg);

	$msg = str_replace('%te%', $museumSettings->te, $msg);

	$msg = str_replace('%museumEmail%', $museumSettings->email, $msg);

	$msg = str_replace('%lat%', $museumSettings->lat, $msg);

	$msg = str_replace('%lon%', $museumSettings->lon, $msg);

	$msg = str_replace('%facebook%', $smArray['Facebook'], $msg);

	$msg = str_replace('%instagram%', $smArray['Instagram'], $msg);

	$msg = str_replace('%twitter%', $smArray['Twitter'], $msg);

	$msg = str_replace('%youtube%', $smArray['Youtube'], $msg);

	$msg = str_replace('%year%', $year, $msg);

	//$sent = Helpers::email('visitasguiadas@museodelholocausto.org.ar', utf8_decode('Nueva inscripcion a visita guiada'), $msg);
	
	$sent = Helpers::email('mariano.makedonsky@gmail.com', utf8_decode('Nueva suscripción a visita guiada'), $msg);

	//for sender...

	$msg2 = file_get_contents('../../../email_templates/sender_individual_subscription.html');
	//$msg2 = file_get_contents('../../../email_templates/sender_individual_subscription_temp.html');

	$msg2 = str_replace('%projectTitle%', $museumSettings->title, $msg2);

	$msg2 = str_replace('%name%', Input::get('inscription-name'), $msg2);

	$msg2 = str_replace('%surname%', Input::get('inscription-surname'), $msg2);

	$msg2 = str_replace('%day%', Input::get('readableDay'), $msg2);

	$msg2 = str_replace('%date%', Input::get('readableHour'), $msg2);

	$msg2 = str_replace('%place%', Input::get('readablePlace'), $msg2);
	
	$msg2 = str_replace('%ip%', Helpers::getIP(), $msg2);

	$msg2 = str_replace('%os%', Helpers::getOS(), $msg2);

	$msg2 = str_replace('%browser%', Helpers::getBrowser(), $msg2);

	$msg2 = str_replace('%email%', $GLOBALS['supportSettings']->support_email, $msg2);

	$msg2 = str_replace('%address%', $address, $msg2);

	$msg2 = str_replace('%te%', $museumSettings->te, $msg2);

	$msg2 = str_replace('%museumEmail%', $museumSettings->email, $msg2);

	$msg2 = str_replace('%lat%', $museumSettings->lat, $msg2);

	$msg2 = str_replace('%lon%', $museumSettings->lon, $msg2);

	$msg2 = str_replace('%facebook%', $smArray['Facebook'], $msg2);

	$msg2 = str_replace('%instagram%', $smArray['Instagram'], $msg2);

	$msg2 = str_replace('%twitter%', $smArray['Twitter'], $msg2);

	$msg2 = str_replace('%youtube%', $smArray['Youtube'], $msg2);

	$msg2 = str_replace('%year%', $year, $msg2);

	$sent2 = Helpers::email(Input::get('inscription-email'), utf8_decode('Suscripción exitosa!'), $msg2);

	if($sent && $sent2){

		$status['envio'] = 'sent';
		
		$status['resoslve'] = $resolve;

	}else{

		$status['envio'] = 'ops!';

	}

}else{

	$status['envio'] = 'no exitoso';

}

echo json_encode($status);

return;

?>