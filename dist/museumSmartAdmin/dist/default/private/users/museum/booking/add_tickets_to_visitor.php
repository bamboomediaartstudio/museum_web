<?php

/**
 * @summary Upate the max n of tickets visitors...
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the includes */

require_once '../../core/init.php';

/**all the variables */

$status['init'] = true;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_booking_update_max_tickets_per_visitor'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array( 

	'id' => array('display'=> 'id', 'required' => true),

	'bookingId' => array('display'=> 'bookingId', 'required' => true),

	'tickets_per_person' => array('display'=> 'tickets_per_person', 'required' => true)

));

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	if($user->isLoggedIn()){

		//obtener cuantos eran antes...

		$db->query('SELECT tickets_per_person FROM museum_booking_assistants WHERE id = ?', [Input::get('id')]);

		$q = $db->first();

		$previousTickets = $q->tickets_per_person;

		//

		//tambien cambiar en user...

		if($previousTickets > Input::get('tickets_per_person')){

			$diff = $previousTickets - Input::get('tickets_per_person');

			DB::getInstance()->query(

				'UPDATE museum_booking SET booked_places = booked_places - ? WHERE id = ?', 

				[$diff, Input::get('bookingId')]

			);

		}else{

			$diff = Input::get('tickets_per_person') - $previousTickets;

			DB::getInstance()->query(

				'UPDATE museum_booking SET booked_places = booked_places + ? WHERE id = ?', 

				[$diff, Input::get('bookingId')]

			);

		}

		$fields=array('tickets_per_person'=>Input::get('tickets_per_person'));

		$db->update('museum_booking_assistants',Input::get('id'), $fields);

		//...

		addLogData($user);

		printData(1, 'ok');
	}

}

/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 		user object.
*/

function addLogData($user){

	$systemString = 'The ammount of tickets for a visitor was reasigned.';

	$userString = 'Actualizaste la cantidad de tickets para un visitante.';

	Logger::addLogData($user->data()->id, 

		'user',

		$systemString,

		$userString,

		'tickets',

		'tickets');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>