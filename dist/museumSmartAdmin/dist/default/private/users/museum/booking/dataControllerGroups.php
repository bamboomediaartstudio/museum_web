<?php

//News Data Controller (list data)

require_once '../../core/init.php';

include '../../services/ListDataResponseGroups.class.php';

//Read data from datatables

$columnIndex = $_POST['order'][0]['column'];

$columnName = $_POST['columns'][$columnIndex]['data'];

$setArr = array(

	"draw"        => $_POST['draw'],

	"row"         => $_POST['start'],

	"rowPerPage"  => $_POST['length'],

	"columnName"  => $columnName,

	"searchValue" => $_POST['search']['value']
	
);

$objPrensaListData = new ListDataResponseGroups(

	$setArr, 

	$_POST["table"], 

	$_POST["searchColumn"], 

	(isset($_POST["searchColumnTwo"]) ? $_POST["searchColumnTwo"] : ""),

	$_POST["date"], 

	$_POST["type"]

);

echo json_encode($objPrensaListData->getTotalData());


?>
