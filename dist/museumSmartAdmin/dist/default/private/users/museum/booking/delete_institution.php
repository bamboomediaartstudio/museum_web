<?php

/**
 * @summary delete tickets
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the includes */

require_once '../../core/init.php';

/**all the variables */

$status['init'] = true;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_booking_update_max_visitors'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array( 

	'id' => array('display'=> 'id', 'required' => true)

));

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	if($user->isLoggedIn()){

		$query = DB::getInstance()->query('SELECT * FROM museum_booking_institutions_relations WHERE id_institution = ?', [Input::get('id')]);

		foreach($query->results() as $result){

			$bookingId = $result->id_booking;

			DB::getInstance()->query('DELETE FROM museum_booking_institutions_relations WHERE id_institution = ?', [Input::get('id')]);

			DB::getInstance()->query('UPDATE museum_booking set booked_places = ? WHERE id = ?', 

				[0, $bookingId]);
		}

		//$query = DB::getInstance()->query('DELETE * from museum_booking_institutions_relations WHERE id_institution = ?', [Input::get('id')]);

		//$secondQuery = DB::getInstance()->query('UPDATE set booked_places = ? WHERE id = ?');

		//foreach($query->results() as $result){

		//}

		/*DB::getInstance()->query(

			'UPDATE museum_booking SET booked_places = ? WHERE id = ?', 

			[0, Input::get('id')]

		);

		$userData = $db->query('SELECT * from museum_booking_assistants WHERE id = ?', [Input::get('userId')]);

		$userRow = $userData->first();

		$userEmail = $userRow->email;*/

		$status['envio'] = 'done!';

		echo json_encode($status);

		exit();

		
	}

}


/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>
