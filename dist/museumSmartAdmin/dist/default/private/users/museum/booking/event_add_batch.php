<?php

/**
 * @summary Add new event...
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the includes */

require_once '../../core/init.php';

/**all the variables */

$status['init'] = true;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_booking_event_add'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists', null);

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'fromTime' => array('display'=> 'fromTime', 'required' => true),

	'toTime' => array('display'=> 'toTime', 'required' => true),

	'forGroups' => array('display'=> 'forGroups', 'required' => true),

	'dates' => array('display'=> 'dates', 'reqiured' => true)));

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed', null);

else{

	$availablePlaces;

	if(Input::get('forGroups') == "false"){

		$availablePlaces = 8;

		$sid = 2;

	} else{

		$availablePlaces = 1;

		$sid = 1;

	}

	$user = new User();

	if($user->isLoggedIn()){

		$parseData = json_decode(Input::get('dates'));

		foreach($parseData as $key) {

			$dateStart = $key->date . ' ' . Input::get('fromTime');

			$dateEnd = $key->date . ' ' . Input::get('toTime');

			$db->insert('museum_booking',[			

				'sid'=> $sid,

				'date_start'=> $dateStart,

				'date_end'=> $dateEnd,

				'available_places'=> $availablePlaces


			]);
				
			$lastId = $db->lastId();

			if(Input::get('guide') && Input::get('guide') != -1){

				$db->insert('museum_booking_guides',['id_booking'=> $lastId, 'id_guide'=> Input::get('guide')]);

			}
		}		

	}


	$status['fromTime']  = Input::get('fromTime');
	
	$status['toTime']  = Input::get('toTime');

	$status['forGroups']  = Input::get('forGroups');

	$status['guide']  = Input::get('guide');

	$status['dates']  = Input::get('dates');
	
	$status['availablePlaces']  = $availablePlaces;

	echo json_encode($status);

	exit();

}

/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 		user object.
*/

function addLogData($user){

	$systemString = 'a new booking event was added.';

	$userString = 'Agregaste un nuevo horario de reserva.';

	Logger::addLogData($user->data()->id, 

		'user',

		$systemString,

		$userString,

		'booking',

		'booking');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from, $lastId){

	$status['from'] = $from;

	$status['lastId'] = $lastId;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>