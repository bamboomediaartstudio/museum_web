<?php

require_once '../../core/init.php';

$db = DB::getInstance();

$status = [];

$query = $db->get('museum_booking',['id','=',Input::get('id')], false);

$status['tickets_per_visitor'] = $query->first()->tickets_per_visitor;

$status['available_places'] = $query->first()->available_places;

$status['booked_places'] = $query->first()->booked_places;

echo json_encode($status);

 exit();

?>