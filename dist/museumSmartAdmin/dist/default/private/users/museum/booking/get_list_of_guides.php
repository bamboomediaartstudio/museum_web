<?php

require_once '../../core/init.php';

$db = DB::getInstance();

$status = [];

$query = 'SELECT * FROM museum_guides WHERE active = ? AND deleted = ?';

$query = $db->query($query, [1, 0]);

if($query){

	$status['items'][] = "";

	foreach ($db->results() as $result){

		$status['items'][$result->id] = $result->name . ' ' . $result->surname;

	}

}

$secondQuery = 'SELECT * FROM museum_booking_guides WHERE id_booking = ? AND active = ? AND deleted = ?';

$secondQuery = $db->query($secondQuery, [Input::get('id'), 1, 0]);

if($db->count()>0){

	$status['selected'] = $secondQuery->first()->id_guide;

}else{

	$status['selected'] = 0;

}

header('Content-Type: application/json');

$json =  json_encode($status, JSON_PRETTY_PRINT);
 
 print_r($json);
?>