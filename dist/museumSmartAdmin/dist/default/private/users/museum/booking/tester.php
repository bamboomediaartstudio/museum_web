<?php

/**
 * @summary delete booking
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the includes */

require_once '../../core/init.php';

/**all the variables */

$status['init'] = true;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_booking_update_max_visitors'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

//if(!Input::exists()) printData(0, 'data not exists');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_GET, array( 

	'id' => array('display'=> 'id', 'required' => true)

));

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	if($user->isLoggedIn()){

		//get id

		$query = DB::getInstance()->query('SELECT * FROM museum_booking as mb inner join museum_booking_institutions_relations as mbir on mb.id = mbir.id_booking inner join museum_booking_institutions as mbi on mbir.id_institution = mbi.id WHERE mb.id = ?', [Input::get('id')]);

		$dataRow = $query->first();

		setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

		$saveDate = ' día <b>' . utf8_encode(strftime("%A, %d de %B del %Y", strtotime($dataRow->date_start))) . '</b>';

		$saveDate.= ' desde las ' . strftime("%H:%M:%S", strtotime($dataRow->date_start));

		$saveDate.= ' hasta las ' . strftime("%H:%M:%S", strtotime($dataRow->date_end));

		DB::getInstance()->query('DELETE FROM museum_booking_institutions_relations WHERE id_booking = ?', [Input::get('id')]);

		DB::getInstance()->query('UPDATE museum_booking set booked_places = ? WHERE id = ?',  [0, Input::get('id')]);

		//email...

		$settingsQ = $db->query("SELECT * FROM settings");

		$settings = $GLOBALS['settings'] = $settingsQ->first();

		$supportSettingsQ = $db->query("SELECT * FROM support_settings");

		$GLOBALS['supportSettings'] = $supportSettingsQ->first();

		$museumSettingsQ = $db->query("SELECT * FROM museum_data");

		$museumSettings = $museumSettingsQ->first();

		//social media...

		$museumSocialMediaQ = $db->query("SELECT * FROM museum_sm WHERE active = ? AND deleted = ? ORDER BY internal_order", [1, 0]);

		$smArray = array();

		foreach($museumSocialMediaQ->results() as $result){

		$smArray[$result->name] = $result->url;

		}

		//end of social media...

		$year = date("Y");

		$address = $museumSettings->address . ' ' . $museumSettings->address_number . ', ' . $museumSettings->zip_code . ' ' . $museumSettings->city;

		$msg = file_get_contents('../../../email_templates/delete_institution_subscription.html');

		$msg = str_replace('%projectTitle%', $museumSettings->title, $msg);

		$msg = str_replace('%institution%', $dataRow->institution_name, $msg);
		
		$msg = str_replace('%contactName%', $dataRow->inscription_contact_name, $msg);

		$msg = str_replace('%saveDate%', $saveDate, $msg);

		$msg = str_replace('%ip%', Helpers::getIP(), $msg);

		$msg = str_replace('%os%', Helpers::getOS(), $msg);

		$msg = str_replace('%browser%', Helpers::getBrowser(), $msg);

		$msg = str_replace('%email%', $GLOBALS['supportSettings']->support_email, $msg);

		$msg = str_replace('%address%', $address, $msg);

		$msg = str_replace('%te%', $museumSettings->te, $msg);

		$msg = str_replace('%museumEmail%', $museumSettings->email, $msg);

		$msg = str_replace('%lat%', $museumSettings->lat, $msg);

		$msg = str_replace('%lon%', $museumSettings->lon, $msg);

		$msg = str_replace('%facebook%', $smArray['Facebook'], $msg);

		$msg = str_replace('%instagram%', $smArray['Instagram'], $msg);

		$msg = str_replace('%twitter%', $smArray['Twitter'], $msg);

		$msg = str_replace('%youtube%', $smArray['Youtube'], $msg);

		$msg = str_replace('%year%', $year, $msg);

		$sent = Helpers::email('mariano.makedonsky@gmail.com', utf8_decode('Se eliminó un turno'), $msg);

		$status['envio'] = 'done!';

		echo json_encode($status);

		exit();
		
	}

}


/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>