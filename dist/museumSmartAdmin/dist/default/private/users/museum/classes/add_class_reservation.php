<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

require_once '../../core/init.php';

require_once('../../../framework/mailer/Emailer.php');

require_once('../../../framework/mailer/EmailTemplate.php');


$status['errors'] = false;

$status['init'] = true;

$db = DB::getInstance();

if(Input::exists()){

	$validate = new Validate();

	$validation = $validate->check($_POST, array(

		'virtual-class-inscription-name' => array('display'=>'virtual-class-inscription-name', 'required' => true),

		'virtual-class-inscription-surname' => array('display'=>'virtual-class-inscription-surname', 'required' => true),

		'virtual-class-inscription-email' => array('display'=>'virtual-class-inscription-email', 'required' => true),

		'virtual-class-inscription-phone' => array('display'=>'virtual-class-inscription-phone', 'required' => true),

		'id' => array('display'=>'id', 'required' => true),

		'booking-id' => array('display'=>'booking-id', 'required' => true)

	));

		
	if($validation->passed()){
	
      	DB::getInstance()->insert('museum_virtual_classes_events_inscriptions',[

			'id_event'=> Input::get('booking-id'),

			'id_class'=> Input::get('id'),

			'name'=> Input::get('virtual-class-inscription-name'),

			'surname'=> Input::get('virtual-class-inscription-surname'),

			'email'=> Input::get('virtual-class-inscription-email'),
			
			'phone'=> Input::get('virtual-class-inscription-phone')]);

      }

      DB::getInstance()->query(

		'UPDATE museum_virtual_classes_events SET booked_places = booked_places + ? WHERE id = ?', [Input::get('tickets-per-person'), Input::get('booking-id')]

	);

      $emailer = new Emailer([Input::get('virtual-class-inscription-email')], 'Invitación');

      $template = new EmailTemplate('../../../framework/mailer/templates/custom.php');

      $template->title = 'Suscripción a clase virtual: ' . Input::get('class');

      $inviteString = 'hola ' . Input::get('virtual-class-inscription-name') . ' ' . Input::get('virtual-class-inscription-surname') . '!<br><br>';
      
      $inviteString .= 'Recibimos tu inscripción a la clase virtual <strong> ' . Input::get('class') . '</strong> el ' . Input::get('readableDay') . ' ' . Input::get('readableHour');
      
      $inviteString .= 'Para conocer los datos de tu registro, utilizá el siguiente link:<br><br>';

      $inviteString .= 'https://museodelholocausto.org.ar/proximoEvento/index.php?id=' . Input::get('booking-id') . '<br><br>';

      $inviteString .= 'Si los datos del <b>Zoom Meeting</b> aun no fueron asignados, volvé a entrar al link en los próximos días. Puede que esta información sea publicada 24hs antes de la clase virtual. Si a menos de 24hs del evento no ves la información del Zoom, contactanos.<br><br>';

      $template->bodyContent = $inviteString;

      $emailer->SetTemplate($template);

      $mailWasSent = $emailer->send();

      //...

      $institutionMailing = new Emailer(['visitasguiadas@museodelholocausto.org.ar'], 'Invitación');

      $institutionTemplate = new EmailTemplate('../../../framework/mailer/templates/custom.php');

      $institutionTemplate->title = 'Nueva suscripción a clase virtual: ' . Input::get('class') ;

      $institutionString = 'Buenas noticias! hay un nuevo suscripto a la clase virtual <strong>' . Input::get('class') . '</strong> del ' . Input::get('readableDay') . ' ' . Input::get('readableHour') . '.<br><br>';

      $institutionString .= '<strong>Nombre: </strong>' . Input::get('virtual-class-inscription-name') . '<br>';

      $institutionString .= '<strong>Apellido: </strong>' . Input::get('virtual-class-inscription-surname') . '<br>';

      $institutionString .= '<strong>Email: </strong>' . Input::get('virtual-class-inscription-email') . '<br>';

      $institutionString .= '<strong>Teléfono: </strong>' . Input::get('virtual-class-inscription-phone') . '<br><br>';

      $institutionTemplate->bodyContent = $institutionString;

      $institutionMailing->SetTemplate($institutionTemplate);

      $mailWasSent = $institutionMailing->send();

  }


echo json_encode($status);

return;

?>


