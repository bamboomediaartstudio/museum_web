<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

require_once '../../core/init.php';

require_once('../../../framework/mailer/Emailer.php');

require_once('../../../framework/mailer/EmailTemplate.php');


$status['errors'] = false;

$status['init'] = true;

$museumEmail = 'visitasguiadas@museodelholocausto.org.ar'; //(20-11-2020 mail Gabriel pidiendo cambio de email a info@mdh )
//$museumEmail = 'info@museodelholocausto.org.ar';

$db = DB::getInstance();

if(Input::exists()){

	$validate = new Validate();

	$validation = $validate->check($_POST, array(

            'institution-virtual-class-inscription-students' => array('display'=>'institution-virtual-class-inscription-students', 'required' => true),

            'institution-virtual-class-inscription-name' => array('display'=>'institution-virtual-class-inscription-name', 'required' => true),

            'institution-virtual-class-inscription-email' => array('display'=>'institution-virtual-class-inscription-email', 'required' => true, 'email'=>true),

            'institution-virtual-class-inscription-phone' => array('display'=>'institution-virtual-class-inscription-phone', 'required' => true),

            'institution-virtual-class-inscription-name-2' => array('display'=>'institution-virtual-class-inscription-name-2', 'required' => true),

            'id' => array('display'=>'id', 'required' => true),

		'booking-id' => array('display'=>'booking-id', 'required' => true)

	));


	if($validation->passed()){

      	DB::getInstance()->insert('museum_booking_institutions',[

                  'institution_students' => Input::get('institution-virtual-class-inscription-students'),

                  'institution_name'=> Input::get('institution-virtual-class-inscription-name-2'),

                  'institution_address'=> Input::get('institution-virtual-class-inscription-address'),

                  'institution_state'=> Input::get('institution-inscription-state'),

                  'inscription_contact_name'=> Input::get('institution-virtual-class-inscription-name'),

                  'inscription_contact_email'=> Input::get('institution-virtual-class-inscription-email'),

                  'inscription_contact_phone'=> Input::get('institution-virtual-class-inscription-phone'),

                  'origin'=> 'class',

                  'eduction_level'=> Input::get('education-level'),

                  'education_grade'=> Input::get('education-grade'),

                  'institution_type'=> Input::get('institution-type')

            ]);

            $institutionId = DB::getInstance()->lastId();

            DB::getInstance()->insert('museum_virtual_classes_institutions_inscriptions_relations',[

                  'id_event'=> Input::get('booking-id'),

                  'id_class'=> Input::get('id'),

                  'id_institution'=> $institutionId

            ]);

            DB::getInstance()->query(

                  'UPDATE museum_virtual_classes_events SET booked_places = booked_places + ? WHERE id = ?', [Input::get('tickets-per-person'), Input::get('booking-id')]

            );


            $emailer = new Emailer([Input::get('institution-virtual-class-inscription-email')], 'Suscripción a clase virtual.');

            $template = new EmailTemplate('../../../framework/mailer/templates/custom.php');

            $template->title = 'Suscripción a clase virtual: ' . Input::get('class');

            $inviteString = 'hola <b>' . Input::get('institution-virtual-class-inscription-name') . '</b><br><br>';

            $inviteString .= 'Recibimos tu inscripción a la clase virtual: <strong> ' . Input::get('class') . '</strong> para ' . Input::get('institution-virtual-class-inscription-students') .  ' alumnos de la institución <strong>' . Input::get('institution-virtual-class-inscription-name-2')  .  '</strong> <br><br>';

            $inviteString .= 'La clase virtual será el ' . Input::get('readableDay') . ' ' . Input::get('readableHour') .  ' <br><br>';

						$inviteString .= 'Para conocer los datos de registro, utilizá el siguiente link:<br><br>';

						$inviteString .= 'https://museodelholocausto.org.ar/proximoEvento/institutions.php?id=' . Input::get('booking-id') . '<br><br>';

            $inviteString .= 'Nos pondremos en contacto para ultimar detalles y enviarte el acceso a la plataforma. Recordá que la invitación y los datos de acceso para los '  . Input::get('institution-virtual-class-inscription-students') .  ' alumnos que participarán de la clase virtual queda a cargo de la institución<br><br>';



						//New correction


            $inviteString .= 'El funcionamiento del Recorrido Virtual 360° / de la clase ' . Input::get('class') . ' está sujeto a la disponibilidad técnica de quienes realizan la actividad. En caso de algún problema técnico o de conexión que impida la realización de la actividad, lo informaremos por mail.<br><br>';

            $inviteString .= 'Gracias.';

            $template->bodyContent = $inviteString;

            $emailer->SetTemplate($template);

            $mailWasSent = $emailer->send();



            //...

            $institutionMailing = new Emailer([$museumEmail], 'Invitación - Institución');

            $institutionTemplate = new EmailTemplate('../../../framework/mailer/templates/custom.php');

            $institutionTemplate->title = 'Suscripción de institución a clase virtual: ' . Input::get('class') ;

            $institutionString = 'Buenas noticias! hay una nueva institución suscripta a la clase virtual <strong>' . Input::get('class') . '</strong> del ' . Input::get('readableDay') . ' ' . Input::get('readableHour') . '.<br><br>';

            $institutionString .= '<strong>Persona de contacto: </strong>' . Input::get('institution-virtual-class-inscription-name') . '<br>';

            $institutionString .= '<strong>Email: </strong>' . Input::get('institution-virtual-class-inscription-email') . '<br>';

            $institutionString .= '<strong>Teléfono: </strong>' . Input::get('institution-virtual-class-inscription-phone') . '<br><br><br>';

            $institutionString .= '<strong>INSTITUCIÓN: </strong><br><br>';

            $institutionString .= '<strong>Nombre: </strong>' . Input::get('institution-virtual-class-inscription-name-2') . '<br>';

            $institutionString .= '<strong>Dirección: </strong>' . Input::get('institution-virtual-class-inscription-address') . '<br>';

            $institutionString .= '<strong>Tipo de institución: </strong>' . Input::get('institution-type') . '<br>';

            $institutionString .= '<strong>Nivel: </strong>' . Input::get('education-level') . '<br><br>';

            $institutionString .= 'Esta información se encuentra disponible en el administrador de contenidos del museo.';

            $institutionTemplate->bodyContent = $institutionString;

            $institutionMailing->SetTemplate($institutionTemplate);

            $mailWasSent = $institutionMailing->send();

            $status['init'] = $mailWasSent;


      }



  }


echo json_encode($status);

return;

?>
