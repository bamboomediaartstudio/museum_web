<?php

/**
 * @summary Add new alert to the DB
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0	
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the includes */

require_once '../../core/init.php';

require_once('../../../framework/mailer/Emailer.php');

require_once('../../../framework/mailer/EmailTemplate.php');

$status['init'] = true;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_alert_add'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

if(!Input::exists()) printData(0, 'error');

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'name' => array('display'=> 'name', 'required' => true),

	'surname' => array('display'=> 'surname', 'required' => true),

	'email' => array('display'=> 'email', 'required' => true),

	'pass' => array('display'=> 'pass', 'required' => true)));

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	if($user->isLoggedIn()){

		$newSalt = Hash::salt(33);

		$savePass = Hash::make(Input::get('pass'), $newSalt);

		$dt = new DateTime(Input::get('calendar-date'));

		$date = $dt->format('Y-m-d');

		$db->insert('museum_experience_access',[

			'name'=> Input::get('name'),

			'surname'=> Input::get('surname'),

			'email'=> Input::get('email'),

			'password'=> $savePass,

			'salt'=> $newSalt,

			'expire'=> $date
		]);

		$lastId = $db->lastId();

		//...

		$st = 'Hola ' . Input::get('name') . ' ' . Input::get('surname') . '!<br><br>';

		$st .= 'Te invitamos a descubrir la versión full del Recorrido Virtual 360° del <b>Museo Del Holocausto</b>. Para acceder, hacelo de la siguiente manera:<br><br>';

		$st .= '1 - Ingresá a <a href="https://museodelholocausto.org.ar/360">https://museodelholocausto.org.ar/360</a><br>';

		$st .= '2 - Cliqueá en el botón del borde superior izquierdo, que dice <b>acceso privado</b><br>';

		$st .= '3 - Ingresá tu correo: <b>' . Input::get('email')  . '</b><br>';

		$st .= '4 - Ingresá la siguiente contraseña: <b>' . Input::get('pass')  . '</b><br><br>';

		$st .= 'Esperamos que lo disfrutes!';


		$emailer = new Emailer([Input::get('email')], 'Aceso full a Recorrido Virtual 360°.');
		
		$template = new EmailTemplate('../../../framework/mailer/templates/custom.php');

		$template->title = 'Aceso full a Recorrido Virtual 360°.';
 
		$template->bodyContent = $st;

		$emailer->SetTemplate($template);

		$mailWasSent = $emailer->send();

		//...

		printData(1, 'ok');
		
	}

}


/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['status'] = 1;

	$status['title'] = "usuario añadido!";

	$status['msg'] = "Se añadió un nuevo usuario full y se le está enviando la invitación por email en este momento, con su password y el link de acceso.";

	$status['alert']  = "success";

	$status['button']  = "entendido!";

	echo json_encode($status);

	exit();
}

?>