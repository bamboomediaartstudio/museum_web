<?php

/**
 * @summary Add new batch to virtual classes...
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the includes */

require_once '../../core/init.php';

require_once('../../../framework/mailer/Emailer.php');

require_once('../../../framework/mailer/EmailTemplate.php');


/**all the variables */

$status['init'] = true;

$db = DB::getInstance();

$className = '';

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists', null);

/**initialize validation process */

$validate = new Validate();

$sentMailToGuide = false;

$sentMailToHost = false;

$batchCounter = 0;

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'id', 'required' => true),

	'fromTime' => array('display'=> 'fromTime', 'required' => true),

	'toTime' => array('display'=> 'toTime', 'required' => true),

	'forGroups' => array('display'=> 'forGroups', 'required' => true),

	'dates' => array('display'=> 'dates', 'reqiured' => true)));

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed', null);

else{

	//hacer llegar.

	if(Input::get('forGroups') == 'true'){

		$availablePlaces = 1;

		$sid = 1;

	}else{

		$sid = 2;

		$availablePlaces = 90;

	}

	$user = new User();

	if($user->isLoggedIn()){

		$parseData = json_decode(Input::get('dates'));

		foreach($parseData as $key) {

			$dateStart = $key->date . ' ' . Input::get('fromTime');

			$dateEnd = $key->date . ' ' . Input::get('toTime');

			$batchCounter+=1;

			$db->insert('museum_virtual_classes_events',[			

				'sid'=> Input::get('id'),

				'booking_type'=> $sid,

				'date_start'=> $dateStart,

				'date_end'=> $dateEnd,

				'available_places'=> $availablePlaces

			]);
				
			$lastId = $db->lastId();

			if(Input::get('guide') && Input::get('guide') != -1){

				//inser guide...
				
				$db->insert('museum_virtual_classes_events_guides_relations',['id_event'=> $lastId, 'id_guide'=> Input::get('guide')]);

				$sentMailToGuide = true;


			}else{
				
				$db->insert('museum_virtual_classes_events_guides_relations',['id_event'=> $lastId, 'id_guide'=> -1]);
			
			}

			//...

			if(Input::get('host') && Input::get('host') != -1){
				
				$db->insert('museum_virtual_classes_events_hosts_relations',['id_event'=> $lastId, 'id_host'=> Input::get('host')]);

				$sentMailToHost = true;

			}else{
				$db->insert('museum_virtual_classes_events_hosts_relations',['id_event'=> $lastId, 'id_host'=> -1]);
			}
		
		}

		if($sentMailToGuide == true){

			//class data...

			$classQuery = DB::getInstance()->query('SELECT * FROM museum_virtual_classes WHERE id = ?', [Input::get('id')])->first();

			$className = $classQuery->name;

			$classUrl = $classQuery->url;

			$query = $db->query("SELECT * FROM users where id = ?", [Input::get('guide')])->first();

			$fullName = $query->name . ' ' . $query->surname;

			$email = $query->email;

			//$emailer = new Emailer(['mariano.makedonsky@gmail.com'], 'Invitación');
			
			$emailer = new Emailer([$email], 'Invitación');

			$template = new EmailTemplate('../../../framework/mailer/templates/custom.php');

			$template->title = 'Se te asignaron ' . $batchCounter . ' nuevos turnos!';

			$inviteString = 'hola ' . $fullName . '! :)<br><br>';

			$inviteString .= 'Los administradores de <b>Museo Del Holocausto</b> te asignaron el rol de <b>guía</b> en ' . $batchCounter . ' nuevos turnos para la clase virtual <a target = "_blank" href="https://museodelholocausto.org.ar/clases/' . $classUrl . '">' . $className . '</a>';
			
			$template->bodyContent = $inviteString;

			$emailer->SetTemplate($template);

			$mailWasSent = $emailer->send();

		}

		//...

		if($sentMailToHost == true){

			//class data...

			$classQuery = DB::getInstance()->query('SELECT * FROM museum_virtual_classes WHERE id = ?', [Input::get('id')])->first();

			$className = $classQuery->name;

			$classUrl = $classQuery->url;

			$query = DB::getInstance()->query("SELECT * FROM users where id = ?", [Input::get('host')])->first();

			$fullName = $query->name . ' ' . $query->surname;

			$email = $query->email;

			//$emailer = new Emailer(['mariano.makedonsky@gmail.com'], 'Invitación');
			
			$emailer = new Emailer([$email], 'Invitación');

			$template = new EmailTemplate('../../../framework/mailer/templates/custom.php');

			$template->title = 'Se te asignaron ' . $batchCounter . ' nuevos turnos!';

			$inviteString = 'hola ' . $fullName . '! :)<br><br>';

			$inviteString .= 'Los administradores de <b>Museo Del Holocausto</b> te asignaron el rol de <b>anfitrión</b> en  ' . $batchCounter . ' nuevos turnos para la clase virtual <a target = "_blank" href="https://museodelholocausto.org.ar/clases/' . $classUrl . '">' . $className . '</a>';
			
			$template->bodyContent = $inviteString;

			$emailer->SetTemplate($template);

			$mailWasSent = $emailer->send();

		}


	}


	$status['id']  = Input::get('id');

	echo json_encode($status);

	exit();

}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from, $lastId){

	$status['from'] = $from;

	$status['lastId'] = $lastId;

	echo json_encode($status);

	exit();
}

?>