<?php

/**
 * @summary Add new zoom data...
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0	
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

require_once '../../core/init.php';

require_once('../../../framework/mailer/Emailer.php');

require_once('../../../framework/mailer/EmailTemplate.php');

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

$status['init'] = true;

$db = DB::getInstance();

if(!Input::exists()) printData(0, 'data not exists');


if(!Token::check(Input::get('token'))) printData(0, 'token problem');

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'zoom-url' => array('display'=> 'zoom-url', 'required' => true),

	'zoom-id' => array('display'=> 'zoom-id', 'required' => true),

	'zoom-account' => array('display'=> 'zoom-account', 'required' => true),

	'idEvent' => array('display'=> 'idEvent', 'required' => true),

	'zoom-password' => array('display'=> 'zoom-password', 'required' => true)));

if(!$validation->passed()) printData(0, 'validation not passed');

$user = new User();

if($user->isLoggedIn()){
	
	$newSalt = Hash::salt(33);

	$hashedToken = Hash::make(Input::get('zoom-password'), $newSalt);

	$reference = substr(Input::get('zoom-password'), -4);
	
	$passwordLength = strlen(Input::get('zoom-password'));

	$enc = openssl_encrypt(Input::get('zoom-password'),"AES-128-ECB",'Muse@2021!');

	$fields = array('zoom_account'=>Input::get('zoom-account'), 'zoom_url'=>Input::get('zoom-url'), 'zoom_id'=>Input::get('zoom-id'), 'zoom_password'=>$hashedToken, 'zoom_salt' => $newSalt, 'zoom_password_reference' => $reference, 'zoom_password_length'=>$passwordLength, 'zoom_password_encrypt'=>$enc);

	DB::getInstance()->update('museum_virtual_classes_events', Input::get('idEvent'), $fields);

	//--------------------------------------------------------------------------------------------------------
	//insitution...

	if(Input::get('sendEmailtoInstitution') == 1){

		$query = DB::getInstance()->query('SELECT *, mvc.name as class_name FROM museum_virtual_classes_events as mvce 

			INNER JOIN museum_virtual_classes_institutions_inscriptions_relations AS mvciir

			ON mvce.id = mvciir.id_event

			INNER JOIN museum_booking_institutions as mbi

			ON mvciir.id_institution = mbi.id 

			INNER JOIN museum_virtual_classes as mvc

			ON mvce.sid = mvc.id

			WHERE mvce.id = ?', [Input::get('idEvent')])->first();

		$dtStart = new DateTime($query->date_start);

		$d = $dtStart->format('m/d/Y');

		$date = strftime("%A %d de %B del %Y", strtotime($query->date_start));

		$dtEnd = new DateTime($query->date_end);

		$timeStart = $dtStart->format('H:i');
		
		$timeEnd = $dtEnd->format('H:i');

		$st = 'Hola <b>' . $query->inscription_contact_name . '.</b><br><br> Te escribimos para enviarte el link de Zoom para la clase virtual <b>' . $query->class_name . '</b> que tendrá lugar el día ' . $date . ' a las ' . $timeStart . ' para los ' . $query->institution_students . ' alumnos de la institución <b>' . $query->institution_name . '.</b><br><br>';

		$st .= '<b>link:</b> ' . $query->zoom_url . '<br>';

		$st .= '<b>id:</b> ' . Input::get('zoom-id') . '<br>';

		$st .= '<b>contraseña:</b> ' . Input::get('zoom-password') . '<br><br>';


		$st .= 'Recordá que la invitación y los datos de acceso para los alumnos que participarán de la clase virtual queda a cargo de la institución.<br><br>';

		$emailer = new Emailer([$query->inscription_contact_email], 'link de Zoom.');

		$template = new EmailTemplate('../../../framework/mailer/templates/custom.php');

		$template->title = 'link de Zoom.';
 
		$template->bodyContent = $st;

		$emailer->SetTemplate($template);

		$mailWasSent = $emailer->send();

	}

	//--------------------------------------------------------------------------------------------------------
	//guide...

	if(Input::get('sendEmailToGuide') == 1){

		$query = DB::getInstance()->query('SELECT *, mvc.name as class_name, u.name as guide_name, u.surname as guide_surname, u.email as guide_email FROM museum_virtual_classes_events as mvce 

			INNER JOIN museum_virtual_classes_events_guides_relations as mvcegr

			ON mvce.id = mvcegr.id_event

			INNER JOIN users as u 

			ON mvcegr.id_guide = u.id

			INNER JOIN museum_virtual_classes as mvc

			ON mvce.sid = mvc.id 

			WHERE mvce.id = ?', [Input::get('idEvent')])->first();

		$dtStart = new DateTime($query->date_start);

		$d = $dtStart->format('m/d/Y');

		$date = strftime("%A %d de %B del %Y", strtotime($query->date_start));

		$dtEnd = new DateTime($query->date_end);

		$timeStart = $dtStart->format('H:i');
		
		$timeEnd = $dtEnd->format('H:i');

		$st = 'Hola <b>' . $query->guide_name . '.</b><br><br> Te escribimos para enviarte el link de Zoom para la clase virtual <b>' . $query->class_name . '</b> que tendrá lugar el día ' . $date . ' a las ' . $timeStart . '.<br><br>';

		$st .= '<b>link:</b> ' . $query->zoom_url . '<br>';

		$st .= '<b>id:</b> ' . Input::get('zoom-id') . '<br>';

		$st .= '<b>contraseña:</b> ' . Input::get('zoom-password') . '<br><br>';

		$st .= 'podés ver la fecha desde el administrador en el siguiente <b>link:</b><br><br>';

		$st .= 'https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/museum_virtual_classes_dates_list.php?date=' . date('Y-m-d', strtotime($query->date_start));

		$emailer = new Emailer([$query->guide_email], 'link de Zoom.');
				
		$template = new EmailTemplate('../../../framework/mailer/templates/custom.php');

		$template->title = 'link de Zoom.';
 
		$template->bodyContent = $st;

		$emailer->SetTemplate($template);

		$mailWasSent = $emailer->send();

	}

	//--------------------------------------------------------------------------------------------------------
	//host...

	if(Input::get('sendEmailToHost') == 1){

		$query = DB::getInstance()->query('SELECT *, mvc.name as class_name, u.name as host_name, u.surname as host_surname, u.email as host_email FROM museum_virtual_classes_events as mvce 

			INNER JOIN museum_virtual_classes_events_hosts_relations as mvcehr

			ON mvce.id = mvcehr.id_event

			INNER JOIN users as u 

			ON mvcehr.id_host = u.id

			INNER JOIN museum_virtual_classes as mvc

			ON mvce.sid = mvc.id 

			WHERE mvce.id = ?', [Input::get('idEvent')])->first();

		$dtStart = new DateTime($query->date_start);

		$d = $dtStart->format('m/d/Y');

		$date = strftime("%A %d de %B del %Y", strtotime($query->date_start));

		$dtEnd = new DateTime($query->date_end);

		$timeStart = $dtStart->format('H:i');
		
		$timeEnd = $dtEnd->format('H:i');

		$st = 'Hola <b>' . $query->host_name . ' ' . $query->host_surname  . '.</b><br><br> Te escribimos para enviarte el link de Zoom para la clase virtual <b>' . $query->class_name . '</b> que tendrá lugar el día ' . $date . ' a las ' . $timeStart . '.<br><br>';

		$st .= '<b>link:</b> ' . $query->zoom_url . '<br>';

		$st .= '<b>id:</b> ' . Input::get('zoom-id') . '<br>';

		$st .= '<b>contraseña:</b> ' . Input::get('zoom-password') . '<br><br>';

		$st .= 'podés ver la fecha desde el administrador en el siguiente <b>link:</b><br><br>';

		$st .= 'https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/museum_virtual_classes_dates_list.php?date=' . date('Y-m-d', strtotime($query->date_start));

		$emailer = new Emailer([$query->host_email], 'link de Zoom.');
		
		$template = new EmailTemplate('../../../framework/mailer/templates/custom.php');

		$template->title = 'link de Zoom.';
 
		$template->bodyContent = $st;

		$emailer->SetTemplate($template);

		$mailWasSent = $emailer->send();

	}

	printData(1, 'sent');

} else {

	printData(0, 'not logged');

}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	echo json_encode($status);

	exit();
}

?>