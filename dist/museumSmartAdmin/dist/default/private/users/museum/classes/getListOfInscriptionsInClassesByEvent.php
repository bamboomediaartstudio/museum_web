<?php

/**
* @summary change password...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

setlocale(LC_TIME, 'es_ES', 'esp_esp');

require('../../core/init.php');

require('../../../libraries/datatables/ssp.customized.class.php');

$user = new User();

if(!$user->isLoggedIn()) exit();


$table = 'museum_virtual_classes_events_inscriptions';

//$userId = $_GET['userId'];

$primaryKey = 'id';

$columns = array(

	array( 'db' => '`u`.`id`', 'dt' => 'id', 'field'=>'id'),

	array( 'db' => '`u`.`id_event`', 'dt' => 'id_event', 'field'=>'id_event'),

	array( 'db' => '`u`.`id_class`', 'dt' => 'id_class', 'field'=>'id_class'),

	array( 'db' => '`u`.`name`', 'dt' => 'name', 'field'=>'name', 'as' => 'name'),

	array( 'db' => '`u`.`surname`', 'dt' => 'surname', 'field'=>'surname'),
	
	array( 'db' => '`u`.`email`', 'dt' => 'email', 'field'=>'email'),

	array( 'db' => '`u`.`phone`', 'dt' => 'phone', 'field'=>'phone'),

	array( 'db' => '`u`.`assisted`', 'dt' => 'assisted', 'field'=>'assisted'),

	array( 'db' => '`uc`.`date_start`', 'dt' => 'date_start', 'field'=>'date_start', 'formatter' => function($d, $row) { return utf8_encode(strftime("%A, %d de %B de %Y", strtotime($d)));}),

	array( 'db' => '`uc`.`date_start`', 'dt' => 'readable_hour', 'field'=>'readable_hour', 'as' =>'readable_hour', 'formatter' => function($d, $row) { return utf8_encode(strftime("%H:%Mhs", strtotime($d)));}),

	array( 'db' => '`uc`.`date_end`', 'dt' => 'date_end', 'field'=>'date_end', 'formatter' => function($d, $row) { return utf8_encode(strftime("%A, %d de %B de %Y", strtotime($d)));}),

	array( 'db' => '`ucc`.`name`', 'dt' => 'class_name', 'field'=>'class_name', 'as' => 'class_name'),

	array( 'db' => '`uc`.`zoom_account`', 'dt' => 'zoom_account', 'field'=>'zoom_account', 'as' => 'zoom_account'),

	array( 'db' => '`uc`.`zoom_url`', 'dt' => 'zoom_url', 'field'=>'zoom_url', 'as' => 'zoom_url'),

	array( 'db' => '`uc`.`zoom_id`', 'dt' => 'zoom_id', 'field'=>'zoom_id', 'as' => 'zoom_id'),

	array( 'db' => '`uc`.`zoom_id`', 'dt' => 'zoom_id', 'field'=>'zoom_id', 'as' => 'zoom_id'),

	array( 'db' => '`uc`.`zoom_password_encrypt`', 'dt' => 'zoom_password_encrypt', 'field'=>'zoom_password_encrypt', 'as'=>'zoom_password_encrypt', 

		'formatter' => function($d, $row) { 

			return openssl_decrypt($d,"AES-128-ECB", 'Muse@2021!');

			}),

);

$whitelist = array('127.0.0.1', '::1');

$sql_details = array(

	'host'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '127.0.0.1' : '108.179.242.98',

	'user'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? 'root' : 'brpxmzm5_make',

	'pass'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '' : 'n},iT0aJTqnF',

	'db'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? 'museum_smart_admin' : 'brpxmzm5_museum_smart_admin'

);

$joinQuery = "FROM `museum_virtual_classes_events_inscriptions` AS `u` INNER JOIN `museum_virtual_classes_events` AS `uc` ON (`u`.`id_event` = `uc`.`id`) INNER JOIN `museum_virtual_classes` AS `ucc` ON (`u`.`id_class` = `ucc`.`id`)";

$extraWhere = "`u`.`deleted` = 0 AND `uc`.`id` = " . $_POST['eventId'];

header('Content-type: text/javascript');

echo json_encode( SSP::simple($_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere), JSON_PRETTY_PRINT );
?>

