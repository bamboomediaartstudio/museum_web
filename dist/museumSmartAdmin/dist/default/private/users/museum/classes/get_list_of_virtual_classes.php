<?php

require_once '../../core/init.php';

$db = DB::getInstance();

$status = [];

$query = 'SELECT * FROM museum_virtual_classes WHERE active = ? AND deleted = ?';

$query = $db->query($query, [1, 0]);

if($query){

	//$status['items'][] = "";

	foreach ($db->results() as $result){

		$status['items'][$result->id] = $result->name;

	}

}


header('Content-Type: application/json');

$json =  json_encode($status, JSON_PRETTY_PRINT);
 
 print_r($json);
?>