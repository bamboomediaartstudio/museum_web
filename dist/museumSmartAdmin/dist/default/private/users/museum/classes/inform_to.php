<?php

/**
 * @summary Add new zoom data...
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0	
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

require_once '../../core/init.php';

require_once('../../../framework/mailer/Emailer.php');

require_once('../../../framework/mailer/EmailTemplate.php');

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

$status['init'] = true;

$db = DB::getInstance();

if(!Input::exists()) printData(0, 'data not exists');

if(!Token::check(Input::get('token'))) printData(0, 'token problem');

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'id', 'required' => true),

	'type' => array('display'=> 'type', 'required' => true),

	'guideId' => array('display'=> 'guideId', 'required' => true)
)

);

if(!$validation->passed()) printData(0, 'validation not passed');

$user = new User();

if($user->isLoggedIn()){

		$guide = DB::getInstance()->query('SELECT * FROM users WHERE id = ?', [Input::get('guideId')])->first();

		$status['name'] = $guide->name;

		$status['surname'] = $guide->surname;
		
		$status['email'] = $guide->email;

		$status['guideId'] = Input::get('guideId');

		$event = DB::getInstance()->query('

			SELECT *, mvc.name as class_name from museum_virtual_classes_events as mvce

			INNER JOIN museum_virtual_classes as mvc

			ON mvce.sid = mvc.id

			WHERE mvce.id = ?', [Input::get('id')])->first();

		$status['event'] = $event->date_start;

		$status['end'] = $event->date_end;

		$status['class_name'] = $event->class_name;

		//...

		$dtStart = new DateTime($event->date_start);

		$d = $dtStart->format('m/d/Y');

		$date = strftime("%A %d de %B del %Y", strtotime($event->date_start));

		$dtEnd = new DateTime($event->date_end);

		$timeStart = $dtStart->format('H:i');
		
		$timeEnd = $dtEnd->format('H:i');

		//...

		$st = 'Hola <b>' . $guide->name . ' ' . $guide->surname . '!</b><br><br>';

		if(Input::get('type') == 'id_guide'){

			$st .= 'Te asignaron el rol de guía en el turno del ' . $date . ' a las ' . $timeStart . ' para la clase virtual <b>' . $event->class_name . '</b><br>';

			$st .= 'Te enviaremos un nuevo email cuando se asigne el zoom con su respectivo id, link y password.<br><br>';

			$st .= 'podés ver la nueva fecha desde el administrador en el siguiente <b>link:</b><br><br>';

			$st .= 'https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/museum_virtual_classes_dates_list.php?date=' . date('Y-m-d', strtotime($query->date_start));

			$st .='<br>';


		}else{

			$st .= 'Te asignaron el rol de anfitrión en el turno del ' . $date . ' a las ' . $timeStart . ' para la clase virtual <b>' . $event->class_name . '.</b>';

			$st .= 'Te enviaremos un nuevo email cuando se asigne el zoom con su respectivo id, link y password.<br><br>';

			$st .= 'podés ver la nueva fecha desde el administrador en el siguiente <b>link:</b><br><br>';

			$st .= 'https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/museum_virtual_classes_dates_list.php?date=' . date('Y-m-d', strtotime($query->date_start));

			$st .='<br>';

		}

		$status['st'] = $st;

		//$emailer = new Emailer(['mariano.makedonsky@gmail.com'], 'Te asignaron una clase virtual.');
		
		$emailer = new Emailer([$guide->email], 'Te asignaron una clase virtual.');
		
		$template = new EmailTemplate('../../../framework/mailer/templates/custom.php');

		$template->title = 'Te asignaron una clase virtual.';
 
		$template->bodyContent = $st;

		$emailer->SetTemplate($template);

		$mailWasSent = $emailer->send();

		echo json_encode($status);

} else {

	printData(0, 'not logged');

}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;
	
	echo json_encode($status);

	exit();
}

?>