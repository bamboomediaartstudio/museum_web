<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

ini_set('display_errors', 1);


require_once '../../core/init.php';

require_once('../../../framework/mailer/Emailer.php');

require_once('../../../framework/mailer/EmailTemplate.php');

$status['errors'] = false;

$status['init'] = true;

use PHPMailer\PHPMailer\PHPMailer;

use PHPMailer\PHPMailer\Exception;

//...

$institutionMailing = new Emailer(['mariano.Makedonsky@gmail.com'], 'Invitación');

$institutionTemplate = new EmailTemplate('../../../framework/mailer/templates/custom.php');

$institutionTemplate->title = 'Nueva suscripción a clase virtual: ';

$institutionString = 'ok...testing';

$institutionTemplate->bodyContent = $institutionString;

$institutionMailing->SetTemplate($institutionTemplate);

$mailWasSent = $institutionMailing->send();

//echo $mailWasSent;

/*try{

      $institutionMailing = $emailer->send();

}catch (Exception $e){

      $status['mailingErrror'] = $e; 

}*/

echo json_encode($status);

return;

?>


