<?php

/**
* @summary change password...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

//LEFT JOIN `museum_virtual_classes` AS `fix` ON (`fix`.`id` = `mvciir`.`id_class`) 

setlocale(LC_TIME, 'es_ES', 'esp_esp');

require('../../core/init.php');

require('../../../libraries/datatables/ssp.customized.class.php');

$user = new User();

if(!$user->isLoggedIn()) exit();


$table = 'museum_virtual_classes_events';

//$userId = $_GET['userId'];

$primaryKey = 'id';

$columns = array(

	array( 'db' => '`mvce`.`id`', 'dt' => 'id_event', 'field'=>'id_event', 'as' => 'id_event'),

	array( 'db' => '`mvce`.`sid`', 'dt' => 'id_class', 'field'=>'id_class', 'as'=>'id_class'),

	array( 'db' => '`mvce`.`active`', 'dt' => 'active', 'field'=>'active', 'as'=>'active'),

	array( 'db' => '`mvce`.`zoom_url`', 'dt' => 'zoom_url', 'field'=>'zoom_url', 'as'=>'zoom_url'),

	array( 'db' => '`mvce`.`zoom_account`', 'dt' => 'zoom_account', 'field'=>'zoom_account', 'as'=>'zoom_account'),

	array( 'db' => '`mvce`.`zoom_id`', 'dt' => 'zoom_id', 'field'=>'zoom_id', 'as'=>'zoom_id'),




	array( 'db' => '`mvce`.`zoom_password_encrypt`', 'dt' => 'zoom_password_encrypt', 'field'=>'zoom_password_encrypt', 'as'=>'zoom_password_encrypt', 

		'formatter' => function($d, $row) { 

			return openssl_decrypt($d,"AES-128-ECB", 'Muse@2021!');

			}),

	array( 'db' => '`mvce`.`zoom_password_reference`', 'dt' => 'zoom_password_reference', 'field'=>'zoom_password_reference', 'as'=>'zoom_password_reference'),

	array( 'db' => '`mvce`.`zoom_password_length`', 'dt' => 'zoom_password_length', 'field'=>'zoom_password_length', 'as'=>'zoom_password_length'),

	array( 'db' => '`mbe`.`id`', 'dt' => 'id_institution', 'field'=>'id_institution', 'as'=>'id_institution'),

	array( 'db' => '`mvce`.`notes`', 'dt' => 'notes', 'field'=>'notes', 'as'=>'notes'),

	array( 'db' => '`mbe`.`institution_name`', 'dt' => 'institution_name', 'field'=>'institution_name', 'as'=>'institution_name'),

	array( 'db' => '`mbe`.`institution_students`', 'dt' => 'institution_students', 'field'=>'institution_students', 'as'=>'institution_students'),

	array( 'db' => '`mbe`.`institution_address`', 'dt' => 'institution_address', 'field'=>'institution_address', 'as'=>'institution_address'),

	array( 'db' => '`mbe`.`institution_state`', 'dt' => 'institution_state', 'field'=>'institution_state', 'as'=>'institution_state'),

	array( 'db' => '`mbe`.`eduction_level`', 'dt' => 'education_level', 'field'=>'education_level', 'as'=>'education_level'),

	array( 'db' => '`mbe`.`institution_type`', 'dt' => 'institution_type', 'field'=>'institution_type', 'as'=>'institution_type'),
	
	array( 'db' => '`mbe`.`inscription_contact_name`', 'dt' => 'inscription_contact_name', 'field'=>'inscription_contact_name', 'as'=>'inscription_contact_name'),

	array( 'db' => '`mbe`.`inscription_contact_email`', 'dt' => 'inscription_contact_email', 'field'=>'inscription_contact_email', 'as'=>'inscription_contact_email'),

	array( 'db' => '`mbe`.`inscription_contact_phone`', 'dt' => 'inscription_contact_phone', 'field'=>'inscription_contact_phone', 'as'=>'inscription_contact_phone'),

	//....
	
	/*array( 'db' => '`mvc`.`name`', 'dt' => 'class_name', 'field'=>'class_name', 'as'=>'class_name'),

	array( 'db' => '`mvc`.`url`', 'dt' => 'class_url', 'field'=>'class_url', 'as'=>'class_url'),

	array( 'db' => '`mvc`.`description`', 'dt' => 'class_description', 'field'=>'class_description', 'as'=>'class_description'),

	array( 'db' => '`mvc`.`duration`', 'dt' => 'class_duration', 'field'=>'class_duration', 'as'=>'class_duration'),*/



	array( 'db' => '`fix`.`name`', 'dt' => 'class_name', 'field'=>'class_name', 'as'=>'class_name'

		,'formatter' => function($d, $row) {  

			if($d == ""){

				$st = 'libre!';

			}else{

				$st = $d;
			}

			return $st; 


			

		}),

	//array( 'db' => '`fix`.`url`', 'dt' => 'class_url', 'field'=>'class_url', 'as'=>'class_url','formatter' => function($d, $row) {  return ""; }),

	//array( 'db' => '`fix`.`description`', 'dt' => 'class_description', 'field'=>'class_description', 'as'=>'class_description','formatter' => function($d, $row) {  return ""; }),

	//array( 'db' => '`fix`.`duration`', 'dt' => 'class_duration', 'field'=>'class_duration', 'as'=>'class_duration','formatter' => function($d, $row) {  return ""; }),

	
	//....

	array( 'db' => '`mvce`.`date_start`', 'dt' => 'date_start', 'field'=>'date_start', 'as' =>'date_start'),

	array( 'db' => '`mvce`.`date_end`', 'dt' => 'date_end', 'field'=>'date_end', 'as' =>'date_end'),

	//...

	array( 'db' => '`guides`.`id_guide`', 'dt' => 'id_guide', 'field'=>'id_guide', 'as' =>'id_guide'),

	array( 'db' => '`guides`.`id`', 'dt' => 'id_guide_relation', 'field'=>'id_guide_relation', 'as' =>'id_guide_relation'),
	
	array( 'db' => '`users`.`name`', 'dt' => 'guide_name', 'field'=>'guide_name', 'as' =>'guide_name'),
	
	array( 'db' => '`users`.`surname`', 'dt' => 'guide_surname', 'field'=>'guide_surname', 'as' =>'guide_surname'),

	//...

	array( 'db' => '`hosts`.`id_host`', 'dt' => 'id_host', 'field'=>'id_host', 'as' =>'id_host'),

	array( 'db' => '`hosts`.`id`', 'dt' => 'id_host_relation', 'field'=>'id_host_relation', 'as' =>'id_host_relation'),

	array( 'db' => '`users_hosts`.`name`', 'dt' => 'host_name', 'field'=>'host_name', 'as' =>'host_name'),

	array( 'db' => '`users_hosts`.`surname`', 'dt' => 'host_surname', 'field'=>'host_surname', 'as' =>'host_surname'),

	//...

	/*array( 'db' => '`mvce`.`date_start`', 'dt' => 'readable_date_start', 'field'=>'readable_date_start', 'as' =>'readable_date_start', 'formatter' => function($d, $row) { return utf8_encode(strftime("%d de %B de %Y", strtotime($d)));}),*/

	array( 'db' => '`mvce`.`date_start`', 'dt' => 'readable_date_start', 'field'=>'readable_date_start', 'as' =>'readable_date_start', 'formatter' => function($d, $row) { return utf8_encode(strftime("%d/%m/%Y", strtotime($d)));}),

	array( 'db' => '`mvce`.`date_end`', 'dt' => 'readable_date_end', 'field'=>'readable_date_end', 'as' =>'readable_date_end', 'formatter' => function($d, $row) { return utf8_encode(strftime("%d de %B de %Y", strtotime($d)));}),

	array( 'db' => '`mvce`.`date_start`', 'dt' => 'readable_hour', 'field'=>'readable_hour', 'as' =>'readable_hour', 'formatter' => function($d, $row) { return utf8_encode(strftime("%H:%Mhs", strtotime($d)));}),

	//....


	array( 'db' => '`mvciir`.`id_class`', 'dt' => 'id_class_testering', 'field'=>'id_class_testering', 'as'=>'id_class_testering'),

	array( 'db' => '`fix`.`name`', 'dt' => 'fucking_fix', 'field'=>'fucking_fix', 'as'=>'fucking_fix')

);

$whitelist = array('127.0.0.1', '::1');

$sql_details = array(

	'host'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '127.0.0.1' : '162.214.65.131',

	'user'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? 'root' : 'brpxmzm5_make',

	'pass'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '' : 'n},iT0aJTqnF',

	'db'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? 'museum_smart_admin' : 'brpxmzm5_museum_smart_admin'

);

if($_POST["userType"] == 4){

	$joinQuery = "FROM `museum_virtual_classes_events` AS `mvce` 

	INNER JOIN `museum_virtual_classes` AS `mvc` ON (`mvce`.`sid` = `mvc`.`id`) 

	INNER JOIN `museum_virtual_classes_institutions_inscriptions_relations` AS `mvciir` ON (`mvce`.`id` = `mvciir`.`id_event`) 








	INNER JOIN `museum_virtual_classes` AS `fix` ON (`fix`.`id` = `mvciir`.`id_class`)









	INNER JOIN `museum_booking_institutions` AS `mbe` ON (`mvciir`.`id_institution` = `mbe`.`id`)

	LEFT JOIN `museum_virtual_classes_events_guides_relations` as `guides` ON  (`mvce`.`id` = `guides`.`id_event`)

	LEFT JOIN `users` as `users` ON (`users`.`id` = `guides`.`id_guide`)

	LEFT JOIN `museum_virtual_classes_events_hosts_relations` as `hosts` ON  (`mvce`.`id` = `hosts`.`id_event`)

	LEFT JOIN `users` as `users_hosts` ON (`users_hosts`.`id` = `hosts`.`id_host`)

	";



	$extraWhere = "DATE(`mvce`.`date_start`) =  '" . $_POST["date"] . "' AND `mvce`.`active` = 1 AND `mvce`.`deleted` = 0 AND `users`.`id` = ". $_POST['userId'] ;


}else if($_POST["userType"] == 6){

	$joinQuery = "FROM `museum_virtual_classes_events` AS `mvce` 

	INNER JOIN `museum_virtual_classes` AS `mvc` ON (`mvce`.`sid` = `mvc`.`id`) 

	INNER JOIN `museum_virtual_classes_institutions_inscriptions_relations` AS `mvciir` ON (`mvce`.`id` = `mvciir`.`id_event`) 

	





	INNER JOIN `museum_virtual_classes` AS `fix` ON (`fix`.`id` = `mvciir`.`id_class`)








	INNER JOIN `museum_booking_institutions` AS `mbe` ON (`mvciir`.`id_institution` = `mbe`.`id`)

	LEFT JOIN `museum_virtual_classes_events_guides_relations` as `guides` ON  (`mvce`.`id` = `guides`.`id_event`)

	LEFT JOIN `users` as `users` ON (`users`.`id` = `guides`.`id_guide`)

	INNER JOIN `museum_virtual_classes_events_hosts_relations` as `hosts` ON  (`mvce`.`id` = `hosts`.`id_event`)

	INNER JOIN `users` as `users_hosts` ON (`users_hosts`.`id` = `hosts`.`id_host`)

	";

	$extraWhere = "DATE(`mvce`.`date_start`) =  '" . $_POST["date"] . "' AND `mvce`.`active` = 1 AND `mvce`.`deleted` = 0 AND `users_hosts`.`id` = ". $_POST['userId'];

}else{

	$joinQuery = "FROM `museum_virtual_classes_events` AS `mvce` 

	INNER JOIN `museum_virtual_classes` AS `mvc` ON (`mvce`.`sid` = `mvc`.`id`) 

	LEFT JOIN `museum_virtual_classes_institutions_inscriptions_relations` AS `mvciir` ON (`mvce`.`id` = `mvciir`.`id_event`) 

	LEFT JOIN `museum_virtual_classes` AS `fix` ON (`fix`.`id` = `mvciir`.`id_class`) 

	LEFT JOIN `museum_booking_institutions` AS `mbe` ON (`mvciir`.`id_institution` = `mbe`.`id`)

	LEFT JOIN `museum_virtual_classes_events_guides_relations` as `guides` ON  (`mvce`.`id` = `guides`.`id_event`)

	LEFT JOIN `users` as `users` ON (`users`.`id` = `guides`.`id_guide`)

	LEFT JOIN `museum_virtual_classes_events_hosts_relations` as `hosts` ON  (`mvce`.`id` = `hosts`.`id_event`)

	LEFT JOIN `users` as `users_hosts` ON (`users_hosts`.`id` = `hosts`.`id_host`)


	";

	if($_POST["dateTo"] == null){

		//$extraWhere = "DATE(`mvce`.`date_start`) =  '" . $_POST["date"] . "' AND `mvce`.`deleted` = 0 AND `mvce`.`booking_type` = 1 GROUP BY `mvce`.`id`" ;
		
		$extraWhere = "DATE(`mvce`.`date_start`) =  '" . $_POST["date"] . "' AND `mvce`.`deleted` = 0 AND `mvce`.`booking_type` = 1" ;

	}else{

		$extraWhere = "DATE(`mvce`.`date_start`) >=  '" . $_POST["date"] . "' AND DATE(`mvce`.`date_end`) <=  '" . $_POST["dateTo"] . "' AND `mvce`.`deleted` = 0 AND `mvce`.`booking_type` = 1" ;

	}


}

//$groupBy = '';

//$having = '';


header('Content-type: text/javascript');

echo json_encode( SSP::simple($_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having), JSON_PRETTY_PRINT );

?>





				