<?php

/**
* @summary Manage all the data, access, login, logout about the user...
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @see {@link http://www.aditivointeractivegroup.com}
*
*/

require_once '../../core/init.php';

require_once('../../../framework/mailer/Emailer.php');

require_once('../../../framework/mailer/EmailTemplate.php');


$status['errors'] = false;

$status['init'] = true;

$db = DB::getInstance();

if(Input::exists()){

	$validate = new Validate();

	$validation = $validate->check($_POST, array('eventId' => array('display'=>'eventId', 'required' => true) ));

      if($validation->passed()){

            DB::getInstance()->query( 'UPDATE museum_virtual_classes_events SET booked_places = booked_places - ? WHERE id = ?', [1, Input::get('eventId')] );

      }

}

echo json_encode($status);

return;

?>


