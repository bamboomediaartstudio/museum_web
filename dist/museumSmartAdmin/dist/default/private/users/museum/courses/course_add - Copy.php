<?php

/**
 * @summary Add new course to the DB
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0	
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../core/init.php';

require '../../../libraries/claviska/SimpleImage.php';

/**all the variables */

$filesManager = new FilesManager();

$ds = DIRECTORY_SEPARATOR;

$image = new \claviska\SimpleImage();

$defaultPrefix = '_original';

$allowProgramDownload = 0;

$status['init'] = true;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_course_add'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');

/**if there is no token, print and out */

//if(!Token::check(Input::get('token'))) printData(0, 'token problem');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'name' => array('display'=> 'name', 'required' => true),

	'description' => array('display'=> 'description', 'required' => true),

	'email' => array('display'=> 'email', 'email' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	if($user->isLoggedIn()){

		$db->insert('museum_courses',[

			'name'=> Input::get('name'),

			'url'=> Input::get('url'),

			'description'=> Input::get('description'),

			'is_virtual'=> (Input::get('is_virtual') == 'on') ? 1 : 0,
			
			'in_location'=> (Input::get('in_location') == 'on') ? 1 : 0,

			'show_price'=> (Input::get('show_price') == 'on') ? 1 : 0,

			'price'=> Input::get('price'),

			'number_of_classes'=> Input::get('number_of_classes'),

			'show_duration'=> (Input::get('show_duration') == 'on') ? 1 : 0,

			'duration'=> Input::get('duration'),
			
			'start_date' => (Input::get('start_date') == null) ? NULL : Input::get('start_date'),

			'end_date' => (Input::get('end_date') == null) ? NULL : Input::get('end_date'),

			//'end_date'=> $addDateEnd,
			
			'address'=> (!empty(Input::get('address'))) ? Input::get('address') : null,

			'email'=> (!empty(Input::get('email'))) ? Input::get('email') : null,

			'phone'=> (!empty(Input::get('phone'))) ? Input::get('phone') : null,

			'whatsapp'=> (!empty(Input::get('whatsapp'))) ? Input::get('whatsapp') : null,

			'facebook_group'=> (!empty(Input::get('facebook_group'))) ? Input::get('facebook_group') : null,
			
			'observations'=> (!empty(Input::get('observations'))) ? Input::get('observations') : null,

			'added' => date('Y-m-d H:i:s'),

			'allow_enrollment'=> (Input::get('allow_enrollment') == 'on') ? 1 : 0,

			'allow_share'=> (Input::get('allow_share') == 'on') ? 1 : 0

		]);

		$lastId = $db->lastId();

		//save PDF program...

		if (isset($_FILES['course-program'])) {

			$pdfFolder = '../../../sources/pdf/courses/' . $lastId . '/pdf';

			$pdfFile = $pdfFolder .  '/' . Input::get('url') . '.pdf';

			$pdfFolder = $filesManager->makeDirectory($pdfFolder);

			$finfo = finfo_open(FILEINFO_MIME_TYPE);

			$mime = finfo_file($finfo, $_FILES['course-program']['tmp_name']);

			if ($mime == 'application/pdf') {

				if(move_uploaded_file($_FILES['course-program']['tmp_name'], $pdfFile)) {

					$allowProgramDownload = 1;

					$db->update('museum_courses',$lastId, array('allow_program_download'=>$allowProgramDownload));
					
				}

			}

		}else{

			$allowProgramDownload = 0;
		}

		//selected tutors!

		if(!empty(Input::get('selectedTutors'))){

			$jsonObject = json_decode(Input::get('selectedTutors'));

			foreach ($jsonObject as $key => $value) {

				$db->insert('museum_courses_tutors_relations', 

					['id_course'=>$lastId, 'id_tutor'=>$value->id]);
				
			}

		}

		//add new tutors and relations...

		if(!empty(Input::get('addedTutors'))){

			$jsonObject = json_decode(Input::get('addedTutors'));

			foreach ($jsonObject as $key => $value) {

				$db->insert('museum_courses_tutors', [

					'name'=>$value->tutorName,

					'surname'=>$value->tutorSurname

				]);

				$newTutorId = $db->lastId();

				$db->insert('museum_courses_tutors_relations', 

					['id_course'=>$lastId, 'id_tutor'=>$newTutorId]);
				
			}

		}

		//save relations between course and modalities...

		foreach(Input::get('checkboxes') as $selected){

			$insert = $db->insert('museum_courses_modalities_relations', 
				[

					'id_course'=> $lastId,

					'id_modality' => $selected

				]

			);

		}

		//copy template...

		$permalinkFolder = '../../../../../../../cursos/' . Input::get('url');

		$mk = $filesManager->makeDirectory($permalinkFolder);

		$dst = $permalinkFolder;

		$src = '../../../templates/course_template';

		copyFiles($src, $dst);

		//...

		if($_FILES['file']['name'] == "blob" && $_FILES['file']['type'] == "application/octet-stream" && $_FILES['file']['size'] == 0) {

			addLogData($user);

			printData(1, 'ok without image...por que?');

		}else{

			$uniqueId = uniqid();

			$fileSize = 20971520;

			$courseFolder = '../../../sources/images/courses/' . $lastId;

			$deleteFolder = '../../../sources/images/courses/' . $lastId . '/delete';

			$fileFolder = $courseFolder . '/' . $uniqueId;

			$courseFolderExists = $filesManager->checkDirectory($courseFolder);

			if(!$courseFolderExists){

				$courseFolderCreated = $filesManager->makeDirectory($courseFolder);

				$deleteFolder = $filesManager->makeDirectory($deleteFolder);
			}

			$folderExists = $filesManager->checkDirectory($fileFolder);

			if(!$folderExists){

				$folderCreated = $filesManager->makeDirectory($fileFolder);

			}else{

				$folderCreated = true;
			}

			if($folderCreated){

				$checkPHPExtensions = $filesManager->checkPHPExtensions();
			}

			if($checkPHPExtensions){

				if (!empty($_FILES)) {

					$tempFile = $_FILES['file']['tmp_name'];

					if(is_file($tempFile)){

						$checkFileSize = $filesManager->checkFileSize($tempFile, $fileSize);

						if($checkFileSize){

							$targetPath = dirname( __FILE__ ) . $ds . $fileFolder . $ds;

							$name = $lastId . '_' . $uniqueId;

							$checkMimeType = $filesManager->checkImageMimeType($_FILES['file']['tmp_name']);

							if($checkMimeType){

								//-----------------------------------------------------
								//general image settings...

								$newMime = 'image/jpeg';

								$newExtension = 'jpeg';

								//-----------------------------------------------------
								//the default image will be the retina image...

								$retinaFile =  $targetPath . $name . '_original@2x.' . $newExtension;

								move_uploaded_file($tempFile, $retinaFile);

								//-----------------------------------------------------
								//the normal image is the half of the retina....

								$normalFile =  $targetPath . $name . '_original.' . $newExtension;

								$image->fromFile($retinaFile)->resize(null, 500)->toFile($normalFile, $newMime, 70);

								//-----------------------------------------------------
								//then we have a retina thumb.

								$retinaFileThumb =  $targetPath . $name . '_thumb@2x.' . $newExtension;

								$image->fromFile($normalFile)->resize(null, 250)

								->toFile($retinaFileThumb, $newMime, 70);

								//-----------------------------------------------------
								//and then we have a normal thumb.

								$normalThumb =  $targetPath . $name . '_thumb.' . $newExtension;

								$image->fromFile($normalFile)->resize(null, 125)

								->toFile($normalThumb, $newMime, 70);

								$q = DB::getInstance()->query('SELECT unique_id, id FROM museum_images WHERE sid = ? AND source = ?', array($lastId, 'courses'));

								//-----------------------------------------------------
								//then we have a retina square.

								$retinaSq =  $targetPath . $name . '_sq@2x.' . $newExtension;

								$image->fromFile($normalFile)->thumbnail(500, 500, 250)

								->toFile($retinaSq, $newMime, 70);

								//-----------------------------------------------------
								//and a normal sq.

								$normalSq =  $targetPath . $name . '_sq.' . $newExtension;

								$image->fromFile($normalFile)->thumbnail(250, 250, 125)

								->toFile($normalSq, $newMime, 70);


								$c = $q->count();

								if($c < 1){

									DB::getInstance()->insert('museum_images', 

										array('sid'=>$lastId, 'unique_id'=>$uniqueId, 'mimetype'=>$newMime, 'source'=>'courses', 'path'=>'courses'));

									$lastPictureId = DB::getInstance()->lastId();

									DB::getInstance()->insert('museum_image_crop_box',

										array('id'=>$lastPictureId));

									DB::getInstance()->insert('museum_image_data',

										array('id'=>$lastPictureId));

									DB::getInstance()->insert('museum_image_filters',

										array('id'=>$lastPictureId));

									DB::getInstance()->insert('museum_image_canvas_data',

										array('id'=>$lastPictureId));

									addLogData($user);

								}else{

								}

								printData(1, '1');

							}else{

								printData(4, '4');

							}

						}else{

							printData(3, '3');

						}

					}else{

						printData(2, '2');

					}

				}

			}

		}

	}else{ }

}



function copyFiles($src, $dst) {

	$dir = @opendir($src);

	if (!file_exists($dst)) @mkdir($dst);

	while (false !== ($file = readdir($dir))) {

		if (( $file != '.' ) && ( $file != '..' )) {

			if ( is_dir($src . '/' . $file) ) copyFiles($src . '/' . $file, $dst . '/' . $file); 
			
			else copy($src . '/' . $file, $dst . '/' . $file);
		
		}

	}
	
	closedir($dir); 
}


/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The course ' . Input::get('name') .  ' was added.';

	$userString = 'Agregaste el curso <b>' . Input::get('name') . ' ' . Input::get('surname') . '</b> al sistema.';

	Logger::addLogData($user->data()->id, 

		'user',

		$systemString,

		$userString,

		'course',

		'course');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>