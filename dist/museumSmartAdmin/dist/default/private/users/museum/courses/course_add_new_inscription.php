<?php

require_once '../../core/init.php';

$status['init'] = true;

$db = DB::getInstance();

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'email' => array('display'=>'email', 'email' => true),
	
	'inscription-name' => array('display'=>'inscription-name', 'required' => true),
	
	'inscription-surname' => array('display'=>'inscription-surname', 'required' => true),

	'course-id' => array('display'=>'course-id', 'required' => true),

	'course-name' => array('display'=>'course-name', 'required' => true),

	'course-url' => array('display'=>'course-url', 'required' => true)

));

if($validation->passed()){
	
	$status['inscription-name'] = Input::get('inscription-name');
	
	$status['inscription-surname'] = Input::get('inscription-surname');

	$status['email'] = Input::get('email');

	$fields = array(

		'name'=>Input::get('inscription-name'),

		'course_id' => Input::get('course-id'),

		'surname'=>Input::get('inscription-surname'),

		'email'=>Input::get('email'),

		'phone'=>Input::get('phone'),

		'city'=>Input::get('city'),

		'address'=>Input::get('address'),

		'medium'=>Input::get('mediumRadioOptions'),

		'first_time' =>Input::get('firstTimeRadioOptions'),

		'payment_method'=>Input::get('paymentRadioOptions'),

		'deleted'=>0

	);

	$db->insert('museum_courses_inscriptions', $fields);

	//make template replacements...

	//...

	//general settings...

	$settingsQ = $db->query("SELECT * FROM settings");

	$settings = $GLOBALS['settings'] = $settingsQ->first();

	//support settings...

	$supportSettingsQ = $db->query("SELECT * FROM support_settings");

	$GLOBALS['supportSettings'] = $supportSettingsQ->first();

	//museum settings...

	$museumSettingsQ = $db->query("SELECT * FROM museum_data");

	$museumSettings = $museumSettingsQ->first();

	//museum social media...

	$museumSocialMediaQ = $db->query("SELECT * FROM museum_social_media");

	$museumSocialMediaSettings = $museumSocialMediaQ->first();

	//template for subscriber...

	$address = $museumSettings->address . ' ' . $museumSettings->address_number . ', ' . $museumSettings->zip_code . ' ' . $museumSettings->city;

	$finalURL = 'https://www.museodelholocausto.org.ar/cursos/' . Input::get('course-url');

	$fullName = Input::get('inscription-name') . ' ' . Input::get('inscription-surname');

	$message = file_get_contents('../../../email_templates/course_subscriber.html');

	$message = str_replace('%projectTitle%', $museumSettings->title, $message);

	$message = str_replace('%courseName%', Input::get('course-name'), $message);
	
	$message = str_replace('%subscriber%', $fullName, $message);

	$message = str_replace('%courseURL%', $finalURL, $message);

	$message = str_replace('%ip%', Helpers::getIP(), $message);

	$message = str_replace('%os%', Helpers::getOS(), $message);

	$message = str_replace('%browser%', Helpers::getBrowser(), $message);

	$message = str_replace('%email%', $GLOBALS['supportSettings']->support_email, $message);

	$message = str_replace('%address%', $address, $message);

	$message = str_replace('%te%', $museumSettings->te, $message);

	$message = str_replace('%museumEmail%', $museumSettings->email, $message);

	$message = str_replace('%lat%', $museumSettings->lat, $message);

	$message = str_replace('%lon%', $museumSettings->lon, $message);

	$message = str_replace('%facebook%', $museumSocialMediaSettings->facebook, $message);

	$message = str_replace('%instagram%', $museumSocialMediaSettings->instagram, $message);

	$message = str_replace('%twitter%', $museumSocialMediaSettings->twitter, $message);

	//$sent = Helpers::email(Input::get('email'), utf8_decode('Inscripción a curso'), $message);
	
	$sent = Helpers::email(Input::get('email'), utf8_decode('Inscripción a curso'), $message);

	if($sent){

		$status['envio'] = $sent;

	}else{

		$status['envio'] = $sent;

	}

	//template for museum...

	$museumMessage = file_get_contents('../../../email_templates/course_subscription.html');

	$museumMessage = str_replace('%courseName%', Input::get('course-name'), $museumMessage);
	
	$museumMessage = str_replace('%subscriber%', $fullName, $museumMessage);

	$museumMessage = str_replace('%email%', Input::get('email'), $museumMessage);

	$museumMessage = str_replace('%phone%', Input::get('phone'), $museumMessage);

	$museumMessage = str_replace('%city%', Input::get('city'), $museumMessage);

	$museumMessage = str_replace('%address%', Input::get('address'), $museumMessage);

	$museumMessage = str_replace('%medium%', Input::get('mediumRadioOptions'), $museumMessage);

	$museumMessage = str_replace('%firstTime%', Input::get('firstTimeRadioOptions'), $museumMessage);
	
	$museumMessage = str_replace('%paymentMethod%', Input::get('paymentRadioOptions'), $museumMessage);

	$museumMessage = str_replace('%ip%', Helpers::getIP(), $museumMessage);

	$museumMessage = str_replace('%os%', Helpers::getOS(), $museumMessage);

	$museumMessage = str_replace('%browser%', Helpers::getBrowser(), $museumMessage);

	$sentMuseumMsg = Helpers::email('info@museodelholocausto.org.ar', utf8_decode('Inscripción a curso'), $museumMessage);

	if($sentMuseumMsg){

		$status['envio2'] = 'exitoso';

	}else{

		$status['envio2'] = 'no exitoso';

	}

}

echo json_encode($status);


?>