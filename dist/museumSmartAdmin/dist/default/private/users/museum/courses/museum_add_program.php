<?php

?>

<?php
?>

<?php

/**
 * @summary Add new PDF program to course.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the includes */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

require_once '../../core/init.php';

$filesManager = new FilesManager();

$ds = DIRECTORY_SEPARATOR;


/**all the variables */

$status['init'] = true;

$db = DB::getInstance();

if(!Input::exists()) exit();

$id = Input::get('id');

$courseName = Input::get('courseName');

$courseURL = Input::get('courseURL');

//echo json_encode($status);

//exit();

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'idi', 'required' => true),

	'courseName' => array('display'=> 'courseName', 'required' => true),

	'courseURL' => array('display'=> 'courseURL', 'required' => true))
);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()){


}else{

	$user = new User();

	if($user->isLoggedIn()){

		$status['login'] = 'true';

		if (isset($_FILES['course-program'])) {

			$status['fileExists'] = 'true';

			$pdfFolder = '../../../sources/pdf/courses/' . $id . '/pdf';

			$pdfFile = $pdfFolder .  '/' . Input::get('courseURL') . '.pdf';

			$check = $filesManager->checkDirectory($pdfFolder);

			if(!$check) $filesManager->makeDirectory($pdfFolder);

			//$pdfFolder = $filesManager->makeDirectory($pdfFolder);

			$finfo = finfo_open(FILEINFO_MIME_TYPE);

			$mime = finfo_file($finfo, $_FILES['course-program']['tmp_name']);

			if ($mime == 'application/pdf') {

				if(move_uploaded_file($_FILES['course-program']['tmp_name'], $pdfFile)) {

					$allowProgramDownload = 1;

					$db->update('museum_courses',$id, array('allow_program_download'=>$allowProgramDownload));
					
				}

			}

		}else{

			$allowProgramDownload = 0;
		}

		$status['validation'] = 'passed';

		echo json_encode($status);

		exit();
	}

}

?>