<?php

?>

<?php
?>

<?php

/**
 * @summary Add new tutor to the system.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the includes */

require_once '../../core/init.php';

/**all the variables */

$status['init'] = true;

$db = DB::getInstance();

if(!Input::exists()) exit();

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'tutor_name' => array('display'=> 'tutor_name', 'required' => true),

	'tutor_surname' => array('display'=> 'tutor_surname', 'required' => true),

	'course_id' => array('display'=> 'course_id', 'required' => true))
);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	if($user->isLoggedIn()){

		$db->insert('museum_courses_tutors',[

			'name'=> Input::get('tutor_name'),

			'surname'=> Input::get('tutor_surname')

		]);

		$lastId = $db->lastId();

		//once we add the tutor, then we create the relation...

		$db->insert('museum_courses_tutors_relations', [

			'id_tutor' => $lastId,

			'id_course' => Input::get('course_id')
		
		]);

		$relationId = $db->lastId();

		$status['newTutorId']  = $lastId;

		$status['newRelationId']  = $relationId;

		$status['newTutorName']  = Input::get('tutor_name');

		$status['newTutorSurname']  = Input::get('tutor_surname');

		echo json_encode($status);

		exit();
	}

}

?>