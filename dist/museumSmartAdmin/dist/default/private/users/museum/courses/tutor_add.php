<?php

/**
 * @summary Add new tutor to the DB
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0	
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../core/init.php';

require '../../../libraries/claviska/SimpleImage.php';

/**all the variables */

$filesManager = new FilesManager();

$ds = DIRECTORY_SEPARATOR;

$image = new \claviska\SimpleImage();

$allowProgramDownload = 0;

$status['init'] = true;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_tutor_add'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');

/**if there is no token, print and out */

//if(!Token::check(Input::get('token'))) printData(0, 'token problem');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'name' => array('display'=> 'name', 'required' => true),

	'surname' => array('display'=> 'surname', 'required' => true),

	'email' => array('display'=> 'email', 'email' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	if($user->isLoggedIn()){

		$db->insert('museum_courses_tutors',[

			'name'=> Input::get('name'),

			'surname'=> Input::get('surname'),

			'bio'=> strip_tags(Input::get('bio'), '<br><b><strong><i>'),

			'email'=> (!empty(Input::get('email'))) ? Input::get('email') : null,

			'phone'=> (!empty(Input::get('phone'))) ? Input::get('phone') : null,

			'whatsapp'=> (!empty(Input::get('whatsapp'))) ? Input::get('whatsapp') : null

		]);

		$lastId = $db->lastId();


		if(!file_exists($_FILES['main-image']['tmp_name']) || !is_uploaded_file($_FILES['main-image']['tmp_name'])){

			addLogData($user);

			printData(1, 'ok without image');

		}else{

			$uniqueId = uniqid();

			$fileSize = 20971520;

			$tutorFolder = '../../../sources/images/tutors/' . $lastId;

			$deleteFolder = '../../../sources/images/tutors/' . $lastId . '/delete';

			$fileFolder = $tutorFolder . '/' . $uniqueId;

			$itemFolderExists = $filesManager->checkDirectory($tutorFolder);

			if(!$itemFolderExists){

				$tutorFolderWasCreated = $filesManager->makeDirectory($tutorFolder);

				$deleteFolder = $filesManager->makeDirectory($deleteFolder);
			}

			$folderExists = $filesManager->checkDirectory($fileFolder);

			if(!$folderExists){

				$folderCreated = $filesManager->makeDirectory($fileFolder);

			}else{

				$folderCreated = true;
			}

			if($folderCreated){

				$checkPHPExtensions = $filesManager->checkPHPExtensions();
			}

			if($checkPHPExtensions){

				if (!empty($_FILES)) {

					$tempFile = $_FILES['main-image']['tmp_name'];

					if(is_file($tempFile)){

						$checkFileSize = $filesManager->checkFileSize($tempFile, $fileSize);

						if($checkFileSize){

							$targetPath = dirname( __FILE__ ) . $ds . $fileFolder . $ds;

							$name = $lastId . '_' . $uniqueId;

							$checkMimeType = $filesManager->checkImageMimeType($_FILES['main-image']['tmp_name']);

							if($checkMimeType){

								//-----------------------------------------------------
								//general image settings...

								$newMime = 'image/jpeg';

								$newExtension = 'jpeg';

								//-----------------------------------------------------
								//the default image will be the retina image...

								$retinaFile =  $targetPath . $name . '_original@2x.' . $newExtension;

								move_uploaded_file($tempFile, $retinaFile);

								//-----------------------------------------------------
								//the normal image is the half of the retina....

								$normalFile =  $targetPath . $name . '_original.' . $newExtension;

								$image->fromFile($retinaFile)->resize(null, 250)->toFile($normalFile, $newMime, 100);

								//-----------------------------------------------------
								//then we have a retina thumb.

								$retinaFileThumb =  $targetPath . $name . '_thumb@2x.' . $newExtension;

								$image->fromFile($normalFile)->resize(null, 250)

								->toFile($retinaFileThumb, $newMime, 100);

								//-----------------------------------------------------
								//and then we have a normal thumb.

								$normalThumb =  $targetPath . $name . '_thumb.' . $newExtension;

								$image->fromFile($normalFile)->resize(null, 125)

								->toFile($normalThumb, $newMime, 100);

								//-----------------------------------------------------
								//then we have a retina square.

								$retinaSq =  $targetPath . $name . '_sq@2x.' . $newExtension;

								$image->fromFile($normalFile)->thumbnail(500, 500, 250)

								->toFile($retinaSq, $newMime, 100);

								//-----------------------------------------------------
								//and a normal sq.

								$normalSq =  $targetPath . $name . '_sq.' . $newExtension;

								$image->fromFile($normalFile)->thumbnail(250, 250, 125)

								->toFile($normalSq, $newMime, 100);

								$q = DB::getInstance()->query('SELECT unique_id, id FROM museum_images WHERE sid = ? AND source = ?', array($lastId, 'tutors'));

								$c = $q->count();

								if($c < 1){

									DB::getInstance()->insert('museum_images', 

										array('sid'=>$lastId, 'unique_id'=>$uniqueId, 'mimetype'=>$newMime, 'source'=>'tutors', 'path'=>'tutors'));

									$lastPictureId = DB::getInstance()->lastId();

									DB::getInstance()->insert('museum_image_crop_box',

										array('id'=>$lastPictureId));

									DB::getInstance()->insert('museum_image_data',

										array('id'=>$lastPictureId));

									DB::getInstance()->insert('museum_image_filters',

										array('id'=>$lastPictureId));

									DB::getInstance()->insert('museum_image_canvas_data',

										array('id'=>$lastPictureId));

									addLogData($user);

								}else{

								}

								printData(1, '1');

							}else{

								printData(4, '4');

							}

						}else{

							printData(3, '3');

						}

					}else{

						printData(2, '2');

					}

				}

			}

		}

	}else{ }

}




/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The tutor ' . Input::get('name') .  ' was added.';

	$userString = 'Agregaste un nuevo tutor al sistema.';

	Logger::addLogData($user->data()->id, 

		'user',

		$systemString,

		$userString,

		'tutors',

		'tutors');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>