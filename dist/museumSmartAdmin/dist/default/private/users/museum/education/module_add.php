<?php

/**
 * @summary Add new module to the DB
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0	
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the includes */

require_once '../../core/init.php';

require 'EducationKeywords.class.php';

$filesManager = new FilesManager();

/**all the variables */

$status['init'] = true;

$ds = DIRECTORY_SEPARATOR;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_alert_add'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'error');

/**if there is no token, print and out */

//if(!Token::check(Input::get('token'))) printData(0, 'token problem');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'title' => array('display'=> 'title', 'required' => true)

	/*,'caption' => array('display'=> 'caption', 'required' => true)*/

)

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	if($user->isLoggedIn()){

		$db->insert('museum_education_modules',[

			'title'=> Input::get('title'),

			'caption'=> strip_tags(Input::get('caption'), '<br><b><strong><i>'),

			'button_label'=> Input::get('button_label'),

			'link'=> Input::get('link'),

			'is_blank'=> (Input::get('is_blank') == 'on') ? 1 : 0,

			'is_youtube'=> (Input::get('is_youtube') == 'on') ? 1 : 0,

			'is_instagram'=> (Input::get('is_instagram') == 'on') ? 1 : 0,

			'is_facebook'=> (Input::get('is_facebook') == 'on') ? 1 : 0,

			'is_mdh'=> (Input::get('is_mdh') == 'on') ? 1 : 0,

			'is_zoom'=> (Input::get('is_zoom') == 'on') ? 1 : 0,

			'is_other'=> (Input::get('is_other') == 'on') ? 1 : 0,

			'added' => date('Y-m-d H:i:s')
		]);

		$lastId = $db->lastId();

		$addTags = addTags($lastId, Input::get('keywords'));

		if(!file_exists($_FILES['main-image']['tmp_name']) || !is_uploaded_file($_FILES['main-image']['tmp_name'])){

			addLogData($user);

			printData(1, 'ok without image...');

		}else{

			$uniqueId = uniqid();

			$fileSize = 20971520;

			$exhibitionFolder = '../../../sources/mix/' . $lastId;

			$deleteFolder = '../../../sources/mix/' . $lastId . '/delete';

			$fileFolder = $exhibitionFolder . '/' . $uniqueId;

			$exhibitionFolderExists = $filesManager->checkDirectory($exhibitionFolder);

			if(!$exhibitionFolderExists){

				$exhibitionFolderCreated = $filesManager->makeDirectory($exhibitionFolder);

				$deleteFolder = $filesManager->makeDirectory($deleteFolder);
			}

			$folderExists = $filesManager->checkDirectory($fileFolder);

			if(!$folderExists){

				$folderCreated = $filesManager->makeDirectory($fileFolder);

			}else{

				$folderCreated = true;
			}

			if($folderCreated){

				$checkPHPExtensions = $filesManager->checkPHPExtensions();
			}

			if($checkPHPExtensions){

				if (!empty($_FILES)) {

					$tempFile = $_FILES['main-image']['tmp_name'];

					if(is_file($tempFile)){

						$checkFileSize = $filesManager->checkFileSize($tempFile, $fileSize);

						if($checkFileSize){

							$targetPath = dirname( __FILE__ ) . $ds . $fileFolder . $ds;

							$name = $lastId . '_' . $uniqueId;

							$checkMimeType = $filesManager->checkImageMimeType($_FILES['main-image']['tmp_name']);
							
							$ext = pathinfo($_FILES['main-image']['name'], PATHINFO_EXTENSION);

							//-----------------------------------------------------
							//the default image will be the retina image...

							$uploaded =  $targetPath . $name . '_uploaded.' . $ext;

							move_uploaded_file($tempFile, $uploaded);

							$db->update("museum_education_modules", $lastId, ["uid"=>$uniqueId, "extension"=>$ext]);


							addLogData($user);

							printData(1, 'ok...');


						}

					}

				}

			}
		}

	}

}

/**
* @function addTags
* @description Add keywords to prensa - json decodes, parse and add
*
* @param {json} $keywods 	- 	 stringyfy jquery array.
*/

function addTags($id, $keywords){

  $keywordsDecoded = json_decode($keywords);

  $arrLength = count($keywordsDecoded);

  $db = DB::getInstance();

  $realCantKeywordsToAdd = 0;

  $keywordsAdded = 0;

  $relationsAdded = 0;

  foreach($keywordsDecoded as $keywordResult){

    $objKeywords = new EducationKeywords();

    $keywordsExist = $objKeywords->checkExist($keywordResult);

    if($keywordsExist==0){

      $realCantKeywordsToAdd++;

      try{

          $addKeywordLastId = $objKeywords->addKeyword($keywordResult);

          if($addKeywordLastId!=0){

            $keywordsAdded++;

            $doRelation = $objKeywords->addRelation($id, $addKeywordLastId);

            if($doRelation == 1){

              $relationsAdded++;

            }

          }

      }catch(Exception $e){

        echo "Error al agregar keyword a db: ".$e->getMessage() . " - ".$e->getCode();

      }

    }else{

      $addKeywordLastId = $keywordsExist;

      $doRelation= $objKeywords->addRelation($id, $addKeywordLastId);

      if($doRelation == 1){

        $relationsAdded++;    //relation saved

      }

    }

  }

  if(($realCantKeywordsToAdd == $keywordsAdded) && ($relationsAdded == $arrLength)) { //La cantidad de relaciones guardas tiene que ser igual al array de tags que envia el user.

    //Todo insertado en la db
    return 1;

  }else{

    //Keys no insertadas
    return 0;

  }


}




/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The module ' . Input::get('title') .  ' was added.';

	$userString = 'Agregaste una nueva alerta.';

	Logger::addLogData($user->data()->id, 

		'user',

		$systemString,

		$userString,

		'education_module',

		'education_module');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>