<?php

/**
 * @summary Add new event to the DB
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0	
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../core/init.php';

require '../../../libraries/claviska/SimpleImage.php';

require '../../../libraries/google/vendor/autoload.php';


/**all the variables */

$filesManager = new FilesManager();

$ds = DIRECTORY_SEPARATOR;

$image = new \claviska\SimpleImage();

$allowProgramDownload = 0;

$status['init'] = true;

$db = DB::getInstance();

$retinaOriginalWidth = 2000;

$retinaOriginalHeight = 1000; 


/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_event_add'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');

/**if there is no token, print and out */

//if(!Token::check(Input::get('token'))) printData(0, 'token problem');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'name' => array('display'=> 'name', 'required' => true),

	'description' => array('display'=> 'description', 'required' => true),

	'email' => array('display'=> 'email', 'email' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	if($user->isLoggedIn()){

		$db->insert('museum_events',[

			'name'=> Input::get('name'),

			'url'=> Input::get('url'),

			'description'=> strip_tags(Input::get('description'), '<br><b><strong><i>'),
						
			'start_date' => (Input::get('start_date') == 'null') ? NULL : Input::get('start_date'),

			'end_date' => (Input::get('end_date') == 'null') ? NULL : Input::get('end_date'),
			
			'address'=> (!empty(Input::get('address'))) ? Input::get('address') : null,

			'lat'=> Input::get('lat'),

			'long'=> Input::get('long'),

			'email'=> (!empty(Input::get('email'))) ? Input::get('email') : null,

			'phone'=> (!empty(Input::get('phone'))) ? Input::get('phone') : null,

			'whatsapp'=> (!empty(Input::get('whatsapp'))) ? Input::get('whatsapp') : null,
			
			//'observations'=> (!empty(Input::get('observations'))) ? Input::get('observations') : null,

			'observations'=> strip_tags(Input::get('observations'), '<br><b><strong><i>'),

			'added' => date('Y-m-d H:i:s'),

			'allow_share'=> (Input::get('allow_share') == 'on') ? 1 : 0,

			'allow_save'=> (Input::get('allow_save') == 'on') ? 1 : 0,

			'is_free'=> (Input::get('is_free') == 'on') ? 1 : 0,

			'is_highlighted'=> (Input::get('is_highlighted') == 'on') ? 1 : 0,

			'is_with_inscription'=> (Input::get('is_with_inscription') == 'on') ? 1 : 0,

			'show_address'=> (Input::get('show_address') == 'on') ? 1 : 0

		]);

		$lastId = $db->lastId();

		$originalURL = 'https://www.museodelholocausto.org.ar/eventos/' . Input::get('url');

		$shortURL = json_decode(file_get_contents("http://api.bit.ly/v3/shorten?login=museodelholocausto&apiKey=R_1e0643a976f44107926cfc4026c192e4&longUrl=".urlencode($originalURL)."&format=json"))->data->url;

		$db->insert('museum_short_urls',[

			'sid' => $lastId,

			'source' => 'events',

			'original' => $originalURL,

			'short_url' => $shortURL
		]);

		//save relations between events and modalities...

		foreach(Input::get('checkboxes') as $selected){

			$insert = $db->insert('museum_events_types_relations', 
				[
					'id_event'=> $lastId,

					'id_event_type' => $selected

				]

			);

		}

		//copy template...

		$permalinkFolder = '../../../../../../../eventos/' . Input::get('url');

		$mk = $filesManager->makeDirectory($permalinkFolder);

		$dst = $permalinkFolder;

		$src = '../../../templates/event_template';

		copyFiles($src, $dst);

		//copy template...

		//if($_FILES['main-image']['name'] == "blob" && $_FILES['main-image']['type'] == "application/octet-stream" && $_FILES['main-image']['size'] == 0) {

		if(!file_exists($_FILES['main-image']['tmp_name']) || !is_uploaded_file($_FILES['main-image']['tmp_name'])){

			addLogData($user);

			printData(1, 'ok without image...por que?');

		}else{

			$uniqueId = uniqid();

			$fileSize = 20971520;

			$eventFolder = '../../../sources/images/events/' . $lastId;

			$deleteFolder = '../../../sources/images/events/' . $lastId . '/delete';

			$fileFolder = $eventFolder . '/' . $uniqueId;

			$eventFolderExists = $filesManager->checkDirectory($eventFolder);

			if(!$eventFolderExists){

				$eventFolderCreated = $filesManager->makeDirectory($eventFolder);

				$deleteFolder = $filesManager->makeDirectory($deleteFolder);
			}

			$folderExists = $filesManager->checkDirectory($fileFolder);

			if(!$folderExists){

				$folderCreated = $filesManager->makeDirectory($fileFolder);

			}else{

				$folderCreated = true;
			}

			if($folderCreated){

				$checkPHPExtensions = $filesManager->checkPHPExtensions();
			}

			if($checkPHPExtensions){

				if (!empty($_FILES)) {

					$tempFile = $_FILES['main-image']['tmp_name'];

					if(is_file($tempFile)){

						$checkFileSize = $filesManager->checkFileSize($tempFile, $fileSize);

						if($checkFileSize){

							$targetPath = dirname( __FILE__ ) . $ds . $fileFolder . $ds;

							$name = $lastId . '_' . $uniqueId;

							$checkMimeType = $filesManager->checkImageMimeType($_FILES['main-image']['tmp_name']);

							if($checkMimeType){

								//-----------------------------------------------------
								//general image settings...

								$newMime = 'image/jpeg';

								$newExtension = 'jpeg';

								//-----------------------------------------------------
								//the default image will be the retina image...

								$uploaded =  $targetPath . $name . '_uploaded.' . $newExtension;

								move_uploaded_file($tempFile, $uploaded);

								//-----------------------------------------------------
								//the normal image is the half of the retina...

								$originalFileRetina =  $targetPath . $name . '_original@2x.' . $newExtension;

								$image->fromFile($uploaded)->thumbnail($retinaOriginalWidth, $retinaOriginalHeight, $retinaOriginalHeight/2)->toFile($originalFileRetina, $newMime, 70);


								//-----------------------------------------------------
								//this is a square from the retina image

								$originalFileRetinaSq =  $targetPath . $name . '_original_sq@2x.' . $newExtension;
								
								$image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight, $retinaOriginalHeight, $retinaOriginalHeight/2)->toFile($originalFileRetinaSq, $newMime, 70);

								//-----------------------------------------------------
								//the normal image is the half of the retina...

								$originalFile =  $targetPath . $name . '_original.' . $newExtension;

								$image->fromFile($originalFileRetina)->resize(null, $retinaOriginalHeight/2)->toFile($originalFile, $newMime, 70);

								//-----------------------------------------------------
								//the normal image sq version

								$originalFileSq =  $targetPath . $name . '_original_sq.' . $newExtension;
								
								$image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight/2, $retinaOriginalHeight/2, $retinaOriginalHeight/4)->toFile($originalFileSq, $newMime, 70);

								//-----------------------------------------------------
								//the medium version for the retina...

								$mediumRetinaFile =  $targetPath . $name . '_medium@2x.' . $newExtension;

								$image->fromFile($originalFileRetina)->resize(null, $retinaOriginalHeight/2)->toFile($mediumRetinaFile, $newMime, 70);

								//-----------------------------------------------------
								//the medium sq version for the retina...

								$mediumRetinaFileSq =  $targetPath . $name . '_medium_sq@2x.' . $newExtension;
								
								$image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight/2, $retinaOriginalHeight/2, $retinaOriginalHeight/4)->toFile($mediumRetinaFileSq, $newMime, 70);

								//-----------------------------------------------------
								//the medium version for the normal...

								$mediumFile =  $targetPath . $name . '_medium.' . $newExtension;

								$image->fromFile($originalFileRetina)->resize(null, $retinaOriginalHeight/4)->toFile($mediumFile, $newMime, 70);

								//-----------------------------------------------------
								//the medium sq version for the retina...

								$mediumFileSq =  $targetPath . $name . '_medium_sq.' . $newExtension;
								
								$image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight/4, $retinaOriginalHeight/4, $retinaOriginalHeight/2)->toFile($mediumFileSq, $newMime, 70);

								//-----------------------------------------------------
								//small retina

								$smallRetinaFile =  $targetPath . $name . '_small@2x.' . $newExtension;

								$image->fromFile($originalFileRetina)->resize(null, $retinaOriginalHeight/4)->toFile($smallRetinaFile, $newMime, 70);

								//-----------------------------------------------------
								//the small sq version for the retina...

								$smallRetinaSq =  $targetPath . $name . '_small_sq@2x.' . $newExtension;
								
								$image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight/4, $retinaOriginalHeight/4, $retinaOriginalHeight/8)->toFile($smallRetinaSq, $newMime, 70);

								//-----------------------------------------------------
								//small 

								$smallFile =  $targetPath . $name . '_small.' . $newExtension;

								$image->fromFile($originalFileRetina)->resize(null, $retinaOriginalHeight/8)->toFile($smallFile, $newMime, 70);

								//-----------------------------------------------------
								//the small sq version for the retina...

								$smallFileSq =  $targetPath . $name . '_small_sq.' . $newExtension;
								
								$image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight/8, $retinaOriginalHeight/8, $retinaOriginalHeight/16)->toFile($smallFileSq, $newMime, 70);

								$q = DB::getInstance()->query('SELECT unique_id, id FROM museum_images WHERE sid = ? AND source = ?', array($lastId, 'events'));

								$c = $q->count();

								if($c < 1){

									DB::getInstance()->insert('museum_images', 

										array('sid'=>$lastId, 'unique_id'=>$uniqueId, 'mimetype'=>$newMime, 'source'=>'events', 'path'=>'events'));

									$lastPictureId = DB::getInstance()->lastId();

									DB::getInstance()->insert('museum_image_crop_box',

										array('id'=>$lastPictureId));

									DB::getInstance()->insert('museum_image_data',

										array('id'=>$lastPictureId));

									DB::getInstance()->insert('museum_image_filters',

										array('id'=>$lastPictureId));

									DB::getInstance()->insert('museum_image_canvas_data',

										array('id'=>$lastPictureId));

									addLogData($user);

								}else{

								}

								printData(1, '1');

							}else{

								printData(0, 'error mimetype');

							}

						}else{

							printData(0, 'filesize error');

						}

					}else{

						printData(0, 'empty file');

					}

				}

			}

		}

	}else{ }

}


/**
* @function copyFiles
* @description Copy all the files.
*
* @param {object} $src  	- 	 the source.
* @param {object} $dst  	- 	 the destination.
*/

function copyFiles($src, $dst) {

	$dir = @opendir($src);

	if (!file_exists($dst)) @mkdir($dst);

	while (false !== ($file = readdir($dir))) {

		if (( $file != '.' ) && ( $file != '..' )) {

			if ( is_dir($src . '/' . $file) ) copyFiles($src . '/' . $file, $dst . '/' . $file); 
			
			else copy($src . '/' . $file, $dst . '/' . $file);
		
		}

	}
	
	closedir($dir); 
}


/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The event ' . Input::get('name') .  ' was added.';

	$userString = 'Agregaste un evento <b>' . ' ' . '</b> al sistema.';

	Logger::addLogData($user->data()->id, 

		'user',

		$systemString,

		$userString,

		'events',

		'events');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['end_date'] = Input::get('end_date');

	$status['start_date'] = Input::get('start_date');

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>