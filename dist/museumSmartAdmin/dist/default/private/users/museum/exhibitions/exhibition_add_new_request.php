<?php

require_once '../../core/init.php';

$status['init'] = true;

$db = DB::getInstance();

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'email' => array('display'=>'email', 'email' => true),
	
	'inscription-name' => array('display'=>'inscription-name', 'required' => true),
	
	'inscription-surname' => array('display'=>'inscription-surname', 'required' => true),

	'exhibition-id' => array('display'=>'exhibition-id', 'required' => true),

	'exhibition-name' => array('display'=>'exhibition-name', 'required' => true),

	'exhibition-url' => array('display'=>'exhibition-url', 'required' => true)

));

if($validation->passed()){
	
	$status['inscription-name'] = Input::get('inscription-name');
	
	$status['inscription-surname'] = Input::get('inscription-surname');

	$status['email'] = Input::get('email');

	$status['exhibition-id'] = Input::get('exhibition-id');

	$status['phone'] = Input::get('phone');

	$status['city'] = Input::get('institution-city');

	$status['address'] = Input::get('address');

	$status['medium'] = Input::get('medium');

	$status['exhibition-name'] = Input::get('exhibition-name');

	$fields = array(

		'id_exhibition' => Input::get('exhibition-id'),

		'name'=>Input::get('inscription-name'),

		'surname'=>Input::get('inscription-surname'),

		'email'=>Input::get('email'),

		'phone'=>Input::get('phone'),

		'institution_city'=>Input::get('institution-city'),

		'institution_address'=>Input::get('institution-address'),

		'institution_url'=>Input::get('institution-url'),

		'institution_name'=>Input::get('institution-name'),

		'institution_type'=>Input::get('institution-type'),

	);

	$db->insert('museum_exhibitions_requests', $fields);


	//general settings...

	$settingsQ = $db->query("SELECT * FROM settings");

	$settings = $GLOBALS['settings'] = $settingsQ->first();

	//support settings...

	$supportSettingsQ = $db->query("SELECT * FROM support_settings");

	$GLOBALS['supportSettings'] = $supportSettingsQ->first();

	//museum settings...

	$museumSettingsQ = $db->query("SELECT * FROM museum_data");

	$museumSettings = $museumSettingsQ->first();

	//museum social media...

	$museumSocialMediaQ = $db->query("SELECT * FROM museum_social_media");

	$museumSocialMediaSettings = $museumSocialMediaQ->first();

	//template for subscriber...

	$address = $museumSettings->address . ' ' . $museumSettings->address_number . ', ' . $museumSettings->zip_code . ' ' . $museumSettings->city;

	$finalURL = 'https://www.museodelholocausto.org.ar/muestras/' . Input::get('exhibition-url');

	$fullName = Input::get('inscription-name') . ' ' . Input::get('inscription-surname');

	$message = file_get_contents('../../../email_templates/exhibition_subscriber.html');

	$message = str_replace('%projectTitle%', $museumSettings->title, $message);

	$message = str_replace('%exhibitionName%', Input::get('exhibition-name'), $message);
	
	$message = str_replace('%subscriber%', $fullName, $message);

	$message = str_replace('%ip%', Helpers::getIP(), $message);

	$message = str_replace('%os%', Helpers::getOS(), $message);

	$message = str_replace('%browser%', Helpers::getBrowser(), $message);

	$message = str_replace('%email%', $GLOBALS['supportSettings']->support_email, $message);

	$message = str_replace('%address%', $address, $message);

	$message = str_replace('%te%', $museumSettings->te, $message);

	$message = str_replace('%museumEmail%', $museumSettings->email, $message);

	$message = str_replace('%lat%', $museumSettings->lat, $message);

	$message = str_replace('%lon%', $museumSettings->lon, $message);

	$message = str_replace('%facebook%', $museumSocialMediaSettings->facebook, $message);

	$message = str_replace('%instagram%', $museumSocialMediaSettings->instagram, $message);

	$message = str_replace('%twitter%', $museumSocialMediaSettings->twitter, $message);
	
	$sent = Helpers::email('mariano.aditivo@gmail.com', utf8_decode('Solicitud de muestra'), $message);

	if($sent){

		$status['envio'] = $sent;

	}else{

		$status['envio'] = $sent;

	}

	//template for museum...

	$museumMessage = file_get_contents('../../../email_templates/exhibition_request.html');

	$museumMessage = str_replace('%exhibitionName%', Input::get('exhibition-name'), $museumMessage);
	
	$museumMessage = str_replace('%subscriber%', $fullName, $museumMessage);

	$museumMessage = str_replace('%email%', Input::get('email'), $museumMessage);

	$museumMessage = str_replace('%phone%', Input::get('phone'), $museumMessage);

	$museumMessage = str_replace('%institutionName%', Input::get('institution-name'), $museumMessage);

	$museumMessage = str_replace('%institutionType%', Input::get('institution-type'), $museumMessage);

	$museumMessage = str_replace('%institutionCity%', Input::get('institution-city'), $museumMessage);

	$museumMessage = str_replace('%institutionAddress%', Input::get('institution-address'), $museumMessage);

	$museumMessage = str_replace('%institutionURL%', Input::get('institution-url'), $museumMessage);

	$museumMessage = str_replace('%ip%', Helpers::getIP(), $museumMessage);

	$museumMessage = str_replace('%os%', Helpers::getOS(), $museumMessage);

	$museumMessage = str_replace('%browser%', Helpers::getBrowser(), $museumMessage);

	$sentMuseumMsg = Helpers::email('mariano.aditivo@gmail.com', utf8_decode('Solicitud de muestra'), $museumMessage);

	if($sentMuseumMsg){

		$status['envio2'] = 'exitoso';

	}else{

		$status['envio2'] = 'no exitoso';

	}

	echo json_encode($status);

	return;

}

?>