<?php

require_once '../core/init.php';

$status['init'] = true;

$db = DB::getInstance();

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_general'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();


if(!Input::exists()) printData(0);

if(!Token::check(Input::get('token'))) printData(0);

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'te' => array('display'=> 'te', 'required' => true),

	'email' => array('display'=> 'email', 'required' => true),

	'address' => array('display'=> 'address', 'required' => true),

	'address_number' => array('display'=> 'address_number', 'required' => true),

	'zip_code' => array('display'=> 'zip_code', 'required' => true),

	'city' => array('display'=> 'city', 'required' => true)

)

);

if(!$validation->passed()) printData(0);

else{

	$musuemData =$db->query("SELECT * FROM museum_data");

	$db->update('museum_data',

		$musuemData->first()->id,

		['te' => Input::get('te'),

		'te_2' => Input::get('te_2'),

		'te_3' => Input::get('te_3'),

		'email' => Input::get('email'),

		'email_2' => Input::get('email_2'),

		'email_3' => Input::get('email_3'),

		'address' => Input::get('address'),

		'dates_and_times' => Input::get('dates_and_times'),

		'address_number' => Input::get('address_number'),

		'zip_code' => Input::get('zip_code'),

		'city' => Input::get('city')

	]);

	printData(1);
}


function printData($dataId){

	$status['token'] = Input::get('token');
	
	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	echo json_encode($status);

	exit();
}

echo json_encode($status);


?>