<?php

//Update the date of last modified contegory content (Apps)

class UpdateCategoriesDates{

  private $source;
  private $idCategory;
  private $db;

  //source = category app (string) - "prensa" - "guetos" - etc
  public function __construct($_source) {

    $this->source = $_source;

     $this->db = DB::getInstance();

   }

   private function getCategoryBySource(){

     $sourceQuery = $this->db->query("SELECT id, source FROM app_museum_modified_categories WHERE source = ?", [$this->source]);
     
     $actualSource = $sourceQuery->first();

     return $actualSource->id;

   }


  public function updateModifiedDate(){

    $idCat = $this->getCategoryBySource();

    $date = date("Y-m-d H:i:s");

    if($idCat != 0 || $idCat != '' || $idCat != NULL){

      //$updateQuery = $this->db->query("UPDATE app_museum_modified_dates SET last_update = ? WHERE id_category = ?", [$date, $idCat]);

      $query = "INSERT INTO app_museum_modified_dates (id_category, last_update) VALUES (?, ?)
               ON DUPLICATE KEY UPDATE
               last_update=?";

      $updateQuery = $this->db->query($query, [$idCat, $date, $date]);

      return 1;

    }else{

      return 0;

    }


  }


  public function getLastModifiedDate(){

    $idCat = $this->getCategoryBySource();

    $sql = "SELECT id_category, last_update, active, deleted FROM app_museum_modified_dates WHERE id_category = ? AND active = ? AND deleted = ?";

    $lastUpdateDate = $this->db->query($sql, [$idCat, 1, 0]);

    $date = $lastUpdateDate->results(true);

    if(!empty($date)){

      $arrResponse = array(

        "last_update" => $date[0]["last_update"]

      );

    }else{

      $arrResponse = "no date data for this app";

    }


    return $arrResponse;

  }


}


?>
