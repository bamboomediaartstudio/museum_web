<?php

/**
 * @summary Delete item :).
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the requires */

require_once '../../core/init.php';

/**initialization */

$user = new User();

if(!$user->isLoggedIn()){

	Redirect::to('index.php');

}

$status['init'] = true;

$db = DB::getInstance();

/**if there is no data, print and out */

if(!Input::exists()) printData(0);

/**if there is no token, print and out */

//if(!Token::check(Input::get('token'))) printData(0);

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'id', 'required' => true),

	'action' => array('display'=> 'action', 'required' => true),

	'table' => array('display'=> 'table', 'required' => true),

	'source' => array('display'=> 'source', 'required' => true),

	'status' => array('display'=> 'status', 'required' => true)));

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0);

/**Select the wording according to the type of action...update or delete. */

else{

	$itemName = Input::get('name');

	switch (Input::get('source')) {

		/*objectives*/

		case 'objectives':

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_objectives_delete'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = "An objective was deleted.";

		$userString = "Eliminaste un objetivo.";

		$query = "SELECT internal_order FROM " .  Input::get('table') . " WHERE id = ?";

		$order = $db->query($query, [Input::get('id')]);

		$params = [$order->first()->internal_order];

		$maxResults = 'SELECT * FROM ' . Input::get('table') . ' WHERE internal_order > ?';

		/*shoa faq*/

		case 'faq':

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_faq_delete'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = "A question was deleted from the Shoá FAQ.";

		$userString = "Eliminaste una pregunta del listado del FAQ de La Shoá";

		$query = "SELECT internal_order FROM " .  Input::get('table') . " WHERE id = ?";

		$order = $db->query($query, [Input::get('id')]);

		$params = [$order->first()->internal_order];

		$maxResults = 'SELECT * FROM ' . Input::get('table') . ' WHERE internal_order > ?';

		/*shoa faq*/

		case 'shoa_faq':

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_shoa_faq_delete'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = "A question was deleted from the Shoá FAQ.";

		$userString = "Eliminaste una pregunta del listado del FAQ de La Shoá";

		$query = "SELECT internal_order FROM " .  Input::get('table') . " WHERE id = ?";

		$order = $db->query($query, [Input::get('id')]);

		$params = [$order->first()->internal_order];

		$maxResults = 'SELECT * FROM ' . Input::get('table') . ' WHERE internal_order > ?';
		
		break;

		/*testimony*/

		case 'testimonials':

		$category = Input::get('category');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_staff_delete_member'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = "The testimony of " . $itemName . " was deleted from the category " . $category;

		$userString = "Eliminaste el testimonio de <b>" . $itemName . "</b> de la categoría <b>" . $category . '</b>';

		$query = "SELECT internal_order, id_museum_testimonial_category FROM " . Input::get('table') . 

		" WHERE id = ?";

		$order = $db->query($query, [Input::get('id')]);

		$params = [$order->first()->internal_order, $order->first()->id_museum_testimonial_category];

		$maxResults = 'SELECT * from ' . Input::get('table') . ' WHERE internal_order > ? AND id_museum_testimonial_category = ?';
		
		break;

		case 'opinions':

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_opinion_delete'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = "The opinion of " . $itemName . " was deleted.";

		$userString = "Eliminaste la opinión de <b>" . $itemName . '</b>';

		$query = "SELECT internal_order FROM " .  Input::get('table') . " WHERE id = ?";

		$order = $db->query($query, [Input::get('id')]);

		$params = [$order->first()->internal_order];

		$maxResults = 'SELECT * FROM ' . Input::get('table') . ' WHERE internal_order > ?';
		
		break;

		case 'staff':

		$category = Input::get('category');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_staff_delete_member'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = "The staff member " . $itemName . " was deleted from the category " . $category;

		$userString = "Eliminaste a <b>" . $itemName . "</b> de la categoría <b>" . $category . '</b>';

		$query = "SELECT internal_order, id_museum_staff_category FROM " . Input::get('table') . 

		" WHERE id = ?";

		$order = $db->query($query, [Input::get('id')]);

		$params = [$order->first()->internal_order, $order->first()->id_museum_staff_category];

		$maxResults = 'SELECT * from ' . Input::get('table') . ' WHERE internal_order > ? AND id_museum_staff_category = ?';
		
		break;
		
		default:
		
		break;
	}

	$change = (Input::get('status') == 'true') ? 1 : 0;

	$updated = $db->update(Input::get('table'),Input::get('id'), array('deleted'=>$change));

	/**If we are deleting, there is an extra step where we have to reorder all, reducing in one 
	the sort index of all the subsequent items inside items.

	example:
	1, 2, 3 - deleted, 4, 5

	then:
	1, 2, 4, 5

	solution:
	1, 2, 3, 4
	 */

	$all = $db->query($maxResults, $params);

	foreach($all->results() as $result){

		$newOrder = $result->internal_order - 1;

		$id = $result->id;

		$fields = array('internal_order' => $newOrder);

		$db->update(Input::get('table'), $id, $fields);
	}

	Logger::addLogData($user->data()->id, 'user',  $systemString,  $userString, Input::get('source'), Input::get('source'));

	printData(1);	
}

/**
* @function printData
* @description easy way to print only once all the json data for JS reading.
*
* @param {int} $dataId - id for the wording.
*/

function printData($dataId){

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	$status['changeStatus']  = Input::get('status');

	echo json_encode($status);

	exit();
}


?>