<?php

/**
 * @summary Sort images.
 *
 * @description -
 *
 * @author Colo baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the includes */

require_once '../../core/init.php';

include '../general/UpdateCategoriesDates.class.php';

/**validate.. */

$user = new User();

if($user->isLoggedIn()){

	$validate = new Validate();

	$validation = $validate->check($_POST, array(

		'stack' => array('display'=> 'stack', 'required' => true),

		'db' => array('display'=> 'db', 'required' => true)

	));

}


/**pass validation... */


if($validation->passed()){

	$json = json_decode(Input::get('stack'), true);

	$counter = 1;

	foreach ($json as $item => $actualItem) {

		$fields=array('internal_order'=> $counter);

		DB::getInstance()->update(Input::get('db'), $actualItem['key'], $fields);

		$counter+=1;
	}

	$objUpdateCatDate = new UpdateCategoriesDates("guetos");

	$objUpdateCatDate->updateModifiedDate();

	$status['success'] = 'success';

	$status['test'] = 'ok';

	$status['title'] = 'Se actualizó el order de las imágenes';

	$status['body'] = 'El orden se actualizó correctamente';

	echo json_encode($status);

	return;

}



?>
