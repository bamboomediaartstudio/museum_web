<?php

/**
 * @summary Move a batch of images.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the initializations   */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

/**all the includes */

require_once '../../core/init.php';

require '../../../libraries/claviska/SimpleImage.php';

/**all the variables */

$filesManager = new FilesManager();

$ds = DIRECTORY_SEPARATOR;

$image = new \claviska\SimpleImage();

$originalPrefix = '_original';

$squarePrefix = '_sq';

$defaultPrefix = '_default';

$status['init'] = true;

$db = DB::getInstance();

$counterQuery = DB::getInstance()->query('SELECT MAX(internal_order) as internal_order FROM museum_images WHERE sid = ? AND source = ?', 

    [Input::get('id'), Input::get('source')]);

$c = $counterQuery->count();

if($c == 0){

    $counter = 1;

}else{

    $counterRow = $counterQuery->first();

    $counter = $counterRow->internal_order + 1;

} 

/**if there is no data, print and out */

if(!Input::exists()){

    echo json_encode(['error'=>'Missing input!']); 

    return;

}

/**validate.. */

$user = new User();

if($user->isLoggedIn()){

    $validate = new Validate();

    $validation = $validate->check($_POST, array(

        'id' => array('display'=> 'id', 'required' => true)

    ));

    /**if validation did not pass, print and out. Else is a valid verification. Continue */

    if(!$validation->passed()){

        echo json_encode(['error'=>'validation not passed']); 

        return;

    }else{

        $newMime = 'image/jpeg';

        $newExtension = 'jpeg';

        switch (Input::get('source')) {


            case 'books':

            $fileSize = Config::get('books/books_picture_max_size');

            $userFolder = Config::get('books/books_picture_folder') .  Input::get('id');

            break;

            case 'education':

            $fileSize = Config::get('education/education_picture_max_size');

            $userFolder = Config::get('education/education_picture_folder') .  Input::get('id');

            break;

            case 'visits':

            $fileSize = Config::get('visits/visits_picture_max_size');

            $userFolder = Config::get('visits/visits_picture_folder') .  Input::get('id');

            break;

            case 'library':

            $fileSize = Config::get('library/library_picture_max_size');

            $userFolder = Config::get('library/library_picture_folder') .  Input::get('id');

            break;

            case 'individual_heritage':

            $fileSize = Config::get('heritage/heritage_picture_max_size');

            $userFolder = Config::get('heritage/heritage_picture_folder') .  Input::get('id');

            break;

            case 'sponsors':

            $fileSize = Config::get('sponsors/sponsors_picture_max_size');

            $userFolder = Config::get('sponsors/sponsors_picture_folder') .  Input::get('id');

            $newMime = 'image/png';

            $newExtension = 'png';

            break;

            case 'individual_course':

            $fileSize = Config::get('courses/courses_picture_max_size');

            $userFolder = Config::get('courses/courses_picture_folder') .  Input::get('id');

            break;

            case 'individual_event':

            $fileSize = Config::get('events/events_picture_max_size');

            $userFolder = Config::get('events/events_picture_folder') .  Input::get('id');

            break;

            
            case 'individual_exhibition':

            $fileSize = Config::get('exhibitions/exhibitions_picture_max_size');

            $userFolder = Config::get('exhibitions/exhibitions_picture_folder') .  Input::get('id');

            break;

            case 'individual_news':

            $fileSize = Config::get('news/news_picture_max_size');

            $userFolder = Config::get('news/news_picture_folder') .  Input::get('id');

            break;

            case 'individual_class':

            $fileSize = Config::get('classes/classes_picture_max_size');

            $userFolder = Config::get('classes/classes_picture_folder') .  Input::get('id');

            break;

            default:
                # code...
                break;
        }

        $fileFolder = $userFolder . '/images';

        $userFolderExists = $filesManager->checkDirectory($fileFolder);

        if($userFolderExists == false){

            $folderCreated = $filesManager->makeDirectory($fileFolder);

        }else{

            $folderCreated = true;

        }

        if($folderCreated){

            $checkPHPExtensions = $filesManager->checkPHPExtensions();

        }

        if($checkPHPExtensions == false){

            echo json_encode(['error'=>'Missing PHP extensions.']); 

            return;

        }

        if (empty($_FILES)) {

            echo json_encode(['error'=>'No files.']); 

            return;


        }

        if (empty($_FILES['images-batch'])) {

            echo json_encode(['error'=>'No files found for upload.']); 

            return;
        }

        $output = [];

        $images = $_FILES['images-batch'];

        $success = null;

        $filenames = $images['name'];
        
        for($i=0; $i < count($filenames); $i++){
            
            $tempFile = $images['tmp_name'][$i];

            if(!is_file($tempFile)){

                foreach ($paths as $file) { unlink($file); }

                echo json_encode(['error'=>'one of the files nos not a file']); 

                return;

            }

            $checkFileSize = $filesManager->checkFileSize($tempFile, $fileSize);

            if($checkFileSize == false){

                echo json_encode(['error'=>'opss! La imagen es demasiado grande']); 

                return;

            }

            $checkMimeType = $filesManager->checkImageMimeType($tempFile);

            if($checkMimeType == false){

                echo json_encode(['error'=>'Un archivo contiene un mimetype dudoso y se lo eliminó.']); 

                return;

            }

            $uniqueId = uniqid();

            //$counter++;

            $collectionFolder = $fileFolder . '/' . $uniqueId . '/';

            $createCollectionFolder = $filesManager->makeDirectory($collectionFolder);

            //-----------------------------------------------------
            //general image settings...

            $targetPath = dirname( __FILE__ ) . $ds . $collectionFolder . $ds;

            $name = Input::get('id') . '_' . $uniqueId;

            //$newMime = 'image/jpeg';

            //$newExtension = 'jpeg';

            //-----------------------------------------------------
            //the default image will be the retina image...

            $retinaFile =  $targetPath . $name . '_original@2x.' . $newExtension;

            //move_uploaded_file($tempFile, $retinaFile);

            if(move_uploaded_file($tempFile, $retinaFile)) {

                //-----------------------------------------------------
                //the normal image is the half of the retina....

                $normalFile =  $targetPath . $name . '_original.' . $newExtension;

                $image->fromFile($retinaFile)->resize(null, 500)->toFile($normalFile, $newMime, 70);

                //...

                //...

                //-----------------------------------------------------
                //then we have a retina thumb.

                $retinaFileThumb =  $targetPath . $name . '_thumb@2x.' . $newExtension;

                $image->fromFile($normalFile)->resize(null, 250)

                ->toFile($retinaFileThumb, $newMime, 70);

                //-----------------------------------------------------
                //and then we have a normal thumb.

                $normalThumb =  $targetPath . $name . '_thumb.' . $newExtension;

                $image->fromFile($normalFile)->resize(null, 125)

                ->toFile($normalThumb, $newMime, 70);

                //-----------------------------------------------------
                //then we have a retina square.

                $retinaSq =  $targetPath . $name . '_sq@2x.' . $newExtension;

                $image->fromFile($normalFile)->thumbnail(500, 500, 250)

                ->toFile($retinaSq, $newMime, 70);

                //-----------------------------------------------------
                //and a normal sq.

                $normalSq =  $targetPath . $name . '_sq.' . $newExtension;

                $image->fromFile($normalFile)->thumbnail(250, 250, 125)

                ->toFile($normalSq, $newMime, 70);

                //db stuff...

                DB::getInstance()->insert('museum_images', 

                array('sid'=>Input::get('id'), 'unique_id'=>$uniqueId, 'mimetype'=>$newMime, 

                    'source'=>Input::get('source'), 'path'=>Input::get('source'), 'internal_order'=>$counter));



                $lastPictureId = DB::getInstance()->lastId();

                DB::getInstance()->insert('museum_image_crop_box',

                    array('id'=>$lastPictureId));

                DB::getInstance()->insert('museum_image_data',

                    array('id'=>$lastPictureId));

                DB::getInstance()->insert('museum_image_filters',

                    array('id'=>$lastPictureId));

                DB::getInstance()->insert('museum_image_canvas_data',

                array('id'=>$lastPictureId));

                $success = true;

                $paths[] = $retinaFile;

            } else {

                $success = false;

                break;

            }

        }

        if ($success == true) {

            $output = [];
            
            $output = ['tira' => 'success si'];


            //save_data($userid, $username, $paths);

            // for example you can get the list of files uploaded this way

            $output = ['uploaded' => $paths];

        } elseif ($success === false) {

            $output = ['error'=>'Error while uploading images. Contact the system administrator'];

            // delete any uploaded files

            foreach ($paths as $file) {

                unlink($file);

            }

        } else {

            $output = ['error'=>'No files were processed.'];

        }

    }

}


$output['test'] = 'que pasa?';

echo json_encode($output);

?>