<?php

/**
 * @summary update and do all the stuff in relation with checkboxes that are not orderable.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the requires */

require_once '../../core/init.php';

/**initialization */

$user = new User();

if(!$user->isLoggedIn()){

	Redirect::to('index.php');

}

/**initialization */

$user = new User();

if(!$user->isLoggedIn()){

	Redirect::to('login.php');

}

$db = DB::getInstance();

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='update_db_value'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'input');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'id', 'required' => true),

	'newValue' => array('display'=> 'newValue', 'required' => true),

	'value' => array('display'=> 'value', 'required' => true),

	'filter' => array('display'=> 'filter', 'required' => true),

	'table' => array('display'=> 'table', 'required' => true),

	'secondTable' => array('display'=> 'secondTable', 'required' => true),

), false);

if(!$validation->passed()) printData(0, 'validation');

else{

	//check if the row exists.

	$query = 'SELECT * FROM ' . Input::get('filter') . ' WHERE ' . Input::get('table') . ' = ? AND deleted = ? AND ' . Input::get('secondTable') . ' = ?';

	$sort = DB::getInstance()->query($query, [Input::get('id'), 0, Input::get('value')]);

	//si existe, eliminar...

	if($sort->count() >0){

		$sortId = $sort->first()->id;

		$db->delete(Input::get('filter'),array('id','=',$sortId));

		$status['delete'] = 'delete';

	}else{

		$fields=array(

		Input::get('table') => Input::get('id'), 

		Input::get('secondTable') => Input::get('value'));

		DB::getInstance()->insert(Input::get('filter'),$fields);

		$status['id'] = Input::get('id');

		$status['value'] = Input::get('value');

		$status['existe']  		= 		'no existe entonces CREATE;';


	}

	echo json_encode($status);

	exit();
}


?>