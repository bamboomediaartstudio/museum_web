<?php

/**
 * @summary Update the order of the list.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the requires */

require_once '../../core/init.php';

/**initialization */

$user = new User();

if(!$user->isLoggedIn()){

	Redirect::to('index.php');

}

$status['init'] = true;

$db = DB::getInstance();

/**wording */

//$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_staff_update_order'");

//if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0);

/**if there is no token, print and out */

//if(!Token::check(Input::get('token'))) printData(0);

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'array' => array('display'=> 'array', 'required' => true),

	'table' => array('display'=> 'table', 'required' => true),

	'source' => array('display'=> 'source', 'required' => true)

), false);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0);

else{

	foreach(Input::get('array') as $item){

		$db->update(Input::get('table'), $item['id'], array('internal_order'=>$item['newPosition']));

	}

	$selectedName = Input::get('selectedName');
	
	$selectedId = Input::get('selectedId');

	$category = Input::get('category');

	switch (Input::get('source')) {

		case 'books':	

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_books_order'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The order of '. Input::get('userName') .' was changed.';
			
			$userString = 'Modificaste el orden de un libro ' . $selectedName . '.';

			break;

		case 'investigations':	

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_investigations_order'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The order of '. Input::get('userName') .' was changed.';
			
			$userString = 'Modificaste el orden de la investigación ' . $selectedName . '.';

			break;


		case 'social_media':

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_sm_update_order'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The order of '. Input::get('userName') .' was changed.';
			
			$userString = 'Modificaste el orden de la red social ' . $selectedName . '.';

			break;

		case 'home_menu':

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_menu_update_order'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The module order of '. Input::get('userName') .' was changed.';
			
			$userString = 'Modificaste el orden del módulo ' . $selectedName . '.';

			break;

		case 'home_modules':

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_home_modules_order'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The module order of '. Input::get('userName') .' was changed.';
			
			$userString = 'Modificaste el orden del módulo ' . $selectedName . '.';

			break;


		case 'objectives':

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_opinion_update_order'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The objective ' . Input::get('selectedName') . ' order was changed.';
			
			$userString = 'Modificaste el orden del objetivo ' . Input::get('selectedName') . '.';

			break;

		case 'faq':

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_opinion_update_order'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The FAQ ' . Input::get('selectedName') . ' order was changed.';
			
			$userString = 'Modificaste el orden de la pregunta ' . Input::get('selectedName') . ' del FAQ.';

			break;

		case 'shoa_faq':

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_opinion_update_order'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The Shoá FAQ ' . Input::get('selectedName') . ' order was changed.';
			
			$userString = 'Modificaste el orden de la pregunta ' . Input::get('selectedName') . ' del FAQ de La Shoá.';

			break;

		case 'staff':

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_staff_update_order'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 

			'the order of the staff member '. $selectedName .' order was modified inside the category ' . $category;
			
			$userString = 'Modificaste el orden de ' . $selectedName . ' dentro de la categoría ' . $category;
			
			break;

		case 'opinions':

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_opinion_update_order'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The opinion order of '. Input::get('userName') .' was changed.';
			
			$userString = 'Modificaste el orden de la opinión de ' . $selectedName . '.';

			break;

		case 'testimonials':

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_staff_update_order'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 

			'the order of the testimony of '. $selectedName .' was modified inside the category ' . $category;
			
			$userString = 'Modificaste el orden del testimonio de ' . $selectedName . ' dentro de la categoría ' . $category;
			
			break;
		
		default:

			break;
	}

	Logger::addLogData($user->data()->id, 'user', $systemString, $userString, Input::get('source'), Input::get('source'));

	printData(1);
}

/**
* @function printData
* @description easy way to print only once all the json data for JS reading.
*
* @param {int} $dataId - id for the wording.
*/

function printData($dataId){

	$status['status'] 		= 		$GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] 		= 		$GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] 			= 		$GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  		= 		$GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  		= 		$GLOBALS['wordingArray'][$dataId]->action_button_label;

	$status['changeStatus'] =		 Input::get('status');

	echo json_encode($status);

	exit();
}

?>