<?php

/**
 * @summary update and do all the stuff in relation with checkboxes and the implications of adding and deletting!
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the requires */

require_once '../../core/init.php';

/**initialization */

$user = new User();

if(!$user->isLoggedIn()){

	Redirect::to('index.php');

}

/**initialization */

$user = new User();

if(!$user->isLoggedIn()){

	Redirect::to('login.php');

}

$db = DB::getInstance();

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='update_db_value'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'input');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'id', 'required' => true),

	'idName' => array('display'=> 'idName', 'required' => true),

	'sid' => array('display'=> 'sid', 'required' => true),

	'action' => array('display'=> 'action', 'required' => true),

	'newValueName' => array('display'=> 'newValueName', 'required' => true),

	'filter' => array('display'=> 'filter', 'required' => true)

), false);

if(!$validation->passed()) printData(0, 'validation');

else{

	/**
	* first we have to know if we are adding a memeber to a new category or if we are deleting it. 
	* we know this information: Input::get('action')
	*
	* if is adding:
	*
	* we have to know the last sorting value inside this list. For this we have to get the max
	* internal order (max(internal_order)) for the received column, checking for deleted as true (1)
	* and deleted as false (0)
	*
	* once we have the order id, we add the new relation and we asign the item order as the last one.
	*
	* if we are deleting: we deduce that the relation already exists
	*
	* we need to know the user order / sort so that we can change the subsequent users once it was deleted.
	*
	* We mark the user as deleted.
	*
	* then we loop through all the subsequent users and modify the sorting reduing it by one.
	*/

	if(Input::get('action') == "1"){

		$lastSort = DB::getInstance()->query('SELECT max(internal_order) as internal_order FROM 

			' . Input::get('filter') . ' WHERE ' . Input::get('newValueName') . ' = ? AND 

			deleted = ? ', [Input::get('sid'), 0]);

		$status['action'] = "adding";

		$status['lastId'] = $lastSort->first()->internal_order;

		$status['newId'] = ($lastSort->first()->internal_order + 1);

		$newRelation = DB::getInstance()->insert(Input::get('filter'), 
			[

				Input::get('idName') => Input::get('id'),

				Input::get('newValueName') => Input::get('sid'),

				'internal_order' => ($lastSort->first()->internal_order + 1)

			]);

		printData(1, 'adding');

	}else if(Input::get('action') == "0"){

		$status['action'] = "deleting";

		/**get the actual sort of the member */

		$lastSort = DB::getInstance()->query('SELECT max(internal_order) as internal_order FROM 

			' . Input::get('filter') .  ' WHERE ' . Input::get('idName') . ' = ? AND ' . Input::get('newValueName') . ' 

			= ? AND deleted = ?', [Input::get('id'), Input::get('column'), 0]);

		/**delete it from DB */

		$markAsDeleted = DB::getInstance()->query('UPDATE ' . Input::get('filter') . ' SET 

			deleted = ? WHERE ' . Input::get('idName') . ' = ? AND ' . Input::get('newValueName') . ' = ?', 

			[1, Input::get('id'), Input::get('sid')]);

		$status['action'] = "deleting";

		$status['lastSortOption'] = $lastSort->first()->internal_order;

		/**select all the staff members  where the sorting is bigger than the deleted item*/

		$all = $db->query("SELECT * from " . Input::get('filter') . " 

			WHERE internal_order > ? AND " . Input::get('newValueName') . " = ? AND deleted = ?", 

			[$lastSort->first()->internal_order, Input::get('sid'), 0]);

		/**reduce the sorting for all of them*/

		foreach($all->results() as $result){

			$newOrder = $result->internal_order - 1;

			$id = $result->id;

			$fields = array('internal_order'=>$newOrder);

			$db->update(Input::get('filter'), $id, $fields);

		}

		printData(1, 'deleting');
	}

}

/**
* @function printData
* @description easy way to print only once all the json data for JS reading.
*
* @param {int} $dataId - id for the wording.
*/

function printData($dataId, $from){

	$status['status'] 		= 		$GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] 		= 		$GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] 			= 		$GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  		= 		$GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  		= 		$GLOBALS['wordingArray'][$dataId]->action_button_label;

	$status['action']  	= 		Input::get('action');
	
	$status['sid']  		= 		Input::get('sid');
	
	$status['column']  		= 		Input::get('column');
	
	$status['token']  		= 		Input::get('token');
	
	$status['from']  		= 		$from;

	echo json_encode($status);

	exit();
}



?>