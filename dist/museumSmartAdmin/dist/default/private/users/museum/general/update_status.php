<?php

/**
 * @summary Update the status or delete.
 *
 * @description -
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the requires */

require_once '../../core/init.php';

include 'UpdateCategoriesDates.class.php';

/**initialization */

$user = new User();

if(!$user->isLoggedIn()){

	Redirect::to('index.php');

}

$status['init'] = true;

$db = DB::getInstance();

$column = 'active';

/**if there is no data, print and out */

if(!Input::exists()) printData(0);


/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'id', 'required' => true),

	'table' => array('display'=> 'table', 'required' => true),

	'source' => array('display'=> 'source', 'required' => true),

	'status' => array('display'=> 'status', 'required' => true)));


/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0);

/**Select the wording according to the type of action...update or delete. */

else{

	$itemName = Input::get('name');

	switch (Input::get('source')) {

		case 'change_date_start':
		case 'change_date_end':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='institution_assisted'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = '...';

		$userString = '...';

		break;

		case 'institution_assisted' :

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='institution_assisted'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'Institution assisted '. $itemName .'  was changed.';

		$userString = 'Se cambió el estado de la asistencia de la institución ' . $itemName . '</b>';

		break;

		case 'institution_payment_done':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='institution_payment_done'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The payment status of '. $itemName .'  was changed.';

		$userString = 'Se cambió el estado del pago de la institución ' . $itemName . '</b>';

		break;


		case 'scholarship_asigned':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='scholarship_asigned'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The scholarship status of '. $itemName .'  was changed.';

		$userString = 'Cambiaste el estado de <b>' . $itemName . " respecto de beca a " .

		((Input::get('status') == 'true') ? "asignada." : "no asignada.") . '</b>';

		break;

		case 'uploaded_to_plataforma':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='uploaded_to_plataforma'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The plataforma status of the institution '. $itemName .'  was changed.';

		$userString = 'Cambiaste el estado de <b>' . $itemName . " respecto de Plataforma a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'attended':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_change_attendant_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The member '. $itemName .'  was .';

		$userString = 'Eliminaste a ' . $itemName . '</b>';

		break;

		case 'delete_booking_member':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_guide_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The member '. $itemName .'  was deleted.';

		$userString = 'Eliminaste a ' . $itemName . '</b>';

		break;

		case 'guides':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_guide_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the guide '. $itemName .'  was changed.';

		$userString = 'Cambiaste la visibilidad del guía <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		//...

		case 'guides':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_guide_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the guide '. $itemName .'  was changed.';

		$userString = 'Cambiaste la visibilidad del guía <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'books':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_books_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the book '. $itemName .'  was changed.';

		$userString = 'Cambiaste la visibilidad del libro <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'investigations':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_investigations_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the investigation '. $itemName .'  was changed.';

		$userString = 'Cambiaste la visibilidad de la investigación <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'heritage':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_heritage_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the object '. $itemName .'  was changed.';

		$userString = 'Cambiaste la visibilidad del objeto <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'quotes':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_quote_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the quote '. $itemName .'  was changed.';

		$userString = 'Cambiaste la visibilidad del frase  <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'sm_change_status':

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='sm_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the button '. $itemName .' was updated.';

		$userString = 'Cambiaste la visibilidad del botón <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;


		case 'home_menu':

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_opinion_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the button '. $itemName .' was updated.';

		$userString = 'Cambiaste la visibilidad del botón <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'home_modules':

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_opinion_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the module '. $itemName .' was updated.';

		$userString = 'Cambiaste la visibilidad del módulo <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;


		case 'alerts' :

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_alerts_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the alrt '. $itemName .'  was changed.';

		$userString = 'Cambiaste la visibilidad de la alerta <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;


		case 'events':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_events_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the event '. $itemName .'  was changed.';

		$userString = 'Cambiaste la visibilidad del evento <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;


		case 'news':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_news_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the news '. $itemName .'  was changed.';

		$userString = 'Cambiaste la visibilidad de la noticia <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'education':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_news_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the news '. $itemName .'  was changed.';

		$userString = 'Cambiaste la visibilidad de la noticia <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'courses_tutors':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_tutor_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the tutor '. $itemName .'  was changed.';

		$userString = 'Cambiaste la visibilidad del tutor <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'exhibitions':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_exhibition_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the exhibition '. $itemName .'  was changed.';

		$userString = 'Cambiaste la visibilidad de la muestra <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'courses':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_course_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the course '. $itemName .'  was changed.';

		$userString = 'Cambiaste la visibilidad del curso <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'objectives':

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_objective_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the objective '. $itemName .'  was changed.';

		$userString = 'Cambiaste la visibilidad del objetivo <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'faq':

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_faq_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the Shoa FAQ '. $itemName .'  was changed.';

		$userString = 'Cambiaste la visibilidad del FAQ de La Shoá <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'shoa_faq':

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_shoa_faq_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the Shoa FAQ '. $itemName .'  was added.';

		$userString = 'Cambiaste la visibilidad del FAQ de La Shoá <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'opinions':

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_opinion_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the opinion of '. $itemName .' image was added.';

		$userString = 'Cambiaste la visibilidad de la opinión de <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'staff':

		$category = Input::get('category');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_staff_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = "The visibility of the staff member " . $itemName . " inside the category " . $category . " was changed to " . ((Input::get('status') == 'true') ? "online." : "offline.");

		$userString = "Cambiaste la visibilidad de <b>" . $itemName . "</b> dentro de la categoría <b>" . $category . "</b> a <b>" . ((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'criminalsApp':

		$column = Input::get('defaultColumn');

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_criminal_change_status'");

		if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

		$systemString = 'The visibility of the state of '. $itemName .' criminal was changed.';

		$userString = 'Cambiaste la visibilidad del criminal <b>' . $itemName . " a " .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		$objUpdateCatDate = new UpdateCategoriesDates("criminales");

		$objUpdateCatDate->updateModifiedDate();


		break;

		case 'guetosApp':
			//CHECK ACTION TO UPODATE or DELeTe
			$column = Input::get('defaultColumn');

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_gueto_change_status'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The visibility of the state of '. $itemName .' gueto was changed.';

			$userString = 'Cambiaste la visibilidad del gueto <b>' . $itemName . " a " .

			((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

			$objUpdateCatDate = new UpdateCategoriesDates("guetos");

			$objUpdateCatDate->updateModifiedDate();

		break;

		case 'righteous':
			//CHECK ACTION TO UPODATE or DELeTe
			$column = Input::get('defaultColumn');

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_righteous_change_status'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The visibility of the state of '. $itemName .' righteous was changed.';

			$userString = 'Cambiaste la visibilidad de este justo <b>' . $itemName . " a " .

			((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

			$objUpdateCatDate = new UpdateCategoriesDates("justos");

			$objUpdateCatDate->updateModifiedDate();

		break;

		case 'survivorsApp':

			$column = Input::get('defaultColumn');

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_survivor_change_status'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The visibility of the state of '. $itemName .' survivor was changed.';

			$userString = 'Cambiaste la visibilidad de este sobreviviente <b>' . $itemName . " a " .

			((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

			$objUpdateCatDate = new UpdateCategoriesDates("sobrevivientes");

			$objUpdateCatDate->updateModifiedDate();

		break;

		case 'prensaApp':
			//CHECK ACTION TO UPODATE or DELeTe
			$column = Input::get('defaultColumn');

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_prensa_change_status'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The visibility of the state of '. $itemName .' survivor was changed.';

			$userString = 'Cambiaste la visibilidad de este sobreviviente <b>' . $itemName . " a " .

			((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

			$objUpdateCatDate = new UpdateCategoriesDates("prensa");

			$objUpdateCatDate->updateModifiedDate();

		break;

		case 'filmsApp':
			//CHECK ACTION TO UPODATE or DELeTe
			$column = Input::get('defaultColumn');

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_films_change_status'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The visibility of the state of '. $itemName .' film was changed.';

			$userString = 'Cambiaste la visibilidad de esta pelicula <b>' . $itemName . " a " .

			((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'random_facts':
			//CHECK ACTION TO UPODATE or DELeTe
			$column = Input::get('defaultColumn');

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_random_facts_change_status'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The visibility of the state of '. $itemName .' fact was changed.';

			$userString = 'Cambiaste la visibilidad de este hecho <b>' . $itemName . " a " .

			((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'trivias':
			//CHECK ACTION TO UPODATE or DELeTe
			$column = Input::get('defaultColumn');

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_trivia_change_status'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The visibility of the state of '. $itemName .' trivia was changed.';

			$userString = 'Cambiaste la visibilidad de esta trivia <b>' . $itemName . " a " .

			((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'mappingApp':

			$column = Input::get('defaultColumn');

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_mapping_change_status'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The visibility of the state of '. $itemName .' name in app Mapping was changed.';

			$userString = 'Cambiaste la visibilidad de este nombre en la app Mapping: <b>' . $itemName . " a " .

			((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'timeline_events':

			$column = Input::get('defaultColumn');

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_timeline_change_status'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = 'The visibility of the state of '. $itemName .' name in app timeline was changed.';

			$userString = 'Cambiaste la visibilidad de este nombre en la app timeline: <b>' . $itemName . " a " .

			((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';

		break;

		case 'survivors_project':

			$column = Input::get('defaultColumn');

			$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='app_survivors_project_change_status'");

			if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

			$systemString = "The visibility of the survivor " . $itemName . " was changed to " . ((Input::get('status') == 'true') ? "online." : "offline.");

			$userString = "Cambiaste la visibilidad de <b>" . $itemName . "</b> a <b>" . ((Input::get('status') == 'true') ? "online." : "offline.") . '</b>';


		default:

		break;

	}// Ends switch source

	$change = (Input::get('status') == 'true') ? 1 : 0;

	$updated = $db->update(Input::get('table'),Input::get('id'), array($column => $change));

	Logger::addLogData($user->data()->id, 'user',  $systemString,  $userString, Input::get('source'),Input::get('source'));

	printData(1);

}// Ends else valid

/**
* @function printData
* @description easy way to print only once all the json data for JS reading.
*
* @param {int} $dataId - id for the wording.
*/

function printData($dataId){

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	$status['changeStatus']  = Input::get('status');

	echo json_encode($status);

	exit();
}


?>
