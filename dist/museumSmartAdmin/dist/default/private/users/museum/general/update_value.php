<?php

/**
 * @summary General updater :)
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the requires */

require_once '../../core/init.php';

include 'UpdateCategoriesDates.class.php';

/**initialization */

$user = new User();

if(!$user->isLoggedIn()){

	Redirect::to('login.php');

}

$db = DB::getInstance();

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='update_db_value'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'input');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'id', 'required' => true),

	'fieldName' => array('display'=> 'fieldName', 'required' => true),

	//'newValue' => array('display'=> 'newValue', 'required' => true),

	'filter' => array('display'=> 'filter', 'required' => true)

), false);

if(!$validation->passed()) printData(0, 'validation');

else{

	$newValue = Input::get('newValue');

	if($newValue == 'NULL' || $newValue == 'null') $newValue = NULL;

	if(Input::get('fieldName') == 'description' || Input::get('fieldName') == 'observations' 

		|| Input::get('fieldName') == 'bio' || Input::get('fieldName') == 'content' || Input::get('fieldName') == 'caption'){

		$newValue = strip_tags($newValue, '<a><br><b><strong><i>');

	}


	if(!is_array(Input::get('fieldName'))){

		$query = $db->update(Input::get('filter'), Input::get('id'), array(

			Input::get('fieldName')=>$newValue));

	}else{

		$count = 0;

		foreach (Input::get('fieldName') as $value) {

			$query = $db->update(Input::get('filter'), Input::get('id'), array(

			$value=>Input::get('newValue')[$count]));

			$count+=1;
		
		}
	
	}

	if(Input::get('source') != NULL && Input::get('source') != ''){

		$objUpdateCatDate = new UpdateCategoriesDates(Input::get('source'));

		$objUpdateCatDate->updateModifiedDate();

	}


	printData(1, 'home');

}


/**
* @function printData
* @description easy way to print only once all the json data for JS reading.
*
* @param {int} $dataId - id for the wording.
*/

function printData($dataId, $from){

	$status['status'] 		= 		$GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] 		= 		$GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] 			= 		$GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  		= 		$GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  		= 		$GLOBALS['wordingArray'][$dataId]->action_button_label;

	$status['newValue']  	= 		Input::get('newValue');

	$status['source']		=		Input::get('source');
				
	$status['from']  		= 		$from;

	$status['test']  		= 		'testing online?';

	echo json_encode($status);

	exit();
}

?>