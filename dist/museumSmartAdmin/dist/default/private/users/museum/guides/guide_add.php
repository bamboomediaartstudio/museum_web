<?php

/**
 * @summary Add new guide to the DB
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0	
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../core/init.php';

require '../../../libraries/claviska/SimpleImage.php';

/**all the variables */

$filesManager = new FilesManager();

$ds = DIRECTORY_SEPARATOR;

$image = new \claviska\SimpleImage();

$allowProgramDownload = 0;

$status['init'] = true;

$db = DB::getInstance();

$retinaOriginalWidth = 2000;

$retinaOriginalHeight = 1000; 

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_guide_add'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');

/**if there is no token, print and out */

//if(!Token::check(Input::get('token'))) printData(0, 'token problem');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'name' => array('display'=> 'name', 'required' => true),

	'surname' => array('display'=> 'surname', 'required' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	if($user->isLoggedIn()){

		$db->insert('museum_guides',[

			'name'=> Input::get('name'),

			'surname'=> Input::get('surname'),

			'email'=> Input::get('email'),

			'te'=> Input::get('te')

		]);

		$lastId = $db->lastId();

		//save relations...

		foreach(Input::get('checkboxes') as $selected){

			$insert = $db->insert('museum_guides_languages_relations', 
				[
					'id_guide'=> $lastId,

					'id_language' => $selected

				]

			);

		}

		addLogData($user);

		printData(1, 'ok');

	}

}




/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The guide ' . Input::get('title') .  ' was added.';

	$userString = 'Agregaste un nuevo guía al sistema.';

	Logger::addLogData($user->data()->id, 

		'user',

		$systemString,

		$userString,

		'guide',

		'guide');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>