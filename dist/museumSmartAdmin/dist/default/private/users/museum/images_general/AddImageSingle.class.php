<?php

/**
 * @summary Add Single Image
 *
 * @description -
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo
 */

require '../../../../libraries/claviska/SimpleImage.php';

class AddImageSingle{

  private $_imgInputName = '';


  //Input image name
  public function __construct($imgInputName) {

     $this->_imgInputName = $imgInputName;

  }


  /**
   * @function loadImage
   * @param lastId - lastId -
   * @param folderName - path img
   * @description upload img multiple sizes
   */
  public function loadImage($lastId, $folderName){

      $filesManager = new FilesManager();

//      $ds = DIRECTORY_SEPARATOR;
      //
      $image = new \claviska\SimpleImage();

      $retinaOriginalWidth = 1000;

      $retinaOriginalHeight = 1000;

      $uniqueId = uniqid();

      $fileSize = 20971520; // Mb

      $itemFolder = '../../../../sources/images/'.$folderName.'/' . $lastId;

      $deleteFolder = '../../../../sources/images/'.$folderName.'/' . $lastId . '/delete';

      $fileFolder = $itemFolder . '/' . $uniqueId;

      $itemFolderExists = $filesManager->checkDirectory($itemFolder);

      if(!$itemFolderExists){

        $itemFolderCreated = $filesManager->makeDirectory($itemFolder);

        $deleteFolder = $filesManager->makeDirectory($deleteFolder);
      }

      $folderExists = $filesManager->checkDirectory($fileFolder);

      if(!$folderExists){

        $folderCreated = $filesManager->makeDirectory($fileFolder);

      }else{

        $folderCreated = true;
      }

      if($folderCreated){

        $checkPHPExtensions = $filesManager->checkPHPExtensions();
      }

      if($checkPHPExtensions){

        if (!empty($_FILES)) {

          //$tempFile = $_FILES['picture']['tmp_name'];
          $tempFile = $_FILES[$this->_imgInputName]['tmp_name'];

          if(is_file($tempFile)){

            $checkFileSize = $filesManager->checkFileSize($tempFile, $fileSize);

            if($checkFileSize){

              //$targetPath = dirname( __FILE__ ) . $ds . $fileFolder . $ds;
              $targetPath = dirname( __FILE__ ) .$fileFolder;

              $name = $lastId . '_' . $uniqueId;

              //$checkMimeType = $filesManager->checkImageMimeType($_FILES['picture']['tmp_name']);
              $checkMimeType = $filesManager->checkImageMimeType($_FILES[$this->_imgInputName]['tmp_name']);

              if($checkMimeType){

                //-----------------------------------------------------
                //general image settings...

                $newMime = 'image/jpeg';

                $newExtension = 'jpeg';

                //-----------------------------------------------------
                //the default image will be the retina image...

                $uploaded =  $targetPath . '/' . $name . '_uploaded.' . $newExtension;


                move_uploaded_file($tempFile, $uploaded);

                //-----------------------------------------------------
                //the normal image is the half of the retina...

                $originalFileRetina =  $targetPath . '/' . $name . '_original@2x.' . $newExtension;

                $image->fromFile($uploaded)->thumbnail($retinaOriginalWidth, $retinaOriginalHeight, $retinaOriginalHeight/2)->toFile($originalFileRetina, $newMime, 70);


                //-----------------------------------------------------
                //the normal image is the half of the retina...

                $originalFile =  $targetPath . '/' . $name . '_original.' . $newExtension;

                $image->fromFile($originalFileRetina)->resize($retinaOriginalWidth/2, $retinaOriginalHeight/2)->toFile($originalFile, $newMime, 70);


                //-----------------------------------------------------
                //the medium version for the normal...

                $mediumFile =  $targetPath . '/' . $name . '_medium.' . $newExtension;

                $image->fromFile($originalFileRetina)->resize($retinaOriginalWidth/4, $retinaOriginalHeight/4)->toFile($mediumFile, $newMime, 70);


                //-----------------------------------------------------
                //small

                $smallFile =  $targetPath . '/' . $name . '_small.' . $newExtension;

                $image->fromFile($originalFileRetina)->resize($retinaOriginalWidth/8, $retinaOriginalHeight/8)->toFile($smallFile, $newMime, 70);

                //......


                $imageUrlDb = $lastId."/".$uniqueId;

                // Return image path.
                return $imageUrlDb;

              }else{

                //printData(1, 'Mime type wrong');
                throw new Exception("Error. Check mime type", 1);

              }

            }else{

              throw new Exception("Error. Check file size", 2);

            }

          }else{

            //printData(1, 'No es un archivo');
            throw new Exception("Error. Check if is a file", 3);

          }

        }else{

          throw new Exception("Error. Empty, nothing to do", 4);

        }

      }else{

        throw new Exception("Error. Check extension", 5);

      }

    }// /.loadImage


    /**
     * @function updateDb
     * @param table -
     * @param lastId - lastId -
     * @param arrDbParams - key -> value (url img path)
     * @description Update db table in single images.
     */
    public function updateDb($table, $lastId, $arrDbParams=array()){

      $updateSql = DB::getInstance()->update($table, $lastId, $arrDbParams);

      if($updateSql){

        return 1;

      }else{

        throw new Exception("Error updating DB", 0);

      }

    }

} //Ends class

?>
