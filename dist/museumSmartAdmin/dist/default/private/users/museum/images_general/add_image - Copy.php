<?php

/**
 * @summary We add a new image for a staff member.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

/**all the includes */

require_once '../../core/init.php';

require '../../../libraries/claviska/SimpleImage.php';

/**all the variables */

$filesManager = new FilesManager();

$ds = DIRECTORY_SEPARATOR;

$image = new \claviska\SimpleImage();

$originalPrefix = '_original';

$squarePrefix = '_sq';

$defaultPrefix = '_default';

$status['init'] = true;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='add_image'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');

/**validate.. */

$user = new User();

if($user->isLoggedIn()){

	$validate = new Validate();

	$validation = $validate->check($_POST, array(

		'id' => array('display'=> 'id', 'required' => true)

	));

	/**if validation did not pass, print and out. Else is a valid verification. Continue */

	if(!$validation->passed()) printData(0, 'validation not passed');

	else{

		$uniqueId = uniqid();

		switch (Input::get('source')) {

			case 'exhibitions' : 
			
			$fileSize = Config::get('exhibitions/exhibitions_picture_max_size');

			$userFolder = Config::get('exhibitions/exhibitions_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('exhibitions/exhibitions_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'Course '. Input::get('courseName') .' image was added.';
			
			$userString = 'Agregaste la imagen al curso <b>' . Input::get('courseName') . '</b>';

			break;

			case 'courses':

			$fileSize = Config::get('courses/courses_picture_max_size');

			$userFolder = Config::get('courses/courses_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('courses/courses_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'Course '. Input::get('courseName') .' image was added.';
			
			$userString = 'Agregaste la imagen al curso <b>' . Input::get('courseName') . '</b>';

			break;

			case 'staff':

			$fileSize = Config::get('staff/staff_profile_picture_max_size');

			$userFolder = Config::get('staff/staff_profile_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('staff/staff_profile_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'Staff member '. Input::get('userName') .' image was added.';
			
			$userString = 'Agregaste la imagen del miembro del staff <b>' . Input::get('userName') . '</b>';

			break;

			case 'opinions':

			$fileSize = Config::get('opinions/opinions_picture_max_size');

			$userFolder = Config::get('opinions/opinions_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('opinions/opinions_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'The image of the opinion of  '. Input::get('userName') .' was added.';
			
			$userString = 'Agregaste la imagen a la opinión de <b>' . Input::get('userName') . '</b>';

			break;

			case 'testimonials':

			$fileSize = Config::get('testimonials/testimonials_picture_max_size');

			$userFolder = Config::get('testimonials/testimonials_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('testimonials/testimonials_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'The image of the testimony of  '. Input::get('userName') .' was added.';
			
			$userString = 'Agregaste la imagen al testimonio de <b>' . Input::get('userName') . '</b>';

			break;
			
			default:

			break;
		}

		$fileFolder = $userFolder . '/' . $uniqueId;

		$userFolderExists = $filesManager->checkDirectory($userFolder);

		if(!$userFolderExists){

			$userFolderCreated = $filesManager->makeDirectory($userFolder);

			$deleteFolder = $filesManager->makeDirectory($deleteFolder);

		}

		$folderExists = $filesManager->checkDirectory($fileFolder);

		if(!$folderExists){

			$folderCreated = $filesManager->makeDirectory($fileFolder);

		}else{

			$folderCreated = true;

		}

		if($folderCreated){

			$checkPHPExtensions = $filesManager->checkPHPExtensions();

		}

		//..

		if($checkPHPExtensions){

			if (!empty($_FILES)) {

				$tempFile = $_FILES['file']['tmp_name'];

				if(is_file($tempFile)){

					$checkFileSize = $filesManager->checkFileSize($tempFile, $fileSize);

					if($checkFileSize){

						$targetPath = dirname( __FILE__ ) . $ds . $fileFolder . $ds;

						$name = Input::get('id') . '_' . $uniqueId;

						$checkMimeType = $filesManager->checkImageMimeType($_FILES['file']['tmp_name']);

						if($checkMimeType){

								//-----------------------------------------------------
								//general image settings...

								$newMime = 'image/jpeg';

								$newExtension = 'jpeg';

								//-----------------------------------------------------
								//the default image will be the retina image...

								$retinaFile =  $targetPath . $name . '_original@2x.' . $newExtension;

								move_uploaded_file($tempFile, $retinaFile);

								//-----------------------------------------------------
								//the normal image is the half of the retina....

								$normalFile =  $targetPath . $name . '_original.' . $newExtension;

								$image->fromFile($retinaFile)->resize(null, 500)->toFile($normalFile, $newMime, 70);

								//-----------------------------------------------------
								//then we have a retina thumb.

								$retinaFileThumb =  $targetPath . $name . '_thumb@2x.' . $newExtension;

								$image->fromFile($normalFile)->resize(null, 250)

								->toFile($retinaFileThumb, $newMime, 70);

								//-----------------------------------------------------
								//and then we have a normal thumb.

								$normalThumb =  $targetPath . $name . '_thumb.' . $newExtension;

								$image->fromFile($normalFile)->resize(null, 125)

								->toFile($normalThumb, $newMime, 70);

							$q = DB::getInstance()->query("SELECT unique_id, id FROM museum_images 

								WHERE sid = ?  AND source = ? ", array(Input::get('id'), Input::get('source')));

							$c = $q->count();

							if($c < 1){

								DB::getInstance()->insert('museum_images', 

									array('sid' => Input::get('id'), 

										'source' => Input::get('source'),

										'path' => Input::get('source'),

										'unique_id' => $uniqueId, 

										'mimetype'=> $newMime)

								);

							}else{

								$previous = $q->first()->unique_id;

								$pre = $userFolder . '/' . $previous;

								$new = $deleteFolder . '/' . $previous;

								rename($pre, $new);

								DB::getInstance()->query('UPDATE museum_images 

									SET unique_id = ?, mimetype = ?, deleted = ? WHERE id = ? AND source = ?', 

									array($uniqueId, $newMime, 0, $q->first()->id, Input::get('source')));

								//...


							}

							//...

							Logger::addLogData($user->data()->id, 'user', $systemString, $userString, 

							Input::get('source'), 'image_edition');		

							//...		

							printData(1, 'ok');

						}else{

							printData(2, 'mimetype');

						}

					}else{

						printData(3, 'file size');

					}

				}else{

					printData(4, 'no file');

				}

			}else{

				printData(5, 'no file');
			}

		}else{

			printData(6, 'missing PHP library');

		}

	}

}

/**
* @function printData
* @description easy way to print only once all the json data for JS reading.
*
* @param {int} $dataId 	- id for the wording.
* @param {string} $from - for similar errors.
*/

function printData($dataId, $from){

	$status['id'] = Input::get('id');

	$status['source'] = Input::get('source');

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>