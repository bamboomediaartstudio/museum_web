<?php

/**
 * @summary We add a new image for a staff member.
 *
 * @description -
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

/**all the includes */

require_once '../../core/init.php';

require '../../../libraries/claviska/SimpleImage.php';

/**all the variables */

$filesManager = new FilesManager();

$ds = DIRECTORY_SEPARATOR;

$image = new \claviska\SimpleImage();

$originalPrefix = '_original';

$squarePrefix = '_sq';

$defaultPrefix = '_default';

$status['init'] = true;

$db = DB::getInstance();

$retinaOriginalWidth = 2000;

$retinaOriginalHeight = 1000;

//-----------------------------------------------------
//general image settings...

$newMime = 'image/jpeg';

$newExtension = 'jpeg';


/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='add_image'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');

/**validate.. */

$user = new User();

if($user->isLoggedIn()){

	$validate = new Validate();

	$validation = $validate->check($_POST, array(

		'id' => array('display'=> 'id', 'required' => true)

	));

	/**if validation did not pass, print and out. Else is a valid verification. Continue */

	if(!$validation->passed()) printData(0, 'validation not passed');

	else{

		$uniqueId = uniqid();

		switch (Input::get('source')) {

			case 'classes' :

			$fileSize = Config::get('classes/classes_picture_max_size');

			$userFolder = Config::get('classes/classes_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('classes/classes_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'a new book image was added.';

			$userString = 'Agregaste la imagen de un nuevo libro.';

			break;

			case 'books' :

			$newMime = 'image/png';

			$newExtension = 'png';

			$fileSize = Config::get('books/books_picture_max_size');

			$userFolder = Config::get('books/books_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('books/books_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'a new book image was added.';

			$userString = 'Agregaste la imagen de un nuevo libro.';

			break;


			case 'heritage' :

			$fileSize = Config::get('heritage/heritage_picture_max_size');

			$userFolder = Config::get('heritage/heritage_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('heritage/heritage_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'Heritage image was added.';

			$userString = 'Agregaste la imagen de un nuevo objeto.';

			break;

			case 'profile' :

			$fileSize = Config::get('profile/profile_picture_max_size');

			$userFolder = Config::get('profile/profile_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('profile/profile_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'Profile image was added.';

			$userString = 'Agregaste la imagen de perfil.';

			break;

			case 'events' :

			$fileSize = Config::get('events/events_picture_max_size');

			$userFolder = Config::get('events/events_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('events/events_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'Event image was added.';

			$userString = 'Agregaste la imagen de un evento.<b>';

			break;

			case 'news' :

			$fileSize = Config::get('news/news_picture_max_size');

			$userFolder = Config::get('news/news_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('news/news_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'News was added.';

			$userString = 'Agregaste la imagen de una noticia.<b>';

			break;

			case 'tutors' :

			$fileSize = Config::get('tutors/tutors_picture_max_size');

			$userFolder = Config::get('tutors/tutors_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('tutors/tutors_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'Tutor was added.';

			$userString = 'Agregaste la imagen de un tutor <b>';

			break;

			case 'exhibitions' :

			$fileSize = Config::get('exhibitions/exhibitions_picture_max_size');

			$userFolder = Config::get('exhibitions/exhibitions_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('exhibitions/exhibitions_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'Course '. Input::get('courseName') .' image was added.';

			$userString = 'Agregaste la imagen al curso <b>' . Input::get('courseName') . '</b>';

			break;

			case 'courses':

			$fileSize = Config::get('courses/courses_picture_max_size');

			$userFolder = Config::get('courses/courses_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('courses/courses_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'Course '. Input::get('courseName') .' image was added.';

			$userString = 'Agregaste la imagen al curso <b>' . Input::get('courseName') . '</b>';

			break;

			case 'staff':

			$fileSize = Config::get('staff/staff_profile_picture_max_size');

			$userFolder = Config::get('staff/staff_profile_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('staff/staff_profile_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'Staff member '. Input::get('userName') .' image was added.';

			$userString = 'Agregaste la imagen del miembro del staff <b>' . Input::get('userName') . '</b>';

			break;

			case 'opinions':

			$fileSize = Config::get('opinions/opinions_picture_max_size');

			$userFolder = Config::get('opinions/opinions_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('opinions/opinions_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'The image of the opinion of  '. Input::get('userName') .' was added.';

			$userString = 'Agregaste la imagen a la opinión de <b>' . Input::get('userName') . '</b>';

			break;

			case 'testimonials':

			$fileSize = Config::get('testimonials/testimonials_picture_max_size');

			$userFolder = Config::get('testimonials/testimonials_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('testimonials/testimonials_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'The image of the testimony of  '. Input::get('userName') .' was added.';

			$userString = 'Agregaste la imagen al testimonio de <b>' . Input::get('userName') . '</b>';

			break;
/*
			case 'survivors' :

			echo "estamos en survivors add_image !!";

			$fileSize = Config::get('survivors/news_picture_max_size');

			$userFolder = Config::get('survivors/news_picture_folder') .  Input::get('id');

			$deleteFolder = Config::get('survivors/news_picture_folder') .  Input::get('id') . '/delete';

			$systemString = 'News was added.';

			$userString = 'Agregaste la imagen de un SOBREVIVIENTE.<b>';

			break;*/

			default:

			break;
		}

		$fileFolder = $userFolder . '/' . $uniqueId;

		$userFolderExists = $filesManager->checkDirectory($userFolder);

		if(!$userFolderExists){

			$userFolderCreated = $filesManager->makeDirectory($userFolder);

			$deleteFolder = $filesManager->makeDirectory($deleteFolder);

		}

		$folderExists = $filesManager->checkDirectory($fileFolder);

		if(!$folderExists){

			$folderCreated = $filesManager->makeDirectory($fileFolder);

		}else{

			$folderCreated = true;

		}

		if($folderCreated){

			$checkPHPExtensions = $filesManager->checkPHPExtensions();

		}

		//..

		if($checkPHPExtensions){

			if (!empty($_FILES)) {

				$tempFile = $_FILES['main-image']['tmp_name'];

				if(is_file($tempFile)){

					$checkFileSize = $filesManager->checkFileSize($tempFile, $fileSize);

					if($checkFileSize){

						$targetPath = dirname( __FILE__ ) . $ds . $fileFolder . $ds;

						$name = Input::get('id') . '_' . $uniqueId;

						$checkMimeType = $filesManager->checkImageMimeType($_FILES['main-image']['tmp_name']);

						if($checkMimeType){



								//-----------------------------------------------------
								//the default image will be the retina image...

							$uploaded =  $targetPath . $name . '_uploaded.' . $newExtension;

							move_uploaded_file($tempFile, $uploaded);


								//-----------------------------------------------------
								//the normal image is the half of the retina...

							$originalFileRetina =  $targetPath . $name . '_original@2x.' . $newExtension;

							$image->fromFile($uploaded)->thumbnail($retinaOriginalWidth, $retinaOriginalHeight, $retinaOriginalHeight/2)->toFile($originalFileRetina, $newMime, 70);


								//-----------------------------------------------------
								//this is a square from the retina image

							$originalFileRetinaSq =  $targetPath . $name . '_original_sq@2x.' . $newExtension;

							$image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight, $retinaOriginalHeight, $retinaOriginalHeight/2)->toFile($originalFileRetinaSq, $newMime, 70);

								//-----------------------------------------------------
								//the normal image is the half of the retina...

							$originalFile =  $targetPath . $name . '_original.' . $newExtension;

							$image->fromFile($originalFileRetina)->resize(null, $retinaOriginalHeight/2)->toFile($originalFile, $newMime, 70);

								//-----------------------------------------------------
								//the normal image sq version

							$originalFileSq =  $targetPath . $name . '_original_sq.' . $newExtension;

							$image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight/2, $retinaOriginalHeight/2, $retinaOriginalHeight/4)->toFile($originalFileSq, $newMime, 70);

								//-----------------------------------------------------
								//the medium version for the retina...

							$mediumRetinaFile =  $targetPath . $name . '_medium@2x.' . $newExtension;

							$image->fromFile($originalFileRetina)->resize(null, $retinaOriginalHeight/2)->toFile($mediumRetinaFile, $newMime, 70);

								//-----------------------------------------------------
								//the medium sq version for the retina...

							$mediumRetinaFileSq =  $targetPath . $name . '_medium_sq@2x.' . $newExtension;

							$image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight/2, $retinaOriginalHeight/2, $retinaOriginalHeight/4)->toFile($mediumRetinaFileSq, $newMime, 70);

								//-----------------------------------------------------
								//the medium version for the normal...

							$mediumFile =  $targetPath . $name . '_medium.' . $newExtension;

							$image->fromFile($originalFileRetina)->resize(null, $retinaOriginalHeight/4)->toFile($mediumFile, $newMime, 70);

								//-----------------------------------------------------
								//the medium sq version for the retina...

							$mediumFileSq =  $targetPath . $name . '_medium_sq.' . $newExtension;

							$image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight/4, $retinaOriginalHeight/4, $retinaOriginalHeight/2)->toFile($mediumFileSq, $newMime, 70);

								//-----------------------------------------------------
								//small retina

							$smallRetinaFile =  $targetPath . $name . '_small@2x.' . $newExtension;

							$image->fromFile($originalFileRetina)->resize(null, $retinaOriginalHeight/4)->toFile($smallRetinaFile, $newMime, 70);

								//-----------------------------------------------------
								//the small sq version for the retina...

							$smallRetinaSq =  $targetPath . $name . '_small_sq@2x.' . $newExtension;

							$image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight/4, $retinaOriginalHeight/4, $retinaOriginalHeight/8)->toFile($smallRetinaSq, $newMime, 70);

								//-----------------------------------------------------
								//small

							$smallFile =  $targetPath . $name . '_small.' . $newExtension;

							$image->fromFile($originalFileRetina)->resize(null, $retinaOriginalHeight/8)->toFile($smallFile, $newMime, 70);

								//-----------------------------------------------------
								//the small sq version for the retina...

							$smallFileSq =  $targetPath . $name . '_small_sq.' . $newExtension;

							$image->fromFile($originalFileRetina)->thumbnail($retinaOriginalHeight/8, $retinaOriginalHeight/8, $retinaOriginalHeight/16)->toFile($smallFileSq, $newMime, 70);

								//......

							$q = DB::getInstance()->query("SELECT unique_id, id FROM museum_images

								WHERE sid = ?  AND source = ? ", array(Input::get('id'), Input::get('source')));

							$c = $q->count();

							if($c < 1){

								DB::getInstance()->insert('museum_images',

									array('sid' => Input::get('id'),

										'source' => Input::get('source'),

										'path' => Input::get('source'),

										'unique_id' => $uniqueId,

										'mimetype'=> $newMime)

								);

							}else{

								$previous = $q->first()->unique_id;

								$pre = $userFolder . '/' . $previous;

								$new = $deleteFolder . '/' . $previous;

								rename($pre, $new);

								DB::getInstance()->query('UPDATE museum_images

									SET unique_id = ?, mimetype = ?, deleted = ? WHERE id = ? AND source = ?',

									array($uniqueId, $newMime, 0, $q->first()->id, Input::get('source')));

								//...


							}

							//...

							Logger::addLogData($user->data()->id, 'user', $systemString, $userString,

								Input::get('source'), 'image_edition');

							//...

							printData(1, 'ok');

						}else{

							printData(2, 'mimetype');

						}

					}else{

						printData(3, 'file size');

					}

				}else{

					printData(4, 'no file');

				}

			}else{

				printData(5, 'no file');
			}

		}else{

			printData(6, 'missing PHP library');

		}

	}

}

/**
* @function printData
* @description easy way to print only once all the json data for JS reading.
*
* @param {int} $dataId 	- id for the wording.
* @param {string} $from - for similar errors.
*/

function printData($dataId, $from){

	$status['id'] = Input::get('id');

	$status['source'] = Input::get('source');

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>
