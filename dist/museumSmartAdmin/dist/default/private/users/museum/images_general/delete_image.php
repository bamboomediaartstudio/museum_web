<?php

/**
 * @summary Delete image
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the initializations */

require_once '../../core/init.php';

$user = new User();

if(!$user->isLoggedIn()){

	//exit?

}


$db = DB::getInstance();

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='delete_image'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

if(!Input::exists()) printData(0, 'missing input');

/**validate? */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'id', 'required' => true),
	
	'source' => array('display'=> 'source', 'required' => true)
)

);

if(!$validation->passed()) printData(0, 'validation not passed');

/**if is valid, delete value... */

else{

	$query = 'UPDATE museum_images SET deleted = ? WHERE sid = ? AND source = ?';

	DB::getInstance()->query($query, [1, input::get('id'), input::get('source')]);

	//echo 'no va';

	//return;

	/**we ll have to filter the source for notifications... */

	switch (Input::get('source')) {

		case 'tutors' : 

		$systemString = 'The image of the tutor was deleted.';

		$userString = 'Eliminaste la imagen del tutor';

		break;


		case 'exhibitions' : 

		$systemString = 'The image of the exhibition '. Input::get('exhibitionName') .'  was deleted.';

		$userString = 'Eliminaste la imagen de la muestra <b>' . Input::get('exhibitionName') . '.';


		break;

		case 'courses':

		$systemString = 'The image of the couruse '. Input::get('courseName') .'  was deleted.';

		$userString = 'Eliminaste la imagen del curso <b>' . Input::get('courseName') . '.';

		break;

		case 'staff':

		$systemString = 'Staff member '. Input::get('userName') .' image was deleted.';

		$userString = 'Eliminaste la imagen del miembro del staff <b>' . Input::get('userName') . '.';

		break;

		case 'opinions':

		$systemString = 'The image of '. Input::get('userName') .' was deleted from the opinions list.';

		$userString = 'Eliminaste la imagen imagen de <b>' . Input::get('userName') . '</b> de la lista de opiniones.';

		break;

		case 'testimonials':

		$systemString = 'The image of '. Input::get('userName') .' was deleted from the testimonials list.';

		$userString = 'Eliminaste la imagen imagen de <b>' . Input::get('userName') . '</b> de la lista de testimonios.';

		break;
		
		default:

		break;
	}

	/**get id of image... */

	$mk = DB::getInstance()->query(

		'SELECT id FROM museum_images WHERE sid = ? AND source = ?', 

		[input::get('id'), input::get('source')]);

	/**reset cropbox... */

	DB::getInstance()->query(

		'UPDATE museum_image_crop_box SET top = ?, `left` = ?, width = ?, height = ? WHERE id = ?', 

		[NULL, NULL, NULL, NULL, $mk->first()->id]

	);

	/**reset canvas... */

	DB::getInstance()->query(

		'UPDATE museum_image_canvas_data SET top = ?, `left` = ?, width = ?, height = ?, natural_width = ?, natural_height = ? WHERE id = ?', 

		[NULL, NULL, NULL, NULL, NULL, NULL, $mk->first()->id]

	);

	/**reset image data... */

	DB::getInstance()->query(

		'UPDATE museum_image_data SET top = ?, `left` = ?, width = ?, height = ?, natural_width = ?, 

		natural_height = ?, rotate = ?, scale_x = ?, scale_y = ?, aspect_ratio = ? WHERE id = ?', 

		[NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, $mk->first()->id]

	);

	/**reset filters... */

	DB::getInstance()->query(

		'UPDATE museum_image_filters SET is_grey_scale = ?, is_custom = ?, brightness = ?, contrast = ?, saturation = ?, sharpen = ?, vibrance = ?, exposure = ? WHERE id = ?', 

		[NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, $mk->first()->id]

	);

	Logger::addLogData($user->data()->id, 'user', $systemString, $userString, Input::get('source'), 'image_edition');		
	printData(1, '');

}

/**
* @function printData
* @description easy way to print only once all the json data for JS reading.
*
* @param {int} $dataId 	- id for the wording.
* @param {string} $from - for similar errors.
*/


function printData($dataId, $from){

	$status['id'] = Input::get('id');

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}



?>