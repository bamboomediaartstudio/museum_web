<?php

/**
 * @summary Get all the data for an image.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

require_once '../../core/init.php';

/** we ll save the wording query for this instance */

$wordingQuery = $db = DB::getInstance()->query(

	"Select * FROM ajax_responses_wording WHERE form_type='get_image_data'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

if(!Input::exists()) printData(0, 'there is no input');

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'id', 'required' => true),

	'imageUniqueId' => array('display'=> 'imageUniqueId', 'required' => true))

);

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	if($user->isLoggedIn()){

		$query = 

		DB::getInstance()->query('SELECT id FROM museum_images WHERE sid = ? AND unique_id = ?', 

			[Input::get('id'), Input::get('imageUniqueId')]);

		$line = $query->first()->id;

		$boxData = DB::getInstance()->query('SELECT * FROM museum_image_crop_box WHERE id=?', 

			[$line])->first();

		$imageData = DB::getInstance()->query('SELECT * FROM museum_image_data WHERE id=?', 

			[$line])->first();

		$canvasData = DB::getInstance()->query('SELECT * FROM museum_image_canvas_data WHERE id=?', 

			[$line])->first();

		$filtersData = DB::getInstance()->query('SELECT * FROM museum_image_filters WHERE id=?', 

			[$line])->first();

		printData(1, 'soy oki', $line, $boxData, $imageData, $filtersData, $canvasData);

	}

}

function printData($dataId, $from, $line, $boxData, $imageData, $filtersData, $canvasData){

	$status['line'] = $line;

	$status['boxData'] = $boxData;

	$status['imageData'] = $imageData;

	$status['canvasData'] = $canvasData;

	$status['filtersData'] = $filtersData;

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>