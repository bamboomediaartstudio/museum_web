<?php

require_once '../core/init.php';

$status['init'] = true;

$db = DB::getInstance();

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_institutional'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

if(!Input::exists()) printData(0);

if(!Token::check(Input::get('token'))) printData(0);

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'mission' => array('display'=> 'mision', 'required' => true),

	'vision' => array('display'=> 'vision', 'required' => true),

	'about' => array('display'=> 'about', 'required' => true),

	'values' => array('display'=> 'values', 'required' => true)

)

);

if(!$validation->passed()) printData(0);

else{

	$musuemData =$db->query("SELECT * FROM museum_institutional");

	$db->update('museum_institutional',

		$musuemData->first()->id,

		['mission' => strip_tags(Input::get('mission'), '<a><br><b><strong><i>'),

		'vision' => strip_tags(Input::get('vision'), '<a><br><b><strong><i>'),

		'about' => strip_tags(Input::get('about'), '<a><br><b><strong><i>'),

		'values' => strip_tags(Input::get('values'), '<a><br><b><strong><i>')

	]);

	printData(1);
}


function printData($dataId){

	$status['token'] = Input::get('token');
	
	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

echo json_encode($status);


?>