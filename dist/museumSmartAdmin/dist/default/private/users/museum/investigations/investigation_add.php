<?php

/**
 * @summary Add new investigation
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0	
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../core/init.php';

require '../../../libraries/claviska/SimpleImage.php';

/**all the variables */

$filesManager = new FilesManager();

$ds = DIRECTORY_SEPARATOR;

$image = new \claviska\SimpleImage();

$allowProgramDownload = 0;

$status['init'] = true;

$db = DB::getInstance();

$retinaOriginalWidth = 2000;

$retinaOriginalHeight = 1000; 

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_investigation_add'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'title' => array('display'=> 'title', 'required' => true),

	'url' => array('display'=> 'url', 'required' => true))

	//'description' => array('display'=> 'description', 'required' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$addDescription = strip_tags(Input::get('description'), '<a><br><b><strong><i>');

	if($addDescription == '<br>') $addDescription = ''; 

	$user = new User();

	if($user->isLoggedIn()){

		$lastOrder = $db->query("SELECT MAX(internal_order) as internal_order FROM museum_investigations 

				WHERE deleted = ?", array(0));

			$item = $lastOrder->first();

			$newOrder = $item->internal_order+=1;

		$db->insert('museum_investigations',[

			'title'=> Input::get('title'),

			'url'=> Input::get('url'),

			'description'=> $addDescription,

			'added' => date('Y-m-d H:i:s'),

			'internal_order' => $newOrder

		]);

		$lastId = $db->lastId();

		//save PDF program...

		if(!file_exists($_FILES['investigation-pdf']['tmp_name']) || 

			!is_uploaded_file($_FILES['investigation-pdf']['tmp_name'])){

			$allowProgramDownload = 0;

		}else{

			$pdfFolder = '../../../sources/pdf/investigations/' . $lastId . '/pdf';

			$pdfFile = $pdfFolder .  '/' . Input::get('url') . '.pdf';

			$pdfFolder = $filesManager->makeDirectory($pdfFolder);

			$finfo = finfo_open(FILEINFO_MIME_TYPE);

			$mime = finfo_file($finfo, $_FILES['investigation-pdf']['tmp_name']);

			if ($mime == 'application/pdf') {

				if(move_uploaded_file($_FILES['investigation-pdf']['tmp_name'], $pdfFile)) {

					$allowProgramDownload = 1;

					printData(1, 'ok');
					
				}

			}

		}

	}

}


/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The investigation ' . Input::get('title') .  ' was added.';

	$userString = 'Agregaste una nueva investigación al sistema.';

	Logger::addLogData($user->data()->id, 

		'user',

		$systemString,

		$userString,

		'investigations',

		'investigations');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>