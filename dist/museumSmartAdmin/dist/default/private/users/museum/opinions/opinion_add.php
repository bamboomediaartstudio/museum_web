<?php

/**
 * @summary Add new opinion!
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

/**all the includes */

require_once '../../core/init.php';

require '../../../libraries/claviska/SimpleImage.php';

/**all the variables */

$filesManager = new FilesManager();

$ds = DIRECTORY_SEPARATOR;

$image = new \claviska\SimpleImage();

$originalPrefix = '_original';

$squarePrefix = '_sq';

$defaultPrefix = '_default';

$status['init'] = true;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_add_opinion'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'name' => array('display'=> 'name', 'required' => true),

	'quote' => array('display'=> 'quote', 'required' => true),

	'role' => array('display'=> 'role', 'required' => true),

	'company' => array('display'=> 'company', 'required' => true),

	'surname' => array('display'=> 'surname', 'required' => true)));

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	if($user->isLoggedIn()){

		$lastOrder = $db->query("SELECT MAX(internal_order) as internal_order FROM museum_opinions 

				WHERE deleted = ?", array(0));

			$item = $lastOrder->first();

			$newOrder = $item->internal_order+=1;


		$db->insert('museum_opinions',[

			'name'=> Input::get('name'),

			'surname'=> Input::get('surname'),

			'role'=> Input::get('role'),

			'company'=> Input::get('company'),

			'quote'=> Input::get('quote'),

			'internal_order' => $newOrder
		]);

		$lastId = $db->lastId();

		if($_FILES['file']['name'] == "blob" && $_FILES['file']['type'] == "application/octet-stream" && $_FILES['file']['size'] == 0) {

			addLogData($user);

			printData(1, 'ok without image');

		}else{

			$uniqueId = uniqid();

			$fileSize = Config::get('opinions/opinions_picture_max_size');

			$userFolder = Config::get('opinions/opinions_picture_folder') .  $lastId;

			$deleteFolder = Config::get('opinions/opinions_picture_folder') .  $lastId . '/delete';

			$fileFolder = $userFolder . '/' . $uniqueId;

			$userFolderExists = $filesManager->checkDirectory($userFolder);

			if(!$userFolderExists){

				$userFolderCreated = $filesManager->makeDirectory($userFolder);

				$deleteFolder = $filesManager->makeDirectory($deleteFolder);

			}

			$folderExists = $filesManager->checkDirectory($fileFolder);

			if(!$folderExists){

				$folderCreated = $filesManager->makeDirectory($fileFolder);
				
			}else{

				$folderCreated = true;

			}

			if($folderCreated){

				$checkPHPExtensions = $filesManager->checkPHPExtensions();
			}

			if($checkPHPExtensions){

				if (!empty($_FILES)) {

					$tempFile = $_FILES['file']['tmp_name'];

					if(is_file($tempFile)){

						$checkFileSize = $filesManager->checkFileSize($tempFile, $fileSize);

						if($checkFileSize){

							$targetPath = dirname( __FILE__ ) . $ds . $fileFolder . $ds;

							$name = $lastId . '_' . $uniqueId;

							$checkMimeType = $filesManager->checkImageMimeType($_FILES['file']['tmp_name']);

							if($checkMimeType){

								$newMime = 'image/jpeg';

								$newExtension = 'jpeg';

								$targetFile =  $targetPath . $name . $originalPrefix  . '.' . $newExtension;

								$targetFileSQ =  $targetPath . $name . $squarePrefix . '.' . $newExtension;

								$targetFileDefault =  $targetPath . $name . $defaultPrefix . '.' . $newExtension;

								move_uploaded_file($tempFile, $targetFile);

								$image->fromFile($targetFile)->thumbnail(200, 200, 100)

								->toFile($targetFileSQ, $newMime, 100);

								//first i have to validate if the image exists.

								$q = DB::getInstance()->query('SELECT unique_id, id FROM museum_images WHERE sid = ? AND source = ?', array($lastId, 'opinions'));

								$c = $q->count();
								
								if($c < 1){

									DB::getInstance()->insert('museum_images', 

										array('sid'=>$lastId, 'unique_id'=>$uniqueId, 'mimetype'=>$newMime, 'source'=>'opinions', 'path'=>'opinions'));

								
									$lastPictureId = DB::getInstance()->lastId();

									DB::getInstance()->insert('museum_image_crop_box',

										array('id'=>$lastPictureId));

									DB::getInstance()->insert('museum_image_data',

										array('id'=>$lastPictureId));

									DB::getInstance()->insert('museum_image_filters',

										array('id'=>$lastPictureId));

									DB::getInstance()->insert('museum_image_canvas_data',

										array('id'=>$lastPictureId));
									

									addLogData($user);

								}else{

									//...?

								}

								printData(1, '1');

							}else{

								printData(4, '4');

							}

						}else{

							printData(3, '3');

						}

					}else{

						printData(2, '2');

					}

				}

			}

		}

	}else{ }

}

/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 		user object.
*/

function addLogData($user){

	$systemString = 'The opinion of ' . Input::get('name') . ' ' . Input::get('surname') . ' was added.';

	$userString = 'Agregaste la opinión de <b>' . Input::get('name') . ' ' . Input::get('surname') . '</b> al sistema.';

	Logger::addLogData($user->data()->id, 

		'user',

		$systemString,

		$userString,

		'opinion',

		'opinion');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['name'] = Input::get('name');

	$status['surname'] = Input::get('surname');

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>