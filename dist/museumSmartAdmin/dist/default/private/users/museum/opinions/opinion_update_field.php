<?php

/**
 * @summary update opinion.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the requires */

require_once '../../core/init.php';

/**initialization */

$user = new User();

if(!$user->isLoggedIn()){

	Redirect::to('index.php');

}

$db = DB::getInstance();

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='update_db_value'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'input');

/**if there is no token, print and out */

//if(!Token::check(Input::get('token'))) printData(0, 'token');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'id', 'required' => true),

	'newValue' => array('display'=> 'newValue', 'required' => true),

	'dataType' => array('display'=> 'dataType', 'required' => true),

	'sid' => array('display'=> 'sid', 'required' => true),

	'action' => array('display'=> 'action', 'required' => true),

	'column' => array('display'=> 'column', 'required' => true)

), false);

if(!$validation->passed()) printData(0, 'validation');

else{

	switch (Input::get('sid')) {

		case 0:

		$table = 'museum_opinions';

		$query = $db->update($table, Input::get('id'), array(Input::get('column')=>Input::get('newValue')));

		printData(1, 'home');

		break;
	}
}

/**
* @function printData
* @description easy way to print only once all the json data for JS reading.
*
* @param {int} $dataId - id for the wording.
*/

function printData($dataId, $from){

	$status['status'] 		= 		$GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] 		= 		$GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] 			= 		$GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  		= 		$GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  		= 		$GLOBALS['wordingArray'][$dataId]->action_button_label;

	$status['newValue']  	= 		Input::get('newValue');
	
	$status['sid']  		= 		Input::get('sid');
	
	$status['column']  		= 		Input::get('column');
	
	$status['token']  		= 		Input::get('token');
	
	$status['from']  		= 		$from;

	echo json_encode($status);

	exit();
}



?>