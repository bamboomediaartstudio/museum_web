<?php

/**
 * @summary Add new Random facts to the DB - General Use
 *
 * @description -
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../core/init.php';

include '../general/UpdateCategoriesDates.class.php';

/**all the variables */


$allowProgramDownload = 0;

$status['init'] = true;

$db = DB::getInstance();


/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_random_facts_add'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');


/**initialize validation process */

$validate = new Validate();


$validation = $validate->check($_POST, array(

  'description' => array('display' => 'description', 'required' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(1, 'validation not passed');

else{

  $user = new User();

  if($user->isLoggedIn()){

    $arrayQuery = [

      'description' => Input::get('description'),

      'active' => 1,

      'deleted'  => 0

    ];


    $insertDbQuery = $db->insert('museum_random_facts', $arrayQuery);

    if($insertDbQuery){

        $lastId = $db->lastId();

        //First add categories, then feedback options to show to the final user

        if(addCategoriesRelations($lastId, Input::get('checkboxes')) == 1){

          addFeedbackOptionsRelations($lastId, Input::get('checkboxesFeedback'));

        }



    }else{

      //No se cargaron datos del formulario de manera exitosa. No cargar imagenes.
      printData(0, 'Error, no se cargaron datos del formulario');

    }


  }


}

function addCategoriesRelations($lastFactId, $arrCategories){

    $totalCategoriesToAdd = count($arrCategories);

    $totalCategoriesAdded = 0;

		foreach($arrCategories as $selected){

			$insert = DB::getInstance()->insert('museum_random_facts_categories_relations',
				[
					'id_fact'     => $lastFactId,

					'id_category' => $selected,

          'active'      => 1,

          'deleted'     => 0

				]

			);

      if($insert){
          $totalCategoriesAdded++;
      }

		}

    //Checkear que todas las categorias se hayan agregado a la db

    if($totalCategoriesToAdd == $totalCategoriesAdded){

      return 1;

    }else{

      printData(1, 'Error al cargar categrias de random facts.');

    }

}

function addFeedbackOptionsRelations($lastFactId, $arrFeedbackOptions){

  $totalOptionsToAdd = count($arrFeedbackOptions);

  $totalOptionsAdded = 0;

  foreach($arrFeedbackOptions as $selected){

    $insert = DB::getInstance()->insert('museum_random_facts_feedback_show_relations',
      [
        'id_fact'             => $lastFactId,

        'id_feedback_option'  => $selected,

        'active'              => 1,

        'deleted'             => 0

      ]

    );

    if($insert){
        $totalOptionsAdded++;
    }

    /*$selectData = DB::getInstance()->query('SELECT * from app_museum_categories WHERE id = ?', [$selected]);

    if($selectData) $result = $selectData->first();

      $objUpdateCatDate = new UpdateCategoriesDates($result->alt_source);

      $objUpdateCatDate->updateModifiedDate();*/

  }

  //Checkear que todas las categorias se hayan agregado a la db

  if($totalOptionsToAdd == $totalOptionsAdded){

    printData(0, 'Datos guardados correctamente');

  }else{

    printData(1, 'Error al cargar feedback options a mostrar de random facts.');

  }

}



/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The Random fact ' . Input::get('name') .  ' was added.';

	$userString = 'Agregaste el random fact <b>' . ' ' . '</b> al sistema.';

	Logger::addLogData($user->data()->id,

		'user',

		$systemString,

		$userString,

		'random_facts',

		'random_facts');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>
