<?php

/**
 * @summary
 *
 * @description -
 *
 * @author Mariano Makedonsky / Colo Baggins <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the includes */

require_once '../../core/init.php';

/**validate.. */


$user = new User();

$db = DB::getInstance();

if($user->isLoggedIn()){

	$validate = new Validate();

	$validation = $validate->check($_POST, array(

			'id' => array('display'=> 'id', 'required' => true),

			'action' => array('display'=> 'action', 'required' => true)
		)
	);


}



if($validation->passed()){

	if(Input::get('action') == 'updateCategories'){

    $arrCategories = Input::get('arrOptionsChecked');

    //Primero borrar las relaciones con este id Film
    $deleteOlderRelations = $db->delete('museum_random_facts_categories_relations', ['id_fact'=> Input::get('id')]);

    //Luego agregar los valores de del arrCategories en columna id_category
    if($arrCategories){

      $totalCatsToAdd = count($arrCategories);

      $totalAdded = 0;

      foreach($arrCategories as $selected){

        $insert = $db->insert('museum_random_facts_categories_relations',
          [
            'id_fact'     => Input::get('id'),

            'id_category' => $selected,

            'active'      => 1,

            'deleted'     => 0

          ]

        );

				if($insert){

						$totalAdded++;

				}


      }

      if($totalCatsToAdd == $totalAdded){

        $status['msg'] = 'Se actualizaron las categorias';

  			$status['button'] = 'Entendido';

      }else{

        $status['msg'] = 'Error al actualizar categorias. Una o mas categorias no se actualizaron.';

  			$status['button'] = 'Ok';

      }

    }

		echo json_encode($status);

	}else if(Input::get('action') == 'updateFeedback'){	//Para cuando venga lo que selecciono el usuario para guardar como stat



		updateFeedback(Input::get('id'), Input::get('arrFeedback'));


	}else if(Input::get('action') == 'updateFeedbackOptions'){

		$arrFeedbackOptions = Input::get('arrOptionsChecked');

    //Primero borrar las relaciones con este id Film
    $deleteOlderOptionsRelations = $db->delete('museum_random_facts_feedback_show_relations', ['id_fact'=> Input::get('id')]);

    //Luego agregar los valores de del arrCategories en columna id_category
    if($arrFeedbackOptions){

      $totalOptsToAdd = count($arrFeedbackOptions);

      $totalAdded = 0;

      foreach($arrFeedbackOptions as $selected){

        $insert = $db->insert('museum_random_facts_feedback_show_relations',
          [
            'id_fact'     				=> Input::get('id'),

            'id_feedback_option' 	=> $selected,

            'active'      				=> 1,

            'deleted'     				=> 0

          ]

        );

				if($insert){

						$totalAdded++;

				}


      }

      if($totalOptsToAdd == $totalAdded){

        $status['msg'] = 'Se actualizaron las opciones de feedback posibles para el usuario final';

  			$status['button'] = 'Entendido';

      }else{

        $status['msg'] = 'Error al actualizar las opciones de feedback para el usuario final. Una o mas opciones no se actualizaron.';

  			$status['button'] = 'Ok';

      }

    }

		echo json_encode($status);

	}

}

/**
* @function updateFeedback
* @description save feedback from user to random facts
*
* @param {array} $arrFeedback 	- 	 array las opciones (ids) tildadas / marcadas por el usuario en el front
*/

function updateFeedback($idFact, $arrFeedback){

	//Loop through feedback options
	if($arrFeedback){

		$totalOptsToAdd = count($arrFeedback);

		$totalAdded = 0;

		foreach($arrFeedback as $selected){

			$insert = $db->insert('museum_random_facts_feedback_relations',
				[
					'id_fact'     => $idFact,

					'id_feedback' => $selected,

					'active'      => 1,

					'deleted'     => 0

				]

			);

			if($insert){

					$totalAdded++;

			}


		}

		if($totalOptsToAdd == $totalAdded){

			$status['msg'] = 'Se guardó el feedback';

			$status['button'] = 'Entendido';

		}else{

			$status['msg'] = 'Error al actualizar Feedback.';

			$status['button'] = 'Ok';

		}
	}

}



?>
