<?php

/**
 * @summary Add new shoa faq.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the includes */

require_once '../../core/init.php';

/**all the variables */

$status['init'] = true;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_add_shoa_faq'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'question' => array('display'=> 'question', 'required' => true),

	'answer' => array('display'=> 'answer', 'required' => true)));

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	if($user->isLoggedIn()){

		$lastOrder = $db->query("SELECT MAX(internal_order) as internal_order FROM museum_shoa_faq 

				WHERE deleted = ?", array(0));

			$item = $lastOrder->first();

			$newOrder = $item->internal_order+=1;


		$db->insert('museum_shoa_faq',[

			'question'=> Input::get('question'),

			'answer'=> strip_tags(Input::get('answer'), '<a><br><b><strong><i>'),

			'internal_order' => $newOrder
		]);

		$lastId = $db->lastId();

		addLogData($user);

		printData(1, 'ok');

	}else{ }

}

/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 		user object.
*/

function addLogData($user){

	$systemString = 'a new FAQ was added to the Shoa page.';

	$userString = 'Agregaste una FAQ a la sección de <b>La Shoá</b>.';

	Logger::addLogData($user->data()->id, 

		'user',

		$systemString,

		$userString,

		'shoa_faq',

		'shoa_faq');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>