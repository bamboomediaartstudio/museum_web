<?php

require_once '../core/init.php';

$status['init'] = true;

$db = DB::getInstance();

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_general_social_media'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

if(!Input::exists()) printData(0);

if(!Token::check(Input::get('token'))) printData(1);

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'facebook' => array('display'=> 'url', 'url' => true),

	'instagram' => array('display'=> 'url', 'url' => true),
	
	'twitter' => array('display'=> 'url', 'url' => true),

	'linkedin' => array('display'=> 'url', 'url' => true),

	'youtube' => array('display'=> 'url', 'url' => true),

	'vimeo' => array('display'=> 'url', 'url' => true),

	'flickr' => array('display'=> 'url', 'url' => true)

));

if(!$validation->passed()) printData(2);

else{

	$museumSocialMedia =$db->query("SELECT * FROM museum_social_media");

	$db->update('museum_social_media',

		$museumSocialMedia->first()->id,

		['facebook' => Input::get('facebook'),

		'instagram' => Input::get('instagram'),

		'twitter' => Input::get('twitter'),

		'youtube' => Input::get('youtube'),

		'vimeo' => Input::get('vimeo'),

		'flickr' => Input::get('flickr'),

		'linked_in' => Input::get('linkedin')]);

	printData(3);
}


function printData($dataId){

	$status['token'] = Input::get('token');
	
	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

echo json_encode($status);


?>