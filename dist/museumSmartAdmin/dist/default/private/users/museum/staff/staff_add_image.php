<?php

/**
 * @summary We add a new image for a staff member.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

/**all the includes */

require_once '../../core/init.php';

require '../../../libraries/claviska/SimpleImage.php';

/**all the variables */

$filesManager = new FilesManager();

$ds = DIRECTORY_SEPARATOR;

$image = new \claviska\SimpleImage();

$originalPrefix = '_original';

$squarePrefix = '_sq';

$defaultPrefix = '_default';

$status['init'] = true;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='add_museum_staff_image'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');

/**validate.. */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'id', 'required' => true)

));

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$uniqueId = uniqid();

	$fileSize = Config::get('staff/staff_profile_picture_max_size');

	$userFolder = Config::get('staff/staff_profile_picture_folder') .  Input::get('id');

	$deleteFolder = Config::get('staff/staff_profile_picture_folder') .  Input::get('id') . '/delete';

	$fileFolder = $userFolder . '/' . $uniqueId;

	$userFolderExists = $filesManager->checkDirectory($userFolder);

	if(!$userFolderExists){

		$userFolderCreated = $filesManager->makeDirectory($userFolder);

		$deleteFolder = $filesManager->makeDirectory($deleteFolder);

	}

	$folderExists = $filesManager->checkDirectory($fileFolder);

	if(!$folderExists){

		$folderCreated = $filesManager->makeDirectory($fileFolder);

	}else{

		$folderCreated = true;

	}

	if($folderCreated){

		$checkPHPExtensions = $filesManager->checkPHPExtensions();

	}

	//..

	if($checkPHPExtensions){

		if (!empty($_FILES)) {

			$tempFile = $_FILES['file']['tmp_name'];

			if(is_file($tempFile)){

				$checkFileSize = $filesManager->checkFileSize($tempFile, $fileSize);

				if($checkFileSize){

					$targetPath = dirname( __FILE__ ) . $ds . $fileFolder . $ds;

					$name = Input::get('id') . '_' . $uniqueId;

					$checkMimeType = $filesManager->checkImageMimeType($_FILES['file']['tmp_name']);

					if($checkMimeType){

						//hardcoding:

						$extension = 'jpeg';

						$mime = 'image/jpeg';

						//$extension = $filesManager->getFileExtensionByMimeType($filesManager->getMimeType());

						$targetFile =  $targetPath . $name . $originalPrefix  . '.' . $extension;

						$targetFileSQ =  $targetPath . $name . $squarePrefix . '.' . $extension;

						$targetFileDefault =  $targetPath . $name . $defaultPrefix . '.' . $extension;

						move_uploaded_file($tempFile, $targetFile);

						$image->fromFile($targetFile)->thumbnail(200, 200, 100)

						->toFile($targetFileSQ, $mime, 100);		

						//...

						$q = DB::getInstance()->query("SELECT unique_id, id FROM museum_images WHERE sid = ?", array(Input::get('id')));

						$c = $q->count();

						if($c < 1){

							DB::getInstance()->insert('museum_images', 

								array('sid' => Input::get('id'), 

									'source' => 'staff',

									'path' => 'staff',

									'unique_id' => $uniqueId, 

									'mimetype'=> $mime)

							);

						}else{

							$previous = $q->first()->unique_id;

							$pre = $userFolder . '/' . $previous;

							$new = $deleteFolder . '/' . $previous;

							rename($pre, $new);

							DB::getInstance()->query('UPDATE museum_images 

								SET unique_id = ?, mimetype = ?, deleted = ? WHERE sid = ? AND source = ?', 

								array($uniqueId, $mime, 0, $q->first()->id, 'staff'));

						}				

						printData(1, 'oki');

					}else{

						printData(2, 'mimetype');

					}

				}else{

						printData(3, 'file size');

				}

			}else{

					printData(4, 'no file');

			}

		}else{

				printData(5, 'no file');
		}
	
	}else{

		printData(6, 'missing PHP library');

	}


	$status['file'] = $fileSize;

	$status['userFolder'] = $userFolder;

	$status['deleteFolder'] = $deleteFolder;

	$status['fileFolder'] = $fileFolder;

	echo json_encode($status);

	exit();	

}


function printData($dataId, $from){

	$status['id'] = Input::get('id');

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>