<?php

/**
 * @summary Delete image
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the initializations */

require_once '../../core/init.php';

$db = DB::getInstance();

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='delete_museum_staff_image'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

if(!Input::exists()) printData(0, 'missing input');

/**validate? */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'id', 'required' => true))

);

if(!$validation->passed()) printData(0, 'validation not passed');

/**if is valid, delete value... */

else{

	DB::getInstance()->query('UPDATE museum_images SET deleted = ? WHERE sid = ?', [1, input::get('id')]);

	printData(1, '');

}

function printData($dataId, $from){

	$status['id'] = Input::get('id');

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}



?>