<?php

/**
 * @summary We add a new image for a staff member.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

 /** define all the initial settings: variables, limits for uploads, requires, etc */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

require_once '../../core/init.php';

require '../../../libraries/claviska/SimpleImage.php';

 /** we will use this library in order to resize, crop, change sizes, manage quality, etc */

$image = new \claviska\SimpleImage();

$createFolders = false;

$finalUniqueId;

$mime = 'image/jpeg';


/** we ll save the wording query for this instance */

$wordingQuery = $db = DB::getInstance()->query("Select * FROM ajax_responses_wording WHERE form_type='edit_museum_staff_image'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/** if there is no data, we will exit from here */

if(!Input::exists()) printData(0, 'data not exists');

/** then we create a new user instance and we check if is logged in */

$user = new User();

if($user->isLoggedIn()){

	/** check all the images staff with this instance */

	$filesManager = new FilesManager();

	$ds = DIRECTORY_SEPARATOR;

	/** if the imageUniqueId we create a new one. This can happen when there was not an image and
	* the user decide to upload a new one. This way we create all the initial setup for the image */

	if(empty(Input::get('imageUniqueId'))){

		$finalUniqueId = uniqid();

		$createFolders = true;

	}else{

		$finalUniqueId = Input::get('imageUniqueId');
	}

	/** and then we create all the paths */

	$userFolder = Config::get('staff/staff_profile_picture_folder') .  Input::get('userId');

	$deleteFolder = Config::get('staff/staff_profile_picture_folder') .  Input::get('userId') . '/delete';

	$fileFolder = $userFolder . '/' . $finalUniqueId;

	$name = Input::get('userId') . '_' . $finalUniqueId;

	$targetFile =  $fileFolder . '/' .  $name . '_edited.jpeg';

	$targetFileRetina =  $fileFolder . '/' .  $name . '_edited@2x.jpeg';

	$original =  $fileFolder . '/' .  $name . '_original.jpeg';

	$sq =  $fileFolder . '/' .  $name . '_sq.jpeg';

	/** if this is the first time that we upload an image, the imageUniqueId must be empty */

	if($createFolders == true){

		$userFolderCreated = $filesManager->makeDirectory($userFolder);

		$deleteFolder = $filesManager->makeDirectory($deleteFolder);

		$folderCreated = $filesManager->makeDirectory($fileFolder);
	}


	$data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', Input::get('image')));

	/** check folder... */

	$userFolderExists = $filesManager->checkDirectory($fileFolder);

	if(!$userFolderExists) printData(0, 'no user folder');

	$folderExists = $filesManager->checkDirectory($fileFolder);

	if(!$folderExists) printData(0, 'no image folder');

	$checkPHPExtensions = $filesManager->checkPHPExtensions();

	if(!$checkPHPExtensions) printData(0, 'no extensions');

	if (!empty($_FILES)) printData(0, 'no files!');

	/** file reader */

	if (substr(Input::get('fileReaderImage'), 0, 5) == 'data:') {

		$fileReader = $image->fromDataUri(Input::get('fileReaderImage'))->toFIle($original, $mime, 100);

		$sqImg = $image->fromDataUri(Input::get('fileReaderImage'))

		->thumbnail(50, 50, 100)

		->toFIle($sq, $mime, 100);
	}

	/** create the retina version */

	$retina = $image->fromDataUri(Input::get('image'))

	->toFile($targetFileRetina, $mime, 70);

	$halfSizeWidth = $retina->getWidth()/2;
	
	$halfSizeHeight = $retina->getWidth()/2;

	/** create the normal version */

	$image->fromDataUri(Input::get('image'))

	->resize($halfSizeWidth, $halfSizeHeight)

	->toFile($targetFile, $mime, 70);

	/** get the image id */

	if($createFolders == true){

		DB::getInstance()->insert('museum_images', 

			array('sid' => Input::get('userId'), 

				'unique_id' => $finalUniqueId, 

				'mimetype'=> $mime,

				'source'=>'staff',

				'path'=>'staff'

			));

		$imageId = DB::getInstance()->lastId();

		DB::getInstance()->insert('museum_staff_profile_image_crop_box',array('id'=>$imageId));

		DB::getInstance()->insert('museum_staff_profile_image_data', array('id'=>$imageId));

		DB::getInstance()->insert('museum_staff_profile_canvas_data', array('id'=>$imageId));

		DB::getInstance()->insert('museum_staff_profile_image_filters', array('id'=>$imageId));

	}else{

		$db = DB::getInstance()->query('SELECT id, deleted FROM museum_images WHERE unique_id = ?', 

			array($finalUniqueId));

		$imageId = $db->first()->id;

		$wasDeleted = $db->first()->deleted;

		if($wasDeleted == 1){

			$fieldsForDelete = array('deleted'=>0);

			$updater = DB::getInstance()->update('museum_images', $imageId, $fieldsForDelete);
		}

	}


	/** crop data */

	$cropBoxFields = array(

		'top'=>json_decode(Input::get('boxDataObject'))->top,

		'left'=>json_decode(Input::get('boxDataObject'))->left,

		'width'=>json_decode(Input::get('boxDataObject'))->width,

		'height'=>json_decode(Input::get('boxDataObject'))->height

	);

	DB::getInstance()->update('museum_staff_profile_image_crop_box', $imageId, $cropBoxFields);

	/** image data */

	$imageDataFields = array(

		'top'=>json_decode(Input::get('imageDataObject'))->top,

		'left'=>json_decode(Input::get('imageDataObject'))->left,

		'width'=>json_decode(Input::get('imageDataObject'))->width,

		'height'=>json_decode(Input::get('imageDataObject'))->height,

		'rotate'=>json_decode(Input::get('imageDataObject'))->rotate,
		
		'scale_x'=>json_decode(Input::get('imageDataObject'))->scaleX,

		'scale_y'=>json_decode(Input::get('imageDataObject'))->scaleY,

		'aspect_ratio'=>json_decode(Input::get('imageDataObject'))->aspectRatio,
		
		'natural_width'=>json_decode(Input::get('imageDataObject'))->naturalWidth,

		'natural_height'=>json_decode(Input::get('imageDataObject'))->naturalHeight
	);

	DB::getInstance()->update('museum_staff_profile_image_data', $imageId, $imageDataFields);

	/** image data */

	$canvasDataFields = array(

		'top'=>json_decode(Input::get('canvasDataObject'))->top,

		'left'=>json_decode(Input::get('canvasDataObject'))->left,

		'width'=>json_decode(Input::get('canvasDataObject'))->width,

		'height'=>json_decode(Input::get('canvasDataObject'))->height,
				
		'natural_width'=>json_decode(Input::get('canvasDataObject'))->naturalWidth,

		'natural_height'=>json_decode(Input::get('canvasDataObject'))->naturalHeight
	);

	DB::getInstance()->update('museum_staff_profile_canvas_data', $imageId, $canvasDataFields);




	/** filters data */

	$filtersFields = array(

		'is_grey_scale'=>(Input::get('isGreyScale') == 'true') ? 1 : 0,

		'is_custom'=>(Input::get('isCustom') == 'true') ? 1 : 0,
		
		'brightness'=>Input::get('brightness'),

		'contrast'=>Input::get('contrast'),

		'saturation'=>Input::get('saturation'),

		'sharpen'=>Input::get('sharpen'),

		'vibrance'=>Input::get('vibrance'),

		'exposure'=>Input::get('exposure')
	);

	DB::getInstance()->update('museum_staff_profile_image_filters', $imageId, $filtersFields);

	Logger::addLogData($user->data()->id, 

		'user',

		'Staff member '. Input::get('userName') .' image was edited.',

		'Editaste la imagen del miembro del staff ' . Input::get('userName')

	);

	printData(1, 'ok...editada');

}

/**
* @function printData
* @description easy way to print only once all the json data for JS reading.
*
* @param {int} $dataId 	- id for the wording.
* @param {string} $from - for similar errors.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	$status['previouslyDeleted'] = Input::get('previouslyDeleted');

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>