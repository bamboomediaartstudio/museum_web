<?php

/**
 * @summary Update the order of the staff.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the requires */

require_once '../../core/init.php';

/**initialization */

$user = new User();

if(!$user->isLoggedIn()){

	Redirect::to('index.php');

}

$db = DB::getInstance();

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='update_db_value'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'input');

/**if there is no token, print and out */

//if(!Token::check(Input::get('token'))) printData(0, 'token');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	//'id' => array('display'=> 'id', 'required' => true),

	//'newValue' => array('display'=> 'newValue', 'required' => true),

	'dataType' => array('display'=> 'dataType', 'required' => true),

	'sid' => array('display'=> 'sid', 'required' => true),

	'action' => array('display'=> 'action', 'required' => true),

	//'column' => array('display'=> 'column', 'required' => true)

), false);

if(!$validation->passed()) printData(0, 'validation');

else{

	switch (Input::get('sid')) {

		case 0:

		$table = 'museum_staff';

		$query = $db->update($table, Input::get('id'), array(Input::get('column')=>Input::get('newValue')));

		printData(1, 'home');

		break;

		case 1:

		$table = 'museum_staff_categories_relations';

		/**
		* first we have to know if we are adding a memeber to a new category or if we are deleting it. 
		* we know this information: Input::get('newValue')
		*
		* if is adding:
		*
		* we have to know the last sorting value inside this list. For this we have to get the max
		* internal order (max(internal_order)) for the received column, checking for active as true (1)
		* and deleted as false (0)
		*
		* once we have the order id, we add the new relation and we asign the item order as the last one.
		*
		* if we are deleting: we deduce that the relation already exists
		*
		* we need to know the user order / sort so that we can change the subsequent users once it was deleted.
		*
		* We mark the user as deleted.
		*
		* then we loop through all the subsequent users and modify the sorting reduing it by one.
		*/

		if(Input::get('newValue') == "1"){

			$lastSort = DB::getInstance()->query('SELECT max(internal_order) as internal_order FROM 

				museum_staff_categories_relations WHERE id_museum_staff_category = ? AND 

				deleted = ? ', [Input::get('column'), 0]);

			$status['newValue'] = "adding";

			$status['lastId'] = $lastSort->first()->internal_order;
			
			$status['newId'] = ($lastSort->first()->internal_order + 1);

			$newRelation = DB::getInstance()->insert('museum_staff_categories_relations', 

				[

					'id_museum_staff_user'=> Input::get('id'),

					'id_museum_staff_category' => Input::get('column'),

					'internal_order' => ($lastSort->first()->internal_order + 1)

				]);

			printData(1, 'adding');


		}else if(Input::get('newValue') == "0"){

			$status['newValue'] = "deleting";

			/**get the actual sort of the member */

			$lastSort = DB::getInstance()->query('SELECT max(internal_order) as internal_order FROM 

				museum_staff_categories_relations WHERE id_museum_staff_user = ? AND id_museum_staff_category = ? 

				AND deleted = ?', [Input::get('id'), Input::get('column'), 0]);

			/**delete it from DB */

			$markAsDeleted = DB::getInstance()->query('UPDATE museum_staff_categories_relations SET 

				deleted = ? WHERE id_museum_staff_user = ? AND id_museum_staff_category = ?', 

				[1, Input::get('id'), Input::get('column')]);

			$status['newValue'] = "deleting";

			$status['lastSortOption'] = $lastSort->first()->internal_order;

			/**select all the staff members  where the sorting is bigger than the deleted item*/

			$all = $db->query("SELECT * from museum_staff_categories_relations 

				WHERE internal_order > ? AND id_museum_staff_category = ? AND deleted = ?", 

				[$lastSort->first()->internal_order, Input::get('column'), 0]);

			/**reduce the sorting for all of them*/

			foreach($all->results() as $result){

				$newOrder = $result->internal_order - 1;

				$id = $result->id;

				$fields = array('internal_order'=>$newOrder);

				$db->update('museum_staff_categories_relations', $id, $fields);

			}

			printData(1, 'deleting');
		}

		case 2:

		$table = 'museum_staff_roles';

		/**
		* when we are updating the role of a staff member, we have two possible situations: 
		* there is an ID, so that means that the data is part of the typeahead system and the role
		* is inside the DB. On that case, we just update the role id inside the user field.
		*
		* otherwise, if the id is null or not exists, the user is uploading a new role. In this situation,
		* we have to create the new role, get the id, and then update the id of the staff member
		*/

		$roleId = null;

		if(!empty(Input::get('column'))){

			$roleId = Input::get('column');

		}else{

			if(!empty(Input::get('newValue'))){

				$checkItem = $db->get('museum_staff_roles',['role','=',Input::get('newValue')], false);

				if($checkItem->count()>0){

					$roleId = $checkItem->first()->id;

				}else{

					$db->insert('museum_staff_roles',['role' => Input::get('newValue')]);

					$roleId = $db->lastId();

				}

			}else{

				$roleId = null;
			}
		}

		$query = $db->update('museum_staff', Input::get('id'), array('role'=>$roleId));

		/*$status['roleId']  		= 		$roleId;

		$status['newValue']  	= 		Input::get('newValue');

		$status['sid']  		= 		Input::get('sid');

		$status['column']  		= 		Input::get('column');

		$status['id']  		= 		Input::get('id');

		echo json_encode($status);

		exit();*/

		printData(1, 'update role');

		break;

		
	}
}

/**
* @function printData
* @description easy way to print only once all the json data for JS reading.
*
* @param {int} $dataId - id for the wording.
*/

function printData($dataId, $from){

	$status['status'] 		= 		$GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] 		= 		$GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] 			= 		$GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  		= 		$GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  		= 		$GLOBALS['wordingArray'][$dataId]->action_button_label;

	$status['newValue']  	= 		Input::get('newValue');
	
	$status['sid']  		= 		Input::get('sid');
	
	$status['column']  		= 		Input::get('column');
	
	$status['token']  		= 		Input::get('token');
	
	$status['from']  		= 		$from;

	echo json_encode($status);

	exit();
}



?>