<?php

/**
 * @summary Update the order of the staff.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the requires */

require_once '../../core/init.php';

/**initialization */

$user = new User();

if(!$user->isLoggedIn()){

	Redirect::to('index.php');

}

$status['init'] = true;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_staff_update_order'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0);

/**if there is no token, print and out */

if(!Token::check(Input::get('token'))) printData(0);

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'array' => array('display'=> 'array', 'required' => true)

), false);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0);

else{

	foreach(Input::get('array') as $item){

		$db->update('museum_staff_categories_relations', $item['id'], 

			array('internal_order'=>$item['newPosition']));

	}

	$staffMember = Input::get('selectedName');

	$category = Input::get('category');

	Logger::addLogData(

		$user->data()->id, 

		'user', 

		'the order of the staff member '. $staffMember .' order was modified inside the category ' . $category, 

		'Modificaste el orden del ' . $staffMember . ' dentro de la categoría ' . $category);

	printData(1);
}

/**
* @function printData
* @description easy way to print only once all the json data for JS reading.
*
* @param {int} $dataId - id for the wording.
*/

function printData($dataId){

	$status['status'] 		= 		$GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] 		= 		$GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] 			= 		$GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  		= 		$GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  		= 		$GLOBALS['wordingArray'][$dataId]->action_button_label;

	$status['changeStatus'] =		 Input::get('status');

	echo json_encode($status);

	exit();
}

?>