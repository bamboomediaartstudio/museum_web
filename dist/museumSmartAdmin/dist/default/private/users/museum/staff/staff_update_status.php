<?php

/**
 * @summary Update the statur or delete.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the requires */

require_once '../../core/init.php';

/**initialization */

$user = new User();

if(!$user->isLoggedIn()){

	Redirect::to('index.php');

}

$status['init'] = true;

$db = DB::getInstance();

/**if there is no data, print and out */

if(!Input::exists()) printData(0);

/**if there is no token, print and out */

if(!Token::check(Input::get('token'))) printData(0);

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'id', 'required' => true),

	'action' => array('display'=> 'action', 'required' => true),

	'status' => array('display'=> 'status', 'required' => true)));

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0);

/**Select the wording according to the type of action...update or delete. */

else{

	if(Input::get('action') == 'update'){
		
		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_staff_change_status'");	
	}else{

		$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_staff_delete_member'");

	}

	if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

	$change = (Input::get('status') == 'true') ? 1 : 0;

	$field = (Input::get('action') == 'delete') ? 'deleted' : 'active';

	$updated = $db->update('museum_staff_categories_relations',Input::get('id'), array($field=>$change));

	/**If we are deleting, there is an extra step where we have to reorder all, reducing in one 
	the sort index of all the subsequent items inside the category.

	example:
	1, 2, 3 - deleted, 4, 5

	then:
	1, 2, 4, 5

	solution:
	1, 2, 3, 4
	 */

	if($field == 'deleted')
	{
		$order = $db->query("SELECT internal_order, id_museum_staff_category FROM museum_staff_categories_relations 

			WHERE id = ?", [Input::get('id')]);

		$all = $db->query("SELECT * from museum_staff_categories_relations 

			WHERE internal_order > ? AND id_museum_staff_category = ?", 

			[$order->first()->internal_order, $order->first()->id_museum_staff_category]);

		$count = 0;

		foreach($all->results() as $result){

			$count+=1;

			$newOrder = $result->internal_order - 1;

			$id = $result->id;

			$fields = array('internal_order'=>$newOrder);

			$db->update('museum_staff_categories_relations',$id,$fields);

		}

	}

	$staffMember = Input::get('name');

	$category = Input::get('category');

	if(Input::get('action') == 'update'){

		$logNote = "The visibility of the staff member " . $staffMember . " inside the category " . $category . " was changed to " . ((Input::get('status') == 'true') ? "online." : "offline."); 

		$logPersonal = "Cambiaste la visibilidad de <b>" . $staffMember . "</b> dentro de la categoría <b>" . $category . "</b> a <b>" .

		((Input::get('status') == 'true') ? "online." : "offline.") . '</b>'; 

		
	}else{

		$logNote = "The staff member " . $staffMember . " was deleted from the category " . $category;

		$logPersonal = "Eliminaste a <b>" . $staffMember . "</b> de la categoría <b>" . $category;

	}

	Logger::addLogData($user->data()->id, 'user',  $logNote,  $logPersonal);

	printData(1);	
}

/**
* @function printData
* @description easy way to print only once all the json data for JS reading.
*
* @param {int} $dataId - id for the wording.
*/

function printData($dataId){

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	$status['changeStatus']  = Input::get('status');

	echo json_encode($status);

	exit();
}


?>