<?php

/**
 * @summary Send support.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**all the includes */

require_once '../../core/init.php';

/**all the variables */

$status['init'] = true;

$db = DB::getInstance();

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_support_add'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'subject' => array('display'=> 'subject', 'required' => true),

	'report' => array('display'=> 'report', 'required' => true),

	'reporter' => array('display'=> 'reporter', 'required' => true),

	'subjectString' => array('display'=> 'subjectString', 'required' => true),

	'reporterId' => array('display'=> 'reporterId', 'required' => true)));

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(0, 'validation not passed');

else{

	$user = new User();

	$reporterEmail = $user->data()->email;

	if($user->isLoggedIn()){

		$db->insert('museum_support_messages',[

			'id_subject'=> Input::get('subject'),

			'id_reporter'=> Input::get('reporterId'),

			'reporter'=> Input::get('reporter'),

			'message' => Input::get('report')
		]);

		//general settings...

		$settingsQ = $db->query("SELECT * FROM settings");

		$settings = $GLOBALS['settings'] = $settingsQ->first();

		//support settings...

		$supportSettingsQ = $db->query("SELECT * FROM support_settings");

		$GLOBALS['supportSettings'] = $supportSettingsQ->first();

		//museum settings...

		$museumSettingsQ = $db->query("SELECT * FROM museum_data");

		$museumSettings = $museumSettingsQ->first();

		//museum social media...

		$museumSocialMediaQ = $db->query("SELECT * FROM museum_social_media");

		$museumSocialMediaSettings = $museumSocialMediaQ->first();

		//template for subscriber...

		$address = $museumSettings->address . ' ' . $museumSettings->address_number . ', ' . $museumSettings->zip_code . ' ' . $museumSettings->city;

		$message = file_get_contents('../../../email_templates/support.html');

		$message = str_replace('%projectTitle%', $museumSettings->title, $message);

		$message = str_replace('%reporter%', Input::get('reporter'), $message);

		$message = str_replace('%subjectString%', Input::get('subjectString'), $message);

		$message = str_replace('%report%', Input::get('report'), $message);

		$message = str_replace('%reporterEmail%', $reporterEmail, $message);

		$message = str_replace('%ip%', Helpers::getIP(), $message);

		$message = str_replace('%os%', Helpers::getOS(), $message);

		$message = str_replace('%browser%', Helpers::getBrowser(), $message);

		$message = str_replace('%email%', $GLOBALS['supportSettings']->support_email, $message);

		$message = str_replace('%address%', $address, $message);

		$message = str_replace('%te%', $museumSettings->te, $message);

		$message = str_replace('%museumEmail%', $museumSettings->email, $message);

		$message = str_replace('%lat%', $museumSettings->lat, $message);

		$message = str_replace('%lon%', $museumSettings->lon, $message);

		$message = str_replace('%facebook%', $museumSocialMediaSettings->facebook, $message);

		$message = str_replace('%instagram%', $museumSocialMediaSettings->instagram, $message);

		$message = str_replace('%twitter%', $museumSocialMediaSettings->twitter, $message);

		$sent = Helpers::email('aditivoestudio@gmail.com', utf8_decode('Soporte Técnico'), $message);

		if($sent){

			$status['envio'] = 'exitoso';

		}else{
			
			$status['envio'] = $sent;
			
			printData(0, 'error');

			

		}

		addLogData($user);

		printData(1, 'ok');

	}else{

		printData(0, 'error'); 

	}

}

/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 		user object.
*/

function addLogData($user){

	$systemString = 'a new report was added.';

	$userString = 'Enviaste un reporte de error.';

	Logger::addLogData($user->data()->id, 

		'user',

		$systemString,

		$userString,

		'faq',

		'faq');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['from'] = $from;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>