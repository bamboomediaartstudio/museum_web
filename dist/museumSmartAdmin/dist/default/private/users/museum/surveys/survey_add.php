<?php

/**
 * @summary Add new Criminals to the DB - APP
 *
 * @description -
 *
 * @author Mariano Makedonsky
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo ...
 */


/**all the includes */

require_once '../../core/init.php';

/**all the variables */


$db = DB::getInstance();

$data;

if(!Input::exists()){

  $data=0;

}

$validate = new Validate();

$validation = $validate->check($_POST, array(

  'content_rate' => array('display' => 'content_rate', 'required' => true),
  
  'device_id' => array('display' => 'device_id', 'required' => true),
  
  'device_name' => array('display' => 'device_name', 'required' => true),

  'device_model' => array('display' => 'device_model', 'required' => true),
  
  'device_brand' => array('display' => 'device_brand', 'required' => true),

  'experience_qualification' => array('display' => 'experience_qualification', 'required' => true),
  
  'time_in_museum' => array('display' => 'time_in_museum', 'required' => true),
  
  'museum_attention' => array('display' => 'museum_attention', 'required' => true),
  
  'app_rate' => array('display' => 'app_rate', 'required' => true),
 
  'guided' => array('display' => 'guided', 'required' => true),

  'recomend' => array('display' => 'recomend', 'required' => true),

  'visit_completed' => array('display' => 'visit_completed', 'required' => true),

  'museum_rate' => array('display' => 'museum_rate', 'required' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()){
    
    $data=1;

}else{

    $arrayQuery = [

      'content_rate' => Input::get('content_rate'),
     
      'device_id' => Input::get('device_id'),
     
      'device_name' => Input::get('device_name'),

      'device_model' => Input::get('device_model'),
      
      'device_brand' => Input::get('device_brand'),

      'experience_qualification' => Input::get('experience_qualification'),

      'time_in_museum' => Input::get('time_in_museum'),

      'museum_attention' => Input::get('museum_attention'),
      
      'app_rate' => Input::get('app_rate'),

      'guided' => Input::get('guided'),

      'recomend' => Input::get('recomend'),

      'visit_completed' => Input::get('visit_completed'),
      
      'museum_rate' => Input::get('museum_rate'),
    ];

    $insertDbQuery = $db->insert('app_survey', $arrayQuery);

    if($insertDbQuery){

      $lastId = $db->lastId();

      $data=2;

    }else{
     
     $data=3;
      
    }

}

echo "Status=".$data;

exit();


?>
