<?php

class Trivias{

  private $db;

  public function __construct(){

    $this->db = DB::getInstance();

  }

  /**
  * Datos de trivias(preguntas) y respuestas por categorias
  * Buscar los datos
  * @param $cat id categoria - Ver id en tabla app_museum_cagories
  * @return {array} $triviasArray - Data cruda como se pide en sql -
  */
  private function getTriviasByCategoryRawData($cat){

    $sql = "SELECT museum_trivias.id AS idTrivia, museum_trivias.trivia,
                  museum_trivias_custom_replies.id AS idReply, museum_trivias_custom_replies.reply, museum_trivias_custom_replies.correct
                  FROM museum_trivias_categories_relations AS R
                  INNER JOIN museum_trivias
                  ON museum_trivias.id = R.id_trivia AND R.id_category = ? AND museum_trivias.active = ? AND museum_trivias.deleted = ?
                  INNER JOIN museum_trivias_custom_replies
                  ON museum_trivias_custom_replies.id_trivia = museum_trivias.id AND museum_trivias_custom_replies.active = ? AND museum_trivias_custom_replies.deleted = ?";

    $triviasQuery = $this->db->query($sql, [$cat, 1, 0, 1, 0]);

    $triviasArray = $triviasQuery->results(true);


    return $triviasArray;

  }


  /**
  * Toma el array de getTriviasByCategoryRawData y organiza la info
  *
  * @param $cat id categoria -
  * @return {array} $finalArray - Datos organizados en trivias - respuestas
  */
  public function getTriviasByCategory($idCat){

    $finalArray = array();

    $triviasArray = $this->getTriviasByCategoryRawData($idCat);

    //Agrupar resultados en arrayFinal

    //Agrupar trivias (preguntas)

    foreach($triviasArray as $genericId => $triviaData){

      $finalArray[$triviaData["idTrivia"]] = array(

        "idTrivia"  => $triviaData["idTrivia"],
        "trivia"    => $triviaData["trivia"],
        "replies"   => array()

      );

    }

    //Agrupar respuestas a cada trivia(preguntas) - Por cada Trivia, todas las respuestas

    foreach($triviasArray as $genericId => $triviaData){

      $finalArray[$triviaData["idTrivia"]]["replies"][$triviaData["idReply"]] = array(

        "idReply"   => $triviaData["idReply"],
        "reply"     => $triviaData["reply"],
        "correct"   => $triviaData["correct"]

      );

    }

    return $finalArray;

  }

}

?>
