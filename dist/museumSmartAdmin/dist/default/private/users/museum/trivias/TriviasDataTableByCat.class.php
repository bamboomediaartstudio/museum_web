<?php

/**
*
 * @summary list data for this app - Datatables info display - Server side
 *
 * @description - Data response for datatables list
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 */

class TriviasDataTableByCat{

  private $draw;
  private $row;
  private $rowPerPage;
  private $columnName;
  private $searchValue;
  private $searchQuery = "";

  private $searchColumnName = ""; //Buscar en "name" (ej)
  private $searchColumnNameTwo = "";// Buscar en Description (ej)

  private $columnNameForOrder = "id"; //Hardcoded - List order by
  private $columnSortOrder = 'desc';  //Hardcoded order to desc

  private $table;
  private $table_relation;

  private $id_for_relation;

  public function __construct($readArr=array(), $_tableName, $_columnName, $_columnNameTwo, $_tableRelationName){

    $this->draw         = $readArr["draw"];
    $this->row          = $readArr["row"];
    $this->rowPerPage   = $readArr["rowPerPage"];
    $this->columnName   = $readArr["columnName"];
    $this->searchValue  = $readArr["searchValue"];

    $this->table            = $_tableName;
    $this->table_relation  = $_tableRelationName;

    $this->searchColumnName     = $_columnName;
    $this->searchColumnNameTwo  = $_columnNameTwo;

  }

  /**
  ** Total number of records without filtering
  **/
  private function getTotalRecords(){

    $totalRecordsQuery = DB::getInstance()->query("SELECT COUNT(*) AS allCount FROM ".$this->table." WHERE deleted = 0");
    $records = $totalRecordsQuery->results(true);

    $totalRecords = $records[0]['allCount'];

    return $totalRecords;

  }


  /**
  ** Fetch Records by category
  *
  * @$table_relation {string}
  * @$id_for_relation {string}
  * @$idCategory {int}
  *
  **/
  public function getFetchRecordsByCategory($id_for_relation, $idCategory){

    $arrayRecordsWithFilter = $this->getTotalRecordsWithFilter($id_for_relation, $idCategory);  //Primero filtrar para armar la parte de la query con filtro ($this->searchQuery)

    $sql = "SELECT t_relation.id as idRelation, t_principal.id as idTrivia, t_principal.trivia, t_principal.active, t_categories.id as idCategory, t_categories.name as categoryName FROM ";
    $sql .= $this->table." as t_principal, ".$this->table_relation." as t_relation, app_museum_categories as t_categories WHERE t_principal.id = t_relation.".$id_for_relation." AND t_relation.id_category LIKE '".$idCategory."' AND t_relation.id_category = t_categories.id ";
    $sql .= "AND t_relation.deleted = 0 AND t_relation.active = 1 AND t_principal.deleted = 0 ".$this->searchQuery." ORDER BY t_principal.".$this->columnNameForOrder . " " . $this->columnSortOrder." LIMIT ".$this->row.", ".$this->rowPerPage;

    //echo $sql;

    $dataQuery = DB::getInstance()->query($sql);
    $dataList  = $dataQuery->results(true);
    $cantResultsFiltered = count($dataList);

    $count = 0;
    foreach($dataList as $id => $guetos){
      $count++;

      $dataList[$id]["order"] = $count;

    }


    //***********//

    $response = array(

      "draw"            => intval($this->draw),

      "recordsTotal"    => sizeof($dataList),

      "recordsFiltered" => $arrayRecordsWithFilter,

      "data"            => $dataList

    );

    //**************//

    //return $dataList;
    return $response;
  }


  /**
  ** Total number of record with filtering
  **/
  private function getTotalRecordsWithFilter($id_for_relation, $idCategory){

    // Search (input search datable)

    if($this->searchValue != ''){

      if(!$this->searchColumnNameTwo==""){

        $this->searchQuery = " and (".$this->searchColumnName." like '%".$this->searchValue."%' or ".$this->searchColumnNameTwo." like '%".$this->searchValue."%' ) ";

      }else{

        $this->searchQuery = " and (".$this->searchColumnName." like '%".$this->searchValue."%') ";

      }

    }

    $sql = "SELECT COUNT(*) allCount FROM ";
    $sql .= $this->table." as t_principal, ".$this->table_relation." as t_relation, app_museum_categories as t_categories WHERE t_principal.id = t_relation.".$id_for_relation." AND t_relation.id_category LIKE '".$idCategory."' AND t_relation.id_category = t_categories.id ";
    $sql .= "AND t_relation.deleted = 0 AND t_relation.active = 1 AND t_principal.deleted = 0 ".$this->searchQuery." ORDER BY t_principal.".$this->columnNameForOrder . " " . $this->columnSortOrder;

    $recordFilteredQuery = DB::getInstance()->query($sql);
    $records = $recordFilteredQuery->results(true);

    //echo $sql;

    //print_r($records);

    $totalRecordwithFilter = $records[0]['allCount'];

    if($this->searchValue != ''){

      //echo $sql;
    }

    return $totalRecordwithFilter;
  }



}


?>
