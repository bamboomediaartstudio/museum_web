<?php


/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../core/init.php';

/**all the variables */


$status['init'] = true;

$cantMaxReplies = 5;

$db = DB::getInstance();


/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists');


/**initialize validation process */

$validate = new Validate();


$validation = $validate->check($_POST, array(

  'reply' => array('display' => 'reply', 'required' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(1, 'validation not passed');

else{

  $user = new User();

  if($user->isLoggedIn()){



    $cantDbReplies = checkActualReplies(Input::get('id'));

    if($cantDbReplies <= $cantMaxReplies){

      $correctReply = 0;

      if($cantDbReplies == 0){

        $correctReply = 1;

      }


      $arrayQuery = [

        'id_trivia' => Input::get('id'),

        'correct' => $correctReply,

<<<<<<< HEAD
        'reply' => strip_tags(Input::get('reply')),
=======
        'reply' => Input::get('reply'),
>>>>>>> e0b157755a5f16e0c10cd2dba9a0139208e88f34

        'active' => 1,

        'deleted'  => 0

      ];


      $insertDbQuery = $db->insert('museum_trivias_custom_replies', $arrayQuery);

      if($insertDbQuery){

        printData(1, "Agregado correctamente");

      }else{

        //No se cargaron datos del formulario de manera exitosa. No cargar imagenes.
        printData(0, "Error al agregar datos");

      }

    }else{

      printData(0, "No se admiten mas de ".$cantMaxReplies." respuestas");
      exit;
    }


  }


}

/**
* @function checkActualReplies
* @description check cant replies
*
*/
function checkActualReplies($idTrivia){

  $db = DB::getInstance();

  $wordingQuery = $db->query("SELECT count(id) as totalReplies FROM museum_trivias_custom_replies WHERE id_trivia = ? AND active = 1 AND deleted = 0 ", array($idTrivia));

  if($wordingQuery){

    $arrReplies = $db->results(true);

    return $arrReplies[0]["totalReplies"];

  }else{
    throw new Exception("Error en la consulta de keywords", 1);
  }

}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from){

	$status['status'] = $dataId;

	$status['from'] = $from;

	// $status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;
  //
<<<<<<< HEAD
	 $status['title'] = "Respueta añadida!";
  //
	$status['msg'] = "Se agregó una nueva respuesta a la trivia. Recordá de indicar cual es la correcta. <br><br>¿añadir otra?";
  //
	 //$status['alert']  = "Alert";
  //
	 $status['button']  = "Agregar Otra";
=======
	 $status['title'] = "Agregar respustas a trivia";
  //
	// $status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;
  //
	 //$status['alert']  = "Alert";
  //
	 $status['button']  = "Entendido";
>>>>>>> e0b157755a5f16e0c10cd2dba9a0139208e88f34

	echo json_encode($status);

	exit();
}


?>
