<?php

/**
 * @summary General updater :)
 *
 * @description -
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**all the requires */

require_once '../../core/init.php';

/**initialization */


//print_r($_POST);


$user = new User();

if(!$user->isLoggedIn()){

	Redirect::to('login.php');

}

$db = DB::getInstance();

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='update_db_value'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'input');

/**initialize validation process */

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'id' => array('display'=> 'id', 'required' => true),

  'idTrivia' => array('display'=> 'idTrivia', 'required' => true)));

if(!$validation->passed()) printData(0, 'validation');

else{

	//Update delete reply

  if(checkActualReplies(Input::get('idTrivia')) <= 2){

    printData(0, "Error, no se puede borrar. La cantidad mínima de respuestas es 2");

  }else{

    if(checkCorrect(Input::get('id')) != 1){


    	$updated = DB::getInstance()->update("museum_trivias_custom_replies",Input::get('id'), array("deleted" => 1));

      printData(1, "Respuesta eliminada");

    }else{

      printData(0, "Error, no se puede borrar, esta es una respuesta correcta");

    }



  }




}

/**
* @function checkActualReplies
* @description check cant replies
*
*/
function checkActualReplies($idTrivia){

  $db = DB::getInstance();

  $wordingQuery = $db->query("SELECT count(id) as totalReplies FROM museum_trivias_custom_replies WHERE id_trivia = ? AND active = 1 AND deleted = 0 ", array($idTrivia));

  if($wordingQuery){

    $arrReplies = $db->results(true);

    return $arrReplies[0]["totalReplies"];

  }else{
    throw new Exception("Error en la consulta total de respuestas a trivia", 1);
  }

}


//Check if this reply is the correctone
function checkCorrect($idReply){

  $db = DB::getInstance();

  $wordingQuery = $db->query("SELECT count(id) as correctReply FROM museum_trivias_custom_replies WHERE id = ? AND correct = 1 AND active = 1 AND deleted = 0 ", array($idReply));

  if($wordingQuery){

    $arrReplies = $db->results(true);

    //print_r($arrReplies);

    return $arrReplies[0]["correctReply"];

  }else{
    throw new Exception("Error en la consulta respuesta correcta de trivia", 2);
  }

}


/**
* @function printData
* @description easy way to print only once all the json data for JS reading.
*
* @param {int} $dataId - id for the wording.
*/

function printData($dataId, $from){

  $status['status'] = $dataId;

  $status['from'] = $from;

  // $status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;
  //
   $status['title'] = "Eliminar respustas a trivia";
  //
   $status['msg'] = $from;
  //
   //$status['alert']  = "Alert";
  //
   $status['button']  = "Entendido";

  echo json_encode($status);

	exit();
}

?>
