<?php

//Films Data Controller By cat (list data) By Cat

require_once '../../core/init.php';
include '../../museum/trivias/TriviasDataTableByCat.class.php';


//Read data from datatables
$columnIndex = $_POST['order'][0]['column'];
$columnName = $_POST['columns'][$columnIndex]['data'];

$setArr = array(

  "draw"        => $_POST['draw'],
  "row"         => $_POST['start'],
  "rowPerPage"  => $_POST['length'],
  "columnName"  => $columnName,
  "searchValue" => $_POST['search']['value']

);


$table_relation = "museum_trivias_categories_relations";
$id_for_relation = "id_trivia";
$idCategory = $_POST["idCategory"];

$objTriviasDataTableByCat = new TriviasDataTableByCat($setArr, $_POST["table"], $_POST["searchColumn"], (isset($_POST["searchColumnTwo"]) ? $_POST["searchColumnTwo"] : ""), $table_relation);

echo json_encode($objTriviasDataTableByCat->getFetchRecordsByCategory($id_for_relation, $idCategory));


?>
