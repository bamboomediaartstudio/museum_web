<?php

/**
 * @summary Add new Trivia to the DB - General Use
 *
 * @description -
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 */

/**all the initializations */

ini_set('memory_limit', '-1');

ini_set('max_execution_time', '60');

ini_set("upload_max_filesize", "20M");

error_reporting(E_ALL | E_STRICT);

/**all the includes */

require_once '../../core/init.php';

/**all the variables */


$allowProgramDownload = 0;

$status['init'] = true;

$db = DB::getInstance();

$lastId = "";

/**wording */

$wordingQuery = $db->query("Select * FROM ajax_responses_wording WHERE form_type='museum_trivia_add'");

if($wordingQuery) $GLOBALS['wordingArray'] = $wordingQuery->results();

/**if there is no data, print and out */

if(!Input::exists()) printData(0, 'data not exists', $lastId);


/**initialize validation process */

$validate = new Validate();


$validation = $validate->check($_POST, array(

  'trivia' => array('display' => 'trivia', 'required' => true))

);

/**if validation did not pass, print and out. Else is a valid verification. Continue */

if(!$validation->passed()) printData(1, 'validation not passed', $lastId);

else{

  $user = new User();

  if($user->isLoggedIn()){

    $arrayQuery = [

<<<<<<< HEAD
      'trivia' => strip_tags(Input::get('trivia'), '<br>'),
=======
      'trivia' => Input::get('trivia'),
>>>>>>> e0b157755a5f16e0c10cd2dba9a0139208e88f34

      'active' => 1,

      'deleted'  => 0

    ];


    $insertDbQuery = $db->insert('museum_trivias', $arrayQuery);

    if($insertDbQuery){

        $lastId = $db->lastId();

        //First add categories, then feedback options to show to the final user
        /*
        if(addCategoriesRelations($lastId, Input::get('checkboxes')) == 1){

<<<<<<< HEAD
          printData(0, 'Datos guardados correctamente', $lastId);
=======
          printData(0, 'Datos guardados correctamente');
>>>>>>> e0b157755a5f16e0c10cd2dba9a0139208e88f34

        }*/

<<<<<<< HEAD
        printData(0, 'Datos guardados correctamente', $lastId);
=======
        printData(0, 'Datos guardados correctamente');
>>>>>>> e0b157755a5f16e0c10cd2dba9a0139208e88f34


    }else{

      //No se cargaron datos del formulario de manera exitosa. No cargar imagenes.
      printData(0, 'Error, no se cargaron datos del formulario', $lastId);

    }


  }


}


/*
function addFeedbackOptionsRelations($lastTriviaId, $arrFeedbackOptions){

  $totalOptionsToAdd = count($arrFeedbackOptions);

  $totalOptionsAdded = 0;

  foreach($arrFeedbackOptions as $selected){

    $insert = DB::getInstance()->insert('museum_trivias_feedback_show_relations',
      [
        'id_trivia'             => $lastTriviaId,

        'id_feedback_option'  => $selected,

        'active'              => 1,

        'deleted'             => 0

      ]

    );

    if($insert){
        $totalOptionsAdded++;
    }

  }

  //Checkear que todas las categorias se hayan agregado a la db

  if($totalOptionsToAdd == $totalOptionsAdded){

    printData(0, 'Datos guardados correctamente');

  }else{

    printData(1, 'Error al cargar feedback options a mostrar de random facts.');

  }

}

*/

/**
* @function addLogData
* @description Add log info to the DB to track actions.
*
* @param {object} $user 	- 	 user object.
*/

function addLogData($user){

	$systemString = 'The trivia ' . Input::get('name') .  ' was added.';

	$userString = 'Agregaste la trivia <b>' . ' ' . '</b> al sistema.';

	Logger::addLogData($user->data()->id,

		'user',

		$systemString,

		$userString,

		'trivias',

		'trivias');
}

/**
* @function printData
* @description Print json data for ajax.
*
* @param {int} $dataId 	 		- 		The id.
* @param {strng} $from 			- 		Helper text.
*/

function printData($dataId, $from, $lastId){

	$status['status'] = $dataId;

	$status['from'] = $from;

  $status['lastId'] = $lastId;

	$status['status'] = $GLOBALS['wordingArray'][$dataId]->form_status;

	$status['title'] = $GLOBALS['wordingArray'][$dataId]->form_title;

	$status['msg'] = $GLOBALS['wordingArray'][$dataId]->string_value;

	$status['alert']  = $GLOBALS['wordingArray'][$dataId]->action_alert;

	$status['button']  = $GLOBALS['wordingArray'][$dataId]->action_button_label;

	echo json_encode($status);

	exit();
}

?>
