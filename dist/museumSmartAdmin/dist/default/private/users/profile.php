<?php

require_once 'core/init.php';

$status['init'] = true;

if(Input::exists()){

	$status['input'] = 'input exists';

	if(Token::check(Input::get('token'))){

		$status['token'] = 'token exists';

		$validate = new Validate();

		$validation = $validate->check($_POST, array(

			'email' => array('display'=> 'email', 'required' => true),

			'name' => array('display'=> 'nombre', 'required' => true)

		));

		if($validation->passed()){

			$user = new User();
		
			//user data...	

			try{

				$originalDate = Input::get('birthday');
				
				$newDate = date("Y-m-d", strtotime($originalDate));

				$user->update(array(

					'name'=> Input::get('name'),

					'surname'=> Input::get('surname'),

					'nickname'=> Input::get('nickname'),

					'email'=> Input::get('email'),

					'phone'=> Input::get('phone'),

					'birthday'=> $newDate

				));

			}catch(Exception $e){

				die($e->getMessage());

			}

			//social media...

			try{

				DB::getInstance()->update('users_social_media', 11, array(

					'facebook'=> Input::get('facebook'),

					'twitter'=> Input::get('twitter'),

					'instagram'=> Input::get('instagram'),

					'linkedin'=> Input::get('linkedin')

				), 'user_id');

				$status['social'] = 'social';

			}catch(Exception $e){

				$status['cachi'] = 'ahi viene cachi';

				die($e->getMessage());

			}
		
		}else{


		}


	}

}

echo json_encode($status);


?>