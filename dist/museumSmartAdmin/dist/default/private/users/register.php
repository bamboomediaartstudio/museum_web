<?php

require_once 'core/init.php';

//var_dump(Token::check(Input::get('token')));

if(Input::exists()){

	if(Token::check(Input::get('token'))){

		$validate = new Validate();

		$validation = $validate->check($_POST, array(

			'username' => array(

				'required' => true,

				'min' => 2,

				'max' =>20,

				'unique' => 'users'
			),

			'password' => array(

				'required' => true,

				'min' => 2

			),
			
			'password_again' => array(

				'required' => true,

				'matches' => 'password',



			),

			'name' => array(

				'required' => true,

				'min'=>2,

				'max'=>20

			)
		));

		if($validation->passed()){

			//Session::flash('success', 'entraste re piola');

			//header('Location: index.php');

			$user = new User();

			$salt = Hash::salt(32);

			try{

				$user->create(array(

					'username'=>Input::get('username'),
					
					'password'=>Hash::make(Input::get('password'), $salt),
					
					'salt'=>$salt,
					
					'name'=>Input::get('name'),
					
					'joined'=>date('Y-m-d H:i:s'),
					
					'group_id'=>1

				));

				Session::flash('home', 'te recontra registrassste!');

				header('Location:index.php');

			}catch(Exception $e){

				die($e->getMessage());
			}

		}else{

			foreach($validation->errors() as $error){

				echo "{$error}<br>"; 

			}

		}

	}

}

?>

<form action="" method="post">

	<div class="field">

		<label for ="username">username</label>

		<input type="text" name="username" id="username" value="<?php echo scape(Input::get('username'))?>" autocomplete="off">

	</div>

	<div class="field">

		<label for="password">Choose a password</label>

		<input type="password" name="password" id="password">	

	</div>

	<div class="field">

		<label for="password_again">repeat your password</label>

		<input type="password" name="password_again" id="password_again">	

	</div>

	<div class="name">

		<label for="password_again">your name</label>

		<input type="text" name="name" id="name" value="<?php echo scape(Input::get('name'))?>">	

	</div>

	<input type="hidden" name="token" value="<?php echo Token::generate();?>">

	<input type="submit" value="enviar!">

</form>