<?php

/**
*
 * @summary list data for this app - Datatables info display - Server side
 *
 * @description - Data response for datatables list
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 */

class ListDataResponse{

  private $draw;
  
  private $row;
  
  private $rowPerPage;
  
  private $columnName;
  
  private $searchValue;
  
  private $searchQuery = "";

  private $searchColumnName = ""; //Buscar en "name" (ej)
  
  private $searchColumnNameTwo = "";// Buscar en Description (ej)

  private $columnNameForOrder = "id"; //Hardcoded - List order by
  
  private $columnSortOrder = 'desc';  //Hardcoded order to desc

  private $table;

  public function __construct($readArr=array(), $_tableName, $_columnName, $_columnNameTwo){

    $this->draw                     = $readArr["draw"];
    
    $this->row                      = $readArr["row"];
    
    $this->rowPerPage               = $readArr["rowPerPage"];
    
    $this->columnName               = $readArr["columnName"];
    
    $this->searchValue              = $readArr["searchValue"];

    $this->table                    = $_tableName;

    $this->searchColumnName         = $_columnName;
    
    $this->searchColumnNameTwo      = $_columnNameTwo;

  }

  /**
  *
  * getTotalRecords();
  * obtiene el numero total de resultados...
  **/
  
  private function getTotalRecords(){

    $totalRecordsQuery = DB::getInstance()->query("SELECT COUNT(*) AS allCount FROM ".$this->table." WHERE deleted = 0");
    
    $records = $totalRecordsQuery->results(true);

    $totalRecords = $records[0]['allCount'];

    return $totalRecords;

  }

  /**
  *
  * getTotalRecordsWithFilter();
  * obtiene el numero total de resultados con fitro...
  **/

  private function getTotalRecordsWithFilter(){

    if($this->searchValue != ''){

      if(!$this->searchColumnNameTwo==""){

        $this->searchQuery = " and (".$this->searchColumnName." like '%".$this->searchValue."%' or ".$this->searchColumnNameTwo." like '%".$this->searchValue."%' ) ";

      }else{

        $this->searchQuery = " and (".$this->searchColumnName." like '%".$this->searchValue."%') ";

      }

    }

    $sql = "SELECT COUNT(*) AS allCount FROM ".$this->table." WHERE deleted = 0 ".$this->searchQuery;
    
    $recordFilteredQuery = DB::getInstance()->query($sql);
    
    $records = $recordFilteredQuery->results(true);

    $totalRecordwithFilter = $records[0]['allCount'];

    return $totalRecordwithFilter;
  }

  /**
  *
  * getFetchRecords();
  * obtiene coincidencias
  **/

  private function getFetchRecords(){

    $sql = "SELECT * FROM " . $this->table . " WHERE deleted = 0 " . $this->searchQuery  ." order by ". $this->columnNameForOrder . " " . $this->columnSortOrder." limit ".$this->row.", ".$this->rowPerPage;

    $dataQuery = DB::getInstance()->query($sql);
    
    $dataList  = $dataQuery->results(true);
    
    $cantResultsFiltered = count($dataList);

    $count = 0;
    
    foreach($dataList as $id => $guetos){

      $count++;

      $dataList[$id]["order"] = $count;

    }

    return $dataList;
  }

  /**
  *
  * getTotalData();
  * array con todas las alternativas...
  **/

  public function getTotalData(){

    $response = array(

      "draw"                => intval($this->draw),

      "recordsTotal"        => $this->getTotalRecords(),

      "recordsFiltered"     => $this->getTotalRecordsWithFilter(),

      "data"                => $this->getFetchRecords()

    );

    return $response;
  }
}


?>
