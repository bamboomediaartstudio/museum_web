<?php

/**
*
 * @summary list data for this app - Datatables info display - Server side
 *
 * @description - Data response for datatables list
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 */

class ListDataResponseAltern{

  private $draw;
  
  private $row;
  
  private $rowPerPage;

  private $from;

  private $to;
  
  private $columnName;
  
  private $searchValue;
  
  private $searchQuery = "";

  private $searchColumnName = ""; //Buscar en "name" (ej)
  
  private $searchColumnNameTwo = "";// Buscar en Description (ej)

  private $columnNameForOrder = "id"; //Hardcoded - List order by
  
  private $columnSortOrder = 'desc';  //Hardcoded order to desc

  private $table;

  public function __construct($readArr=array(), $_tableName, $_columnName, $_columnNameTwo, $_from, $_to){

    $this->draw                     = $readArr["draw"];
    
    $this->row                      = $readArr["row"];
    
    $this->rowPerPage               = $readArr["rowPerPage"];
    
    $this->columnName               = $readArr["columnName"];
    
    $this->searchValue              = $readArr["searchValue"];

    $this->table                    = $_tableName;

    $this->searchColumnName         = $_columnName;
    
    $this->searchColumnNameTwo      = $_columnNameTwo;

    $this->from                     = $_from;

    $this->to                       = $_to;

  }

  /**
  *
  * getTotalRecords();
  * obtiene el numero total de resultados...
  SELECT *, mbi.id as id FROM museum_booking_institutions as mbi inner join museum_booking_institutions_relations as mbir on mbi.id = mbir.id_institution inner join museum_booking as mb on mbir.id_booking = mb.id WHERE mb.date_start >= DATE('2019-03-10') AND mb.date_end <= DATE('2020-03-30') AND mbi.active = 1 AND mbi.deleted = 0 GROUP BY mbi.id ORDER BY mbi.id DESC
  **/
  
  private function getTotalRecords(){

    $totalRecordsQuery = DB::getInstance()->query('SELECT COUNT(*) AS allCount FROM museum_booking_institutions as mbi inner join museum_booking_institutions_relations as mbir on mbi.id = mbir.id_institution inner join museum_booking as mb on mbir.id_booking = mb.id WHERE DATE(mb.date_start) >="' . $this->from .   '" AND DATE(mb.date_end) <= "' . $this->to . '" AND mbi.active = 1 AND mbi.deleted = 0 GROUP BY mbi.id ORDER BY mbi.id DESC');

    //$records = $totalRecordsQuery->first()->allCount;
    $records = $totalRecordsQuery->count();

    return $records;

  }

  /**
  *
  * getTotalRecordsWithFilter();
  * obtiene el numero total de resultados con fitro...
  **/

  private function getTotalRecordsWithFilter(){

    if($this->searchValue != ''){

      if(!$this->searchColumnNameTwo==""){

        $this->searchQuery = " and (".$this->searchColumnName." like '%".$this->searchValue."%' or ".$this->searchColumnNameTwo." like '%".$this->searchValue."%' ) ";

      }else{

        $this->searchQuery = " and (".$this->searchColumnName." like '%".$this->searchValue."%') ";

      }

    }


    //$sql = "SELECT COUNT(*) AS allCount FROM ".$this->table." WHERE deleted = 0 ".$this->searchQuery;

    $sql = "SELECT COUNT(*) AS allCount FROM museum_booking_institutions as mbi inner join museum_booking_institutions_relations as mbir on mbi.id = mbir.id_institution inner join museum_booking as mb on mbir.id_booking = mb.id WHERE DATE(mb.date_start) >='" . $this->from .   "' AND DATE(mb.date_end) <= '" . $this->to . "' AND mbi.active = 1 AND mbi.deleted = 0  GROUP BY mbi.id ORDER BY mbi.id DESC";
    
    $recordFilteredQuery = DB::getInstance()->query($sql);
    
    $records = $recordFilteredQuery->count();

    //$totalRecordwithFilter = $records[0]['allCount'];

    return $records;//$totalRecordwithFilter;
  }

  /**
  *
  * getFetchRecords();
  * obtiene coincidencias
  **/

  private function getFetchRecords(){

    $sql = "SELECT * FROM museum_booking_institutions as mbi inner join museum_booking_institutions_relations as mbir on mbi.id = mbir.id_institution inner join museum_booking as mb on mbir.id_booking = mb.id WHERE DATE(mb.date_start) >='" . $this->from .   "' AND DATE(mb.date_end) <= '" . $this->to . "' AND mbi.active = 1 AND mbi.deleted = 0 " . $this->searchQuery  . " GROUP BY mbi.id ORDER BY mbi.id " . $this->columnSortOrder. " limit " . $this->row. ", " . $this->rowPerPage;

    $dataQuery = DB::getInstance()->query($sql);
    
    $dataList  = $dataQuery->results(true);
    
    $cantResultsFiltered = count($dataList);

    $count = 0;
    
    foreach($dataList as $id => $guetos){

      $count++;

      $dataList[$id]["order"] = $count;

    }

    return $dataList;
  }

  /**
  *
  * getTotalData();
  * array con todas las alternativas...
  **/

  public function getTotalData(){

    $response = array(

      "draw"                => intval($this->draw),

      "recordsTotal"        => $this->getTotalRecords(),

      "recordsFiltered"     => $this->getTotalRecordsWithFilter(),

      "data"                => $this->getFetchRecords()

    );

    return $response;
  }
}


?>
