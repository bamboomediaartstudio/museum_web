<?php

/**
*
 * @summary list data for this app - Datatables info display - Server side
 *
 * @description - Data response for datatables list
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 */

class ListDataResponseGroups{

  private $draw;
  
  private $row;
  
  private $rowPerPage;

  private $from;

  private $to;
  
  private $columnName;
  
  private $searchValue;
  
  private $searchQuery = "";

  private $searchColumnName = ""; //Buscar en "name" (ej)
  
  private $searchColumnNameTwo = "";// Buscar en Description (ej)

  private $columnNameForOrder = "id"; //Hardcoded - List order by
  
  private $columnSortOrder = 'desc';  //Hardcoded order to desc

  private $table;

  public function __construct($readArr=array(), $_tableName, $_columnName, $_columnNameTwo, $_date, $_type){

    $this->draw                     = $readArr["draw"];
    
    $this->row                      = $readArr["row"];
    
    $this->rowPerPage               = $readArr["rowPerPage"];
    
    $this->columnName               = $readArr["columnName"];
    
    $this->searchValue              = $readArr["searchValue"];

    $this->table                    = $_tableName;

    $this->searchColumnName         = $_columnName;
    
    $this->searchColumnNameTwo      = $_columnNameTwo;

    $this->date                     = $_date;

    $this->type                     = $_type;

  }

  /**
  *
  * getTotalRecords();
  * obtiene el numero total de resultados......
  **/
  
  private function getTotalRecords(){

    $totalRecordsQuery = DB::getInstance()->query('SELECT * FROM museum_booking WHERE sid = ? AND DATE(date_start) = ? AND  active = ? AND deleted = ?', [$this->type, $this->date, 1, 0]);

    $records = $totalRecordsQuery->count();

    return $records;

  }

  /**
  *
  * getTotalRecordsWithFilter();
  * obtiene el numero total de resultados con fitro...
  **/

  private function getTotalRecordsWithFilter(){

    if($this->searchValue != ''){

      if(!$this->searchColumnNameTwo==""){

        $this->searchQuery = " and (".$this->searchColumnName." like '%".$this->searchValue."%' or ".$this->searchColumnNameTwo." like '%".$this->searchValue."%' ) ";

      }else{

        $this->searchQuery = " and (".$this->searchColumnName." like '%".$this->searchValue."%') ";

      }

    }


    $sql = "SELECT * FROM museum_booking WHERE sid = ? AND DATE(date_start) = ? AND  active = ? AND deleted = ? " . $this->searchQuery;
    
    $recordFilteredQuery = DB::getInstance()->query($sql, [$this->type, $this->date, 1, 0]);
    
    $records = $recordFilteredQuery->count();

    return $records;
  }

  /**
  *
  * getFetchRecords();
  * obtiene coincidencias
  **/

  private function getFetchRecords(){

    $sql = "SELECT *, mb.id as museum_booking_id FROM museum_booking as mb 

      LEFT JOIN museum_booking_institutions_relations as mbir on

      mb.id = mbir.id_booking 

      LEFT JOIN museum_booking_institutions as mbi on

      mbir.id_institution = mbi.id

      LEFT JOIN museum_booking_guides as mbg on

      mb.id = mbg.id_booking

      LEFT JOIN museum_guides as mg on

      mg.id = mbg.id_guide

      WHERE mb.sid = " . $this->type . " AND DATE(mb.date_start) = '" . $this->date .  "' AND  mb.active = 1 AND mb.deleted = 0 " . $this->searchQuery . " ORDER BY mb.date_start ASC limit " . $this->row. ", " . $this->rowPerPage;


    $dataQuery = DB::getInstance()->query($sql);
    
    $dataList  = $dataQuery->results(true);
    
    $cantResultsFiltered = count($dataList);

    $count = 0;
    
    foreach($dataList as $id => $guetos){

      $count++;

      $dataList[$id]["order"] = $count;

    }

    return $dataList;
  }

  /**
  *
  * getTotalData();
  * array con todas las alternativas...
  **/

  public function getTotalData(){

    $response = array(

      "draw"                => intval($this->draw),

      "recordsTotal"        => $this->getTotalRecords(),

      "recordsFiltered"     => $this->getTotalRecordsWithFilter(),

      "data"                => $this->getFetchRecords()

    );

    return $response;
  }
}


?>
