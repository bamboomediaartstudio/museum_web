<?php

include('../../../core/init.php');

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

header("Content-type: text/xml");

$query = DB::getInstance()->query('

				SELECT *, mc.id 							as mid, 

				img.unique_id 								as uid,

				mc.description 								as itemDesc

				FROM museum_courses 						as mc

				LEFT JOIN museum_images 					as img 		

				ON mc.id = img.sid AND img.source 			= ?

				AND img.deleted 							= ?

				WHERE mc.deleted 							= ? 

				AND mc.active 								= ? 

				ORDER by mid dESC', 

				(array('courses', 0, 0, 1)));


$xml = '<data>';

$xml .= '<courses>';

foreach($query->results() as $course){ 

	$xml .= '<course>';
	
	$xml .= '<id><![CDATA[' .  $course->mid . ']]></id>';

	$xml .= '<url><![CDATA[' .  $course->url . ']]></url>';
	
	$xml .= '<content><![CDATA[' .  $course->itemDesc . ']]></content>';
	
	$xml .= '<observations><![CDATA[' .  $course->observations . ']]></observations>';

	$xml .= '<inscription><![CDATA[' .  $course->allow_enrollment . ']]></inscription>';

	$xml .= '<title><![CDATA[' .  $course->name . ']]></title>';

	if($course->show_duration){ $xml .= '<duration><![CDATA[' .  $course->duration . ']]></duration>'; }

	if($course->show_price){ $xml .= '<price><![CDATA[' .  $course->price . ']]></price>'; }

	$xml .= '<pdf><![CDATA[' .  $course->allow_program_download . ']]></pdf>';

	$xml .= '<date>';

	$xml .= '<added><![CDATA[' . utf8_encode(strftime("%A, %d de %B del %Y", strtotime($course->added))) . ']]></added>';
	
	$startAtTime = date('Y/m/d h:i:s', strtotime($course->start_date));

	$endAtTime = date('Y/m/d h:i:s', strtotime($course->end_date));

	$xml .= '<startAt date="' . $startAtTime . '"><![CDATA[' . utf8_encode(strftime("%A, %d de %B del %Y", strtotime($course->start_date))) . ']]></startAt>';

	$xml .= '<endAt date="'. $endAtTime .'"><![CDATA[' . utf8_encode(strftime("%A, %d de %B del %Y", strtotime($course->end_date))) . ']]></endAt>';

	$now = date('Y-m-d');

	if($course->start_date > $now) { $next = 1; }

	if($course->end_date < $now) {  $next =  0; }

	$xml .= '<next>' . $next . '</next>';
	
	$xml .= '</date>';

	$xml .= '<contact>';
	
	$xml .= '<email><![CDATA[' .  $course->email . ']]></email>';

	$xml .= '<whatsapp><![CDATA[' .  $course->whatsapp . ']]></whatsapp>';

	$xml .= '<phone><![CDATA[' .  $course->phone . ']]></phone>';

	$xml .= '<facebook_group><![CDATA[' .  $course->facebook_group . ']]></facebook_group>';

	$xml .= '</contact>';

	$xml .= '<geolocation>';

	$xml .= '<lat><![CDATA[' .  $course->lat . ']]></lat>';
	
	$xml .= '<long><![CDATA[' .  $course->long . ']]></long>';
	
	$xml .= '<address><![CDATA[' .  $course->address . ']]></address>';

	$xml .= '</geolocation>';

	if($course->mid != null && $course->sid != null){

		$myImg =  $course->mid . '/' . $course->uid . '/' . $course->mid . '_' . $course->uid . '_small_sq.jpeg';

		$xml .= '<image><![CDATA[' . $myImg . ']]></image>'; 

	}

	//...

	$xml.= '<types>';

	$filterQuery = DB::getInstance()->query(

		'SELECT * FROM museum_courses_modalities_relations as mcmr 

		INNER JOIN museum_courses_modalities as mcm ON mcmr.id_modality = mcm.id 

		WHERE mcmr.id_course = ?', [$course->mid]);

		foreach($filterQuery->results() as $cat){

			$xml .= '<type>' . $cat->id . '</type>';

		}

	$xml.= '</types>';


	//...

	$xml .= '</course>';

}

$xml .='</courses>';

$xml .='<types>';

$typesQuery = DB::getInstance()->query('SELECT * FROM museum_courses_modalities WHERE active = ? AND deleted = ?', [1, 0]);

foreach($typesQuery->results() as $type){ 

	$xml .='<types>';

	$xml .= '<id><![CDATA['. $type->id.']]></id>';

	$xml .= '<type><![CDATA[' .  $type->course_modality . ']]></type>';
	
	$xml .='</types>';

}

$xml .='</types>';

$xml .='</data>';

echo $xml;

?>