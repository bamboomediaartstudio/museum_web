<?php

  require('../../../museum/trivias/Trivias.class.php');
  require('../../../museum/random_facts/RandomFacts.class.php');
  require('../../../museum/apps/films/Films.class.php');

  date_default_timezone_set ('America/Argentina/Buenos_Aires');

  setlocale(LC_TIME, 'es_ES', 'esp_esp');

  //require('../../../core/init.php');
  include($_SERVER['DOCUMENT_ROOT'].'/museumSmartAdmin/dist/default/private/users/core/init.php');

  $db = DB::getInstance();

  $finalArray = array();

  //******************************************************************************************
  //TODO:: corregir en la db el campo "range" ( es una palabra clave y no se puede usar)!!!!!
  //******************************************************************************************

  $sqlCrminals = "SELECT name, surname, birth_date, birth_place, death_date, death_place, nationality AS id_country,
                         branch, party, nickname, unity, bio, picture, highlighted, was_in_argentina
                  FROM app_museum_criminals
                  WHERE active = ? AND deleted = ?";

  $criminalsQuery = $db->query($sqlCrminals, [1, 0]);

  $arrayCriminals = $criminalsQuery->results(true);


  /*********************************************************/
  // Get Trivias - preguntas y respuestas

  $objTrivias = new Trivias();

  $arrTrivias = $objTrivias->getTriviasByCategory(6);


  //*********************************************************//
  // Get Sabias Que - (Random Facts)

  $objRandomFacts = new RandomFacts();
  $arrayRF = $objRandomFacts->getFactsAndFeedback(6);

  //*********************************************************//
  // Get Films

  $objFilms = new Films();

  $arrayFilms = $objFilms->getFilmsByCatRawData(6);


  $finalArray["criminals"]      = $arrayCriminals;
  $finalArray["trivias"]        = $arrTrivias;
  $finalArray["random_facts"]   = $arrayRF;
  $finalArray["films"]          = $arrayFilms;


  echo "<pre>";
  print_r($finalArray);
  echo "</pre>";

//echo json_encode(array_values($finalArray));

?>
