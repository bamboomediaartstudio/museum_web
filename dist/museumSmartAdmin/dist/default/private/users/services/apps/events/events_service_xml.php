<?php

include('../../../core/init.php');

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

header("Content-type: text/xml");

$query = DB::getInstance()->query('

				SELECT *, mc.id 										as mid, 

				mc.description 											as event_description,

				img.unique_id 											as uid 

				FROM museum_events 										as mc

				LEFT JOIN museum_short_urls 							as short

				ON mc.id = short.sid and short.source = 				?

				LEFT JOIN museum_images 								as img 		

				ON mc.id = img.sid AND img.source 						= ?

				AND img.deleted 										= ?

				WHERE mc.deleted 										= ? 

				AND mc.active 											= ? 

				ORDER by mc.start_date dESC', 

				(array('events', 'events', 0, 0, 1)));


$xml = '<data>';

$xml .= '<events>';

foreach($query->results() as $event){ 

	$xml .= '<event>';
	
	$xml .= '<id><![CDATA[' .  $event->mid . ']]></id>';

	$xml .= '<url><![CDATA[' .  $event->url . ']]></url>';

	$xml .= '<inscription><![CDATA[' .  $event->is_with_inscription . ']]></inscription>';

	$xml .= '<shortUrl><![CDATA[' .  $event->short_url . ']]></shortUrl>';

	$xml .= '<title><![CDATA[' .  $event->name . ']]></title>';

	$xml .= '<date>';

	$xml .= '<added><![CDATA[' . utf8_encode(strftime("%A, %d de %B del %Y", strtotime($event->added))) . ']]></added>';
	
	$startAtTime = date('Y/m/d h:i:s', strtotime($event->start_date));

	$endAtTime = date('Y/m/d h:i:s', strtotime($event->end_date));

	$xml .= '<startAt date="' . $startAtTime . '"><![CDATA[' . utf8_encode(strftime("%A, %d de %B del %Y", strtotime($event->start_date))) . ']]></startAt>';

	$xml .= '<endAt date="'. $endAtTime .'"><![CDATA[' . utf8_encode(strftime("%A, %d de %B del %Y", strtotime($event->end_date))) . ']]></endAt>';
	
	$xml .= '</date>';

	$xml .= '<content><![CDATA[' .  $event->event_description . ']]></content>';
	
	$xml .= '<observations><![CDATA[' .  $event->observations . ']]></observations>';


	$xml .= '<geolocation>';

	$xml .= '<lat><![CDATA[' .  $event->lat . ']]></lat>';
	
	$xml .= '<long><![CDATA[' .  $event->long . ']]></long>';

	$xml .= '</geolocation>';

	
	if($event->mid != null && $event->sid != null){

		$myImg =  $event->mid . '/' . $event->uid . '/' . $event->mid . '_' . $event->uid . '_small_sq.jpeg';

		$xml .= '<image><![CDATA[' . $myImg . ']]></image>'; 

	}

	$now = new DateTime();

	$checkStartDateTime = new DateTime($event->start_date);

	$endDate = new DateTime($event->end_date); 

	$xml.= '<types>';

	//finalizado...

	if($checkStartDateTime < $now && $endDate < $now)  $xml .='<type>1</type>';

	//en curso...

	if($checkStartDateTime < $now && $now < $endDate) $xml .= '<type>2</type>';

	//futuro

	if($checkStartDateTime > $now)  $xml .='<type>3</type>';
	
	//highlithed...

	if($event->is_highlighted == 1)  $xml .='<type>4</type>';

	$xml.= '</types>';

	$xml .= '</event>';

}

$xml .='</events>';

$xml .='<types>';

	$xml .='<type id="1">PASADOS</type>';

	$xml .='<type id="2">EN CURSO</type>';
	
	$xml .='<type id="3">PRÓXIMOS</type>';

	$xml .='<type id="4">DESTACADOS</type>';

$xml .='</types>';

$xml .='</data>';

echo $xml;

?>