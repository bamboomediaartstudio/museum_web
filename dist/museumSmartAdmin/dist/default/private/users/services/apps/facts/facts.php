<?php

include('../../../core/init.php');

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

header("Content-type: text/xml");

$query = DB::getInstance()->query('SELECT * FROM app_museum_categories');

$xml = '<data>';

foreach($query->results() as $category){ 

//	$category->name

	$myvalue = $category->name;

	$arr = explode(' ',trim($myvalue));

	$value =  $arr[0];

	$xml .= '<facts descriptor="'. $value . '">';

	$contentQuery = DB::getInstance()->query('SELECT *, mrf.id as main_id, amc.name as descriptor FROM museum_random_facts as mrf 

	INNER JOIN museum_random_facts_categories_relations as mrfcr on mrf.id = mrfcr.id_fact 

	INNER JOIN app_museum_categories as amc ON mrfcr.id_category = amc.id WHERE mrf.active = ? AND mrf.deleted = ? AND amc.name = ?', 

	(array(1, 0, $category->name)));

	foreach($contentQuery->results() as $content){

		$xml .= '<fact>';
		
		$xml .= '<id>' . $content->main_id . '</id>';

		$xml .= '<content><![CDATA[' . $content->description . ']]></content>';

		$feedbackQuery = DB::getInstance()->query('SELECT * FROM museum_random_facts_feedback_show_relations AS mrffsr inner join museum_feedback_options as mfo ON mrffsr.id_feedback_option = mfo.id WHERE mrffsr.id_fact = ?', (array($content->main_id)));

		$xml .= '<options>';

		foreach($feedbackQuery->results() as $feedback){
			
			$xml .= '<option>';

			$xml .= '<id>' . $feedback->id . '</id>';
			
			$xml .= '<title>' . $feedback->title . '</title>';
			
			$xml .= '<icon>' . $feedback->icon . '</icon>';

			$xml .= '</option>';
		
		}

		$xml .= '</options>';

		$xml .= '</fact>';
	}

	$xml .= '</facts>';	

	}

$xml .='</data>';

echo $xml;

?>