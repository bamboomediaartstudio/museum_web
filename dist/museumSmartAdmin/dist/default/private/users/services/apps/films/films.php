<?php

include('../../../core/init.php');

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

header("Content-type: text/xml");

$query = DB::getInstance()->query('SELECT * FROM app_museum_categories');

$xml = '<data>';

foreach($query->results() as $category){ 

//	$category->name

	$myvalue = $category->name;

	$arr = explode(' ',trim($myvalue));

	$value =  $arr[0];

	$xml .= '<films descriptor="'. $value . '">';

	$contentQuery = DB::getInstance()->query('SELECT *, amf.id as main_id, amc.name as descriptor FROM app_museum_films as amf 

	INNER JOIN app_museum_films_relations as amfr on amf.id = amfr.id_film 

	LEFT JOIN world_location_countries as wlc on amf.id_country = wlc.id

	INNER JOIN app_museum_categories as amc ON amfr.id_category = amc.id WHERE amf.active = ? AND amf.deleted = ? AND amc.name = ?', 

	(array(1, 0, $category->name)));

	foreach($contentQuery->results() as $content){

		$xml .= '<film>';
		
		$xml .= '<id>' . $content->main_id . '</id>';

		$xml .= '<title>' . $content->title . '</title>';
		
		$xml .= '<picture><![CDATA[' . $content->picture . ']]></picture>';

		$xml .= '<synopsis><![CDATA[' . $content->synopsis . ']]></synopsis>';
		
		$xml .= '<description><![CDATA[' . $content->description . ']]></description>';
		
		$xml .= '<director><![CDATA[' . $content->director . ']]></director>';

		$xml .= '<year><![CDATA[' . $content->year . ']]></year>';

		$xml .= '<country><![CDATA[' . $content->name_es . ']]></country>';

		$xml .= '<links>';
		
		$xml .= '<link category="netflix"><![CDATA[' . $content->link_netflix . ']]></link>';

		$xml .= '<link category="youtube"><![CDATA[' . $content->link_youtube . ']]></link>';

		$xml .= '<link category="imdb"><![CDATA[' . $content->link_imdb . ']]></link>';

		$xml .= '<link category="other"><![CDATA[' . $content->link_other . ']]></link>';

		$xml .= '</links>';

		$xml .= '</film>';
	}

	$xml .= '</films>';	

	}

$xml .='</data>';

echo $xml;

?>