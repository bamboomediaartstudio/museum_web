<?php

include('../../../core/init.php');

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

header("Content-type: text/xml");

$newsQuery = DB::getInstance()->query('SELECT *, amm.id as main_id, amm.name as person_name FROM app_museum_mapping as amm INNER JOIN world_location_countries as wlc ON amm.nationality = wlc.id WHERE amm.active = ? AND amm.deleted = ? order by amm.id DESC', (array(1, 0)));

$xml = '<data>';

foreach($newsQuery->results() as $mapping){ 
	
	$xml .= '<word id = "' . $mapping->main_id . '" country = "' . $mapping->name_es .  '">' .  $mapping->person_name . ' ' . $mapping->surname . '</word>';
}

$xml .= '</data>';

echo $xml;

?>