<?php

include('../../../core/init.php');

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

header("Content-type: text/xml");

$newsQuery = DB::getInstance()->query('

	SELECT *, mc.id as mid, img.unique_id 		as uid 

	FROM museum_news 							as mc

	LEFT JOIN museum_images 					as img 		

	ON mc.id = img.sid AND img.source 			= ?

	AND img.deleted 							= ?

	WHERE mc.deleted 							= ? 

	AND mc.active 								= ? 

	ORDER by added DESC', 

	(array('news', 0, 0, 1))
);


$xml = '<data>';

$xml .= '<news>';

foreach($newsQuery->results() as $new){ 

	$xml .= '<new>';
	
	$xml .= '<id><![CDATA[' .  $new->mid . ']]></id>';

	$xml .= '<url><![CDATA[' .  $new->url . ']]></url>';

	$xml .= '<title><![CDATA[' .  $new->title . ']]></title>';

	$xml .= '<added><![CDATA[' . utf8_encode(strftime("%A, %d de %B del %Y", strtotime($new->added))) . ']]></added>';

	$xml .= '<caption><![CDATA[' .  $new->caption . ']]></caption>';

	$xml .= '<content><![CDATA[' .  $new->content . ']]></content>';

	
	if($new->mid != null && $new->sid != null){

		$myImg =  $new->mid . '/' . $new->uid . '/' . $new->mid . '_' . $new->uid . '_small_sq.jpeg';

		$xml .= '<image><![CDATA[' . $myImg . ']]></image>'; 

	}

	$newCategoriesQuery = DB::getInstance()->query('SELECT * FROM museum_news_types_relations WHERE id_news = ? AND deleted = ?', array($new->mid, 0));

	$xml.= '<types>';

	foreach($newCategoriesQuery->results() as $cat){

		$xml .= '<type>' . $cat->id_type . '</type>';

	}

	$xml.= '</types>';

	$xml .= '</new>';

}

$xml .='</news>';

$xml .='<types>';

$typesQuery = DB::getInstance()->query('SELECT * FROM museum_news_types WHERE active = ? AND deleted = ?', [1, 0]);

foreach($typesQuery->results() as $type){ 

	$xml .='<types>';

	$xml .= '<id><![CDATA['. $type->id.']]></id>';

	$xml .= '<type><![CDATA[' .  $type->type . ']]></type>';
	
	$xml .='</types>';

}

$xml .='</types>';

$xml .='</data>';

echo $xml;

?>