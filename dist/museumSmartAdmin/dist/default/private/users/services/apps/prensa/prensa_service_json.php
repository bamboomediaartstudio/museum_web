<?php

//GENERATE JSON
//Prensa Service : Get data from prensa db, GENERATE JSON  file

//echo $_SERVER['DOCUMENT_ROOT']."/museumSmartAdmin/dist/default/private/users/core/init";


  date_default_timezone_set ('America/Argentina/Buenos_Aires');

  setlocale(LC_TIME, 'es_ES', 'esp_esp');

  //require('../../../core/init.php');
  include($_SERVER['DOCUMENT_ROOT'].'/museumSmartAdmin/dist/default/private/users/core/init.php');

  $db = DB::getInstance();

  //Get all active newspapers

  $sqlNewspapers = "SELECT id, name FROM app_museum_prensa_newspapers WHERE active = ? AND deleted = ?";

  $newspapersQuery = $db->query($sqlNewspapers, [1, 0]);

  $newspapersList = $newspapersQuery->results(true);

  //Re-order newspapers index
  $newspapersArray = array();

  foreach($newspapersList as $id => $newspaper){

    $newspapersArray[$newspaper["id"]] = array(
                                                "id"        => $newspaper["id"],
                                                "newspaper" => $newspaper["name"]
                                              );

  }


//Get prensa and keywords data

  $sql = "SELECT app_museum_prensa.id AS idPrensa, app_museum_prensa.title AS prensaName, app_museum_prensa.id_newspaper, app_museum_prensa.date AS prensaDate, app_museum_prensa.picture, app_museum_prensa_keywords.id AS idKey, app_museum_prensa_keywords.name AS keywordName,
            app_museum_prensa_keywords_relations.id AS idRelation
FROM app_museum_prensa
LEFT JOIN app_museum_prensa_keywords_relations
ON app_museum_prensa.id = app_museum_prensa_keywords_relations.id_prensa AND app_museum_prensa_keywords_relations.active = ? AND app_museum_prensa_keywords_relations.deleted = ?
LEFT JOIN app_museum_prensa_keywords
ON app_museum_prensa_keywords_relations.id_keyword = app_museum_prensa_keywords.id WHERE app_museum_prensa.active = ? AND app_museum_prensa.deleted = ? AND app_museum_prensa.picture != ?
ORDER BY app_museum_prensa.date ASC";




  $prensaQuery = $db->query($sql, [1, 0, 1, 0, ""]);

  $prensaList = $prensaQuery->results(true);

  $finalArray = array();

  $keywordsArray = array();

  //Group all keywords
  foreach($prensaList as $id => $prensaData){

    //adding newspaper name
    //group prensa data by idPrensa
    $finalArray[$prensaData["idPrensa"]] = array(
                                                      "title"         => $prensaData["prensaName"],
                                                      "id_newspaper"  => $prensaData["id_newspaper"],
                                                      "newspaper"     => $newspapersArray[$prensaData["id_newspaper"]]["newspaper"],
                                                      "date"          => $prensaData["prensaDate"],
                                                      "picture"       => $prensaData["picture"],
                                                      "keywords"      => array()
                                                  );
    //Adding keywordS
    //group keywords data by idPrensa and idRelation
    $keywordsArray[$prensaData["idPrensa"]][$prensaData["idRelation"]] = array(
                                                                                  "idRelation"  => $prensaData["idRelation"],
                                                                                  "idKey"       => $prensaData["idKey"],
                                                                                  "keyName"     => $prensaData["keywordName"]
                                                                              );

  }

  //Merge all
  foreach($keywordsArray as $idPrensa => $data){

    foreach($data as $idRelation => $keywords){

      $finalArray[$idPrensa]["keywords"][$idRelation] = array(
                                                                "idRelation"  => $keywords["idRelation"],
                                                                "idKey"       => $keywords["idKey"],
                                                                "keyName"     => $keywords["keyName"]
                                                              );

    }

  }

//
// echo "<pre>";
//   //print_r($newspapersArray);
//   //print_r($keywordsArray);
//   //print_r($prensaList);
// print_r($finalArray);
// echo "</pre>";



echo json_encode(array_values($finalArray));

//Create json file
/*
$fp = fopen('json/prensa_service_json.json', 'w');
fwrite($fp, json_encode(array_values($finalArray)));
fclose($fp);
*/
?>
