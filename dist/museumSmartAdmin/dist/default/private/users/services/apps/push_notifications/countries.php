<?php

include('../../../core/init.php');

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

header("Content-type: text/xml");

$query = DB::getInstance()->query('SELECT * FROM world_location_countries');

$xml = '<countries>';

foreach($query->results() as $data){ 

	$xml .= '<country>';

		$xml .= '<id>' . $data->id . '</id>';

		$xml .= '<name>' . $data->name_es . '</name>';

		$xml .= '<code>' . $data->code . '</code>';



	$xml .= '</country>';
}

$xml .='</countries>';

echo $xml;

?>