<?php

include('../../../core/init.php');

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

header("Content-type: text/xml");

$query = DB::getInstance()->query('SELECT * FROM push_notifications WHERE active = ? AND deleted = ?', [1, 0]);

$xml = '<notifications>';

foreach($query->results() as $data){ 

	$xml .= '<notification>';

		$xml .= '<id>' . $data->id . '</id>';

		$xml .= '<internal_id>' . $data->internal_id . '</internal_id>';

		$xml .= '<title>' . $data->title . '</title>';

		$xml .= '<content>' . $data->content . '</content>';

		$xml .= '<added>' . $data->added . '</added>';

		$xml .= '<sent>' . $data->sent . '</sent>';

	$xml .= '</notification>';
}

$xml .='</notifications>';

echo $xml;

?>