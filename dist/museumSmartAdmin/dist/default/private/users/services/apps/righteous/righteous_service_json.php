<?php

include('../../../core/init.php');
require('../../../museum/apps/righteous/Righteous.class.php');
require('../../../museum/trivias/Trivias.class.php');
require('../../../museum/random_facts/RandomFacts.class.php');
require('../../../museum/apps/films/Films.class.php');

$db = DB::getInstance();

$totalArray = array();

$objRighteous = new Righteous();

$righteousArray = $objRighteous->getRighteous();

$righteousCountriesArray = $objRighteous->getRighteousCountries();

foreach($righteousArray as $key => $righteousData){

  $type = ($righteousData["is_righteous"] == 1) ? "justo" : "rescatador";
 
  $position = ($righteousData["is_righteous"] == 1) ? "justo entre las naciones" : "Rescatador";
 
  $is_alive = ($righteousData["zl"] == 1) ? "Si" : "No";

  $totalArray["righteous"][$righteousData["idRighteous"]] = array(

      "type"          => $type,
      "nombre"        => $righteousData["name"],
      "foto"          => $righteousData["picture"],
      "bandera"       => $righteousData["nationality"],
      "posicion"      => $position,
      "nacimiento"    => $righteousData["birth_date"],
      "muerte"        => $righteousData["death_date"],
      "profesion"     => $righteousData["profession"],
      "apodo"         => $righteousData["nickname"],
      "nacionalidad"  => $righteousData["countryName"],
      "religion"      => $righteousData["religion"],
      "reconocimiento"=> $righteousData["recognition_date"],
      "vive"          => $is_alive,
      "texto"         => $righteousData["bio"]

  );

}

$objTrivias = new Trivias();

$arrTrivias = $objTrivias->getTriviasByCategory(2);

$totalArray["preguntas"]["type"] = "preguntas";

foreach($arrTrivias as $key => $triviaData){

  $totalArray["preguntas"][$triviaData["idTrivia"]]  = array(

    "pregunta"  => $triviaData["trivia"]

  );

  foreach($triviaData["replies"] as $idReply => $replyData){

    $totalArray["preguntas"][$triviaData["idTrivia"]]["replies"][$idReply] = array(
      "reply"   => $replyData["reply"],
      "correct" => $replyData["correct"]
    );

  }

}

$objRandomFacts = new RandomFacts();

$arrayRF = $objRandomFacts->getFactsAndFeedback(2);

$totalArray["randomFacts"]["type"] = "tips";

$totalArray["randomFacts"]["tipsArray"] = $arrayRF;


//*********************************************************//
// Get Films

$objFilms = new Films();

$arrayFilms = $objFilms->getFilmsByCatRawData(2);

$totalArray["peliculas"] = $arrayFilms;

$totalArray["peliculas"]["type"] = "peliculas";

//*********************************************************//
// Get justos por pais

foreach($righteousCountriesArray as $key => $countriesData){

  $totalArray["countries"][] = array(
      "type"    => "nacion",
      "nombre"  => $countriesData["country"],
      "justos"  => $countriesData["total_righteous"],
      "texto"   => $countriesData["bio"]
  );

}

header('Content-Type: application/json');

echo json_encode($totalArray); 

?>
