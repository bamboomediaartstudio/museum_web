<?php

//GENERATE JSON
//Prensa Service : Get data from prensa db, GENERATE JSON  file

//echo $_SERVER['DOCUMENT_ROOT']."/museumSmartAdmin/dist/default/private/users/core/init";

date_default_timezone_set ('America/Argentina/Buenos_Aires');

setlocale(LC_TIME, 'es_ES', 'esp_esp');

include($_SERVER['DOCUMENT_ROOT'].'/museumSmartAdmin/dist/default/private/users/core/init.php');

$db = DB::getInstance();

$sql = "SELECT * FROM app_museum_survivors WHERE active = 1 AND deleted = 0";

$query = $db->query($sql, [1, 0]);

$list = $query->results(true);

$finalArray = array();

foreach($list as $id => $data){

  $st = strtotime($data["birth_date"]);

  $fileName = str_replace("/", "_", $data["picture"]);

  if($data["picture"] != ""){

    $path = "https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/private/sources/images/survivors/" . $data["picture"] . "/" . $fileName . "_uploaded.jpeg"; 

  }else{

    $path = "";

  }

  $finalArray[$data["id"]] = array(

    "first_name" => $data["name"],
    
    "last_name" => $data["surname"],

    "birthdate" => strftime("%d de %B de %Y", $st),

    "bio" => strip_tags($data["bio"]),

    "photo" => $path

      );
  }




echo json_encode(array_values($finalArray));

?>
