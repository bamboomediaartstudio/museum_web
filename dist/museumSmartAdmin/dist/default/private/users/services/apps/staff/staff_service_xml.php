<?php

include('../../../core/init.php');

header("Content-type: text/xml");

$staffQuery = DB::getInstance()->query('

	SELECT *, ms.id as mid, img.unique_id 			as uid 

	FROM museum_staff 								as ms

	LEFT JOIN museum_staff_roles 					as role  	ON role.id = ms.role 

	LEFT JOIN museum_images 						as img 		ON ms.id = img.sid AND img.source = ? 

	AND img.active = ? AND img.deleted = ? 

	', (array('staff', 1, 0))
);


$xml = '<data>';

$xml .= '<members>';

foreach($staffQuery->results() as $member){ 

	$xml .= '<member>';
	
	$xml .= '<name><![CDATA[' .  $member->name . ']]></name>';

	$xml .= '<surname><![CDATA[' .  $member->surname . ']]></surname>';

	$xml .= '<role><![CDATA[' .  $member->role . ']]></role>';

	$showEmail = $member->show_email_on_app;

	$showPhone = $member->show_phone_on_app;

	if($showEmail == 1 && !empty($member->email)){ $xml .= '<email><![CDATA[' .  $member->email . ']]></email>'; }

	if($showPhone == 1 && !empty($member->phone)){ $xml .= '<phone><![CDATA[' .  $member->phone . ']]></phone>'; }

	if($member->mid != null && $member->sid != null){

		$myImg =  $member->mid . '/' . $member->uid . '/' . $member->mid . '_' . $member->uid . '_edited.jpeg';

		$xml .= '<image><![CDATA[' . $myImg . ']]></image>'; 

	}

	$staffRelationsQuery = DB::getInstance()->query('SELECT * FROM museum_staff_categories_relations WHERE id_museum_staff_user = ? AND deleted = ?', array($member->mid, 0));

	$xml.= '<categories>';

	foreach($staffRelationsQuery->results() as $userCategories){

		$xml .= '<category order = "'. $userCategories->internal_order . '">' . $userCategories->id_museum_staff_category . '</category>';

	}

	$xml.= '</categories>';

	$xml .= '</member>';

}

$xml .='</members>';

$xml .='<categories>';

$staffMenuQuery = DB::getInstance()->query('SELECT * FROM museum_staff_categories WHERE active = ? AND deleted = ? ORDER BY internal_order ASC', [1, 0]);

foreach($staffMenuQuery->results() as $staffMenu){ 

	$xml .='<category>';

	$xml .= '<id><![CDATA['. $staffMenu->id.']]></id>';

	$xml .= '<name><![CDATA[' .  $staffMenu->category_name . ']]></name>';

	$xml .= '<order><![CDATA['. $staffMenu->internal_order.']]></order>';
	
	$xml .='</category>';

}

$xml .='</categories>';

$xml .='</data>';

echo $xml;

?>