<?php

include('../../../core/init.php');

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

header("Content-type: text/xml");

$query = DB::getInstance()->query('SELECT * FROM app_museum_categories');

$xml = '<data>';

foreach($query->results() as $category){ 

	$myvalue = $category->name;

	$arr = explode(' ',trim($myvalue));

	$value =  $arr[0];

	$xml .= '<trivias descriptor="'. $value . '">';

	$contentQuery = DB::getInstance()->query('SELECT *, mt.id as main_id, amc.name as descriptor FROM museum_trivias as mt 

		INNER JOIN museum_trivias_categories_relations as mtcr on mt.id = mtcr.id_trivia 

		INNER JOIN app_museum_categories as amc ON mtcr.id_category = amc.id WHERE mt.active = ? AND mt.deleted =  ? AND amc.name = ?', 

	(array(1, 0, $category->name)));

	foreach($contentQuery->results() as $content){

		$xml .= '<trivia>';
		
		$xml .= '<id>' . $content->main_id . '</id>';

		$xml .= '<question><![CDATA[' . $content->trivia . ']]></question>';

		$feedbackQuery = DB::getInstance()->query('SELECT * FROM museum_trivias_custom_replies AS mtcr WHERE id_trivia = ?', 

			(array($content->main_id)));

		$xml .= '<answers>';

		foreach($feedbackQuery->results() as $feedback){

			$isCorrect = "false";

			if($feedback->correct == 1){

				$isCorrect = "true";

			}
			$xml .= '<answer isCorrect="'. $isCorrect . '"><![CDATA[';

			$xml .= $feedback->reply;

			$xml .= ']]></answer>';
		
		}

		$xml .= '</answers>';

		$xml .= '</trivia>';
	}

	$xml .= '</trivias>';	

	}

$xml .='</data>';

echo $xml;

?>