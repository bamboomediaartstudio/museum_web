<?php
/**
 * @summary Get app's last updates
 *
 * @description - Requires id app
 *
 * @author Mariano Makedonsky
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 */

require_once '../../../core/init.php';

$validate = new Validate();

//$validation = $validate->check($_POST, array('appId' => array('display'=> 'appId', 'required' => true)));
$validation = $validate->check($_POST, array('appId' => array('display'=> 'appId', 'required' => true)));

if(!$validation->passed()){

	$status['status'] = 1;

	$status['statusMessage'] = 'Validation not passed';

}else{

	$db = DB::getInstance();

	$date = date("Y-m-d");

	$chckQuery = $db->query("SELECT * FROM app_stats_interactions WHERE id_app = ? AND date = ?", 

		[Input::get('appId'), $date]);

	if($db->count() >=1){

		$updateQuery = $db->query("UPDATE app_stats_interactions SET  interactions = interactions + 1 WHERE id_app = ? AND date = ?", 

			[Input::get('appId'), $date]);

		$status['status'] = 2;

		$status['statusMessage'] = 'Data Updated';


	}else{

		$insertQuery = $db->insert('app_stats_interactions',['id_app'=> Input::get('appId'), 'date' => $date, 'interactions' => 1]);

		$status['status'] = 3;

		$status['statusMessage'] = 'Data insert';

	}

}

echo json_encode($status);
