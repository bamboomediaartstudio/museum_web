<?php

require_once('../../../private/users/museum/apps/timeline/connect.php');


//require_once '../core/init.php';

$sql = "SELECT timelineEvents.id, timelineEvents.date_start, timelineEvents.title, timelineEvents.description FROM timeline_events as timelineEvents WHERE timelineEvents.active = ? AND timelineEvents.deleted = ?";

$eventStmt = $conn->prepare($sql);

$active = 1;

$deleted = 0;

$eventStmt->bind_param('ii', $active, $deleted);

$eventStmt->execute();

$eventStmt->store_result();

$eventStmt->bind_result($id, $date, $title, $description);

$xml = new DOMDocument();

$xml->formatOutput = true;

$timeLineNode = $xml->createElement("timeline");

$xml->appendChild($timeLineNode);

if ($eventStmt->num_rows >= 1) {

	while($eventStmt->fetch()){

		$event = $xml->createElement("event");

		$event->setAttribute('id', $id);

		$timeLineNode->appendChild($event);

		//id...

		//$eventId = $xml->createElement("id");

		//$eventId->appendChild($xml->createCDataSection($id));

		//titulo...
		
		$eventTitle = $xml->createElement("title");

		$eventTitle->appendChild($xml->createCDataSection($title));

		//desceiption...
		
		$eventDescription = $xml->createElement("description");

		$eventDescription->appendChild($xml->createCDataSection($description));

		//date...

		$eventDate = $xml->createElement("date", $date);

		//add data...

		//$event->appendChild($eventId);
		
		$event->appendChild($eventTitle);

		$event->appendChild($eventDescription);

		$event->appendChild($eventDate);

		$actualId = $id;

		//parse images....

		$sql = "SELECT timeline_event_images.id, timeline_event_images.item_order, timeline_event_images.id_file, 

		timeline_event_images.extension, timeline_event_images.description, timeline_event_image_type.type, timeline_event_image_type.id

		FROM timeline_event_images INNER JOIN timeline_event_image_type 

		ON timeline_event_images.item_type = timeline_event_image_type.id

		WHERE timeline_event_images.id_event = ? AND timeline_event_images.active = ? AND timeline_event_images.deleted = ?";

		$imgStmt = $conn->prepare($sql);

		$active = 1;

		$deleted = 0;

		$imgStmt->bind_param('iii', $actualId, $active, $deleted);

		$imgStmt->execute();

		$imgStmt->store_result();

		$imgStmt->bind_result($idImage, $orderImage, $fileImage, $extensionImage, $descriptionImage, $itemType, $itemId);

		$images = $xml->createElement("images");

		if ($imgStmt->num_rows >= 1) {

			while($imgStmt->fetch()){

				$image = $xml->createElement("image");

				$image->setAttribute('id', $idImage);

				$image->setAttribute('order', $orderImage);

				$image->setAttribute('type', $itemType);

				$image->setAttribute('typeId', $itemId);

				$imageFile = $xml->createElement("file", $fileImage);

				$extension = $xml->createElement("extension", $extensionImage);

				$imageDescription = $xml->createElement("description");

				$imageDescription->appendChild($xml->createCDataSection($descriptionImage));

				$image->appendChild($imageFile);

				$image->appendChild($extension);

				$image->appendChild($imageDescription);

				$images->appendChild($image);

			}

			$event->appendChild($images);

		}



		//parse international....

		$sql = "SELECT date_start, title, description FROM timeline_event_international	 WHERE id_event = ? AND active = ? AND deleted = ?";

		$internationalStmt = $conn->prepare($sql);

		$active = 1;

		$deleted = 0;

		$internationalStmt->bind_param('iii', $actualId, $active, $deleted);

		$internationalStmt->execute();

		$internationalStmt->store_result();

		$internationalStmt->bind_result($internationalDateStart, $internationalTitle, $internationalDescription);

		$international = $xml->createElement("international");

		if ($internationalStmt->num_rows >= 1) {

			while($internationalStmt->fetch()){

				$title = $xml->createElement("title");

				$title->appendChild($xml->createCDataSection($internationalTitle));

				//...

				$description = $xml->createElement("description");

				$description->appendChild($xml->createCDataSection($internationalDescription));

				//...

				$date = $xml->createElement("date", $internationalDateStart);

				//...

				$international->appendChild($title);

				$international->appendChild($description);

				$international->appendChild($date);

			}

		}

		$event->appendChild($international);



		//parse national....

		$sql = "SELECT date_start, title, description FROM timeline_event_national WHERE id_event = ? AND active = ? AND deleted = ?";

		$nationalStmt = $conn->prepare($sql);

		$active = 1;

		$deleted = 0;

		$nationalStmt->bind_param('iii', $actualId, $active, $deleted);

		$nationalStmt->execute();

		$nationalStmt->store_result();

		$nationalStmt->bind_result($nationalDateStart, $nationalTitle, $nationalDescription);

		$national = $xml->createElement("national");

		if ($nationalStmt->num_rows >= 1) {

			while($nationalStmt->fetch()){

				$title = $xml->createElement("title");

				$title->appendChild($xml->createCDataSection($nationalTitle));

				//...

				$description = $xml->createElement("description");

				$description->appendChild($xml->createCDataSection($nationalDescription));

				//...

				$date = $xml->createElement("date", $nationalDateStart);

				//...

				$national->appendChild($title);

				$national->appendChild($description);

				$national->appendChild($date);

			}

		}

		$event->appendChild($national);

	}

}

$xml->save("events.xml");

$status['msg'] = 'timeline exported.';

echo json_encode($status);

die();

?>