<?php

require_once '../core/init.php';

$db = DB::getInstance();

$status = [];

$query = 'SELECT * FROM users WHERE active = ? AND deleted = ? AND group_id = ? OR is_host = ?';

$query = $db->query($query, [1, 0, 6, 1]);

if($query){

	//$status['items'][] = "";

	foreach ($db->results() as $result){

		$status['items'][$result->id] = $result->name . ' ' . $result->surname;

	}

}

header('Content-Type: application/json');

$json =  json_encode($status, JSON_PRETTY_PRINT);
 
 print_r($json);
?>