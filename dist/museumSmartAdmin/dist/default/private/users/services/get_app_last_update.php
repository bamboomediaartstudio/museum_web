<?php
/**
 * @summary Get app's last updates
 *
 * @description - Requires id app
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 */

require_once '../core/init.php';

$actualIdApp = Input::get('id_category');

if(Input::exists('getLastUpdate') && Input::get('id_category')){

  $db = DB::getInstance();

  $lastUpdateQuery = $db->query("SELECT id_category, last_update, active, deleted FROM app_museum_updates WHERE id_category = ? AND active = ? AND deleted = ?", [Input::get('id'), 1, 0]);

  if($lastUpdateQuery){

    $actualLastUpdate = $lastUpdateQuery->first();

    $status['error'] = 0;
    $status['errorMessage'] = '';
    $status['id_category'] = $actualLastUpdate->id_category;
    $status['last_update'] = $actualLastUpdate->last_update;


  }else{

    //No está esa categoria en la db (No deberia pasar)

  }

  echo json_encode($status);


}else{

  //validation to get data not passed
  $status['error'] = 1;
  $status['errorMessage'] = 'Validation not passed';

}




?>
