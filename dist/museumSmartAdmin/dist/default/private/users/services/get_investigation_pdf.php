<?php

require_once '../core/init.php';

$db = DB::getInstance();

$status = [];

$query = 'Select * FROM ' . Input::get('from') . ' WHERE id = ? AND deleted = ? ORDER BY internal_order ASC';

$query = $db->query($query, [Input::get('id'),  0]);

if($query){

	foreach ($db->results() as $record){

		$status[] = array(

			'id' => $record->id,

			'url' => $record->url,

			'internal_order' => $record->internal_order,

			'title' => $record->title,
		);

	}

}

header('Content-Type: application/json');

$json =  json_encode($status, JSON_PRETTY_PRINT);
 
 print_r($json);
?>