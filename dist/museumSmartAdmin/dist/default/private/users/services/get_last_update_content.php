<?php
/**
*
 * @summary Get date of latest update of content
 *
 * @description -
 *
 * @author Colo Baggins <colo@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 */

require_once '../core/init.php';



include '../museum/general/UpdateCategoriesDates.class.php';

//Get idCategory to look for app content
$source = "survivors";

$objUpdateCatDate = new UpdateCategoriesDates($source);


header("Content-type: application/json");
echo json_encode($objUpdateCatDate->getLastModifiedDate());



?>
