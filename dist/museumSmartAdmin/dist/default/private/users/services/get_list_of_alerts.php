<?php

require_once '../core/init.php';

$db = DB::getInstance();

$status = [];

$query =

	'SELECT * FROM museum_alerts 			as ma

	INNER JOIN museum_alerts_types 			as mat 

	ON 										ma.alert_type = mat.id

	WHERE ma.active = ? AND ma.deleted = ? ORDER BY ma.id ASC';

$alertsQuery = $db->query($query, [1, 0]);

if($alertsQuery){

	foreach ($db->results() as $alert){

		$status[] = array(

			'id' => $alert->id,

			'title' => $alert->title,

			'caption' => $alert->caption,

			'link' => $alert->link,

			'button' =>$alert->button_label,

			'is_blank' =>$alert->is_blank,

			'type' =>$alert->type,

			'background_color' =>$alert->background_color,

			'text_color' =>$alert->text_color

		);

	}

}

header('Content-Type: application/json');

$json =  json_encode($status, JSON_PRETTY_PRINT);
 
 print_r($json);
?>