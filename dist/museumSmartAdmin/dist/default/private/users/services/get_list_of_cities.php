<?php

require_once '../core/init.php';

$db = DB::getInstance();

$wordingQuery = $db->query("Select * FROM world_location_cities WHERE region_id = ?", [Input::get('id')]);

if($wordingQuery){

	foreach ($db->results() as $record){

		$status[] = array(

			'id' => $record->id,

			'value' =>$record->name

		);

	}

}

header('Content-Type: application/json');

$json =  json_encode($status, JSON_PRETTY_PRINT);
 
 print_r($json);
?>