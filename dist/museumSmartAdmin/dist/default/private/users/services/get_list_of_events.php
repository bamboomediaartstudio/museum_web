<?php

require_once '../core/init.php';

$db = DB::getInstance();

$status = [];

$query = 'SELECT *,  

mb.id as mid, 

mbet.id as category_id,

mb.active as event_active

FROM museum_booking as mb INNER JOIN museum_booking_event_type as mbet on mb.sid = mbet.id';

if(Input::get('condition') &&  Input::get('condition') == 'before'){

	$query .= ' WHERE date_start >= ? AND mb.deleted = ? ORDER BY date_start ASC';

	$query = $db->query($query, [date("Y-m-d"), 0]);

}else{

	$query = $db->query($query);

}

if($query){

	foreach ($db->results() as $record){

		$status[] = array(

			'id' => $record->mid,

			'category_id' => $record->category_id,

			'title' => $record->type,

			'date_start' => $record->date_start,

			'date_end' => $record->date_end,

			'className' => $record->color,

			'backgroundColor' => $record->background_color,

			'available_places' => $record->available_places,

			'booked_places' => $record->booked_places,
			
			'tickets_per_visitor' => $record->tickets_per_visitor,

			'event_active' => $record->event_active,

			'place' => $record->place,

			'notes' => $record->notes
		);

	}

}

header('Content-Type: application/json');

$json =  json_encode($status, JSON_PRETTY_PRINT);
 
 print_r($json);
?>