<?php

require_once '../core/init.php';

$db = DB::getInstance();

$status = [];

$query = 'Select * FROM ' . Input::get('from') . ' WHERE sid = ? and source = ? AND deleted = ? ORDER BY internal_order ASC';

$wordingQuery = $db->query($query, [Input::get('id'), Input::get('source'), 0]);

if($wordingQuery){

	foreach ($db->results() as $record){

		$status[] = array(

			'id' => $record->id,

			'sid' =>$record->sid,

			'unique_id' =>$record->unique_id,

			'internal_order' => $record->internal_order,

			'source' => $record->source,

			'description' => $record->description

		);

	}

}

header('Content-Type: application/json');

$json =  json_encode($status, JSON_PRETTY_PRINT);
 
 print_r($json);
?>