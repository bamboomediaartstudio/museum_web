<?php

/**
*
* FOR ALL GALLERIES FROM ALL APPS - NOT FOR SINGLE IMG
* FOR SINGLE IMAGE UPLOAD FILE AND DB UPDATE, USE APP ADD IN EACH FOLDER (users museum apps)
*
**/

require_once '../core/init.php';

$db = DB::getInstance();

$status = [];

$query = 'Select * FROM ' . Input::get('from') . ' WHERE ' . Input::get('field') . ' = ? AND deleted = ? ORDER BY internal_order ASC';

$wordingQuery = $db->query($query, [Input::get('id'), 0]);

if($wordingQuery){

	foreach ($db->results() as $img){

		$status[] = array(
			'id' 							=> $img->id,
			'path'						=> $img->path,
			'name'						=> $img->name,
			'extension'				=> $img->extension,
			'unique_id' 			=> $img->uniqueId,
			'internal_order'	=> $img->internal_order,
			'description'			=> $img->description
		);

	}

}

/*

$wordingQuery = $db->query($query, [Input::get('id'), Input::get('source'), 0]);

if($wordingQuery){

	foreach ($db->results() as $record){

		$status[] = array(

			'id' => $record->id,

			'sid' =>$record->sid,

			'unique_id' =>$record->unique_id,

			'internal_order' => $record->internal_order,

			'source' => $record->source,

			'description' => $record->description

		);

	}

}

*/

header('Content-Type: application/json');

$json =  json_encode($status, JSON_PRETTY_PRINT);

 print_r($json);
?>
