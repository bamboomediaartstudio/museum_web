<?php

require_once '../core/init.php';

$db = DB::getInstance();

$status = [];


$query = 'Select * FROM ' . Input::get('from') . ' WHERE ' . Input::get('field') . ' = ? AND deleted = ?';


$wordingQuery = $db->query($query, [Input::get('id'), 0]);

if($wordingQuery){

	foreach ($db->results() as $record){

		$status[] = array(

			'id' => $record->id,

			'picture' =>$record->picture,

			'internal_order' => $record->internal_order

		);

	}

}

header('Content-Type: application/json');

$json =  json_encode($status, JSON_PRETTY_PRINT);

 print_r($json);
?>
