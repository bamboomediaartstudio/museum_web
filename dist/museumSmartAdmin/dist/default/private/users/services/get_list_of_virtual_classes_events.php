<?php

require_once '../core/init.php';

$tomorrow = date("Y-m-d", time() + 86400);

$db = DB::getInstance();

$status = [];

if(Input::get('bookingType') == 1){

	$queryString = 'SELECT mvce.id, mvce.date_start, mvce.date_end, mvce.available_places, mvce.booked_places, u.name, u.surname FROM museum_virtual_classes_events AS mvce 

	LEFT JOIN museum_virtual_classes_events_guides_relations as mvcegr 

	ON mvce.id = mvcegr.id_event

	LEFT JOIN users as u 

	ON mvcegr.id_guide = u.id

	WHERE mvce.date_start > ? AND mvce.booking_type = ? AND mvce.active = ? AND mvce.deleted = ?';

	$query = $db->query($queryString, [$tomorrow, Input::get('bookingType'), 1, 0]);

}else{


	$queryString = 'SELECT mvce.id, mvce.date_start, mvce.date_end, mvce.available_places, mvce.booked_places, u.name, u.surname FROM museum_virtual_classes_events AS mvce 

	LEFT JOIN museum_virtual_classes_events_guides_relations as mvcegr 

	ON mvce.id = mvcegr.id_event

	LEFT JOIN users as u 

	ON mvcegr.id_guide = u.id

	WHERE mvce.sid = ? AND mvce.date_start > ? AND mvce.booking_type = ? AND mvce.active = ? AND mvce.deleted = ? AND mvce.booked_places < ?';

	$query = $db->query($queryString, [Input::get('id'), $tomorrow, Input::get('bookingType'), 1, 0, 90]);

}


if($query){

	foreach ($db->results() as $record){

		$status[] = array(

			'id' => $record->id,

			'booking_type' => Input::get('bookingType'),

			'date_start' => $record->date_start,

			'date_end' => $record->date_end,

			'available_places' => $record->available_places,

			'booked_places' => $record->booked_places,

			'guide_name' => $record->name

		);

	}

}

header('Content-Type: application/json');

$json =  json_encode($status, JSON_PRETTY_PRINT);
 
 print_r($json);
?>