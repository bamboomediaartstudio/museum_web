<?php

require_once '../core/init.php';

$db = DB::getInstance();

$status = [];

$query = 'SELECT * FROM museum_youtube_videos WHERE sid = ? and source = ? AND active = ? AND deleted = ? ORDER BY internal_order ASC';

$videosQuery = $db->query($query, [Input::get('id'), Input::get('source'), 1, 0]);

if($videosQuery){

	foreach ($db->results() as $record){

		$status[] = array(

			'id' => $record->id,

			'sid' =>$record->sid,

			'unique_id' =>$record->unique_id,

			'internal_order' => $record->internal_order,

			'description' => $record->description

		);

	}

}

header('Content-Type: application/json');

$json =  json_encode($status, JSON_PRETTY_PRINT);
 
 print_r($json);
?>