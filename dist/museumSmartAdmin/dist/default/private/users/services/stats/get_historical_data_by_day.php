<?php

require_once '../../core/init.php';

setlocale(LC_ALL,"es_ES");

$db = DB::getInstance();

$status = [];

$data = [];

if(Input::get('appId')){

	$actualId = Input::get('appId');

	$query = 'SELECT * FROM app_stats_interactions WHERE id_app = ' . $actualId;

}else{

	$query = 'SELECT * FROM app_stats_interactions';

}

$doQuery = $db->query($query);

$days = array("domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado");

$values = array_fill(0, count($days), 0);

if($doQuery){

	foreach ($db->results() as $item){

		$time = $item->date;

		$dayofweek = date('w', strtotime($time));

		$values[$dayofweek]++;
	}

}


array_push($data, $days);

array_push($data, $values);
	
header('Content-Type: application/json');

$json =  json_encode($data, JSON_PRETTY_PRINT);
 
 print_r($json);
 
?>