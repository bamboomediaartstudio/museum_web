<?php

require_once '../../core/init.php';

$db = DB::getInstance();

$status = [];

$data = [];

if(Input::get('appId')){

	$actualId = Input::get('appId');

	$query = 'SELECT * FROM app_stats_interactions WHERE id_app = ' . $actualId;

}else{

	$query = 'SELECT * FROM app_stats_interactions';

}

$doQuery = $db->query($query);

$hours = array("08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21");

$hoursAndMinutes = array("08:00 AM", "09:00 AM", "10:00 AM", "11:00 AM", "12:00 PM", "13:00 PM", "14:00 PM", "15:00 PM", "16:00 PM", "17:00 PM", "18:00 PM", "19:00 PM", "20:00 PM", "21:00 PM");

$values = array_fill(0, count($hours), 0);

if($doQuery){

	foreach ($db->results() as $item){

		$time = $item->date_time;

		$getHour = date("H",strtotime($time));

		$counter = 0;

		foreach ($hours as $hour){

			if($hour == $getHour){

				$values[$counter]++;

				continue;
			}

			$counter+=1;

		}
	}

}


array_push($data, $hoursAndMinutes);

array_push($data, $values);

header('Content-Type: application/json');

$json =  json_encode($data, JSON_PRETTY_PRINT);
 
 print_r($json);
 
?>