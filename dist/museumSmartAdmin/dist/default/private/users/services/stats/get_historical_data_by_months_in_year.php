<?php

require_once '../../core/init.php';

setlocale(LC_ALL,"es_ES");

$db = DB::getInstance();

$data = [];

$values = [];

if(Input::get('appId')){

	$actualId = Input::get('appId');

	$query = 'SELECT * FROM app_stats_interactions WHERE id_app = ' . $actualId;

}else{

	$query = 'SELECT * FROM app_stats_interactions';

}

$doQuery = $db->query($query);

$years = array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");
	
$values = array_fill(0, sizeof($years), 0);

if($doQuery){

	foreach ($db->results() as $item){

		$time = $item->date;

		$month = date('n', strtotime($time)) - 1;

		$values[$month]++;
	}

}


array_push($data, $years);

array_push($data, $values);
	
header('Content-Type: application/json');

$json =  json_encode($data, JSON_PRETTY_PRINT);
 
 print_r($json);
 
?>