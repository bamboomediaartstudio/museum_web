<?php

require_once '../../core/init.php';

setlocale(LC_ALL,"es_ES");

$db = DB::getInstance();

$data = [];

$days = [];

$values = [];

if(Input::get('appId')){

	$actualId = Input::get('appId');

	$query = 'SELECT * FROM app_stats_interactions WHERE id_app = ' . $actualId;

}else{

	$query = 'SELECT * FROM app_stats_interactions';

}

$doQuery = $db->query($query);

for($i=0; $i <date("t"); $i++){ array_push($days, ($i+1) . ' de ' . strftime("%B")); }

//for($i=0; $i <date("t"); $i++){ array_push($values, 1); }

$values = array_fill(0, date("t"), 0);

if($doQuery){

	foreach ($db->results() as $item){

		$time = $item->date;

		$dayofweek = (int)date('d', strtotime($time)) - 1;

		$values[$dayofweek]++;
	}

}


array_push($data, $days);

array_push($data, $values);
	
header('Content-Type: application/json');

$json =  json_encode($data, JSON_PRETTY_PRINT);
 
 print_r($json);
 
?>