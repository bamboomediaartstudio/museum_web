<?php

require_once '../../core/init.php';

$db = DB::getInstance();

$status = [];

$query =

	'SELECT app_level.level_name as actual_level, COUNT(interactions.date) as interactions_in_level FROM app_stats_levels as app_level 

LEFT JOIN app_stats as app ON app_level.level_id = app.app_level 

LEFT JOIN app_stats_interactions as interactions ON app.id = interactions.id_app

GROUP by app_level ASC';

$alertsQuery = $db->query($query);

if($alertsQuery){

	foreach ($db->results() as $alert){

		$status[] = array(

			'label' => $alert->actual_level,

			'value' => $alert->interactions_in_level

		);

	}

}

header('Content-Type: application/json');

$json =  json_encode($status, JSON_PRETTY_PRINT);
 
 print_r($json);
?>