<?php

require_once '../../core/init.php';

setlocale(LC_ALL,"es_ES");

$db = DB::getInstance();

$data = [];

$values = [];

$row = Input::get('row');

$query = 'SELECT ' . $row . ' as content_value FROM app_survey';

$doQuery = $db->query($query);

if($row == 'content_rate'){
	
	$values = array_fill(0, 10, 0);

	$test = array_fill(0, 10, 0);

}else if($row == 'time_in_museum'){

	$values = array_fill(0, 7, 0);

	$test = array_fill(0, 7, 0);

}else{

	$values = array_fill(0, 5, 0);

	$test = array_fill(0, 5, 0);

}



if($doQuery){

	foreach ($doQuery->results() as $item){ 

		$values[(int)$item->content_value - 1]++; 

	}

}


array_push($data, $values);
	
header('Content-Type: application/json');

$json =  json_encode($data, JSON_PRETTY_PRINT);
 
 print_r($json);
 
?>