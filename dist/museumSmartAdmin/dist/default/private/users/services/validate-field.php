/**
 * @summary Add new course.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

 Dropzone.autoDiscover = false;

/**
* @function MuseumCourseAdd
* @description Initialize and include all the methods of this class.
*/

var MuseumCourseAdd = function() {

    helper = Helper();

    var editMode;

    var imagePath;

    var redirectId;

    var courseId;

    var courseName;

    var addRedirectId = 0;

    var defaultImageWidth = 2000;

    var defaultImageHeight = 1000;

    var maxPDFSizeMB = 50;

    var maxPDFSizeBytes = maxPDFSizeMB * (1024 * 1024);

    var form = $('#my-awesome-dropzone');

    /**
    * @function addFormValidations
    * @description Asign all the form validations for this page.
    */

    var addFormValidations = function() {

        form.validate({

            rules: {

                name: {required: true},

                description: {required: true},

                email: {email:true},

                'facebook_group': {url:true},

                'course-program': { accept: "pdf", filesize: maxPDFSizeBytes }

            },
            messages: {

                name: helper.createErrorLabel('nombre', 'REQUIRED'),

                description: helper.createErrorLabel('descripción', 'REQUIRED'),

                email: helper.createErrorLabel('email', 'EMAIL'),

                'facebook-group' : helper.createErrorLabel('grupo de Facebook', 'INVALID_URL'),

                'course-program' : "El tamaño máximo debe ser de " + maxPDFSizeMB + " MB"

            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();

                window.scroll({top: 0, left: 0, behavior: 'smooth'});
            },

        });

    }

    /**
    * @function addListeners
    * @description Asign all the listeners.
    */

    var addListeners = function(){

        $('#name').on('blur', function() {

            if($(this).attr('id') == 'name') validateTitle($('#name').val(), $('#url').val());

        });

        $('#validate-title').click(function(event){

            validateTitle($('#name').val(), $('#url').val())

        })

        $('.delete-pdf-file').click(function(){

            document.getElementById("course-program").value = "";

            $('.delete-pdf-file').addClass('d-none');

        })

        /*$("input[type=file]").change(function () {

            var fieldVal = $(this).val();

            if (fieldVal != undefined || fieldVal != "") {
                $(this).next(".custom-file-control").attr('data-content', 'archivo.pdf');
            }

        });*/

        $.validator.addMethod('filesize', function(value, element, param) {
            // param = size (en bytes) 
            // element = element to validate (<input>)
            // value = value of the element (file name)
            return this.optional(element) || (element.files[0].size <= param) 
        });

        $('#course-program').bind('change', function() {

            $('.delete-pdf-file').removeClass('d-none');

            var sizeInMB = (this.files[0].size / (1024*1024)).toFixed(2);

            var fileName = this.files[0].name;

            if(sizeInMB > maxPDFSizeMB){

                Swal({

                    title: 'Opss...el PDF es demasiado grande!',

                    html: 'El tamaño máximo permitido es de <b>' +  maxPDFSizeMB + ' MB</b> y el archivo <b>' + fileName + '</b> pesa <b>' + sizeInMB + 'MB</b>. No olvides que los usuarios deben descargar este material a sus equipos!',

                    type: 'error',

                    confirmButtonText: 'Entendido!'

                });


                //alert("ufa...maximo es " + maxPDFSizeMB + " y tenes " + sizeInMB);
            }

//            alert(sizeInMB + 'MB');


});

        $('[data-toggle="tooltip"]').tooltip();

        $('.btn-add-in-edition').click(function(e){

            var getName = $(this).parent().parent().parent().find(".new_tutor_name").val();

            var getSurname = $(this).parent().parent().parent().find(".new_tutor_surname").val();

            if (getName !== "" || getSurname !== ""){

                addNewTutor(getName, getSurname);

                /*var data = {

                    id: 1,
                    
                    text: (getName + ' ' + getSurname)
                };

                var newOption = new Option(data.text, data.id, true, true);

                $('.tutor-selector').append(newOption).trigger('change');

                $(this).parent().parent().parent().find(".new_tutor_name").val('');

                $(this).parent().parent().parent().find(".new_tutor_surname").val('');*/


            }

        });

        $("#m_repeater_1").repeater({

            initEmpty: !1,

            defaultValues: {

                "text-input": "foo"

            },

            show: function() {

                $(this).slideDown()

            },

            hide: function(e) {

                $(this).slideUp(e)

            }

        })

        $('.show_price').change(function() {

            var checked = ($(this).prop('checked'));

            console.log("checked: " + checked);

            $('#price').prop("disabled", !checked);

        });


        $('.in_location').change(function() {

            var checked = ($(this).prop('checked'));

            console.log("checked: " + checked);

            $('#address').prop("disabled", !checked);

        })

        $('.show_duration').change(function() {

            console.log("...");

            var checked = ($(this).prop('checked'));

            console.log("checked: " + checked);

            $('#duration').prop("disabled", !checked);

        });

        $('#duration').timepicker({

            minuteStep: 10,

            template: 'modal',

            appendWidgetTo: 'body',

            showSeconds: true,

            showMeridian: false,

            defaultTime: false

        });

        /*autosize for the textareas */

        autosize(document.querySelector('textarea'));

        /*permalink approach...*/

        $('#name').on('input', function() {

            if(editMode) return;

            var permalink;

            permalink = $.trim($(this).val());

            permalink = permalink.replace(/\s+/g,' ');

            $('#url').val(permalink.toLowerCase());

            $('#url').val($('#url').val().replace(/\W/g, ' '));

            $('#url').val($.trim($('#url').val()));

            $('#url').val($('#url').val().replace(/\s+/g, '-'));

        });

        /*tutor selector with select2*/

        $('.tutor-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá a los tutores"

        });


        $('.tutor-selector').on("select2:select", function (e) { 

            if(editMode) addNewTutorToCourse(e.params.data.id);

        });

        $('.tutor-selector').on("select2:unselect", function (e) { 

            if(editMode){

                var id = $(e.params.data.element).attr('data-relation');

                updateValue(id, 'deleted', 1, 'museum_courses_tutors_relations');

            }

        });

        $('#course-datepicker').datepicker({

            format: 'yyyy/mm/dd',

            todayBtn: true,

            todayHighlight: true

        });

        $('#course-datepicker').on('changeDate', function(event) {

            if(editMode){

                var actual = $(event.target).attr('name');

                if(actual == 'start_date'){

                    console.log("start");

                    updateValue(courseId, $(event.target).attr('name'), event.format(), 'museum_courses');

                }else{

                    console.log("end");

                    updateValue(courseId, $(event.target).attr('name'), event.format(), 'museum_courses');

                }

            }else{

                $('#my_hidden_input').val($('#course-datepicker').datepicker('getFormattedDate'));

            }

        });


        //window.onbeforeunload = function () { window.scrollTo(0, 0); }

        //$(document).on("keypress", "form", function(event){ return event.keyCode != 13; });

        $('#exit_from_form').click(function(e) {  window.location.replace("index.php"); });
        
        $('#back_to_list').click(function(e) { window.location.replace("museum_courses_list.php?tabId=" + redirectId); });

        $(".update-btn").click(function(){

            if(!editMode) return;

            var dbName = $(this).attr('data-db-value');

            var id = $(this).attr('data-id');

            var newValue = $(this).parent().parent().find(':input').val();

            var fieldName = $(this).parent().parent().find(':input').attr('name');

            var valid = $('#' + fieldName).valid();

            console.log(valid);

            if(dbName != newValue && valid){

                $(this).attr('data-db-value', newValue);

                //updateValue(id, fieldName, newValue, 'string', 0, 'update');

                updateValue(id, fieldName, newValue, 'museum_courses');


            }

        });

        $('.toggler-info:checkbox').change(function() {

            if(!editMode) return;

            var fieldName = $(this).attr('name');

            var newValue = ($(this).is(":checked")) ? 1 : 0;

            console.log(courseId, fieldName, newValue);

            updateValue(courseId, fieldName, newValue, 'museum_courses')

        }); 

    }

    /**
    * @function validateTitle
    * @description Send the URL and the permalink to validate.
    *
    * @param {String} name                 - The name of the new item.
    * @param {String} url                  - The url of the new item.
    */

    var validateTitle = function(name, url){

        helper.blockStage("validando nombre...");

        var request = $.ajax({

            url: "private/users/museum/courses/museum_add_tutor_to_system.php",

            type: "POST", 

            data: {tutor_name:name, tutor_surname:surname, course_id:courseId}, 

            dataType: "json" });

        request.done(function(result) {

            var data = {

                id: result.newTutorId,

                text: (result.newTutorName + ' ' + result.newTutorSurname)

            };

            var newOption = new Option(data.text, data.id, true, true);

            $(newOption).attr('data-relation', result.newRelationId);

            $('.tutor-selector').append(newOption).trigger('change');

            helper.showToastr("Se agregó un nuevo tutor al curso", 'se agrego a ' +  data.text + ' como tutor.');

            helper.unblockStage();

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

            helper.unblockStage();


        });

        //data: {name:name, url:url, table:'museum_courses'}

    }

    /**
    * @function addNewTutor
    * @description add new tutor...
    *
    * @param {int} id                 - the id of the turor.
    */

    var addNewTutor = function(name, surname){

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/courses/museum_add_tutor_to_system.php",

            type: "POST", 

            data: {tutor_name:name, tutor_surname:surname, course_id:courseId}, 

            dataType: "json" });

        request.done(function(result) {

            var data = {

                id: result.newTutorId,

                text: (result.newTutorName + ' ' + result.newTutorSurname)

            };

            var newOption = new Option(data.text, data.id, true, true);

            $(newOption).attr('data-relation', result.newRelationId);

            $('.tutor-selector').append(newOption).trigger('change');

            helper.showToastr("Se agregó un nuevo tutor al curso", 'se agrego a ' +  data.text + ' como tutor.');

            helper.unblockStage();

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

            helper.unblockStage();


        });
    }

    /**
    * @function addNewTutorToCourse
    * @description add new tutor...
    *
    * @param {int} id                 - the id of the turor.
    */

    var addNewTutorToCourse = function(id){

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/courses/museum_course_add_tutor.php",

            type: "POST", 

            data: {id_tutor:id, id_course:courseId, courseName:courseName}, 

            dataType: "json" });

        request.done(function(result) {

            helper.showToastr(result.title, result.msg);

            helper.unblockStage();

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

            helper.unblockStage();


        });
    }

    /**
    * @function updateValue
    * @description update individual value in DB.
    *
    * @param {int} id                 - object id.
    * @param {string} fieldName       - the field to modify
    * @param {string} newValue        - object value.
    * @param {int} filter             - filter type.
    */

    var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined){

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/update_value.php",

            type: "POST",

            data: {id:id, fieldName:fieldName, newValue:newValue, filter:filter},

            dataType: "json"

        });

        request.done(function(result) {

            console.log(result);

            helper.showToastr(result.title, result.msg);

            if(callback != undefined) callback(callbackParams);

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");


        });

        if(callback == undefined) helper.unblockStage();

        localStorage.setItem('refresh', 'true');

    }

    var deleteImage = function(id, file){

        helper.blockStage("Eliminando imagen...");

        var request = $.ajax({

            url: "private/users/museum/images_general/delete_image.php",

            type: "POST",

            data: {id:id, file:file, source:'courses', courseName:courseName},

            dataType: "json"

        });

        request.done(function(result) {

            console.log(result);

            helper.unblockStage();

            helper.showToastr(result.title, result.msg);

            myDropzone.removeAllFiles();

            myDropzone.options.maxFiles = 1;

            myDropzone.setupEventListeners();

            //myDropzone.files = [];


        });

        request.fail(function(jqXHR, textStatus) {

            console.log("error");
            
            console.log(textStatus);

            console.log(jqXHR);

        });

        helper.unblockStage();


    }

    /**
    * @function handleDropZone
    * @description manage all in relation woth DropZoneJS for images.
    */

    var handleDropZone = function(){

        var urlPath = (editMode) ? 'private/users/museum/images_general/add_image.php' : 'private/users/museum/courses/course_add.php';

        var autoProcessQueue = (editMode) ? true : false;

        myDropzone = new Dropzone("#my-awesome-dropzone", {

            paramName: 'file',

            url: urlPath,

            clickable: true,

            acceptedFiles: 'image/*',

            autoProcessQueue: autoProcessQueue,

            maxFilesize: 2000,

            uploadMultiple: false,

            addRemoveLinks: true,

            dictRemoveFile: 'eliminar archivo',

            maxFiles: 1,

            thumbnailWidth: 350,

            thumbnailHeight:350,

            thumbnailMethod: 'contain',

            previewsContainer: '.dropzone-previews',

            dictRemoveFileConfirmation: '¿Eliminar imagen de perfil?',

            init: function() {

                var _this = this;

                var myDropzone = this;

                this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {

                    e.preventDefault();

                    e.stopPropagation();

                    if (!form.valid()) { return; }

                    if (form.valid() == true) { 

                        if (myDropzone.getQueuedFiles().length > 0) {  

                            myDropzone.processQueue();  

                            $('#add_new_course').addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                            helper.blockStage("actualizando datos...");

                        } else { 

                            var blob = new Blob();

                            blob.upload = { 'chunked': myDropzone.defaultOptions.chunking };

                            myDropzone.uploadFile(blob);

                        }                                    
                    }            

                });

                this.on("maxfilesexceeded", function(file){

                });

                this.on("complete", function(file){

                    this.removeAllFiles(true); 

                });

                this.on("thumbnail", function(file) {

                    if (!editMode && (file.width != defaultImageWidth || file.height != defaultImageHeight)) {

                        this.removeAllFiles(true); 

                        Swal({

                            title: 'Opss...tamaño incorrecto :(',

                            html: 'Puede no haber imagen, pero de haberla, tiene que ser una imagen de <b>2000px</b> por <b>1000px</b> en formato wide. Necesitamos que ese sea el tamaño exacto para luego poder resizearla y adaptarla a todas las partes del sitio.',

                            type: 'error',

                            confirmButtonText: 'Entendido!'

                        });

                        return;

                    } 

                });


                this.on("removedfile", function(file) {  

                    if(editMode) deleteImage(courseId, file.name); });

                this.on("error", function(file) { 

                    console.log("error");

                    console.log(file);
                });

                this.on("sending", function(file, xhr, formData) {

                    if(editMode){

                        formData.append("id", courseId);

                        formData.append("courseName", courseName);

                        formData.append("source", "courses");

                    }

                    formData.append('course-program', $('#course-program')[0].files[0]); 

                    var objectsVector = new Array();

                    $(".my-repeater-item").each(function() {

                        var getName = $(this).find(".new_tutor_name").val();

                        var getSurname = $(this).find(".new_tutor_surname").val();

                        if (getName !== "" || getSurname !== ""){

                            var object = {};

                            object.tutorName = $(this).find(".new_tutor_name").val();

                            object.tutorSurname = $(this).find(".new_tutor_surname").val(); 

                            objectsVector.push(object);

                        } 

                    });

                    formData.append("addedTutors", JSON.stringify(objectsVector));

                    formData.append('selectedTutors', JSON.stringify($('.tutor-selector').select2('data')));



                });

                this.on("success", function(file, data) {

                    console.log("ok antes");

                    console.log(file, data);

                    var response = JSON.parse(file.xhr.responseText);

                    console.log("responde");

                    console.log(response);

                    myDropzone.removeEventListeners();

                    helper.unblockStage();

                    $('#add_new_course').removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch(Number(response.status)){

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {



                            switch(Number(response.status)){

                             case 1:

                             window.location.replace("museum_courses_list.php?tabId=" + addRedirectId + "&highlight=true");

                             break;

                         }

                     }

                 });

                });
            }

        });

if(editMode && imagePath != ""){

    var mockFile = { name: imagePath, size: 5668, width:200, height:200 };

    myDropzone.emit("addedfile", mockFile);

    myDropzone.emit("thumbnail", mockFile, imagePath);

    myDropzone.emit("complete", mockFile);

    var existingFileCount = 1;

    myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;

    myDropzone.removeEventListeners();

    console.log(myDropzone.options.maxFiles);
}

Dropzone.confirm = function(question, accepted, rejected) {

    Swal({

        title: question,

        html: 'Si eliminas la imagen, mostraremos un avatar a forma de reemplazo.',

        type: 'warning',

        showCancelButton: true,

        confirmButtonText: 'Entendido! Eliminar',

        cancelButtonText: 'salir'

    }).then((result) => {

                /*var checkedValue = $('#not-show-again-checkbox:checked').val();

                if(checkedValue == "on"){

                    myDropzone.options.dictRemoveFileConfirmation = null;

                    localStorage.setItem('dictRemoveFileConfirmation', true);

                }*/

                if (result.value) accepted();

            })
};


        /*$('#process-queue').click(function(e) {

            var files = myDropzone.getQueuedFiles();

            files.sort(function(a, b){

                return ($(a.previewElement).index() > $(b.previewElement).index()) ? 1 : -1;

            })

            myDropzone.removeAllFiles();
          
            myDropzone.handleFiles(files);
          
            myDropzone.processQueue();

        });*/
    }

    return {

        init: function() {

            editMode = ($('#edit-mode').val() == 1) ? true : false;

            if(editMode){

                var temp;

                if(document.referrer != ""){

                    temp = helper.getGETVariables(document.referrer, 'tabId');

                }else{

                    temp = null;
                }

                redirectId = (temp == null) ? 1 : temp; 

                imagePath = $('#image-path').val();

                courseId = $('#course-id').val();

                courseName = $('#course-name').val();

                console.log(imagePath);
            }

            addListeners();

            addFormValidations();

            handleDropZone();

        }

    };

}();

jQuery(document).ready(function() { MuseumCourseAdd.init(); });

