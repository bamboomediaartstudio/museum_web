<?php

require_once '../core/init.php';

$status['init'] = true;

$db = DB::getInstance();

if(Input::exists()){

	$validate = new Validate();

	$validation = $validate->check($_POST, array(

		'name' => array('display'=>'name', 'required' => true),

		'url' => array('display'=>'url', 'required' => true),

		'table' => array('display'=>'table', 'required' => true)

	));

	if($validation->passed()){

		//$myQuery = 'SELECT * FROM ' . Input::get('table') . ' WHERE name = ? AND  url = ?';

		$myQuery = 'SELECT * FROM ' . Input::get('table') . ' WHERE url = ?';

		$query = $db->query($myQuery, 

			[Input::get('url')]);

		$count = $query->count();

		if($count>0){

			$status['valid'] = false;

		}else{

			$status['valid'] = true;

		}
		
	}

	echo json_encode($status);

}

?>