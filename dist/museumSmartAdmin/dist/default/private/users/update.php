<?php

require_once 'core/init.php';

$user = new User();

if(!$user->isLoggedIn()){

	Redirect::to('index.php');
}

if(Input::exists()){

	if(Token::check(Input::get('token'))){

		$validate = new Validate();

		$validation = $validate->check($_POST, array(

			'name' => array(

				'required' => true,

				'min' => 2,

				'max' =>20

			),

			'email'=>array(

				'required'=>true,

				'email'=>true

			)
		));

		if($validation->passed()){
			
			try{

				$user->update(array(

					'name'=> Input::get('name'),

					'nickname'=> Input::get('nickname'),

					'email'=> Input::get('email'),

				));

			}catch(Exception $e){

				die($e->getMessage());

			}
		
		}else{
			
			foreach($validation->errors() as $error){
				
				echo $error, '<br>';

			}
		}
	}
}

?>

<form action="" method="post">

	<div class="field">

		<label for="name"></label>

		<input type="text" name="name" value="<?php echo mysql_real_escape_string($user->data()->name);?>">

	</div>

	<div class="field">

		<label for="nickname"></label>

		<input type="text" name="nickname" value="<?php echo mysql_real_escape_string($user->data()->nickname);?>">

	</div>

	<div class="field">

		<label for="email"></label>

		<input type="text" name="email" value="<?php echo mysql_real_escape_string($user->data()->email);?>">

	</div>

		<input type="submit" value="update!">

		<input type="hidden" name="token" value="<?php echo Token::generate();?>"> 

</form>