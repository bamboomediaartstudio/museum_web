<?php

require_once '../core/init.php';

$db = DB::getInstance();

$status['init'] = true;

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'sponsor-modal-name' => array('display'=>'sponsor-modal-name', 'required' => true),

	'sponsor-modal-surname' => array('display'=>'sponsor-modal-name', 'required' => true),

	'sponsor-modal-email' => array('display'=>'sponsor-modal-email', 'required' => true),

	'sponsor-modal-phone' => array('display'=>'sponsor-modal-phone', 'required' => true),

	'sponsor-type-option' => array('display'=>'sponsor-type-option', 'required' => true),

	'sponsor-modal-city' => array('display'=>'sponsor-modal-city', 'required' => true),

	'sponsor-modal-address' => array('display'=>'sponsor-modal-address', 'required' => true)
));

if($validation->passed()){

	$status['validation'] = 'ok';

	$test = ip2long(Helpers::getIP());

	$resolve =  long2ip($test);

	DB::getInstance()->insert('museum_applicants',[

			'name'=> Input::get('sponsor-modal-name'),

			'surname'=> Input::get('sponsor-modal-surname'),

			'email'=> Input::get('sponsor-modal-email'),

			'phone' => Input::get('sponsor-modal-phone'),

			'city' => Input::get('sponsor-modal-city'),

			'address' => Input::get('sponsor-modal-address'),

			'type' => Input::get('sponsor-type-option'),

			'ip' => ip2long(Helpers::getIP()),

			'os' => Helpers::getOS(),

			'browser' => Helpers::getBrowser(),

			'added' => date("Y-m-d H:i:s")]);

	//general settings...

	$settingsQ = $db->query("SELECT * FROM settings");

	$settings = $GLOBALS['settings'] = $settingsQ->first();

	$supportSettingsQ = $db->query("SELECT * FROM support_settings");

	$GLOBALS['supportSettings'] = $supportSettingsQ->first();

	$museumSettingsQ = $db->query("SELECT * FROM museum_data");

	$museumSettings = $museumSettingsQ->first();

	$museumSocialMediaQ = $db->query("SELECT * FROM museum_social_media");

	$museumSocialMediaSettings = $museumSocialMediaQ->first();

	$address = $museumSettings->address . ' ' . $museumSettings->address_number . ', ' . $museumSettings->zip_code . ' ' . $museumSettings->city;

	$message = file_get_contents('../../email_templates/applicant_subscriber.html');

	$message = str_replace('%projectTitle%', $museumSettings->title, $message);

	$message = str_replace('%name%', Input::get('sponsor-modal-name'), $message);
	
	$message = str_replace('%ip%', Helpers::getIP(), $message);

	$message = str_replace('%os%', Helpers::getOS(), $message);

	$message = str_replace('%browser%', Helpers::getBrowser(), $message);

	$message = str_replace('%email%', $GLOBALS['supportSettings']->support_email, $message);

	$message = str_replace('%address%', $address, $message);

	$message = str_replace('%te%', $museumSettings->te, $message);

	$message = str_replace('%museumEmail%', $museumSettings->email, $message);

	$message = str_replace('%lat%', $museumSettings->lat, $message);

	$message = str_replace('%lon%', $museumSettings->lon, $message);

	$message = str_replace('%facebook%', $museumSocialMediaSettings->facebook, $message);

	$message = str_replace('%instagram%', $museumSocialMediaSettings->instagram, $message);

	$message = str_replace('%twitter%', $museumSocialMediaSettings->twitter, $message);

	$sent = Helpers::email(Input::get('sponsor-modal-email'), 'Recibimos tu mensaje :)', $message);

	//for sender...

	$msg2 = file_get_contents('../../email_templates/applicant_subscription.html');

	$msg2 = str_replace('%projectTitle%', $museumSettings->title, $msg2);

	$msg2 = str_replace('%type%', Input::get('sponsor-type-option'), $msg2);

	$msg2 = str_replace('%name%', Input::get('sponsor-modal-name'), $msg2);

	$msg2 = str_replace('%surname%', Input::get('sponsor-modal-surname'), $msg2);

	$msg2 = str_replace('%contactEmail%', Input::get('sponsor-modal-email'), $msg2);

	$msg2 = str_replace('%contactPhone%', Input::get('sponsor-modal-phone'), $msg2);

	$msg2 = str_replace('%city%', Input::get('sponsor-modal-city'), $msg2);
	
	$msg2 = str_replace('%address%', Input::get('sponsor-modal-address'), $msg2);
	
	$msg2 = str_replace('%ip%', Helpers::getIP(), $msg2);

	$msg2 = str_replace('%os%', Helpers::getOS(), $msg2);

	$msg2 = str_replace('%browser%', Helpers::getBrowser(), $msg2);

	$msg2 = str_replace('%email%', $GLOBALS['supportSettings']->support_email, $msg2);

	$msg2 = str_replace('%address%', $address, $msg2);

	$msg2 = str_replace('%te%', $museumSettings->te, $msg2);

	$msg2 = str_replace('%museumEmail%', $museumSettings->email, $msg2);

	$msg2 = str_replace('%lat%', $museumSettings->lat, $msg2);

	$msg2 = str_replace('%lon%', $museumSettings->lon, $msg2);

	$msg2 = str_replace('%facebook%', $museumSocialMediaSettings->facebook, $msg2);

	$msg2 = str_replace('%instagram%', $museumSocialMediaSettings->instagram, $msg2);

	$msg2 = str_replace('%twitter%', $museumSocialMediaSettings->twitter, $msg2);

	$sent2 = Helpers::email('museodelholocausto.recovery@gmail.com', 'Buenas noticias :)', $msg2);

	if($sent && $sent2){

		$status['envio'] = 'exitoso';
		
		$status['resoslve'] = $resolve;

	}else{

		$status['envio'] = 'no exitoso';

	}

	echo json_encode($status);

	return;

}


?>