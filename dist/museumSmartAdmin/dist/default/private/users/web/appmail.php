<?php

require_once '../core/init.php';

$db = DB::getInstance();

$status['init'] = true;

$status['name'] = Input::get('name');
	
$status['email'] = Input::get('email');

$status['message'] = Input::get('message');

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'name' => array('display'=>'name', 'required' => true),

	'email' => array('display'=>'email', 'email' => true),

	'message' => array('display'=>'message', 'required' => true)

));

if($validation->passed()){

	$status['validation'] = 'ok';

	$test = ip2long(Helpers::getIP());

	$resolve =  long2ip($test);

	/*DB::getInstance()->insert('webmail_messages',[

		'subject'=> Input::get('form-subject'),

		'name'=> Input::get('name'),

		'surname'=> Input::get('surname'),

		'email' => Input::get('email'),

		'ip' => ip2long(Helpers::getIP()),

		'os' => Helpers::getOS(),

		'browser' => Helpers::getBrowser(),

		'added' => date("Y-m-d H:i:s"),

		'message' => Input::get('form-text-area')]);*/

	//general settings...

	$settingsQ = $db->query("SELECT * FROM settings");

	$settings = $GLOBALS['settings'] = $settingsQ->first();

	$supportSettingsQ = $db->query("SELECT * FROM support_settings");

	$GLOBALS['supportSettings'] = $supportSettingsQ->first();

	$museumSettingsQ = $db->query("SELECT * FROM museum_data");

	$museumSettings = $museumSettingsQ->first();

	//social media...

	$museumSocialMediaQ = $db->query("SELECT * FROM museum_sm WHERE active = ? AND deleted = ? ORDER BY internal_order", [1, 0]);

	$smArray = array();

	foreach($museumSocialMediaQ->results() as $result){

		$smArray[$result->name] = $result->url;

	}

	//end of social media...

	$year = date("Y");

	$address = $museumSettings->address . ' ' . $museumSettings->address_number . ', ' . $museumSettings->zip_code . ' ' . $museumSettings->city;

	$message = file_get_contents('../../email_templates/appmail.html');

	$message = str_replace('%projectTitle%', $museumSettings->title, $message);

	$message = str_replace('%name%', Input::get('name'), $message);

	$message = str_replace('%contactEmail%', Input::get('email'), $message);
	
	$message = str_replace('%bodyString%', Input::get('message'), $message);

	$message = str_replace('%ip%', Helpers::getIP(), $message);

	$message = str_replace('%os%', Helpers::getOS(), $message);

	$message = str_replace('%browser%', Helpers::getBrowser(), $message);

	$message = str_replace('%email%', $GLOBALS['supportSettings']->support_email, $message);

	$message = str_replace('%address%', $address, $message);

	$message = str_replace('%te%', $museumSettings->te, $message);

	$message = str_replace('%museumEmail%', $museumSettings->email, $message);

	$message = str_replace('%lat%', $museumSettings->lat, $message);

	$message = str_replace('%lon%', $museumSettings->lon, $message);

	$message = str_replace('%facebook%', $smArray['Facebook'], $message);

	$message = str_replace('%instagram%', $smArray['Instagram'], $message);

	$message = str_replace('%twitter%', $smAsrray['Twitter'], $message);
	
	$message = str_replace('%youtube%', $smArray['Youtube'], $message);

	$message = str_replace('%year%', $year, $message);

	$sent = Helpers::email('aditivoestudio@gmail.com', utf8_decode(Input::get('form-subject')), $message);
		
	//for sender...

	$msg2 = file_get_contents('../../email_templates/sender_appmail.html');

	$msg2 = str_replace('%projectTitle%', $museumSettings->title, $msg2);

	$msg2 = str_replace('%name%', Input::get('name'), $msg2);

	$msg2 = str_replace('%contactEmail%', Input::get('email'), $msg2);
	
	$msg2 = str_replace('%ip%', Helpers::getIP(), $msg2);

	$msg2 = str_replace('%os%', Helpers::getOS(), $msg2);

	$msg2 = str_replace('%browser%', Helpers::getBrowser(), $msg2);

	$msg2 = str_replace('%email%', $GLOBALS['supportSettings']->support_email, $msg2);

	$msg2 = str_replace('%address%', $address, $msg2);

	$msg2 = str_replace('%te%', $museumSettings->te, $msg2);

	$msg2 = str_replace('%museumEmail%', $museumSettings->email, $msg2);

	$msg2 = str_replace('%lat%', $museumSettings->lat, $msg2);

	$msg2 = str_replace('%lon%', $museumSettings->lon, $msg2);

	$msg2 = str_replace('%facebook%', $smArray['Facebook'], $msg2);

	$msg2 = str_replace('%instagram%', $smArray['Instagram'], $msg2);

	$msg2 = str_replace('%twitter%', $smArray['Twitter'], $msg2);

	$msg2 = str_replace('%youtube%', $smArray['Youtube'], $msg2);

	$msg2 = str_replace('%year%', $year, $msg2);

	$sent2 = Helpers::email(Input::get('email'), 'Recibimos tu mensaje', $msg2);

	if($sent && $sent2){

		$server='1';
		
	}else{

		$server="0";

	}

}

echo "Status=".$server;

//echo json_encode($server);

?>