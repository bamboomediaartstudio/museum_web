<?php

require_once '../core/init.php';

$status['init'] = true;

$status['email'] = Input::get('email');

$validate = new Validate();

$validation = $validate->check($_POST, array(

	'email' => array('display'=>'email', 'email' => true)

	
));

if($validation->passed()){

	$status['validation'] = 'ok';

	$apiKey = '42b9dec340616b28edca278636099076-us19';

	$listID = '2bc5f8e1d3';

	$memberID = md5(strtolower(Input::get('email')));
	
	$dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
	
	$url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;

	$json = json_encode([
		
		'email_address' => Input::get('email'),

		'status'        => 'subscribed',

		'merge_fields'  => [ 'FNAME'     => Input::get('name'), ]
	]);

	// send a HTTP POST request with curl
	
	$ch = curl_init($url);
	
	curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
	
	curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
	curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
	
	$result = curl_exec($ch);
	
	$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	
	curl_close($ch);

    // store the status message based on response code
	if ($httpCode == 200) {
		
		$status['msg'] = '<p style="color: #34A853">Suscripción exitosa!.</p>';

		DB::getInstance()->insert('museum_newsletter_backup',[

			'email'=> Input::get('email'),

			'name'=> Input::get('name'),

			'email' => Input::get('email'),

			'ip' => ip2long(Helpers::getIP()),

			'os' => Helpers::getOS(),

			'browser' => Helpers::getBrowser(),

			'added' => date("Y-m-d H:i:s")]);
	
	} else {

		switch ($httpCode) {
			
			case 214:
			
			$status['msg'] = 'You are already subscribed.';
			
			break;
			
			default:
			
			$status['msg'] = 'Some problem occurred, please try again.';
			
			break;
		}

	}


	//make template replacements...

	/*$message = file_get_contents('../../email_templates/webmail.html');

	$message = str_replace('%name%', Input::get('name'), $message);

	$message = str_replace('%surname%', Input::get('surname'), $message);

	$message = str_replace('%email%', Input::get('email'), $message);
	
	$message = str_replace('%msg%', Input::get('form-text-area'), $message);*/

	/*$message = str_replace('%os%', Helpers::getOS(), $message);

	$message = str_replace('%browser%', Helpers::getBrowser(), $message);

	$message = str_replace('%email%', $GLOBALS['settings']->support_email, $message);*/

	//$sent = Helpers::email('mariano.makedonsky@gmail.com', Input::get('form-subject'), $message);

	//if($sent){

		//printData(4);

		//$status['envio'] = 'exitoso';

	//}else{

		//printData(5);

		//$status['envio'] = 'no exitoso';

	//}

}

echo json_encode($status);


?>