<?php

require_once 'private/users/core/init.php';

$uniqueToken = Token::generate();

require_once 'private/users/core/checker.php';

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Perfil de usuario</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});
		
	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
	
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<input type="hidden" name="user-id" id="user-id" value="<?php echo $user->data()->id;?>">

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title ">

								Mi Perfil

							</h3>

						</div>

					</div>

				</div>
				
				<div class="m-content">

					<div class="row">

						<div class="col-xl-3 col-lg-4">

							<div class="m-portlet m-portlet--full-height  ">

								<div class="m-portlet__body">

									<div class="m-card-profile">

										<div class="m-card-profile__title m--hide">

											Mi Perfil

										</div>

										<div class="m-card-profile__pic">

											<div class="m-card-profile__pic-wrapper">

												<img src="<?php echo $profileImage;?>" alt="<?php echo $user->data()->name . ' ' . $user->data()->surname;?>"/>

											</div>

										</div>

										<div class="m-card-profile__details">

											<span class="m-card-profile__name">

												<?php echo $user->data()->name . ' ' . $user->data()->surname;?>

											</span>

											<a href="" class="m-card-profile__email m-link">

												<?php echo $user->data()->email;?>

											</a>

										</div>

									</div>


									<div class="m-portlet__body-separator"></div>

									<p class="text-center">miembro desde: <?php echo $user->data()->joined;?></p>

									<p class="text-center">última sesión: <?php echo $user->data()->last_login;?></p>

									<p class="text-center">logins desde el inicio: <?php echo $user->data()->logins;?></p><br>

								</div>
							</div>
						</div>

						<div class="col-xl-9 col-lg-8">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" 
										role="tablist">

										<li class="nav-item m-tabs__item">
											
											<a class="nav-link m-tabs__link active" data-toggle="tab" href="
											#m_user_profile_tab_1" role="tab">

											<i class="flaticon-share m--hide"></i>

											Actualizar perfil

										</a>
									</li>

									<li class="nav-item m-tabs__item">

										<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">

											Actualizar contraseña

										</a>
									</li>

									<li class="nav-item m-tabs__item">

										<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_3"
										role="tab">

										Cambiar imagen

									</a>

								</li>

							</ul>

						</div>
						
					</div>

					<div class="tab-content">

						<div class="tab-pane active" id="m_user_profile_tab_1">

							<form class="m-form m-form--fit m-form--label-align-right">

								<div class="m-portlet__body">

									<div class="form-group m-form__group m--margin-top-10 m--hide">

										<div class="alert m-alert m-alert--default" role="alert">
											The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
										</div>

									</div>

									<div class="form-group m-form__group row">

										<div class="col-10 ml-auto">
											
											<h3 class="m-form__section">1. Detalles Personales</h3>

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="name" class="col-2 col-form-label">

											Nombre

										</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" value="<?php echo $user->data()->name;?>" name="name" id="name">

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="surname" class="col-2 col-form-label">

											Apellido

										</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" value="<?php echo $user->data()->surname;?>" name="surname" id="surname">

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="nickname" class="col-2 col-form-label">

											Nickname

										</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" value="<?php echo $user->data()->nickname;?>" name="nickname" id="nickname">

										</div>

									</div>

									<div class="form-group m-form__group row">
										<label for="birthday" class="col-2 col-form-label">

											tu cumple

										</label>

										<div class="col-7">

											<div class="input-group date" id="birthday-picker">

												<input type="text" class="form-control m-input" readonly="" 
												value="<?php echo $user->data()->birthday;?>" id="birthday" name="birthday">

												<span class="input-group-addon"><i class="la la-calendar"></i></span>

											</div>

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="email" class="col-2 col-form-label">

											Email:

										</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" value="<?php echo $user->data()->email;?>" name="email" id="email">

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="phone" class="col-2 col-form-label">

											Teléfono:

										</label>

										<div class="col-7">

											<input class="form-control m-input" type="tel" value="<?php echo $user->data()->phone;?>" name="phone" id="phone">

										</div>

									</div>


									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>

									<div class="form-group m-form__group row">

										<div class="col-10 ml-auto">

											<h3 class="m-form__section">

												2. Social Links

											</h3>

										</div>

									</div>
									
									<div class="form-group m-form__group row">

										<label for="linkedin" class="col-2 col-form-label">

											Linkedin

										</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" name="linkedin" id="linkedin" value="<?php echo $user->socialMedia()->linkedin;?>">

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="facebook" class="col-2 col-form-label">

											Facebook

										</label>

										<div class="col-7">

											<input class="form-control m-input" type="text" name="facebook" id="facebook" value="<?php echo $user->socialMedia()->facebook;?>">

										</div>
										
									</div>

									<div class="form-group m-form__group row">
										
										<label for="twitter" class="col-2 col-form-label">

											Twitter

										</label>
										
										<div class="col-7">

											<input class="form-control m-input" type="text" name="twitter" 
											id="twitter" value="<?php echo $user->socialMedia()->twitter;?>">

										</div>

									</div>

									<div class="form-group m-form__group row">
										
										<label for="instagram" class="col-2 col-form-label">

											instagram

										</label>
										
										<div class="col-7">

											<input class="form-control m-input" type="text" name="instagram" id="instagram" value="<?php echo $user->socialMedia()->instagram;?>">

										</div>

									</div>

								</div>

								<div class="m-portlet__foot m-portlet__foot--fit">

									<div class="m-form__actions">
										
										<div class="row">

											<div class="col-2"></div>

											<div class="col-7">

												<input type="hidden" name="token" value="<?php echo $uniqueToken;?>">

												<button id="update_profile" type="reset" class="btn btn-accent m-btn m-btn--air m-btn--custom">
													Guardar Cambios
												</button>
												&nbsp;&nbsp;
												<button id="exit_from_profile" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
													Cancelar

												</button>

											</div>

										</div>

									</div>

								</div>

							</form>

						</div>

						<div class="tab-pane" id="m_user_profile_tab_2">

							<form id="change-password-form" class="m-form m-form--fit m-form--label-align-right">

								<div class="m-portlet__body">

									<div class="form-group m-form__group m--margin-top-10 m--hide">

										<div class="alert m-alert m-alert--default" role="alert">
											The example form below demonstrates common HTML form elements that receive updated styles from Bootstrap with additional classes.
										</div>

									</div>

									<div class="form-group m-form__group row">

										<div class="col-10 ml-auto">
											
											<h3 class="m-form__section">Cambiar contraseña</h3>

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="actualpassword" class="col-2 col-form-label">

											contraseña actual

										</label>

										<div class="col-7">

											<input class="form-control m-input" type="password" value="" name="actualpassword" id="actualpassword">

										</div>

									</div>

									<div class="form-group m-form__group row">

										<label for="newpassword" class="col-2 col-form-label">

											nueva contraseña

										</label>

										<div class="col-7">

											<input class="form-control m-input" type="password" value="" name="newpassword" id="newpassword">

											<div class="progress">

												<div class="progress-bar progress-bar-danger" style="width: 1%;">

												</div>

											</div>

										</div>


									</div>

									<div class="form-group m-form__group row">

										<label for="repeatnewpassword" class="col-2 col-form-label">

											repetir contraseña

										</label>

										<div class="col-7">

											<input class="form-control m-input" type="password" value="" name="repeatnewpassword" id="repeatnewpassword">

										</div>

									</div>

									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>

								</div>

								<div class="m-portlet__foot m-portlet__foot--fit">

									<div class="m-form__actions">
										
										<div class="row">

											<div class="col-2"></div>

											<div class="col-7">

												<input type="hidden" name="token" value="<?php echo $uniqueToken;?>">

												<button id="update_password" type="reset" class="btn btn-accent m-btn m-btn--air m-btn--custom">
													Cambiar contraseña
												</button>
												&nbsp;&nbsp;
												<button id="cancel_update_password" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
													Cancelar

												</button>

											</div>

										</div>

									</div>

								</div>

							</form>

						</div>

						<div class="tab-pane" id="m_user_profile_tab_3">

							<label class="">Imagen de Portada*:
								<br><small>Es la imagen del tutor y tiene que ser de 500 x 500 px o superior.<br>

								</small>

							</label>

							<div class="">

								<input id="main-image" name="main-image" type="file">

							</div>

							<!--<form class="m-form m-form--fit m-form--label-align-right">
								
								<div class="m-portlet__body">

									<div class="form-group m-form__group row">

										<label class="col-form-label col-lg-3 col-sm-12">Subí tu imagen</label>


										<div class="col-lg-12 col-md-12 col-sm-12">
											
											<div class="m-dropzone dropzone" action="" id="m-dropzone-one">

												<div class="m-dropzone__msg dz-message needsclick">

													<h3 class="m-dropzone__msg-title">

														Arrastrá tu imagen aquí

													</h3>

													<span class="m-dropzone__msg-desc">

														O cliqueá para <strong>subirla!</strong>

													</span>

												</div>

											</div>

										</div>

									</div>

								</div>

								<div class="m-portlet__foot m-portlet__foot--fit">

									<div class="m-form__actions m-form__actions">
										
										<div class="row">

											<div class="col-lg-9 ml-lg-auto">

												<input type="hidden" name="add-token" value="<?php echo $uniqueToken;?>">

												<button id="process-queue" type="reset" class="btn btn-brand">Subir archivos</button>


												<button type="reset" class="btn btn-secondary">Cancel</button>

											</div>

										</div>

									</div>

								</div>

							</form>-->

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

</div>

<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
	<i class="la la-arrow-up"></i>
</div>

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="assets/vendors/custom/jquery-ui/jquery-ui.bundle.js" type="text/javascript"></script>

<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/profile.js" type="text/javascript"></script>


</body>

</html>
