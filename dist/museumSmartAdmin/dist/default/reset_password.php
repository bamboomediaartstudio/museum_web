<?php

require_once 'private/users/core/init.php';

$token = Token::generate();

$getVariable = Input::get('vericode');

echo $getVariable;

?>

<!DOCTYPE html>

<html lang="en" >

	<head>

		<meta charset="utf-8" />

		<title>Museo de la Shoá | reset password</title>
		
		<meta name="description" content="Latest updates and statistic charts">
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		
		<script>
        
          WebFont.load({
        
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
        
            active: function() {
        
                sessionStorage.fonts = true;
        
            }
        
          });
		
		</script>
		
		<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		
		<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
		
		<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico?v=2" />

		<script src='https://www.google.com/recaptcha/api.js?theme=dark'></script>
	
	</head>
	
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

		<div class="m-grid m-grid--hor m-grid--root m-page">

			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_reset">

				<div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">

					<div class="m-stack m-stack--hor m-stack--desktop">

						<div class="m-stack__item m-stack__item--fluid">

							<div class="m-login__wrapper">
								
								<div class="m-login__signin">

									<div class="m-login__head">

										<h3 class="m-login__title">

											Reseteá tu contraseña.

										</h3>

										<p align="center">Indicá tu email y tu nueva contraseña.</p>

									</div>

									<form class="m-login__form m-form" action="" autocomplete="off">

										<input style="opacity: 0;position: absolute;">
										<input type="password" style="opacity: 0;position: absolute;">


										<div class="form-group m-form__group">

											<input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off" value="mariano.makedonsky@gmail.com">

										</div>

										<div class="form-group m-form__group">

											<input class="form-control m-input" type="password" placeholder="nueva contraseña" name="newpassword" id="newpassword" value="19Antonietafrancesca85">

											<div class="progress">

												<div class="progress-bar progress-bar-danger" style="width: 1%;">

												</div>

											</div>

										</div>

										<div class="form-group m-form__group">

											<input class="form-control m-input " type="password" placeholder="repetir nueva contraseña" name="repeatnewpassword" id="repeatnewpassword" value="19Antonietafrancesca85">

										</div>

										<div class="m-login__form-action">

											<input type="hidden" name="token" value="<?php echo $token;?>">
										
											<button id="m_reset_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">Validar</button>
										
										</div>
									
									</form>
								
								</div>
								
							</div>
					
						</div>
					
					</div>
				</div>
				
				<div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content" style="background-image: url(assets/app/media/img//bg/bg-1.jpg)">
				
					<div class="m-grid__item m-grid__item--middle">
				
						<h3 class="m-login__welcome">Museo del Holocausto</h3>

						<p class="m-login__msg">Sistema de administración para el Museo del Holocausto.</p>
					
					</div>
				
				</div>
			
			</div>
		
		</div>

		<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		
		<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

		<script src="assets/vendors/custom/jquery-ui/jquery-ui.bundle.js" type="text/javascript"></script>

		<script src="assets/app/js/helper.js" type="text/javascript"></script>
		
		<script src="assets/app/js/reset-password.js" type="text/javascript"></script>

	</body>

</html>
