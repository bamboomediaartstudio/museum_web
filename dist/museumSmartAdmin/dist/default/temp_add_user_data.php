<?php

require_once 'private/users/core/init.php';

$username = 'user for test';

$name = 'test user';

$surname = 'test user';

$email = 'test_admin_user@museodelholocausto.org.ar';

$groupId = 4;

$salt = Hash::salt(32);

$joined = date("Y-m-d H:i:s");

$password = Hash::make('20Muse@DelHolocausto20', $salt);

DB::getInstance()->insert('users', array('username' => $username,'name' => $name, 'surname'=>$surname, 'salt'=>$salt, 'password'=>$password, 'email'=>$email, 'group_id'=>$groupId, 'joined'=>$joined));

?>