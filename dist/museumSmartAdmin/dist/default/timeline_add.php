<?php

//$config = require_once('private/config.php');

//require_once($config['general-urls']['DBConnect']);

//require_once($config['lib-lincludes']['aditivo-libs'] . 'KeepMeLoggedIn.php');

/*$keep = new KeepMeLoggedIn();

if($keep->isCookieRegistred()){

    (!$keep->isValidCookie()) ? $keep->redirectToLoggin() : $keep->getUserData();

}else{

    (!$keep->isValidSession()) ? $keep->redirectToLoggin() : $keep->getUserData();    
}*/

$tagsString = '';

if(isset($_GET['id']) && !empty($_GET['id'])){

    $actualId = $_GET['id'];

    $editMode = true;

    $eventSql = "

    SELECT main.title, main.description, main.date_start, 

    international.title, international.description, international.date_start,

    national.title, national.description, national.date_start 

    FROM timeline_events as main 

    INNER JOIN timeline_event_international as international on main.id = international.id_event 

    INNER JOIN timeline_event_national as national on main.id = national.id_event 

    WHERE main.id = ?";

    $stmt = $conn->prepare($eventSql);

    $stmt->bind_param('i', $actualId);

    $stmt->execute();

    $stmt->store_result();

    $stmt->bind_result($title, $description, $dateStart, $internationalTitle, $internationalDescription, $internationalDate, 

        $nationalTitle, $nationalDescription, $nationalDate);

    $stmt->fetch();

    $mainYear = explode('-', $dateStart)[0];

    $mainMonth = explode('-', $dateStart)[1];

    $mainDay = substr(explode('-', $dateStart)[2], 0, 2);


    if(!empty($internationalDate)){

        $internationalYear = explode('-', $internationalDate)[0];

        $internationalMonth = explode('-', $internationalDate)[1];

        $internationalDay = substr(explode('-', $internationalDate)[2], 0, 2);

    }else{

        $internationalYear = 1933;

        $internationalMonth = 01;

        $internationalDay = 01;

    }

    if(!empty($nationalDate)){

        $nationalYear = explode('-', $nationalDate)[0];

        $nationalMonth = explode('-', $nationalDate)[1];

        $nationalDay = substr(explode('-', $nationalDate)[2], 0, 2);

    }else{

        $nationalYear = 1933;

        $nationalMonth = 01;

        $nationalDay = 01;
    }

    //...

    $sql = "SELECT timelineTags.id, timelineTags.id_tag, tags.tag FROM timeline_tags as timelineTags

    INNER JOIN tags as tags ON timelineTags.id_tag = tags.id WHERE timelineTags.id_timeline_event = ? AND 

    timelineTags.active = ? AND timelineTags.deleted = ?";

    $tagsStmt = $conn->prepare($sql);

    $deleted = 0;

    $active = 1;

    $tagsStmt->bind_param('iii', $actualId, $active, $deleted);

    $tagsStmt->execute();

    $tagsStmt->store_result();

    $tagsStmt->bind_result($idRelation, $idTag, $tag);

    if ($tagsStmt->num_rows >= 1) {

        while($tagsStmt->fetch()){

            /*$item = array();

            $item['idRelation'] = $idRelation;

            $item['idTag'] = $idTag;

            $item['idEvent'] = $id;

            $item['tag'] = $tag;

             $result[] = $item;*/

             $tagsString .= $tag . ', ';

        }

    }

    
}else{

    $editMode = false;

    $mainYear = 1933;

    $mainMonth = 01;

    $mainDay = 1;

    $actualId = -1;

    $internationalYear = 1933;

    $internationalMonth = 01;

    $internationalDay = 01;

    $nationalYear = 1933;

    $nationalMonth = 01;

    $nationalDay = 01;

}

?>

<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->

<!--[if !IE]><!-->

<html lang="en">

<!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

    <meta charset="utf-8" />

    <title>Museo de la Shoá | Timeline | Agregar Evento</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta content="width=device-width, initial-scale=1" name="viewport" />

    <meta content="admin para administradores..." name="description" />

    <meta content="Mariano Makedonsky" name="author" />

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="js/plugins/tagify/dist/tagify.css">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet" type="text/css" />

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" type="text/css" />     

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.0/css/fileinput.min.css" rel="stylesheet" type="text/css" />

    <link href="compatibility/css/image-crop.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link href="compatibility/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    
    <link href="compatibility/css/plugins.min.css" rel="stylesheet" type="text/css" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.min.css" rel="stylesheet" type="text/css" />
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.skinFlat.min.css" rel="stylesheet" type="text/css" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css" />
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" rel="stylesheet" type="text/css" />

    <link href="js/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet" type="text/css" />
   
    <link href="js/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet" type="text/css" />

    <link href="compatibility/css/layout.min.css" rel="stylesheet" type="text/css" />

    <link href="compatibility/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        
    <link href="compatibility/css/custom.css" rel="stylesheet" type="text/css" />

</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">

    <div class="clearfix"> </div>
    
    <div class="page-container">

        <div class="page-content-wrapper">

            <div class="page-content">

                <div class="row">

                   <div class="col-md-12">

                    <div class="portlet light portlet-fit portlet-form bordered">

                        <div class="portlet-title tabbable-line">

                            <div class="caption caption-md">

                                <i class="icon-globe theme-font hide"></i>

                                <span class="caption-subject font-blue-madison bold uppercase">

                                    Linea de tiempo - <?php if($editMode != true){ echo 'agregar evento';}else{ echo 'editar <b>' . $title . '</b>';}?>

                                </span>

                            </div>

                            <br>

                            <br>

                            <button id="delete-event" type="button" class="btn red hide">ELIMINAR EVENTO</button>

                            <br>


                            <ul class="nav nav-tabs">

                                <li>

                                    <a href="#main-data" data-toggle="tab"><?php if($editMode == false) echo 'CREAR EVENTO'; else echo 'EDITAR EVENTO';?></a>

                                </li>

                                <li>

                                    <a class="<?php if($editMode == false) echo 'tab-disabled';?> nav-tab" 

                                        href="#international" data-toggle="<?php if($editMode == true) echo 'tab';?>">CONTEXTO INTERNACIONAL

                                    </a>

                                </li>

                                <li>

                                    <a class="<?php if($editMode == false) echo 'tab-disabled';?> nav-tab"  

                                        href="#national" data-toggle="<?php if($editMode == true) echo 'tab';?>">CONTEXTO NACIONAL

                                    </a>

                                </li>

                                <li>

                                    <a class="<?php if($editMode == false) echo 'tab-disabled';?> nav-tab"  

                                        href="#images-gallery" data-toggle="<?php if($editMode == true) echo 'tab';?>">GALERÍA MULTIMEDIA

                                    </a>

                                </li>

                                <li>

                                    <a class="<?php if($editMode == false) echo 'tab-disabled';?> nav-tab"  

                                        href="#tags" data-toggle="<?php if($editMode == true) echo 'tab';?>">TAGS

                                    </a>

                                </li>

                            </ul>

                        </div>

                        <div class="portlet-body">

                            <div class="tab-content">

                                <div class="tab-pane" id="main-data">

                                    <form role="form" action="#" id="new-event-form" class="form-horizontal">

                                        <div class="form-body">

                                            <div class="alert alert-danger display-hide">

                                                <button class="close" data-close="alert"></button> 

                                                Hay algunos errores en el formulario. Revisá los campos en rojo 

                                            </div>

                                            <div class="alert alert-success display-hide">

                                                <button class="close" data-close="alert"></button> 

                                                Validación exitosa! 

                                            </div>

                                            <!-- nombre -->

                                            <div class="form-group form-md-line-input margin-top-20">

                                                <label class="col-md-3 control-label" for="form_control_1">Nombre del evento

                                                    <span class="required" aria-required="true">*</span>

                                                </label>

                                                <div class="col-md-6">

                                                    <input id="event-name" type="text" class="form-control" placeholder="" name="eventName"

                                                    value= "<?php if($editMode == true) echo $title;?>">

                                                    <div class="form-control-focus"> 

                                                    </div>

                                                    <span class="help-block">por ejemplo: <b>la quema de libros.</b></span>

                                                </div>

                                            </div>

                                            <!-- descripcion -->

                                            <div class="form-group form-md-line-input margin-top-20">

                                                <label class="col-md-3 control-label" for="form_control_1">Descripción / desarrollo

                                                    <span class="required" aria-required="true">*</span>

                                                </label>

                                                <div class="col-md-6">

                                                    <textarea id="event-description" type="text" class="form-control" placeholder="" rows="6" name="eventDescription"><?php if($editMode == true) echo html_entity_decode($description);?></textarea>

                                                    <div class="form-control-focus"> </div>

                                                    <span class="help-block"><b>...</b></span>

                                                </div>

                                            </div> 


                                            <!-- año -->

                                            <div class="form-group margin-top-20">

                                                <label class="control-label col-md-3"> año del evento: 

                                                    <span class="required" aria-required="true">*</span>

                                                </label>

                                                <div class="col-md-6">

                                                    <input type="text" id="years-slider" value="" name="eventYear">

                                                </div>

                                            </div>

                                            <!-- mes -->

                                            <div class="form-group margin-top-20">

                                                <label class="control-label col-md-3"> mes: 

                                                    <span class="required" aria-required="true">*</span>

                                                </label>

                                                <div class="col-md-6">

                                                    <input type="text" id="months-slider" value="" name="eventMonth" />

                                                </div>

                                            </div>

                                            <div class="form-group  margin-top-20">

                                                <label class="control-label col-md-3 tooltips" data-container="body" data-placement="top" 

                                                data-original-title="Si este evento ocurrió un día exacto, lo podemos mostrar en la linea de tiempo. Caso contrario, dejá tildado en NO y solamente se mostrará el mes.">¿Sabés el día exacto?</label>

                                                <div class="col-md-5">

                                                    <div class="input-icon right">

                                                        <input name="toggleDaysSlider" class="toggle-days-slider big" type="checkbox" checked data-toggle="toggle" data-size="small" data-on="SI" data-off="NO">

                                                    </div>

                                                </div>

                                            </div>

                                            <div class="form-group margin-top-20">

                                                <label class="control-label col-md-3"> dia:</label>

                                                <div class="col-md-6">

                                                    <input type="text" id="days-slider" value="" name="eventDay" />

                                                </div>

                                            </div>

                                        </div>

                                        <div class="form-actions">

                                            <div class="margiv-top-10 col-md-offset-3">

                                             <button id="create-event" type="submit" 

                                             class="btn green <?php if($editMode == true) echo 'hide';?>">CREAR EVENTO</button>

                                             <button id="cancel-event" type="button" class="btn grey-salsa btn-outline">SALIR</button>

                                         </div>

                                     </div>

                                 </form>
                             </div>

                             <!-- END CHANGE event-information TAB -->

                             <div class="tab-pane" id="international">

                                <form role="form" action="#" id="international-form" class="form-horizontal">

                                    <div class="form-body">

                                        <div class="alert alert-danger-3 display-hide">

                                            <button class="close" data-close="alert"></button> 

                                            Hay algunos errores en el formulario. Revisá los campos en rojo 

                                        </div>

                                        <div class="alert alert-success-3 display-hide">

                                            <button class="close" data-close="alert"></button> 

                                            Validación exitosa! 

                                        </div>

                                        <!-- otro titulo? -->

                                        <div class="form-group margin-top-20">

                                            <label class="control-label col-md-3 tooltips">¿Usar otro título?<br><small class="snippet-international-title"><b>(Se usará el título "contexto internacional")</b></small></label>

                                            <div class="col-md-5">

                                                <div class="input-icon right">

                                                    <input name = "toggleInternationalTitle" class="toggle-international-title big" type="checkbox" 

                                                    data-toggle="toggle" data-size="small" data-on="SI" data-off="NO" 

                                                    <?php if($editMode == true  && $internationalTitle != "") echo 'checked';?>>

                                                </div>

                                            </div>

                                        </div>

                                        <!-- titulo del evento -->

                                        <div class="form-group form-md-line-input margin-top-20" id="international-context-title">

                                            <label class="col-md-3 control-label" for="form_control_1">Título:</label>

                                            <div class="col-md-6">

                                                <input id="international-title" type="text" class="form-control" name="eventSubtitle"

                                                value="<?php if($editMode == true) echo $internationalTitle;?>">

                                                <div class="form-control-focus"> 

                                                </div>

                                                <span class="help-block"><b>Por ejemplo:</b>mientras tanto en el mundo...</span>

                                            </div>

                                        </div>

                                        <div class="form-group form-md-line-input margin-top-20">

                                            <label class="col-md-3 control-label" for="form_control_1">Descripción / desarrollo</label>

                                            <div class="col-md-6">

                                                <textarea id="international-description" type="text" class="form-control" placeholder="" rows="6" name="eventInternationalDescription"><?php if($editMode == true) echo $internationalDescription;?></textarea>

                                                <div class="form-control-focus"> 

                                                </div>

                                                <span class="help-block"><b>...</b></span>

                                            </div>

                                        </div>

                                        <!-- fecha? -->

                                        <div class="form-group margin-top-20">

                                            <label class="control-label col-md-3 tooltips">¿Usar otra fecha?<br><small class="snippet-international-date"><b>(Se usará la que definiste al crear evento)</b></small></label>

                                            <div class="col-md-5">

                                                <div class="input-icon right">

                                                    <input name = "toggleInternationalDate" class="toggle-international-date big" type="checkbox" data-toggle="toggle" data-size="small" data-on="SI" data-off="NO">

                                                </div>

                                            </div>

                                        </div>

                                        <!-- slider year otra fecha -->

                                        <div class="form-group margin-top-20">

                                            <label class="control-label col-md-3">año del evento: 

                                                <span class="required" aria-required="true">*</span>

                                            </label>

                                            <div class="col-md-6">

                                                <input type="text" id="years-slider-international" value="" name="eventInternationalYear" />

                                            </div>

                                        </div>

                                        <div class="form-group margin-top-20">

                                            <label class="control-label col-md-3">mes del evento: 

                                                <span class="required" aria-required="true">*</span>

                                            </label>

                                            <div class="col-md-6">

                                                <input type="text" id="months-slider-international" value="" name="eventInternationalMonth" />

                                            </div>

                                        </div>

                                        <div class="form-group margin-top-20" id="toggle-days-slider-international">

                                            <label class="control-label col-md-3 tooltips" data-container="body" data-placement="top" 

                                            data-original-title="Si este evento ocurrió un día exacto, lo podemos mostrar en la linea de tiempo. Caso contrario, dejá tildado en NO y solamente se mostrará el mes.">¿Sabés el día exacto?</label>

                                            <div class="col-md-5">

                                                <div class="input-icon right">

                                                    <input name="toggleDaysSliderInternational" class="toggle-days-slider-international big" type="checkbox" checked data-toggle="toggle" data-size="small" data-on="SI" data-off="NO">

                                                </div>

                                            </div>

                                        </div>

                                        <div id = "national-context-month" class="form-group margin-top-20">

                                            <label class="control-label col-md-3">dia del evento: 

                                                <span class="required" aria-required="true">*</span>

                                            </label>

                                            <div class="col-md-6">

                                                <input type="text" id="days-slider-international" value="" name="eventInternationalDay" />

                                            </div>

                                        </div>



                                    </div>

                                    <div class="form-actions">

                                        <div class="margiv-top-10 col-md-offset-3">

                                         <button id="add-international-info" type="submit" class="btn green form-button">Agregar Información</button>

                                         <!--<button id="delete-event" type="button" class="btn red ">ELIMINAR TODO</button>-->


                                     </div>

                                 </div>

                             </form>

                         </div>

                         <!-- national / nacional -->


                         <div class="tab-pane" id="national">

                            <form role="form" action="#" id="national-form" class="form-horizontal">

                                <div class="form-body">

                                    <div class="alert alert-danger-4 display-hide">

                                        <button class="close" data-close="alert"></button> 

                                        Hay algunos errores en el formulario. Revisá los campos en rojo 

                                    </div>

                                    <div class="alert alert-success-4 display-hide">

                                        <button class="close" data-close="alert"></button> 

                                        Validación exitosa! 

                                    </div>

                                    <!-- otro titulo? -->

                                    <div class="form-group margin-top-20">

                                        <label class="control-label col-md-3 tooltips">¿Usar otro título?<br><small class="snippet-national-title"><b>(Se usará el título "contexto nacional")</b></small></label>

                                        <div class="col-md-5">

                                            <div class="input-icon right">

                                                <input name = "toggleNationalTitle" class="toggle-national-title big" type="checkbox" data-toggle="toggle" 

                                                data-size="small" data-on="SI" data-off="NO"

                                                <?php if($editMode == true  && $nationalTitle != "") echo 'checked';?>>

                                            </div>

                                        </div>

                                    </div>

                                    <!-- titulo del evento -->

                                    <div class="form-group form-md-line-input margin-top-20" id="national-context-title">

                                        <label class="col-md-3 control-label" for="form_control_1">Título:</label>

                                        <div class="col-md-6">

                                            <input id="national-title" type="text" class="form-control" placeholder="" name="eventSubtitle"

                                            value="<?php if($editMode == true) echo $nationalTitle;?>">

                                            <div class="form-control-focus"> 

                                            </div>

                                            <span class="help-block"><b>Por ejemplo:</b>mientras tanto en Argentina...</span>

                                        </div>

                                    </div>

                                    <div class="form-group form-md-line-input margin-top-20">

                                        <label class="col-md-3 control-label" for="form_control_1">Descripción / desarrollo</label>

                                        <div class="col-md-6">

                                            <textarea id="national-description" type="text" class="form-control" placeholder="" rows="6" name="eventNationalDescription"><?php if($editMode == true) echo $nationalDescription;?></textarea>

                                            <div class="form-control-focus"> 

                                            </div>

                                            <span class="help-block"><b>...</b></span>

                                        </div>

                                    </div>

                                    <!-- fecha? -->

                                    <div class="form-group margin-top-20">

                                        <label class="control-label col-md-3 tooltips">¿Usar otra fecha?<br><small class="snippet-national-date"><b>(Se usará la que definiste al crear evento)</b></small></label>

                                        <div class="col-md-5">

                                            <div class="input-icon right">

                                                <input name="toggleNationalDate" class="toggle-national-date big" type="checkbox" data-toggle="toggle" data-size="small" data-on="SI" data-off="NO">

                                            </div>

                                        </div>

                                    </div>

                                    <!-- slider year otra fecha -->

                                    <div class="form-group margin-top-20">

                                        <label class="control-label col-md-3">año del evento: </label>

                                        <div class="col-md-6">

                                            <input type="text" id="years-slider-national" value="" name="eventNationalYear" />

                                        </div>

                                    </div>

                                    <div class="form-group margin-top-20">

                                        <label class="control-label col-md-3">mes del evento: 

                                            <span class="required" aria-required="true">*</span>

                                        </label>

                                        <div class="col-md-6">

                                            <input type="text" id="months-slider-national" value="" name="eventNationalMonth" />

                                        </div>

                                    </div>

                                    <div class="form-group margin-top-20" id="toggle-days-slider-national">

                                        <label class="control-label col-md-3 tooltips">¿Sabés el día exacto?</label>

                                        <div class="col-md-5">

                                            <div class="input-icon right">

                                                <input name="toggleDaysSliderNational" class="toggle-days-slider-national big" type="checkbox" checked data-toggle="toggle" data-size="small" data-on="SI" data-off="NO">

                                            </div>

                                        </div>

                                    </div>

                                    <div id = "national-context-month" class="form-group margin-top-20">

                                        <label class="control-label col-md-3">dia del evento: 

                                            <span class="required" aria-required="true">*</span>

                                        </label>

                                        <div class="col-md-6">

                                            <input type="text" id="days-slider-national" value="" name="eventNationalDay" />

                                        </div>

                                    </div>



                                </div>

                                <div class="form-actions">

                                    <div class="margiv-top-10 col-md-offset-3">

                                     <button id="add-national-info" type="submit" class="btn green form-button">Agregar Información</button>

                                 </div>

                             </div>

                         </form>

                     </div>

                     <div class="tab-pane" id="images-gallery">

                        <form id="fileupload" action="private/actions/images-management/upload.php" method="POST" enctype="multipart/form-data">

                            <div class="row fileupload-buttonbar">

                                <div class="col-lg-7">

                                    <span class="btn green fileinput-button">

                                        <i class="fa fa-plus"></i>

                                        <span>Agregar imagen/video...</span>

                                        <input type="file" name="files[]" multiple=""> </span>

                                        <button type="submit" class="btn blue start start-upload-btn">

                                            <i class="fa fa-upload"></i>

                                            <span>Subir</span>

                                        </button>


                                        <span class="fileupload-process"> </span>

                                    </div>

                                    <div class="col-lg-5 fileupload-progress fade">

                                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">

                                            <div class="progress-bar progress-bar-success" style="width:0%;"> </div>

                                        </div>

                                        <div class="progress-extended"> &nbsp; </div>

                                    </div>

                                </div>

                                <table role="presentation" class="table table-striped clearfix">

                                    <tbody class="files"> </tbody>

                                </table>

                            </form>

                        </div>

                        
                        <div class="tab-pane" id="tags">

                            <form role="form" action="#" id="new-event-form" class="form-horizontal">

                                <div class="form-body">

                                    <!-- tags -->

                                    <div class="form-group form-md-line-input margin-top-20" id="international-context-title">

                                        <label class="col-md-3 control-label" for="form_control_1">Tags:</label>

                                        <div class="col-md-6">

                                            <input name='tags' placeholder='agregar tags' value='<?php echo $tagsString;?>'>

                                            <div class="form-control-focus"> 

                                            </div>

                                            <span class="help-block"><b>Por ejemplo:</b>mientras tanto en el mundo...</span>

                                        </div>

                                    </div>


                                </div>

                            </form>

                            
                         </div>

                 </div>

             </div>

         </div>

     </div>

 </div>

</div>

</div>

</div>

<?php //require_once($config['url-includes']['footer']); ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>

<script type="text/javascript" src="js/plugins/tagify/dist/tagify.js"></script>

<script type="text/javascript" src="js/plugins/tagify/dist/jQuery.tagify.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/additional-methods.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/js/ion.rangeSlider.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js" type="text/javascript"></script>

<script src="js/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js" type="text/javascript"></script>

<script src="js/plugins/jquery-file-upload/js/vendor/tmpl.min.js" type="text/javascript"></script>

<script src="js/plugins/jquery-file-upload/js/vendor/load-image.min.js" type="text/javascript"></script>

<script src="js/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js" type="text/javascript"></script>

<script src="js/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js" type="text/javascript"></script>

<script src="js/plugins/jquery-file-upload/js/jquery.iframe-transport.js" type="text/javascript"></script>

<script src="js/plugins/jquery-file-upload/js/jquery.fileupload.js" type="text/javascript"></script>

<script src="js/plugins/jquery-file-upload/js/jquery.fileupload-process.js" type="text/javascript"></script>

<script src="js/plugins/jquery-file-upload/js/jquery.fileupload-image.js" type="text/javascript"></script>

<script src="js/plugins/jquery-file-upload/js/jquery.fileupload-audio.js" type="text/javascript"></script>

<script src="js/plugins/jquery-file-upload/js/jquery.fileupload-video.js" type="text/javascript"></script>

<script src="js/plugins/jquery-file-upload/js/jquery.fileupload-validate.js" type="text/javascript"></script>

<script src="js/plugins/jquery-file-upload/js/jquery.fileupload-ui.js" type="text/javascript"></script>

<script src="js/app.min.js" type="text/javascript"></script>

<script src="js/layout.min.js" type="text/javascript"></script>   

<script id="template-upload" type="text/x-tmpl"> {% for (var i=0, file; file=o.files[i]; i++) { %}

    <tr class="template-upload fade">

        <td>

            <span class="preview"></span>

        </td>

        <td>

            <p class="name">{%=file.name%}</p>

            <strong class="error text-danger label label-danger"></strong>

        </td>

        <td>

            <p class="size">Procesando...</p>

            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">

                <div class="progress-bar progress-bar-success" style="width:0%;"></div>

            </div>

        </td>

        <td> {% if (!i && !o.options.autoUpload) { %}

            <button class="btn blue start" disabled>

                <i class="fa fa-upload"></i>

                <span>Subir</span>

            </button> {% } %} {% if (!i) { %}

            <button class="btn red cancel">

                <i class="fa fa-ban"></i>

                <span>Cancelar</span>

            </button> {% } %} 

        </td>

    </tr> {% } %} 

</script>

<script id="template-download" type="text/x-tmpl"> {% for (var i=0, file; file=o.files[i]; i++) { %}

    <tr class="template-download fade">

        <td>

            <span class="preview"> {% if (file.thumbnailUrl) { %}

                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery>

                    <img src="{%=file.thumbnailUrl%}">

                </a> {% } %} 

            </span>

        </td>

        <td>

            <p class="name"> {% if (file.url) { %}

                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl? 'data-gallery': ''%}>{%=file.name%}</a> {% } else { %}

                <span>{%=file.name%}</span> {% } %} 

            </p> {% if (file.error) { %}

            <div>

                <span class="label label-danger">Error</span> {%=file.error%}

            </div> {% } %} 

        </td>

        <td>

            <span class="size">{%=o.formatFileSize(file.size)%}</span>

        </td>

        <td> {% if (file.deleteUrl) { %}

            <button class="btn red delete btn-sm" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}" {% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}' {% } %}>

                <i class="fa fa-trash-o">

                </i>

                <span>Delete</span>

            </button>

            <!--<input type="checkbox" name="delete" value="1" class="toggle"> {% } else { %}-->

            <button class="btn yellow cancel btn-sm">

                <i class="fa fa-ban"></i>

                <span>Cancel</span>

            </button> {% } %}

        </td>

    </tr> {% } %} 

</script>

<script type="text/javascript"> 

    var mainYear = <?php echo json_encode($mainYear); ?>; 

    var mainMonth = <?php echo json_encode($mainMonth); ?>; 

    var mainDay = <?php echo json_encode($mainDay); ?>; 

    var internationalYear = <?php echo json_encode($internationalYear); ?>; 

    var internationalMonth = <?php echo json_encode($internationalMonth); ?>; 

    var internationalDay = <?php echo json_encode($internationalDay); ?>; 

    var nationalYear = <?php echo json_encode($nationalYear); ?>; 

    var nationalMonth = <?php echo json_encode($nationalMonth); ?>; 

    var nationalDay = <?php echo json_encode($nationalDay); ?>; 

    var editMode = <?php echo json_encode($editMode); ?>; 

    var actualId = <?php echo json_encode($actualId); ?>;

    var dateStart = <?php echo json_encode($dateStart); ?>;

    var internationalDate = <?php echo json_encode($internationalDate); ?>;

    var nationalDate = <?php echo json_encode($nationalDate); ?>;

</script>  

<script src="js/timeline_add.js" type="text/javascript"></script>

</body>

</html>