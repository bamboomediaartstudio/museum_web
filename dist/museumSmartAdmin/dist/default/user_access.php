<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

if(Input::exists("get") && Input::get('id')){

	$actualId = Input::get('id');

	$editMode = true;

	$query = DB::getInstance()->query(

		"SELECT *,

		users.id 						as 			mid,

		mi.unique_id					as 			unique_id

		FROM users 						as 			users

		LEFT JOIN museum_images 		as 			mi

		ON users.id = mi.sid 

		AND mi.source = ?

		WHERE users.id = ? AND users.deleted = ?", array('profile', Input::get('id'), 0));

	$actualUser = $query->first();

	$name = $actualUser->name;

	$surname = $actualUser->surname;

	$email = $actualUser->email;

	$uniqueId = $actualUser->unique_id;

	$joined = $actualUser->joined;

	$lastLogin = $actualUser->last_login;

	$logins = $actualUser->logins;

	if($uniqueId != null){

		$userImage = 'private/sources/images/profile/' . $actualUser->mid . '/' . $actualUser->unique_id . '/' . $actualUser->mid . '_' . $actualUser->unique_id . '_small_sq@2x.jpeg';

	}else{

		$userImage = 'private/img/user.png';

	}
}

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Accesos y permisos para un usuario.</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});
		
	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
	
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<input type="hidden" name="admin-id" id="admin-id" value="<?php echo $user->data()->id;?>">

	<input type="hidden" name="user-id" id="user-id" value="<?php echo $actualId;?>">

	<input type="hidden" name="user-name" id="user-name" value="<?php echo $name;?>">

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title ">

								Permisos para <?php echo $name;?>

							</h3>

							<p><strong><?php echo $user->data()->name . '</strong>, un gran poder conlleva una gran responsabilidad! :)<br>Desde esta página podés asignar o eliminar permisos y privilegios a <strong>' . $name;?></strong></p>	

						</div>

					</div>

				</div>
				
				<div class="m-content">

					<div class="row">

						<div class="col-xl-3 col-lg-4">

							<div class="m-portlet m-portlet--full-height  ">

								<div class="m-portlet__body">

									<div class="m-card-profile">

										<div class="m-card-profile__pic">

											<div class="m-card-profile__pic-wrapper">

												<img src="<?php echo $userImage;?>" alt="<?php echo $name . ' ' . $surname;?>"/>

											</div>

										</div>

										<div class="m-card-profile__details">

											<span class="m-card-profile__name">

												<?php echo $name . ' ' . $surname;?>

											</span>

											<a href="" class="m-card-profile__email m-link">

												<?php echo $email;?>

											</a>

										</div>

									</div>


									<div class="m-portlet__body-separator"></div>

									<p class="text-center">miembro desde: <?php echo $joined;?></p>

									<p class="text-center">última sesión: <?php echo $lastLogin;?></p>

									<p class="text-center">logins desde el inicio: <?php echo $logins;?></p><br>

								</div>
							</div>
						</div>

						<div class="col-xl-9 col-lg-8">
							
							<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" 
										role="tablist">

										<li class="nav-item m-tabs__item">
											
											<a class="nav-link m-tabs__link active" data-toggle="tab" href="
											#m_user_profile_tab_1" role="tab">

											<i class="flaticon-share m--hide"></i>

											Listado de permisos

										</a>

									</li>				

								</ul>

							</div>

						</div>

						<div class="tab-content">

							<div class="tab-pane active" id="m_user_profile_tab_1">


								<div class="m-portlet__body">

									<div class="form-group m-form__group row">

										<label class="col-form-label col-lg-3 col-sm-12">USUARIO MASTER<br>

											<small data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Implica que <?php echo $name;?> tendá control absoluto sobre el administrador: sintéticamente, contará con tu mimos privilegios y podrá realizar todas tus mismas acciones. Es importante saber que los usuarios master no se pueden dar de baja una vez creados o dados de alta.">¿qué implica?</small>		

										</label>

										<div class="col-lg-4 col-md-9 col-sm-12">

											<div class="m-form__group form-group row">

												<div class="col-3">

													<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

														<button class="give-master-access btn m-btn--pill m-btn m-btn--gradient-from-primary m-btn--gradient-to-info">ASIGNAR PRIORIDAD MASTER</button>

													</span>

												</div>

											</div>

										</div>

									</div>

									<?php

									$query = DB::getInstance()->query(

										"SELECT 										*,

										masg.id 										as mid

										FROM museum_admin_sections_groups 				as masg

										LEFT JOIN museum_admin_sections_users_relations as masur

										ON masg.id = masur.id_group 

										AND masur.id_user 								= ?

										WHERE masg.active 								= ? 

										AND masg.deleted 								= ?

										ORDER by masg.id ASC", 

										array($actualId,1, 0));


									foreach($query->results() as $result){

											//echo 'status: ' . $result->status;

										?>

										<div class="form-group m-form__group row">

											<label class="col-form-label col-lg-3 col-sm-12"><?php echo $result->group_name;?><br>

												<small data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?php echo $name . ' ' . $result->description;?>">¿qué implica?</small>		

											</label>

											<div class="col-lg-4 col-md-9 col-sm-12">

												<div class="m-form__group form-group row">

													<div class="col-3">

														<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

															<label>

																<input <?php if($result->status==1) echo 'checked';?>  type="checkbox" class="highlighted toggler-info" name="is_highlighted" data-on="Enabled" data-id="<?php echo $result->mid;?>"  data-name="<?php echo $result->group_name;?>">

																<span></span>

															</label>

														</span>

													</div>

												</div>

											</div>

										</div>

									<?php } ?>

								</div>
								
							</div>		

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<?php require_once 'private/includes/footer.php'; ?>

</div>

<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
	<i class="la la-arrow-up"></i>
</div>

<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<script src="assets/vendors/custom/jquery-ui/jquery-ui.bundle.js" type="text/javascript"></script>

<script src="assets/app/js/helper.js" type="text/javascript"></script>

<script src="assets/app/js/users-access.js" type="text/javascript"></script>


</body>

</html>
