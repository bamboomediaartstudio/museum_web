<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title>Museo de la Shoá | Usuarios registrados</title>

	<meta name="description" content="User profile example page">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});
		
	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
	
</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<input type="hidden" name="user-id" id="user-id" value="<?php echo $actualUser;?>">

	<div class="m-grid m-grid--hor m-grid--root m-page">

		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title ">

								USUARIOS

							</h3>

							<p>Sólo los administradores <strong>masters</strong> y <strong>devs</strong> tienen acceso a esta lista.<br>Desde la misma podés asignar o relegar permisos.<br>Si querés dar de alta un nuevo usuario lo podés hacer haciendo <a href="#">click aquí</a>.</p>

						</div>

					</div>

				</div>
				
				<div class="m-content">

					<div class="row">

						<?php

						$query = DB::getInstance()->query(

							"SELECT *,

							users.id 						as 			mid,

							mi.unique_id					as 			unique_id,

							users.name 						as 			name,
							
							users.surname 					as 			surname

							FROM users 						as 			users

							LEFT JOIN museum_images 		as 			mi

							ON users.id = mi.sid 

							AND mi.source = ?

							INNER JOIN users_categories		as 			cat

							ON users.group_id = cat.id

							WHERE users.deleted = ? ORDER BY cat.id ", array('profile', 0));

						$usersList = $query->results();

						$counter = 0;

						foreach($usersList as $actualUser){

							if($user->data()->id == $actualUser->mid) continue;

							$name = $actualUser->name;

							$surname = $actualUser->surname;

							$email = $actualUser->email;

							$id = $actualUser->mid;

							//$phone = $actualUser->phone;

							//$birthday = $actualUser->birthday;

							$lastLogin = $actualUser->last_login;

							$totalLogins =  $actualUser->logins;

							$joined =  $actualUser->joined;

							//echo $actualUser->category_description;

							if($actualUser->unique_id){

								$userImage = 'private/sources/images/profile/' . $actualUser->mid . '/' . $actualUser->unique_id . '/' . $actualUser->mid . '_' . $actualUser->unique_id . '_small_sq@2x.jpeg';

							}else{

								$userImage = 'private/img/user.png';

							}

							?>

							<div class="col-xl-3 col-lg-4">

								<div class="m-portlet m-portlet--full-height  ">

									<div class="m-portlet__body">

										<div class="m-card-profile">

											<div class="m-card-profile__pic">

												<div class="m-card-profile__pic-wrapper">

													<img src="<?php echo $userImage;?>" alt="<?php echo $user->data()->name . 	' ' . $user->data()->surname;?>"/>

												</div>

											</div>

											<div class="m-card-profile__details">

												<span class="m-card-profile__name">

													<?php echo $name . ' ' . $surname;?>

												</span>

												<p class="text-truncate">

													<a href="" class="m-card-profile__email m-link text-truncate">

														<?php echo $email;?>

													</a>

												</p>

												<p class="text-truncate">

													<strong>última sesión: </strong><?php echo $lastLogin;?>

												</p>

												<p class="text-truncate">

													<strong>Sesiones desde el inicio: </strong><?php echo $totalLogins;?>

												</p>

												<p class="text-truncate">

													<strong>Miembro desde: </strong><?php echo $joined;?>

												</p>


											</div>


										</div>


									</div>

									<div class="text-center"> 

										<?php if($actualUser->group_id == 1){ ?>

											<button

											data-title= "<?php echo $actualUser->label;?>" 

											data-category="<?php echo $actualUser->category_description;?>" 

											type="button" class="info-button btn m-btn m-btn--pill m-btn--air m-btn--gradient-from-metal m-btn--gradient-to-accent">developer</button>

										<?php }else if($actualUser->group_id == 2){ ?>

											<button 

											data-title= "<?php echo $actualUser->label;?>" 

											data-category="<?php echo $actualUser->category_description;?>" type="button" class="info-button btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent">usuario master</button>

										<?php }else if($actualUser->group_id == 3){ ?>

											<a href="user_access.php?id=<?php echo $id;?>" role="button" class="btn m-btn--pill m-btn m-btn--gradient-from-primary m-btn--gradient-to-info">permisos</a>

											<button 

											data-name= "<?php echo $actualUser->name;?>" 

											data-id= "<?php echo $actualUser->mid;?>" 

											type="button" class="delete-user btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning">Eliminar</button>

											<button 

											data-title= "<?php echo $actualUser->label;?>" 

											data-category="<?php echo $actualUser->category_description;?>" type="button" class="info-button btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent">info</button>


										<?php }else if($actualUser->group_id == 4){ ?>

											<button 

											data-title= "<?php echo $actualUser->label;?>" 

											data-category="<?php echo $actualUser->category_description;?>" type="button" class="info-button btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent">guía</button>

											<a href="user_access.php?id=<?php echo $id;?>" role="button" class="btn m-btn--pill m-btn m-btn--gradient-from-primary m-btn--gradient-to-info">permisos</a>

										<?php }else if($actualUser->group_id == 6){ ?>

											<button 

											data-title= "<?php echo $actualUser->label;?>" 

											data-category="<?php echo $actualUser->category_description;?>" type="button" class="info-button btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent">Anfitrión</button>

											<a href="user_access.php?id=<?php echo $id;?>" role="button" class="btn m-btn--pill m-btn m-btn--gradient-from-primary m-btn--gradient-to-info">permisos</a>

										<?php } ?>




									</div>

								</div>

							</div>

						<?php } ?>	

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
		<i class="la la-arrow-up"></i>
	</div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/vendors/custom/jquery-ui/jquery-ui.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="assets/app/js/users.js" type="text/javascript"></script>


</body>

</html>
