<?php

require_once 'private/users/core/init.php';

require_once 'private/users/core/checker.php';

$user = new User();

//print_r($user->data());

$idUser = $user->data()->id;
$groupId= $user->data()->group_id;

//echo "idUser: ".$idUser." ** rol/groupId: ".$groupId;

//TODO::Mover a clase User si conviene

//Permisos a mostrar en desplegable segun el tipo de user que sea quien esta logueado
//Ninugno puede dar de baja a un user con su mismo rol
$devArr = [1,2,3,4,5]; //Todopoeroso
$hierArr = [2,3,4,5]; //Jerarquia
$masterArr = [2,3,4]; // Master
$adminArr = [];	//Por ahora no puede dar de alta ni de baja a nadie
$guiaArr = [];	//User guia no puede hacer nada. De echo, no tiene que estar ni en el sidebar la opcion de manejar usuarios

$arrRolTypes = array(

		1	=> $devArr,
		2	=> $masterArr,
		3	=> $adminArr,
		4	=> $guiaArr,
		5 => $hierArr

);



/*************************************/
/**** GET PERMSSIONS FOR ADMIN USER */
/*************************************/

$sqlPermissions = "SELECT id, group_name, description FROM museum_admin_sections_groups WHERE active = ? AND deleted = ?";
$queryPermissions = DB::getInstance()->query($sqlPermissions, [1, 0]);
$arrPerm = $queryPermissions->results();


/**
** Page General Data
** Set
*/

$page_title = "Museo de la Shoá | Usuarios | Invitar Usuario";
$page_subtitle_h3 = "Usuarios";
$page_subtitle_li = "Dar de alta usuario";
$page_subtitle_i = "Usuarios";

$page_code_url = "assets/app/js/museum/users/users-invitation-add.js";


$actualTab = 'general-tab';

$editMode = false;


if(Input::exists("get") && Input::get('id')){

	$editMode = true;

}


//museum data...

$museumData = DB::getInstance();

$museumDataQ = $museumData->query('SELECT * FROM museum_data');

if($museumDataQ->count()>0) $museumDataObject = $museumDataQ->first();

//museum social media data...

$museumSocialMediaData = DB::getInstance();

$museumSocialMediaDataQ = $museumSocialMediaData->query('SELECT * FROM museum_social_media');

if($museumSocialMediaDataQ->count()>0) $museumSocialMediaObject = $museumSocialMediaDataQ->first();

?>

<!DOCTYPE html>

<html lang="en" >

<head>

	<meta charset="utf-8" />

	<title><?php echo $page_title; ?></title>

	<meta name="description" content="add new Survivor">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

	<script>

		WebFont.load({

			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},

			active: function() {

				sessionStorage.fonts = true;

			}

		});

	</script>

	<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

	<style>

	.summernote-description-error{

		color: red !important;

	}

	.museum-finder{

		cursor:pointer;

	}

	#editor {overflow:scroll; max-height:300px !important}

	input::-webkit-outer-spin-button,

	input::-webkit-inner-spin-button {

		-webkit-appearance: none;

		margin: 0;
	}


	.disabled {

		pointer-events: none !important;

		opacity: 0.4 !important;

	}

	.tt-query {
		-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}

	.tt-hint {
		color: #999
	}

	.tt-menu {    /* used to be tt-dropdown-menu in older versions */
		width: 422px;
		margin-top: 4px;
		padding: 4px 0;
		background-color: #fff;
		border: 1px solid #ccc;
		border: 1px solid rgba(0, 0, 0, 0.2);
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px;
		-webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		-moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
		box-shadow: 0 5px 10px rgba(0,0,0,.2);
	}

	.tt-suggestion {
		padding: 3px 20px;
		line-height: 24px;
	}

	.tt-suggestion.tt-cursor,.tt-suggestion:hover {
		color: #fff;
		background-color: #0097cf;

	}

	.tt-suggestion p {
		margin: 0;
	}

	.twitter-typeahead{
		width: 100%;
	}

	.adminPermissions{
		display:none;
	}


</style>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

	<div class="m-grid m-grid--hor m-grid--root m-page">


		<?php require_once 'private/includes/header.php'; ?>

		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

			<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">

				<i class="la la-close"></i>

			</button>

			<?php require_once 'private/includes/sidebar.php'; ?>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<div class="m-subheader ">

					<div class="d-flex align-items-center">

						<div class="mr-auto">

							<h3 class="m-subheader__title m-subheader__title--separator"><?php echo $page_subtitle_h3; ?></h3>
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

								<li class="m-nav__item m-nav__item--home">

									<a href="index.php" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a>

								</li>

								<li class="m-nav__separator">-</li>

								<li class="m-nav__item">

									<a href="" class="m-nav__link">

										<span class="m-nav__link-text"><?php echo $page_subtitle_li; ?></span>

									</a>

								</li>

							</ul>

						</div>

					</div>

				</div>

				<div class="m-content">

					<div class="row">

						<div class="col-xl-12 col-lg-12">

							<div class="m-portlet m-portlet--full-height m-portlet--tabs">

								<div class="m-portlet__head">

									<div class="m-portlet__head-tools">

										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">

											<li class="nav-item m-tabs__item">

												<a class="tab-navigation nav-link m-tabs__link <?php if($actualTab =='general-tab') echo 'active';?>" data-toggle="tab" href="#general-tab" role="tab">

													<i class="flaticon-share m--hide"></i>

													Información general

												</a>

											</li>

										</ul>

									</div>

								</div>


								<div class="tab-content">

									<!-- general tab -->

									<div class="tab-pane <?php if($actualTab =='general-tab') echo 'active';?>" id="general-tab">

										<form id="add-users-invitation-form" class="m-form m-form--fit m-form--label-align-right" action="">

											<input type="hidden" id="edit-mode" name="edit-mode" value="<?php echo $editMode;?>">

											<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">

												<div class="m-alert__icon">

													<i class="la la-warning"></i>

												</div>

												<div class="m-alert__text">

													Opss! hay algunos errores en el formulario. Por favor, revisalos.

												</div>

												<div class="m-alert__close">

													<button type="button" class="close" data-close="alert" aria-label="Close">

													</button>

												</div>

											</div>

											<!-- Nombre -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Nombre:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="name" name="name" type="text" class="form-control m-input" placeholder="Nombre" value="">

												</div>

											</div>
											<!-- /.Nombre -->

											<!-- Apellido -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Apellido:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="last_name" name="last_name" type="text" class="form-control m-input" placeholder="Apellido" value="">

												</div>

											</div>
											<!-- /.Apellido -->

											<!-- Email -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Email:</label>

												<div class="col-lg-4 col-12 input-group">

													<input id="email" name="email" type="email" class="form-control m-input" placeholder="Email" value="">

												</div>

											</div>
											<!-- /.Email -->

											<!-- Category -->
											<div class="form-group m-form__group row pt-4">

												<label class="col-form-label col-lg-3 col-12">Categoria:

													<br>
													<small data-toggle="tooltip" data-placement="bottom" title="Seleccioná un rol para este nuevo usuario">
														¡Lee esto!
													</small>

												</label>

												<div class="col-lg-4 col-12 input-group">

													<select class="selectCat" id="rol" name="rol">

														<option value="">Seleccionar</option>

													<?php

													$sqlCats = "SELECT id, label, category_description FROM users_categories WHERE active = ? AND deleted = ?";

													$catsQuery = DB::getInstance()->query($sqlCats, [1, 0]);

													$catsArray = $catsQuery->results(true);

													foreach($catsArray as $key => $catsData){

														if(	in_array($catsData["id"], $arrRolTypes[$groupId])){

															?>

															<option value="<?php echo $catsData["id"]; ?>"><?php echo $catsData["label"]; ?></option>

															<?php
														}

													}

													?>

													</select>

												</div>

											</div>
											<!-- /.Category -->

											<div class="adminPermissions">

											<div class="col">
												<div class="col-lg-12 col-12">

													<h5>Seleccioná los permiso que le querés asignar al nuevo usuario:</h5>

												</div>
											</div>

											<?php

											foreach($arrPerm as $result){

													//echo 'status: ' . $result->status;

												?>

												<div class="form-group m-form__group row">

													<label class="col-form-label col-lg-3 col-sm-12"><?php echo $result->group_name;?>

														<br>
														<small data-toggle="tooltip" data-placement="bottom" title="<?php echo $result->description;?>">
															Que implica?
														</small>

													</label>

													<div class="col-lg-4 col-md-9 col-sm-12">

														<div class="m-form__group form-group row">

															<div class="col-3">

																<span class="m-switch m-switch--outline m-switch--icon m-switch--success">

																	<label>
																		<input type="checkbox" id="permission" name="permission[]" class="highlighted toggler-info" data-on="Enabled" data-name="<?php echo $result->group_name;?>" value="<?php echo $result->id; ?>">

																		<span></span>

																	</label>

																</span>

															</div>

														</div>

													</div>

												</div>


											<?php } ?>

											</div>


											<div class="form-group m-form__group row pt-4">

												<div class="col-lg-6 col-12 ">

													<div class="col-md-12">

														<span class="alert-text"><b>* Recordá</b> La invitación al usuario tiene una duración de 20 hs.</span>

													</div>

												</div>

											</div>

											<!-- ... -->

											<div class="m-portlet__foot m-portlet__foot--fit">

												<div class="m-form__actions">


													<div class="row">

														<div class="col-2"></div>

														<div class="col-7">

															<input type="hidden" name="token" value="<?php echo Token::generate()?>">

															<?php

															$hiddenClass = ($editMode) ? 'd-none' : '';

															?>

															<button id="add_notification" type="submit"

															class="<?php echo $hiddenClass;?> btn btn-accent m-btn m-btn--air m-btn--custom">Invitar</button>
															&nbsp;&nbsp;

															<?php

															$buttonLabel = (!$editMode) ? 'Cancelar' : 'volver';

															$buttonId =

															(!$editMode) ? 'exit_from_form' : 'back_to_list';

															?>

															<button id="<?php echo $buttonId;?>" type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">

																<?php echo $buttonLabel;?>

															</button>

														</div>

													</div>

												</div>

											</div>

										</form>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<?php require_once 'private/includes/footer.php'; ?>

	</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300"> <i class="la la-arrow-up"></i></div>

	<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>

	<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

	<script src="assets/app/js/helper.js" type="text/javascript"></script>

	<script src="<?php echo $page_code_url; ?>" type="text/javascript"></script>

</body>

</html>
