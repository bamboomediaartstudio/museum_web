  /**
  * @summary Helpers for the framework :)
  *
  * @description -
  *
  * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
  *
  * @version 1.0.1
  *
  * @see {@link http://www.aditivointeractivegroup.com}
  *
  */

  var Utils = function() {

    //variables...

    var array  = ['brand', 'danger', 'success', 'warning', 'primary', 'info'];

    /**
    * getRandomState();
    * @description 									-						get a random state...
    * @return       								-						string
    */

    return {

      getDatatableButtonsObject : function(fileName, useCopyButton = true, useCSVButton = true, useExcelButton = true, usePDFButton = true, usePrintButton = true){

        var buttons = new Array();

        if(useCopyButton) buttons.push({extend: 'copyHtml5', title: fileName, text:'Copiar'});

        if(useCSVButton) buttons.push({extend: 'csvHtml5', title: fileName, text:'CSV'});

        if(useExcelButton) buttons.push({extend: 'excelHtml5', title: fileName, text:'Excel'});

        if(usePDFButton) buttons.push({extend: 'pdfHtml5', title: fileName, text:'Archivo PDF'});

        if(usePrintButton) buttons.push({text: 'Imprimir'});

        return buttons;

      },

      /**
      * getDatatableLanguageObject();
      * @description 									-						get an object with all the configuration for datatables...
      * @return       								-						object
      */

      getDatatableLanguageObject : function(){

        var object = {

          decimal:        "",

          emptyTable:     "No se encontro información",

          info:           "Mostrando _START_ de _END_ de _TOTAL_ entradas",

          infoEmpty:      "Mostrando 0 de 0 de 0 entradas",

          infoFiltered:   "(filtrado de _MAX_ entradas)",

          infoPostFix:    "",

          thousands:      ",",

          lengthMenu:     "Mostrando _MENU_ entradas",

          loadingRecords: "Cargando...",

          processing:     "Procesando...",

          search:         "Buscar:",

          zeroRecords:    "No se encontraron registros.",

          paginate: {

              first:      "Primero",

              last:       "Último",

              next:       "Siguiente",

              previous:   "Anterior"
			    },

          aria: {

              sortAscending:  ": activar para filtrar columnas de forma ascendente",

              sortDescending: ": activar para filtrar columnas de forma descendente"
			    }

        }

        return object;

      },

      getRandomState : function(){ return array[this.getRandomNumber(0, array.length -1)]; },

      /**
      * getStatesArray();
      * @description 									-						get all the states in an array
      * @param      									-						the max number...
      * @return       								-						array
      */

      getStatesArray : function(){ return array; },

      /**
      * getRandomNumber();
      * @description 									-						generate a random number between a min and a max value...
      * @param      									-						the min number...
      * @param      									-						the max number...
      * @return       								-						random number...
      */

      getRandomNumber: function(min, max){ return Math.floor(Math.random() * (max - min + 1)) + min; },

      /**
      * block();
      * @description 									-						block the stage...
      * @param      									-						options...color, text, etc...
      */

      block: function(options) {

          var el = $('body');

          var target = 'body';

          options = $.extend(true, { opacity: 0.1, overlayColor: '#000000', type: 'v2', size: '', state: 'success', centerX: true, centerY: true, message: '', shadow: true, width: 'auto' }, options);

          var html;

          var version = options.type ? 'kt-spinner--' + options.type : '';

          var state = options.state ? 'kt-spinner--' + options.state : '';

          var size = options.size ? 'kt-spinner--' + options.size : '';

          var spinner = '<div class="kt-spinner ' + version + ' ' + state + ' ' + size + '"></div';

          if (options.message && options.message.length > 0) {

              var classes = 'blockui ' + (options.shadow === false ? 'blockui' : '');

              html = '<div class="' + classes + '"><span>' + options.message + '</span><span>' + spinner + '</span></div>';

              var el = document.createElement('div');

              $('body').prepend(el);

              $('body').addClass(el, classes);

              el.innerHTML = '<span>' + options.message + '</span><span>' + spinner + '</span>';

              options.width = 200; //return KTUtil.actualCss(el, 'width', cache);

              $('body').remove(el);

              if (target == 'body') { html = '<div class="' + classes + '" style="margin-left:-' + (options.width / 2) + 'px;"><span>' + options.message + '</span><span>' + spinner + '</span></div>'; }

          } else {

              html = spinner;
          }

          var params = { message: html, centerY: options.centerY, centerX: options.centerX,

              css: { top: '30%', left: '50%', border: '0', padding: '0', backgroundColor: 'none', width: options.width },

              overlayCSS: { backgroundColor: options.overlayColor, opacity: options.opacity, cursor: 'wait', zIndex: (target == 'body' ? 1100 : 10) },

              onUnblock: function() {

                  if (el && el[0]) {

                      $('body').css(el[0], 'position', '');

                      $('body').css(el[0], 'zoom', '');

                  }
              }
          };

          if (target == 'body') {

              params.css.top = '50%';

              $.blockUI(params);

          } else {

              $(target).block(params);
          }
      },

      /**
      * unblock();
      * @description 									-						unblock the stage...
      */

      unblock: function() { $.unblockUI();

      },

      /**
      * toastr();
      * @description 									-						manage toastrs and all the stuff in relation with it...
      * @param title 	      					-						the title for the toastr...
      * @param message 	     					-						the message for the toastr...
      * @param options 	     					-						the object with editions...
      * @param type   	     					-						the type if toastr (success, warning, info, error)
      */

      toastr : function(title, message, options, type = 'success'){

        options = $.extend(true, { "closeButton": true, "debug": false, "newestOnTop": true, "progressBar": true, "positionClass": "toast-bottom-left", "preventDuplicates": true, "onclick": null,
          "showDuration": "300", "hideDuration": "1000", "timeOut": "5000", "extendedTimeOut": "1000", "showEasing": "swing", "hideEasing": "linear", "showMethod": "fadeIn", "hideMethod": "fadeOut"
        }, options);

        toastr.options = options;

        switch (type) {

          case 'success': toastr.success(message, title); break;

          case 'info': toastr.info(message, title); break;

          case 'warning': toastr.warning(message, title); break;

          case 'error':
          case 'danger':

          toastr.error(message, title); break;

        }

      },

      /**
      * alert();
      * @description 									-						manage alerts with styles, callbacks, etc...
      * @param title 	      					-						the title for the alert...
      * @param message 	     					-						the message for the alert...
      * @param buttonOptions 	     		-						array with the options for buttons. We use it to decide if we use two buttons or just one...
      * @param callbacks 	     		    -  					callback functions in the scope function....
      * @param type   	     					-						the type if alert (success, warning, info, error)
      * @param cancelable   	     		-						useful for important alerts :)
      */

      alert : function(title, message, buttonOptions = ['cerrar'], callbacks = [undefined, undefined] , type = 'success', cancelable = false, confirmButtonClass = "btn btn-sm btn-bold btn-danger", cancelButtonClass = "btn btn-sm btn-bold btn-brand"){

        var options;

        if(buttonOptions.length >1){

          options = {buttonsStyling: false, title: title, html: message, type: type, showCancelButton: true, confirmButtonText: buttonOptions[0], cancelButtonText: buttonOptions[1], reverseButtons: true, timerProgressBar: true, confirmButtonClass: confirmButtonClass, cancelButtonClass: cancelButtonClass}

        }else{

          options = {buttonsStyling: false, title: title, html: message, type: type, confirmButtonText: buttonOptions[0], reverseButtons: false, timerProgressBar: true, confirmButtonClass: confirmButtonClass, cancelButtonClass: cancelButtonClass}

        }

        options.confirmButtonColor = '#0abb87';

        if(cancelable) options.allowOutsideClick = options.allowEscapeKey = options.allowEnterKey = false;

        swal.fire(options).then(function(result){

          if (result.value) {

            console.log("ok value");

            if(callbacks[0] != undefined) callbacks[0]();

          }else{

            if(buttonOptions.length >1 && callbacks[1] != undefined){

              callbacks[1]();

            }

          }

      });

    },

    /**
    * checkFirstTime();
    * check if this is the user first time...
    */


    checkFirstTime : function(){

    },

    /**
    * securityTimeout();
    * create the session timeut timer and slider...
    */

    securityTimeout : function(){

      $.ajax({

        url: "private/framework/services/getTimeoutData.php",

        cache: false,

        success: function(response){

          var response = JSON.parse(response);

          if(response.use_timeout == 1){

            $.sessionTimeout({ title: response.title, message: response.message,

              countdownSmart: ((response.countdown_smart == 1) ? true : false),

              countdownBar: ((response.use_bar == 1) ? true : false),

              ignoreUserActivity: ((response.ignore_user_activity == 1) ? true : false),

            	keepAliveUrl: '',

            	logoutUrl: 'private/framework/users/logout.php',

            	redirUrl: 'private/framework/users/logout.php',

              logoutButton: response.logout_button,

              keepAliveButton: response.keep_alive_button,

            	warnAfter: parseInt(response.warn_after),

            	redirAfter: parseInt(response.redir_after)

            });

          }
      }

      });

    },

    /**
    * scrollToTop();
    * create the session timeut timer and slider...
    * @param delay          -           the delay to execute the scroll
    */

    scrollToTop : function(delay = undefined){

      (delay == undefined) ? window.scrollTo({top: 0, behavior: 'smooth'}) : setTimeout(function(){ window.scrollTo({top: 0, behavior: 'smooth'}); }, delay);

    },

    addStrenghtRule : function(globalScore){

      return function (value, element) { return (globalScore >= 4) ? true : false; };

    },

    /**
    * addNotEqualToRule();
    * rule for the validator. Useful for passwords.
    */

    addNotEqualToRule : function(){

      return function(value, element, param) {

          var notEqual = true;

          value = $.trim(value);

          for (var i = 0; i < param.length; i++) { if (value == $.trim($(param[i]).val())) { notEqual = false; } }

          return this.optional(element) || notEqual;

      }

    },

    /**
    * reloadPage();
    * reload de page...
    *
    * @param fromServer        -            reload from the server?
    * @return string           -            the value for the key...
    */

    reload : function(fromServer){

      window.location.reload(fromServer);

    },

    /**
    * navigateTo();
    * navigate to page...
    *
    * @param url               -            the URL...
    * @return allowGoBack      -            depending on the go back posibility, it defines the method...
    */

    navigateTo : function(url, allowGoBack = true){

      if(allowGoBack){

        window.location.href = url;

      }else{

        window.location.replace(url);

      }

    },

    /**
    * searchURLParam();
    * find get param in url...
    *
    * @param param        -     the name...
    * @return string      -     the value for the key...
    */

    searchURLParam : function(param){

      var urlParams = new URLSearchParams(window.location.search);

      return urlParams.get(param);

    },
  }
}
