/**
 * @summary Helpers!
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

 var Helper = function() {

    return {

        mobileCheck : function() {

            var check = false;

            (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);

            return check;

        },

        setMenu : function(){

            var menu = mLayout.getAsideMenu();

            var url = window.location.pathname;

            var filename = url.split('/').pop().split('#')[0].split('?')[0];

            console.log(url, filename);

            var item = $(menu).find("a[href='"+filename+"']").parent('.m-menu__item');

            var selector = $(menu).find("a[href='"+filename+"'] span");

            menu.setActiveItem(item);

            selector.css('color', '#ffffff');

        },

        /**
        * @function blockStage
        * @description block the stage using the blockUI library.
        * @param {string} msg       -       the msg for the blocker.
        */

        blockStage : function(msg){

            $.blockUI.defaults.baseZ = 4000;

            mApp.blockPage({overlayColor: '#000000', 

                type: 'loader', state: 'success', size: 'lg', baseZ: 2000,

                message: msg
            })

            $(".m-blockui").css("margin-left", "0");

        },

        /**
        * @function blockModal
        * @description block a modal using the blockUI library.
        * @param {div} modal       -         modal to block.
        * @param {string} msg      -         the msg while it is blocking.
        */

        blockModal: function(modal, msg){

            mApp.block(modal, {overlayColor: '#000000', 

                type: 'loader', state: 'success', size: 'lg', baseZ: 2000,

                message: msg
            })

        },

        /**
        * @function unblockModal
        * @description unblock the modal.
        * @param {div} modal       -         modal to unblock.
        */

        unblockModal: function(modal){

          mApp.unblock(modal);

      },

        /**
        * @function unblockStage
        * @description unblock the stage.
        */

        unblockStage: function(){ mApp.unblockPage(); },

        /**
        * @function createErrorLabel
        * @description Create an error label for fields.
        * @param {string} field      -       The string that has the errror label.
        * @param {string} type       -       A string to filter the type of error.
        * @param {array} params      -       Aditional params.
        */

        createErrorLabel: function(field, type, params){

            var string;

            switch(type){

                case 'REQUIRED':

                string = 'El campo <strong>' + field + '</strong> es obligatorio.';

                break;

                case 'TOO_SHORT':

                string = 'El campo <strong>' + field + '</strong> es demasiado corto.';

                break;

                case 'INVALID_URL':

                string = 'la URL de <strong>' + field + '</strong> es inválida.';

                break;

                case 'INDICATE':

                string = 'Debes indicar tu <strong>' + field + '</strong>.';

                break;

                case 'MIN_LENGTH':

                string = '<strong>' + field + '</strong> debe tener por lo menos ' + params[0];

                break;

                case 'STRENGHT':

                string = '<strong>' + field + '</strong> es insegura. Dale, tenés que llenar la barrita!)';

                break;

                case 'MATCH':

                string = 'El campo <strong>' + field + '</strong> debe ser igual al campo ' + params[0];

                break;

                case 'DIFFERENT':

                string = 'El campo <strong>' + field + '</strong> debe ser distinto al campo <strong>' + params[0] + "</strong>";

                break;

                case 'NUMERIC':

                string = 'El campo <strong>' + field + '</strong> debe ser un número';

                break;

                case 'EMAIL':

                string = 'El campo <strong>' + field + '</strong> debe ser un correo electrónico válido.';

                break;

                case 'AT_LEAST_ONE':

                string = 'Debes seleccionar al menos una opción para el campo <strong>' + field + '</strong>.';

                break;

                case 'YOUTUBE_VIDEO':

                string = 'El campo <strong>' + field + '</strong> debe ser un link de <strong>YouTube</strong>.';

                break;

                default:

                string = "";
            }

            return string;
        },

        /**
        * @function passwordScore
        * @description Retrn the strenght of a password.
        * @param {string} string           -       The password.
        * @return {int} result.score       -       The strenght of the password.
        */

        passwordScore:function(string){

            var result = zxcvbn(string);

            return result.score;
        },

        /**
        * @function getGETVariables
        * @description Return a get variable value.
        * @param    {string}    url              -       The url where we should check for the params.
        * @param    {string}    params           -       The param that we are looking for.
        * @return   {string}    value            -       The returned value.
        */

        getGETVariables:function(url, param){

            var url = new URL(url);

            var value = url.searchParams.get(param);

            return value;

        },

         /**
        * @function showToastr
        *
        * @description Create a toastr acording to params.
        * @param    {string}    title            -       The title of the toastr.
        * @param    {string}    msg              -       The msg.
        * @return   {string}    type             -       The type of toastr.
        * @return   {boolean}   closeButton      -       Show the close button?.
        * @return   {boolean}   progressBar      -       Show the progress bar?.
        * @return   {string}    position         -       The position of the toastr.
        */

        showToastr: function(title, msg, type="success", closeButton = false, progressBar = false, position = "toast-bottom-left" ){

            toastr.clear();

            toastr.options = {

                "maxOpened": 1,

                "autoDismiss": true,

                "closeButton": closeButton,

                "newestOnTop": false,

                "progressBar": progressBar,

                "positionClass": position,

                "preventDuplicates": true,

                "preventOpenDuplicates": true,

                "onclick": null,

                "showDuration": "300",

                "hideDuration": "1000",

                "timeOut": "5000",

                "extendedTimeOut": "1000",

                "showEasing": "swing",

                "hideEasing": "linear",

                "showMethod": "fadeIn",

                "hideMethod": "fadeOut"
            };

            if(type=="success"){

                toastr.success(msg, title);

            }else if(type=="error"){

                toastr.error(msg, title);

            }
        },

        /**
        * @function getDataTableLanguageConfig
        *
        * @description We creae an object for language for datatables so we call this method.
        * we use this because we have datatablas everywhere.
        */

        getDataTableLanguageConfig: function(){

            var object = {

                "decimal":        "",

                "emptyTable":     "No se encontraron registros.",

                "info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",

                "infoEmpty":      "Mistrando 0 a 0 de 0 entradas",

                "infoFiltered":   "(Filtrado de  _MAX_ entradas)",

                "infoPostFix":    "",

                "thousands":      ",",

                "lengthMenu":     "Mostrando _MENU_ entradas",

                "loadingRecords": "Cargando...",

                "processing":     "Procesando...",

                "search":         "Buscar:",

                "zeroRecords":    "No se encontraron registros.",

                "paginate": {

                  "first":      "Primero",

                  "last":       "Última",

                  "next":       "Siguiente",

                  "previous":   "Anterior"
              },

              "aria": {

                  "sortAscending":  ": activate to sort column ascending",

                  "sortDescending": ": activate to sort column descending"

              }

          }

          return object;

      },

        /**
        * @function replaceUrlParam
        *
        * @description Replace a param inside the URL.
        * @param    {string}    url            -       The url o find the param.
        * @param    {string}    paramName      -       The param that we are looking for.
        * @param    {string}    paramValue     -       The new param.
        */

        replaceUrlParam:function(url, paramName, paramValue){

            if (paramValue == null) { paramValue = ''; }

            var pattern = new RegExp('\\b('+paramName+'=).*?(&|#|$)');

            if (url.search(pattern)>=0) {

                return url.replace(pattern,'$1' + paramValue + '$2');

            }

            url = url.replace(/[?#]$/,'');

            return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue;

        },

        addyoutubeValidationMethod:function(container){

            container.addMethod("youtube", function(value, element) {

            var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;

            return (value.match(p)) ? RegExp.$1 : false;

        }, "Opss!");

        },

        getYoutubeVideoId:function(url){

            var regExp = /.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;

            var match = url.match(regExp);

            return (match&&match[1].length==11)? match[1] : false;
        }

    }

}