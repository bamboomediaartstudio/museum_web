/**
 * @summary Add new alert.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */


/**
 * @function MuseumAlertAdd
 * @description Initialize and include all the methods of this class.
 */

 var MuseumAlertAdd = function() {

    helper = Helper();

    var editMode;

    var alertId;

    var alertTitle;

    var form = $('#add-data-form');
    
    
    /**
     * @function showErrorSwal
     * @param msg - the msg.
     * @description Swal alert.
     */

     var showErrorSwal = function(msg){

        Swal({

            title: 'Opss!.',

            html: msg,

            type: 'error',

            confirmButtonText: 'Entendido!'

        })
    }

    
    /**
     * @function addFormValidations
     * @description Asign all the form validations for this page.
     */

     var addFormValidations = function() {

        jQuery.validator.setDefaults({

            ignore: ":hidden, [contenteditable='true']:not([name])"

        });

        helper.addyoutubeValidationMethod($.validator);

        form.validate({

            rules: {

                title: {
                    required: true
                },

                
                link: {

                    url: true
                }


            },
            messages: {

                title: helper.createErrorLabel('título', 'REQUIRED'),

                link: helper.createErrorLabel('link', 'INVALID_URL')
            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();

                
                window.scroll({top: 0, left: 0, behavior: 'smooth' });
            },

            submitHandler: function(form, event) {

                event.preventDefault();

                event.stopPropagation();

                helper.blockStage("actualizando datos...");

                var formData = new FormData(form);

                formData.append("id", alertId);

                formData.append("alertTitle", alertTitle);

                formData.append("source", "alerts");

                formData.append("caption", $('#caption').summernote('code'));

                //formData.append("url", $('#url').val());

                var request = $.ajax({

                    url: "private/users/museum/alerts/alert_add.php",

                    type: "POST",

                    contentType: false,
                    
                    processData: false,

                    data: formData,

                    dataType: "json"
                });

                request.done(function(response) {

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch (Number(response.status)) {

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {

                            switch (Number(response.status)) {

                                case 1:

                                window.location.replace("museum_alerts_list.php");

                                break;

                            }

                        }

                    });


                    helper.showToastr("Se acualizo", 'Se actualizo');

                    helper.unblockStage();

                });

                request.fail(function(jqXHR, textStatus) {

                    console.log(jqXHR);

                    console.log("error");

                    helper.showToastr("ops", 'error');

                    helper.unblockStage();

                });

            }

        });

    }

    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

     var addListeners = function() {

        $('input[type=radio][name=radio_group]').change(function() {

            if(editMode)  updateValue(alertId, 'alert_type', this.value, 'museum_alerts');

            //console.log(this.value);
           
        });

        $('.toggler-info:checkbox').change(function() {

            if (!editMode) return;

            var fieldName = $(this).attr('name');

            var newValue = ($(this).is(":checked")) ? 1 : 0;

            updateValue(alertId, fieldName, newValue, 'museum_alerts');

        });

        $('#caption').summernote({

            disableDragAndDrop: true,

            height: 200,

            toolbar: [

            ['style', ['bold', 'italic', 'underline']],

            ['Misc', ['fullscreen', 'undo', 'redo']]

            ],

            callbacks: {

                onInit: function() { },

                onEnter: function() { },

                onKeyup: function(e) { },

                onKeydown: function(e) { },

                onPaste: function(e) { },

                onChange: function(contents, $editable) { },

                onBlur: function(){ },

                onFocus: function() { }

            }

        });

        $('[data-toggle="tooltip"]').tooltip();

        $('.show_address').change(function() {

            var checked = ($(this).prop('checked'));

            if (checked == true) {

                $('.museum_map_block, .museum_map_address_block, #map').removeClass('disabled');

            } else {

                $('.museum_map_block, .museum_map_address_block, #map').addClass('disabled');
            }

        })

        /*autosize for the textareas */

        autosize(document.querySelector('textarea'));

        $('#exit_from_form').click(function(e) {  window.location.replace("index.php"); });

        $('#back_to_list').click(function(e) { window.location.replace("museum_alerts_list.php"); });

        $(".update-btn").click(function() {

            if (!editMode) return;

            var dbName = $(this).attr('data-db-value');

            var id = $(this).attr('data-id');

            var newValue = $(this).parent().parent().find(':input').val();

            var fieldName = $(this).parent().parent().find(':input').attr('name');

            var valid = $('#' + fieldName).valid();

            if (dbName != newValue && valid) {

                $(this).attr('data-db-value', newValue);

                updateValue(id, fieldName, newValue, 'museum_alerts');

            }

        });

    }

    /**
     * @function window.onbeforeunload
     * @description scroll to top
     */

     window.onbeforeunload = function () { if(!editMode) window.scrollTo(0,0); }

    /**
     * @function updateValue
     * @description update individual value in DB.
     *
     * @param {int} id                 - object id.
     * @param {string} fieldName       - the field to modify
     * @param {string} newValue        - object value.
     * @param {int} filter             - filter type.
     */

     var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/update_value.php",

            type: "POST",

            data: {

                id: id,
                
                fieldName: fieldName,
                
                newValue: newValue,
                
                filter: filter
            },

            dataType: "json"

        });

        request.done(function(result) {

            console.log(result);

            helper.showToastr(result.title, result.msg);

            if (callback != undefined) callback(callbackParams);

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });

        if (callback == undefined) helper.unblockStage();

        localStorage.setItem('refresh', 'true');

    }


    return {

        init: function() {

            editMode = ($('#edit-mode').val() == 1) ? true : false;

            if (editMode) {

                alertId = $('#alert-id').val();

                alertTitle = $('#alert-title').val();

            }

            addListeners();

            addFormValidations();
        }

    };

}();

jQuery(document).ready(function() { MuseumAlertAdd.init(); });