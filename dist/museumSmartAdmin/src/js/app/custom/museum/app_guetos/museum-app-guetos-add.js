/**
 * @summary Add new Gueto to the APP.
 *
 * @description - APP Guetos - Add Guetos
 *
 * @author Colo baggins <colorado@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */


/**
 * @function MuseumAppGuetosAdd
 * @description Initialize and include all the methods of this class.
 */

 var MuseumAppGuetosAdd = function() {

    helper = Helper();

    var editMode;

    var redirectId;

    var guetoId;

    var guetoName;

    var addRedirectId = 0;

    var defaultImageWidth = 1000;

    var defaultImageHeight = 1000;

    var isFirstLoad = true;

    var map;

    var myLatlng;

    var guetoLat;

    var guetoLng;

    var guetoStartDate = $("#year_start");

    //var geocoder;

    var markers = [];

    var myElement;

    var form = $('#add-guetos-data-form');

    //cover config...
/*
    initialPreviewCoverConfig = [];

    initialPreviewCoverPaths = [];*/

    initialPreviewGalleryConfig = [];

    initialPreviewGalleryPaths = [];

    /**
     * @function addFormValidations
     * @description Asign all the form validations for this page.
     */

     var addFormValidations = function() {

        jQuery.validator.setDefaults({

            ignore: ":hidden, [contenteditable='true']:not([name])"

        });


        form.validate({

            rules: {

                name: {
                    required: true
                },

                country: {
                    required: true
                },

                year_start: {
                    required: true
                },

                year_end: {
                    required: true
                },

                guetoLat: {
                    required: true
                },

                guetoLng: {
                    required: true
                },

                'checkboxes[]': {
                    required: !0
                }

            },
            messages: {

                name: helper.createErrorLabel('Nombre', 'REQUIRED'),

                country: helper.createErrorLabel('Pais', 'REQUIRED'),

                year_start: helper.createErrorLabel('Año de inicio', 'REQUIRED'),

                year_end: helper.createErrorLabel('Año de fin', 'REQUIRED'),

                guetoLat: helper.createErrorLabel('Latitud', 'REQUIRED'),

                guetoLng: helper.createErrorLabel('Longitud', 'REQUIRED'),

                'checkboxes[]': helper.createErrorLabel('categoría', 'AT_LEAST_ONE')

            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();



                window.scroll({top: 0, left: 0, behavior: 'smooth' });
            },

            submitHandler: function(form, event) {

                event.preventDefault();

                event.stopPropagation();

                helper.blockStage("actualizando datos...");

                var formData = new FormData(form);


                var request = $.ajax({

                    url: "private/users/museum/apps/guetos/guetos_add.php",

                    type: "POST",

                    contentType: false,

                    processData: false,

                    data: formData,

                    dataType: "json"
                });

                request.done(function(response) {

                  console.log("response antes de swal y toastr");

                  console.log(response);

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch (Number(response.status)) {

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {

                            switch (Number(response.status)) {

                                case 1:

                                window.location.replace("museum_app_guetos_list.php");

                                break;

                            }

                        }

                    });


                    helper.showToastr("Se acualizo", 'Se actualizo');

                    helper.unblockStage();

                });

                request.fail(function(jqXHR, textStatus) {

                    console.log(jqXHR);

                    console.log("error");

                    helper.showToastr("ops", 'error');

                    helper.unblockStage();

                });

            }

        });
      }


    /**
     * @function createGallery
     * @description Create the gallery.
     */

     var createGallery = function() {

       console.log("Create gallery!!");

        var btns = '<button data-origin="image" type="button" class="kv-cust-btn btn btn-sm btn-kv btn-default btn-outline-secondary" title="Editar"{dataKey}>' +
        '<i class="fas fa-pen"></i>' + '</button>';

        $("#images-batch").fileinput({

            otherActionButtons: btns,

            initialPreviewAsData: true,

            initialPreview: initialPreviewGalleryPaths,

            initialPreviewConfig: initialPreviewGalleryConfig,

            theme: "fas",

            uploadUrl: "private/users/museum/general/update_images_batch_new.php",

            deleteUrl: "private/users/museum/apps/guetos/guetos_delete.php",

            showCaption: true,

            showPreview: true,

            showRemove: false,

            showUpload: true,

            showCancel: true,

            showClose: false,

            browseOnZoneClick: true,

            previewFileType: 'any',

            language: "es",

            maxFileSize: 0,

            allowedFileTypes: ["image"],

            overwriteInitial: false,

            allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],

            maxFilePreviewSize: 10240,

            uploadExtraData: function() {

                return {

                    id: guetoId,

                    source: 'guetos',

                    galleryFolder: 'guetos'

                };

            }

        }).on('filesorted', function(e, params) {

            console.log("Rearange");
            rearangeStack('app_museum_guetos_images', JSON.stringify(params.stack));


        }).on('fileuploaded', function(e, params) {


        }).on('filebatchuploadcomplete', function(event, files, extra) {

            location.reload();

        }).on("filepredelete", function(jqXHR) {


        }).on('filedeleted', function(event, key, jqXHR, data) {

            return {

                source: 'guetos',

                galleryFolder: 'guetos'

            };

            console.log("data: "+data);
            console.log(data);

            helper.showToastr("Imagen eliminada", 'Se eliminó la imagen del gueto ' + guetoName);

        }).on('fileuploaderror', function(event, data, msg) {

            var form = data.form, files = data.files, extra = data.extra,

            response = data.response, reader = data.reader;

            console.log(event, data, msg);

            //showErrorSwal(msg);

        }).on('filebeforedelete', function() {

            return new Promise(function(resolve, reject) {

                swal({

                    title: "¿Eliminar imagen?",

                    allowOutsideClick: false,

                    html: "La misma no aparecerá más listada en esta muestra.",

                    type: "warning",

                    showCancelButton: true,

                    confirmButtonText: 'Si, borrar',

                    cancelButtonText: 'no, salir'

                }).then((result) => {

                    if (result.value) {

                        resolve();

                    }else{

                        reject();
                    }

                })

            });
        });

        $('#myModal').on('shown.bs.modal', function () {

            $('#image-description').trigger('focus');

        });

        $('.kv-cust-btn').on('click', function() {

            origin = $(this).attr('data-origin');

            var $btn = $(this);

            var key = $btn.data('key');

            openKey = key;

            var item = initialPreviewGalleryConfig.find(item => item.key == key);

            $('#image-description').val(item.description);

            $('#myModal').modal('show');

        });

    }// Ends create Gallery

    /**
     * @function rearangeStack
     * @param db - db to rearange by order.
     * @param stack - the items to rearange.
     * @description Modify items order.
     */

     var rearangeStack = function(db, stack){

       console.log("rearange functin, stack: ");
       console.log(stack);

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/rearange_images_sort_new.php",

            type: "POST",

            data: {

                source: "guetos",

                stack: stack,

                db: db

            },

            dataType: "json",

            success: function(result) {

                helper.showToastr(result.title, result.body);

                helper.unblockStage();

            },

            error: function(request, error) {

                console.log(request);

                helper.unblockStage();
            }
        });

    }

    /**
     * @function loadAjaxImages
     * @param loadSource - evaluate this param to get one batch or a single one.
     * @description if we are in edition mode load all the images for Bootstrap File Input.
     */

     //TODO:: terminar de cargar imgs al editar

     var loadAjaxImages = function(from, field, loadSource, config, myPreviews) {

       console.log("Buscar imagenes para este gueto!!");

        var request = $.ajax({

            url: "private/users/services/get_list_of_images_new_gallery.php",

            type: "POST",

            data: {

                id: guetoId,

                field: field,

                source: loadSource,

                from: from
            },

            dataType: "json"
        });

        request.done(function(result) {

            for (var i = 0; i < result.length; i++) {

                var internalOrder = result[i].internal_order;

                var id = result[i].id;

                var source = result[i].source;

                var description = result[i].description;

                var imgName = guetoId + '_' + result[i].unique_id + '_original.jpeg';

                var path;

                path = 'private/sources/images/guetos/' + guetoId + '/images/' + result[i].unique_id + '/' + imgName;

                console.log("gueto path: ");
                console.log(path);

                myPreviews.push(path);

                config.push({

                    'key': id,

                    'internal_order': internalOrder,

                    'source': source,

                    'description' : description,

                    'caption' : (description != null) ? description : '',

                    //'url' : 'private/users/museum/general/delete_image.php'

                });

                console.log("configObj: ");
                console.log(config);

            }// /.ends for

            createGallery();




        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

        });

    }

    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

     var addListeners = function() {

       $('[data-toggle="tooltip"]').tooltip();

       //Predictive Country
       //Country origin predictive
       t = new Bloodhound({

           datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

           queryTokenizer: Bloodhound.tokenizers.whitespace,

           prefetch:{

               url : "private/users/services/get_list_of_countries.php",

               cache: false
           }

       })

       var context = $("#country-list .typeahead").typeahead(null, {

           hint: false,

           highlight: false,

           minLength: 1,

           name: "best-pictures",

           display: "value",

           source: t,

           autoselect: true,

           templates: {

               suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")

           }

       }).on("typeahead:render", function() {

           var searchTerm = $(this).val();

           $('#country-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

       }).on('typeahead:selected', function (e, datum) {

           $('#hidden-country-id').val(datum.id);
           console.log("Para que sirve este id?: "+datum.id);

       }).on('keyup', this, function (event) {


       }).on('keydown', this, function (event) {

       }).on('focus', this, function(event){

           if(editMode) $('#hidden-country-id').val("");

       });

       //Gueto Year Start Datepicker
       $('#year_start').datepicker({

           autoclose: true,

           format: 'yyyy',

           viewMode: 'years',

           minViewMode: 'years',

           defaultViewDate: {year: '1930'},

       }).on('changeDate', function(selected){
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));

             //Set year end - this year as start date

             $('#year_end').datepicker('setStartDate', startDate);

             //Enable year end prop if we are not in edit mode
             if(!editMode){
                $('#year_end').prop('disabled', false);
             }


        });


       //Gueto Year End Datepicker
       $('#year_end').datepicker({

           autoclose: true,

           format: 'yyyy',

           viewMode: 'years',

           minViewMode: 'years',

           defaultViewDate: {year: '1930'}

       });


       $('.tab-navigation').click(function(){

           setQueryStringParameter('tab', $(this).attr('href').substring(1));

       })

       //Description
       myElement = $('#description').summernote({

           disableDragAndDrop: true,

           fontNames : ['CooperHewitt-Medium', 'Arial', 'sans-serif'],

           height: 200,

           toolbar: [

           ['style', ['bold', 'italic', 'underline']],

           ['Misc', ['fullscreen', 'undo', 'redo']]

           ],

           callbacks: {

               onInit: function() { },

               onEnter: function() { },

               onKeyup: function(e) {

                   if($(this).attr('name') == 'description'){

                       summernoteDescriptionCounter = e.currentTarget.innerText;

                       if(summernoteDescriptionCounter !=0){

                           $('.summernote-description-error').addClass('d-none');

                       }else{

                           $('.summernote-description-error').removeClass('d-none');

                       }

                   }

               },

               onKeydown: function(e) { },

               onPaste: function(e) { },

               onChange: function(contents, $editable) { },

               onBlur: function(){ },

               onFocus: function() { }

           }

       });


       //Image description

       $('.save-description').click(function(event){

           console.log("origin: " + origin);

           var description = $('#image-description').val();

           $('#myModal').modal('hide');

           var findArray;

           var fileInput;

           var table;

           if(origin == 'image'){

               findArray = initialPreviewGalleryConfig;

               fileInput = $("#images-batch");

               table = 'app_museum_guetos_images';

           }

           var item = findArray.find(item => item.key == openKey);

           item.caption = item.description = description;

           fileInput.fileinput('destroy');

           createGallery();

           console.log("***** key: "+openKey+" *****");

           //openKey : id gueto

           updateValue(openKey, 'description', description, table);

       });


       //Lat & lng
       $('.search-gueto-map').click(function(){
        console.log("buscar coordenadas en mapa: ");

        guetoLat = $('#guetoLat').val();
        guetoLng = $('#guetoLng').val();

        console.log("Lat: "+guetoLat);
        console.log("Lng: "+guetoLng);

        newLocation(guetoLat, guetoLng);

       });

       $('#exit_from_form').click(function(e) {
           window.location.replace("index.php");
       });

       $('#back_to_list').click(function(e) {
           window.location.replace("museum_app_guetos_list.php?tabId=" + redirectId);
       });

       $(".update-btn").click(function() {
         console.log("UPDATE FIELD");

           if (!editMode) return;

           var dbName = $(this).attr('data-db-value');

           var id = $(this).attr('data-id');

           console.log("Id: "+id);

           var newValue = $(this).parent().parent().find(':input').val();

           var fieldName = $(this).parent().parent().find(':input').attr('name');

           console.log("dbName: "+dbName+ " - (Sería el valor viejo a modificar)");
           console.log("id: "+id);
           console.log("newValue: "+newValue);
           console.log("fieldName: "+fieldName);

           //Check for changes in latLng (1 update btn for 2 fields)

           if(dbName== "latLngFlag"){
            console.log("Se quiere actualizar lat y lng");

            var validLat = $('#guetoLat').valid();

            var validLng = $('#guetoLng').valid();


            //Update solo para lat&lng, validación diferente por ubicación de inputs
            if(validLat && validLng){

                //Update Lat
                updateValue(id, 'lat', $('#guetoLat').val(), 'app_museum_guetos');

                //Update Lng
                updateValue(id, 'lng',  $('#guetoLng').val(), 'app_museum_guetos');


                //reubicar en mapa
                newLocation($('#guetoLat').val(), $('#guetoLng').val());
            }

            //Update solo para Country, validación diferente por ubicación de inputs
          }else if(dbName == "countryFlag"){


              console.log("Actualizar country: "+$('#hidden-country-id').val());
                updateValue(id, 'id_country', $('#hidden-country-id').val(), 'app_museum_guetos');

          }else{

            //Update Resto de campos

            var valid = $('#' + fieldName).valid();

            if (dbName != newValue && valid) {

                console.log("changing something! Go to Update");

                $(this).attr('data-db-value', newValue);

                updateValue(id, fieldName, newValue, 'app_museum_guetos');

            }else{
              console.log("nothing changed, nothing happens");
            }

          }

       });


       $('.toggler-info:checkbox').change(function() {

           if (!editMode) return;

           var fieldName = $(this).attr('name');

           var newValue = ($(this).is(":checked")) ? 1 : 0;

           updateValue(guetoId, fieldName, newValue, 'app_museum_guetos');

       });

     }// /.addListeners


     /**
      * @function updateValue
      * @description update individual value in DB.
      *
      * @param {int} id                 - object id.
      * @param {string} fieldName       - the field to modify
      * @param {string} newValue        - object value.
      * @param {int} filter             - filter type.
      */

      var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

         console.log("Update - FieldName: "+fieldName + " - newValue: "+newValue + " - table: "+filter);

         console.log("update value...only once?");

         helper.blockStage("actualizando...");

         var request = $.ajax({

             url: "private/users/museum/general/update_value.php",

             type: "POST",

             data: {

                 id: id,

                 fieldName: fieldName,

                 newValue: newValue,

                 source: 'guetos',

                 filter: filter
             },

             dataType: "json"

         });

         request.done(function(result) {

             console.log("tracing result!");

             console.log(result);

             helper.showToastr(result.title, result.msg);

             if (callback != undefined) callback(callbackParams);

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

             console.log("error");

         });

         if (callback == undefined) helper.unblockStage();

         localStorage.setItem('refresh', 'true');

     }// /.updateValue



     /**
      * @function initMap
      * @description Initialize the Google Map and create the marker.
      */

      window.initMap = function() {
        console.log("inicializar mapa");

        if(!editMode){

          myLatlng = new google.maps.LatLng(-34.562386, -58.447446);

          map = new google.maps.Map(document.getElementById('map'), {

              center: myLatlng,

              zoom: 15,

              mapTypeControl: false,

              fullscreenControl: false,

              zoomControl: false

          });

        }else{

          newLocation(guetoLat, guetoLng);

        }


     }// Ends initMap



     /**
      * @function newLocation
      *
      * @param lat - latitud
      * @param lng - longitud
      *
      * @description new location of gueto with lat & lng
      */
     var newLocation = function(lat, lng){

       myLatLng = new google.maps.LatLng(lat, lng)

       map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });

     }


     /**
      * @function deleteMarkers
      * @description Delete all the markers from the map....
      */

      var deleteMarkers = function() {

         for (var i = 0; i < markers.length; i++) markers[i].setMap(null);

             markers = [];

     }

     function setQueryStringParameter(name, value) {

         const params = new URLSearchParams(location.search);

         params.set(name, value);

         window.history.replaceState({}, "", decodeURIComponent(`${location.pathname}?${params}`));

     }

    return {

        init: function() {

            editMode = ($('#edit-mode').val() == 1) ? true : false;

            helper.setMenu();

            if (editMode) {

              console.log("Estamos en edit mode!");
              guetoLat = $('#guetoLat').val();
              guetoLng = $('#guetoLng').val();

//              newLocation(guetoLat, guetoLng);

                var temp;

                if (document.referrer != "") {

                    temp = helper.getGETVariables(document.referrer, 'tabId');

                    console.log("temP: "+temp);


                } else {

                    temp = null;
                }

                redirectId = (temp == null) ? 1 : temp;

                guetoId = $('#gueto-id').val();

            } else {

                isFirstLoad = false;


            }

            addListeners();

            addFormValidations();

            if(editMode){

//              createGallery();

              //Enable year end prop if we are not in edit mode
              $('#year_end').prop('disabled', false);

              loadAjaxImages('app_museum_guetos_images','id_gueto', 'guetos', initialPreviewGalleryConfig, initialPreviewGalleryPaths)

            }else{

                createGallery();
                helper.setMenu();

            }// /.editMode if else



        } // /.init function

    }; // /.return

 }();

 jQuery(document).ready(function() { MuseumAppGuetosAdd.init(); });
