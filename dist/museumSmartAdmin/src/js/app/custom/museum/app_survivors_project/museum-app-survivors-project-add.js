/**
 * @sumy Survivors project
 *
 * @description - APP Survivors Project - Add Data
 *
 * @author Colo baggins <colorado@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */


/**
 * @function MuseumAppSurvivorsProjectAdd
 * @description Initialize and include all the methods of this class.
 */

 var MuseumAppSurvivorsProjectAdd = function() {

    helper = Helper();

    var sendYear = '0000';


    var sendMonth = '00';

    var sendDay = '00';

    var isFromYear = true;

    var editMode;

    var redirectId;

    var survivorId;

    var survivorName;

    var addRedirectId = 0;

    var defaultImageWidth = 1000;

    var defaultImageHeight = 1000;

    var isFirstLoad = true;

    var myElement;

    var saveYear;

    var form = $('#add-survivor-project-data-form');

    //cover config...

    initialPreviewCoverConfig = [];

    initialPreviewCoverPaths = [];

    //gallery config...

    initialPreviewGalleryConfig = [];

    initialPreviewgalleryPaths = [];

    var letT;

    /**
     * @function addFormValidations
     * @description Asign all the form validations for this page.
     */

     var addFormValidations = function() {

        jQuery.validator.setDefaults({ ignore: ":hidden, [contenteditable='true']:not([name])" });

        form.validate({

            rules: {

                name: { required: true },

                surname: { required: true },

                'checkboxes[]': { required: !0 }

            },

            messages: {

                name: helper.createErrorLabel('Nombre', 'REQUIRED'),

                surname: helper.createErrorLabel('Apellido', 'REQUIRED'),

                'checkboxes[]': helper.createErrorLabel('categorÃ­a', 'AT_LEAST_ONE')

            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();

                window.scroll({top: 0, left: 0, behavior: 'smooth' });
            },

            submitHandler: function(form, event) {

                event.preventDefault();

                event.stopPropagation();

                helper.blockStage("actualizando datos...");

                var formData = new FormData(form);

                formData.append("year", $('#birthday_year').val());
                
                formData.append("month", $('#birthday_month').val());
                
                formData.append("day", $('#birthday_day').val());

                formData.append("countryId", $('#hidden-country-id').val());

                formData.append("cityId", $('#hidden-city-id').val());
                
                formData.append("atThatMomentId", $('#hidden-at-that-moment-id').val());

                formData.append("atTheBeginningId", $('#hidden-at-the-beginning-id').val());

                formData.append("liberationId", $('#hidden-liberation-place-id').val());

                var alternativeNamesVector = new Array();

                $(".alternative-name-repeater").each(function() {

                    var getName = $(this).find(".new_alternative_name").val();

                    var getSurname = $(this).find(".new_alternative_surname").val();

                    if (getName !== "" || getSurname !== "") {

                        var object = {};

                        object.name = $(this).find(".new_alternative_name").val();

                        object.surname = $(this).find(".new_alternative_surname").val();

                        alternativeNamesVector.push(object);

                    }

                });

                formData.append("alternartiveNames", JSON.stringify(alternativeNamesVector));

                console.log("important:");

                console.log(alternativeNamesVector);


                /***********************************************************/
                // ###### Add Books ######

                var newBooksVector = new Array();

                //New Books to add
                $(".book-name-repeater").each(function() {

                    var getName = $(this).find(".new_book_name").val();


                    if (getName !== "" ) {

                        var object = {};

                        object.name = $(this).find(".new_book_name").val();

                        newBooksVector.push(object);

                    }

                });

                formData.append("booksList", JSON.stringify(newBooksVector));

                //Books selected from dropbox
                formData.append('selectedBooks', JSON.stringify($('.book-selector').select2('data')));
                
                console.log("important:");

                console.log(newBooksVector);

                // ###### /.books ######
                /***********************************************************/



                /***********************************************************/
                // ###### Add Films ######

                var newFilmsVector = new Array();

                //New films to add
                $(".film-repeater-item").each(function() {

                    var getName = $(this).find(".new_film_name").val();

                    console.log(`filmName: ${getName}`);

                    if (getName !== "" ) {

                        var object = {};

                        object.name = $(this).find(".new_film_name").val();

                        newFilmsVector.push(object);

                    }

                });

                formData.append("filmsList", JSON.stringify(newFilmsVector));

                //Films selected from dropbox
                formData.append('selectedFilms', JSON.stringify($('.film-selector').select2('data')));

                console.log("Films... :");

                console.log(newFilmsVector);


                // ###### /.Films ######
                /***********************************************************/



                /***********************************************************/
                // ###### Add Honors ######

                var newHonorsVector = new Array();

                //New honors to add
                $(".honor-repeater-item").each(function() {

                    var getName = $(this).find(".new_honor_name").val();

                    console.log(`honorName: ${getName}`);

                    if (getName !== "" ) {

                        var object = {};

                        object.name = $(this).find(".new_honor_name").val();

                        newHonorsVector.push(object);

                    }

                });

                formData.append("honorList", JSON.stringify(newHonorsVector));

                formData.append('selectedHonors', JSON.stringify($('.honor-selector').select2('data')));

                // ###### /.Honors ######
                /***********************************************************/


                /***********************************************************/
                // ###### Add Rescuers ######

                var newRescuersVector = new Array();

                //New rescuers to add
                $(".rescuer-repeater-item").each(function() {

                    var getName = $(this).find(".new_rescuer_name").val();
                    var getSurname = $(this).find(".new_rescuer_surname").val();

                    console.log(`rescuerName: ${getName}`);
                    console.log(`rescuerSURName: ${getSurname}`);

                    if (getName !== "" || getSurname !== "") {

                        var object = {};

                        object.name = getName;
                        object.surname = getSurname;

                        newRescuersVector.push(object);

                    }

                });

                formData.append("rescuerList", JSON.stringify(newRescuersVector));

                formData.append('selectedRescuers', JSON.stringify($('.rescuer-selector').select2('data')));

                // ###### /.Rescuers ######
                /***********************************************************/


                //FAMILIARES
                
                //******
                //Guardar id y id relation de conyugue, hijos, nietos.  (Select multiple) en selectedFamilyVector.
                //Cuando se seleccionan desde el dropdown solo necesito saber el id. Luego aca le ponemos que relacion familiar tiene, dependiendo de que select hablamos.
                //Si es el de conyugue, es el id 1, si es de hijo el id 2, etc.

                var selectedFamilyVector = new Array(); //Guardamos en este array los familiares seleccionados de los selects.

                var familyVector = new Array(); // Guardamos nuevos integrantes (nuevos registros para la db) (conyuges, hijos, nietos).


                /***********************************************************/
                // ###### Add Spouses (conyugues) ######

                var spouseRelationType = 1;

                $(".spouse-repeater-item").each(function(){

                    var getName = $(this).find('.new_spouse_name').val();
                    
                    var getSurname = $(this).find('.new_spouse_surname').val();


                    if(getName !== "" || getSurname !== ""){

                        var object = {};

                        object.name = $(this).find('.new_spouse_name').val();

                        object.surname = $(this).find('.new_spouse_surname').val();

                        object.relation_type = spouseRelationType;
                        
                        familyVector.push(object);

                    }

                });

                //Select multiple from spouses

                $(".spouse-selector option:selected").each(function(index, item){

                    var object = {};

                    object.id = $(item).val();

                    object.relation_type = spouseRelationType;

                    selectedFamilyVector.push(object);

               });

                // ###### /.Spouses ######
                /***********************************************************/



                /***********************************************************/
                // ###### Add Sons  ######

                var sonRelationType = 2;

                $('.son-repeater-item').each(function(){

                    var getName = $(this).find('.new_son_name').val();
                    
                    var getSurname = $(this).find('.new_son_surname').val();

                    if(getName !== "" || getSurname !== ""){

                        var object = {};

                        object.name = getName;

                        object.surname = getSurname;

                        object.relation_type = sonRelationType;

                        familyVector.push(object);

                    }

                });

                //Select multiple from Son

                $(".son-selector option:selected").each(function(index, item){

                    var object = {};

                    object.id = $(item).val();

                    object.relation_type = sonRelationType;

                    selectedFamilyVector.push(object);
               });
                

                // ###### /.Sons ######
                /***********************************************************/



                /***********************************************************/
                // ###### Add Grandson ######

                var grandSonRelationType = 3;

                $('.grandson-repeater-item').each(function(){

                    var getName = $(this).find('.new_grandson_name').val();
                    
                    var getSurname = $(this).find('.new_grandson_surname').val();

                    if(getName !== "" || getSurname !== ""){

                        var object = {};

                        object.name = getName;

                        object.surname = getSurname;

                        object.relation_type = grandSonRelationType;

                        familyVector.push(object);

                    }

                });


                //Select multiple from Grandson

                $(".grandson-selector option:selected").each(function(index, item){

                    var object = {};

                    object.id = $(item).val();
                    object.relation_type = grandSonRelationType;

                    selectedFamilyVector.push(object);
               });

                // ###### /.Grandsons ######
                /***********************************************************/



                //Send family data all together

                formData.append('familyList', JSON.stringify(familyVector));

                formData.append('selectedFamily', JSON.stringify(selectedFamilyVector));

                

                /***********************************************************/
                // ###### Add Guetos ######

                var newGuetosVector = new Array();

                //New honors to add
                $(".gueto-repeater-item").each(function() {

                    var getName = $(this).find(".new_gueto_name").val();

                    console.log(`guetoName: ${getName}`);

                    if (getName !== "" ) {

                        var object = {};

                        object.name = getName;

                        newGuetosVector.push(object);

                    }

                });

                formData.append("guetoList", JSON.stringify(newGuetosVector));

                formData.append('selectedGuetos', JSON.stringify($('.gueto-selector').select2('data')));

                // ###### /.Guetos ######
                /***********************************************************/



                /***********************************************************/
                // ###### Add Campos ######

                var newCamposVector = new Array();

                //New campo to add
                $(".campo-repeater-item").each(function() {

                    var getName = $(this).find(".new_campo_name").val();

                    console.log(`campoName: ${getName}`);

                    if (getName !== "" ) {

                        var object = {};

                        object.name = getName;

                        newCamposVector.push(object);

                    }

                });

                formData.append("campoList", JSON.stringify(newCamposVector));

                formData.append('selectedCampos', JSON.stringify($('.campo-selector').select2('data')));

                // ###### /.Campos ######
                /***********************************************************/


                /***********************************************************/
                // ###### Add Reference country ######

                formData.append('selectedReferenceCountries', JSON.stringify($('.referenced-selector').select2('data')));

                // ###### /.Reference country ######
                /***********************************************************/

                var request = $.ajax({

                    url: "private/users/museum/apps/survivors_project/survivors_project_add.php",

                    type: "POST",

                    contentType: false,

                    processData: false,

                    data: formData,

                    dataType: "json"
                });

                request.done(function(response) {

                  console.log("response antes de swal y toastr");

                  console.log(response);

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch (Number(response.status)) {

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {

                            switch (Number(response.status)) {

                                case 1:

                                window.location.replace("museum_app_survivors_project_list.php");

                                break;

                            }

                        }

                    });


                    helper.showToastr("Se acualizo", 'Se actualizo');

                    helper.unblockStage();

                });

                request.fail(function(jqXHR, textStatus) {

                    console.log(jqXHR);

                    console.log("error");

                    helper.showToastr("ops", 'error');

                    helper.unblockStage();

                });

            }

        });
      }

    /**
     * @function createProfileImage
     * @description Create the newspaper image.
     */

     var createProfileImage = function(){

        console.log(`initialPreviewCoverConfig`);
        console.log(initialPreviewCoverConfig);

        
        $("#picture").fileinput({

            initialPreviewAsData: true,

            initialPreview: initialPreviewCoverPaths,

            initialPreviewConfig: initialPreviewCoverConfig,

            dropZoneEnabled: editMode,

            theme: "fas",

            uploadUrl: "private/users/museum/apps/survivors_project/survivors_project_img_upload.php", //on modification image upload only!!

            deleteUrl: "private/users/museum/apps/survivors_project/survivors_project_delete_image_main.php",

            showCaption: true,

            showPreview: true,

            showRemove: false,

            showUpload: editMode,

            showCancel: false,

            showClose: false,

            browseOnZoneClick: true,

            previewFileType: 'any',

            language: "es",

            maxFileSize: 0,

            allowedFileTypes: ["image"],

            overwriteInitial: false,

            allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],

            maxFilePreviewSize: 10240,
            /*
            minImageWidth: 500,

            minImageHeight: 500,*/

            uploadExtraData: function() {   //On modification img upload data!!

                return {

                    id: survivorId,

                    editMode: editMode,

                    source: 'survivors_project',

                    galleryFolder: 'survivors_project'

                };

            }

        }).on('filesorted', function(e, params) {



        }).on('fileuploaded', function(e, params) {

            console.log("onfileuploaded!!???");

            location.reload();

        }).on('filebatchuploadcomplete', function(event, files, extra) {

            console.log(event);
            console.log(files);

            console.log("filebatchuploadcomplete...");

            location.reload();

        }).on("filepredelete", function(jqXHR) {


        }).on('filedeleted', function(event, key, jqXHR, data) {

            console.log(event);
            console.log(key);
            console.log(data);

            return {

                source: 'survivors_project',

                galleryFolder: 'survivors_project'

            };

            helper.showToastr("Imagen eliminada", 'Se eliminó la imagen del survivors');

        }).on('fileimageresizeerror', function(event, data, msg) {

            showErrorSwal(msg);

        }).on('fileuploaderror', function(event, data, msg) {

            var form = data.form, files = data.files, extra = data.extra,

            response = data.response, reader = data.reader;

            console.log(event, data, msg);

            //showErrorSwal(msg);

        }).on('filebeforedelete', function() {

            return new Promise(function(resolve, reject) {

                swal({

                    title: "¿Eliminar imagen?",

                    allowOutsideClick: false,

                    html: "La misma no aparecerá más listada. Esta acción no se puede deshacer.",

                    type: "warning",

                    showCancelButton: true,

                    confirmButtonText: 'Si, borrar',

                    cancelButtonText: 'no, salir'

                }).then((result) => {

                    if (result.value) {

                        resolve();

                    }else{

                        reject();
                    }

                })

            });
        });

        $('.kv-cust-btn').on('click', function() {

            origin = $(this).attr('data-origin');

            var $btn = $(this);

            var key = $btn.data('key');

            var test = $btn.data('test');

            openKey = key;

            
            var item = initialPreviewCoverConfig.find(item => item.key == key);
            
            $('#image-description').val(item.description);

            $('#myModal').modal('show');
            

        });



    }// /.createProfileImage


    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

    var addListeners = function() {

        $('.was_rescued').on('change', function(){

            var checked = ($(this).prop('checked'));

            $('.rescuer-selector').prop("disabled", !checked);
            $('.new_rescuer_name').prop("disabled", !checked);
            $('.new_rescuer_surname').prop("disabled", !checked);

        });

        $('input[type=radio][name=radio_group]').change(function() {

            if(editMode) updateValue(survivorId, 'liberation_army', this.value, 'app_museum_survivors_project');
           
        });


        $('#bio').summernote({

            disableDragAndDrop: true,

            height: 200,

            toolbar: [

            ['style', ['bold', 'italic', 'underline']],

            ['Misc', ['fullscreen', 'undo', 'redo']]

            ],

            callbacks: {

                onInit: function() { },

                onEnter: function() { },

                onKeyup: function(e) { },

                onKeydown: function(e) { },

                onPaste: function(e) { },

                onChange: function(contents, $editable) { },

                onBlur: function(){ },

                onFocus: function() { }

            }

        });

        $("#m_repeater_1").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_2").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_3").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_4").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_5").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_6").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_7").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_8").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $("#m_repeater_9").repeater({

            initEmpty: !1,

            defaultValues: { "text-input": "foo" },

            show: function() { $(this).slideDown() },

            hide: function(e) { $(this).slideUp(e) }

        })

        $('.tutor-selector').select2({ tokenSeparators: [',', ' '], placeholder: "seleccioná o agregá otros paises de referencia" });

        $('.referenced-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá a paises"

        });

        $('.rescuer-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá a rescatador/es"

        });

        $('.spouse-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá a su/s cónyugue/s"

        });

        $('.son-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá a sus/s hijo/s"

        });

        $('.grandson-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá a su/s hijo/s"

        });

        $('.book-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá a su/s libro/s"

        });

        $('.film-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá a su/s películas/s"

        });

        $('.honor-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá a su/s distinciones/s"

        });

        $('.gueto-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá los guetos donde estuvo"

        });

        $('.campo-selector').select2({

            tokenSeparators: [',', ' '],

            placeholder: "seleccioná o agregá los campos donde estuvo"

        });

       $('[data-toggle="tooltip"]').tooltip();

       createCountryFinder();

       createAtThatMomentFinder();

       createAtTheBeginningFinder();

       createLiberationPlace();

       if(editMode && $('#hidden-country-id').val() != "") findCities($('#hidden-country-id').val()); 

       $('#entry_year').datepicker({

           format: 'yyyy',

           todayBtn: false,

           todayHighlight: true,

           autoclose: true,

           changeYear: true,

           changeMonth: false,

            viewMode: "years", 

            minViewMode: "years",

           defaultViewDate: {year: '1930'}

       }).datepicker().on('changeDate', function (ev) {

            calculateAge();

            if(editMode){

                updateValue(survivorId, 'entry_year', $('#entry_year').val(), 'app_museum_survivors_project');

            }

        });


        $('#birthday_year').datepicker({

           format: 'yyyy',

           todayBtn: false,

           todayHighlight: true,

           autoclose: true,

           changeYear: true,

           changeMonth: false,

            viewMode: "years", 

            minViewMode: "years",

           defaultViewDate: {year: '1930'}

        }).datepicker().on('changeDate', function (ev) {

            $('#birthday_month').removeAttr('disabled');

            if(editMode){

                console.log("updateDate: " + updateDate);

                var updateDate = $('#birthday_year').val() + '-' + $('#birthday_month').val() + '-' + $('#birthday_day').val(); 

                updateValue(survivorId, 'birthday', updateDate, 'app_museum_survivors_project');

            }else{

                saveYear = ev.date.getFullYear();

                sendYear = ev.date.getFullYear();

                sendMonth = '00';

                sendDay = '00';

                $('#birthday_month').datepicker("setDate", new Date(ev.date.getFullYear(),1,1) );

                $('#birthday_month').val('');

                $('#birthday_day').addAttr('disabled');

            }

        });

        $('#birthday_month').datepicker({

           format: 'mm',

           todayBtn: false,

           todayHighlight: true,

           autoclose: true,

           changeYear: false,

           changeMonth: false,

            viewMode: "months", 

            minViewMode: "months",

            showButtonPanel: false,

            defaultViewDate: {year: '1955'}

        }).datepicker().on('changeDate', function (ev) {

            $('#birthday_day').removeAttr('disabled');

            if(editMode){

                console.log("updateDate: " + updateDate);

                var updateDate = $('#birthday_year').val() + '-' + $('#birthday_month').val() + '-' + $('#birthday_day').val(); 

                updateValue(survivorId, 'birthday', updateDate, 'app_museum_survivors_project');
            
            }else{

                sendMonth = ev.date.getMonth();

                sendDay = '00';

                $('#birthday_day').datepicker("setDate", new Date(saveYear,ev.date.getMonth(),1) );

                $('#birthday_day').val('');

            }

        });

        $('#birthday_day').datepicker({

           format: 'dd',

           todayBtn: false,

           todayHighlight: true,

           autoclose: true,

           changeYear: false,

           changeMonth: false,

            viewMode: "days", 

            minViewMode: "days",

            showButtonPanel: false,

            defaultViewDate: {year: '1955'}


        });

        $('#birthday_day').datepicker().on('changeDate', function (ev) {

            if(editMode){
            
                console.log("updateDate: " + updateDate);

                var updateDate = $('#birthday_year').val() + '-' + $('#birthday_month').val() + '-' + $('#birthday_day').val(); 

                updateValue(survivorId, 'birthday', updateDate, 'app_museum_survivors_project');

            }else{

                sendDay = ev.date.getDate();

                console.log("all: " + sendYear + " - " + sendMonth + " - " + sendDay);

            }

        });


        $('#death_date').datepicker({

           format: 'yyyy-mm-dd',

           todayBtn: false,

           todayHighlight: true,

           autoclose: true,

           changeYear: true,

           defaultViewDate: {year: '1930'}

        });

       


       $('.emigrated').change(function() {

            var checked = ($(this).prop('checked'));

            console.log("checked: " + checked);

            $('#emigrated_place').prop("disabled", !checked);

        });


       $('.war-emigrated').change(function() {

            var checked = ($(this).prop('checked'));

            console.log("checked: " + checked);

            $('#war_emigrated_place').prop("disabled", !checked);

        });


       $('.war-ghetto').change(function() {

            var checked = ($(this).prop('checked'));

            console.log("checked: " + checked);

            $('#war_ghetto_name').prop("disabled", !checked);

        });


       $('.war-ghetto').change(function() {

            var checked = ($(this).prop('checked'));

            console.log("checked: " + checked);

            $('#war_ghetto_name').prop("disabled", !checked);

        });

        $('.entry-legal').change(function() {
            console.log("Change entry legal");

            var checked = ($(this).prop('checked'));

            console.log("checked: " + checked);

            $('#entry_illegal_route').prop("disabled", checked);

        });



       $('#exit_from_form').click(function(e) {
           window.location.replace("index.php");
       });

       $('#back_to_list').click(function(e) {
           window.location.replace("museum_app_survivors_project_list.php?tabId=" + redirectId);
       });

       $(".update-btn").click(function() {
           
           if (!editMode) return;

           var dbName = $(this).attr('data-db-value');

           var id = $(this).attr('data-id');

           var newValue = $(this).parent().parent().find(':input').val();

           var fieldName = $(this).parent().parent().find(':input').attr('name');

           var valid = $('#' + fieldName).valid();

           if (dbName != newValue && valid) {

                $(this).attr('data-db-value', newValue);

                updateValue(id, fieldName, newValue, 'app_museum_survivors_project');

            }

       });


        $(document).on('click', '.select-update-btn', function(){

            if (!editMode) return;

            var idSurivior = $(this).attr('data-id');

            var action = $(this).attr('data-action');

            console.log(`upate select bnt! idSurvivor:  ${idSurivior} - action: ${action}`);


            ////Books selected from dropbox
            //formData.append('selectedBooks', JSON.stringify($('.book-selector').select2('data')));


            var optionsArr = JSON.stringify($(this).parent().parent().find('.selector-for-update').select2('data'));

            console.log(optionsArr);

            
            
            var listTagClass = '.' + action + '-tags';  //Para saber donde agregar o quitar los elementos dinamicamente al actualizar.

            console.log(`listTagClass para add delete item: ${listTagClass}`);

            updateAddMultipleSelect(idSurivior, optionsArr, action, $(this), listTagClass);

        });

        //Delete Alternative Name relation
        $(document).on('click', '.delete-alternative-relation-tag', function(){

            var idRelation = $(this).attr('id');

            var name = $(this).parent().text();



            deleteRelationSwalNotification(idRelation, "Eliminar este nombre?", name, "alternative_name", $(this));

        });

        //Delete referenced countries relation
        $(document).on('click', '.delete-referenced-relation-tag', function(){

            var idRelation = $(this).attr('id');

            var name = $(this).parent().text();


            deleteRelationSwalNotification(idRelation, "Eliminar referencia?", name, "referenced", $(this));

        });

        //Delete GUETO relation
        $(document).on('click', '.delete-gueto-relation-tag', function(){

            var idRelation = $(this).attr('id');

            var name = $(this).parent().text();

            deleteRelationSwalNotification(idRelation, "Eliminar Gueto?", name, "gueto", $(this));

        });

       //Delete CAMPO relation
       $(document).on('click', '.delete-campo-relation-tag', function(){

            var idRelation = $(this).attr('id');

            var name = $(this).parent().text();

            deleteRelationSwalNotification(idRelation, "Eliminar Campo?", name, "campo", $(this));

       });


       //Delete Family Relations (tags)

        $(document).on('click', '.delete-family-relation-tag', function(){

            var idRelation = $(this).attr('id');

            var name = $(this).parent().text();

            var familyRelation = $(this).attr('data-family-relation');

            console.log(`familyRelation: ${familyRelation}`);

            //deleteRelationSwalNotification(idRelation, "Eliminar relacion familiar?", name, "delete_family_relation", $(this));
            deleteRelationSwalNotification(idRelation, "Eliminar relacion familiar?", name, familyRelation, $(this));

        });


       

       //Delete book relation
       $(document).on('click', '.delete-book-relation-tag', function(){

            var idRelation = $(this).attr('id');

            var name = $(this).parent().text();

            deleteRelationSwalNotification(idRelation, "Eliminar Libro?", name, "book", $(this));

       });

       //Delete film relation
       $(document).on('click', '.delete-film-relation-tag', function(){

            var idRelation = $(this).attr('id');

            var name = $(this).parent().text();

            deleteRelationSwalNotification(idRelation, "Eliminar Película?", name, "film", $(this));

       });

       //Delete honor relation
       $(document).on('click', '.delete-honor-relation-tag', function(){

            var idRelation = $(this).attr('id');

            var name = $(this).parent().text();

            deleteRelationSwalNotification(idRelation, "Eliminar Reconocimiento?", name, "honor", $(this));

       });

       //Delete honor relation
       $(document).on('click', '.delete-rescuer-relation-tag', function(){

            var idRelation = $(this).attr('id');

            var name = $(this).parent().text();

            deleteRelationSwalNotification(idRelation, "Eliminar Rescatador?", name, "rescuer", $(this));

        });


       $('.toggler-info:checkbox').change(function() {

           if (!editMode) return;

           var fieldName = $(this).attr('name');

           var newValue = ($(this).is(":checked")) ? 1 : 0;

           updateValue(survivorId, fieldName, newValue, 'app_museum_survivors_project');

        });



        // ** 
        //On edit mode, add new items: alternativeNames, books, films, etc

        $('.btn-add-in-edition-alternatives').click(function(){

            console.log(`send to add alternative names`);

            var name = $('.new_alternative_name').val();
            var surname = $('.new_alternative_surname').val();

            if(name == "" || surname == ""){

                emptySwalNotifier();
                return;

            }

            var action = 'alternativeName';

            console.log(`item field: ${itemField} action: ${action}`);

            var vectorData = Array();

            var object = {};

            object.name = name;

            object.surname = surname;

            vectorData.push(object);

            requestAddNewItem(action, vectorData, $(this));

        });


        //Add Campo

        $('.btn-add-in-edition-campo').click(function(){

            console.log(`send to add campo`);

            var campoName = $('.new_campo_name').val();

            if(campoName == ""){

                emptySwalNotifier();
                return;

            }

            var action = 'addCampo';

            requestAddNewItem(action, campoName, $(this));

        });

        //Spouse, son, grandson
        $('.btn-add-in-edition-family').click(function(){

            console.log(`send to add spouse name and surname`);

            var action = 'addFamilyData';

            var familyTypeData = $(this).data('family-type');

            var relationTpye = 1;

            var name;

            var surname;

            if(familyTypeData == 'spouse'){

                name = $('.new_spouse_name').val();

                surname = $('.new_spouse_surname').val();

            }else if(familyTypeData == 'son'){
                
                name = $('.new_son_name').val();

                surname = $('.new_son_surname').val();

                relationTpye = 2;

            }else if(familyTypeData == 'grandson'){

                name = $('.new_grandson_name').val();

                surname = $('.new_grandson_surname').val();

                relationTpye = 3;

            }

            

            var vectorData = [];

            var object = {};

            object.name = name;

            object.surname = surname;

            if(name == "" || surname == ""){

                emptySwalNotifier();
                return;

            }

            object.relation_type = relationTpye;

            vectorData.push(object);

            requestAddNewItem(action, vectorData, $(this));

        });


        $('.btn-add-in-edition-book').click(function(){

            console.log(`send to add book`);

            var bookName = $('.new_book_name').val();

            if(bookName == ""){
                emptySwalNotifier();
                return;
            }

            var action = 'addBook';

            requestAddNewItem(action, bookName, $(this));

        });
    
        $('.btn-add-in-edition-film').click(function(){

            console.log(`send to add film`);

            var filmName = $('.new_film_name').val();

            if(filmName == ""){
                emptySwalNotifier();
                return;
            }

            var action = 'addFilm';

            requestAddNewItem(action, filmName, $(this));

        });

        $('.btn-add-in-edition-honor').click(function(){

            console.log(`send to add honor`);

            var honorName = $('.new_honor_name').val();

            if(honorName == ""){
                emptySwalNotifier();
                return;
            }

            var action = 'addHonor';

            requestAddNewItem(action, honorName, $(this));

        });

        $('.btn-add-in-edition-rescuer').click(function(){

            console.log(`send to add rescuer`);

            var rescuerName = $('.new_rescuer_name').val();
            var rescuerSurname = $('.new_rescuer_surname').val();

            if(rescuerName == "" || rescuerSurname == ""){

                emptySwalNotifier();
                
                return;
            }

            var action = 'addRescuer';

            var object = {};

            object.name = rescuerName;
            object.surname = rescuerSurname;

            var vectorData = [];

            vectorData.push(object);

            requestAddNewItem(action, vectorData, $(this));

        });


    }// /. Ends addListeners()

    var emptySwalNotifier = function(){

        swal({

            title: "Completar campos",

            allowOutsideClick: false,

            html: "Verificá que no tengas campos vacíos",

            showCancelButton: false,

            confirmButtonText: 'Entendido'

        })

    }

    var requestAddNewItem = function(action, items, elemt){


        var request = $.ajax({

            url:'private/users/museum/apps/survivors_project/survivors_project_items_controller.php',
            
            type: 'POST',

            data:{

                id: survivorId,

                action: action,

                items: items

            },

            dataType: 'json'

        });

        request.done(function(response){

            console.log(response);

            swal({

                title: response.title,

                allowOutsideClick: false,

                html: response.desc,

                type: response.status,

                showCancelButton: false,

                confirmButtonText: 'confirm'

            }).then((result) => {

                console.log("Agregar el element nombre y apellido..");

                location.reload();


                //TODO:: no hacer reload, agregar dinamicamente los elementos con su boton de eliminar
                
            
            });

            helper.showToastr("Se acualizo", 'Se actualizo');

            helper.unblockStage();

        });

        request.fail(function(jqXHR, textStatus){

            console.log(jqXHR);

            console.log("error");

            helper.showToastr("ops", 'error');

            helper.unblockStage();

        });

    }


    var createCountryFinder = function(){

        t = new Bloodhound({

           datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

           queryTokenizer: Bloodhound.tokenizers.whitespace,

           prefetch:{

               url : "private/users/services/get_list_of_countries.php",

               cache: false
           }
        })

        var context = $("#country-list .typeahead").typeahead(null, {

           hint: false,

           highlight: false,

           minLength: 1,

           name: "best-pictures",

           display: "value",

           source: t,

           autoselect: true,

           templates: {

               suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")

           }

        }).on("typeahead:render", function() {

           var searchTerm = $(this).val();

           $('#country-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

        }).on('typeahead:selected', function (e, datum) {

            findCities(datum.id);

            if(editMode){

                updateValue(survivorId, 'nationality', datum.id, 'app_museum_survivors_project');
                
                updateValue(survivorId, 'city', -1, 'app_museum_survivors_project');

            }else{

                $('#hidden-country-id').val(datum.id);

            }

            if(datum.id == 51 || datum.id == 11 || datum.id == 236){

                $('.emigrated-toggle').removeClass('disabled');

            }else{

                $('.emigrated-toggle').prop("disabled", true);

                $('.emigrated-toggle').addClass('disabled');

                var test = $('.toggler-info-emigrated:checkbox').is(":checked");

                if(test){

                    $('.toggler-info-emigrated').click();

                }



            }

        }).on('keyup', this, function (event) {

        }).on('keydown', this, function (event) {

        }).on('focus', this, function(event){

           if(editMode) $('#hidden-country-id').val("");

        });

    }

    


     var calculateAge = function(){

        var entry = parseInt($('#entry_year').val());

        var birth = parseInt($('#birthday_year').val());

        if(isNaN(birth)) return;

        if(entry < birth){

            swal({

                title: 'opss!',

                allowOutsideClick: false,

                html: 'La fecha de llega a la argentina no puede ser menor a la fecha de su nacimiento. Por favor, revisá ambos valores',

                type: 'error',

                showCancelButton: false,

                confirmButtonText: 'entendido!'

            })

        }else{

            var name = $('#name').val();

            var surname = $('#surname').val();

            var st = '<b>' + name + ' ' + surname + '</b> tenía <b>' + (entry - birth).toString() + '</b> años al momento de su ingreso llegada';

            $('.age-calculator').html(st);

        }

     }

     var createAtThatMomentFinder = function(){

        t = new Bloodhound({

           datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

           queryTokenizer: Bloodhound.tokenizers.whitespace,

           prefetch:{ url : "private/users/services/get_list_of_countries.php", cache: false }

       })

       var context = $("#at-that-moment-list .typeahead").typeahead(null, {

           hint: false, highlight: false, minLength: 1, name: "best-pictures",

           display: "value", source: t, autoselect: true,

           templates: { suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")}

       }).on("typeahead:render", function() {

           var searchTerm = $(this).val();

           $('#country-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

       }).on('typeahead:selected', function (e, datum) {

            if(editMode){

                updateValue(survivorId, 'nationality_at_that_moment', datum.id, 'app_museum_survivors_project');
                
            }else{

                $('#hidden-at-that-moment-id').val(datum.id);

            }

       }).on('keyup', this, function (event) {

       }).on('keydown', this, function (event) {

       }).on('focus', this, function(event){

           if(editMode) $('#hidden-country-id').val("");

       });

     }

     var createAtTheBeginningFinder = function(){

        t = new Bloodhound({

           datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

           queryTokenizer: Bloodhound.tokenizers.whitespace,

           prefetch:{ url : "private/users/services/get_list_of_countries.php", cache: false }

       })

       var context = $("#at-the-beginning-list .typeahead").typeahead(null, {

           hint: false, highlight: false, minLength: 1, name: "best-pictures",

           display: "value", source: t, autoselect: true,

           templates: { suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")}

       }).on("typeahead:render", function() {

           var searchTerm = $(this).val();

           $('#country-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

       }).on('typeahead:selected', function (e, datum) {

            if(editMode){

                updateValue(survivorId, 'nationality_at_the_beginning_of_war', datum.id, 'app_museum_survivors_project');
                
            }else{

                $('#hidden-at-the-beginning-id').val(datum.id);

            }

       }).on('keyup', this, function (event) {

       }).on('keydown', this, function (event) {

       }).on('focus', this, function(event){

           //if(editMode) $('#hidden-country-id').val("");

       });

     }

     var createLiberationPlace = function(){

        t = new Bloodhound({

           datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

           queryTokenizer: Bloodhound.tokenizers.whitespace,

           prefetch:{ url : "private/users/services/get_list_of_countries.php", cache: false }

       })

       var context = $("#liberation-place .typeahead").typeahead(null, {

           hint: false, highlight: false, minLength: 1, name: "best-pictures",

           display: "value", source: t, autoselect: true,

           templates: { suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")}

       }).on("typeahead:render", function() {

           var searchTerm = $(this).val();

           $('#country-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

       }).on('typeahead:selected', function (e, datum) {

            if(editMode){

                updateValue(survivorId, 'liberation_place', datum.id, 'app_museum_survivors_project');
                
            }else{

                $('#hidden-liberation-place-id').val(datum.id);

            }

       }).on('keyup', this, function (event) {

       }).on('keydown', this, function (event) {

       }).on('focus', this, function(event){

           //if(editMode) $('#hidden-country-id').val("");

       });

     }
     
     var findCities = function(id){

        $('#cities-list').removeClass('disabled');

        if(letT != undefined){

            letT.clear();

            letT.clearPrefetchCache();
            
            letT.clearRemoteCache();

            $('.cleanner').empty();

            $('.cleanner').append('<input type="text" id="xcv" name="xcv" class="typeahead form-control m-input" placeholder="Ciudad" >');

            $('.cleanner').append('<input type="hidden" id="hidden-city-id" name="hidden-city-id" >');
        }

        letT = new Bloodhound({

            datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

            queryTokenizer: Bloodhound.tokenizers.whitespace,

            prefetch:{

                url : "private/users/services/get_list_of_cities_by_country_id.php?id=" + id,

                cache: false

            }

        })

        letT.initialize(true);

        var context = $("#cities-list .typeahead").typeahead(null, {

           hint: false,

           highlight: false,

           minLength: 1,

           name: "best-pictures",

           display: "value",

           source: letT,

           autoselect: true,

           templates: { suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>") }

       }).on("typeahead:render", function() {

           $('#cities-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

       }).on('typeahead:selected', function (e, datum) {

            if(editMode){
                
                updateValue(survivorId, 'city', datum.id, 'app_museum_survivors_project');

            }else{

                $('#hidden-city-id').val(datum.id);
            }


       }).on('keyup', this, function (event) {

       }).on('keydown', this, function (event) {

       }).on('focus', this, function(event){

           if(editMode) $('#hidden-city-id').val("");

       });

     }


     var deleteRelationSwalNotification = function(idRelation, titleStr, nameHtml, actionTagToDelete, elemt){


        console.log(`deleteRelationSwalNotif: idRelation: ${idRelation} actionTagToDelete: ${actionTagToDelete}`);

        Swal({

                title: titleStr,

                html: 'Esta acción eliminará a <b>'+nameHtml+'</b>.<br><br>Esta acción no se puede deshacer. <br><br>¿Eliminar de todos modos?',

                type: 'error',

                showCancelButton: true,

                confirmButtonText: 'Si, eliminar',

                cancelButtonText: 'no, salir'   

            }).then((result) => { 

                if(result.value == true){

                    console.log("ELiminar este tag de relacion!");

                    deleteRelationTag(idRelation, elemt, actionTagToDelete);

                }

            });


     } 

     /**
      * @function update/add options to multiple select (books, family relations, films, honors)
      * @description add new items to relation tables for this survivor
      * @param {int} id  - idSurivor 
      * @param {string} action - films, books, family... 
      * @param {elmt} tagElmt 
      *
      */

     var updateAddMultipleSelect = function(id_survivor, optionsArr, action, elmt, listTagClass){

       var request = $.ajax({

             url: "private/users/museum/apps/survivors_project/survivors_project_update_add.php",

             type: "POST",

             data: {

                 id_survivor: id_survivor,

                 items: optionsArr,

                 action: action
             },

             dataType: "json"
         });

         request.done(function(response) {

            console.log(response);

            var itemInsertedData = response.itemInsertedData;

            console.log(itemInsertedData);

            var parsedData = JSON.parse(itemInsertedData);

             swal({

                 title: response.title,

                 allowOutsideClick: false,

                 html: response.msg,

                 type: response.alert,

                 showCancelButton: false,

                 confirmButtonText: response.button

             }).then((result) => {


                //TODO:: resfresh select para tenner las opciones que ahora dejaron estar relacionadas.. y poder volver a elegirlas
                

                var deleteClassItem = 'delete-' +  action.slice(0, -1) + '-relation-tag';   //films -> films, books -> book, etc

                console.log(`el action al agregar es: ${action}`);

                var strDataFamilyRelation = '';

                if(action == "grandsons" || action == "sons" || action == "spouses"){

                    deleteClassItem =  'delete-family-relation-tag';

                    strDataFamilyRelation = 'data-family-relation="'+action.slice(0, -1)+'"';

                }

                var strElmt = '';
                
                $.each(parsedData, function(i, v){

                    console.log(`lastId: ${v.lastId} - name: ${v.name}`);

                    strElmt += '<div class="btn-group">';
                    strElmt += '<a  href="#" role="button" class="btn btn-default btn-xs disabled" aria-disabled="true">';
                    strElmt += v.name;
                    strElmt += '</a>';
                    strElmt += '<a id="'+v.lastId+'" data-item-id="'+v.id+'" '+strDataFamilyRelation+' class="btn btn-xs '+deleteClassItem+'" data-toggle="tooltip" title="Eliminar">';
                    strElmt += '<i class="fas fa-times-circle"></i>'; 
                    strElmt += '</a>';
                    strElmt += '</div>';


                    $(`.${action.slice(0, -1)}-selector option[value='${v.id}']`).remove(); //Quitar de la lista del select los items que ya se relacionan / agregaron a este sobreviviente

                });
                
                elmt.parent().parent().parent().find(listTagClass).append(strElmt);

                /*

                var data = {
                    id: 1,
                    text: 'Barn owl'
                };

                
                var newOption = new Option(data.text, data.id, false, false);
                $('.book-selector').append(newOption).trigger('change');

                */


                $(`.${action.slice(0, -1)}-selector`).val(null).trigger('change');    //Limpia opciones seleccionadas (no saca del listado)

             });


             helper.showToastr("Se acualizo", 'Se actualizo');

             helper.unblockStage();

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

             console.log("error");

             helper.showToastr("ops", 'error');

             helper.unblockStage();

         });

     }


      /**
      * @function deleteRelationTag
      * @description delete relation tag in DB. Books, family relations, films...
      * @param {int} id_relation 
      * @param {string} action - films, books, family... 
      * @param {elmt} tagElmt 
      *
      */

     var deleteRelationTag = function(id_relation, tagElmt, action){

       var request = $.ajax({

             url: "private/users/museum/apps/survivors_project/survivors_project_delete.php",

             type: "POST",

             data: {

                 id: id_relation,

                 action: action
             },

             dataType: "json"
         });

         request.done(function(response) {

           console.log(response);

             swal({

                 title: response.title,

                 allowOutsideClick: false,

                 html: response.msg,

                 type: response.alert,

                 showCancelButton: false,

                 confirmButtonText: response.button

             }).then((result) => {

                 console.log("Quitar el boton/tag  de la vista al confirmar");

                 tagElmt.parent().hide();    //Quitar el boton/tag  de la vista al confirmar


                 //Agregar el elemento (optioin) a la lista del select...

                var itemDeletedId = tagElmt.attr('data-item-id');

                var itemDeletedName = tagElmt.parent().find('.btn').text();
                
                var newOption = new Option(itemDeletedName, itemDeletedId, false, false);


                //Hay campos que no tienen select, como alternative_names
                if(action != 'alternative_name'){

                    console.log(`add option to select: ${action}-selector`);
                

                    $(`.${action}-selector`).append(newOption).trigger('change');

                }

                

                
             });

             helper.showToastr("Se acualizo", 'Se actualizo');

             helper.unblockStage();

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

             console.log("error");

             helper.showToastr("ops", 'error');

             helper.unblockStage();

         });

     }


     /**
      * @function updateValue
      * @description update individual value in DB.
      *
      * @param {int} id                 - object id.
      * @param {string} fieldName       - the field to modify
      * @param {string} newValue        - object value.
      * @param {int} filter             - filter type.
      */

      var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

         console.log("Update - FieldName: "+fieldName + " - newValue: "+newValue + " - table: "+filter);

         console.log("update value...only once?");

         helper.blockStage("actualizando...");

         var request = $.ajax({

             url: "private/users/museum/general/update_value.php",

             type: "POST",

             data: {

                 id: id,

                 fieldName: fieldName,

                 newValue: newValue,

                 source: "sobrevivientesProject",

                 filter: filter
             },

             dataType: "json"

         });

         request.done(function(result) {

             console.log("tracing result!");

             console.log(result);

             helper.showToastr(result.title, result.msg);

             if (callback != undefined) callback(callbackParams);

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

             console.log("error");

         });

         if (callback == undefined) helper.unblockStage();

         localStorage.setItem('refresh', 'true');

     }

     /**
      * @function loadAjaxImages
      * @param loadSource - evaluate this param to get one batch or a single one.
      * @description if we are in edition mode load all the images for Bootstrap File Input.
      *                
      * callFrom: if call to get profile image or batch to gallery
      */

      var loadAjaxImages = function(from, field, loadSource, config, myPreviews) {

        console.log(`load source: ${loadSource}`);

        console.log("loadAjaxImages: from:" + from + " - laodSource: " + loadSource + " - config: " + config + " - myPreviews: " + myPreviews);

        var request = $.ajax({

             url: "private/users/services/get_survivor_project_main_image.php",

             type: "POST",

             data: {

                 id: survivorId,

                 field: field,

                 source: loadSource,

                 from: from
             },

             dataType: "json"
         });

         request.done(function(result) {


            console.log(result);

            console.log(result.length);


            if(result.length >0){

                for (var i = 0; i < result.length; i++) {

                    var id = result[i].id;

                    var sid = result[i].sid;
                 
                    var imgPath = result[i].picture;
                    
                    var itemName = sid + '_' + imgPath + '_original.jpeg';
                    

                    console.log(`id ${id} - sid: ${sid} - picture: ${result[i].picture}`);

                    imgPath = (loadSource == 'mainPicture') ? imgPath : 'images/'+imgPath;


                    path = 'private/sources/images/survivors_project/' + sid + '/' + imgPath + '/' + itemName;

                    
                    console.log("prueba: " + path);

                    myPreviews.push(path);

                    config.push({

                        'key': id,

                        //'table': from,

                        //'internal_order': internalOrder,


                    });

                }

            }else{
             
             console.log("sin imagenes para este item");
           
            }

            if(loadSource == "mainPicture"){

                console.log('create profileImage..');

                createProfileImage();

            }

            if(loadSource == "gallery"){

                console.log('create gallery..');
                createGallery();

            }

            

             

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

         });

     }



     /**
     * @function createGallery
     * @description Create the gallery.
     */

      var createGallery = function() {

        console.log('createGllery..');


        console.log(initialPreviewgalleryPaths);

        console.log(initialPreviewGalleryConfig);


        var btns = '<button data-origin="image" type="button" class="kv-cust-btn btn btn-sm btn-kv btn-default btn-outline-secondary" title="Editar"{dataKey}>' +
        '<i class="fas fa-pen"></i>' + '</button>';

        $("#images-batch").fileinput({

            otherActionButtons: btns,

            initialPreviewAsData: true,

            initialPreview: initialPreviewgalleryPaths,

            initialPreviewConfig: initialPreviewGalleryConfig,

            theme: "fas",

            uploadUrl: "private/users/museum/apps/survivors_project/survivors_project_update_images_batch.php",

            deleteUrl: "private/users/museum/apps/survivors_project/survivors_project_delete_image_gallery.php",     //TODO:: cambiar este archivo

            showCaption: true,

            showPreview: true,

            showRemove: false,

            showUpload: true,

            showCancel: true,

            showClose: false,

            browseOnZoneClick: true,

            previewFileType: 'any',

            language: "es",

            maxFileSize: 0,

            allowedFileTypes: ["image"],

            overwriteInitial: false,

            allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],

            maxFilePreviewSize: 10240,

            uploadExtraData: function() {

                return {

                    id: survivorId,

                    source: 'survivors_project'

                };

            }

        }).on('filesorted', function(e, params) {

            rearangeStack('museum_images', JSON.stringify(params.stack));

        }).on('fileuploaded', function(e, params) {

        }).on('filebatchuploadcomplete', function(event, files, extra) {

           
            location.reload();

        }).on("filepredelete", function(jqXHR) {


        }).on('filedeleted', function(event, key, jqXHR, data) {

            console.log(event, key, jqXHR, data);

            helper.showToastr("Imagen eliminada", 'Se eliminó la imagen del sobreviviente');

        }).on('fileuploaderror', function(event, data, msg) {

            var form = data.form, files = data.files, extra = data.extra,

            response = data.response, reader = data.reader;

            console.log(event, data, msg);

            //showErrorSwal(msg);

        }).on('filebeforedelete', function() {

            return new Promise(function(resolve, reject) {

                swal({

                    title: "¿Eliminar imagen?",

                    allowOutsideClick: false,

                    html: "La misma no aparecerá más en el listado",

                    type: "warning",

                    showCancelButton: true,

                    confirmButtonText: 'Si, borrar',

                    cancelButtonText: 'no, salir'

                }).then((result) => {

                    if (result.value) {

                        resolve();

                    }else{

                        reject();
                    }

                })

            });
        });

        $('.kv-cust-btn').on('click', function() {

            origin = $(this).attr('data-origin');

            var $btn = $(this);

            var key = $btn.data('key');

            openKey = key;

            var item = initialPreviewGalleryConfig.find(item => item.key == key);

            $('#image-description').val(item.description);

            $('#myModal').modal('show');

        });

    }



    

    return {

        init: function() {

            editMode = ($('#edit-mode').val() == 1) ? true : false;

            helper.setMenu();

            if (editMode) {

              console.log("Estamos en edit mode!");

                var temp;

                if (document.referrer != "") {

                    temp = helper.getGETVariables(document.referrer, 'tabId');

                    console.log("temP: "+temp);


                } else {

                    temp = null;
                }

                redirectId = (temp == null) ? 1 : temp;

                survivorId = $('#survivor-project-id').val();

            } else {

                isFirstLoad = false;
            }

            addListeners();

            addFormValidations();


            if(editMode){

                loadAjaxImages('app_museum_survivors_project_main_picture', 'sid', 'mainPicture', initialPreviewCoverConfig, initialPreviewCoverPaths)

                loadAjaxImages('app_museum_survivors_project_gallery_pictures', 'sid', 'gallery', initialPreviewGalleryConfig, initialPreviewgalleryPaths);

                

            }else{

                createProfileImage();

                createGallery();
                

            }

        }

    };

 }();

 jQuery(document).ready(function() { MuseumAppSurvivorsProjectAdd.init(); });