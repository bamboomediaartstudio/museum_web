/**
 * @summary Manage all the messages backup actions.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */
 
/**
* @function MuseumEmailsBackup
* @description Initialize and include all the methods of this class.
*/

var MuseumEmailsBackup = function() {

    helper = new Helper();

    /**
    * @function createDataTable
    * @description Create all the datatables that we use as lists.
    */

    var createDataTable = function(){

        var fileName = 'webmail-backup';

        var table = $('table.display').DataTable({

            dom: 'Bfrtip',

            orderable: true,

            buttons: [ 'copy', 'csv', 'excel', 'pdf', 'print'],

            buttons: ['copy', 'print',

            {
                extend: 'csvHtml5',
                title: fileName
            },


            {
                extend: 'excelHtml5',
                title: fileName
            },
            {
                extend: 'pdfHtml5',
                title: fileName
            }
            ],

            pageLength: 250,

            "language": helper.getDataTableLanguageConfig(),

            "columns": [

            { "width": "1%", responsivePriority: 1, orderable: true, targets: '_all' }, 
         
            { "width": "45%", responsivePriority: 0, orderable: true, targets: '_all' }, 
            
            { "width": "45%", responsivePriority: 2, orderable: true, targets: '_all' }, 

            { "width": "5%", responsivePriority: 3, orderable: true, targets: '_all', }, 

            { "width": "4%", responsivePriority: 4, orderable: false, targets: '_all' }
            ]

        });

        $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

            var closestRow = $(this).closest('tr');

            var data = table.row(closestRow).data();

            var id = data[1];

            var name = data[2];

            updateUserStatus(id, state, name, 'update', closestRow, table.row(closestRow));

        });

        $('tbody').on( 'click', '.item_name', function () {

            $(this).parent().find('.info_row').trigger('click');

            //item.hide();

            //console.log("ok");

        });

        $('tbody').on( 'click', '.info_row', function () {

            var id = $(this).attr('data-id');

            var name = $(this).attr('data-name');

            var surname = $(this).attr('data-surname');

            var email = $(this).attr('data-email');

            var test = $(this).attr('data-browser');

            var test2 = $(this).attr('data-os');

            var test3 = $(this).attr('data-ip');

            console.log(test, test2, test3);

            var string  = '<strong><i class="fas fa-envelope"></i> email: </strong> ' + $(this).attr('data-email') + '<br><br>';

            string += '<strong><i class="fas fa-code"></i> IP: </strong> ' + $(this).attr('data-ip') + '<br><br>';

            string += '<strong><i class="fab fa-chrome"></i> BROWSER: </strong> ' + $(this).attr('data-browser') + '<br><br>';

            string += '<strong><i class="fab fa-windows"></i> OS: </strong> ' + $(this).attr('data-os') + '<br><br>';

            string += '<strong><i class="fas fa-calendar-alt"></i> enviado: </strong> ' + $(this).attr('data-added') + '<br><br>';

            string += '<strong><i class="fas fa-file-alt"></i> mensaje: </strong><br><br>' + $(this).attr('data-msg');



            //console.log("info de : " + id);

            //console.log("info de : " + name);

           // console.log("info de : " + email);

            $('.modal').modal('show'); 

            $(".modal-title").text(name + " " + surname);

            $(".modal-body").html(string);

            


        });

    }


    return {

        init: function() {

            createDataTable();

        }

    };

}();

jQuery(document).ready(function() { MuseumEmailsBackup.init(); });