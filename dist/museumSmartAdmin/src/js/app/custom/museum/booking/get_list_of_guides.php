<?php

require_once '../core/init.php';

$db = DB::getInstance();

$status = [];

$query = 'SELECT * FROM museuem_guides WHERE active = ? AND deleted = ? ORDER BY id ASC';

$query = $db->query($query, [1, 0]);

if($query){

	foreach ($db->results() as $result){

		$status[] = array(

			$result->id => $result->name

			/*'title' => $result->title,

			'caption' => $result->caption,

			'link' => $result->link,

			'button' =>$result->button_label,

			'is_blank' =>$result->is_blank,

			'type' =>$result->type,

			'background_color' =>$result->background_color,

			'text_color' =>$result->text_color*/

		);

	}

}

header('Content-Type: application/json');

$json =  json_encode($status, JSON_PRETTY_PRINT);
 
 print_r($json);
?>