/**
* @summary booking list!
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/

/**
* @function MuseumBookingEventList
* @description Initialize and include all the methods of this class.
*/

var MuseumBookingEventList = function() {

    

    helper = new Helper();

    //var host = window.location.host;

     /**

     * @function addListeners
     * @description Asign all the listeners / event habdlers for this page.
     */

     var addListeners = function(){

        $("[name='my-checkbox']").bootstrapSwitch();

        $("[name='my-payment-checkbox']").bootstrapSwitch();

        $(window).focus(function() {

            var refresh = localStorage.getItem('refresh');

            if(refresh == 'true'){

                localStorage.removeItem('refresh');

                location.reload();

            } 

        });

        $(window).blur(function() { localStorage.removeItem('refresh'); });


    }


/**
* @function createDataTable
* @description Create all the datatables that we use as lists.
*/

var createDataTable = function(){

    var fileName = $('#booking-name').val();

    var table = $('table.display').DataTable({

        dom: 'Bfrtip',

        buttons: [ 'copy', 'csv', 'excel', 'pdf', 'print'],

        buttons: ['copy', 'print',

        {
            extend: 'csvHtml5',
            title: fileName
        },


        {
            extend: 'excelHtml5',
            title: fileName
        },
        {
            extend: 'pdfHtml5',
            title: fileName
        }
        ],

        pageLength: 50,

        "language": helper.getDataTableLanguageConfig(),

        bAutoWidth: false, 

        processing:true,

        "columns": [

        { "width": "5", responsivePriority: 4, orderable: true, targets: 0}, 

        { "visible": false, orderable:false },

        { "width": "85%", responsivePriority: 0, orderable: false, targets: '_all' }, 
        
        { "width": "85%", responsivePriority: 5, orderable: false, targets: '_all' }, 

        { "width": "85%", responsivePriority: 3, orderable: false, targets: '_all' }, 

        { "width": "5%", responsivePriority: 4, orderable: false, targets: '_all', }, 
        
        { "width": "5%", responsivePriority: 1, orderable: false, targets: '_all', }, 

        { "width": "5%", responsivePriority: 2, orderable: false, targets: '_all' }
        ]

    });

     $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

        var closestRow = $(this).closest('tr');

        var data = table.row(closestRow).data();

        var id = data[1];

        var name = data[2];

        updateUserStatus(id, state, name, 'update', closestRow, table.row(closestRow), null, null, 'attended', 'attended');


    });

    /*$('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

        var closestRow = $(this).closest('tr');

        var data = table.row(closestRow).data();

        var id = data[1];

        var name = data[2];

        updateUserStatus(id, state, name, 'update', closestRow, table.row(closestRow), null, null, 'active');

    });*/



    

    $('tbody').on( 'click', '.aditional_data', function () {

        var row = table.row($(this).parents('tr'));

        var animatable = $(this).parents('tr');

        var data = table.row( $(this).parents('tr') ).data();

        var order = data[0];

        var id = data[1];

        var name = data[2];

        var tickets = data[3];

        var email = data[4];

        var phone = data[5];

        var paymentMethod = data[5];

        var paymentDone = ($(this).attr('data-payment-done') == 0) ? 'no' : 'si';

        var dni = $(this).attr('data-dni');

        var reason = $(this).attr('data-reason');

        var origin = $(this).attr('data-origin');

        var how = $(this).attr('data-how');

        var os = $(this).attr('data-os');

        var browser = $(this).attr('data-browser');

        var ip = $(this).attr('data-ip');

        var added = $(this).attr('data-added');

        var finalString = '<b>INFORMACIÓN PERSONAL: </b><br><br>';        

        finalString += '<strong>Nombre: </strong>' + name + '<br>';
        
        finalString += '<strong>email: </strong>' + email + '<br>';

        finalString += '<strong>teléfono: </strong>' + phone + '<br>';

        finalString += '<strong>tickets solicitados: </strong>' + tickets + '<br>';

        finalString += '<strong>suscripto desde: </strong>' + origin + '<br>';

        finalString += '<strong>DNI: </strong>' + dni + '<br>';

        finalString += '<strong>motivo de la visita: </strong>' + reason + '<br>';

        finalString += '<strong>¿Cómo se enteró del museo?: </strong>' + how + '<br>';

        finalString+= '<br><b>INFO DE SISTEMA</b><br><br>';

        finalString += '<strong>Sistema Operativo: </strong>' + os + '<br>';

        finalString += '<strong>Browser: </strong>' + browser + '<br>';

        finalString += '<strong>IP: </strong>' + ip + '<br>';

        finalString += '<strong>fecha de solicitud: </strong>' + added + '<br>';

        $('.modal').modal('show'); 

        $('.modal-title').text(name);

        $('.modal-body').html(finalString);


    });

    //


    $('tbody').on( 'click', '.tickets_manager', function () {

        var bookingId = $(this).attr('data-booking-id');

        console.log("booking id " + bookingId);

        var tickets = $(this).attr('data-tickets');

        var name = $(this).attr('data-name');
        
        var id = $(this).attr('data-id');

        var actual = tickets;

        var content = 'Este slider te permite definir la cantidad de tickets que solicitó <b>' + name + '</b>. Actualmente' + name + ' cuenta con <b>' + tickets + ' tickets.</b><br><br> Si solicitó más tickets o devolver tickets, puedes controrlarlo desde acá. Utilizá el slider para reasignar la cantidad de tickets disponibles para el visitante ' + name;

        var max = parseInt(20) - parseInt(10);

        var min = 1;

        var actual = parseInt(tickets);

        Swal.fire({

            title: 'Cambiar la cantidad de tickets por visitante',

            html: content,

            input: 'range',

            inputAttributes: {

                min: min,

                max: max,

                step: 1

            },

            inputValue: actual

        }).then((result) => {

            console.log("ok");

            console.log(result);

            console.log(result.value);

            console.log(actual);    

            if(result.value != actual && result.value != undefined){

                console.log("sending!");

                var request = $.ajax({

                url: "private/users/museum/booking/add_tickets_to_visitor.php",

                type: "POST",

                data: { id: id, tickets_per_person: result.value, bookingId:bookingId},

                dataType: "json",

                success: function(result) {

                    helper.showToastr(result.title, result.msg, result.alert);

                    helper.unblockStage();

                    location.reload();
                },

                error: function(request, error) {

                    console.log(request);

                    helper.unblockStage();

                }

            });
                

            }

            /*helper.unblockStage();

            var request = $.ajax({

                url: "private/users/museum/booking/event_update_max_tickets_per_visitor.php",

                type: "POST",

                data: { id: id, tickets_per_visitor: result.value},

                dataType: "json",

                success: function(result) {

                    helper.showToastr(result.title, result.msg, result.alert);

                    helper.unblockStage();

                    resetCalendar();
                },

                error: function(request, error) {

                    console.log(request);

                    helper.unblockStage();

                }

            });*/

        })


    });

    $('tbody').on( 'click', '.delete_row', function () {

        var row = table.row($(this).parents('tr'));

        var animatable = $(this).parents('tr');

        var data = table.row( $(this).parents('tr') ).data();

        var order = data[0];

        var id = data[1];

        var name = data[2];

        var actualTable = $("#tab-1").DataTable();

        var tickets = $(this).attr('data-tickets');

        var date = $(this).attr('data-date');

        var bookingId = $(this).attr('data-booking-id');

        console.log("booking id " + bookingId);

        var inputValue = name + ", te enviamos este correo para avisarte que fuiste dado de baja de la visita guiada del día " + date + " del Museo Del Holocausto. Recordá que si querés volver a solicitar un turno, lo podés hacer desde la siguiente url: https://museodelholocausto.org.ar/visitas/";

        Swal({

            input: 'textarea',

            inputValue: inputValue,

            title: "Ops!! ¿Eliminar Inscripto?",

            html: 'Esta acción eliminará a <b>'+name+'</b>. Esta acción no se puede deshacer.<br><br>¿Eliminar de todos modos? Esto dejará <b>' + tickets + ' ticket/s</b> disponibles para la fecha.<br><br>Este es el mensaje ' + name + ' completá el campo a continuación. Caso contrario, dejalo vacío.',

            type: 'error',

            showCancelButton: true,

            confirmButtonText: 'Si, eliminar',

            cancelButtonText: 'no, salir'

        }).then((result) => {

            if(result.value != undefined) {

                updateUserStatus(id, 'true', name, 'delete', animatable, row, actualTable, null,  'deleted');

                deleteUser(tickets, bookingId, result.value, id);

            }else{

            }

        })

    });

}

var deleteUser = function(tickets, bookingId, inputValue, userId){

    console.log(tickets, bookingId, inputValue, userId);

    //return;

    $.ajax({

        type: "POST",
        
        url: "private/users/museum/booking/delete_user.php",
        
        data: {tickets:tickets, bookingId:bookingId, inputValue:inputValue, userId:userId},

        success: function(result){

            console.log(result);

        },

        error: function(xhr, status, error) {

            console.log(xhr.responseText);

            var err = eval("(" + xhr.responseText + ")");

            console.log(err);

        },

        dataType: "json"
    });


}

/**
* @function updateOrder
* @description Update / sort the order of the items inside the list.
*
* @param {Object[]} usersToSort             - Array with objects. Each object has the info for the sort.
* @param {int} usersToSort[].id             - Object id.
* @param {int} usersToSort[].oldData        - The old position of the object.
* @param {int} usersToSort[].newData        - The new position of the object.
* @param {string} usersToSort[].name        - The name of the sorted person. 
*
* @param {string} selectedName              - The actual person.
* @param {int} setOld                       - The old position of the actual person.
* @param {int} setNew                       - The new position of the actual person.
*/

var updateOrder = function(usersToSort, selectedId, selectedName, setOld, setNew){

    helper.blockStage('Actualizando...');

    $.ajax({

        type: "POST",
        
        url: "private/users/museum/general/update_order.php",
        
        data: {array:usersToSort, table:'museum_faq', selectedId:selectedId, source:'faq'},

        success: function(result){

            var st1 = result.msg.replace('%name%', '<b>' + selectedName + '</b>');

            var st2 = st1.replace('%oldPosition%', '<b>' + setOld + '</b>');

            var st3 = st2.replace('%newPosition%', '<b>' + setNew + '</b>');
            
            helper.unblockStage();

            helper.showToastr(result.title, st3);

        },

        error: function(xhr, status, error) {

            console.log(xhr.responseText);



            var err = eval("(" + xhr.responseText + ")");

            console.log(err);

        },

        dataType: "json"
    });

}

/**
* @function updateUserStatus
*
* @description Update / Delete user status from list: we use this to mark the user as online / offline and
* for delete an user completely from the list.
*
* @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
* @param {boolean} state                    - True or false: use it for both, update and delete.
* @param {string} name                  - The name of the person being manipulated.
* @param {string} action                    - Posibilities: 'update' or 'delete'. 
* @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
* @param {DataTable} row                    - Reference to the row that we are dealing with.
* @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
* @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
*/

var updateUserStatus = function(id, state, name, action, animatable, row, processTable = null, order = null, defaultColumn = null, source = 'delete_booking_member'){

    helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando visitante...');

    var url = "private/users/museum/general/update_status.php"; 

    $.ajax({

        type: "POST",

        url: url,

        data: {id: id, status:state, action:action, name:name, source: source,

        table:'museum_booking_assistants', defaultColumn:defaultColumn},

        success: function(result){

            console.log(result);

            helper.unblockStage();

            if(action == 'update'){

                var newStatus = (result.changeStatus == 'true') ? 'asistió' : 'no asistió';

                var value = 'Cambiaste la asistencia de  ' + name + ' a: ' + newStatus;

                helper.showToastr('ASISTENCIA', value);

            }else{

                animatable.fadeOut('slow','linear',function(){

                    console.log(result.msg);

                    helper.showToastr('ELIMINADO', 'Eliminaste a ' + name + ' de la visita guiada.');

                    var count = 0;

                    var data = processTable.rows().data();

                    processTable.rows().every( function (rowIdx, tableLoop, rowLoop) {

                        if(processTable.cell(rowIdx, 0).data() > Number(order)){

                            var saveValue = Number(processTable.cell(rowIdx, 0).data() - 1);

                            processTable.cell(rowIdx, 0).data(saveValue);

                        }

                    });

                    row.remove().draw(true);

                    processTable.rows().invalidate().draw(false);

                });

            }

        },

        error: function(xhr, status, error) {

            console.log(xhr);

            var err = eval("(" + xhr.responseText + ")");

            alert(err.Message);
        },

        dataType: "json"
    });

}

/**
* @function animateAddedElement.
* @description animate the last object when we come from editing.
*/

var animateAddedElement = function(){

    TweenMax.to(window, 1, {scrollTo:{y:"max"}, delay:1});

    var y = document.getElementsByClassName('row-1');

    var n = $(y).css("backgroundColor");

    var c = $(y).css("color");

    TweenMax.to(y, 1, {backgroundColor:"rgba(132,173,169,0.37)", color: '#1c9081', ease:Cubic.easeOut, delay:2});

    TweenMax.to(y, 1, {backgroundColor:n, color:c, ease:Cubic.easeOut, delay:.5, delay:3});

}

return {

    init: function() {

        addListeners();

        createDataTable();

        var highlight = (helper.getGETVariables(window.location, "highlight") == "true") ? true : false;
        
        if(highlight == true) animateAddedElement();

    }

};

}();

jQuery(document).ready(function() { MuseumBookingEventList.init(); });