/**
 * @summary historical! 
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
 * @function MuseumBookingList
 * @description Initialize and include all the methods of this class.
 */
 var MuseumBookingList = function() {

    helper = new Helper();

    var readableDateFormat = "dd-mm-yyyy";

    var saveId;

    var finalStartDate;

    var finalEndDate;

    var startDate;

    var endDate;

    var date;


    /**
     * @function createDataTable
     * @description Create all the datatables that we use as lists.
     */

     var createDataTable = function(value, bookingType) {

        console.log(">> " + bookingType);

        var table = value.DataTable({

            "language": helper.getDataTableLanguageConfig(),

            "processing": true,

            "serverSide": true,

            'serverMethod': 'post',

            "ajax": {

                url: "private/users/museum/booking/dataControllerGroups.php",

                dataSrc: "data",

                dataType: 'json',

                "data": function(outData) {

                    outData.table = "museum_booking_institutions";

                    outData.searchColumn = "institution_name";

                    outData.searchColumnTwo = "institution_email";

                    outData.date = date;
                    
                    outData.type = bookingType;

                    return outData;
                },

                dataFilter: function(inData) { return inData; },

                error: function(err, status) {},
            },


            "columns": [

            { orderable: false, width: "8%", responsivePriority: 2,

            "defaultContent": '',

            render: function(data, type, row) {

                var st;

                if(bookingType == 1){

                    if(row.institution_name == null){

                        st = "<p data-toggle='tooltip' data-placement='bottom' title='Este turno aún no fue reservado por ninguna institución'>libre</p>";

                    }else{

                        st = "<p data-toggle='tooltip' data-placement='bottom' title='Este turno ya fue reservado por la institución " + row.institution_name + ".'>" + row.institution_name + "</p>";

                    }

               }else if(bookingType ==2 || bookingType == 4 || bookingType == 5){ 

                    var tooltip = " data-toggle='tooltip' data-placement='bottom' title='hay " +  row.booked_places + " lugares reservados de un total de " + row.available_places + " lugares. Aún quedan " + (row.available_places - row.booked_places) + " lugares disponibles'";

                    st = "<p " + tooltip + ">" + row.booked_places + " ocupados de " + row.available_places + "</p>";

                }else if(bookingType == 3){

                    st = (row.notes == null) ? "" : "<p data-toggle='tooltip' data-placement='bottom' title='" + row.notes  + "'>" + row.notes.substr(0, 40) + "...</p>";
                }

                return st;
            }
        },

        {
            "defaultContent": '',

            render: function(data, type, row) {

                var f = moment(row.date_start).format('DD-MM-YYYY');

                var today = moment(new Date()).format('DD-MM-YYYY');

                var st;

                if(f == today){

                    st = 'hoy!';

                }else{

                    st = f;
                }

                return st;
            }
        },

        {
            "defaultContent": '',

            render: function(data, type, row) {

                var start = moment(row.date_start).format('HH:mm');
                
                var end = moment(row.date_end).format('HH:mm');

                var f = "<p data-toggle='tooltip' data-placement='bottom' title='horario de comienzo: " + start + " /  horario de fin: " + end + "'>" + start + "</p>";

                return f;
            }
        },

        {
            "defaultContent": '',

            render: function(data, type, row) {

                 var guide;

                if(bookingType != 2){

                   
                    var tooltip = "data-toggle='tooltip' data-placement='bottom' title='hacé click para cambiarlo o asignarlo.'";

                    if(row.name == null && row.surname == null){

                        guide = "<a href='' class='set_guide' " + tooltip + ">asignar</a>";

                    }else{

                        guide = "<a href='' class='set_guide' " + tooltip + ">" + row.name + " " + row.surname; + "</a>";

                    }

                }else{

                    guide = "recorrido libre";

                }


                return guide;
            }
        },


        {

            orderable: false, width: "8%", responsivePriority: 1,

            "defaultContent": '',

            render: function(data, type, row) {

                console.log(row);

                var renderString = '<td><span>';

                if(bookingType == 1){

                    console.log(row.institution_name);

                    var tooltipAboutInstitution;

                    var tooltipAboutPerson;

                    if(row.institution_name != null){

                        tooltipAboutInstitution = "data-toggle='tooltip' data-placement='bottom' title='hacé click en el botón para acceder a más info de la institución " + row.institution_name  + "'";
                        
                        tooltipAboutPerson = "data-toggle='tooltip' data-placement='bottom' title='hacé click en el botón para acceder a más info de "  + row.inscription_contact_name + ", la persona a cargo de la institución " + row.institution_name  + "'";

                    }else{

                        tooltipAboutInstitution = "";

                        tooltipAboutPerson = "";
                    }

                     
                    renderString += '<button ' + tooltipAboutInstitution + ' class="info_institution_data m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-university"></i></button>';

                    renderString += '<button ' + tooltipAboutPerson +  ' class="info_contact_person m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-id-card-alt"></i></button>';

                }

                var stickyNoteClass = (row.notes == null || row.notes == "") ? "m-btn--hover-warning" : "btn-warning";
                
                var rollOverNote = (row.notes == null || row.notes == "") ? "Usá esta herramienta para dejarte notitas, reminders, post-its, sticky notes :)" : row.notes;

                renderString += '<button data-toggle="tooltip" data-placement="bottom" title="¿eliminar turno?" class="delete_row m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill "><i class="fas fa-trash-alt"></i></button>';
                
                renderString += '<button data-toggle="tooltip" data-placement="bottom" title="ver inscriptos" class="see_list m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-list-alt"></i></button>';

                //renderString += '<button data-toggle="tooltip" data-placement="bottom" title="Usá esta herramienta para dejarte notitas, reminders, post-its, sticky notes :)" class="sticky-note m-portlet__nav-link btn m-btn  m-btn--icon m-btn--icon-only m-btn--pill '+stickyNoteClass+'" title="notas"><i class="fas fa-file-alt"></i></button>';

                renderString += '<button data-toggle="tooltip" data-placement="bottom" title="' + rollOverNote + '" class="sticky-note m-portlet__nav-link btn m-btn m-btn--icon m-btn--icon-only m-btn--pill '+stickyNoteClass+'" title="notas"><i class="fas fa-file-alt"></i></button>';

                if(bookingType == 1 || bookingType == 3){

                    renderString += '<button data-toggle="tooltip" data-placement="bottom" title="Cambiá o asigná al guía de este turno" class="set_guide m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="guia"><i class="fas fa-user-shield"></i></button>';

                }

                
                renderString += '<button data-toggle="tooltip" data-placement="bottom" title="Cambiá la hora de comienzo o la hora de fin del turno" class="change_time m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="guia"><i class="far fa-clock"></i></button>';

                if(bookingType == 2 || bookingType == 4 || bookingType == 5) {

                    renderString += '<button data-toggle="tooltip" data-placement="bottom" title="Definí la máxima cantidad de tickets para este turno." class="define_ammount_of_tickets m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="guia"><i class="fas fa-ticket-alt"></i></button>';
                }

               

                if(bookingType == 5){

                    renderString += '<button data-toggle="tooltip" data-placement="bottom" title="accedé a la lista de inscriptos" class="see_list m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="inscriptos"><i class="fas fa-list"></i></button>';

                }

                renderString += '</td></span>';

                return renderString;

            }
        }

        ],

        rowCallback: function(row, data) {

                //...

                $('.define_ammount_of_tickets', row).attr('data-booking-id', data.museum_booking_id);

                //...

                $('.change_time', row).attr('data-booking-id', data.museum_booking_id);
                
                $('.change_time', row).attr('data-date-start', data.date_start);

                $('.change_time', row).attr('data-date-end', data.date_end);

                //...

                $('.set_guide', row).attr('data-booking-id', data.museum_booking_id);

                //...

                $('.open-link', row).attr('data-url', data.url);

                $('.sticky-note', row).attr('notes', data.notes);

                $('.sticky-note', row).attr('data-booking-id', data.museum_booking_id);

                //...

                $('.delete_row', row).attr('data-booking-id', data.museum_booking_id);

                $('.delete_row', row).attr('data-name', data.institution_name);

                $('.delete_row', row).attr('data-cuit', data.institution_cuit);
                
                $('.delete_row', row).attr('data-guide-name', data.name);

                $('.delete_row', row).attr('data-guide-surname', data.surname);

                //...

                $('.see_list', row).attr('data-booking-id', data.museum_booking_id);


                //...

                $('.schedulle', row).prop('title', 'Conocé los turnos asignados a la institución ' + data.institution_name);

                //...

                $('.edit_data', row).prop('title', 'Editar información de ' + data.institution_name);

                //...

                //...

                $('.info_institution_data', row).attr('data-name', data.institution_name);

                $('.info_institution_data', row).attr('data-cuit', data.institution_cuit);

                $('.info_institution_data', row).attr('data-iva', data.iva);

                $('.info_institution_data', row).attr('data-address', data.institution_address);

                $('.info_institution_data', row).attr('data-phone', data.institution_phone);

                $('.info_institution_data', row).attr('data-email', data.institution_email);

                $('.info_institution_data', row).attr('data-state', data.institution_state);

                $('.info_institution_data', row).attr('data-level', data.eduction_level);

                $('.info_institution_data', row).attr('data-grade', data.education_grade);

                $('.info_institution_data', row).attr('data-type', data.institution_type);

                $('.info_institution_data', row).attr('data-needs', data.special_need);

                //...

                $('.info_contact_person', row).attr('data-contact-name', data.inscription_contact_name);

                $('.info_contact_person', row).attr('data-contact-email', data.inscription_contact_email);

                $('.info_contact_person', row).attr('data-contact-phone', data.inscription_contact_phone);

                $('.info_contact_person', row).attr('data-first-time', data.first_time);

                $('.info_contact_person', row).attr('data-first-time', data.first_time);

                $('.info_contact_person', row).attr('data-institution-first-time', data.institution_first_time);

                $('.info_contact_person', row).attr('data-students', data.institution_students);

                $('.info_contact_person', row).attr('data-origin', data.origin);

                $('.edit_data', row).attr('data-id', data.id);

            },

            "fnDrawCallback": function(settings) {

                 var hash = window.location.hash;
        
                $('#myTab a[href="' + hash + '"]').tab('show');




                $("[name='my-scholarship-checkbox']").bootstrapSwitch();

                $("[name='my-payment-checkbox']").bootstrapSwitch();

                $("[name='my-upload-checkbox']").bootstrapSwitch();

                $("[name='my-assisted-checkbox']").bootstrapSwitch();

                $('[data-toggle="tooltip"]').tooltip();

                $('body').on('click', '.define_ammount_of_tickets', function(event) { 

                    var id = $(this).attr("data-booking-id");

                    helper.blockStage("cargando cantidad...");

                    var request = $.ajax({

                        url: "private/users/museum/booking/event_get_max_visitors.php",

                        type: "POST",

                        data: { id: id} ,

                        dataType: "json",

                        success: function(result) {

                            console.log(result.available_places, result.booked_places);

                            helper.unblockStage();

                            var max = 100;

                            var actual = result.available_places;

                            var content = 'Desde este slider podés modificar la cantidad de visitantes para este turno';

                            var min;

                            if(result.booked_places != 0){

                                min = result.booked_places;

                                content += "<br><br><strong>IMPORTANTE: </strong> Este evento ya tiene inscriptos."; 

                            }else{

                                min = 5;

                            }


                            Swal.fire({

                                title: 'Cambiar la cantidad de visitantes',

                                html: content,

                                input: 'range',

                                inputAttributes: {

                                    min: min,

                                    max: 100,

                                    step: 1

                                },

                                inputValue: actual

                            }).then((result) => {

                                helper.unblockStage();

                                var request = $.ajax({

                                    url: "private/users/museum/booking/event_update_max_visitors.php",

                                    type: "POST",

                                    data: { id: id, visitors: result.value},

                                    dataType: "json",

                                    success: function(result) {

                                        helper.showToastr(result.title, result.msg, result.alert);

                                        helper.unblockStage();
                                    },

                                    error: function(request, error) {

                                        console.log(request);

                                        helper.unblockStage();

                                    }

                                });

                            })

                        },

                        error: function(request, error) {

                        }

                    });

                });

                $('body').on('click', '.change_time', function(event) {

                    console.log(`cambiar fecha!!`);

                    saveId = $(this).attr("data-booking-id");

                    var startTime = $(this).attr("data-date-start");

                    var endTime = $(this).attr("data-date-end");

                    startDate = moment(startTime).format('DD-MM-YYYY');

                    endDate = moment(endTime).format('DD-MM-YYYY');

                    //Datepicker

                    finalString = '<form name="change-date" id="change-date">';

                    finalString += '<div class="form-group m-form__group form-group m-form__group col-12">';

                    finalString  += '<label class="col-form-label">Desde el:<br></label> <div class="col-12">';

                    finalString  += '<div class="input-group timpeicker" >';

                    finalString  += '<input type="text" id="date-from" class="form-control m-input" readonly="" placeholder="seleccionar fecha de comienzo">';

                    finalString  += '<span class="input-group-addon"> <i class="far fa-calendar-alt mt-2"></i> </span></div></div></div>';

                    finalString += '<div class="form-group m-form__group form-group m-form__group col-12">';

                    finalString  += '<label class="col-form-label">Hasta el:<br></label> <div class="col-12">';

                    finalString  += '<div class="input-group timepicker" >';
                    
                    finalString  += '<input type="text" id="date-to" class="form-control m-input" readonly="" placeholder="seleccionar fecha de finalización">';

                    finalString  += '<span class="input-group-addon"> <i class="far fa-calendar-alt mt-2"></i> </span></div></div></div>';


                    //Timepicker

                    finalString += '<div class="form-group m-form__group form-group m-form__group col-12">';

                    finalString  += '<label class="col-form-label">Desde las:<br></label> <div class="col-12">';

                    finalString  += '<div class="input-group timepicker" >';
                    
                    finalString  += '<input type="text" id="from-time" class="form-control m-input" readonly="" placeholder="seleccionar horario de comienzo">';

                    finalString  += '<span class="input-group-addon"> <i class="la la-clock-o mt-2"></i> </span></div></div></div>';

                    finalString += '<div class="form-group m-form__group form-group m-form__group col-12">';

                    finalString  += '<label class="col-form-label">Desde las:<br></label> <div class="col-12">';

                    finalString  += '<div class="input-group timepicker" >';
                    
                    finalString  += '<input type="text" id="to-time" class="form-control m-input" readonly="" placeholder="seleccionar horario de comienzo">';

                    finalString  += '<span class="input-group-addon"> <i class="la la-clock-o mt-2"></i> </span></div></div></div>';

                    finalString += '</form>';

                    $('.modal').modal('show');

                    $('.modal-title').text('Cambiar la fecha / hora');

                    $('.modal-body').html(finalString);
                    

                    var today = new Date();

                    $("#date-from").val(startDate);

                    $("#date-to").val(endDate);


                    $('#date-from').datepicker({startDate: today, orientation: 'bottom', autoclose: true, language: 'es',  format: readableDateFormat});

                    $('#date-to').datepicker({ autoclose: true, language: 'es',  format: readableDateFormat});


                    $('#date-from').datepicker().on('changeDate', function (ev) {

                        var dateFrom = $('#date-from').data('datepicker').getFormattedDate('yyyy-mm-dd');
            
                        var mydate = new Date(dateFrom);
            
                        //mydate.setDate(mydate.getDate() + 2);
                        mydate.setDate(mydate.getDate() + 1);
            
                        $('#date-to').datepicker('destroy');
            
                        $('#date-to').datepicker({startDate: mydate, autoclose: true, language: 'es',  format: readableDateFormat});
            
                    });



                    $("#from-time").val(moment(startTime).format('HH:mm'));
                    
                    $("#to-time").val(moment(endTime).format('HH:mm'));

                    $("#from-time, #to-time").timepicker({minuteStep: 15, showMeridian: false, snapToStep: true});

                    $('#from-time').timepicker().on('changeTime.timepicker', function (e) {

                        /*
                        var newhour =(e.time.hours) + 1;

                        var newTime = newhour + ':'+ e.time.minutes;

                        $("#to-time").val(newTime);
                        */



                        
                        
                    });

                    $('#to-time').timepicker().on('changeTime.timepicker', function (e) {

                        date = moment(new Date()).format('YYYY-MM-DD');


                        var to = date + " " + e.time.hours + ":" + e.time.minutes;

                        var from = date + " " + $("#from-time").val();

                        /*
                        if (to <= from) {

                            var newhour =(e.time.hours) - 1;

                            var newTime = newhour + ':'+ e.time.minutes;

                            $("#from-time").val(newTime);

                        }
                        */

                        


                    });

                    
                    finalStartDate = startDate + " " + $("#from-time").val() + ":00";

                    finalEndDate = endDate + " " + $("#to-time").val() + ":00";
                    

                });


                $('body').on('click', '.set_guide', function(event) {

                    var id = $(this).attr("data-booking-id");

                    helper.blockStage("cargando lista de guías...");

                    var request = $.ajax({

                        url: "private/users/museum/booking/get_list_of_guides.php",

                        data: { id: id},

                        type: "POST",

                        dataType: "json",

                        success: function(result) {

                            helper.unblockStage();

                            var actual = result.selected;

                            Swal({

                                title: "Seleccionar guía",

                                input: 'select',

                                inputOptions: result.items,

                                inputValue: result.selected,

                                html: 'Selecciona un guia de ela lista. El mismo quedará asignado al turno.',

                                type: 'success',

                                showCancelButton: true,

                                confirmButtonText: 'Asignar',

                                cancelButtonText: 'salir'

                            }).then((result) => {

                                if(result.value != actual){

                                    var request = $.ajax({

                                        url: "private/users/museum/booking/event_update_guide.php",

                                        data: { id_booking: id, id_guide:result.value},

                                        type: "POST",

                                        dataType: "json",

                                        success: function(result) {

                                            helper.showToastr(result.title, result.msg, result.alert);

                                            helper.unblockStage();

                                            window.location.reload();

                                        },

                                        error: function(request, error) {


                                        }

                                    })

                                }

                            })

                        },

                        error: function(request, error) {

                            helper.unblockStage();

                        }

                    });


                });


                $('tbody').on('click', '.sticky-note', function() {

                    var item = $(this);

                    var notes = $(this).attr('notes');

                    if(notes == undefined || notes == "undefined") notes = '...';

                    var id =  $(this).attr('data-booking-id');

                    Swal.fire({

                        title : 'Asignar notas.',

                        html: 'Podes utilizar este espacio para crear notas relativas al evento.',

                        input: 'textarea',

                        inputValue: notes,
                        
                        inputPlaceholder: 'Escribir notas',
                        
                        showCancelButton: true

                    }).then((queryResult) => {

                        helper.unblockStage();

                        var request = $.ajax({

                            url: "private/users/museum/booking/event_update_note.php",

                            type: "POST",

                            data: { id: id, notes: queryResult.value},

                            dataType: "json",

                            success: function(result) {

                                helper.showToastr(result.title, result.msg, result.alert);

                                helper.unblockStage();

                                item.attr('notes', queryResult.value);
                            },

                            error: function(request, error) {

                                helper.unblockStage();

                            }

                        });

                    })

                });


                $('tbody').on('click', '.info_contact_person', function() {

                    var contactName = $(this).attr('data-contact-name');

                    if(contactName == undefined){

                        showSwal('turno sin asignar', 'Este turno todavía no fue tomado por ninguna institución: no hay información para mostrar.', 'warning');

                    }else{

                        var contactPhone = $(this).attr('data-contact-phone');

                        var contactEmail = $(this).attr('data-contact-email');

                        var firstTime = $(this).attr('data-first-time');

                        var origin = $(this).attr('data-origin');

                        var institutionFirstTime = $(this).attr('data-institution-first-time');

                        var finalString = '<strong>Nombre: </strong>' + contactName + '<br>';

                        finalString += '<strong>Email: </strong>' + contactEmail + '<br>';

                        finalString += '<strong>Teléfono: </strong>' + contactPhone + '<br><br>';

                        finalString += '<strong>GENERALES  : </strong><br><br>';

                        finalString += '<strong>¿Es la primera vez que visitas la institución?: </strong>' + firstTime + '<br>';

                        finalString += '<strong>¿Es la primera vez que la institución donde trabajas visita el Museo?: </strong>' + institutionFirstTime + '<br>';

                        $('.modal').modal('show');

                        $('.modal-title').text('PERSONA DE CONTACTO');

                        $('.modal-body').html(finalString);

                    }

                });


                $('tbody').on('click', '.info_institution_data', function() {

                    var name = $(this).attr('data-name');

                    if(name == undefined){

                        showSwal('turno sin asignar', 'Este turno todavía no fue tomado por ninguna institución: no hay información para mostrar.', 'warning');

                    }else{

                        var students = $(this).attr('data-students');

                        var cuit = $(this).attr('data-cuit');

                        var iva = $(this).attr('data-iva');

                        var address = $(this).attr('data-address');

                        var email = $(this).attr('data-email');

                        var phone = $(this).attr('data-phone');

                        var state = $(this).attr('data-state');

                        var level = $(this).attr('data-level');

                        var grade = $(this).attr('data-grade');

                        var type = $(this).attr('data-type');

                        var needs = $(this).attr('data-needs');

                        var finalString = 'La institiución <strong>' + name + '</strong> solicitó turnos para <strong>' + students + ' alumnos</strong>. La institución fue dada de alta desde la/el <strong>' + origin + '</strong><br>';

                        finalString += '<br><b>IMPOSITIVO:</b><br><br>';

                        finalString += '<strong>Nombre: </strong>' + name + '<br>';

                        finalString += '<strong>CUIT: </strong>' + cuit + '<br>';

                        finalString += '<strong>Condición frente al IVA: </strong>' + iva + '<br><br>';

                        finalString += '<strong>INSTITUCIONAL: </strong><br><br>';

                        finalString += '<strong>dirección: </strong>' + address + '<br>';

                        finalString += '<strong>teléfono: </strong>' + phone + '<br>';

                        finalString += '<strong>email: </strong>' + email + '<br>';

                        finalString += '<strong>provincia: </strong>' + state + '<br>';

                        finalString += '<strong>nivel: </strong>' + level + '<br>';

                        finalString += '<strong>grado / año: </strong>' + grade + '<br>';

                        finalString += '<strong>tipo de institución: </strong>' + type + '<br>';

                        finalString += '<strong>¿Tiene necesidades especiales?: </strong>' + needs + '<br><br>';

                        $('.modal').modal('show');

                        $('.modal-title').text(name);

                        $('.modal-body').html(finalString);

                    }

                });

                $('tbody').on('click', '.edit_data', function() {

                    var itemId = $(this).attr('data-id');

                    //window.location.href = "museum_institution_add.php?id=" + itemId;

                });

                $('tbody').on('click', 'tr', function() {

                    $("[name='my-scholarship-checkbox']").bootstrapSwitch();

                    $("[name='my-payment-checkbox']").bootstrapSwitch();

                    $("[name='my-upload-checkbox']").bootstrapSwitch();

                    $("[name='my-assisted-checkbox']").bootstrapSwitch();

                });
            }
        });

table.draw();


$('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-assisted-checkbox"]', function(event, state) {

    var itemId = $(this).attr('data-id');

    var name = $(this).attr('data-name');

    updateUserStatus(itemId, state, name, 'update', 'assisted', 'institution_assisted');

});

$('tbody').on('click', '.see_list', function() {

    var id = $(this).attr('data-booking-id');

    window.location.href = "museum_booking_event_list.php?id=" + id;



});


$('tbody').on('click', '.delete_row', function() {

    var name = $(this).attr('data-name');

    var guideName = $(this).attr('data-guide-name');

    var guideSurname = $(this).attr('data-guide-surname');

    var id = $(this).attr('data-booking-id');

    var string;

    if(name == undefined){

        string = "Se eliminará el turno.";

    }else{

        string = "Atención!. Este turno ya fue asignado a la institución <b>" + name + "</b>. Si eliminas el tuno, <b>" + name + "</b> perderá el turno";

    }

    if(guideName != undefined && guideSurname != undefined){

        //string += "<br><br>Este turno fue asignado a <b>" + guideName + " " + guideSurname + "</b>. Le avisaremos que se eliminó el turno";

    }

    Swal({

        title: "¿Eliminar turno?",

        html: string,

        type: 'error',

        showCancelButton: true,

        confirmButtonText: 'Si, eliminar',

        cancelButtonText: 'no, salir'

    }).then((result) => {

        console.log(result);

        if (result.value != undefined) {

            console.log("????");

            updateValue(id, 'deleted', 1, 'museum_booking');

            //updateUserStatus(id, 'true', name, 'delete', animatable, row, actualTable, null, 'deleted');

            //deleteInstitution(id);

        } else {

        }

    })

});

}

    /**
    *
    * @function showErrorSwal
    * @param msg - the msg.
    * @description Swal alert.
    */

    var showSwal = function(title, msg, type = 'error'){

        Swal({

            title: title,

            html: msg,

            type: type,

            confirmButtonText: 'Entendido!'

        })
    }

    
    var deleteInstitution = function(id) {

        $.ajax({

            type: "POST",

            url: "private/users/museum/booking/delete_booking.php",

            data: {
                id: id
            },

            success: function(result) {


            },

            error: function(xhr, status, error) {


                var err = eval("(" + xhr.responseText + ")");

            },

            dataType: "json"
        });


    }

     /**
      * @function updateValue
      * @description update individual value in DB.
      *
      * @param {int} id                 - object id.
      * @param {string} fieldName       - the field to modify
      * @param {string} newValue        - object value.
      * @param {int} filter             - filter type.
      */

      var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

         helper.blockStage("actualizando...");

         var request = $.ajax({

             url: "private/users/museum/general/update_value.php",

             type: "POST",

             data: {

                id: id,

                fieldName: fieldName,

                newValue: newValue,

                filter: filter
            },

            dataType: "json"

        });

         request.done(function(result) {

             helper.showToastr(result.title, result.msg);

             helper.unblockStage();

             window.location.reload();

             if (callback != undefined) callback(callbackParams);

         });

         request.fail(function(jqXHR, textStatus) {

             helper.unblockStage();

         });
     }

    /**
     * @function updateUserStatus
     *
     * @description Update / Delete user status from list: we use this to mark the user as online / offline and
     * for delete an user completely from the list.
     *
     * @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
     * @param {boolean} state                    - True or false: use it for both, update and delete.
     * @param {string} name                  - The name of the person being manipulated.
     * @param {string} action                    - Posibilities: 'update' or 'delete'. 
     * @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
     * @param {DataTable} row                    - Reference to the row that we are dealing with.
     * @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
     * @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
     */

     var updateUserStatus = function(id, state, name, action, defaultColumn = null, source = 'delete_booking_member') {

        console.log("?!?!");

        console.log(id, state, name, action, defaultColumn);

        helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando visitante...');

        var url = "private/users/museum/general/update_status.php";

        $.ajax({

            type: "POST",

            url: url,

            data: {

                id: id,
                
                status: state,
                
                action: action,
                
                name: name,
                
                source: source,

                table: 'museum_booking',

                defaultColumn: defaultColumn
            },

            success: function(result) {

                helper.unblockStage();

                if (action == 'update') {

                    helper.showToastr(result.title, result.msg);

                } else {

                    animatable.fadeOut('slow', 'linear', function() {

                        helper.showToastr('ELIMINADO', 'Eliminaste a ' + name + ' de la visita guiada.');

                        var count = 0;

                        var data = processTable.rows().data();

                        processTable.rows().every(function(rowIdx, tableLoop, rowLoop) {

                            if (processTable.cell(rowIdx, 0).data() > Number(order)) {

                                var saveValue = Number(processTable.cell(rowIdx, 0).data() - 1);

                                processTable.cell(rowIdx, 0).data(saveValue);

                            }

                        });

                        row.remove().draw(true);

                        processTable.rows().invalidate().draw(false);

                    });

                }

            },

            error: function(xhr, status, error) {

                var err = eval("(" + xhr.responseText + ")");

                alert(err.Message);
            },

            dataType: "json"
        });

    }

    /**
     * @function findGetParameter
     * @description obtiene parametros...
     */

     var findGetParameter = function(parameterName) {

        var result = null,

        tmp = [];

        location.search.substr(1).split("&").forEach(function(item) {

            tmp = item.split("=");

            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);

        });

        return result;
    }

    /**
     * @function addListeners
     * @description ...
     */

     var addListeners = function(){

        $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {

            $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();

            e.preventDefault();

            var id = $(e.target).attr("href");

            history.pushState(null, null, id);

        });


        $('.save-description').click(function(){


            var formatFromDate = $("#date-from").val().split('-').reverse().join('-');
            
            var formatToDate = $("#date-to").val().split('-').reverse().join('-');

            
            
            finalStartDate = formatFromDate + " " + $("#from-time").val() + ":00";

            finalEndDate = formatToDate + " " + $("#to-time").val() + ":00";
            

            console.log(`finalStartDate ${finalStartDate} - finalEndDate: ${finalEndDate}`);


            if(formatFromDate == ""  || formatFromDate == null || formatFromDate == undefined || formatToDate == "" || formatToDate == null || formatToDate == undefined){


                Swal.fire({

                    title: 'No puede haber campos vacíos',

                    html: 'Verificá que todos los campos de este formulario esten completos',

                });

            }else{

               updateValue(saveId, ['date_start', 'date_end'], [finalStartDate, finalEndDate], 'museum_booking');

               $('.modal').modal('hide');

            }

        });

        $('#date-from').datepicker({orientation: 'bottom', autoclose: true, language: 'es',  format: readableDateFormat});

        $('#date-from').datepicker().on('changeDate', function (ev) {

            var dateFrom = $('#date-from').data('datepicker').getFormattedDate('yyyy-mm-dd');

            window.location.href = "museum_booking_list.php?date=" + dateFrom;

        });

        $('.go-to-today').click(function(){

            $finalURL = 'museum_booking_list.php?date=' + $('#today').val();

            window.location.href = $finalURL;

        });

        $('.go-to-tomorrow').click(function(){

            $finalURL = 'museum_booking_list.php?date=' + $('#tomorrow').val();

            window.location.href = $finalURL;

        });

        $('.go-to-day-after-tomorrow').click(function(){

            $finalURL = 'museum_booking_list.php?date=' + $('#dayAfterTomorrow').val();

            window.location.href = $finalURL;

        });
    }


    return {

        init: function() {

            date = findGetParameter('date');

            if(date == null) date = moment(new Date()).format('YYYY-MM-DD');

            addListeners();

            createDataTable($('#tab-1'), 1);

            createDataTable($('#tab-2'), 2);

            createDataTable($('#tab-3'), 3);

            createDataTable($('#tab-4'), 4);
            
            createDataTable($('#tab-5'), 5);

            //$('body').on('click', '.change_time'
            //$('.change_time').click();
        }

    };

}();

jQuery(document).ready(function() { MuseumBookingList.init(); });