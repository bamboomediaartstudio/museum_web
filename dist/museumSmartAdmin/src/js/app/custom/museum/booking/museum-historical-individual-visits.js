/**
* @summary historical! 
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/

/**
* @function MuseumHistoricalIndividualVisits
* @description Initialize and include all the methods of this class.
*/

var MuseumHistoricalIndividualVisits = function() {

    helper = new Helper();

    //var host = window.location.host;

     /**

     * @function addListeners
     * @description Asign all the listeners / event habdlers for this page.
     */

     var addListeners = function(){

        $("[name='my-checkbox']").bootstrapSwitch();

        $("[name='my-payment-checkbox']").bootstrapSwitch();

        $(window).focus(function() {

            var refresh = localStorage.getItem('refresh');

            if(refresh == 'true'){

                localStorage.removeItem('refresh');

                location.reload();

            } 

        });

        $(window).blur(function() { localStorage.removeItem('refresh'); });

    }


/**
* @function createDataTable
* @description Create all the datatables that we use as lists.
*/

var createDataTable = function(){

    var fileName = $('#historical-list').val();

    var table = $('table.display').DataTable({

        dom: 'Bfrtip',

        buttons: [ 'copy', 'csv', 'excel', 'pdf', 'print'],

        buttons: ['copy', 'print',

        {
            extend: 'csvHtml5',
            title: fileName
        },


        {
            extend: 'excelHtml5',
            title: fileName
        },
        {
            extend: 'pdfHtml5',
            title: fileName
        }
        ],

        pageLength: 50,

        "language": helper.getDataTableLanguageConfig(),

        bAutoWidth: false, 

        processing:true,

        "columns": [

        { "width": "5", responsivePriority: 4, orderable: true, targets: 0}, 

        { "visible": false, orderable:false },

        { "width": "85%", responsivePriority: 1, orderable: false, targets: '_all' }, 
        
        { "width": "85%", responsivePriority: 1, orderable: false, targets: '_all' }, 

        { "width": "85%", responsivePriority: 1, orderable: false, targets: '_all' }, 

        { "width": "5%", responsivePriority: 0, orderable: false, targets: '_all', }, 
        
        //{ "width": "5%", responsivePriority: 0, orderable: false, targets: '_all', }, 

        { "width": "5%", responsivePriority: 0, orderable: false, targets: '_all' }
        ]

    });

    $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-payment-checkbox"]', function(event, state) {

        var closestRow = $(this).closest('tr');

        var data = table.row(closestRow).data();

        var id = data[1];

        var name = data[2];

        //console.log(id, name);

        updateUserStatus(id, state, name, 'update', closestRow, table.row(closestRow), null, null, 'payment_done');


    });

    $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

        var closestRow = $(this).closest('tr');

        var data = table.row(closestRow).data();

        var id = data[1];

        var name = data[2];

        updateUserStatus(id, state, name, 'update', closestRow, table.row(closestRow), null, null, 'active');

    });



    

    $('tbody').on( 'click', '.aditional_data', function () {

        var row = table.row($(this).parents('tr'));

        var animatable = $(this).parents('tr');

        var data = table.row( $(this).parents('tr') ).data();

        var order = data[0];

        var id = data[1];

        var name = data[2];

        var tickets = data[3];

        var email = data[4];

        var phone = data[5];

        var paymentMethod = data[5];

        var paymentDone = ($(this).attr('data-payment-done') == 0) ? 'no' : 'si';

        var dni = $(this).attr('data-dni');

        var reason = $(this).attr('data-reason');

        var how = $(this).attr('data-how');

        var os = $(this).attr('data-os');

        var browser = $(this).attr('data-browser');

        var ip = $(this).attr('data-ip');

        var added = $(this).attr('data-added');

        var start = $(this).attr('data-start');

        var end = $(this).attr('data-end');

        var date = $(this).attr('data-date');

        var finalString = '<br><b>SOBRE LA VISITA:</b><br><br>';

        finalString += '<strong>fecha: </strong>' + date + '<br>';
        
        finalString += '<strong>horario de inicio: </strong>' + start + '<br>';

        finalString += '<strong>horario de fin: </strong>' + end + '<br><br>';

        finalString += '<b>INFORMACIÓN PERSONAL: </b><br><br>';        

        finalString += '<strong>Nombre: </strong>' + name + '<br>';
        
        finalString += '<strong>email: </strong>' + email + '<br>';

        finalString += '<strong>teléfono: </strong>' + phone + '<br>';

        finalString += '<strong>tickets solicitados: </strong>' + tickets + '<br>';

        finalString += '<strong>DNI: </strong>' + dni + '<br>';

        finalString += '<strong>motivo de la visita: </strong>' + reason + '<br>';

        finalString += '<strong>¿Cómo se enteró del museo?: </strong>' + how + '<br>';

        finalString+= '<br><b>INFO DE SISTEMA:</b><br><br>';

        finalString += '<strong>Sistema Operativo: </strong>' + os + '<br>';

        finalString += '<strong>Browser: </strong>' + browser + '<br>';

        finalString += '<strong>IP: </strong>' + ip + '<br>';

        finalString += '<strong>fecha de solicitud: </strong>' + added + '<br>';





        $('.modal').modal('show'); 

        $('.modal-title').text(name);

        $('.modal-body').html(finalString);

    });

}


return {

    init: function() {

        addListeners();

        createDataTable();
    }

};

}();

jQuery(document).ready(function() { MuseumHistoricalIndividualVisits.init(); });