/**
 * @summary historical! 
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
 * @function MuseumHistoricalInstitutions
 * @description Initialize and include all the methods of this class.
 */
var MuseumHistoricalInstitutions = function() {

    helper = new Helper();

    var from, to, tableType;

    /**
     * @function createDataTable
     * @description Create all the datatables that we use as lists.
     */

    var createDataTable = function() {

        var fileName = $('#historical-list').val();

        var tableType = $('#table-type').val();

        console.log("is historial: " + tableType);

        var table = $('table.display').DataTable({

            "language": helper.getDataTableLanguageConfig(),

            "processing": true,

            "serverSide": true,

            'serverMethod': 'post',

            "ajax": {

                url: "private/users/museum/booking/dataControllerAltern.php",

                dataSrc: "data",

                dataType: 'json',

                "data": function(outData) {

                    console.log(outData);

                    outData.table = "museum_booking_institutions";

                    outData.searchColumn = "institution_name";

                    outData.searchColumnTwo = "institution_email";

                    outData.from = from;

                    outData.to = to;

                    return outData;
                },
                dataFilter: function(inData) {
                    return inData;
                },

                error: function(err, status) {},
            },

            "columns": [

                {
                    "data": "institution_name"
                },

                {
                    "data": "institution_email"
                },

                {
                    "data": "institution_phone"
                },

                {
                    "defaultContent": '',

                    render: function(data, type, row) {

                        var value = (row.scholarship_asigned == 1) ? 'checked' : '';

                        var string = '<td><input ' + value + ' data-id = ' + row.id_institution + ' data-name = "' + row.institution_name + '" type="checkbox" class="my-scholarship-checkbox my-checkbox" name="my-scholarship-checkbox" data-size="mini" data-on-color="success" data-on-text="si" data-off-text="no"></td>';

                        return string;
                    }
                },

                {
                    "defaultContent": '',

                    render: function(data, type, row) {

                        var value = (row.payment_done == 1) ? 'checked' : '';

                        var string = '<td><input ' + value + ' data-id = ' + row.id_institution + ' data-name = "' + row.institution_name + '" type="checkbox" class="my-payment-checkbox my-checkbox" name="my-payment-checkbox" data-size="mini" data-on-color="success" data-on-text="si" data-off-text="no"></td>';

                        return string;

                    }
                },

                {
                    "defaultContent": '',

                    render: function(data, type, row) {

                        var value = (row.uploaded_to_plataforma == 1) ? 'checked' : '';

                        var string = '<td><input ' + value + ' data-id = ' + row.id_institution + ' data-name = "' + row.institution_name + '" type="checkbox" class="my-upload-checkbox my-checkbox" name="my-upload-checkbox" data-size="mini" data-on-color="success" data-on-text="si" data-off-text="no"></td>';

                        return string;

                    }
                },

                {
                    "defaultContent": '',

                    render: function(data, type, row) {

                        var value = (row.assisted == 1) ? 'checked' : '';

                        var string = '<td><input ' + value + ' data-id = ' + row.id_institution + ' data-name = "' + row.institution_name + '" type="checkbox" class="my-assisted-checkbox my-checkbox" name="my-assisted-checkbox" data-size="mini" data-on-color="success" data-on-text="si" data-off-text="no"></td>';

                        return string;

                    }
                },

                {
                    "defaultContent": '',

                    render: function(data, type, row) {

                        var renderString = '<td><span>';

                        renderString += '<button data-cuit = "' + row.institution_cuit + '" data-name="' + row.institution_name + '" class="info_data m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="informacion de la institucion"><i class="fas fa-info-circle"></i></button>';

                        renderString += '</td></span>';

                        return renderString;

                    }


                }

            ],

            rowCallback: function(row, data) {

                //...

                $('.open-link', row).attr('data-url', data.url);

                //...

                $('.delete_row', row).attr('data-booking-id', data.id);

                $('.delete_row', row).attr('data-name', data.institution_name);

                $('.delete_row', row).prop('title', '¿Eliminar a ' + data.institution_name + " de la lista?");


                //...

                $('.schedulle', row).prop('title', 'Conocé los turnos asignados a la institución ' + data.institution_name);

                //...

                $('.edit_data', row).prop('title', 'Editar información de ' + data.institution_name);

                //...

                $('.info_data', row).prop('title', 'más info de la institución ' + data.institution_name);

                //...

                $('.info_data', row).attr('data-name', data.institution_name);



                $('.info_data', row).attr('data-iva', data.iva);

                $('.info_data', row).attr('data-address', data.institution_address);

                $('.info_data', row).attr('data-phone', data.institution_phone);

                $('.info_data', row).attr('data-email', data.institution_email);

                $('.info_data', row).attr('data-state', data.institution_state);

                $('.info_data', row).attr('data-level', data.eduction_level);

                $('.info_data', row).attr('data-type', data.institution_type);

                $('.info_data', row).attr('data-needs', data.special_need);

                $('.info_data', row).attr('data-contact-name', data.inscription_contact_name);

                $('.info_data', row).attr('data-contact-email', data.inscription_contact_email);

                $('.info_data', row).attr('data-contact-phone', data.inscription_contact_phone);

                $('.info_data', row).attr('data-first-time', data.first_time);

                $('.info_data', row).attr('data-first-time', data.first_time);

                $('.info_data', row).attr('data-institution-first-time', data.institution_first_time);

                $('.info_data', row).attr('data-students', data.institution_students);

                $('.info_data', row).attr('data-origin', data.origin);

                $('.edit_data', row).attr('data-id', data.id);

            },

            "fnDrawCallback": function(settings) {

                $("[name='my-scholarship-checkbox']").bootstrapSwitch();

                $("[name='my-payment-checkbox']").bootstrapSwitch();

                $("[name='my-upload-checkbox']").bootstrapSwitch();

                $("[name='my-assisted-checkbox']").bootstrapSwitch();


                $('tbody').on('click', '.info_data', function() {

                    var name = $(this).attr('data-name');

                    var students = $(this).attr('data-students');

                    var cuit = $(this).attr('data-cuit');

                    var iva = $(this).attr('data-iva');

                    var address = $(this).attr('data-address');

                    var email = $(this).attr('data-email');

                    var phone = $(this).attr('data-phone');

                    var state = $(this).attr('data-state');

                    var level = $(this).attr('data-level');

                    var type = $(this).attr('data-type');

                    var needs = $(this).attr('data-needs');

                    var contactName = $(this).attr('data-contact-name');

                    var contactPhone = $(this).attr('data-contact-phone');

                    var contactEmail = $(this).attr('data-contact-email');

                    var firstTime = $(this).attr('data-first-time');

                    var origin = $(this).attr('data-origin');

                    var institutionFirstTime = $(this).attr('data-institution-first-time');

                    var finalString = 'La institiución <strong>' + name + '</strong> solicitó turnos para <strong>' + students + ' alumnos</strong>. La institución fue dada de alta desde la/el <strong>' + origin + '</strong><br>';

                    finalString += '<br><b>IMPOSITIVO:</b><br><br>';

                    finalString += '<strong>Nombre: </strong>' + name + '<br>';

                    finalString += '<strong>CUIT: </strong>' + cuit + '<br>';

                    finalString += '<strong>Condición frente al IVA: </strong>' + iva + '<br><br>';

                    finalString += '<strong>INSTITUCIONAL: </strong><br><br>';

                    finalString += '<strong>dirección: </strong>' + address + '<br>';

                    finalString += '<strong>teléfono: </strong>' + phone + '<br>';

                    finalString += '<strong>email: </strong>' + email + '<br>';

                    finalString += '<strong>provincia: </strong>' + state + '<br>';

                    finalString += '<strong>nivel: </strong>' + level + '<br>';

                    finalString += '<strong>tipo de institución: </strong>' + type + '<br>';

                    finalString += '<strong>¿Tiene necesidades especiales?: </strong>' + needs + '<br><br>';

                    finalString += '<strong>PERSONA DE CONTACTO  : </strong><br><br>';

                    finalString += '<strong>Nombre: </strong>' + contactName + '<br>';

                    finalString += '<strong>Email: </strong>' + contactEmail + '<br>';

                    finalString += '<strong>Teléfono: </strong>' + contactPhone + '<br><br>';

                    finalString += '<strong>GENERALES  : </strong><br><br>';

                    finalString += '<strong>¿Es la primera vez que visitas la institución?: </strong>' + firstTime + '<br>';

                    finalString += '<strong>¿Es la primera vez que la institución donde trabajas visita el Museo?: </strong>' + institutionFirstTime + '<br>';

                    $('.modal').modal('show');

                    $('.modal-title').text(name);

                    $('.modal-body').html(finalString);

                });

                $('tbody').on('click', '.edit_data', function() {

                    var itemId = $(this).attr('data-id');

                    //window.location.href = "museum_institution_add.php?id=" + itemId;

                });

                $('tbody').on('click', 'tr', function() {

                    $("[name='my-scholarship-checkbox']").bootstrapSwitch();

                    $("[name='my-payment-checkbox']").bootstrapSwitch();

                    $("[name='my-upload-checkbox']").bootstrapSwitch();

                    $("[name='my-assisted-checkbox']").bootstrapSwitch();

                });
            }
        });

        $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-scholarship-checkbox"]', function(event, state) {

            var itemId = $(this).attr('data-id');

            var name = $(this).attr('data-name');

            updateUserStatus(itemId, state, name, 'update', 'scholarship_asigned', 'scholarship_asigned');
        });

        $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-payment-checkbox"]', function(event, state) {

            var itemId = $(this).attr('data-id');

            var name = $(this).attr('data-name');

            updateUserStatus(itemId, state, name, 'update', 'payment_done', 'institution_payment_done');

        });


        $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-upload-checkbox"]', function(event, state) {

           var itemId = $(this).attr('data-id');

            var name = $(this).attr('data-name');

            updateUserStatus(itemId, state, name, 'update', 'uploaded_to_plataforma', 'uploaded_to_plataforma');

        });

        

        $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-assisted-checkbox"]', function(event, state) {

            var itemId = $(this).attr('data-id');

            var name = $(this).attr('data-name');

            console.log(itemId, name, status);

            updateUserStatus(itemId, state, name, 'update', 'assisted', 'institution_assisted');

        });

        $('tbody').on('click', '.delete_row', function() {

            var row = table.row($(this).parents('tr'));

            var animatable = $(this).parents('tr');

            var data = table.row($(this).parents('tr')).data();

            var actualTable = $("#tab-1").DataTable();

            var name = $(this).attr('data-name');

            var id = $(this).attr('data-booking-id');

            Swal({

                title: "¿Eliminar Institución?",

                html: 'Esta acción eliminará a <b>' + name + '</b>. Esta acción no se puede deshacer.<br><br>¿Eliminar de todos modos? Esta acción liberará los turnos que hayan sido tomados por la institución.',

                type: 'error',

                showCancelButton: true,

                confirmButtonText: 'Si, eliminar',

                cancelButtonText: 'no, salir'

            }).then((result) => {

                if (result.value != undefined) {

                    updateUserStatus(id, 'true', name, 'delete', animatable, row, actualTable, null, 'deleted');

                    deleteInstitution(id);

                } else {

                }

            })

        });

    }

    var deleteInstitution = function(id) {

        $.ajax({

            type: "POST",

            url: "private/users/museum/booking/delete_institution.php",

            data: {
                id: id
            },

            success: function(result) {

                console.log(result);

            },

            error: function(xhr, status, error) {

                console.log(xhr.responseText);

                var err = eval("(" + xhr.responseText + ")");

                console.log(err);

            },

            dataType: "json"
        });


    }

    /**
     * @function updateUserStatus
     *
     * @description Update / Delete user status from list: we use this to mark the user as online / offline and
     * for delete an user completely from the list.
     *
     * @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
     * @param {boolean} state                    - True or false: use it for both, update and delete.
     * @param {string} name                  - The name of the person being manipulated.
     * @param {string} action                    - Posibilities: 'update' or 'delete'. 
     * @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
     * @param {DataTable} row                    - Reference to the row that we are dealing with.
     * @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
     * @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
     */

    var updateUserStatus = function(id, state, name, action, defaultColumn = null, source = 'delete_booking_member') {

        helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando visitante...');

        var url = "private/users/museum/general/update_status.php";

        $.ajax({

            type: "POST",

            url: url,

            data: {
                
                id: id,
                
                status: state,
                
                action: action,
                
                name: name,
                
                source: source,

                table: 'museum_booking_institutions',
               
                defaultColumn: defaultColumn
            },

            success: function(result) {

                console.log(result);

                helper.unblockStage();

                if (action == 'update') {

                    //var newStatus = (result.changeStatus == 'true') ? 'asistió' : 'no asistió';

                    //var value = 'Cambiaste la asistencia de  ' + name + ' a: ' + newStatus;

                    helper.showToastr(result.title, result.msg);

                } else {

                    animatable.fadeOut('slow', 'linear', function() {

                        console.log(result.msg);

                        helper.showToastr('ELIMINADO', 'Eliminaste a ' + name + ' de la visita guiada.');

                        var count = 0;

                        var data = processTable.rows().data();

                        processTable.rows().every(function(rowIdx, tableLoop, rowLoop) {

                            if (processTable.cell(rowIdx, 0).data() > Number(order)) {

                                var saveValue = Number(processTable.cell(rowIdx, 0).data() - 1);

                                processTable.cell(rowIdx, 0).data(saveValue);

                            }

                        });

                        row.remove().draw(true);

                        processTable.rows().invalidate().draw(false);

                    });

                }

            },

            error: function(xhr, status, error) {

                console.log(xhr);

                var err = eval("(" + xhr.responseText + ")");

                alert(err.Message);
            },

            dataType: "json"
        });

    }

    /**
     * @function findGetParameter
     * @description obtiene parametros...
     */

    var findGetParameter = function(parameterName) {

        var result = null,

            tmp = [];

        location.search.substr(1).split("&").forEach(function(item) {

            tmp = item.split("=");

            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);

        });

        return result;
    }




    return {

        init: function() {

            from = findGetParameter('from');

            if(from == null) from = "2018-01-01";

            to = findGetParameter('to');

            if(to == null) to = "2050-01-01";//(new Date().toISOString().slice(0,10)).toString();

            console.log(to);

            createDataTable();
        }

    };

}();

jQuery(document).ready(function() {
    MuseumHistoricalInstitutions.init();
});