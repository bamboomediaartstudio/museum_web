/**
 * @summary News list.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
* @function CalendarExternalEvents
* @description Manage all the events.
*/

var CalendarExternalEvents = function() {

    helper = new Helper();

    var calendar = $('#m_calendar');

    /**
    * @function initExternalEvents
    * @description Init all :)
    */

    var initExternalEvents = function() {

        $('#m_calendar_external_events .fc-event').each(function() {

            $(this).data('event', {

                title: $.trim($(this).text()),

                backgroundColor: "purple",

                stick: true,

                start: '08:00:00',
                
                end: '08:30:00',
                
                className: $(this).data('color'),
                
                description: '...'

            });
            
            $(this).draggable({

                zIndex: 999,
                
                revert: true,

                revertDuration: 0
            });
        });
    }

    /**
    * @function addListeners
    * @description Add all the listeners.
    */

    var addListeners = function(){

        $('body').on('click', '.event-status', function(event) {

            var id = $(this).attr("data-id");

            var newStatus;

            if($(this).text() == 'online'){

                newStatus = 0;

                $(this).text('offline');

                $(this).addClass('is-offline');

                $(this).removeClass('is-online');

            }else{

                newStatus = 1;

                $(this).text('online');

                $(this).addClass('is-online');

                $(this).removeClass('is-offline');

            }

        
            var request = $.ajax({

                url: "private/users/museum/booking/event_update_status.php",

                type: "POST",

                data: { id: id, status:newStatus},

                dataType: "json",

                success: function(result) {

                    var title;

                    if(newStatus == 0){

                        title = 'Cambio de online a offline';

                        content = 'Cambiaste la visibilidad del estado de online a online';

                        color = 'error';

                    }else{

                        title = 'Cambio de offline a online';

                        content = 'Cambiaste la visibilidad del estado de offline a online';

                        color = 'success';

                    }

                    helper.showToastr(title, content, color);

                },

                error: function(request, error) {

                    console.log(request);

                    console.log("error");

                }

            });


        }); 

        $('body').on('click', '.see-event', function(event) { 

            location.href = 'museum_booking_event_list.php?id=' + $(this).attr("data-id");

        });

        $('body').on('click', '.change-tickets-per-visitors', function(event) { 

            var id = $(this).attr("data-id");;

            helper.blockStage("cargando tickets...");

            var request = $.ajax({

                url: "private/users/museum/booking/event_get_max_tickets_per_visitors.php",

                type: "POST",

                data: { id: id} ,

                dataType: "json",

                success: function(result) {

                    console.log(result.available_places, result.booked_places, result.tickets_per_visitor);

                    helper.unblockStage();

                    var actual = result.tickets_per_visitor;

                    var content = 'Este slider te permite definir la cantidad de tickets / reservas que puede hacer una única persona.';

                    var max = parseInt(result.available_places) - parseInt(result.booked_places);

                    var min = 1;

                    var actual = result.tickets_per_visitor;

                    Swal.fire({

                        title: 'Cambiar la cantidad de tickets por visitante',

                        html: content,

                        input: 'range',

                        inputAttributes: {

                            min: min,

                            max: max,

                            step: 1

                        },

                        inputValue: actual

                    }).then((result) => {

                        helper.unblockStage();

                        var request = $.ajax({

                            url: "private/users/museum/booking/event_update_max_tickets_per_visitor.php",

                            type: "POST",

                            data: { id: id, tickets_per_visitor: result.value},

                            dataType: "json",

                            success: function(result) {

                                helper.showToastr(result.title, result.msg, result.alert);

                                helper.unblockStage();

                                resetCalendar();
                            },

                            error: function(request, error) {

                                console.log(request);

                                helper.unblockStage();

                            }

                        });

                    })
                    
                },

                error: function(request, error) {

                }

            });

        });

        $('body').on('click', '.define-ammount', function(event) { 

            var id = $(this).attr("data-id");;

            helper.blockStage("cargando cantidad...");

            var request = $.ajax({

                url: "private/users/museum/booking/event_get_max_visitors.php",

                type: "POST",

                data: { id: id} ,

                dataType: "json",

                success: function(result) {

                    console.log(result.available_places, result.booked_places);

                    helper.unblockStage();

                    var max = 100;

                    var actual = result.available_places;

                    var content = 'Desde este slider podés modificar la cantidad de visitantes para este turno';

                    var min;

                    if(result.booked_places != 0){

                        min = result.booked_places;

                        content += "<br><br><strong>IMPORTANTE: </strong> Este evento ya tiene inscriptos."; 

                    }else{

                        min = 5;

                    }


                    Swal.fire({

                        title: 'Cambiar la cantidad de visitantes',

                        html: content,

                        input: 'range',

                        inputAttributes: {

                            min: min,

                            max: 100,

                            step: 1

                        },

                        inputValue: actual

                    }).then((result) => {

                        helper.unblockStage();

                        var request = $.ajax({

                            url: "private/users/museum/booking/event_update_max_visitors.php",

                            type: "POST",

                            data: { id: id, visitors: result.value},

                            dataType: "json",

                            success: function(result) {

                                helper.showToastr(result.title, result.msg, result.alert);

                                helper.unblockStage();

                                resetCalendar();
                            },

                            error: function(request, error) {

                                console.log(request);

                                helper.unblockStage();

                            }

                        });

                    })

                },

                error: function(request, error) {

                }

            });

        });

        $('body').on('click', '.change-event-place', function(event) { 

            var id = $(this).attr("data-id");;

            helper.blockStage("cargando lugar...");

            var request = $.ajax({

                url: "private/users/museum/booking/event_get_place.php",

                type: "POST",

                data: { id: id} ,

                dataType: "json",

                success: function(result) {

                    console.log(result);

                    helper.unblockStage();

                    Swal.fire({

                        title : 'Cambiar lugar del turno / evento.',

                        html: 'El lugar actual del evento es <strong>' + result.place + "</strong>. Si querés cambiarlo, hacelo desde acá.",

                        input: 'textarea',
                        
                        inputPlaceholder: 'Agregá el nuevo lugar acá',
                        
                        showCancelButton: true

                    }).then((result) => {

                        helper.unblockStage();

                        var request = $.ajax({

                            url: "private/users/museum/booking/event_update_place.php",

                            type: "POST",

                            data: { id: id, place: result.value},

                            dataType: "json",

                            success: function(result) {

                                helper.showToastr(result.title, result.msg, result.alert);

                                helper.unblockStage();

                                resetCalendar();
                            },

                            error: function(request, error) {

                                console.log(request);

                                helper.unblockStage();

                            }

                        });

                    })

                },

                error: function(request, error) {

                }

            });

        });

        $('body').on('click', '.delete-event', function(event) { 

            var saveId = $(this).attr("data-id");

            Swal({

                title: "¡Buu! ¿Eliminar evento?",

                html: 'Nos entristece saberlo! pero si aun así querés eliminar un evento, necesitamos que nos lo confirmes.<br><br>Siempre podés volver a crear nuevos eventos!',

                type: 'error',

                showCancelButton: true,

                confirmButtonText: 'Si, eliminar',

                cancelButtonText: 'no, salir'

            }).then((result) => {

                if(result.value == true) deleteEvent(saveId);

            })


        });

    }

    /**
    * @function deleteEvent
    * @description We are removing it!
    */

    var deleteEvent = function(id){

        console.log("preparing to delete " + id);

        var request = $.ajax({

            url: "private/users/museum/booking/event_delete.php",

            type: "POST",

            data: { id: id },

            dataType: "json",

            success: function(result) {

                console.log("deleete ok");

                helper.showToastr('Genial!', 'El turno se eliminó exitosamente.', 'error');

                helper.unblockStage();

                resetCalendar();
            },

            error: function(request, error) {

                console.log(request);

                helper.unblockStage();

            }

        });

    }

    /**
    * @function addNewEvent
    * @description Add a new event :)
    *
    * @param sid        - the sid of the event
    * @param start      - Start Time!
    * @param end        - End Time!
    */

    var addNewEvent = function(sid, start, end){

        helper.blockStage("creando evento...");

        var request = $.ajax({

            url: "private/users/museum/booking/event_add.php",

            type: "POST",

            data: {

                sid: sid,

                //title: (sid == 0) ? 'Visita Grupal' : 'Visita Escolar',

                start: start.format(),

                end: end.format()

            },

            dataType: "json",

            success: function(result) {

                helper.showToastr('ok');

                helper.unblockStage();

                resetCalendar();
            },

            error: function(request, error) {

                console.log(request);

                helper.unblockStage();

            }

        });

    }

    /**
    * @function updateEvent
    * @description Update a new event :)
    *
    * @param id        - The id of the object to update...
    * @param dateStart - the new start date
    * @param dateEnd   - the new end date
    */

    var updateEvent = function(id, dateStart, dateEnd){

        var request = $.ajax({

            url: "private/users/museum/booking/event_update.php",

            type: "POST",

            data: { 

                id: id,

                date_start: dateStart, 

                date_end: dateEnd

            },

            dataType: "json",

            success: function(result) {

                helper.showToastr('Genial!', 'Actualizaste el turno :)');

                helper.unblockStage();

                resetCalendar();
            },

            error: function(request, error) {

                console.log(request);

                helper.unblockStage();

            }

        });


    }


    /**
    * @function resetCalendar
    * @description It will remove all the events and refeach calling ajax
    */

    var resetCalendar = function(){

        calendar.fullCalendar('removeEvents');

        calendar.fullCalendar('refetchEvents');
    }

    /**
    * @function initCalendar
    * @description Init the calendar :D
    */

    var initCalendar = function() {

        calendar.fullCalendar({

            selectable: true,

            header: { left: 'prev,next today', center: 'title', right: 'month,agendaWeek,agendaDay,listWeek' },

            //header: { left: 'prev,next today', center: 'title', right: 'month' },
            
            eventLimit: true,
            
            navLinks: true,

            slotDuration: '00:15:00', 

            events: function(start, end, timezone, callback) {

                jQuery.ajax({

                    url: 'private/users/services/get_list_of_events.php',
                    
                    type: 'POST',
                    
                    dataType: 'json',
                    
                    data: {start: start.format(), end: end.format() },
                    
                    success: function(doc) {

                        var events = [];
                        
                        $.map(doc, function( r ) {

                            var finalBackgroundColor;

                            var finalTextColor;

                            var isCompleted;

                            if(r.available_places == r.booked_places){

                                finalBackgroundColor = r.backgroundColor;

                                finalTextColor = '#ff0000';

                                isCompleted = true;

                            }else{

                                finalBackgroundColor = '#fff';

                                finalTextColor = '#fff';

                                isCompleted = false;

                            }

                            events.push({

                                id: r.id, 

                                isCompleted: isCompleted,

                                active: r.event_active,

                                title: r.title, 

                                color: finalTextColor,

                                start: r.date_start, 

                                className : r.className,

                                backgroundColor: finalBackgroundColor,

                                category_id: r.category_id,
                                
                                total_days: r.total_days,

                                end: r.date_end,

                                available_places: r.available_places,

                                booked_places: r.booked_places

                            });

                        });

                        callback(events);
                    }

                });

            },

            editable: true,
            
            droppable: true,

            dayClick: function(date, jsEvent, view) {

                calendar.fullCalendar('clientEvents', function (event) {

                    if (moment(event.start).isSame(date, 'day')) {

                        console.log("event: " + event.id);

                        console.log("title: " + event.title);

                        console.log("check this one: " + event.category_id);

                        console.log("available: " + event.available_places);

                        console.log("booked: " + event.booked_places);

                    }

                })

            },

            eventClick: function(calEvent, jsEvent, view) {

            },

            eventDrop: function(event, delta, revertFunc) {

                console.log("event.id: " + event.id);

                console.log(moment(event.start).format());

                console.log(moment(event.end).format());

                updateEvent(event.id, moment(event.start).format(), moment(event.end).format());

            },

            select: function(start, end) {

                if(start.isBefore(moment())) {

                    calendar.fullCalendar('unselect');
                    
                    return false;
                }
            },

            eventResize: function(event, jsEvent, ui, view ) { 

                updateEvent(event.id, moment(event.start).format(), moment(event.end).format());

            },

            eventReceive : function(info){ },

            drop: function(date, jsEvent, ui, resourceId) {

                console.log($(this).attr('data-start'));

                console.log($(this).attr('data-end'));

                var sdate = $.fullCalendar.moment(date.format());  

                sdate.stripTime();

                sdate.time($(this).attr('data-start'));

                var edate = $.fullCalendar.moment(date.format());

                edate.stripTime();

                edate.time($(this).attr('data-end'));

                $(this).data('event').start = sdate;

                $(this).data('event').end = edate;

                addNewEvent($(this).attr('data-id'), $(this).data('event').start, $(this).data('event').end);

            },

            eventDragStop: function(event,jsEvent) {

                var trashEl = jQuery('.trash-for-events');

                var ofs = trashEl.offset();

                var x1 = ofs.left;

                var x2 = ofs.left + trashEl.outerWidth(true);

                var y1 = ofs.top;

                var y2 = ofs.top + trashEl.outerHeight(true);

                if (jsEvent.pageX >= x1 && jsEvent.pageX<= x2 && jsEvent.pageY >= y1 && jsEvent.pageY <= y2) { 

                    //console.log('delete!');

                    //calendar.fullCalendar('removeEvents', event.id);

                    var saveId = event.id;

                    console.log("por eliminar");

                    console.log(saveId);

                    Swal({

                        title: "¡Buu! ¿Eliminar evento?",

                        html: 'Nos entristece saberlo! pero si aun así querés eliminar un evento, necesitamos que nos lo confirmes.<br><br>Siempre podés volver a crear nuevos eventos!',

                        type: 'error',

                        showCancelButton: true,

                        confirmButtonText: 'Si, eliminar',

                        cancelButtonText: 'no, salir'

                    }).then((result) => {

                        if(result.value == true) deleteEvent(saveId);

                    })

                }

            },

            eventAfterRender:function( event, element, view ) {  },

            eventRender: function(event, element) {

                if (event.isCompleted) {

                    element.data('content', 'Turno Completo :)');

                    element.data('placement', 'top');

                    mApp.initPopover(element);
                }
                element.find('.fc-title').append('<button data-id="' + event.id + '" class="ml-4 btn red delete-event">ELIMINAR EVENTO</button>');

                element.find('.fc-title').append('<button data-id="' + event.id + '" class="ml-2 btn see-event">VER EVENTO</button>');
                
                element.find('.fc-title').append('<button data-id="' + event.id + '" class="ml-2 btn change-event-place">LUGAR</button>');

                if(event.category_id == 2) element.find('.fc-title').append('<button data-id="' + event.id + '" class="ml-2 btn define-ammount">N DE VISITANTES</button>');

                if(event.category_id == 2) element.find('.fc-title').append('<button data-id="' + event.id + '" class="ml-2 btn change-tickets-per-visitors">N DE TICKETS</button>');

                var activeClass = (event.active == 1) ? 'is-online' : 'is-offline';

                var label = (event.active == 1) ? 'online' : 'offline';

                element.find('.fc-title').append('<button data-id="' + event.id + '" class="' + activeClass + ' ml-2 btn event-status">' + label + '</button>');

            }

        });

}



return {

    init: function() {

        initExternalEvents();

        initCalendar(); 

        addListeners();

    }
};
}();

jQuery(document).ready(function() { CalendarExternalEvents.init(); });