/**
* @summary historical! 
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/

/**
* @function MuseumHistoricalInstitutions
* @description Initialize and include all the methods of this class.
*/

var MuseumHistoricalInstitutions = function() {

    helper = new Helper();

     /**

     * @function addListeners
     * @description Asign all the listeners / event habdlers for this page.
     */

     var addListeners = function(){

        $("[name='my-assisted-checkbox']").bootstrapSwitch();

        $('tbody').on( 'click', '.info_data', function () {

            var name = $(this).attr('data-name');

            var students = $(this).attr('data-students');

            var cuit = $(this).attr('data-cuit');

            var iva = $(this).attr('data-iva');

            var address = $(this).attr('data-address');

            var email = $(this).attr('data-email');
            
            var phone = $(this).attr('data-phone');

            var state = $(this).attr('data-state');

            var level = $(this).attr('data-level');

            var type = $(this).attr('data-type');

            var needs = $(this).attr('data-needs');

            var contactName = $(this).attr('data-contact-name');

            var contactPhone = $(this).attr('data-contact-phone');

            var contactEmail = $(this).attr('data-contact-email');

            var firstTime = $(this).attr('data-first-time');
            
            var origin = $(this).attr('data-origin');

            var institutionFirstTime = $(this).attr('data-institution-first-time');

            var finalString = 'La institiución <strong>' + name + '</strong> solicitó turnos para <strong>' + students + ' alumnos</strong>. La institución fue dada de alta desde la/el <strong>' + origin + '</strong><br>';

            finalString += '<br><b>IMPOSITIVO:</b><br><br>';
            
            finalString+='<strong>Nombre: </strong>' + name + '<br>';

            finalString+='<strong>CUIT: </strong>' + cuit + '<br>';

            finalString+='<strong>Condición frente al IVA: </strong>' + iva + '<br><br>';
            
            finalString+='<strong>INSTITUCIONAL: </strong><br><br>';

            finalString+='<strong>dirección: </strong>' + address + '<br>';

            finalString+='<strong>teléfono: </strong>' + phone + '<br>';

            finalString+='<strong>email: </strong>' + email + '<br>';

            finalString+='<strong>provincia: </strong>' + state + '<br>';

            finalString+='<strong>nivel: </strong>' + level + '<br>';

            finalString+='<strong>tipo de institución: </strong>' + type + '<br>';

            finalString+='<strong>¿Tiene necesidades especiales?: </strong>' + needs + '<br><br>';

            finalString+='<strong>PERSONA DE CONTACTO  : </strong><br><br>';

            finalString+='<strong>Nombre: </strong>' + contactName + '<br>';

            finalString+='<strong>Email: </strong>' + contactEmail + '<br>';

            finalString+='<strong>Teléfono: </strong>' + contactPhone + '<br><br>';

            finalString+='<strong>GENERALES  : </strong><br><br>';

            finalString+='<strong>¿Es la primera vez que visitas la institución?: </strong>' + firstTime + '<br>';
            
            finalString+='<strong>¿Es la primera vez que la institución donde trabajas visita el Museo?: </strong>' + institutionFirstTime + '<br>';

            $('.modal').modal('show'); 

            $('.modal-title').text(name);

            $('.modal-body').html(finalString);

        });
    }


    /**
    * @function createDataTable
    * @description Create all the datatables that we use as lists.
    */

    var createDataTable = function(){

        var fileName = $('#historical-list').val();

        var table = $('table.display').DataTable({

            dom: 'Bfrtip',

            buttons: [ 'copy', 'csv', 'excel', 'pdf', 'print'],

            buttons: ['copy', 'print',

            {
                extend: 'csvHtml5',
                title: fileName
            },


            {
                extend: 'excelHtml5',
                title: fileName
            },
            {
                extend: 'pdfHtml5',
                title: fileName
            }
            ],

            pageLength: 10,

            "language": helper.getDataTableLanguageConfig(),

            bAutoWidth: true,

            responsive:true, 

            processing:true,

            "columns": [

            { "width": "5", responsivePriority: 5, orderable: true, targets: 0}, 

            { "width": "35%", responsivePriority: 0, orderable: true, targets: '_all' }, 

            { "width": "5%", responsivePriority: 4, orderable: true, targets: '_all' }, 
            
            { "width": "10%", responsivePriority: 6, orderable: true, targets: '_all' }, 
            
            { "width": "35%", responsivePriority: 7, orderable: true, targets: '_all' }, 

            { "width": "5%", responsivePriority: 3, orderable: true, targets: '_all' },

            { "width": "5%", responsivePriority: 1, orderable: true, targets: '_all' },
            
            { "width": "5%", responsivePriority: 2, orderable: true, targets: '_all' }

            ]

        });

        $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-assisted-checkbox"]', function(event, state) {

            var closestRow = $(this).closest('tr');

            var data = table.row(closestRow).data();

            var name = data[1];

            var itemId = $(this).attr('data-id');

            updateUserStatus(itemId, state, name, 'update', closestRow, table.row(closestRow), null, null, 'assisted', 'institution_assisted');


        });

    }


/**
* @function updateUserStatus
*
* @description Update / Delete user status from list: we use this to mark the user as online / offline and
* for delete an user completely from the list.
*
* @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
* @param {boolean} state                    - True or false: use it for both, update and delete.
* @param {string} name                  - The name of the person being manipulated.
* @param {string} action                    - Posibilities: 'update' or 'delete'. 
* @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
* @param {DataTable} row                    - Reference to the row that we are dealing with.
* @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
* @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
*/

var updateUserStatus = function(id, state, name, action, animatable, row, processTable = null, order = null, defaultColumn = null, source = 'delete_booking_member'){

    helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando visitante...');

    var url = "private/users/museum/general/update_status.php"; 

    $.ajax({

        type: "POST",

        url: url,

        data: {id: id, status:state, action:action, name:name, source: source,

            table:'museum_booking_institutions', defaultColumn:defaultColumn},

            success: function(result){

                console.log(result);

                helper.unblockStage();

                if(action == 'update'){

                    //var newStatus = (result.changeStatus == 'true') ? 'asistió' : 'no asistió';

                    //var value = 'Cambiaste la asistencia de  ' + name + ' a: ' + newStatus;

                    helper.showToastr(result.title, result.msg);

                }else{

                    animatable.fadeOut('slow','linear',function(){

                        console.log(result.msg);

                        helper.showToastr('ELIMINADO', 'Eliminaste a ' + name + ' de la visita guiada.');

                        var count = 0;

                        var data = processTable.rows().data();

                        processTable.rows().every( function (rowIdx, tableLoop, rowLoop) {

                            if(processTable.cell(rowIdx, 0).data() > Number(order)){

                                var saveValue = Number(processTable.cell(rowIdx, 0).data() - 1);

                                processTable.cell(rowIdx, 0).data(saveValue);

                            }

                        });

                        row.remove().draw(true);

                        processTable.rows().invalidate().draw(false);

                    });

                }

            },

            error: function(xhr, status, error) {

                console.log(xhr);

                var err = eval("(" + xhr.responseText + ")");

                alert(err.Message);
            },

            dataType: "json"
        });

}



return {

    init: function() {

        addListeners();

        createDataTable();

        console.log("valid");
    }

};

}();

jQuery(document).ready(function() { MuseumHistoricalInstitutions.init(); });