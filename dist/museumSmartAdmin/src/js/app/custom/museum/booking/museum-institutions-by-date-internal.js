/**
 * @summary Filter by date
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
* @function Init
* @description filter...
*/

var Init = function() {

    helper = new Helper();

    var calendar = $('#m_calendar');

    
    /**
    * @function addListeners
    * @description Add all the listeners.
    */

    var addListeners = function(){

        $('[data-toggle="tooltip"]').tooltip();

        $('#date-from').datepicker({orientation: 'bottom', autoclose: true, language: 'es',  format: 'yyyy-mm-dd'});

        $('#date-to').datepicker({ autoclose: true, language: 'es',  format: 'yyyy-mm-dd'});

        $('.go-to-yesterday').click(function(){

            console.log("ir a ayer");

            dateFrom = $('#yesterday').val();

            dateTo = $('#today').val();

            $finalURL = 'https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/museum_institutions_assistance.php?from=' + dateFrom + '&to=' + dateTo;

            window.location.href = $finalURL;

        });

        $('.go-to-today').click(function(){

            console.log("ir a hoy");

            dateFrom = $('#today').val();

            dateTo = $('#tomorrow').val();

            $finalURL = 'https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/museum_institutions_assistance.php?from=' + dateFrom + '&to=' + dateTo;

            window.location.href = $finalURL;

        });

        $('.go-to-tomorrow').click(function(){

            console.log("ir a manana");

            dateFrom = $('#tomorrow').val();

            dateTo = $('#dayAfterTomorrow').val();

            $finalURL = 'https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/museum_institutions_assistance.php?from=' + dateFrom + '&to=' + dateTo;

            window.location.href = $finalURL;

        });

        $('.go-to-two-days').click(function(){

            console.log("ir a dos dias");

            dateFrom = $('#dayAfterTomorrow').val();

            dateTo = $('#inThreeDays').val();

            $finalURL = 'https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/museum_institutions_assistance.php?from=' + dateFrom + '&to=' + dateTo;

            window.location.href = $finalURL;

        });

        $('.go-to-this-week').click(function(){

            console.log("ir a la semana");

            dateFrom = $('#mondayThisWeek').val();

            dateTo = $('#sundayThisWeek').val();

            $finalURL = 'https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/museum_institutions_assistance.php?from=' + dateFrom + '&to=' + dateTo;

            window.location.href = $finalURL;

        });

        $('.go-to-month').click(function(){

            console.log("ir a month");

            dateFrom = $('#first').val();

            dateTo = $('#last').val();

            $finalURL = 'https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/museum_institutions_assistance.php?from=' + dateFrom + '&to=' + dateTo;

            window.location.href = $finalURL;

        });

        $('.export-excel').click(function(){

            var dateFrom = $('#date-from').datepicker('getFormattedDate');

             var dateTo = $('#date-to').datepicker('getFormattedDate');

            if(dateFrom == null || dateFrom == ""){

                var title = "ops!! y la fecha de comienzo?";

                var msg = "Debes indicar la fecha desde la cual querés exportar!";

                showErrorSwal(title, msg);

            }else if(dateTo == null || dateTo == "") {
              
                var title = "ops!! y la fecha de fin?";

                var msg = "Debes indicar la fecha hasta la cual querés exportar!";

                showErrorSwal(title, msg);
          
            }else{

                if(dateTo <= dateFrom){
                    
                    var title = "ehh!!";

                    var msg = "la fecha de fin tiene que ser mayor a la de inicio. Por favor, revisá las fechas!";

                    showErrorSwal(title, msg);

                }else{

                    $finalURL = 'https://museodelholocausto.org.ar/museumSmartAdmin/dist/default/museum_institutions_assistance.php?from=' + dateFrom + '&to=' + dateTo;

                    window.location.href = $finalURL;

                    console.log("validar from y to");
                }

            }
        
             console.log(dateFrom);

             console.log(dateTo);


        })

    }

    /**
     * @function showErrorSwal
     * @param msg - the msg.
     * @description Swal alert.
     */

     var showErrorSwal = function(title, msg){

        Swal({

            title: title,

            html: msg,

            type: 'error',

            confirmButtonText: 'Entendido!'

        })
    }

    return {

        init: function() {

            addListeners();

        }
    };

}();

jQuery(document).ready(function() { Init.init(); });