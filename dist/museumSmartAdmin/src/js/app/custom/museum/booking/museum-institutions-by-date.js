/**
 * @summary Filter by date
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
* @function Init
* @description filter...
*/

var Init = function() {

    helper = new Helper();

    var calendar = $('#m_calendar');

    var readableDateFormat = "dd-mm-yyyy";

    
    /**
    * @function addListeners
    * @description Add all the listeners.
    */

    var addListeners = function(){

        $('[data-toggle="tooltip"]').tooltip();

        $('#date-from').datepicker({orientation: 'bottom', autoclose: true, language: 'es',  format: readableDateFormat});

        $('#date-to').datepicker({ autoclose: true, language: 'es',  format: readableDateFormat});

        $('.go-to-yesterday').click(function(){

            dateFrom = $('#yesterday').val();

            dateTo = $('#today').val();

            $finalURL = 'museum_historical_institutions.php?from=' + dateFrom + '&to=' + dateTo;

            window.location.href = $finalURL;

        });

        $('.go-to-today').click(function(){

            dateFrom = $('#today').val();

            dateTo = $('#today').val();

            $finalURL = 'museum_historical_institutions.php?from=' + dateFrom + '&to=' + dateTo;

            window.location.href = $finalURL;

        });

        $('.go-to-tomorrow').click(function(){

            dateFrom = $('#tomorrow').val();

            dateTo = $('#tomorrow').val();

            $finalURL = 'museum_historical_institutions.php?from=' + dateFrom + '&to=' + dateTo;

            window.location.href = $finalURL;

        });

        $('.go-to-two-days').click(function(){

            dateFrom = $('#dayAfterTomorrow').val();

            dateTo = $('#dayAfterTomorrow').val();

            $finalURL = 'museum_historical_institutions.php?from=' + dateFrom + '&to=' + dateTo;

            window.location.href = $finalURL;

        });

        $('.go-to-this-week').click(function(){

            dateFrom = $('#mondayThisWeek').val();

            dateTo = $('#sundayThisWeek').val();

            $finalURL = 'museum_historical_institutions.php?from=' + dateFrom + '&to=' + dateTo;

            window.location.href = $finalURL;

        });

        $('.go-to-month').click(function(){

            dateFrom = $('#first').val();

            dateTo = $('#last').val();

            $finalURL = 'museum_historical_institutions.php?from=' + dateFrom + '&to=' + dateTo;

            window.location.href = $finalURL;

        });

        $('.export-excel').click(function(){

            var dateFrom = $('#date-from').data('datepicker').getFormattedDate('yyyy-mm-dd');//$('#date-from').datepicker('getFormattedDate');

             var dateTo = $('#date-to').data('datepicker').getFormattedDate('yyyy-mm-dd');//$('#date-to').datepicker('getFormattedDate');

            if(dateFrom == null || dateFrom == ""){

                var title = "ops!! y la fecha de comienzo?";

                var msg = "Debes indicar la fecha desde la cual querés exportar!";

                showErrorSwal(title, msg);

            }else if(dateTo == null || dateTo == "") {
              
                var title = "ops!! y la fecha de fin?";

                var msg = "Debes indicar la fecha hasta la cual querés exportar!";

                showErrorSwal(title, msg);
          
            }else{

                if(dateTo <= dateFrom){
                    
                    var title = "ehh!!";

                    var msg = "la fecha de fin tiene que ser mayor a la de inicio. Por favor, revisá las fechas!";

                    showErrorSwal(title, msg);

                }else{

                    $finalURL = 'museum_historical_institutions.php?from=' + dateFrom + '&to=' + dateTo;

                    window.location.href = $finalURL;

                    console.log("validar from y to");
                }

            }
        
             console.log(dateFrom);

             console.log(dateTo);


        })

    }

    /**
     * @function showErrorSwal
     * @param msg - the msg.
     * @description Swal alert.
     */

     var showErrorSwal = function(title, msg){

        Swal({

            title: title,

            html: msg,

            type: 'error',

            confirmButtonText: 'Entendido!'

        })
    }

    return {

        init: function() {

            addListeners();

        }
    };

}();

jQuery(document).ready(function() { Init.init(); });