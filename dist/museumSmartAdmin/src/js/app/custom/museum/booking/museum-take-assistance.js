/**
 * @summary News list.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
* @function AssistancesManager
* @description Manage all the events.
*/

var AssistancesManager = function() {

    helper = new Helper();

    var calendar = $('#m_calendar');

    
    /**
    * @function addListeners
    * @description Add all the listeners.
    */

    var addListeners = function(){

        $('[data-toggle="tooltip"]').tooltip();

        /*$('.take-assistance').click(function(){

            alert("ok!");

        })*/

    }

    return {

        init: function() {

            addListeners();

        }
    };

}();

jQuery(document).ready(function() { AssistancesManager.init(); });