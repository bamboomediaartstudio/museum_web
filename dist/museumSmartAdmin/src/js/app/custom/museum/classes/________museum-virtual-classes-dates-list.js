/**
 * @summary historical! 
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
 * @function InitModule
 * @description Initialize and include all the methods of this class.
 */

 var InitModule = function() {

    helper = new Helper();

    var readableDateFormat = "dd-mm-yyyy";

    var saveId;

    var finalStartDate;

    var finalEndDate;

    var startDate;

    var endDate;

    var date;

    var dateTo;

    var token;

    var formUtils = new FormUtils();

    var utils = new Utils();

    var table;

    var isZoomData = false;

    var formIdEvent;

     var formIdGuide;

     var formIdHost;

     var formIdInstitution;

     var className;

     var readableDate;     

     var isAddingDate = false;         

     var selectedClassId;

     var isInstitution = false;

     var isContact = false;

     var userType;

     var userId;

     var tempInstitutionId;

     var institutionsEventsTable;

     var individualEventstable;

     var individualsTable;

     var actualIdentifier;

     var actualTab = 1;

     var lastTab = 1;

     var holidaysTypes = new Array('Feriados inamovibles', 'Feriados trasladables', 'Días no laborables');

    var nationalHolidays = new Array(

        {'date': '2020-01-01', 'description' : 'Año nuevo', type:0},

        {'date': '2020-02-24', 'description' : 'Carnaval', type:0},

        {'date': '2020-02-25', 'description' : 'Carnaval', type:0},
        
        {'date': '2020-03-23', 'description' : 'Feriado con Fines turístico', type:0},

        {'date': '2020-03-24', 'description' : 'Día Nacional de la Memoria por la Verdad y la Justicia.', type:0},

        {'date': '2020-04-02', 'description' : 'Día del Veterano y de los Caídos en la Guerra de Malvinas. Feriado inamovible.', type:0},
        
        {'date': '2020-04-09', 'description' : 'Jueves Santo.Día no laborable', type:2},
        
        {'date': '2020-04-10', 'description' : 'Viernes Santo. Feriado inamovible', type:0},

        {'date': '2020-04-10', 'description' : 'Viernes Santo. Feriado inamovible', type:2},

        {'date': '2020-04-15', 'description' : 'Pascuas Judías.Día no laborable', type:2},

        {'date': '2020-04-16', 'description' : 'Pascuas Judías.Día no laborable', type:2},
        
        {'date': '2020-04-24', 'description' : 'Día de acción por la tolerancia y el respeto entre los pueblos (a).', type:2},
        
        {'date': '2020-05-01', 'description' : 'Día del trabajador. Feriado inamovible', type:0},

        {'date': '2020-05-24', 'description' : 'Fiesta de la Ruptura del Ayuno del Sagrado Mes de Ramadán (Id al-Fitr) (c).Día no laborable', type:2},

        {'date': '2020-05-25', 'description' : 'Día de la Revolución de Mayo.', type:0},

        {'date': '2020-06-15', 'description' : 'Día Paso a la Inmortalidad del General Martín Miguel de Güemes (17/06).Feriado trasladable.', type:1},
        
        {'date': '2020-06-20', 'description' : 'Día Paso a la Inmortalidad del General Manuel Belgrano.', type:0},

        {'date': '2020-07-09', 'description' : 'Día de la Independencia. Feriado inamovible.', type:0},
        
        {'date': '2020-07-10', 'description' : 'Feriado con Fines Turísticos. Dia no laborable.', type:0},

        {'date': '2020-07-31', 'description' : 'Fiesta del Sacrificio (c)', type:2},

        {'date': '2020-08-17', 'description' : 'Paso a la Inmortalidad del Gral. José de San Martín.Feriado trasladable.', type:1},

        {'date': '2020-08-20', 'description' : 'Año Nuevo Islámico (c).', type:2},
        
        {'date': '2020-09-19', 'description' : 'Año Nuevo Judío (b)**. Dia no laborable.', type:2},

        {'date': '2020-09-20', 'description' : 'Año Nuevo Judío (b)**. Dia no laborable.', type:2},
        
        {'date': '2020-09-28', 'description' : 'Día del Perdón (b)***', type:2},
        
        {'date': '2020-10-12', 'description' : 'Día del Respeto a la Diversidad Cultural.', type:1},
        
        {'date': '2020-11-20', 'description' : 'Día de la Soberanía Nacional (20/11).', type:1},
        
        {'date': '2020-12-07', 'description' : 'Feriado con Fines Turísticos.Dia no laborable.', type:0},

        {'date': '2020-12-08', 'description' : 'Inmaculada Concepción de María. Feriado inamovible.', type:0},
        
        {'date': '2020-12-08', 'description' : 'Navidad', type:0}


        );


    /**
     * @function createDataTable
     * @description Create all the datatables that we use as lists.
     */

     var createDataTable = function(value, bookingType, eventId = undefined) {

        var pageLength = 100;
        
        var url;

        var cols = new Array();

        if(bookingType == 1){

            url = "private/users/museum/classes/getListOfInstitutionsClassesByDate.php";

            cols = [2, 3, 4, 5, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];

            console.log("calling");

        }else if(bookingType == 2){

            url = "private/users/museum/classes/getListOfClassesForIndividuals.php";

            cols = [2, 3, 4, 5, 8, 9, 10, 11, 12, 13];
            
        }else if(bookingType == 3){

            pageLength = 200;

            url = "private/users/museum/classes/getListOfInscriptionsInClassesByEvent.php";

            cols = [1, 2, 4, 5]
           
        }

        table = value.DataTable({

            retrieve: true,

            dom: 'Bfrtip',

            buttons: [
            {
                extend: 'excelHtml5',
                
                exportOptions: {
                
                    columns: cols
                
                }
            },

            ],

            language: helper.getDataTableLanguageConfig(),

            processing: true,

            serverSide: true,

            order: [ 22, "asc" ],

            //autoWidth: true,

            pageLength: pageLength,

            serverMethod: 'post',

            "ajax": {

                url: url,

                dataSrc: "data",

                dataType: 'json',

                data: function(outData) {

                    console.log(outData);

                    //console.log("importante: " + dateTo);

                    outData.date = date;
                    
                    outData.dateTo = dateTo;

                    outData.userId = userId;

                    outData.userType = userType;

                    if(bookingType == 3) outData.eventId = eventId;

                  },

                dataFilter: function(inData) {  return inData; },

                error: function(err, status) { },
            },


            "columns": [

            {data: 'id_event', visible: false},

            {data: 'reference', 

            width: '5px',

            render: function(data, type, full, meta) { 

                console.log(full);

                if(bookingType == 3){

                    return full.name;

                }else{

                    var alert;

                    var errorString = 'Falta asignar información:<br><br>';

                    var isError = false;

                    var errorCounter = 0;

                    if(full.zoom_url == null){

                        isError = true;

                        errorString += 'Falta asignar info del Zoom.<br>';

                        errorCounter += 1;

                    }
                    
                    if(full.id_guide == -1){

                        isError = true;

                        errorString += 'Falta asignar al guía.<br>';

                        errorCounter += 1;

                    }

                    if(full.id_host == -1){

                        isError = true;

                        errorString += 'Falta asignar al anfitrión.<br>';

                        errorCounter += 1;

                    }

                    var finalString;

                    var finalIcon;

                    var color;

                    if(isError == true){

                        finalString = errorString;

                        finalIcon = 'fa-exclamation-triangle';

                        if(errorCounter == 3){

                            color = 'danger';

                        }else{
                            
                            color = 'warning';

                        }

                    }else{

                        finalString = 'Este turno tiene toda la info!'

                        finalIcon = 'fas fa-check';

                        color = 'success';
                    }


                    var st = '<i class="fas ' + finalIcon + ' text-' + color + '" data-html="true" data-toggle="tooltip" data-placement="bottom" title="' + finalString + '"></i>';

                    return st;

                }

            }

            },

            {data: 'date_dart',

            width: '5px',

            //orderable: true,

            render: function(data, type, full, meta) { 

                if(bookingType == 3){

                    return full.surname;

                }else{

                    //sp = '<span data-toggle="tooltip" data-placement="bottom" title="' + full.readable_date_start + '">fecha y hora</span>';

                    sp = full.readable_date_start;

                    return sp;

                }

            }

            },

            {data: 'readable_hour',

            width: '5px',

            visible: (bookingType == 3) ? false : true,

            render: function(data, type, full, meta) { 

                return full.readable_hour

                /*if(bookingType == 3){

                    return full.email;

                }else{

                    //sp = '<span data-toggle="tooltip" data-placement="bottom" title="' + full.readable_date_start + '">fecha y hora</span>';

                    sp = full.readable_date_start;

                    return sp;

                }*/

            }

            },

            {data: 'class_name',

            render: function(data, type, full, meta) { 

                if(bookingType == 3){

                    //return full.surname;
                    return full.phone;

                }else{

                    var st;

                    var sp;

                    var url = "https://museodelholocausto.org.ar/proximoEvento/index.php?id=" + full.id_event;

                    if(full.institution_name == null){

                        st = full.class_name;

                        if(st.length > 10) st = st.substr(0, 20) + "...";

                        sp = '<span data-toggle="tooltip" data-placement="bottom" title="' + full.class_name + '"><a href="' + url + '" target = "_blank">' + st + '</a></span>';

                    }else{

                        st = full.class_name;

                        if(st.length > 10) st = st.substr(0, 20) + "...";

                        sp = '<span data-toggle="tooltip" data-placement="bottom" title="' + full.class_name + '"><a href="' + url + '" target = "_blank">' + st + '</a></span>';

                        

                    }

                    return sp; 
                }


            }

            },

            {data: 'institution_name',

            responsivePriority: 1,

            render: function(data, type, full, meta) { 

                if(bookingType == 3){

                    return full.email;

                }else if(bookingType == 2){

                    var total = full.available_places;

                    var actual = full.booked_places;

                    var st = actual + " de " + total;

                    return st;

                }else{

                    var st;

                    var isAvailable;

                    if(full.institution_name == null){

                        isAvailable = true;

                        st = 'libre';

                    }else{

                        isAvailable = false;

                        st = full.institution_name;

                        //if(st.length > 10) st = st.substr(0, 20) + "...";

                    }

                    var sp;

                    if(isAvailable){

                        sp = st;

                    }else{

                        sp = '<span data-toggle="tooltip" data-placement="bottom" title="' + full.institution_name + '">' + st + '</span>';

                    }

                
                    return sp; 

                }

            }


            },

             
            {data: 'active',

            width: '5px',

            visible: (userType == 6 || userType == 4 || bookingType == 3) ? false : true,

            render: function(data, type, full, meta) { 

                var checked = (full.active == 1) ? 'checked' : '';

                st = '<td><input data-id="' + full.id_event + '" type="checkbox" class="editor-active my-checkbox" name="my-checkbox" data-size="mini" data-on-color="success" data-on-text="online" data-off-text="offline" ' + checked + '></td>';

                return st;

            }},

            {data: 'name',

            width: (bookingType == 3) ? '40' : '160',

            responsivePriority: 1,

            render: function(data, type, full, meta) { 

                var renderString = "";

                renderString += "";

                if(bookingType == 3){

                    if(userType != 6 && userType != 4) renderString += '<button data-toggle="tooltip" data-placement="bottom" title="¿Eliminar usuario?" class="delete-visitor m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-trash"></i></button>';

                    return renderString;
                    
                }

                var institutionRegistered;

                var toolTip;

                var toolTipInstitution;

                if(full.id_institution == null){

                    institutionRegistered = false;
                    
                    toolTip = 'No hay info aun porque el turno no fue reservado';
                    
                    toolTipInstitution = 'No hay info aun porque el turno no fue reservado';

                }else{

                    institutionRegistered = true;

                    toolTip = 'ver info de la persona de contacto: ' + full.inscription_contact_name;

                    toolTipInstitution = 'ver info de la institución: ' + full.institution_name;

                }

                if(full.id_guide != -1){

                    if(userType == 6 || userType == 4){

                        guideTooltip = 'El guía es: ' + full.guide_name + ' ' + full.guide_surname;

                    }else{

                        guideTooltip = 'Cambiar guía (actualmente ' + full.guide_name + ' ' + full.guide_surname + ')';

                    }

                    
                
                }else{

                    guideTooltip = 'Guía sin asignar';


                }

                if(full.id_host != -1){

                    if(userType == 6 || userType == 4){

                        hostTooltip = 'El host es: ' + full.host_name + ' ' + full.host_surname;

                    }else{

                       hostTooltip = 'Cambiar anfitrión (actualmente ' + full.host_name + ' ' + full.host_surname + ')';

                    }
  

                }else{

                    hostTooltip = 'Anfitrión sin asignar';


                }

                

                if(bookingType == 2) renderString += '<button data-toggle="tooltip" data-placement="bottom" title="la lista de inscriptos" class="list-of-persons m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-list"></i></button>';

                if(bookingType == 1) renderString += '<button data-toggle="tooltip" data-placement="bottom" title="' + toolTip + '" class="inscription-contact-data m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-user"></i></button>';

                if(bookingType == 1) renderString += '<button data-toggle="tooltip" data-placement="bottom" title="' + toolTipInstitution + '" class="institution-data m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-university"></i></button>';

                //if(userType != 6 && userType != 4) renderString += '<button data-toggle="tooltip" data-placement="bottom" title="¿Cambiar horario?" class="change-time m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-clock"></i></button>';

                if(userType != 6 && userType != 4) renderString += '<button data-toggle="tooltip" data-placement="bottom" title="¿Liberar turno?" class="delete-item m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-trash"></i></button>';
                
                if(userType != 4) renderString += '<button data-toggle="tooltip" data-placement="bottom" title="' + guideTooltip + '" class="set-guide m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-user-check"></i></button>';
                
                if(userType != 6) renderString += '<button data-toggle="tooltip" data-placement="bottom" title="' + hostTooltip + '" class="set-host m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-user-cog"></i></button>';
                
                renderString += '<button data-toggle="tooltip" data-placement="bottom" title="datos de Zoom Meeting" class="set-zoom m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-laptop"></i></button>';

                renderString += '<button data-toggle="tooltip" data-placement="bottom" title="Usá esta herramienta para dejarte notitas, reminders, post-its, sticky notes :)" class="sticky-note m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="notas"><i class="fas fa-file-alt"></i></button>';
                
                //if(userType != 6 && userType != 4) renderString += '<button data-toggle="tooltip" data-placement="bottom" title="Cambiar turno" class="change-booking m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="notas"><i class="fas fa-sync"></i></button>';
                
                if(userType != 6 && userType != 4) renderString += '<button data-toggle="tooltip" data-placement="bottom" title="Clonar turno" class="clone-event m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="clonar el turno"><i class="fas fa-copy"></i></button>';

                return renderString;

            }

            },

            {data: 'zoom_account', visible: false},

            {data: 'zoom_url', visible: false},

            {data: 'zoom_id', visible: false},

            {data: 'zoom_password_encrypt', visible: false},

            {data: 'guide', visible: false, render: function(data, type, full, meta){ return full.guide_name + ' ' + full.guide_surname; }},

            {data: 'host', visible: false, render: function(data, type, full, meta){ return full.host_name + ' ' + full.host_surname; }},

            {data: 'institution_students', visible: false, render: function(data, type, full, meta){ 

                if(bookingType == 1){

                    return full.institution_students;
               
                }else{

                    return "";
                }

            }},

            {data: 'institution_address', visible: false, render: function(data, type, full, meta){ 

                if(bookingType == 1){

                    return full.institution_address;
               
                }else{

                    return "";
                }

            }},

            {data: 'institution_state', visible: false, render: function(data, type, full, meta){ 

                if(bookingType == 1){

                    return full.institution_state;
               
                }else{

                    return "";
                }

            }},

            {data: 'education_level', visible: false, render: function(data, type, full, meta){ 

                if(bookingType == 1){

                    return full.education_level;
               
                }else{

                    return "";
                }

            }},

            {data: 'institution_type', visible: false, render: function(data, type, full, meta){ 

                if(bookingType == 1){

                    return full.institution_type;
               
                }else{

                    return "";
                }

            }},

            {data: 'inscription_contact_name', visible: false, render: function(data, type, full, meta){ 

                if(bookingType == 1){

                    return full.inscription_contact_name;
               
                }else{

                    return "";
                }

            }},

            {data: 'inscription_contact_phone', visible: false, render: function(data, type, full, meta){ 

                if(bookingType == 1){

                    return full.inscription_contact_phone;
               
                }else{

                    return "";
                }

            }},

            {data: 'inscription_contact_email', visible: false, render: function(data, type, full, meta){ 

                if(bookingType == 1){

                    return full.inscription_contact_email;
               
                }else{

                    return "";
                }

            }},

            {data: 'date_start', visible: false},

            ],

            "initComplete": function() {

                this.api().columns.adjust();

            },

            "fnCreatedRow": function(nRow, aData, iDataIndex ) { addData(nRow, aData, bookingType, eventId); },

            rowCallback: function(row, data) { addData(row, data, bookingType, eventId); },

            "fnDrawCallback": function(settings) {

                var hash = window.location.hash;
        
                $('#myTab a[href="' + hash + '"]').tab('show');

                $('[data-toggle="tooltip"]').tooltip();

                $("[name='my-checkbox']").bootstrapSwitch();

                //$('#tab-3').DataTable().columns.adjust();
                
            }
        });

        table.draw();

        table.columns.adjust().draw();

    }

    var addData = function(row, data, bookingType, eventId = undefined){

        if(bookingType == 3){

            $('.delete-visitor', row).attr('data-id', data.id);
 
            $('.delete-visitor', row).attr('data-name', data.name);

            $('.delete-visitor', row).attr('data-surname', data.surname);
           
            $('.delete-visitor', row).attr('id-event', eventId);

        }
        
        $('.inscription-contact-data', row).attr('data-contact-name', data.inscription_contact_name);

        $('.inscription-contact-data', row).attr('data-contact-email', data.inscription_contact_email);

        $('.inscription-contact-data', row).attr('data-contact-phone', data.inscription_contact_phone);

        $('.inscription-contact-data', row).attr('data-first-time', data.first_time);

        $('.inscription-contact-data', row).attr('data-first-time', data.first_time);

        $('.inscription-contact-data', row).attr('data-institution-first-time', data.institution_first_time);

        $('.inscription-contact-data', row).attr('data-students', data.institution_students);

        $('.inscription-contact-data', row).attr('data-origin', data.origin);

        //...

        $('.institution-data', row).attr('data-name', data.institution_name);

        $('.institution-data', row).attr('data-institution-id', data.id_institution);

        $('.institution-data', row).attr('data-cuit', data.institution_cuit);

        $('.institution-data', row).attr('data-iva', data.iva);

        $('.institution-data', row).attr('data-address', data.institution_address);

        $('.institution-data', row).attr('data-students', data.institution_students);

        $('.institution-data', row).attr('data-phone', data.institution_phone);

        $('.institution-data', row).attr('data-email', data.institution_email);

        $('.institution-data', row).attr('data-state', data.institution_state);

        $('.institution-data', row).attr('data-level', data.education_level);

        $('.institution-data', row).attr('data-type', data.institution_type);

        //...

        $('.change-time', row).attr('data-id', data.id_event);
        
        $('.change-time', row).attr('data-date-start', data.date_start);

        $('.change-time', row).attr('data-date-end', data.date_end);

        //...

        $('.clone-event', row).attr('data-id', data.id_event);
        
        $('.clone-event', row).attr('data-class-name', data.class_name);
        
        $('.clone-event', row).attr('data-date-start', data.date_start);

        $('.clone-event', row).attr('data-date-end', data.date_end);
        
        $('.clone-event', row).attr('data-class-id', data.id_class);

        $('.clone-event', row).attr('data-guide-id', data.id_guide);

        $('.clone-event', row).attr('data-host-id', data.id_host);

        $('.clone-event', row).attr('data-guide-name', data.guide_name);

        $('.clone-event', row).attr('data-guide-surname', data.guide_surname);

        $('.clone-event', row).attr('data-host-name', data.host_name);

        $('.clone-event', row).attr('data-host-surname', data.host_surname);
        
        $('.clone-event', row).attr('data-booking-type', bookingType);

        //...

        $('.set-guide', row).attr('data-id', data.id_event);

        $('.set-guide', row).attr('data-guide-id', data.id_guide);

        $('.set-guide', row).attr('data-guide-id-relation', data.id_guide_relation);

        $('.set-guide', row).attr('data-guide-name', data.guide_name);

        $('.set-guide', row).attr('data-guide-surname', data.guide_surname);

        //...

        $('.list-of-persons', row).attr('data-id', data.id_event);

        //...
        
        $('.set-host', row).attr('data-id', data.id_event);

        $('.set-host', row).attr('data-host-id', data.id_host);

        $('.set-host', row).attr('data-host-id-relation', data.id_host_relation);

        $('.set-host', row).attr('data-host-name', data.host_name);

        $('.set-host', row).attr('data-host-surname', data.host_surname);

        //...

        $('.set-zoom', row).attr('data-id', data.id_event);

        $('.set-zoom', row).attr('data-guide-id', data.id_guide);
       
        $('.set-zoom', row).attr('data-host-id', data.id_host);

        $('.set-zoom', row).attr('data-institution-id', data.id_institution);

        $('.set-zoom', row).attr('data-class-name', data.class_name);

        $('.set-zoom', row).attr('date-readable-date', data.readable_date_start);

        $('.set-zoom', row).attr('data-zoom-id', data.zoom_id);

        $('.set-zoom', row).attr('data-zoom-url', data.zoom_url);

        $('.set-zoom', row).attr('data-zoom-account', data.zoom_account);

        $('.set-zoom', row).attr('data-zoom-password-ref', data.zoom_password_reference);

        $('.set-zoom', row).attr('data-zoom-password-encrypt', data.zoom_password_encrypt);

        $('.set-zoom', row).attr('data-readable-date', data.readable_date_start);
        
        //...

        $('.open-link', row).attr('data-url', data.url);

        $('.sticky-note', row).attr('notes', data.notes);

        $('.sticky-note', row).attr('data-id', data.id_event);

        //...

        $('.delete-item', row).attr('data-id', data.id_event);

        $('.delete-item', row).attr('data-name', data.institution_name);
        
        //...

        $('.schedulle', row).prop('title', 'Conocé los turnos asignados a la institución ' + data.institution_name);
    }

    /**
    *
    * @function showSwal
    * @param msg - the msg.
    * @description Swal alert.
    */

    var showSwal = function(title, msg, type = 'error'){

        utils.alert(title, msg, ["entendido!"], [], type); 

    }

    /**
    *
    * @function setData
    * @param msg - id, sid, relation, url, title, content, itemTable, row, mailing...
    * @description updaters para guias y afitriones...
    */

    //setData(id, guideId, relation, url, title, content, table, row);

    var setData = function(id, sid, relation, url, title, content, itemTable, row, mailing){

        console.log(id, sid, relation, url, title, content, itemTable, row, mailing);

        var request = $.ajax({
                            
            url: url,

            data: {id: id},

            type: "POST",

            dataType: "json",

            success: function(result) {

                helper.unblockStage();

                Swal({

                    title: title,

                    input: 'select',

                    inputOptions: result.items,

                    inputValue: sid,

                    html: content,

                    type: 'success',

                    showCancelButton: true,

                    confirmButtonText: 'Asignar',

                    cancelButtonText: 'salir'

                }).then((result) => {

                    if(result.value != sid && result.value != undefined){

                        var object = {id : relation, table: itemTable, row : row, value: result.value, token:token};

                        formUtils.ajaxCall('private/framework/utils/general/updater.php', object, onSuccess, onError);

                        function onSuccess(data){

                            if(row == 'id_guide' && $('#set-guide-checkbox').prop('checked') == true){

                                var object = {id:id, guideId: result.value, token:token, type:row};

                                formUtils.ajaxCall('private/users/museum/classes/inform_to.php', object, testSuccess, onError);

                                function testSuccess(data){

                                    console.log(data);

                                }

                            }

                            if (row == 'id_host' && $('#set-host-checkbox').prop('checked') == true){

                                var object = {id:id, guideId: result.value, token:token, type:row};

                                formUtils.ajaxCall('private/users/museum/classes/inform_to.php', object, testSuccess, onError);

                                function testSuccess(data){

                                    console.log(data);

                                }

                            }

                            utils.toastr(data.title, data.msg, {}, data.type);

                            table.ajax.reload(null, false); 

                        };

                        function onError(xhr, status, error){ console.log(xhr, status, error); } }

                })

            },

            error: function(request, error) {

                console.log(request, error);

                helper.unblockStage();

            }

        });

    }

    
     /**
      * @function updateValue
      * @description update individual value in DB.
      *
      * @param {int} id                 - object id.
      * @param {string} fieldName       - the field to modify
      * @param {string} newValue        - object value.
      * @param {int} filter             - filter type.
      */

      var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

         helper.blockStage("actualizando...");

         var request = $.ajax({

             url: "private/users/museum/general/update_value.php",

             type: "POST",

             data: {

                id: id,

                fieldName: fieldName,

                newValue: newValue,

                filter: filter
            },

            dataType: "json"

        });

         request.done(function(result) {

             helper.showToastr(result.title, result.msg);

             helper.unblockStage();

             window.location.reload();

             if (callback != undefined) callback(callbackParams);

         });

         request.fail(function(jqXHR, textStatus) {

             helper.unblockStage();

         });
     }

    /**
     * @function findGetParameter
     * @description obtiene parametros...
     */

     var findGetParameter = function(parameterName) {

        var result = null,

        tmp = [];

        location.search.substr(1).split("&").forEach(function(item) {

            tmp = item.split("=");

            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);

        });

        return result;
    }

    /**
     * @function addListeners
     * @description ...
     */

     var addListeners = function(){

        $('body').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

            var id = $(this).attr('data-id');

            var object = {id : id, table: 'museum_virtual_classes_events', row : 'active', value: ((state == true) ? 1 : 0), token:token};

            formUtils.ajaxCall('private/framework/utils/general/updater.php', object, onSuccess, onError);

            function onSuccess(data){

                utils.toastr(data.title, data.msg, {}, data.type)

            };

            function onError(xhr, status, error){ console.log(xhr, status, error); }


        });

        $("#list-modal").on("hidden.bs.modal", function () { actualTab = lastTab; });

        $('body').on('click', '.delete-visitor', function() {

             var name = $(this).attr('data-name');

             var surname = $(this).attr('data-surname');

             var id = $(this).attr('data-id');

             var eventId = $(this).attr('id-event');

             var title = "¿Eliminar a " + name + " " + surname + "?";

             var description = "Al eliminar a  " + name + " " + surname + " quedará disponible un turno.";

             Swal({

                title: title,

                html: description,

                type: 'error',

                showCancelButton: true,

                confirmButtonText: 'Si, eliminar',

                cancelButtonText: 'no, salir'

            }).then((result) => {

                if (result.value != undefined) {

                    var object = {id : id, table: 'museum_virtual_classes_events_inscriptions', row : 'deleted', value: 1, token:token};

                    formUtils.ajaxCall('private/framework/utils/general/updater.php', object, onSuccess, onError);

                    function onSuccess(data){ 

                        utils.toastr(data.title, data.msg, {}, data.type);


                        //...

                        var object = {eventId : eventId, token:token};

                        formUtils.ajaxCall('private/users/museum/classes/remove_individual.php', object, onRemoveSuccess, onError);

                        function onRemoveSuccess(data){

                            console.log("removio bien");
                            
                            console.log(data);

                            $('#tab-3').DataTable().ajax.reload(null, false);    

                            $('#tab-2').DataTable().ajax.reload(null, false);    

                        }

                        function onError(xhr, status, error){ console.log(xhr, status, error); }

                        //...


                                              

                    };

                    function onError(xhr, status, error){ console.log(xhr, status, error); } 

                } 

            })


        });


        $('body').on('click', '.delete-item', function() {

            var name = $(this).attr('data-name');

            var id = $(this).attr('data-id');

            var string;

            if(name == undefined){

                string = "Esta acción eliminará el turno.";

            }else{

                string = "Atención!. Este turno ya fue asignado a la institución <b>" + name + "</b>. Si eliminas el tuno, <b>" + name + "</b> perderá el turno";

            }

            Swal({

                title: "¿Eliminar turno?",

                html: string,

                type: 'error',

                showCancelButton: true,

                confirmButtonText: 'Si, eliminar',

                cancelButtonText: 'no, salir'

            }).then((result) => {

                if (result.value != undefined) {

                    var object = {id : id, table: 'museum_virtual_classes_events', row : 'deleted', value: 1, token:token};

                    formUtils.ajaxCall('private/framework/utils/general/updater.php', object, onSuccess, onError);

                    function onSuccess(data){ 

                        utils.toastr(data.title, data.msg, {}, data.type);

                        if(actualTab == 1){

                            $('#tab-1').DataTable().ajax.reload(null, false); 

                        }else if(actualTab == 2){

                            $('#tab-2').DataTable().ajax.reload(null, false); 


                        }

                        

                    };

                    function onError(xhr, status, error){ console.log(xhr, status, error); } 

                } else {

                }

            })

        });

        $('body').on('click', '.clone-event', function(event) {

            var bookingType = $(this).attr("data-booking-type");

            var forGroups = (bookingType == 1) ? 'true' : 'false';

            idClass = $(this).attr("data-class-id");

            className = $(this).attr("data-class-name");

            idGuide = $(this).attr("data-guide-id");

            guideFullName = $(this).attr("data-guide-name") + " " + $(this).attr("data-guide-surname");

            hostFullName = $(this).attr("data-host-name") + " " + $(this).attr("data-host-surname");

            idHost = $(this).attr("data-host-id");

            var startTime = $(this).attr("data-date-start");

            var endTime = $(this).attr("data-date-end");

            startDate = moment(startTime).format('YYYY-MM-DD');

            endDate = moment(endTime).format('YYYY-MM-DD');

            var fromTime = moment(startTime).format('HH:mm');

            var toTime = moment(endTime).format('HH:mm');

            console.log(idClass, idGuide, idHost, startDate, fromTime, toTime);

            var confirmText = 'Esta acción va a clonar un turno en la clase <strong>' + className + '</strong><br><br>';

            confirmText += 'El turno se asignará el día <strong>' + startDate + '</strong> a las </strong>' + fromTime + '</strong><br><br>';

            if(idGuide != -1){

                confirmText += 'El guía del evento será <strong>' + guideFullName + '</strong><br><br>';

            }

            if(idHost != -1){

               confirmText += 'El host del evento será <strong>' + hostFullName + '</strong><br><br>'; 
            }

            confirmText  += 'Recordá que esta información se puede editar.'

            utils.alert("¿Clonar Evento?", confirmText, ["¡confirmar!", "no, cerrar"], [uploadData, cancel], 'info');

            function uploadData(){

                var dates = new Array();

                dates.push({date:startDate});

                sendDataToServer(idClass, idGuide, idHost, fromTime, toTime, forGroups, dates);

            }

            function cancel(){

            }

        });



        $('body').on('click', '.change-time', function(event) {

            $('.save-description').show();

            saveId = $(this).attr("data-id");

            var startTime = $(this).attr("data-date-start");

            var endTime = $(this).attr("data-date-end");

            startDate = moment(startTime).format('YYYY-MM-DD');

            endDate = moment(endTime).format('YYYY-MM-DD');
            
            finalString = '<div class="form-group m-form__group form-group m-form__group col-12">';

            finalString  += 'Si la institución o el museo decidió cambiar el horario, modificalo desde acá para dejar constancia por sistema.<br><br>';

            finalString  += '<label class="col-form-label">Desde las:<br></label> <div class="col-12">';

            finalString  += '<div class="input-group timepicker" >';
            
            finalString  += '<input type="text" id="from-time" class="form-control m-input" readonly="" placeholder="seleccionar horario de comienzo">';

            finalString  += '<span class="input-group-addon"> <i class="la la-clock-o mt-2"></i> </span></div></div></div>';

            finalString += '<div class="form-group m-form__group form-group m-form__group col-12">';

            finalString  += '<label class="col-form-label">Desde las:<br></label> <div class="col-12">';

            finalString  += '<div class="input-group timepicker" >';
            
            finalString  += '<input type="text" id="to-time" class="form-control m-input" readonly="" placeholder="seleccionar horario de comienzo">';

            finalString  += '<span class="input-group-addon"> <i class="la la-clock-o mt-2"></i> </span></div></div></div>';

            $('#actions-modal').modal('show');

            $('.modal-title').text('cambiar el horario');

            $('.modal-body').html(finalString);

            $("#from-time").val(moment(startTime).format('HH:mm'));
            
            $("#to-time").val(moment(endTime).format('HH:mm'));

            $("#from-time, #to-time").timepicker({minuteStep: 15, showMeridian: false, snapToStep: true});

            $('#from-time').timepicker().on('changeTime.timepicker', function (e) {

                var newhour =(e.time.hours) + 1;

                var newTime = newhour + ':'+ ((e.time.minutes == 0) ? '00' : e.time.minutes);

                //$("#to-time").val(newTime);
            });

            $('#to-time').timepicker().on('changeTime.timepicker', function (e) {

                date = moment(new Date()).format('YYYY-MM-DD');

                var to = date + " " + e.time.hours + ":" + e.time.minutes;

                var from = date + " " + $("#from-time").val();

                if (to <= from) {

                    var newhour =(e.time.hours) - 1;

                    var newTime = newhour + ':'+ ((e.time.minutes == 0) ? '00' : e.time.minutes);

                   // $("#from-time").val(newTime);

                }


            });

            finalStartDate = startDate + " " + $("#from-time").val() + ":00";

            finalEndDate = endDate + " " + $("#to-time").val() + ":00";

        });

        $('body').on('click', '.set-zoom', function(event) {

                var zoomId = ($(this).attr('data-zoom-id') != null) ? $(this).attr('data-zoom-id') : '';

                var zoomUrl = ($(this).attr('data-zoom-url') != null) ? $(this).attr('data-zoom-url') : '';  
                
                var zoomAccount = ($(this).attr('data-zoom-account') != null) ? $(this).attr('data-zoom-account') : '';  
                
                var zoomRef = ($(this).attr('data-zoom-password-ref') != null) ? $(this).attr('data-zoom-password-ref') : '';  
                
                var readable = ($(this).attr('data-readable-date') != null) ? $(this).attr('data-readable-date') : '';  

                var penc = ($(this).attr('data-zoom-password-encrypt') != null) ? $(this).attr('data-zoom-password-encrypt') : '';                          

                isZoomData = true;

                console.log(zoomAccount);

            if(userType == 6 || userType == 4){

                var st;

                if(zoomUrl){

                    st = "Estos son los datos del zoom:<br><br>";

                    st += '<b>zoom account: </b>' + zoomAccount + '<br>';

                    st += '<b>zoom id: </b>' + zoomId + '<br>';

                    st += '<b>zoom url: </b>' + zoomUrl + '<br>';
                    
                    st += '<b>zoom password: </b>' + penc + '<br><br>';

                    st += "No te olvides de conectarte el " + readable;

                
                }else{

                    st = 'Aun no se definieron los datos de este Zoom.';

                }

                showSwal('Datos del Zoom:', st, '');

                

            }else{

                $('.save-description').show();

                formIdEvent = $(this).attr('data-id');
                
                formIdGuide = $(this).attr('data-guide-id');

                formIdHost = $(this).attr('data-host-id');

                formIdInstitution = $(this).attr('data-institution-id');

                className = $(this).attr('data-class-name');

                readableDate = $(this).attr('date-readable-date');                
               
                
                $('#actions-modal').modal('show');

                $('.modal-title').text('DATOS DE ZOOM MEETING');

                var finalString = 'Completá manualmente los datos de la plataforma Zoom para este turno. Estos datos corresponden a la clase <strong>' + className + '</strong> del ' + readableDate + '.<br><br>';

                finalString += '<form id="zoom-form">';

                //...

                finalString += '<div>';

                finalString += '<label class="form-check-label" for="zoom-url">Cuenta:</label>';

                finalString += '<input type="text" class="form-control" name="zoom-account" autocomplete="off" placeholder="MUSEO DEL HOLOCAUSTO 2" value = "' + zoomAccount + '">';

                finalString += '</div><br>';


                //...

                finalString += '<div>';

                finalString += '<label class="form-check-label" for="zoom-url">URL del zoom:</label>';

                finalString += '<input type="text" class="form-control" name="zoom-url" autocomplete="off" placeholder="https://us04web.zoom.us" value = "' + zoomUrl + '">';

                finalString += '</div><br>';

                finalString += '<div>';

                finalString += '<label class="form-check-label" for="zoom-id">Meeting ID:</label>';

                finalString += '<input type="text" class="form-control" name="zoom-id" autocomplete="off" placeholder="000 0000 0000" value = "' + zoomId + '">';

                finalString += '</div><br>';

                finalString += '<div>';

                finalString += '<label  data-toggle="tooltip" title="Por motivos de seguridad, no almacenamos el password plano en la base de datos." data-placement="top" class="form-check-label" for="zoom-password">Meeting Password:</label>';

                finalString += '<input type="text" class="form-control" name="zoom-password" autocomplete="off" placeholder="'  + penc + '">';

                finalString += '</div><br><br>';
                
                if(formIdInstitution != undefined || formIdHost != -1 || formIdGuide != -1) finalString += 'Podés tildar los siguientes checks para enviar mailings:<br><br>';

                if(formIdInstitution != undefined) finalString += '<div class="form-check"><input class="form-check-input" type="checkbox" value="" id="institution-mailing-checkbox"><label class="form-check-label" for="institution-mailing-checkbox">avisarle por email a la institución.</label></div><br>';

                if(formIdHost != -1) finalString += '<div class="form-check"><input class="form-check-input" type="checkbox" value="" id="host-mailing-checkbox"><label class="form-check-label" for="host-mailing-checkbox">avisarle por email al anfitrión.</label></div><br>';

                if(formIdGuide != -1) finalString += '<div class="form-check"><input class="form-check-input" type="checkbox" value="" id="guide-mailing-checkbox"><label class="form-check-label" for="guide-mailing-checkbox">avisarle por email al guía.</label></div><br>';

                 finalString += '</form>';

                $('.modal-body').html(finalString);

                $('[data-toggle="tooltip"]').tooltip();


                setZoomForm();

            }

        });




        $('body').on('click', '.set-host', function(event) {

            if(userType == 4){

                var hostName = $(this).attr("data-host-name");

                var hostSurname = $(this).attr("data-host-surname");

                var st;

                if(hostName == undefined && hostSurname == undefined){

                    st = 'Opss! aun no se asignó el anfitrión para este evento. Pronto será asignado.';

                }else{

                    st = "El anfitrión asignado para este evento es <b>" + hostName + " " + hostSurname + "</b>";

                }

                
                showSwal('El Anfitrión:', st, '');

            }else{

                var id = $(this).attr("data-id");

                var hostId = parseInt($(this).attr("data-host-id"));

                var hostRelation = parseInt($(this).attr("data-host-id-relation"));

                var url = "private/users/services/getListOfHosts.php";

                var title = "Seleccionar anfitrión";

                var content = 'Selecciona un anfitrión de la lista. El mismo quedará asignado al turno.<br><br><div class="form-check"><input class="form-check-input" type="checkbox" value="" id="set-host-checkbox"><label class="form-check-label" for="set-host-checkbox">avisarle por email</label></div>';

                var table = 'museum_virtual_classes_events_hosts_relations';

                var row = 'id_host'; 

                var relation = parseInt($(this).attr("data-host-id-relation"));

                setData(id, hostId, relation, url, title, content, table, row);

            }

        });

        $('body').on('click', '.set-guide', function(event) {

            if(userType == 6){

                var guideName = $(this).attr("data-guide-name");

                var guideSurname = $(this).attr("data-guide-surname");

                var st;

                if(guideName == undefined && guideSurname == undefined){

                    st = 'Opss! aun no se asignó el guía para este evento. Pronto será asignado.';

                }else{

                    st = "El guía asignado para este evento es <b>" + guideName + " " + guideSurname + "</b>";

                }

                
                showSwal('El guía:', st, '');

            }else{

                var id = $(this).attr("data-id");

                var guideId = parseInt($(this).attr("data-guide-id"));

                var relation = parseInt($(this).attr("data-guide-id-relation"));

                var url = "private/users/services/getListOfGuides.php";

                var title = "Seleccionar guía";

                var content = 'Selecciona un guía de la lista. El mismo quedará asignado al turno.<br><br><div class="form-check"><input class="form-check-input" type="checkbox" value="" id="set-guide-checkbox"><label class="form-check-label" for="set-guide-checkbox">avisarle por email</label></div>';

                var table = 'museum_virtual_classes_events_guides_relations';

                var row = 'id_guide'; 

                var mailing = 'avisarle al guía por email';

                setData(id, guideId, relation, url, title, content, table, row);

            }

        });

        


        $('body').on('click', '.change-booking', function() {

            showSwal('Cambiar turno', 'En breve podrás cambiar el turno desde acá.', 'success');

        });
        
        $('body').on('click', '.sticky-note', function() {

            var item = $(this);

            var notes = $(this).attr('notes');

            if(notes == undefined) notes = "";

            var id =  $(this).attr('data-id');

            Swal.fire({

                title : 'Asignar notas.',

                html: 'Podes utilizar este espacio para crear notas relativas al evento.',

                input: 'textarea',

                inputValue: notes,
                
                inputPlaceholder: 'Escribir notas',
                
                showCancelButton: true

            }).then((queryResult) => {

                if(queryResult.value != undefined){

                    helper.unblockStage();

                    var object = {id : id, table: 'museum_virtual_classes_events', row : 'notes', value: queryResult.value, token:token};

                    formUtils.ajaxCall('private/framework/utils/general/updater.php', object, onSuccess, onError);

                    function onSuccess(data){ 

                        utils.toastr(data.title, data.msg, {}, data.type);
                        
                        table.ajax.reload(null, false); 

                    };

                    function onError(xhr, status, error){ console.log(xhr, status, error); }

                }

            })

        });


        $('body').on('click', '.inscription-contact-data', function() {

            $('.save-description').hide();

            isContact = true;

            var contactName = $(this).attr('data-contact-name');

            console.log("contactName: " + contactName);

            if(contactName == undefined){

                showSwal('turno sin asignar', 'Este turno todavía no fue tomado por ninguna institución: no hay información para mostrar.', 'warning');

            }else{

                var contactPhone = $(this).attr('data-contact-phone');

                var contactEmail = $(this).attr('data-contact-email');

                var firstTime = $(this).attr('data-first-time');

                var origin = $(this).attr('data-origin');

                var institutionFirstTime = $(this).attr('data-institution-first-time');

                var finalString = '<strong>' + contactName + '</strong> es la persona a cargo que definió la institución. Si querés contactarlo, podés hacerlo por email o telefónicamente.<br><br>' 

                finalString += '<strong>Email de contacto: </strong><a href="mailto:' + contactEmail + '">' + contactEmail + '</a><br>';

                finalString += '<strong>Teléfono: </strong><a href="te:' + contactPhone + '">' + contactPhone + '</a><br><br>';

                $('#actions-modal').modal('show');

                $('.modal-title').text('PERSONA DE CONTACTO');

                $('.modal-body').html(finalString);

            }

        });


        $('body').on('click', '.institution-data', function() {

           $('.save-description').show();

           isInstitution = true;

           $('.save-description').text("editar");

            var name = $(this).attr('data-name');

            console.log(">>>" + name);
            
            tempInstitutionId = $(this).attr('data-institution-id');

            if(name == undefined){

                showSwal('turno sin asignar', 'Este turno todavía no fue tomado por ninguna institución: no hay información para mostrar.', 'warning');

            }else{

                var students = $(this).attr('data-students');

                var cuit = ($(this).attr('data-cuit') == undefined) ? ' - ' : $(this).attr('data-cuit');

                var institutionPhone = ($(this).attr('data-institution-phone') == undefined) ? ' - ' : $(this).attr('data-institution-phone');

                var institutionEmail = ($(this).attr('data-institution-email') == undefined) ? ' - ' : $(this).attr('data-institution-email');

                var iva = $(this).attr('data-iva');

                var address = $(this).attr('data-address');

                var email = ($(this).attr('data-email') == undefined) ? '-' : $(this).attr('data-email');
                
                var phone = ($(this).attr('data-phone') == undefined) ? '-' : $(this).attr('data-phone');

                var state = ($(this).attr('data-state') == undefined) ? '-' : $(this).attr('data-state');

                var level = $(this).attr('data-level');

                var type = $(this).attr('data-type');

                var needs = $(this).attr('data-needs');

                var finalString = 'La institiución <strong>' + name + '</strong> solicitó turnos para <strong>' + students + ' alumnos</strong>.<br><br>';

                //finalString += '<br><b>IMPOSITIVO:</b><br><br>';

                finalString += '<strong>NOMBRE DE LA INSTITUCIÓN: </strong>' + name + '<br><br>';

                finalString += '<strong>CUIT: </strong>' + cuit + '<br><br>';

                finalString += '<strong>EMAIL: </strong>' + institutionEmail + '<br><br>';

                finalString += '<strong>TELÉFONO: </strong>' + institutionPhone + '<br><br>';

                finalString += '<strong>ESTUDIANTES: </strong>' + students + '<br><br>';

                finalString += '<strong>DIRECCIÓN: </strong>' + address + '<br><br>';

                finalString += '<strong>TELÉFONO: </strong>' + phone + '<br><br>';

                finalString += '<strong>EMAIL: </strong><a href="mailto:' + email + '">' + email + '</a><br><br>';

                finalString += '<strong>PROVINCIA: </strong>' + state + '<br><br>';

                finalString += '<strong>NIVEL: </strong>' + level + '<br><br>';

                finalString += '<strong>TIPO DE LA INSTITUCIÓN: </strong>' + type + '<br><br>';

                $('#actions-modal').modal('show');

                $('.modal-title').text(name.toUpperCase());

                $('.modal-body').html(finalString);

            }

        });


        $('body').on('click', 'tr', function() {

             /*var tr = $(this).closest('tr');

             var row = table.row( tr );

             console.log(row);*/

             //table.columns.adjust().draw();


            $("[name='my-scholarship-checkbox']").bootstrapSwitch();

            $("[name='my-payment-checkbox']").bootstrapSwitch();

            $("[name='my-upload-checkbox']").bootstrapSwitch();

            $("[name='my-assisted-checkbox']").bootstrapSwitch();

        });

         
        $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {

            $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();

            e.preventDefault();

            actualIdentifier = $(e.target).attr("href");

            if(actualIdentifier == '#tab_1'){
                
                actualTab = 1;

            }else if(actualIdentifier == '#tab_2'){

                actualTab = 2;
            
            }else if(actualIdentifier == '#tab_3'){

                actualTab = 3;
            }

            history.pushState(null, null, actualIdentifier);

        });

        $('.export-excel').click(function(){ showSwal("Descargar Excel", 'Proximamente. Estamos trabajando en esto.', ''); })



        
        $('.add-date').click(function(){

            console.log("check this: " + date);

            isAddingDate = true;

            $('#actions-modal').modal('show');

            $('.modal-title').text('¿agregar turno?');

            var theClass = $(this).attr('data-class-name');

            var theDate = $(this).attr('date-readable-date');

            var finalString = 'Se va a añadir un turno a la clase virtual para la fecha ' + date + '<br><br>';
            
            finalString += '<div class="form-group m-form__group form-group m-form__group col-12">';

            finalString += '<label class="col-form-label">Seleccioná la clase virtual a asignar fechas:<br></label>';

            finalString += '<select id = "list_of_classes_picker" name = "list_of_classes_picker" title = "Seleccioná la clase virtual" class = "form-control m-bootstrap-select list_of_classes_picker"></select>';

            finalString += '</div>'

            //start at...

            finalString += '<label class="col-form-label">Desde las:<br> </label>';
            
            finalString += '<div class="col-12">';

            finalString += '<div class="input-group timepicker">';

            finalString += '<input autocomplete="off" type="text" id="from-time-single" class="form-control m-input" readonly="" placeholder="seleccionar horario de comienzo">';

            finalString += '<span class="input-group-addon">';

            finalString += '<i class="la la-clock-o mt-2"></i>';

            finalString += '</span>';

            finalString += '</div>';

            finalString += '</div><br>';

            //end at...

            finalString += '<label class="col-form-label">Desde las:<br> </label>';
            
            finalString += '<div class="col-12">';

            finalString += '<div class="input-group timepicker">';

            finalString += '<input autocomplete="off" type="text" id="to-time-single" class="form-control m-input" readonly="" placeholder="seleccionar horario de comienzo">';

            finalString += '<span class="input-group-addon">';

            finalString += '<i class="la la-clock-o mt-2"></i>';

            finalString += '</span>';

            finalString += '</div>';

            finalString += '</div><br>';

            //guide...

            finalString += '<div class="form-group m-form__group form-group m-form__group col-12">';

            finalString += '<label class="col-form-label">Si sabés quien va a ser el guía, asignalo:<br></label>';

            finalString += '<select id = "list_of_guides_picker" name = "list_of_guides_picker" title = "Seleccioná la clase virtual" class = "form-control m-bootstrap-select list_of_guides_picker"></select>';

            finalString += '</div><br>'

            //host...

            finalString += '<div class="form-group m-form__group form-group m-form__group col-12">';

            finalString += '<label class="col-form-label">Si sabés quien va a ser el anfitrión, asignalo:<br></label>';

            finalString += '<select id = "list_of_hosts_picker" name = "list_of_hosts_picker" title = "Seleccioná la clase virtual" class = "form-control m-bootstrap-select list_of_hosts_picker"></select>';

            finalString += '</div>'

            //type...

            finalString += '<label class="col-form-label"> ¿Es para colegios/grupos?: <br> </label>';

            finalString += '<div class="col-lg-6 col-md-9 col-sm-12">';

            finalString += '<div class="m-form__group form-group row">';

            finalString += '<div class="col-3">';

            finalString += '<span class="m-switch m-switch--outline m-switch--icon m-switch--success">';

            finalString += '<label> <input type="checkbox" class="highlighted for-groups" name="for-groups"  data-on="Enabled" data-id="" checked>';

            finalString += '<span></span> </label> </span> </div> </div> </div>';

            //...

            $('.modal-body').html(finalString);

            getPickerData($('#list_of_classes_picker'), 'private/users/museum/classes/get_list_of_virtual_classes.php');
            
            getPickerData($('#list_of_guides_picker'), 'private/users/services/getListOfGuides.php');
            
            getPickerData($('#list_of_hosts_picker'), 'private/users/services/getListOfHosts.php');

            $("#from-time-single, #to-time-single").timepicker({minuteStep: 15, showMeridian: false, snapToStep: true});

            var now = new Date();

            now.setHours(now.getHours() + 2);

            $("#to-time-single").val(moment(now).format("HH") + ":00");// + moment(now).format("mm"));

            $('#from-time-single').timepicker().on('changeTime.timepicker', function (e) {

                var newhour =(e.time.hours) + 1;

                var newTime = newhour + ':'+ ((e.time.minutes == 0) ? '00' : e.time.minutes);

                console.log(newTime);

                //$("#to-time-single").val(newTime);
            });

            $('#to-time-single').timepicker().on('changeTime.timepicker', function (e) {

                var tempDate = moment(new Date()).format('YYYY-MM-DD');

                var to = tempDate + " " + e.time.hours + ":" + e.time.minutes;

                var from = tempDate + " " + $("#from-time-single").val();

                if (to <= from) {

                    var newhour =(e.time.hours) - 1;

                    var newTime = newhour + ':' + ((e.time.minutes == 0) ? '00' : e.time.minutes);

                    //$("#from-time-single").val(newTime);

                }


            });


        });


        $('.save-description').click(function(){

            if(isInstitution){

                isInstitution = false;

                window.location.href = "museum_institution_add.php?id=" + tempInstitutionId;

            }else if(isAddingDate == true){

                var idClass = $("#list_of_classes_picker").val();

                var idGuide = $("#list_of_guides_picker").val();

                var idHost = $("#list_of_hosts_picker").val();

                var fromTime = $("#from-time-single").val();

                var toTime = $("#to-time-single").val();

                var forGroups = ($('.for-groups').prop('checked') == true) ? 'true' : 'false';

                var className = $("#list_of_classes_picker").find("option[value='" + $("#list_of_classes_picker").val() + "']").text();
                
                var guideName = $("#list_of_guides_picker").find("option[value='" + $("#list_of_guides_picker").val() + "']").text();
                
                var hostName = $("#list_of_hosts_picker").find("option[value='" + $("#list_of_hosts_picker").val() + "']").text();

                if(idClass == ""){

                    utils.alert("ops! falta la clase!", "Tenés que indicar la clase virtual a la cual pertenece el turno que vas a asignar.");
                    
                }else{

                    var confirmText = "bueno, bien! Respiramos profundo, nos tomamos unos segundiso, y chequeamos los datos:<br><br>";

                    confirmText += "Vas a dar de alta un turno para la clase <strong>" + className + "</strong><br><br>";
                    
                    confirmText += "El turno tendrá lugar el  <strong>" + date + "</strong> desde las <strong>" + fromTime  + " a las " + toTime + "</strong><br><br>";

                    if(idGuide != "") confirmText += "El guía de la visita será <strong>" + guideName + "</strong>. Recordá que esta información la podés cambiar luego.<br><br>";

                    if(idHost != "") confirmText += "El anfitrión de la visita será <strong>" + hostName + "</strong>. Al igual que el guía, esta información también la podés cambiar luego.<br><br>";

                    confirmText += "Por favor, presioná <strong>confirmar</strong> para dar de alta estos datos.";

                    utils.alert("¡Repasemos!", confirmText, ["¡confirmar!", "no, cerrar"], [uploadData, cancel], 'info');

                    function uploadData(){

                        if(idGuide == "") idGuide = -1;

                        if(idHost == "") idHost = -1;

                        var dates = new Array();

                        dates.push({date:date});

                        sendDataToServer(idClass, idGuide, idHost, fromTime, toTime, forGroups, dates);
                        
                        $('#actions-modal').modal('hide');


                    }

                    function cancel(){ }
                    

                    
                }

            }else if(isZoomData){

                if($('#zoom-form').valid()){

                    $('#zoom-form').submit();

                }

            }else{

                finalStartDate = startDate + " " + $("#from-time").val() + ":00";

                finalEndDate = endDate + " " + $("#to-time").val() + ":00";

                updateValue(saveId, ['date_start', 'date_end'], [finalStartDate, finalEndDate], 'museum_virtual_classes_events');

                $('#actions-modal').modal('hide');

                $('.save-description').hide();

            }

        });

        $('#date-from').datepicker({orientation: 'bottom', autoclose: true, language: 'es',  format: readableDateFormat});

        $("#date-from").datepicker("update", moment(date).format('DD-MM-YYYY'));
        
        $('#date-to').datepicker({orientation: 'bottom', autoclose: true, language: 'es',  format: readableDateFormat, startDate:moment(date).format('DD-MM-YYYY')});

        $("#date-to").datepicker("update", moment(dateTo).format('DD-MM-YYYY'));

 
        $('#date-from').datepicker().on('changeDate', function (ev) {

            var dateFrom = $('#date-from').data('datepicker').getFormattedDate('yyyy-mm-dd');

            window.location.href = "museum_virtual_classes_dates_list.php?date=" + dateFrom;

        });

        $('#date-to').datepicker().on('changeDate', function (ev) {

            var dateFrom = $('#date-from').data('datepicker').getFormattedDate('yyyy-mm-dd');

            var dateTo = $('#date-to').data('datepicker').getFormattedDate('yyyy-mm-dd');

            window.location.href = "museum_virtual_classes_dates_list.php?date=" + dateFrom + "&dateTo=" + dateTo;

            console.log(dateFrom + " " + dateTo);


            //window.location.href = "museum_virtual_classes_dates_list.php?date=" + dateFrom;

        });


        $('.go-to-today').click(function(){

            $finalURL = 'museum_virtual_classes_dates_list.php?date=' + $('#today').val();

            window.location.href = $finalURL;

        });

        $('.go-to-tomorrow').click(function(){

            $finalURL = 'museum_virtual_classes_dates_list.php?date=' + $('#tomorrow').val();

            window.location.href = $finalURL;

        });

        $('.go-to-day-after-tomorrow').click(function(){

            $finalURL = 'museum_virtual_classes_dates_list.php?date=' + $('#dayAfterTomorrow').val();

            window.location.href = $finalURL;

        });

        $('body').on('click', '.list-of-persons', function(event) {

            $('#tab-3').DataTable().destroy();

            createDataTable($('#tab-3'), 3, $(this).attr("data-id"));

            $('#list-modal').modal('show');

            lastTab = actualTab;

            actualTab = 3;



        });
    }

    /**
     *
     * @function sendDataToServer
     * @description send data...
     */

    var sendDataToServer = function(idClass, idGuide, idHost, fromTime, toTime, forGroups, dates){

        console.log("start");

    
        helper.blockStage("almacenando fecha.");

        console.log(idClass, idGuide, idHost, fromTime, toTime, forGroups, dates);

        console.log(fromTime);

        console.log(toTime);

        console.log(dates);

        //return;

        var formData = new FormData();

        formData.append("id", idClass);

        formData.append("guide", idGuide);

        formData.append("host", idHost);

        formData.append("dates", JSON.stringify(dates));

        formData.append("fromTime", fromTime);
        
        formData.append("toTime", toTime);

        formData.append("forGroups", forGroups);

        for (var pair of formData.entries()) { console.log(pair[0]+ ', ' + pair[1]); }

            
        var request = $.ajax({

            url: "private/users/museum/classes/class_add_batch.php",

            type: "POST",

            contentType: false,

            processData: false,

            data: formData,

            dataType: "json"
        });

        request.done(function(response) {

            isAddingDate = false;
            
            helper.unblockStage();

            helper.showToastr("turno añadido!", 'se añadió exitosamente el turno al día');

            location.reload();


            //table.ajax.reload(null, false); 

        });

        request.fail(function(jqXHR, textStatus) {

            isAddingDate = false;

            helper.showToastr("ops", 'error');

            helper.unblockStage();

        });

    }

    /**
    *
    * @function getPickerData();
    * @description obtener todas las clases virtuales...
    */

    var getPickerData = function(item, url){

        var request = $.ajax({

            url: url,

            type: "POST",

            dataType: "json",

            success: function(result) {

                var options = [];

                for (x in result.items) {

                    var option = "<option value=" + x + ">" + result.items[x] + "</option>";

                    options.push(option);

                }

                item.html(options);

                item.selectpicker('refresh');


            },

            error: function(request, error) {

                console.log(request);

                console.log("error");

            }

        });

    }


    /**
     *
     * @function setZoomForm
     * @description control para el form de zoom...
     */

    var setZoomForm = function(){

        $('#zoom-form').validate({

            rules: { 

                'zoom-url': { required: true, url:true },

                'zoom-account': { required: true },

                'zoom-id': { required: true, minlength:10 },

                'zoom-password': { required: true, minlength:5 }

            },
            messages: {

                'zoom-account':{

                    required: helper.createErrorLabel('url account', 'REQUIRED')
                },

                'zoom-url':{

                    required: helper.createErrorLabel('url zoom', 'REQUIRED'),

                    url: helper.createErrorLabel('url zoom', 'INVALID_URL')
                },

                'zoom-id': {

                    required: helper.createErrorLabel('Meeting ID', 'REQUIRED'), 

                    minlength: helper.createErrorLabel('Meeting ID', 'MIN_LENGTH', [10]), 

                },

                'zoom-password': {

                    required: helper.createErrorLabel('Meeting Password', 'REQUIRED'), 

                    minlength: helper.createErrorLabel('Meeting Password', 'MIN_LENGTH', [5]), 

                }

            },

            invalidHandler: function(e, r) { },

            submitHandler: function(form, event) {



                var sendEmailtoInstitution = ($('#institution-mailing-checkbox').is(":checked")) ? 1 : 0;
                
                var sendEmailToHost = ($('#host-mailing-checkbox').is(":checked")) ? 1 : 0;

                var sendEmailToGuide = ($('#guide-mailing-checkbox').is(":checked")) ? 1 : 0;

                console.log(sendEmailtoInstitution, sendEmailToHost, sendEmailToGuide);

                event.preventDefault();

                event.stopPropagation();

                helper.blockStage("actualizando datos...");

                var formData = new FormData(form);

                formData.append("idEvent", formIdEvent);

                formData.append("idGuide", formIdGuide);

                formData.append("idHost", formIdHost);

                formData.append("idInstitution", formIdInstitution);

                formData.append("className", className);

                formData.append("readableDate", readableDate);
                
                formData.append("token", token);

                formData.append("sendEmailtoInstitution", sendEmailtoInstitution);

                formData.append("sendEmailToHost", sendEmailToHost);

                formData.append("sendEmailToGuide", sendEmailToGuide);

                var request = $.ajax({

                    url: "private/users/museum/classes/class_add_zoom_data.php",

                    type: "POST",

                    contentType: false,

                    processData: false,

                    data: formData,

                    dataType: "json"
                });

                request.done(function(response) {

                    helper.showToastr("Datos actualizados correctamente", 'Zoom Meeting');

                    helper.unblockStage();

                    $('#actions-modal').modal('hide');

                    $('.save-description').hide();

                    isZoomData = false;

                    table.ajax.reload(null, false); 

                });

                request.fail(function(jqXHR, textStatus) {

                    console.log("error");

                    console.log(jqXHR, textStatus);
                });

            }

        });
    }

    /**
     *
     * @function getUserVariables();
     * @description variables....
     */

    var getUserVariables = function(){
        
        token = $('#token').val();

        userType = $('#user-type').val();

        userId = $('#user-id').val();



    }

    return {


        init: function() {


            //$.fn.dataTable.moment( 'M/D/YYYY hh:mm:ss A' );

            helper.setMenu();
            
            date = findGetParameter('date');

            dateTo = findGetParameter('dateTo');

            if(date == null) date = moment(new Date()).format('YYYY-MM-DD');

            console.log(dateTo);
            
            //if(dateTo == null) dateTo = moment(new Date()).format('YYYY-MM-DD');

            getUserVariables();

            addListeners();
            
            createDataTable($('#tab-1'), 1);

            createDataTable($('#tab-2'), 2);

            for(var j = 0; j < nationalHolidays.length; j++){

                if(nationalHolidays[j].date == date){

                    var title = 'Ops! esta fecha es feriado!';

                    var content = 'El ' + date + ' es feriado: ' + nationalHolidays[j].description + '<br><br> Tené en consideración que posiblemente las instituciones no dicten actividades este día.<br><br>Si tenes turnos asignados en este día y querés eliminarlos, lo podés hacer desde la lista cliqueando el iconito de <b>eliminar: <i class="fas fa-trash"></i></b>';

                    showSwal(title, content, '');

                }

            }

        }

    };

}();

jQuery(document).ready(function() { InitModule.init(); });