/**
 * @summary Add new news.
 *
 * @description -
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

/**
 * @function MuseumNewsAdd
 * @description Initialize and include all the methods of this class.
 */

 var MuseumNewsAdd = function() {

    helper = Helper();

    var editMode;

    var redirectId;

    var newsId;

    var newsTitle;

    var addRedirectId = 0;

    var form = $('#add-data-form');

    var origin;

    /**
     * @function addFormValidations
     * @description Asign all the form validations for this page.
     */

     var addFormValidations = function() {

        $.validator.addMethod("maxlength", function (value, element, len) {

            return value == "" || value.length <= len;

        });

        form.validate({

            rules: {

                email: { required: true, email: true },

                name: { required: true },
                
                surname: { required: true },

                pass: { required: true, maxlength:8 },
                
                'calendar-date': { required: true },

            },

            messages: {

                email:{

                    email: helper.createErrorLabel('email', 'EMAIL'),
                    
                    required: helper.createErrorLabel('email', 'REQUIRED')

                },

                name: helper.createErrorLabel('nombre', 'REQUIRED'),

                pass:{
                    
                    required: helper.createErrorLabel('pass', 'REQUIRED'),

                    maxlength: '8'

                },


                surname: helper.createErrorLabel('apellido', 'REQUIRED'),

                'calendar-date': helper.createErrorLabel('fecha', 'REQUIRED')

            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();

                window.scroll({top: 0, left: 0, behavior: 'smooth' });
           
            },

            submitHandler: function(form, event) {

                event.preventDefault();

                event.stopPropagation();

                helper.blockStage("actualizando datos...");

                var formData = new FormData(form);

                var request = $.ajax({

                    url: "private/users/museum/classes/add_full_access.php",

                    type: "POST",

                    contentType: false,

                    processData: false,

                    data: formData,

                    dataType: "json"

                });

                request.done(function(response) {

                    console.log(response.str);

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: true,

                        confirmButtonText: response.button,

                        cancelButtonText: 'añadir nuevo'

                    }).then((result) => {

                        if(result.value == true){

                            switch (Number(response.status)) {

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                
                                window.location.replace("museum_news_add.php?id=" + response.id);

                                break;

                            }

                         } else if (result.dismiss === Swal.DismissReason.cancel) {

                            window.location.reload();

                         }


                    });


                    helper.showToastr("Se acualizo", 'Se actualizo');

                    helper.unblockStage();

                });

                request.fail(function(jqXHR, textStatus) {

                    console.log(jqXHR);

                    console.log("error");

                    helper.showToastr("ops", 'error');

                    helper.unblockStage();

                });

            }

        });
    }

    var makeid = function(length) {

        var result           = '';

        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        var charactersLength = characters.length;

        for ( var i = 0; i < length; i++ ) {

            result += characters.charAt(Math.floor(Math.random() * charactersLength));

        }

        return result;

    }


    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

     var addListeners = function() {


        $('#create-pass').click(function(){ $('#pass').val(makeid(8)); });

        $('#calendar-date').datepicker({ autoclose: true, language: 'es',  format: 'yyyy-mm-dd'});

        if(!editMode){

             var today = new Date();
            
            var nextweek = new Date(today.getFullYear(), today.getMonth(), today.getDate()+7);

            $('#datepicker').datepicker('update', nextweek);

        }

        if(editMode){

            $('#datepicker').on('changeDate', function() {

                var newValue = $('#calendar-date').datepicker('getFormattedDate');//.getFormattedDate('yyyy-mm-dd');

                console.log(newValue);

                //updateValue(newsId, 'added', newValue, 'museum_news');

            });

        }

        $('#title').on('input', function(e) {

            if (editMode) return;

            $('#validate-title').addClass('btn-info');

            $('#validate-title').removeClass('btn-success');

            $('#validate-title').text('validar');

            if ($(this).val() == "") {

                $('#validate-title').attr('disabled', true);

            } else {

                $('#validate-title').attr('disabled', false);

            }

        });

        $('#email').on('blur', function() {

            if ($(this).attr('id') == 'email' && $(this).val() != "") {

                validateEmail($('#email').val());

            }

        });

        $('#validate-title').click(function(event) {

            if ($('#title').val() != "") validateEmail($('#email').val());

        })

        $('[data-toggle="tooltip"]').tooltip();

        
        $('#exit_from_form').click(function(e) { window.location.replace("index.php"); });

        $('#back_to_list').click(function(e) { window.location.replace("museum_360_access_list.php?tabId=" + redirectId); });

        $(".update-btn").click(function() {

            if (!editMode) return;

            var dbName = $(this).attr('data-db-value');

            var id = $(this).attr('data-id');

            var newValue = $(this).parent().parent().find(':input').val();

            var fieldName = $(this).parent().parent().find(':input').attr('name');

            var valid = $('#' + fieldName).valid();

            if (dbName != newValue && valid) {

                console.log("changing something!");

                $(this).attr('data-db-value', newValue);

                updateValue(id, fieldName, newValue, 'museum_news');

            }

        });

    }
 

    /**
     * @function window.onbeforeunload
     * @description scroll to top
     */

     window.onbeforeunload = function () { if(!editMode) window.scrollTo(0,0); }

    /**
     * @function validateEmail
     * @description Send the URL and the permalink to validate.
     *
     * @param {String} name                 - The name of the new item.
     * @param {String} url                  - The url of the new item.
     */

     var validateEmail = function(name, url) {
        
        return;

        console.log("validando!");

        helper.blockStage("validando email...");

        var request = $.ajax({

            url: "private/users/services/validate_field.php",

            type: "POST",

            data: {

                name: name,

                url: url,

                table: 'museum_news'

            },

            dataType: "json"
        });

        request.done(function(result) {

            if (result.valid) {

                helper.showToastr("VALIDO! :)", "El nombre <b>" + name + "</b> está disponible para su uso.");

                $('#validate-title').removeClass('btn-info');

                $('#validate-title').addClass('btn-success');

                $('#validate-title').text('valido!');

            } else {

                $('#validate-title').addClass('btn-info');

                $('#validate-title').text('validar');

                Swal({

                    title: 'Ya existe una noticia con ese título.',

                    html: 'Ya existe un link creado con la URL <b>' + url + '</b><br>No pueden haber dos noticias con la misma dirección web.',

                    type: 'error',

                    confirmButtonText: 'Entendido!'

                }).then((result) => {

                    $("#title").val('');

                    $("#title").focus();

                });

            }

            helper.unblockStage();

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

            helper.unblockStage();

        });

    }

    return {

        init: function() {
            
            editMode = ($('#edit-mode').val() == 1) ? true : false;

            if (editMode) {

                newsId = $('#news-id').val();

                newsTitle = $('#news-name').val();

            }

            addListeners();

            addFormValidations();

            $('#pass').val(makeid(8));

        }

    };

}();

jQuery(document).ready(function() { MuseumNewsAdd.init(); });
