/**
 * @summary historical! 
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
 * @function MuseumBookingList
 * @description Initialize and include all the methods of this class.
 */
 var MuseumBookingList = function() {

    helper = new Helper();

    var readableDateFormat = "dd-mm-yyyy";

    var formUtils = new FormUtils();

    var utils = new Utils();

    var saveId;

    var finalStartDate;

    var finalEndDate;

    var startDate;

    var endDate;

    var date;

    var table;

    /**
     * @function createDataTable
     * @description Create all the datatables that we use as lists.
     */

     var createDataTable = function(value, bookingType) {


        table = value.DataTable({

            "language": helper.getDataTableLanguageConfig(),

            "processing": true,

            "serverSide": true,

            'serverMethod': 'post',

            "ajax": {

                url: "private/users/museum/classes/dataControllerGroups.php",

                dataSrc: "data",

                dataType: 'json',

                "data": function(outData) {

                    outData.table = "museum_virtual_classes";

                    outData.searchColumn = "name";

                    outData.searchColumnTwo = "bio";

                    return outData;
                },

                dataFilter: function(inData) { return inData; },

                error: function(err, status) {},
            },


            "columns": [

            { orderable: false, width: "80%", responsivePriority: 2,

            "defaultContent": '',

            render: function(data, type, row) {

                var st;

                if(row.name.length >=30){

                    st = row.name.substr(0, 30) + "...";
                
                }else{

                    st = row.name;

                }

                return st;
            }
        },

        {
            "defaultContent": '',

            render: function(data, type, row) {

                console.log(row);

                var disabled = (parseInt(row.deleted) == 1) ? "disabled" : "";

                var checked = (parseInt(row.active) == 1) ? 'checked="checked"' : '';

                var disabled = (parseInt(row.deleted) == 1) ? "disabled" : "";

                var onColor = (parseInt(row.deleted) == 1) ? "default" : "success";

                var offColor = (parseInt(row.deleted) == 1) ? "default" : "danger";

                var onLabel = (parseInt(row.deleted) == 1) ? "desactivado" : "online";

                var offLabel = (parseInt(row.deleted) == 1) ? "desactivado" : "offline";

                return '<input name="my-checkbox" data-id= ' + row.id + ' class="status-toggle" data-switch="true" ' + disabled + '  type="checkbox" ' + checked + ' data-on-color="' + onColor + '" data-off-color="' + offColor + '" data-on-text="' + onLabel + '" data-handle-width="50" data-off-text="' + offLabel + '" data-size="mini">';
               
                
            }
        },

        {

            orderable: false, width: "8%", responsivePriority: 1,

            "defaultContent": '',

            render: function(data, type, row) {

                var renderString = '<td><span>';


                renderString += '<button data-id = "' + row.id + '" data-toggle="tooltip" data-placement="bottom" title="Editar Clase" class="edit-item m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-edit"></i></button>';

                renderString += '<button data-id = "' + row.id + '" data-toggle="tooltip" data-placement="bottom" title="Editar Imágenes" class="add-images m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-images"></i></button>';

                renderString += '<button data-id = "' + row.id + '" data-toggle="tooltip" data-placement="bottom" title="Editar Videos" class="add-videos m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-video"></i></button>';

                renderString += '<button data-id = "' + row.id + '" data-name = "' + row.name + '" data-toggle="tooltip" data-placement="bottom" title="¿Eliminar clase?" class="delete-item m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-trash-alt"></i></button>';

                renderString += '<button data-id = "' + row.id + '" data-url = "' + row.url + '" data-name = "' + row.name + '" data-toggle="tooltip" data-placement="bottom" title="ver online!" class="link-item m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill"><i class="fas fa-globe"></i></button>';

                renderString += '</td></span>';

                return renderString;

            }
        }

        ],

        rowCallback: function(row, data) {

            $("[name='my-checkbox']").bootstrapSwitch();

            $('[data-toggle="tooltip"]').tooltip();

            $('.define_ammount_of_tickets', row).attr('data-booking-id', data.museum_booking_id);

            //...

            $('.change_time', row).attr('data-booking-id', data.museum_booking_id);
            
            $('.change_time', row).attr('data-date-start', data.date_start);

            $('.change_time', row).attr('data-date-end', data.date_end);

            //...

            $('.set_guide', row).attr('data-booking-id', data.museum_booking_id);

            //...

            $('.open-link', row).attr('data-url', data.url);

            $('.sticky_note', row).attr('notes', data.notes);

            $('.sticky_note', row).attr('data-booking-id', data.museum_booking_id);

            //...

            $('.delete_row', row).attr('data-booking-id', data.museum_booking_id);

            $('.delete_row', row).attr('data-name', data.institution_name);

            $('.delete_row', row).attr('data-cuit', data.institution_cuit);
            
            $('.delete_row', row).attr('data-guide-name', data.name);

            $('.delete_row', row).attr('data-guide-surname', data.surname);

            //...

            $('.see_list', row).attr('data-booking-id', data.museum_booking_id);


            //...

            $('.schedulle', row).prop('title', 'Conocé los turnos asignados a la institución ' + data.institution_name);

            //...

            $('.edit_data', row).prop('title', 'Editar información de ' + data.institution_name);

                
            },

            "fnDrawCallback": function(settings) {

                 $("[name='my-checkbox']").bootstrapSwitch();

                 $('body').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

                    var id = $(this).attr('data-id');

                    var object = {id : id, table: 'museum_virtual_classes', row : 'active', value: ((state == true) ? 1 : 0), token:token};

                    formUtils.ajaxCall('private/framework/utils/general/updater.php', object, onSuccess, onError);

                    function onSuccess(data){

                        console.log(data);

                        utils.toastr(data.title, data.msg, {}, data.type)

                    };

                    function onError(xhr, status, error){ console.log(xhr, status, error); }

                    
                });

                 var hash = window.location.hash;
        
                $('#myTab a[href="' + hash + '"]').tab('show');


                $("[name='my-scholarship-checkbox']").bootstrapSwitch();

                $("[name='my-payment-checkbox']").bootstrapSwitch();

                $("[name='my-upload-checkbox']").bootstrapSwitch();

                $("[name='my-assisted-checkbox']").bootstrapSwitch();

                $('[data-toggle="tooltip"]').tooltip();

                
                $('tbody').on('click', 'tr', function() {

                    $("[name='my-scholarship-checkbox']").bootstrapSwitch();

                    $("[name='my-payment-checkbox']").bootstrapSwitch();

                    $("[name='my-upload-checkbox']").bootstrapSwitch();

                    $("[name='my-assisted-checkbox']").bootstrapSwitch();

                });
            }
        });

        table.draw();

        $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-assisted-checkbox"]', function(event, state) {

            var itemId = $(this).attr('data-id');

            var name = $(this).attr('data-name');

            updateUserStatus(itemId, state, name, 'update', 'assisted', 'institution_assisted');

        });

        $('tbody').on('click', '.see_list', function() {

            var id = $(this).attr('data-booking-id');

            window.location.href = "museum_booking_event_list.php?id=" + id;

        });

        $('body').on('click', '.add-images', function() {

            var itemId = $(this).attr('data-id');

            console.log("ok: " + itemId);

            window.location.href = "museum_virtual_class_add.php?id=" + itemId + "&tab=images-tab";

        });

        $('body').on('click', '.add-videos', function() {

            var itemId = $(this).attr('data-id');

            console.log("ok: " + itemId);

            window.location.href = "museum_virtual_class_add.php?id=" + itemId + "&tab=videos-tab";

        });


        $('body').on('click', '.edit-item', function() {

            var itemId = $(this).attr('data-id');

            console.log("ok: " + itemId);

            window.location.href = "museum_virtual_class_add.php?id=" + itemId;

        });

        $('body').on('click', '.link-item', function (event) {

            var url = $(this).attr('data-url');

           var serverUrl = "https://www.museodelholocausto.org.ar/clases/" + url;

            window.open(serverUrl, '_blank');




        });



$('body').on('click', '.delete-item', function (event) {

    var id = $(this).attr('data-id');

    var dataName = $(this).attr('data-name');

    var string = "¿Eliminar por completo la clase <b>" +  dataName + "</b>? esta operación no se puede deshacer. Recordá que podés activarla o desactivarla con el botón de estado.";

    utils.alert('¿eliminar?', string, ['si, eliminar', 'no, salir'], [onDelete], 'error', false);

    function onDelete(){

        var object = {id : id, table: 'museum_virtual_classes', row : 'deleted', value: 1, token:token};
       
        formUtils.ajaxCall('private/framework/utils/general/updater.php', object, onSuccess, onError);

        function onSuccess(data){

            utils.toastr(data.title, data.msg, {}, data.type);

            if(data.status == 6){ 

                table.ajax.reload(null, false); 

            }

        }

        function onError(xhr, status, error){ console.log(xhr, status, error); }

    }

});


}

    
    var deleteInstitution = function(id) {

        $.ajax({

            type: "POST",

            url: "private/users/museum/booking/delete_booking.php",

            data: {
                id: id
            },

            success: function(result) {


            },

            error: function(xhr, status, error) {


                var err = eval("(" + xhr.responseText + ")");

            },

            dataType: "json"
        });


    }

     /**
      * @function updateValue
      * @description update individual value in DB.
      *
      * @param {int} id                 - object id.
      * @param {string} fieldName       - the field to modify
      * @param {string} newValue        - object value.
      * @param {int} filter             - filter type.
      */

      var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

         helper.blockStage("actualizando...");

         var request = $.ajax({

             url: "private/users/museum/general/update_value.php",

             type: "POST",

             data: {

                id: id,

                fieldName: fieldName,

                newValue: newValue,

                filter: filter
            },

            dataType: "json"

        });

         request.done(function(result) {

             helper.showToastr(result.title, result.msg);

             helper.unblockStage();

             window.location.reload();

             if (callback != undefined) callback(callbackParams);

         });

         request.fail(function(jqXHR, textStatus) {

             helper.unblockStage();

         });
     }

    /**
     * @function updateUserStatus
     *
     * @description Update / Delete user status from list: we use this to mark the user as online / offline and
     * for delete an user completely from the list.
     *
     * @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
     * @param {boolean} state                    - True or false: use it for both, update and delete.
     * @param {string} name                  - The name of the person being manipulated.
     * @param {string} action                    - Posibilities: 'update' or 'delete'. 
     * @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
     * @param {DataTable} row                    - Reference to the row that we are dealing with.
     * @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
     * @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
     */

     var updateUserStatus = function(id, state, name, action, defaultColumn = null, source = 'delete_booking_member') {

        helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando visitante...');

        var url = "private/users/museum/general/update_status.php";

        $.ajax({

            type: "POST",

            url: url,

            data: {

                id: id,
                
                status: state,
                
                action: action,
                
                name: name,
                
                source: source,

                table: 'museum_booking',

                defaultColumn: defaultColumn
            },

            success: function(result) {

                helper.unblockStage();

                if (action == 'update') {

                    helper.showToastr(result.title, result.msg);

                } else {

                    animatable.fadeOut('slow', 'linear', function() {

                        helper.showToastr('ELIMINADO', 'Eliminaste a ' + name + ' de la visita guiada.');

                        var count = 0;

                        var data = processTable.rows().data();

                        processTable.rows().every(function(rowIdx, tableLoop, rowLoop) {

                            if (processTable.cell(rowIdx, 0).data() > Number(order)) {

                                var saveValue = Number(processTable.cell(rowIdx, 0).data() - 1);

                                processTable.cell(rowIdx, 0).data(saveValue);

                            }

                        });

                        row.remove().draw(true);

                        processTable.rows().invalidate().draw(false);

                    });

                }

            },

            error: function(xhr, status, error) {

                var err = eval("(" + xhr.responseText + ")");

                alert(err.Message);
            },

            dataType: "json"
        });

    }

    /**
     * @function findGetParameter
     * @description obtiene parametros...
     */

     var findGetParameter = function(parameterName) {

        var result = null,

        tmp = [];

        location.search.substr(1).split("&").forEach(function(item) {

            tmp = item.split("=");

            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);

        });

        return result;
    }

    /**
     * @function addListeners
     * @description ...
     */

     var addListeners = function(){


        $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {

            $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();

            e.preventDefault();

            var id = $(e.target).attr("href");

            history.pushState(null, null, id);

        });


        $('.save-description').click(function(){

            finalStartDate = startDate + " " + $("#from-time").val() + ":00";

            finalEndDate = endDate + " " + $("#to-time").val() + ":00";

            updateValue(saveId, ['date_start', 'date_end'], [finalStartDate, finalEndDate], 'museum_booking');

            $('.modal').modal('hide');

        });

        $('#date-from').datepicker({orientation: 'bottom', autoclose: true, language: 'es',  format: readableDateFormat});

        $('#date-from').datepicker().on('changeDate', function (ev) {

            var dateFrom = $('#date-from').data('datepicker').getFormattedDate('yyyy-mm-dd');

            window.location.href = "museum_booking_list.php?date=" + dateFrom;

        });

        $('.go-to-today').click(function(){

            $finalURL = 'museum_booking_list.php?date=' + $('#today').val();

            window.location.href = $finalURL;

        });

        $('.go-to-tomorrow').click(function(){

            $finalURL = 'museum_booking_list.php?date=' + $('#tomorrow').val();

            window.location.href = $finalURL;

        });

        $('.go-to-day-after-tomorrow').click(function(){

            $finalURL = 'museum_booking_list.php?date=' + $('#dayAfterTomorrow').val();

            window.location.href = $finalURL;

        });
    }

    var getUserVariables = function(){
        
        token = $('#token').val();

    }


    return {

        init: function() {

             helper.setMenu();

            getUserVariables();

            createDataTable($('#tab-1'), 1);

        }

    };

}();

jQuery(document).ready(function() { MuseumBookingList.init(); });