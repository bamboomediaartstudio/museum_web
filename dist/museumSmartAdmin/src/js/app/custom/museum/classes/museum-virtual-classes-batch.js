/**
 * @summary News list.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
* @function Manager
* @description Manage all the events.
*/

var Manager = function() {

    helper = new Helper();

    var calendar = $('#m_calendar');

    var readableDateFormat = "dd-mm-yyyy";

    var dates;

    var coincidences;

    var currentDate;

    var stopDate;

    var forGroups = false;

    var holidaysTypes = new Array('Feriados inamovibles', 'Feriados trasladables', 'Días no laborables');

    var nationalHolidays = new Array(

        {'date': '2022-01-01', 'description' : 'Año nuevo', type:0},

        {'date': '2022-02-28', 'description' : 'Carnaval', type:0},

        {'date': '2022-03-01', 'description' : 'Carnaval', type:0},
        
        {'date': '2022-03-24', 'description' : 'Día Nacional de la Memoria por la Verdad y la Justicia', type:0},

        {'date': '2022-04-02', 'description' : 'Día del Veterano y de los Caídos en la Guerra de Malvinas. Feriado inamovible.', type:0},
        
        {'date': '2022-04-14', 'description' : 'Jueves Santo.Día no laborable', type:2},
        
        {'date': '2022-04-15', 'description' : 'Viernes Santo. Feriado inamovible', type:0},

        {'date': '2022-04-16', 'description' : 'Pascuas Judías.Día no laborable', type:2},

        {'date': '2022-04-17', 'description' : 'Pascuas Judías.Día no laborable', type:2},
        
        {'date': '2022-04-22', 'description' : 'Últimos dos días de la Pascua Judía (b).', type:2},

        {'date': '2022-04-23', 'description' : 'Últimos dos días de la Pascua Judía (b).', type:2},

        {'date': '2022-04-24', 'description' : 'Día de acción por la tolerancia y el respeto entre los pueblos (a).', type:2},
        
        {'date': '2022-05-01', 'description' : 'Día del trabajador. Feriado inamovible', type:0},
        
        {'date': '2022-05-18', 'description' : 'Censo nacional 2022', type:0},

        {'date': '2022-05-25', 'description' : 'Día de la Revolución de Mayo.', type:0},

        {'date': '2022-06-17', 'description' : 'Día Paso a la Inmortalidad del General Martín Miguel de Güemes (17/06).', type:1},
        
        {'date': '2022-06-20', 'description' : 'Día Paso a la Inmortalidad del General Manuel Belgrano.', type:0},

        {'date': '2022-07-09', 'description' : 'Día de la Independencia. Feriado inamovible.', type:0},

        {'date': '2022-08-15', 'description' : 'Paso a la Inmortalidad del Gral. José de San Martín  (17/8).', type:1},
        
        {'date': '2022-09-26', 'description' : 'Año Nuevo Judío. Dia no laborable.', type:2},

        {'date': '2022-09-27', 'description' : 'Año Nuevo Judío. Dia no laborable.', type:2},
        
        {'date': '2022-10-05', 'description' : 'Día del Perdón', type:2},
        
        {'date': '2022-10-07', 'description' : 'Feriado con fines turísticos.', type:1},

        {'date': '2022-10-10', 'description' : 'Día del Respeto a la Diversidad Cultural (12/10).', type:1},
        
        {'date': '2022-11-20', 'description' : 'Día de la Soberanía Nacional (20/11).', type:1},
        
        {'date': '2022-11-21', 'description' : 'Feriado con Fines Turísticos.Dia no laborable.', type:0},

        {'date': '2022-12-08', 'description' : 'Inmaculada Concepción de María. Feriado inamovible.', type:0},

        {'date': '2022-12-09', 'description' : 'Feriado con Fines Turísticos.Dia no laborable.', type:0},
        
        {'date': '2022-12-25', 'description' : 'Navidad', type:0}


        );

    /* * * DELETE IF NO HAVE ERRORS * * */

    /*

    var nationalHolidays = new Array(

        {'date': '2020-01-01', 'description' : 'Año nuevo', type:0},

        {'date': '2020-02-24', 'description' : 'Carnaval', type:0},

        {'date': '2020-02-25', 'description' : 'Carnaval', type:0},
        
        {'date': '2020-03-23', 'description' : 'Feriado con Fines turístico', type:0},

        {'date': '2020-03-24', 'description' : 'Día Nacional de la Memoria por la Verdad y la Justicia.', type:0},

        {'date': '2020-04-02', 'description' : 'Día del Veterano y de los Caídos en la Guerra de Malvinas. Feriado inamovible.', type:0},
        
        {'date': '2020-04-09', 'description' : 'Jueves Santo.Día no laborable', type:2},
        
        {'date': '2020-04-10', 'description' : 'Viernes Santo. Feriado inamovible', type:0},

        {'date': '2020-04-10', 'description' : 'Viernes Santo. Feriado inamovible', type:2},

        {'date': '2020-04-15', 'description' : 'Pascuas Judías.Día no laborable', type:2},

        {'date': '2020-04-16', 'description' : 'Pascuas Judías.Día no laborable', type:2},
        
        {'date': '2020-04-24', 'description' : 'Día de acción por la tolerancia y el respeto entre los pueblos (a).', type:2},
        
        {'date': '2020-05-01', 'description' : 'Día del trabajador. Feriado inamovible', type:0},

        {'date': '2020-05-24', 'description' : 'Fiesta de la Ruptura del Ayuno del Sagrado Mes de Ramadán (Id al-Fitr) (c).Día no laborable', type:2},

        {'date': '2020-05-25', 'description' : 'Día de la Revolución de Mayo.', type:0},

        {'date': '2020-06-15', 'description' : 'Día Paso a la Inmortalidad del General Martín Miguel de Güemes (17/06).Feriado trasladable.', type:1},
        
        {'date': '2020-06-20', 'description' : 'Día Paso a la Inmortalidad del General Manuel Belgrano.', type:0},

        {'date': '2020-07-09', 'description' : 'Día de la Independencia. Feriado inamovible.', type:0},
        
        {'date': '2020-07-10', 'description' : 'Feriado con Fines Turísticos. Dia no laborable.', type:0},

        {'date': '2020-07-31', 'description' : 'Fiesta del Sacrificio (c)', type:2},

        {'date': '2020-08-17', 'description' : 'Paso a la Inmortalidad del Gral. José de San Martín.Feriado trasladable.', type:1},

        {'date': '2020-08-20', 'description' : 'Año Nuevo Islámico (c).', type:2},
        
        {'date': '2020-09-19', 'description' : 'Año Nuevo Judío (b)**. Dia no laborable.', type:2},

        {'date': '2020-09-20', 'description' : 'Año Nuevo Judío (b)**. Dia no laborable.', type:2},
        
        {'date': '2020-09-28', 'description' : 'Día del Perdón (b)***', type:2},
        
        {'date': '2020-10-12', 'description' : 'Día del Respeto a la Diversidad Cultural.', type:1},
        
        {'date': '2020-11-20', 'description' : 'Día de la Soberanía Nacional (20/11).', type:1},
        
        {'date': '2020-12-07', 'description' : 'Feriado con Fines Turísticos.Dia no laborable.', type:0},

        {'date': '2020-12-08', 'description' : 'Inmaculada Concepción de María. Feriado inamovible.', type:0},
        
        {'date': '2020-12-08', 'description' : 'Navidad', type:0}


        );

    */


    /**
    *
    * @function addListeners
    * @description Add all the listeners.
    */

    var addListeners = function(){

        var options = [];

        var src = [

        {id: 1, txt: "Lunes"}, {id: 2, txt: "Martes"}, {id: 3, txt: "Miércoles"}, {id: 4, txt: "Jueves"}, {id: 5, txt: "Viernes"}

        , {id: 6, txt: "Sábado"}, {id: 0, txt: "Domingo"}];

        src.forEach(function (item) {

            var option = "<option value=" + item.id + ">" + item.txt + "</option>";

            options.push(option);

        });

        $('.m_selectpicker').html(options);

        $('.m_selectpicker').selectpicker('refresh');

        $("#from-time, #to-time").timepicker({minuteStep: 15, showMeridian: false, snapToStep: true});

        var now = new Date();
        
        now.setHours(now.getHours() + 2);

        $("#to-time").val(moment(now).format("HH") + ":" + moment(now).format("mm"));

        $('[data-toggle="tooltip"]').tooltip();

        var today = new Date();

        $('#date-from').datepicker({startDate: today, orientation: 'bottom', autoclose: true, language: 'es',  format: readableDateFormat});

        $('#date-to').datepicker({ autoclose: true, language: 'es',  format: readableDateFormat});

        $('#date-from').datepicker().on('changeDate', function (ev) {

            var dateFrom = $('#date-from').data('datepicker').getFormattedDate('yyyy-mm-dd');

            var mydate = new Date(dateFrom);

            mydate.setDate(mydate.getDate() + 2)

            $('#date-to').datepicker('destroy');

            $('#date-to').datepicker({startDate: mydate, autoclose: true, language: 'es',  format: readableDateFormat});

        });

        //$("#from-time").val(newTime);


        $('#from-time').timepicker().on('changeTime.timepicker', function (e) {

            var newhour =(e.time.hours) + 1;

            var newTime = newhour + ':'+ e.time.minutes;

            $("#to-time").val(newTime);
        });


        $('.for-groups').change(function() {

            forGroups = ($(this).prop('checked'));

            console.log(forGroups);


        });


        $('.add-dates').click(function(){


            if($('.m_selectpicker_3').val() == ""){

                var title = "Falta la clase virtual!";

                var msg = "Debes añadir la clase virtual a la cual queres asignarle días y horarios!";

                showErrorSwal(title, msg);

                return;

            }

            var dateFrom = $('#date-from').data('datepicker').getFormattedDate('yyyy-mm-dd');

            var dateTo = $('#date-to').data('datepicker').getFormattedDate('yyyy-mm-dd');

            if(dateFrom == null || dateFrom == ""){

                var title = "ops!! ¿y la fecha de comienzo?";

                var msg = "Debes indicar la fecha desde la cual se debe comenzar a insertar fechas";

                showErrorSwal(title, msg);

            }else if(dateTo == null || dateTo == "") {

                var title = "ops!! ¿y la fecha de fin?";

                var msg = "Debes indicar la fecha hasta la cual se deben insertar fechas";

                showErrorSwal(title, msg);

    
            }else if($('.m_selectpicker').val().length === 0){

                var title = "falta añadir los días";

                var msg = "Debes indicar al menos un día a la semana en el cual insertar las clases virtuales.";

                showErrorSwal(title, msg);

            }else{

                dates = getDates(dateFrom, dateTo);

                doValidation();

            }            
        })

    }

    /**
    *
    * @function doValidation();s
    * @description alerta previa...
    */

    var doValidation = function(){

        coincidences = new Array();

        for(var i=0; i < dates.length; i++){

            var checkDate = dates[i];

            console.log(checkDate);

            if(checkDate.inRevision == true){

                for(var j = 0; j < nationalHolidays.length; j++){

                    var holiday = nationalHolidays[j];

                    if(checkDate.date == holiday.date){

                        coincidences.push({date: checkDate, description: holiday.description, type: holiday.type});

                        break;
                    }
                    
                }

            }

        }

        if(coincidences.length >=1){

            showHolidaysAlert();
            
        }else{

            showPreviousAlert();

        }

    }

    /**
    *
    * @function showPreviousAlert
    * @description alerta previa...
    */

    var showPreviousAlert = function(){

        var dateFrom = $('#date-from').data('datepicker').getFormattedDate('yyyy-mm-dd');

        var dateTo = $('#date-to').data('datepicker').getFormattedDate('yyyy-mm-dd');

        var startStringFormat = moment(dateFrom).format("DD")  + " de " + moment(dateFrom).format("MMMM") + " del " + moment(dateFrom).format("YYYY");
        
        var endStringFormat = moment(dateTo).format("DD")  + " de " + moment(dateTo).format("MMMM") + " del " + moment(dateTo).format("YYYY");

        var fromTime = $("#from-time").val();

        var toTime = $("#to-time").val();

        console.log(startStringFormat, endStringFormat, fromTime, toTime);

        var daysString = "";

        daysString = "los días " + daysArray.join().replace(/,(?=[^,]*$)/, ' y ');

        var virtualClass = $(".m_selectpicker_3").find("option[value='" + $(".m_selectpicker_3").val() + "']").text();

        var guideText;

        if($('.m_selectpicker_2').val() &&  $('.m_selectpicker_2').val() != -1){

            var guideLabel = $(".m_selectpicker_2").find("option[value='" + $(".m_selectpicker_2").val() + "']").text();

            guideText = "Seleccionaste a <b>" + guideLabel + "</b> como guía designado para estos " + dates.length + " eventos";

        }else{

            guideText = "";
        }

       /* var forGroupsLabel;

        if(forGroups){

            forGroupsLabel = "para escuelas/grupos";

        }else{

            forGroupsLabel = "individuales";

        }*/

        var finalString = "Repasemos una última vez: <br><br>Vas a dar de alta días y horarios para la clase virtual <b>" + virtualClass + "</b><br><br>En total, se van a dar de alta <b>" + dates.length + " turnos</b>. Serán asignados los días <b>" + daysArray.join().replace(/,(?=[^,]*$)/, ' y ') + "</b> desde las <b>" + fromTime + "hs</b> hasta las <b>" + toTime + "hs</b> entre el <b>" + startStringFormat + "</b> y el <b>" + endStringFormat + "</b><br><br>" + guideText +  ".<br><br>¿Confimar?";

        swal({

         title: 'Confirmar fechas',

         allowOutsideClick: false,

         html: finalString,

         type: "question",

         showCancelButton: true,

         confirmButtonText: 'confirmar',

         cancelButtonText: 'cancelar'

     }).then((result) => {

        if (result.value) { sendDataToServer(); }

    })

 }

   /**
    *
    * @function sendDataToServer();
    * @description mandar la data al servidor...
    */

    var sendDataToServer = function(){

        helper.blockStage("almacenando fechas...");

        var formData = new FormData();

        formData.append("fromTime", $("#from-time").val());
        
        formData.append("toTime", $("#to-time").val());

        formData.append("dates", JSON.stringify(dates));

        formData.append("forGroups", forGroups);

        formData.append("guide", $('.m_selectpicker_2').val());

        formData.append("id", $('.m_selectpicker_3').val());

        formData.append("host", $('.m_selectpicker_4').val());

        for (var pair of formData.entries()) { console.log(pair[0]+ ', ' + pair[1]); }

        //return...

        var request = $.ajax({

            url: "private/users/museum/classes/class_add_batch.php",

            type: "POST",

            contentType: false,

            processData: false,

            data: formData,

            dataType: "json"
        });

        request.done(function(response) {
            
            console.log(response.str);
            
            console.log(response);

            helper.unblockStage();

            showSuccessSwal();

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

            helper.showToastr("ops", 'error');

            helper.unblockStage();

        });

    }


    /**
    *
    * @function showHolidaysAlert
    * @description si llegamos aca, es porque hay feriados...
    */

    var showHolidaysAlert = function(){

        var day = moment(coincidences[0].date.date).format("DD");

        var month = moment(coincidences[0].date.date).format("MMMM");

        var year = moment(coincidences[0].date.date).format("YYYY");

        var showDate = day + " de " + month + " del " + year;

        var message = 'De las ' + dates.length + ' fechas indicadas para las clases virtuales, encontramos ' + coincidences.length + ' feriados.<br><br>Te pedimos que te tomes un momento y confimes o canceles las fechas que te vamos a mostrar.';

        message += '<br><br>El <b>' + showDate + '</b> tiene lugar el siguiente feriado: <b>' + coincidences[0].description + ' (' + holidaysTypes[coincidences[0].type] + ')</b>.<br><br>¿Querés que mantengamos los tunos para ese día o los eliminamos?'

        swal({

            title: 'Ops! Un minuto! Hay feriados y días no laborables entre las fechas',

            allowOutsideClick: false,

            html: message,

            type: "warning",

            showCancelButton: true,

            confirmButtonText: 'mantener',

            cancelButtonText: 'eliminar'

        }).then((result) => {

            if (result.value) {

                coincidences[0].date.inRevision = false;

                helper.showToastr("Fecha Almacenada", 'Mantendremos el turno del ' + showDate + " (" + coincidences[0].description + ")");

                doValidation();

            }else{ 

                dates.splice(dates.findIndex(item => item.id === coincidences[0].date.id), 1);

                helper.showToastr("Fecha Eliminada", 'Se eliminó el turno del ' + showDate + " (" + coincidences[0].description + ")", "error");

                doValidation();

            }
        })

    }

    /**
    *
    * @function getDates
    * @param startDate          -           desde donde...
    * @param stopDate           -           hasta donde...
    *
    * @description parsa la data entre dos fechas...
    */

    var getDates = function(startDate, stopDate) {

        var selectedDays = [];

        daysArray = new Array();

        selectedDays = $('.m_selectpicker').val();

        for (var i = 0; i < selectedDays.length; i++){

            daysArray.push($(".m_selectpicker option[value='" + selectedDays[i] + "']").text());

        }

        console.log(daysArray);

        var datesArray = new Array();

        currentDate = moment(startDate);

        stopDate = moment(stopDate);

        var counter = 0;

        var checker = 0;

        while (currentDate <= stopDate) {

            checker+=1;

            for(var i = 0; i < selectedDays.length; i++){

                console.log(currentDate.day() + " lo valida con " + selectedDays[i]);

                if(currentDate.day() == parseInt(selectedDays[i])){

                    datesArray.push({date : moment(currentDate).format('YYYY-MM-DD'), inRevision : true, id: counter });

                    counter += 1;
                }
            }

            currentDate = moment(currentDate).add(1, 'days');
        }

        console.log("valido " + checker + " y tiene un total de dias la semana " + daysArray.length);

        return datesArray;
    }


    /**
    *
    * @function showErrorSwal
    * @param msg - the msg.
    * @description Swal alert.
    */

    var showSuccessSwal = function(title, msg){

        Swal({

            title: 'batch subido exitosamente! :)',

            html: 'los ' + dates.length + ' turnos que subiste, ya se encuentran disponibles y visibles desde la web el museo.',

            type: 'success',

            confirmButtonText: 'Entendido!'

        }).then((result) => {

        if (result.value) { 

            location.reload();

        }

    })

    }

    /**
    *
    * @function showErrorSwal
    * @param msg - the msg.
    * @description Swal alert.
    */

    var showErrorSwal = function(title, msg){

        Swal({

            title: title,

            html: msg,

            type: 'error',

            confirmButtonText: 'Entendido!'

        })
    }

    /**
    *
    * @function getClasses();
    * @description obtener todas las clases virtuales...
    */

    var getClasses = function(){

        var request = $.ajax({

            url: "private/users/museum/classes/get_list_of_virtual_classes.php",

            type: "POST",

            dataType: "json",

            success: function(result) {

                var options = [];

                for (x in result.items) {

                    var option = "<option value=" + x + ">" + result.items[x] + "</option>";

                    options.push(option);

                }

                $('.m_selectpicker_3').html(options);

                $('.m_selectpicker_3').selectpicker('refresh');

            },

            error: function(request, error) {

                console.log(request);

                console.log("error");

            }

        });

    }

    /**
    *
    * @function getGuides();
    * @description levanta la lista de guias...
    */

    var getGuides = function(){

        var request = $.ajax({

            url: "private/users/services/getListOfGuides.php",

            type: "POST",

            dataType: "json",

            success: function(result) {

                var options = [];

                options.push('<option value = "-1" >Lo defino luego</option>');

                for (x in result.items) {

                    var option = "<option value=" + x + ">" + result.items[x] + "</option>";

                    options.push(option);

                }

                $('.m_selectpicker_2').html(options);

                $('.m_selectpicker_2').selectpicker('refresh');

            },

            error: function(request, error) {

                console.log(request);

                console.log("error");

            }

        });
    }

    /**
    *
    * @function getHosts();
    * @description levanta la lista de hosts...
    */

    var getHosts = function(){

        var request = $.ajax({

            url: "private/users/services/getListOfHosts.php",

            type: "POST",

            dataType: "json",

            success: function(result) {

                var options = [];

                options.push('<option value = "-1" >Lo defino luego</option>');

                for (x in result.items) {

                    var option = "<option value=" + x + ">" + result.items[x] + "</option>";

                    options.push(option);

                }

                $('.m_selectpicker_4').html(options);

                $('.m_selectpicker_4').selectpicker('refresh');

            },

            error: function(request, error) {

                console.log(request);

                console.log("error");

            }

        });
    }

    return {

        init: function() {

            helper.setMenu();

            addListeners();

            getGuides();

            getHosts();

            getClasses();

        }
    };

}();

jQuery(document).ready(function() { Manager.init(); });