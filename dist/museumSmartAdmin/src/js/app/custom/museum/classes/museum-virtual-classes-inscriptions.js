/**
 * @summary historical! 
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
 * @function MuseumBookingList
 * @description Initialize and include all the methods of this class.
 */
 var MuseumBookingList = function() {

    helper = new Helper();

    var readableDateFormat = "dd-mm-yyyy";

    var formUtils = new FormUtils();

    var utils = new Utils();

    var saveId;

    var finalStartDate;

    var finalEndDate;

    var startDate;

    var endDate;

    var date;

    var table;

    /**
     * @function createDataTable
     * @description Create all the datatables that we use as lists.
     */

     var createDataTable = function(value, bookingType) {

        table = value.DataTable({

            dom: 'Bfrtip',

            language: helper.getDataTableLanguageConfig(),

            processing: true,

            serverSide: true,

            autoWidth: true,

            pageLength: 20,

            serverMethod: 'post',

            "ajax": {

                url: "private/users/museum/classes/getListOfInscriptionsInClasses.php",
                
                dataSrc: "data",

                dataType: 'json',

                "data": function(outData) { 

                },

                dataFilter: function(inData) { 

                    console.log(inData);

                    return inData; },

                error: function(err, status) {},
            },


            "columns": [

            {data: 'id', visible: false},

            {data: 'name',

            render: function(data, type, full, meta) { return full.name + ' ' + full.surname; }

            },

            { data : 'surname', visible: false },

            {data: 'email',

            render: function(data, type, full, meta) { 

                var st = '<span class = "text-info" data-toggle="tooltip" data-title = "Hace click para copiar el email de  ' + full.name + ' ' + full.surname + ' " href="#"><span><i class="fas fa-envelope mr-2"></i>' + full.email + '</span><span>';

                return st; 

            }

            },

            {data: 'phone',

            render: function(data, type, full, meta) { 

                var url = 'https://wa.me/' + full.phone + '?text=Contacto desde el administrador.';

                var st = '<a target = "_blank" href="' + url + '"><span><i class="fab fa-whatsapp mr-2"></i>' + full.phone + '</span><a>';

                return st; }

            },

             {data: 'asistencia',

             render : function(data, type, full, meta){

                var checked = (parseInt(full.assisted) == 1) ? 'checked="checked"' : '';

                var onColor = "success";

                var offColor = "danger";

                var onLabel =  'asistió';

                var offLabel = 'no';

                return '<input name="my-checkbox" data-id= ' + full.id + ' class="status-toggle my-checkbox" data-switch="true"  type="checkbox" ' + checked + ' data-on-color="' + onColor + '" data-off-color="' + offColor + '" data-on-text="' + onLabel + '" data-handle-width="50" data-off-text="' + offLabel + '" data-size="mini">';

            }


            },

            {data: 'acciones',

            width:50,

            render: function(data, type, full, meta) { 

                var fullName = full.name + ' ' + full.surname;

                var mainId = full.mainId;

                var positioning = 'top';

                var st ='<button data-fullname = "' + fullName + '" data-id = "' + mainId + '" class="edit_row m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" data-title="Ver info de ' + fullName + '" data-toggle="tooltip"><i class="la la-edit"></i></button>';
                
                st+='<button data-fullname = "' + fullName + '" data-id = "' + mainId + '" class="delete_row m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-title="¿Eliminar a ' + fullName + '?" data-toggle="tooltip"><i class="la la-trash"></i></button>';

                return st; 

            }

            }

            
        ],

        rowCallback: function(row, data) {

            $('.define_ammount_of_tickets', row).attr('data-booking-id', data.museum_booking_id);

            //...

            $('.change_time', row).attr('data-booking-id', data.museum_booking_id);
            
            $('.change_time', row).attr('data-date-start', data.date_start);

            $('.change_time', row).attr('data-date-end', data.date_end);

            //...

            $('.set_guide', row).attr('data-booking-id', data.museum_booking_id);

            //...

            $('.open-link', row).attr('data-url', data.url);

            $('.sticky_note', row).attr('notes', data.notes);

            $('.sticky_note', row).attr('data-booking-id', data.museum_booking_id);

            //...

            $('.delete_row, .edit_row ', row).attr('data-id', data.id);

            $('.delete_row, .edit_row ', row).attr('data-id-event', data.id_event);

            $('.delete_row, .edit_row ', row).attr('data-id-class', data.id_class);
            
            $('.delete_row, .edit_row', row).attr('data-name', data.name);

            $('.delete_row, .edit_row', row).attr('data-class-name', data.class_name);

            $('.delete_row, .edit_row', row).attr('data-surname', data.surname);

            $('.delete_row, .edit_row', row).attr('data-date-start', data.date_start);

            $('.delete_row, .edit_row', row).attr('data-date-end', data.date_end);
            
            $('.delete_row, .edit_row', row).attr('data-email', data.email);

            $('.delete_row, .edit_row', row).attr('data-phone', data.phone);
            
            //...

           
            $('.edit_data', row).prop('title', 'Editar información de ' + data.institution_name);

                
            },

            "fnDrawCallback": function(settings) {

                $('[data-toggle="tooltip"]').tooltip();

                $('[data-switch=true]').bootstrapSwitch();

                 $('body').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

                    var id = $(this).attr('data-id');

                    var object = {id : id, table: 'museum_virtual_classes_events_inscriptions', row : 'assisted', value: ((state == true) ? 1 : 0), token:token};

                    formUtils.ajaxCall('private/framework/utils/general/updater.php', object, onSuccess, onError);

                    function onSuccess(data){ utils.toastr(data.title, data.msg, {}, data.type) };

                    function onError(xhr, status, error){ console.log(xhr, status, error); }
                    
                });

                 var hash = window.location.hash;
        
                $('#myTab a[href="' + hash + '"]').tab('show');

                $('tbody').on('click', 'tr', function() { $('[data-switch=true]').bootstrapSwitch(); });
            }
        });

        table.draw();

        
        $('body').on('click', '.edit-item', function() {

            var itemId = $(this).attr('data-id');

            console.log("ok: " + itemId);

            window.location.href = "museum_virtual_class_add.php?id=" + itemId;

        });

        $('body').on('click', '.edit_row', function (event) {

            var dataName = $(this).attr('data-name');

            var className = $(this).attr('data-class-name');

            $('#content-modal').modal('show');

            $('.modal-title').text(dataName);

            var st = "<strong>CLASE: </strong>" + $(this).attr('data-class-name') + '<br>';

            st += "<strong>INSTITUCIÓN: </strong>" + $(this).attr('data-name') + '<br>';
            
            st += "<strong>FECHA: </strong>" + $(this).attr('data-date-start') + '<br>';
            
            st += "<strong>EMAIL: </strong>" + $(this).attr('data-email') + '<br>';

            st += "<strong>TE: </strong>" + $(this).attr('data-phone') + '<br>';

            //st += "<strong>EMAIL: </strong>" + $(this).attr('data-class-name') + '<br>';

            $('.modal-body').html(st);



        });
        
        $('body').on('click', '.delete_row', function (event) {

            var id = $(this).attr('data-id');

            var idEvent = $(this).attr('data-id-event');

            var idClass = $(this).attr('data-id-class');

            var dataName = $(this).attr('data-name');

            var className = $(this).attr('data-class-name');

            var dateStart = $(this).attr('data-date-start');

            var dateEnd = $(this).attr('data-date-start');

            var string = "¿Eliminar al inscripto <b>" +  dataName + "</b>?<br><br>Esta acción va a eliminar a " + dataName + " del turno del "  + dateStart + " de la clase <b>" + className + "</b><br><br>Si eliminas a <b>" + dataName + "</b> se le enviará un correo automáico para informarle de la baja.";

            utils.alert('¿eliminar?', string, ['si, eliminar', 'cancelar'], [onDelete], 'error', false);

            function onDelete(){

                var object = {id : id, table: 'museum_virtual_classes_events_inscriptions', row : 'deleted', value: 1, token:token};
               
                formUtils.ajaxCall('private/framework/utils/general/updater.php', object, onSuccess, onError);

                function onSuccess(data){

                    utils.toastr(data.title, data.msg, {}, data.type);

                    if(data.status == 6){ 

                        table.ajax.reload(null, false); 

                    }

                }

                function onError(xhr, status, error){ console.log(xhr, status, error); }

            }

        });


        }

    

    

    /**
     * @function addListeners
     * @description ...
     */

     var addListeners = function(){

        $('[data-toggle="tooltip"]').tooltip();

    }

    var getUserVariables = function(){
        
        token = $('#token').val();

    }


    return {

        init: function() {

            helper.setMenu();

            getUserVariables();

            createDataTable($('#tab-1'), 1);

        }

    };

}();

jQuery(document).ready(function() { MuseumBookingList.init(); });