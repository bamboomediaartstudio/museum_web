/**
 * @summary Manage all the users actions.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
 * @function UsersList
 * @description Initialize and include all the methods of this class.
 */
 var UsersList = function() {

 	var helper = Helper();

     var address;

      var fallbackCopyTextToClipboard = function(text) {

          var textArea = document.createElement("textarea");

          textArea.value = text;

           textArea.style.position="fixed";

           document.body.appendChild(textArea);

           textArea.focus();

           textArea.select();

           console.log(textArea.value);

           try {

               var successful = document.execCommand('copy');

               var msg = successful ? 'successful' : 'unsuccessful';

               console.log('Fallback: Copying text command was ' + msg);

          } catch (err) {

               console.error('Fallback: Oops, unable to copy', err);

          }

          document.body.removeChild(textArea);
     }

     var copyTextToClipboard = function(text) {

          if (!navigator.clipboard) {

               fallbackCopyTextToClipboard(text);

               return;

          }

          navigator.clipboard.writeText(text).then(function() {

               console.log('Async: Copying to clipboard was successful!');

          }, function(err) {

               console.error('Async: Could not copy text: ', err);

          });
     }

 	/**
     * @function deleteUser
     * @description update individual value in DB.
     *
     * @param {int} id                 - object id.
     * @param {string} fieldName       - the field to modify
     * @param {string} newValue        - object value.
     * @param {int} filter             - filter type.
     */

     var deleteUser = function(id, fieldName, newValue, filter) {

     	helper.blockStage("actualizando...");

     	var request = $.ajax({

     		url: "private/users/museum/general/update_value.php",

     		type: "POST",

     		data: {

     			id: id,

     			fieldName: fieldName,

     			newValue: newValue,

     			filter: filter
     		},

     		dataType: "json"

     	});

     	request.done(function(result) {

     		console.log(result);

     		helper.unblockStage();

     		helper.showToastr(result.title, result.msg);

     		window.location.href = "users.php";

     	});

     	request.fail(function(jqXHR, textStatus) {

     		console.log(jqXHR);

     		console.log("error");

     	});

     }

     return {


     	init: function() {
               
               $('.mt-clipboard-container').click(function(){

                    copyTextToClipboard(address);

                    helper.showToastr("COPIAR LINK!", "Texto copiado al portapapeles correctamente.");

               });

     		$('.info-button').click(function(){

                    $('.modal').modal('show');

                    address = $(this).attr('data-address');

                    var name = $(this).attr('data-name');

                    $('.modal-title').text("Recibir " + $(this).attr('data-name') + "! :)");

                    var sendString = "¿Alguien quiere enviarte <strong>" + name.toUpperCase() + "</strong>?<br><br>Copiá y pegá la siguiente dirección y enviásela a quien quiera transferir <br><br> <strong>IMPORTANTE:</strong><br>Validá dos veces que la dirección / address que enviás es correcta, y están todos los números. Las transacciones en criptoactivos no se pueden deshacer.<br><br><strong>DIRECCIÓN:</strong>";

                    $('.modal-body-line-1').html(sendString);

                    $('#target-address').val(address);

                    var qrString = "<br>¿Necesitás pasar la address a tu celular? podés escanear el siguiente código qr.<br><br>";

                    $('.modal-body-line-2').html(qrString);

                    $('#qrcode').empty();

                    $('#qrcode').qrcode(address);
     		});


     		$('.open-blockchain').click(function(){

     			var blockchain = $(this).attr('data-blockchain');

                    window.open(blockchain, '_blank'); 


     		})

     	}

     };

 }();

 jQuery(document).ready(function() {  UsersList.init(); });


