/**
 * @summary Add new education module.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */


/**
 * @function MuseumEducationModuleAdd
 * @description Initialize and include all the methods of this class.
 */

 var MuseumEducationModuleAdd = function() {

    helper = Helper();

    var editMode;

    var moduleId;

    var moduleTitle;

    var imagePath;

    var form = $('#add-data-form');

    var initialPreviewCoverConfig = [{key:'http://museodelholocausto.local/museumSmartAdmin/dist/default/private/test.mp4'}];

    var initialPreviewCoverPaths = ['http://museodelholocausto.local/museumSmartAdmin/dist/default/private/test.mp4'];

    var keywordsArr = [];
    
    
    /**
     * @function showErrorSwal
     * @param msg - the msg.
     * @description Swal alert.
     */

     var showErrorSwal = function(msg){

        Swal({

            title: 'Opss!.',

            html: msg,

            type: 'error',

            confirmButtonText: 'Entendido!'

        })
    }

    
    /**
     * @function addFormValidations
     * @description Asign all the form validations for this page.
     */

     var addFormValidations = function() {

        jQuery.validator.setDefaults({

            ignore: ":hidden, [contenteditable='true']:not([name])"

        });

        helper.addyoutubeValidationMethod($.validator);

        form.validate({

            rules: {

                title: {
                    required: true
                },

                
                link: {

                    url: true
                }


            },
            messages: {

                title: helper.createErrorLabel('título', 'REQUIRED'),

                link: helper.createErrorLabel('link', 'INVALID_URL')
            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();

                
                window.scroll({top: 0, left: 0, behavior: 'smooth' });
            },

            submitHandler: function(form, event) {

                if (!validURL($('#link').val()) && $('#main-image').get(0).files.length === 0) {

                    var alert = "para subir un nuevo módulo, al menos debe haber un link o un archivo";

                    showErrorSwal(alert);

                    return;
    
                }


                event.preventDefault();

                event.stopPropagation();

                helper.blockStage("actualizando datos...");

                var formData = new FormData(form);

                formData.append("id", moduleId);

                formData.append("moduleTitle", moduleTitle);

                formData.append("source", "alerts");

                formData.append("caption", $('#caption').summernote('code'));

                //formData.append("url", $('#url').val());

                formData.append('keywords', JSON.stringify(keywordsArr));

                var request = $.ajax({

                    url: "private/users/museum/education/module_add.php",

                    type: "POST",

                    contentType: false,
                    
                    processData: false,

                    data: formData,

                    dataType: "json"
                });

                request.done(function(response) {

                    swal({

                        title: 'SE AÑADIÓ UN NUEVO MÓDULO!',

                        allowOutsideClick: false,

                        html: 'se acaba de dar de alta un nuevo módulo educativo',

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch (Number(response.status)) {

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {

                            switch (Number(response.status)) {

                                case 1:

                                //window.location.replace("museum_alerts_list.php");

                                break;

                            }

                        }

                    });


                    helper.showToastr("Se acualizo", 'Se actualizo');

                    helper.unblockStage();

                });

                request.fail(function(jqXHR, textStatus) {

                    console.log(jqXHR);

                    console.log("error");

                    helper.showToastr("ops", 'error');

                    helper.unblockStage();

                });

            }

        });

    }

    /**
     * @function createCover
     * @description Create the cover image.
     */

     var createCover = function(){

       $("#main-image").fileinput({

        initialPreview : (editMode) ? initialPreviewCoverPaths : '',

        //initialPreviewConfig: initialPreviewCoverConfig,

        initialPreviewAsData: true,

        theme: "fas",

        uploadUrl: "private/users/museum/images_general/add_image.php",

        deleteUrl: "private/users/museum/general/delete_image.php",

        showCaption: true,

        showPreview: true,

        showRemove: false,

        showUpload: false,

        showCancel: false,

        showDrag: false,

        fileActionSettings: {showDrag: false, showUpload:false},

        showClose: false,

        browseOnZoneClick: (editMode) ? ((imagePath == '') ? true : false)  : true,

        showBrowse: (editMode) ? ((imagePath == '') ? true : false)  : true,

        previewFileType: 'any',

        language: "es",

        maxFileSize: 0,

        //allowedFileTypes: ["image"],

        overwriteInitial: false,

        preferIconicPreview: true,

        //allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],
       
        uploadExtraData: function() {

            return {

                id: exhibitionId,

                source: 'exhibitions'
            };

        }

        }).on('fileloaded', function(event, file, previewId, index, reader) {



        }).on('fileerror', function(event, data, msg) {



        }).on('fileimageresizeerror', function(event, data, msg) {

            showErrorSwal(msg);

        }).on('fileuploaderror', function(event, data, msg) {

            var form = data.form, files = data.files, extra = data.extra,

            response = data.response, reader = data.reader;

            showErrorSwal(msg);

        }).on('filedeleted', function(event, key, jqXHR, data) {

            $('#main-image').fileinput('refresh',

                {browseLabel: 'Buscar otra...', showBrowse: true, browseOnZoneClick: true,

                uploadAsync: false, showUpload: false, showRemove: false});

        }).on("filebatchselected", function(event, files) {

            if(editMode) $('#main-image').fileinput("upload");

        });

    }

    var uncheckAll = function(){

        $('.toggler-info:checkbox').each(function( index ) {

            if($(this).is(":checked")){

                $(this).prop("checked", false);


            }


        });


    }

    var validURL = function(str) {

        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
        '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
      return !!pattern.test(str);
    }

    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

     var addListeners = function() {


        $('#link').on('input', function(e) {

            if(!validURL($(this).val())){

                uncheckAll();

            }else{

                uncheckAll();

                var pastedData = $(this).val();

                if (pastedData.indexOf("youtube") > -1 || pastedData.indexOf("youtu.be") > -1) {

                    $('.is_youtube').prop("checked", true);

                }else if(pastedData.indexOf("instagram") > -1){

                    $('.is_instagram').prop("checked", true);

                }else if(pastedData.indexOf("facebook") > -1){

                    $('.is_facebook').prop("checked", true);

                }else if(pastedData.indexOf("museodelholocausto.org.ar") > -1){

                    $('.is_mdh').prop("checked", true);

                }else if(pastedData.indexOf("zoom.us") > -1){

                    $('.is_zoom').prop("checked", true);

                }else{

                    $('.is_other').prop("checked", true);

                }


            }

        });

        $('.toggler-info-blank:checkbox').change(function() {

            if($(this).is(":checked")){

                $(this).prop("checked", true);

             }else{

                $(this).prop("checked", false);

                

             }

             var fieldName = $(this).attr('name');

            var newValue = ($(this).is(":checked")) ? 1 : 0;

            updateValue(moduleId, fieldName, newValue, 'museum_education_modules');

        });


        $('.toggler-info:checkbox').change(function() {

             if($(this).is(":checked")){

                uncheckAll();

                $(this).prop("checked", true);

             }else{

                $(this).prop("checked", false);

                

             }

            if (!editMode) return;

            var fieldName = $(this).attr('name');

            var newValue = ($(this).is(":checked")) ? 1 : 0;

            $('.toggler-info:checkbox').each(function( index ) {

                var exitName = $(this).attr('name');

                console.log(exitName + " - " + fieldName)

                if(fieldName != exitName){

                    console.log("entering: " + fieldName + " exit: " + exitName);

                    updateValue(moduleId, exitName, 0, 'museum_education_modules');

                }

            });

            updateValue(moduleId, fieldName, newValue, 'museum_education_modules');


        });

        $('#caption').summernote({

            disableDragAndDrop: true,

            height: 200,

            toolbar: [

            ['style', ['bold', 'italic', 'underline']],

            ['Misc', ['fullscreen', 'undo', 'redo']]

            ],

            callbacks: {

                onInit: function() { },

                onEnter: function() { },

                onKeyup: function(e) { },

                onKeydown: function(e) { },

                onPaste: function(e) { },

                onChange: function(contents, $editable) { },

                onBlur: function(){ },

                onFocus: function() { }

            }

        });

        $('[data-toggle="tooltip"]').tooltip();

        autosize(document.querySelector('textarea'));

        $('#exit_from_form').click(function(e) {  window.location.replace("index.php"); });

        $('#back_to_list').click(function(e) { window.location.replace("museum_alerts_list.php"); });

        $(".update-btn").click(function() {

            if (!editMode) return;

            var dbName = $(this).attr('data-db-value');

            var id = $(this).attr('data-id');

            var newValue = $(this).parent().parent().find(':input').val();

            var fieldName = $(this).parent().parent().find(':input').attr('name');

            var valid = $('#' + fieldName).valid();

            if (dbName != newValue && valid) {

                $(this).attr('data-db-value', newValue);

                updateValue(id, fieldName, newValue, 'museum_education_modules');

            }

        });

        t = new Bloodhound({

           datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),

           queryTokenizer: Bloodhound.tokenizers.whitespace,

           prefetch:{

               url : "private/users/services/get_list_of_education_modules_keywords.php",

               cache: false
           }

       })

       var context = $("#key-list .typeahead").typeahead(null, {

           hint: false,

           highlight: false,

           minLength: 1,

           name: "best-pictures",

           display: "value",

           source: t,

           autoselect: true,

           templates: {

               suggestion: Handlebars.compile("<div><strong class='text-success'>{{value}}</strong></div>")

           }

       }).on("typeahead:render", function() {

           var searchTerm = $(this).val();

           $('#key-list').parent().find('.tt-selectable:first').addClass('tt-cursor');

       }).on('typeahead:selected', function (e, datum) {

           $('#hidden-key-id').val(datum.id);
           console.log("Para que sirve este id?: "+datum.id);

       }).on('keyup', this, function (event) {


       }).on('keydown', this, function (event) {

       }).on('focus', this, function(event){

           if(editMode) $('#hidden-key-id').val("");

       });

       $(document).on('click', '.tag a', function(e){

         if(editMode){


         }else{

           e.preventDefault();

           console.log("Eliminar tag de la vista y del array");

           var tagText = $(this).parent().find('.tag-value').text();

           //Si se elimina del array, lo elimino de la vista

           if(deleteKeyFromArr(tagText)){

              $(this).parent().remove();

           }

         }

       });

      $('.deleteTag a').click(function(){

        var tagId = $(this).parent().attr('id');

        var name = "keyword Test Name";

        console.log("Delete tag id: "+tagId);

        Swal({

            title: "Eliminar Keyword?",

            html: 'Esta acción eliminará  <b>'+name+'</b>.<br><br>Esta acción no se puede deshacer. <br><br>¿Eliminar de todos modos?',

            type: 'error',

            showCancelButton: true,

            confirmButtonText: 'Si, eliminar',

            cancelButtonText: 'no, salir'

        }).then((result) => {

          if(result.value == true){

            deleteTag(tagId);

          }


        });

       });

       $('#add-keyword').click(function(){

         var keyword = $(this).parent().parent().find("#keyword");

         console.log("Add keyword to array");

         if(keyword.valid()){

           if(keyword.val() != ""){

              console.log("keyword: "+keyword.val());

              addKeyToArr(keyword.val());

           }

           $("#keyword").val('');

         }

       });

       $("#add-keyword-direct").click(function(){

         var tagName = $('#keyword').val();

         console.log(tagName);

         if(tagName!=''){

           console.log("agregar tag: " + tagName);

           var totalTags = $('.tagsinput .tag-value').length;
           
           var tagsChecked = 0;

           if(totalTags !=0){

               $('.tagsinput .tag-value').each(function(i, obj){

                 var existingTag = $(this).text();

                 console.log("looping " + i + " - "  + obj + " ->" + existingTag);

                 if(existingTag.toLowerCase() ===tagName.toLowerCase()){

                    console.log("existe una igual");

                 }else{

                   console.log("Agregar keyword, no esta actualmente agregada. tagsChecked++");
                   
                   tagsChecked++;
                 }

                 if(totalTags == tagsChecked){

                    console.log("condicion donde totalTags == tagsChecked");

                    addDirectTag(tagName);

                 }

               });

           }else{

            console.log("no habia otras pero hay una nueva...");

            addDirectTag(tagName);


           }

         }else{
           console.log("tagname empty");
         }

       });

    }

    /**
      * @function addDirectTag
      * @description add tag direct on edit mode
      * @param {string} tagName -
      *
      */

     var addDirectTag = function(tagName){

        console.log("will add direct");

         var request = $.ajax({

               url: "private/users/museum/education/education_service_ajax.php",

               type: "POST",

               data: {

                   id: moduleId,

                   action: 'addDirectTag',

                   tagName: tagName
               },

               dataType: "json"
           });

           request.done(function(response) {

             console.log("response antes de swal y toastr");

             console.log(response);

               swal({

                   title: 'tag añadido!',

                   allowOutsideClick: false,

                   html: 'se vinculó el tag al módulo educativo',

                   type: response.alert,

                   showCancelButton: (response.status == 1) ? true : false,

                   confirmButtonText: response.button,

                   cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

               }).then((result) => {

                   location.reload();

               });


               helper.showToastr("Se acualizo", 'Se actualizo');

               helper.unblockStage();

           });

           request.fail(function(jqXHR, textStatus) {

               console.log(jqXHR);

               console.log("error");

               helper.showToastr("ops", 'error');

               helper.unblockStage();

           });
     }

     /**
      * @function deleteTag
      * @description delete relation tag in DB.
      * @param {int} tagId - id relation tag
      *
      */

     var deleteTag = function(tagId){

       var request = $.ajax({

             url: "private/users/museum/education/education_service_ajax.php",

             type: "POST",

             data: {

                 id: tagId,

                 action: 'deleteTag'
             },

             dataType: "json"
         });

         request.done(function(response) {

           console.log("response antes de swal y toastr");

           console.log(response);

             swal({

                 title: 'tag eliminado!',

                 allowOutsideClick: false,

                 html: 'se eliminó a relación entre el tag y este módulo educativo',

                 type: response.alert,

                 showCancelButton: (response.status == 1) ? true : false,

                 confirmButtonText: response.button,

                 cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

             }).then((result) => {

                 location.reload();

             });


             helper.showToastr("Se acualizo", 'Se actualizo');

             helper.unblockStage();

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

             console.log("error");

             helper.showToastr("ops", 'error');

             helper.unblockStage();

         });

     }

     /**
      * @function addKeyToArr
      * @description add item to array if not exists
      *
      * @param {key} key                 - keyword
      *
      */

     var addKeyToArr = function(key){

        var found = jQuery.inArray(key, keywordsArr);

        if (found >= 0) {

        } else {
            keywordsArr.push(key);

            strKeyLi = '<span class="tag"><span><span class="tag-value">'+key+'</span>&nbsp;&nbsp;</span><a href="#" title="Eliminar Keyword">x</a></span>';
           
            $('.tagsinput').append(strKeyLi);

        }

        console.log("keywordsArr:");

        console.log(keywordsArr);

     }


     /**
      * @function deleteKeyFromAr
      * @description delete item from array by name
      *
      * @param {key} key                 - keyword
      *
      */

     var deleteKeyFromArr = function(key){

       keywordsArr.splice(keywordsArr.indexOf(key), 1);

       var found = jQuery.inArray(key, keywordsArr);

       console.log("se elimino key. Array actual: ");
       
       console.log(keywordsArr);

       if (found >= 0) { } else { return true; }

     }

    /**
     * @function window.onbeforeunload
     * @description scroll to top
     */

     window.onbeforeunload = function () { if(!editMode) window.scrollTo(0,0); }

    /**
     * @function updateValue
     * @description update individual value in DB.
     *
     * @param {int} id                 - object id.
     * @param {string} fieldName       - the field to modify
     * @param {string} newValue        - object value.
     * @param {int} filter             - filter type.
     */

     var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/update_value.php",

            type: "POST",

            data: {

                id: id,
                
                fieldName: fieldName,
                
                newValue: newValue,
                
                filter: filter
            },

            dataType: "json"

        });

        request.done(function(result) {

            console.log(result);

            helper.showToastr(result.title, result.msg);

            if (callback != undefined) callback(callbackParams);

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });

        if (callback == undefined) helper.unblockStage();

        localStorage.setItem('refresh', 'true');

    }

    /**
      * @function loadAjaxImages
      * @param loadSource - evaluate this param to get one batch or a single one.
      * @description if we are in edition mode load all the images for Bootstrap File Input.
      */

      var loadAjaxImages = function(from, field, loadSource, config, myPreviews) {

         console.log("loadAjaxImages: from: "+from+" - laodSource: "+loadSource+" - config: "+config+" - myPreviews: "+myPreviews);

         var request = $.ajax({

             url: "private/users/services/get_list_of_images_new_single.php",

             type: "POST",

             data: {

                 id: moduleId,

                 field: field,

                 source: loadSource,

                 from: from
             },

             dataType: "json"
         });

         request.done(function(result) {

           console.log(result);

           //Si hay imagen (path en db) armo vista de img
           if(result[0].picture != ""){

             for (var i = 0; i < result.length; i++) {

                 var id = result[i].id;
                 var imgPath = result[i].picture;
                 var splitImgName = imgPath.split('/');

                 var imgName = moduleId + '_' + splitImgName[1] + '_original.jpeg';

                 var path;

                path = 'private/sources/images/prensa/' + result[i].picture + '/' + imgName;

                 myPreviews.push(path);

                 config.push({

                     'key': id,

                     //'internal_order': internalOrder,


                 });

             }// /.ends for


           }


             //createNewsImg();

         });

         request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

         });

     }


    return {

        init: function() {

            editMode = ($('#edit-mode').val() == 1) ? true : false;

            addListeners();

            addFormValidations();

            if (editMode) {

                moduleId = $('#module-id').val();

                moduleTitle = $('#module-title').val();

                createCover();

            }else{

                createCover();

            }

                        
        }

    };

}();

jQuery(document).ready(function() { MuseumEducationModuleAdd.init(); });