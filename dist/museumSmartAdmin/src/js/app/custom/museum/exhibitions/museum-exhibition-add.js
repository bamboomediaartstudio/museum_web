/**
 * @summary Add new exhibition.
 *
 * @description -
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */


/**
 * @function MuseumExhibitionAdd
 * @description Initialize and include all the methods of this class.
 */

 var MuseumExhibitionAdd = function() {

    helper = Helper();

    var myElement;

    var editMode;

    var imagePath = '';

    var redirectId;

    var exhibitionId;

    var exhibitionName;

    var addRedirectId = 0;

    var defaultImageWidth = 2000;

    var defaultImageHeight = 1000;

    var isFirstLoad = true;

    var form = $('#add-data-form');

    var youtubeForm = $('#youtube-form');

    var origin;

    var map;

    var geocoder;

    var markers = [];

    var summernoteDescriptionCounter = 0;

    //cover config...

    initialPreviewCoverConfig = [];

    initialPreviewCoverPaths = [];

    //gallery config...

    initialPreviewGalleryConfig = [];

    initialPreviewgalleryPaths = [];

    //videos config...

    initialPreviewVideosConfig = [];

    initialPreviewVideosPaths = [];

    var openKey;

    var openVideoKey;

    var lat = -34.562386;

    var long = -58.447446;

    var createVideosGallery = function(){

        var btns = '<button data-origin="video" type="button" class="kv-cust-video-btn btn btn-sm btn-kv btn-default btn-outline-secondary" title="Editar"{dataKey}>' +
        '<i class="fas fa-pen"></i>' + '</button>';

        $("#videos-batch").fileinput({

            otherActionButtons: btns,

            showBrowse: false,

            language: "es",

            initialPreviewAsData: true,

            initialPreview: initialPreviewVideosPaths,

            initialPreviewConfig: initialPreviewVideosConfig,

            theme: "fas",

            uploadUrl: "private/users/museum/general/update_images_batch.php",

            deleteUrl: "private/users/museum/videos_general/delete_video.php",

            uploadExtraData: function() {

                return {

                    id: exhibitionId,

                    source: 'exhibitions'
                };

            }

        }).on('filesorted', function(e, params) {

            rearangeStack('museum_youtube_videos', JSON.stringify(params.stack));

        }).on('filedeleted', function(event, key, jqXHR, data) {

            console.log(event, key, jqXHR, data);

            helper.showToastr("Video eliminado", 'Se eliminó un video de la muestra ' + exhibitionName);

        }).on('filebeforedelete', function() {

            return new Promise(function(resolve, reject) {

                swal({

                    title: "¿Eliminar video?",

                    allowOutsideClick: false,

                    html: "El mismo no aparecerá más listado en esta muestra.",

                    type: "warning",

                    showCancelButton: true,

                    confirmButtonText: 'Si, borrar',

                    cancelButtonText: 'no, salir'

                }).then((result) => {

                    if (result.value) {

                        resolve();

                    }else{

                        reject();
                    }

                })

            });
        });

        $('.kv-cust-video-btn').on('click', function() {

            origin = $(this).attr('data-origin');

            var $btn = $(this);

            var key = $btn.data('key');

            openKey = key;

            var item = initialPreviewVideosConfig.find(item => item.key == key);

            $('#image-description').val(item.description);

            $('#myModal').modal('show');

        });

    }

    /**
     * @function createCover
     * @description Create the cover image.
     */

     var createCover = function(){

       $("#main-image").fileinput({

        initialPreview : (editMode) ? initialPreviewCoverPaths : '',

        initialPreviewConfig: initialPreviewCoverConfig,

        initialPreviewAsData: true,

        theme: "fas",

        uploadUrl: "private/users/museum/images_general/add_image.php",

        deleteUrl: "private/users/museum/general/delete_image.php",

        showCaption: true,

        showPreview: true,

        showRemove: false,

        showUpload: false,

        showCancel: false,

        showDrag: false,

        fileActionSettings: {showDrag: false, showUpload:false},

        showClose: false,

        browseOnZoneClick: (editMode) ? ((imagePath == '') ? true : false)  : true,

        showBrowse: (editMode) ? ((imagePath == '') ? true : false)  : true,

        previewFileType: 'any',

        language: "es",

        maxFileSize: 0,

        allowedFileTypes: ["image"],

        overwriteInitial: false,

        preferIconicPreview: true,

        allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],

        maxFilePreviewSize: 10240,

        minImageWidth: 2000,

        minImageHeight: 1000,

        uploadExtraData: function() {

            return {

                id: exhibitionId,

                source: 'exhibitions'
            };

        }

    }).on('fileloaded', function(event, file, previewId, index, reader) {



    }).on('fileerror', function(event, data, msg) {



    }).on('fileimageresizeerror', function(event, data, msg) {

        showErrorSwal(msg);

    }).on('fileuploaderror', function(event, data, msg) {

        var form = data.form, files = data.files, extra = data.extra,

        response = data.response, reader = data.reader;

        showErrorSwal(msg);

    }).on('filedeleted', function(event, key, jqXHR, data) {

        $('#main-image').fileinput('refresh',

            {browseLabel: 'Buscar otra...', showBrowse: true, browseOnZoneClick: true,

            uploadAsync: false, showUpload: false, showRemove: false});

    }).on("filebatchselected", function(event, files) {

        if(editMode) $('#main-image').fileinput("upload");

    });

}

    /**
     * @function createGallery
     * @description Create the gallery.
     */

     var createGallery = function() {

       console.log("Create gallery!!");

        var btns = '<button data-origin="image" type="button" class="kv-cust-btn btn btn-sm btn-kv btn-default btn-outline-secondary" title="Editar"{dataKey}>' +
        '<i class="fas fa-pen"></i>' + '</button>';

        $("#images-batch").fileinput({

            otherActionButtons: btns,

            initialPreviewAsData: true,

            initialPreview: initialPreviewgalleryPaths,

            initialPreviewConfig: initialPreviewGalleryConfig,

            theme: "fas",

            uploadUrl: "private/users/museum/general/update_images_batch.php",

            deleteUrl: "private/users/museum/general/delete_image.php",

            showCaption: true,

            showPreview: true,

            showRemove: false,

            showUpload: true,

            showCancel: true,

            showClose: false,

            browseOnZoneClick: true,

            previewFileType: 'any',

            language: "es",

            maxFileSize: 0,

            allowedFileTypes: ["image"],

            overwriteInitial: false,

            allowedFileExtensions: ["jpg", "png", "gif", "jpeg"],

            maxFilePreviewSize: 10240,

            uploadExtraData: function() {

                return {

                    id: exhibitionId,

                    source: 'individual_exhibition'

                };

            }

        }).on('filesorted', function(e, params) {

            rearangeStack('museum_images', JSON.stringify(params.stack));

        }).on('fileuploaded', function(e, params) {

        }).on('filebatchuploadcomplete', function(event, files, extra) {

            location.reload();

        }).on("filepredelete", function(jqXHR) {


        }).on('filedeleted', function(event, key, jqXHR, data) {

            console.log(event, key, jqXHR, data);

            helper.showToastr("Imagen eliminada", 'Se eliminó la imagen de la muestra ' + exhibitionName);

        }).on('fileuploaderror', function(event, data, msg) {

            var form = data.form, files = data.files, extra = data.extra,

            response = data.response, reader = data.reader;

            console.log(event, data, msg);

            //showErrorSwal(msg);

        }).on('filebeforedelete', function() {

            return new Promise(function(resolve, reject) {

                swal({

                    title: "¿Eliminar imagen?",

                    allowOutsideClick: false,

                    html: "La misma no aparecerá más listada en esta muestra.",

                    type: "warning",

                    showCancelButton: true,

                    confirmButtonText: 'Si, borrar',

                    cancelButtonText: 'no, salir'

                }).then((result) => {

                    if (result.value) {

                        resolve();

                    }else{

                        reject();
                    }

                })

            });
        });

        $('.kv-cust-btn').on('click', function() {

            origin = $(this).attr('data-origin');

            var $btn = $(this);

            var key = $btn.data('key');

            openKey = key;

            var item = initialPreviewGalleryConfig.find(item => item.key == key);

            $('#image-description').val(item.description);

            $('#myModal').modal('show');

        });

    }

    /**
     * @function rearangeStack
     * @param db - db to rearange by order.
     * @param stack - the items to rearange.
     * @description Modify items order.
     */

     var rearangeStack = function(db, stack){

       console.log("rearange functin, stack: ");
       console.log(stack);

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/rearange_images_sort.php",

            type: "POST",

            data: {

                stack: stack,

                db: db

            },

            dataType: "json",

            success: function(result) {

                helper.showToastr(result.title, result.body);

                helper.unblockStage();

            },

            error: function(request, error) {

                console.log(request);

                helper.unblockStage();
            }
        });

    }

    /**
     * @function loadAjaxImages
     * @param loadSource - evaluate this param to get one batch or a single one.
     * @description if we are in edition mode load all the images for Bootstrap File Input.
     */

     var loadAjaxImages = function(from, loadSource, config, myPreviews) {

        var request = $.ajax({

            url: "private/users/services/get_list_of_images.php",

            type: "POST",

            data: {

                id: exhibitionId,

                source: loadSource,

                from: from
            },

            dataType: "json"
        });

        request.done(function(result) {

            for (var i = 0; i < result.length; i++) {

                var internalOrder = result[i].internal_order;

                var id = result[i].id;

                var source = result[i].source;

                var description = result[i].description;

                var imgName = exhibitionId + '_' + result[i].unique_id + '_original.jpeg';

                var path;

                if(loadSource == 'exhibitions'){

                    path = 'private/sources/images/exhibitions/' + exhibitionId + '/' + result[i].unique_id + '/' + imgName;

                }else if(loadSource == 'individual_exhibition'){

                    path = 'private/sources/images/exhibitions/' + exhibitionId + '/images/' + result[i].unique_id + '/' + imgName;
                    console.log("individual_exhibition path: ");
                    console.log(path);
                    // private/sources/images/exhibitions/6/images/5c4e277babede/6_5c4e277babede_original.jpeg
                }else if(loadSource == 'exhibition_video'){

                    path = "//img.youtube.com/vi/" + result[i].unique_id + "/0.jpg"

                }

                myPreviews.push(path);
                console.log("path: ");
                console.log(path);

                config.push({

                    'key': id,

                    'internal_order': internalOrder,

                    'source': source,

                    'description' : description,

                    'caption' : (description != null) ? description : ''

                    //'url' : 'private/users/museum/general/delete_image.php'

                });

            }

            if(loadSource == 'exhibitions'){

                //createCover();
                console.log("exhibitions YES");

                console.log(result);

            }else if(loadSource == 'individual_exhibition'){

                createGallery();

            }else if(loadSource == 'exhibition_video'){

                //createVideosGallery();
            }


        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

        });

    }

    /**
     * @function addFormValidations
     * @description Asign all the form validations for this page.
     */

     var addFormValidations = function() {

        jQuery.validator.setDefaults({

            ignore: ":hidden, [contenteditable='true']:not([name])"

        });

        helper.addyoutubeValidationMethod($.validator);

        youtubeForm.validate({

            rules: {

                youtube_id: {required: true, url:true, youtube:true}

            },

            messages: {

                youtube_id:{

                    required: helper.createErrorLabel('link a video', 'REQUIRED'),

                    url: helper.createErrorLabel('link a video', 'INVALID_URL'),

                    youtube: helper.createErrorLabel('url', 'YOUTUBE_VIDEO')

                }

            },

            invalidHandler: function(e, r) {

                console.log("invalid");

            },

            submitHandler: function(form, event) {

                event.preventDefault();

                helper.blockStage("actualizando datos...");

                var videoId = helper.getYoutubeVideoId($('#youtube_id').val());

                var request = $.ajax({

                    url: "private/users/museum/videos_general/add_youtube_video.php",

                    type: "POST",

                    data: {

                        id: exhibitionId,

                        youtube_id: videoId,

                        source: 'exhibition_video'

                    },

                    dataType: "json",

                    success: function(result) {

                        console.log(result);

                        helper.showToastr(result.title, result.msg);

                        helper.unblockStage();

                        //var item = initialPreviewVideosConfig.find(item => item.key == openKey);

                        //item.caption = item.description = description;

                        initialPreviewVideosConfig = [];

                        initialPreviewVideosPaths = [];

                        $("#videos-batch").fileinput('destroy');

                        loadAjaxImages('museum_youtube_videos', 'exhibition_video', initialPreviewVideosConfig, initialPreviewVideosPaths);


                        //createVideosGallery();

                    },

                    error: function(request, error) {

                        console.log(request);

                        helper.unblockStage();
                    }

                });

            }

        })

        form.validate({

            rules: {

                name: {
                    required: true
                },

                description: {
                    required: true
                },

                email: {
                    email: true
                },

                'checkboxes[]': {
                    required: !0
                }

            },
            messages: {

                name: helper.createErrorLabel('nombre', 'REQUIRED'),

                description: helper.createErrorLabel('descripción', 'REQUIRED'),

                email: helper.createErrorLabel('email', 'EMAIL'),

                'checkboxes[]': helper.createErrorLabel('categoría', 'AT_LEAST_ONE'),

                youtube_id:{

                    required: helper.createErrorLabel('link a video', 'REQUIRED'),

                    url: helper.createErrorLabel('link a video', 'INVALID_URL'),

                    youtube: helper.createErrorLabel('url', 'YOUTUBE_VIDEO')

                }

            },

            invalidHandler: function(e, r) {

                $("#error_msg").removeClass("m--hide").show();

                if ($('#description').summernote('isEmpty'))
                {

                    $('.summernote-description-error').removeClass('d-none');

                    //window.scroll({top: 0, left: 0, behavior: 'smooth' });

                }

                window.scroll({top: 0, left: 0, behavior: 'smooth' });
            },

            submitHandler: function(form, event) {

                if ($('#description').summernote('isEmpty'))
                {

                    $('.summernote-description-error').removeClass('d-none');

                    window.scroll({top: 0, left: 0, behavior: 'smooth' });

                    return;
                }


                event.preventDefault();

                event.stopPropagation();

                helper.blockStage("actualizando datos...");

                var formData = new FormData(form);

                formData.append("id", exhibitionId);

                formData.append("exhibitionName", exhibitionName);

                formData.append("source", "exhibitions");

                formData.append("description", $('#description').summernote('code'));

                formData.append("observations", $('#observations').summernote('code'));

                formData.append("url", $('#url').val());

                var request = $.ajax({

                    url: "private/users/museum/exhibitions/exhibition_add.php",

                    type: "POST",

                    contentType: false,

                    processData: false,

                    data: formData,

                    dataType: "json"
                });

                request.done(function(response) {

                    swal({

                        title: response.title,

                        allowOutsideClick: false,

                        html: response.msg,

                        type: response.alert,

                        showCancelButton: (response.status == 1) ? true : false,

                        confirmButtonText: response.button,

                        cancelButtonText: (response.status == 1) ? 'ir a la lista' : ''

                    }).then((result) => {

                        if (result.value) {

                            switch (Number(response.status)) {

                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 4:

                                location.reload();

                                break;

                            }

                        } else if (result.dismiss === Swal.DismissReason.cancel) {

                            switch (Number(response.status)) {

                                case 1:

                                window.location.replace("museum_exhibitions_list.php");

                                break;

                            }

                        }

                    });


                    helper.showToastr("Se acualizo", 'Se actualizo');

                    helper.unblockStage();

                });

                request.fail(function(jqXHR, textStatus) {

                    console.log(jqXHR);

                    console.log("error");

                    helper.showToastr("ops", 'error');

                    helper.unblockStage();

                });

            }

        });
}

var validateSummernote = function(){

}

    /**
     * @function addListeners
     * @description Asign all the listeners.
     */

     var addListeners = function() {

       $('.museum-finder').click(function(){

        document.getElementById('address').value = 'Museo del Holocausto de Buenos Aires';

        geocodeAddress();


    })


       $('.tab-navigation').click(function(){

        setQueryStringParameter('tab', $(this).attr('href').substring(1));

    })

       myElement = $('#description, #observations').summernote({

        disableDragAndDrop: true,

        height: 200,

        toolbar: [

        ['style', ['bold', 'italic', 'underline']],

        ['Misc', ['fullscreen', 'undo', 'redo']]

        ],

        callbacks: {

            onInit: function() { },

            onEnter: function() { },

            onKeyup: function(e) {

                if($(this).attr('name') == 'description'){

                    summernoteDescriptionCounter = e.currentTarget.innerText;

                    if(summernoteDescriptionCounter !=0){

                        $('.summernote-description-error').addClass('d-none');

                    }else{

                        $('.summernote-description-error').removeClass('d-none');

                    }

                }

            },

            onKeydown: function(e) { },

            onPaste: function(e) { },

            onChange: function(contents, $editable) { },

            onBlur: function(){ },

            onFocus: function() { }

        }

    });

       $('.save-description').click(function(event){

        console.log("origin: " + origin);

        var description = $('#image-description').val();

        $('#myModal').modal('hide');

        var findArray;

        var fileInput;

        var table;

        if(origin == 'image'){

            findArray = initialPreviewGalleryConfig;

            fileInput = $("#images-batch");

            table = 'museum_images';

        }else if(origin == 'video'){

            findArray = initialPreviewVideosConfig;

            fileInput = $("#videos-batch");

            table = 'museum_youtube_videos';

        }

        var item = findArray.find(item => item.key == openKey);

        item.caption = item.description = description;

        fileInput.fileinput('destroy');

        (origin == 'image') ? createGallery() : createVideosGallery();

        updateValue(openKey, 'description', description, table);

    })

       $('.search-btn').click(function(event) { geocodeAddress(); });

       $('.m-checkbox-list :checkbox').change(function() {

        if (!editMode) return;

        if ($('.m-checkbox-list input[type=checkbox]:checked').length == 0) {

            $(this).prop('checked', true);

            swal({

                title: "opss!",

                allowOutsideClick: false,

                html: "Cada muestra debe pertener al menos a una categoría/grupo.",

                type: "warning",

                confirmButtonText: "Entendido!"

            })

        }

        var item = $(this).attr('name');

        var value = $(this).attr('value');

        var newValue = ($(this).is(":checked")) ? 1 : 0;

        var id = $(this).attr('data-id');

        updateNonOrderableCheckbox(id, value, newValue);

    });

       $('#name').on('input', function(e) {

        if (editMode) return;

        $('#validate-title').addClass('btn-info');

        $('#validate-title').removeClass('btn-success');

        $('#validate-title').text('validar');

        if ($(this).val() == "") {

            $('#validate-title').attr('disabled', true);

        } else {

            $('#validate-title').attr('disabled', false);

        }

    });

       $('#name').on('blur', function() {

        if (editMode) return;

        if ($(this).attr('id') == 'name' && $(this).val() != "") {

            validateTitle($('#name').val(), $('#url').val());

        }

    });

       $('#validate-title').click(function(event) {

        if ($('#name').val() != "") validateTitle($('#name').val(), $('#url').val())

    })

       $('[data-toggle="tooltip"]').tooltip();

       $('.show_address').change(function() {

        var checked = ($(this).prop('checked'));

        if (checked == true) {

            $('.museum_map_block, .museum_map_address_block, #map').removeClass('disabled');

        } else {

            $('.museum_map_block, .museum_map_address_block, #map').addClass('disabled');
        }

    })

       /*autosize for the textareas */

       autosize(document.querySelector('textarea'));

       /*permalink approach...*/

       $('#name').on('input', function() {

        if (editMode) return;

        var permalink;

        permalink = $.trim($(this).val());

        permalink = permalink.replace(/\s+/g, ' ');

        $('#url').val(permalink.toLowerCase());

        $('#url').val($('#url').val().replace(/\W/g, ' '));

        $('#url').val($.trim($('#url').val()));

        $('#url').val($('#url').val().replace(/\s+/g, '-'));

    });

       /*tutor selector with select2*/

       $('#exhibition-datepicker').datepicker({

        format: 'yyyy/mm/dd',

        todayBtn: true,

        todayHighlight: true,

        autoclose: true

    });

       $('#exhibition-datepicker').on('changeDate', function(event) {

        if (editMode) {

            var actual = $(event.target).attr('name');

            if (actual == 'start_date') {

                updateValue(exhibitionId, $(event.target).attr('name'), event.format(), 'museum_exhibitions');

            } else {

                updateValue(exhibitionId, $(event.target).attr('name'), event.format(), 'museum_exhibitions');

            }

        } else {

            $('#my_hidden_input').val($('#exhibition-datepicker').datepicker('getFormattedDate'));

        }

    });

       $('#exit_from_form').click(function(e) {
        window.location.replace("index.php");
    });

       $('#back_to_list').click(function(e) {
        window.location.replace("museum_exhibitions_list.php?tabId=" + redirectId);
    });

       $(".update-btn").click(function() {

        if (!editMode) return;

        var dbName = $(this).attr('data-db-value');

        var id = $(this).attr('data-id');

        var newValue = $(this).parent().parent().find(':input').val();

        var fieldName = $(this).parent().parent().find(':input').attr('name');

        var valid = $('#' + fieldName).valid();

        if (dbName != newValue && valid) {

            $(this).attr('data-db-value', newValue);

            updateValue(id, fieldName, newValue, 'museum_exhibitions');

        }

    });

       $('.toggler-info:checkbox').change(function() {

        if (!editMode) return;

        var fieldName = $(this).attr('name');

        var newValue = ($(this).is(":checked")) ? 1 : 0;

        updateValue(exhibitionId, fieldName, newValue, 'museum_exhibitions')

    });

   }

    /**
     * @function showErrorSwal
     * @param msg - the msg.
     * @description Swal alert.
     */

     var showErrorSwal = function(msg){

        Swal({

            title: 'Opss!.',

            html: msg,

            type: 'error',

            confirmButtonText: 'Entendido!'

        })
    }

    /**
     * @function window.onbeforeunload
     * @description scroll to top
     */

     window.onbeforeunload = function () { if(!editMode) window.scrollTo(0,0); }

    /**
     * @function initMap
     * @description Initialize the Google Map and create the marker.
     */

     window.initMap = function() {

        if(editMode){

            lat = $('#lat').val();

            long = $('#long').val();
        }

        var myLatlng = new google.maps.LatLng(lat, long);

        map = new google.maps.Map(document.getElementById('map'), {

            center: myLatlng,

            zoom: 15,

            mapTypeControl: false,

            fullscreenControl: false,

            zoomControl: false

        });

        geocoder = new google.maps.Geocoder();

        /*if(lat == 0.00000000) lat = -34.562386;

        if(long == 0.00000000) long = -58.447446;


        map.setCenter(new google.maps.LatLng(lat,long));

        var marker = new google.maps.Marker({

            position: new google.maps.LatLng(lat, long),

            map: map,

            title: $('#address').val()

        });*/

    }

    /**
     * @function deleteMarkers
     * @description Delete all the markers from the map....
     */

     var deleteMarkers = function() {

        for (var i = 0; i < markers.length; i++) markers[i].setMap(null);

            markers = [];

    }

    /**
     * @function geocodeAddress
     * @description geocode some addres
     */

     var geocodeAddress = function() {

        var address = document.getElementById('address').value;

        geocoder.geocode({

            'address': address

        }, function(results, status) {

            if (status === 'OK') {

                $('#lat').val(results[0].geometry.location.lat());

                $('#long').val(results[0].geometry.location.lng());

                map.setCenter(results[0].geometry.location);

                var marker = new google.maps.Marker({

                    map: map,

                    position: results[0].geometry.location

                });

                markers.push(marker);

                if (editMode) {

                    updateValue(exhibitionId, 'address', address, 'museum_exhibitions');

                    updateValue(exhibitionId, 'lat', $('#lat').val(), 'museum_exhibitions');

                    updateValue(exhibitionId, 'long', $('#long').val(), 'museum_exhibitions');

                }

            } else {

                /*Swal({

                    title: 'Opss!.',

                    html: 'Google Maps no pudo encontrar el lugar que indicaste. El mensaje que devolvió es:<br><br>' + status,

                    type: 'error',

                    confirmButtonText: 'Entendido!'

                })*/

            }

        });

    }

    /**
     * @function updateNonOrderableCheckbox
     * @description update checkbox value...
     *
     * @param {int} id                    - exhibition id.
     * @param {int} value                 - The relation id.
     * @param {int} newValue              - The the new value for the relation.
     */

     var updateNonOrderableCheckbox = function(id, value, newValue) {

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/update_non_orderable_checkboxes.php",

            type: "POST",

            data: {

                id: id,

                value: value,

                newValue: newValue,

                filter: 'museum_exhibitions_types_relations',

                table: 'id_exhibition',

                secondTable: 'id_exhibition_type'
            },

            dataType: "json"
        });

        request.done(function(result) {

            helper.showToastr("Se acualizo", 'Se actualizo');

            helper.unblockStage();

        });

        request.fail(function(jqXHR, textStatus) {


        });
    }

    /**
     * @function validateTitle
     * @description Send the URL and the permalink to validate.
     *
     * @param {String} name                 - The name of the new item.
     * @param {String} url                  - The url of the new item.
     */

     var validateTitle = function(name, url) {

        helper.blockStage("validando nombre...");

        var request = $.ajax({

            url: "private/users/services/validate_field.php",

            type: "POST",

            data: {

                name: name,

                url: url,

                table: 'museum_exhibitions'

            },

            dataType: "json"
        });

        request.done(function(result) {

            if (result.valid) {

                helper.showToastr("VALIDO! :)", "El nombre <b>" + name + "</b> está disponible para su uso.");

                $('#validate-title').removeClass('btn-info');

                $('#validate-title').addClass('btn-success');

                $('#validate-title').text('valido!');

            } else {

                $('#validate-title').addClass('btn-info');

                $('#validate-title').text('validar');

                Swal({

                    title: 'El curso ya existe o el permalink está siendo utilizado.',

                    html: 'Ya existe un link creado con la URL <b>' + url + '</b><br>No pueden haber dos cursos con la misma dirección web.<br> Probá agregandole algún dato al nombre.',

                    type: 'error',

                    confirmButtonText: 'Entendido!'

                }).then((result) => {

                    $("#name").val('');

                    $("#name").focus();

                });

            }

            helper.unblockStage();

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

            helper.unblockStage();

        });

    }

    /**
     * @function updateValue
     * @description update individual value in DB.
     *
     * @param {int} id                 - object id.
     * @param {string} fieldName       - the field to modify
     * @param {string} newValue        - object value.
     * @param {int} filter             - filter type.
     */

     var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined) {

        helper.blockStage("actualizando...");

        var request = $.ajax({

            url: "private/users/museum/general/update_value.php",

            type: "POST",

            data: {

                id: id,

                fieldName: fieldName,

                newValue: newValue,

                filter: filter
            },

            dataType: "json"

        });

        request.done(function(result) {

            console.log(result);

            helper.showToastr(result.title, result.msg);

            if (callback != undefined) callback(callbackParams);

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });

        if (callback == undefined) helper.unblockStage();

        localStorage.setItem('refresh', 'true');

    }

    function setQueryStringParameter(name, value) {

        const params = new URLSearchParams(location.search);

        params.set(name, value);

        window.history.replaceState({}, "", decodeURIComponent(`${location.pathname}?${params}`));

    }


    return {

        init: function() {

        helper.setMenu();

        editMode = ($('#edit-mode').val() == 1) ? true : false;

        if (editMode) {

            var temp;

            if (document.referrer != "") {

                temp = helper.getGETVariables(document.referrer, 'tabId');

            } else {

                temp = null;
            }

            redirectId = (temp == null) ? 1 : temp;

            imagePath = $('#image-path').val();

            exhibitionId = $('#exhibition-id').val();

            exhibitionName = $('#exhibition-name').val();

        } else {

            isFirstLoad = false;
        }

        addListeners();

        addFormValidations();

        if(editMode){

            //loadAjaxImages('museum_images', 'exhibitions', initialPreviewCoverConfig, initialPreviewCoverPaths)

            loadAjaxImages('museum_images', 'individual_exhibition', initialPreviewGalleryConfig, initialPreviewgalleryPaths);

            //loadAjaxImages('museum_youtube_videos', 'exhibition_video', initialPreviewVideosConfig, initialPreviewVideosPaths);

        }else{

            createCover();

            createGallery();

            createVideosGallery();

            helper.setMenu();

        }

        /*if(lat == 0.00000000) lat = -34.562386;

        if(long == 0.00000000) long = -58.447446;

        map.setCenter(new google.maps.LatLng(lat,long));

        var marker = new google.maps.Marker({

            position: new google.maps.LatLng(lat, long),

            map: map,

            title: $('#address').val()

        });*/

    }

};

}();

jQuery(document).ready(function() { MuseumExhibitionAdd.init(); });
