var InstitutionalInfo = function() {

    helper = Helper();

    var form = $("#institutional-form");

    var v;

    $.validator.setDefaults({

        highlight: function(element){

            $(element)

            .closest('.form-group')

            .addClass('has-danger');

        },

        unhighlight: function(element){

            $(element)

            .closest('.form-group')

            .removeClass('has-danger');

        },

        errorPlacement: function(error, element){

            if(element.prop('type')==='textarea'){

                error.insertAfter(element.next());

            }else{

                error.insertAfter(element);

            }

        }

    })

    var handleGeneralUpdateSubmit = function() {

        v = form.validate({

            onfocusout: false,

            rules: {

                mission: {required: true, minlength:20},

                vision: {required: true, minlength:20},

                about: {required: true, minlength:20},

                values: {required: true, minlength:20}

            },
            messages: {

                mission: {

                    required: helper.createErrorLabel('misión', 'REQUIRED'),

                    minlength: helper.createErrorLabel('misión', 'TOO_SHORT')

                },

                vision: {

                    required: helper.createErrorLabel('visión', 'REQUIRED'),

                    minlength: helper.createErrorLabel('visión', 'TOO_SHORT')

                },

                about: {

                    required: helper.createErrorLabel('visión', 'REQUIRED'),

                    minlength: helper.createErrorLabel('visión', 'TOO_SHORT')

                },

                values: {

                    required: helper.createErrorLabel('visión', 'REQUIRED'),

                    minlength: helper.createErrorLabel('visión', 'TOO_SHORT')

                }

            },

            invalidHandler: function (form) {

                console.log(form);

            }



        });

        myElement = $('#mission, #vision, #values, #about');

        myElement.summernote({

            disableDragAndDrop: true,

            height: 200,

            toolbar: [

            ['style', ['bold', 'italic', 'underline']],

            ['Misc', ['fullscreen', 'undo', 'redo']],

            ['link', ['linkDialogShow', 'unlink']]

            ],

            callbacks: {

                onInit: function() { },

                onEnter: function() { },

                onKeyup: function(e) { },

                onKeydown: function(e) { },

                onPaste: function(e) { },

                onChange: function(contents, $editable) {

                    var element = $(this);

                    console.log(element);

                    element.val(element.summernote('isEmpty') ? "" : contents);

                    v.element(element);
                },

                onBlur: function(){ },

                onFocus: function() { }

            }

        });

        $('#update_institutional_info, #update_institutional_presentation').click(function(e) {

            e.preventDefault();

            var btn = $(this);
            
            if (!form.valid()) { return; }

            updateGeneralInfo(btn, form);

        });

    }

    var updateGeneralInfo = function(btn, form){

        btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

        form.ajaxSubmit({

            cache: false,

            url: 'private/users/museum/institutional.php',

            type: 'post',

            dataType: 'json',

            success: function(response, status, xhr, $form) {

                console.log(response);

                helper.unblockStage();

                swal({

                    title: response.title,

                    html: response.msg,

                    type: response.alert,

                    confirmButtonText: response.button

                }).then((result) => {

                    switch(Number(response.status)){

                        case 0:
                        
                        location.reload();

                        break;

                        case 1:

                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);

                        break;

                    }

                });

            },

            error: function(response, status, xhr, $form){

                console.log("error");

                console.log(response);

            },

            beforeSubmit: function(){ helper.blockStage("actualizando..."); }

        });


    }

    return {

        init: function() {

            handleGeneralUpdateSubmit();

            helper.setMenu();

        }

    };

}();

jQuery(document).ready(function() { InstitutionalInfo.init(); });

