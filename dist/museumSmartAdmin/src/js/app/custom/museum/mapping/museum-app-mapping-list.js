
/**
 * @summary Mapping List
 *
 * @description -
 *
 * @author Colo Baggins <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */

   /**
    * @function MappingNamesList
    * @description Initialize and include all the methods of this class.
    */

    var MappingNamesList = function() {

        helper = new Helper();

        //var host = window.location.host;


         /**

         * @function addListeners
         * @description Asign all the listeners / event habdlers for this page.
         */

         var addListeners = function(){

           $("[name='my-checkbox']").bootstrapSwitch();

            $(window).focus(function() {

                var refresh = localStorage.getItem('refresh');

                if(refresh == 'true'){

                    localStorage.removeItem('refresh');

                    location.reload();

                }

            });

            $(window).blur(function() { localStorage.removeItem('refresh'); });

        }


    /**
    * @function createDataTable
    * @description Create all the datatables that we use as lists.
    */

    var createDataTable = function(){

        var table = $('table.display').DataTable({

            "language": helper.getDataTableLanguageConfig(),

            "processing": true,

            "serverSide": true,

            'serverMethod': 'post',

            "ajax": {
                url:"private/users/museum/apps/mapping/dataController.php",

                dataSrc: "data",

                dataType:'json',

                "data":function(outData){

                    // what is being sent to the server
                    outData.table = "app_museum_mapping";
                    outData.searchColumn = "name";
                    outData.searchColumnTwo = "surname";
                    //console.log(outData);
                    return outData;
                },
                dataFilter:function(inData){
                    // what is being sent back from the server (if no error)
                    console.log("Datos devueltos");

                    console.log(inData);

                    return inData;
                },
                error:function(err, status){
                    // what error is seen(it could be either server side or client side.
                    console.log(err);
                },
            },

            "columns": [

              { "data": "order", "width": "2%", responsivePriority: 4, orderable: false, targets: '_all' },

              { "data": "id", "visible": false, orderable:true },

              { "data": "name", "width": "2%", responsivePriority: 1, orderable: false, targets: '_all' },

              { "data": "surname", "width": "2%", responsivePriority: 1, orderable: false, targets: '_all' },

              { "defaultContent": '<td><input type="checkbox" class="editor-active my-checkbox" name="my-checkbox" data-size="mini" data-on-color="success" data-on-text="online" data-off-text="offline"></td>', "width": "2%", responsivePriority: 1, orderable: false, targets: '_all' },

              { "defaultContent": '<td><span><button class="edit_row m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"><i class="la la-edit"></i></button><button class="delete_row m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"><i class="la la-trash"></i></button></span></td>', "width": "2%", responsivePriority: 1, orderable: false, targets: '_all' }

            ],

            rowCallback: function ( row, data ) {

              $('input.editor-active', row).prop( 'checked', data.active == 1 ); //Setea checkbox active o no segun server data

            },
            "fnDrawCallback": function(settings){
              /*
              var api = this.api();
              var json = api.ajax.json();
              console.log("api json");
              console.log(json);*/

              //bootstrap switch draw

              $("[name='my-checkbox']").bootstrapSwitch();

            }

        });


        $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

          var closestRow = $(this).closest('tr');

          var data = table.row(closestRow).data();
          var data_row = table.row($(this).closest('tr')).data();

          var id = data_row.id;

          var name = data_row.description;

          console.log("state update: id: "+id+ " name: "+name);

          updateUserStatus(id, state, name, 'update', closestRow, table.row(closestRow), null, null, 'active');

        });


        $('tbody').on( 'click', '.edit_row', function () {
          /*
            var data_row = table.row($(this).closest('tr')).data();

            var id = data_row.id;

            console.log("row id: "+data_row.id);

            window.location.href = "museum_app_random_facts_add.php?id=" + id;*/

            var row = table.row($(this).parents('tr'));

            var data = table.row( $(this).parents('tr') ).data();

            var datos;

            // This work when data table is responsive - smaller
            if(data == undefined) {

              var selected_row = $(this).parents('tr');
              if (selected_row.hasClass('child')) {
                  selected_row = selected_row.prev();
              }

              var rowData = $('#tab-1').DataTable().row(selected_row).data();

              console.log(rowData);

              datos = rowData;

            } else {

              //This work when data table is bigger

              var row = table.row($(this).parents('tr'));
              var data_row = table.row($(this).closest('tr')).data();

              console.log(data_row);

              datos = data_row;

            }

              var id = datos.id;

              window.location.href = "museum_app_mapping_names_add.php?id=" + id;

        });


        $('tbody').on( 'click', '.delete_row', function () {

          var row = table.row($(this).parents('tr'));

          var data = table.row( $(this).parents('tr') ).data();

          var datos;

          // This work when data table is responsive - smaller
          if(data == undefined) {

            var selected_row = $(this).parents('tr');
            if (selected_row.hasClass('child')) {
                selected_row = selected_row.prev();
            }

            var rowData = $('#tab-1').DataTable().row(selected_row).data();

            console.log(rowData);

            datos = rowData;

            } else {

              //This work when data table is bigger

              var row = table.row($(this).parents('tr'));
              var data_row = table.row($(this).closest('tr')).data();

              console.log(data_row);

              datos = data_row;

            }


            var animatable = $(this).parents('tr');

            var name = datos.name;

            var id = datos.id;


            var actualTable = $("#tab-1").DataTable();

            Swal({

                title: "Eliminar Nombre",

                html: 'Esta acción eliminará el nombre <b>'+name+'</b>.<br><br>Esta acción no se puede deshacer. <br><br>Recordá que también podés usar botón de <b>online</b> y <b>offline</b> para modificar la visibilidad la misma.<br><br>¿Eliminar de todos modos?',

                type: 'error',

                showCancelButton: true,

                confirmButtonText: 'Si, eliminar',

                cancelButtonText: 'no, salir'

            }).then((result) => {

                if(result.value == true){
                  console.log("mandar a actualizar delete = 1!!!");
                  updateUserStatus(id, 'true', name, 'delete', animatable, row, actualTable, null,  'deleted');
                }

            })

        });


    }


    /**
    * @function updateUserStatus
    *
    * @description Update / Delete user status from list: we use this to mark the user as online / offline and
    * for delete an user completely from the list.
    *
    * @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
    * @param {boolean} state                    - True or false: use it for both, update and delete.
    * @param {string} name                  - The name of the person being manipulated.
    * @param {string} action                    - Posibilities: 'update' or 'delete'.
    * @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
    * @param {DataTable} row                    - Reference to the row that we are dealing with.
    * @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
    * @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
    */

    var updateUserStatus = function(id, state, name, action, animatable, row, processTable = null, order = null, defaultColumn = null){

        helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando hecho...');

        //var url = (action == 'update') ? "private/users/museum/general/update_status.php"  :

        //"private/users/museum/general/delete_item.php";

        var url = "private/users/museum/general/update_status.php";



        $.ajax({

            type: "POST",

            url: url,

            data: {id: id, status:state, action:action, name:name, source: 'mappingApp',

            table:'app_museum_mapping', defaultColumn:defaultColumn},

            success: function(result){

                console.log(result);

                helper.unblockStage();

                if(action == 'update'){

                    console.log(result.msg);

                    var newStatus = (result.changeStatus == 'true') ? 'online' : 'offline';

                    var previousStatus = (newStatus == 'online') ? 'offline' : 'online';

                    var st1 = result.msg.replace('%name%', '<b>' + name + '</b>');

                    var st2 = st1.replace('%newStatus%', '<b>' + newStatus + '</b>');

                    var st3 = st2.replace('%previousStatus%', '<b>' + previousStatus + '</b>');

                    console.log(st3);

                    helper.showToastr(result.title, st3);

                }else{

                    animatable.fadeOut('slow','linear',function(){

                        console.log(result.msg);

                        helper.showToastr('ELIMINADO', 'Esta trivia se eliminó exitosamentne.');

                        var count = 0;

                        var data = processTable.rows().data();

                        processTable.rows().every( function (rowIdx, tableLoop, rowLoop) {

                            if(processTable.cell(rowIdx, 0).data() > Number(order)){

                                var saveValue = Number(processTable.cell(rowIdx, 0).data() - 1);

                                processTable.cell(rowIdx, 0).data(saveValue);

                            }

                        });

                        row.remove().draw(true);

                        processTable.rows().invalidate().draw(false);

                    });

                }// Ends else

            }, // Ends success

            error: function(xhr, status, error) {

                console.log(xhr);

                var err = eval("(" + xhr.responseText + ")");

                alert(err.Message);
            },

            dataType: "json"
        });

    }

    /**
    * @function animateAddedElement.
    * @description animate the last object when we come from editing.
    */

    var animateAddedElement = function(){

        TweenMax.to(window, 1, {scrollTo:{y:"max"}, delay:1});

        var y = document.getElementsByClassName('row-1');

        var n = $(y).css("backgroundColor");

        var c = $(y).css("color");

        TweenMax.to(y, 1, {backgroundColor:"rgba(132,173,169,0.37)", color: '#1c9081', ease:Cubic.easeOut, delay:2});

        TweenMax.to(y, 1, {backgroundColor:n, color:c, ease:Cubic.easeOut, delay:.5, delay:3});

    }

    return {

        init: function() {

            helper.setMenu();

            addListeners();

            createDataTable();

            var highlight = (helper.getGETVariables(window.location, "highlight") == "true") ? true : false;

            if(highlight == true) animateAddedElement();

        }

    };

}();

jQuery(document).ready(function() { MappingNamesList.init(); });
