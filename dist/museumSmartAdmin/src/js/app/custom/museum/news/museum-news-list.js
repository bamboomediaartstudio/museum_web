/**
* @summary News list.
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/

/**
* @function NewsList
* @description Initialize and include all the methods of this class.
*/

var NewsList = function() {

    helper = new Helper();

    var cat;

         /**

         * @function addListeners
         * @description Asign all the listeners / event habdlers for this page.
         */

         var addListeners = function(){

            $("[name='my-checkbox']").bootstrapSwitch();

            $("[name='my-contract-checkbox']").bootstrapSwitch();

            $(window).focus(function() {

                var refresh = localStorage.getItem('refresh');

                if(refresh == 'true'){

                    localStorage.removeItem('refresh');

                    location.reload();

                } 

            });

            $(window).blur(function() { localStorage.removeItem('refresh'); });

        }


    /**
    * @function createDataTable
    * @description Create all the datatables that we use as lists.
    */

    var createDataTable = function(){

        var table = $('#tab-1').DataTable({

            "language": helper.getDataTableLanguageConfig(),

             "processing": true,

             "serverSide": true,

             'serverMethod': 'post',

             "ajax": {

                 url:"private/users/museum/news/dataController.php",

                 dataSrc: "data",

                 dataType:'json',

                 "data":function(outData){

                     // what is being sent to the server
                     
                     outData.table = "museum_news";
                     
                     outData.searchColumn = "title";
                     
                     outData.searchColumnTwo = "content";
                    
                     console.log(outData);
                     
                     return outData;
                 },
                 dataFilter:function(inData){
                                          
                     return inData;
                 },
                 error:function(err, status){
                                        
                     console.log(err);
                 },
             },

            "columns": [

            { "data": "title", "width": "85%", responsivePriority: 0, orderable: false, targets: '_all', render: function ( data, type, row ) { return data.substr( 0, 50 ) + "..." }}, 

            { "defaultContent": '',

            render: function(data, type, row) {

                var st;

                if(cat == 3){

                    st = '-';
               
                }else{

                    st = '<td><input type="checkbox" class="editor-active my-checkbox" name="my-checkbox" data-size="mini" data-on-color="success" data-on-text="online" data-off-text="offline"></td>';

                }

                return st;
           
            }, responsivePriority: 1, orderable: false, targets: '_all'

            },

        
            { "defaultContent": '',

            render: function(data, type, row) {

                var st;

                if(cat == 3){

                    st = '<td><span><button class="edit_row m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"><i class="la la-edit"></i></button><button class="add_images m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Agregar imagenes"><i class="la la-photo"></i></button><button class="add_videos m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Agregar imagenes"><i class="fab fa-youtube"></i></button><button class="open-link m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="abrir link de la muestra" > <i class="fas fa-globe-americas"></i></button></span></td>';

                }else{

                    st = '<td><span><button class="edit_row m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"><i class="la la-edit"></i></button><button class="delete_row m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"><i class="la la-trash"></i></button><button class="add_images m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Agregar imagenes"><i class="la la-photo"></i></button><button class="add_videos m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Agregar imagenes"><i class="fab fa-youtube"></i></button><button class="open-link m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="abrir link de la muestra" > <i class="fas fa-globe-americas"></i></button></span></td>';

                }

                return st;                

            }, responsivePriority: 1, orderable: false, targets: '_all'

            }
            
            ],

             rowCallback: function ( row, data ) {

                console.log(data.active + " - " + row + " - data URL: " + data.url);

               $('input.editor-active', row).prop( 'checked', data.active == 1 ); 

               $('.open-link', row).attr('data-url', data.url); 

             },

             "fnDrawCallback": function(settings){

                 $("[name='my-checkbox']").bootstrapSwitch();

             }

        });

        $('body').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

            var closestRow = $(this).closest('tr');

            var data = table.row(closestRow).data();

            var data_row = table.row($(this).closest('tr')).data();

            var id = data_row.id;

            var name = data_row.name;

            console.log("state update: id: "+id+ " name: "+name);

            updateUserStatus(id, state, name, 'update', closestRow, table.row(closestRow), null, null, 'active');

        });

        $('body').on( 'click', '.pomex', function () {

            var id = table.row( $(this).parents('tr') ).data()[1];

            window.location.href = "museum_news_add.php?id=" + id;

        });

        $('body').on( 'click', '.edit_row', function () {

            var data_row = table.row($(this).closest('tr')).data();

            var id = data_row.id;

            window.location.href = "museum_news_add.php?id=" + id;


        });

        $('body').on( 'click', '.add_images', function () {

            var data_row = table.row($(this).closest('tr')).data();

            var id = data_row.id;

            window.location.href = "museum_news_add.php?id=" + id + '&tab=images-tab';

        });

        $('body').on( 'click', '.add_videos', function (    ) {

            var data_row = table.row($(this).closest('tr')).data();

            var id = data_row.id;

            window.location.href = "museum_news_add.php?id=" + id + '&tab=videos-tab';

        });


        $('body').on( 'click', '.open-link', function () {

            var host = 'http://' + window.location.host + '/prensa/';

            var attr = $(this).attr('data-url');

            var url = host + attr;

            window.open(url, '_blank'); 

            console.log(url);

        });

        $('body').on( 'click', '.delete_row', function () {

            var row = table.row($(this).parents('tr'));

            console.log(row);

            console.log(row.data());

            var data_row = table.row($(this).closest('tr')).data();

            console.log(data_row);

            var animatable = $(this).parents('tr');

            var name = data_row.title;

            var id = data_row.id;

            var actualTable = $("#tab-1").DataTable();

            /*var row = table.row($(this).parents('tr'));

            var animatable = $(this).parents('tr');

            var data = table.row( $(this).parents('tr') ).data();

            var order = data[0];

            var id = data[1];

            var news = data[2];

            var actualTable = $("#tab-1").DataTable();*/

            Swal({

                title: "¿Eliminar Noticia?",

                html: 'Esta acción eliminará la noticia <b>'+name+'</b>.<br><br>Esta acción no se puede deshacer. <br><br>Recordá que también podés usar botón de <b>online</b> y <b>offline</b> para modificar la visibilidad de la misma.<br><br>¿Eliminar de todos modos?',

                type: 'error',

                showCancelButton: true,

                confirmButtonText: 'Si, eliminar',

                cancelButtonText: 'no, salir'

            }).then((result) => {

                if(result.value == true) updateUserStatus(id, 'true', name, 'delete', animatable, row, actualTable, null,  'deleted');

            })

        });

    }

    /**
    * @function updateUserStatus
    *
    * @description Update / Delete user status from list: we use this to mark the user as online / offline and
    * for delete an user completely from the list.
    *
    * @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
    * @param {boolean} state                    - True or false: use it for both, update and delete.
    * @param {string} name                  - The name of the person being manipulated.
    * @param {string} action                    - Posibilities: 'update' or 'delete'. 
    * @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
    * @param {DataTable} row                    - Reference to the row that we are dealing with.
    * @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
    * @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
    */

    var updateUserStatus = function(id, state, name, action, animatable, row, processTable = null, order = null, defaultColumn = null){

        console.log(defaultColumn);

        helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando opinión...');

        var url = "private/users/museum/general/update_status.php"; 

        $.ajax({

            type: "POST",

            url: url,

            data: {id: id, status:state, action:action, name:name, source: 'news',

            table:'museum_news', defaultColumn:defaultColumn},

            success: function(result){

                console.log(result);

                helper.unblockStage();

                if(action == 'update'){

                    helper.showToastr("Modificación de noticia", "Se modificó la visibilidad de una noticia.");

                }else{

                    animatable.fadeOut('slow','linear',function(){

                        console.log(result.msg);

                        helper.showToastr('ELIMINADO', 'La noticia eliminó exitosamennte.');

                        var count = 0;

                        var data = processTable.rows().data();

                        processTable.rows().every( function (rowIdx, tableLoop, rowLoop) {

                            if(processTable.cell(rowIdx, 0).data() > Number(order)){

                                var saveValue = Number(processTable.cell(rowIdx, 0).data() - 1);

                                processTable.cell(rowIdx, 0).data(saveValue);

                            }

                        });

                        row.remove().draw(true);

                        processTable.rows().invalidate().draw(false);

                    });

                }

            },

            error: function(xhr, status, error) {

                console.log(xhr);

                var err = eval("(" + xhr.responseText + ")");

                alert(err.Message);
            },

            dataType: "json"
        });

    }

    /**
    * @function animateAddedElement.
    * @description animate the last object when we come from editing.
    */

    var animateAddedElement = function(){

        TweenMax.to(window, 1, {scrollTo:{y:"max"}, delay:1});

        var y = document.getElementsByClassName('row-1');

        var n = $(y).css("backgroundColor");

        var c = $(y).css("color");

        TweenMax.to(y, 1, {backgroundColor:"rgba(132,173,169,0.37)", color: '#1c9081', ease:Cubic.easeOut, delay:2});

        TweenMax.to(y, 1, {backgroundColor:n, color:c, ease:Cubic.easeOut, delay:.5, delay:3});

    }

    return {

        init: function() {

            cat = $('#cat').val();

            addListeners();

            createDataTable();

            helper.setMenu();

            var highlight = (helper.getGETVariables(window.location, "highlight") == "true") ? true : false;
            
            if(highlight == true) animateAddedElement();

        }

    };

}();

jQuery(document).ready(function() { NewsList.init(); });