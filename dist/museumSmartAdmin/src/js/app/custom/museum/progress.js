/**
 * @summary Edit museum progress.
 *
 * @description - ...
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */


/**
* @function MuseumProgress
* @description Initialize and include all the methods of this class.
*/

var MuseumProgress = function() {

	helper = Helper();

	var v;

	var form = $('#faq-form');

	/**
	* @function addListeners
	* @description Asign all the listeners.
	*/

	var addListeners = function(){

		$("#progress_slider").ionRangeSlider({

			min: 0,
		    
		    max: 100,
		    
		    from: $('#progress').val(),
		    
		    type: "single",
		    
		    postfix: "%",

		    grid: true,

		    onFinish: function (data) {

		    	console.log(data);

		    	updateValue(1, 'progress', data.from, 'museum_progress');

		    },
		});

	}

	/**
    * @function updateValue
    * @description update individual value in DB.
    *
    * @param {int} id                 - object id.
    * @param {string} fieldName       - the field to modify
    * @param {string} newValue        - object value.
    * @param {int} filter             - filter type.
    */

    var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined){

    	helper.blockStage("actualizando...");

    	var request = $.ajax({

    		url: "private/users/museum/general/update_value.php",

    		type: "POST",

    		data: {id:id, fieldName:fieldName, newValue:newValue, filter:filter},

    		dataType: "json"

    	});

    	request.done(function(result) {

    		console.log(result);

    		helper.showToastr(result.title, result.msg);

    		if(callback != undefined) callback(callbackParams);

    	});

    	request.fail(function(jqXHR, textStatus) {

    		console.log(jqXHR);

    		console.log("error");


    	});

    	if(callback == undefined) helper.unblockStage();

    	localStorage.setItem('refresh', 'true');

    }

    return {

    	init: function() {

    		addListeners();

    	}

    };

}();

jQuery(document).ready(function() { MuseumProgress.init(); });

