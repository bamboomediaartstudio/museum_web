/**
 * @summary Add or edit quotes
 *
 * @description - ...
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */


/**
* @function InitPage
* @description Initialize and include all the methods of this class.
*/

var InitPage = function() {

	helper = Helper();

	var v;

	var form = $('#quote-form');

	/**
	* @function addFormValidations
	* @description Asign all the form validations for this page.
	*/

	var addFormValidations = function() {

		$.validator.setDefaults({

			highlight: function(element){

				$(element)

				.closest('.form-group')

				.addClass('has-danger');

			},

			unhighlight: function(element){

				$(element)

				.closest('.form-group')

				.removeClass('has-danger');

			},

			errorPlacement: function(error, element){

				if(element.prop('type')==='textarea'){

					error.insertAfter(element.next());

				}else{

					error.insertAfter(element);

				}

			}

		})

		form.validate({

			rules: {

				'quote': {required: true},

				'author': {required: true},

			},
			messages: {

				'quote': helper.createErrorLabel('frase', 'REQUIRED'),

				'author': helper.createErrorLabel('autor', 'REQUIRED')
			},

			invalidHandler: function(e, r) {

				$("#error_msg").removeClass("m--hide").show();

				window.scroll({top: 0, left: 0, behavior: 'smooth'});
			},

		});

	}

	/**
	* @function addListeners
	* @description Asign all the listeners.
	*/

	var addListeners = function(){

		myElement = $('#quote');

		myElement.summernote({

			disableDragAndDrop: true,

			height: 200,

			toolbar: [

			['style', ['bold', 'italic', 'underline']],

			['Misc', ['fullscreen', 'undo', 'redo']],

			['link', ['linkDialogShow', 'unlink']]

			],

			callbacks: {

				onInit: function() { },

				onEnter: function() { },

				onKeyup: function(e) { },

				onKeydown: function(e) { },

				onPaste: function(e) { },

				onChange: function(contents, $editable) {

					var element = $(this);

					element.val(element.summernote('isEmpty') ? "" : contents);

					v.element(element);
				},

				onBlur: function(){ },

				onFocus: function() { }

			}

		});
		
		$('#back_to_quotes').click(function(e) { window.location.replace("museum_quotes_list.php"); });

		$('#add_quote').click(function(e) {

			e.preventDefault();

			var btn = $(this);

			if (!form.valid()) { return; }

			addNewValue($('#quote').val(), $('#author').val());

		});

		$('#update_quote').click(function(e) {

			//alert("update");

			//return;

			e.preventDefault();

			var btn = $(this);

			if (!form.valid()) { return; }

			updateValue(

				$('#quote-id').val(), 

				['quote', 'author'], 

				[$('#quote').val(), $('#author').val()], 

				'museum_quotes');

		});

	}

	/**
    * @function addNewValue
    * @description add a new value to the FAQ...
    *
    * @param {string} quote  			 - the author to add.
    * @param {string} author 			 - the answer to the author :)
    */


    var addNewValue = function(quote, author){

    	console.log(quote, author);

    	helper.blockStage("Agregando nueva frase...");

    	var request = $.ajax({

    		url: "private/users/museum/quotes/quote_add.php",

    		type: "POST",

    		data: {quote:quote, author:author},

    		dataType: "json"

    	});

    	request.done(function(result) {

    		swal({

    			title: result.title,

    			allowOutsideClick: false,

    			html: result.msg,

    			type: result.alert,

    			showCancelButton: (result.status == 1) ? true : false,

    			confirmButtonText: result.button,

    			cancelButtonText: (result.status == 1) ? 'ir a la lista' : ''

    		}).then((swalResult) => {

    			if (swalResult.value) {

    				switch(Number(result.status)){

    					case 0:
    					case 1:
    					case 2:
    					case 3:
    					case 4:

    					location.reload();

    					break;

    				}

    			} else if (swalResult.dismiss === Swal.DismissReason.cancel) {

    				switch(Number(result.status)){

    					case 1:

    					window.location.replace("museum_quotes_list.php?highlight=true");

    					break;

    				}

    			}

    		});

            helper.showToastr(result.title, result.msg);

        });

    	request.fail(function(jqXHR, textStatus) {

    		console.log(jqXHR);

    		console.log("error");


    	});

    	helper.unblockStage();

    	localStorage.setItem('refresh', 'true');

    }

	/**
    * @function updateValue
    * @description update individual value in DB.
    *
    * @param {int} id                 - object id.
    * @param {string} fieldName       - the field to modify
    * @param {string} newValue        - object value.
    * @param {int} filter             - filter type.
    */

    var updateValue = function(id, fieldName, newValue, filter, callback = undefined, callbackParams = undefined){

    	helper.blockStage("actualizando...");

    	var request = $.ajax({

    		url: "private/users/museum/general/update_value.php",

    		type: "POST",

    		data: {id:id, fieldName:fieldName, newValue:newValue, filter:filter},

    		dataType: "json"

    	});

    	request.done(function(result) {

    		console.log(result);

    		helper.showToastr(result.title, result.msg);

    		if(callback != undefined) callback(callbackParams);

    	});

    	request.fail(function(jqXHR, textStatus) {

    		console.log(jqXHR);

    		console.log("error");


    	});

    	if(callback == undefined) helper.unblockStage();

    	localStorage.setItem('refresh', 'true');

    }

    return {

    	init: function() {

    		addListeners();

    		helper.setMenu();

    		addFormValidations();

    	}

    };

}();

jQuery(document).ready(function() { InitPage.init(); });

