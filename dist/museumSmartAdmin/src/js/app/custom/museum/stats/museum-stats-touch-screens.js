/**
 * @summary Manage all the users actions.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
 * @function MuseumStatsTouchScreens
 * @description Initialize and include all the methods of this class.
 */
 var MuseumStatsTouchScreens = function() {

    var helper = Helper();

    /**
    *
    * loadAverageDaysInMonthInThisYear();
    * @description ...
    */

    var loadAverageDaysInMonthInThisYear = function() {

        if ($('#chart_activity_by_month_in_year').length == 0) { return; }

        var request = $.ajax({

            url: "private/users/services/stats/get_historical_data_by_months_in_year.php",

            type: "POST", contentType: false, processData: false, dataType: "json"
        });

        request.done(function(response) {

            console.log(response);

            var chartData = {

                labels: response[0],

                datasets: [{

                    label: "Promedio de usuarios en este mes",  backgroundColor: mUtil.getColor('danger'),  data: response[1]  

                }]
                };

                var chartContainer = $('#chart_activity_by_month_in_year');

                if (chartContainer.length == 0) { return; }

                var chart = new Chart(chartContainer, {

                    type: 'bar',

                    data: chartData,

                    options: {

                        title: { display: false, },

                        tooltips: { intersect: false, mode: 'nearest', xPadding: 10, yPadding: 10, caretPadding: 10 },

                        legend: { display: false },

                        responsive: true, maintainAspectRatio: false, barRadius: 4,

                        scales: {

                            xAxes: [{ display: false, gridLines: false, stacked: true }],

                            yAxes: [{ display: false, stacked: true, gridLines: false }]
                        },

                        layout: { padding: { left: 0, right: 0, top: 0, bottom: 0 } }
                    }
                });

            });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });
    }

    /**
    *
    * loadAverageDaysInMonth();
    * @description ...
    */

    var loadAverageDaysInMonth = function() {

        if ($('#chart_average_by_days_in_month').length == 0) { return; }

        var request = $.ajax({

            url: "private/users/services/stats/get_historical_data_in_month.php",

            type: "POST", contentType: false, processData: false, dataType: "json"
        });

        request.done(function(response) {

            var chartData = {

                labels: response[0],

                datasets: [{

                    label: "Promedio de usuarios en este día del mes", backgroundColor: mUtil.getColor('success'), data: response[1]
             
                }]
            };

            var chartContainer = $('#chart_average_by_days_in_month');

            if (chartContainer.length == 0) { return; }

            var chart = new Chart(chartContainer, {

                type: 'bar', data: chartData,

                options: {

                    title: { display: false, },

                    tooltips: { intersect: false, mode: 'nearest', xPadding: 10, yPadding: 10, caretPadding: 10 },

                    legend: { display: false }, responsive: true, maintainAspectRatio: false, barRadius: 4,

                    ykeys: ["lala", "lala", "lala", "lala"],

                    scales: {

                        xAxes: [{ display: false, gridLines: false, stacked: true }],

                        yAxes: [{ display: false, stacked: true, gridLines: false }]
                    },

                    layout: { padding: { left: 0, right: 0, top: 0, bottom: 0 } }
                }
            });

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });

    }

    /**
    *
    * statsByLevel();
    * @description ...
    */

    var statsByLevel = function() {

        if ($('#chart_stats_by_level').length == 0) { return; }

        var request = $.ajax({

            url: "private/users/services/stats/get_level_data.php",

            type: "POST", contentType: false, processData: false, dataType: "json"
        });

        request.done(function(response) {

            Morris.Donut({

                element: 'chart_stats_by_level', data: response, colors: [mUtil.getColor('accent'), mUtil.getColor('danger'), mUtil.getColor('brand')]
            });

            var total = 0;

            for (var i = 0; i < response.length; i++) {

                total += Number(response[i].value);
            }

            var value1 = response[0].value * 100 / total;

            var value2 = response[1].value * 100 / total;

            var value3 = response[2].value * 100 / total;

            $('.sub').html(value1.toFixed(2) + "%");

            $('.pp').html(value3.toFixed(2) + "%");

            $('.pb').html(value2.toFixed(2) + "%");

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });
    }

    /**
    *
    * loadAverageInteractionsPerDay();
    * @description ...
    */

    var loadAverageInteractionsPerDay = function(){

        var request = $.ajax({

            url: "private/users/services/stats/get_historical_data_by_day.php",

            type: "POST", contentType: false, processData: false, dataType: "json"
        });

        request.done(function(response) {

            var config2 = {

                type: 'line',

                data: {
                    
                    labels: response[0],

                    datasets: [{

                        label: "Promedio de usuarios en este día",

                        borderColor: mUtil.getColor('brand'),

                        borderWidth: 2,

                        pointBackgroundColor: mUtil.getColor('succes'),

                        backgroundColor: mUtil.getColor('danger'),

                        pointHoverBackgroundColor: mUtil.getColor('danger'),

                        pointHoverBorderColor: Chart.helpers.color(mUtil.getColor('danger')).alpha(0.2).rgbString(),

                        data: response[1]

                    }]
                },
                options: {

                    title: { display: false, },
                    
                    tooltips: { intersect: false, mode: 'nearest', xPadding: 10, yPadding: 10, caretPadding: 10 },

                    legend: { display: false, labels: { usePointStyle: false } },

                    responsive: true,

                    maintainAspectRatio: false,

                    hover: { mode: 'index' },

                    scales: {
                        xAxes: [{ display: false, gridLines: false, scaleLabel: { display: true, labelString: 'Month' } }],
                        yAxes: [{ display: false, gridLines: false, scaleLabel: { display: true, labelString: 'Value' } }]
                    },

                    elements: { point: { radius: 3, borderWidth: 0, hoverRadius: 8, hoverBorderWidth: 2 } }
                }
            };

            var chart2 = new Chart($('#chart_activity_by_day_in_week'), config2);


        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });
    }

    /**
    *
    * loadAverageInteractionsPerHour();
    * @description ...
    */

    var loadAverageInteractionsPerHour = function() {

        if ($('#chart_activity_by_hour_today').length == 0) { return; }

        var request = $.ajax({

            url: "private/users/services/stats/get_historical_data_by_hour.php",

            type: "POST", contentType: false, processData: false, dataType: "json"
        });

        request.done(function(response) {

            var config = {

                type: 'line',

                data: {

                    labels: response[0],

                    datasets: [{

                        label: "Promedio de usuarios a esta hora",

                        borderColor: mUtil.getColor('brand'),

                        borderWidth: 2,

                        pointBackgroundColor: mUtil.getColor('success'),

                        backgroundColor: mUtil.getColor('accent'),

                        pointHoverBackgroundColor: mUtil.getColor('brand'),

                        pointHoverBorderColor: Chart.helpers.color(mUtil.getColor('brand')).alpha(0.2).rgbString(),

                        data: response[1]
                    }]
                },
                options: { title: { display: false, },

                tooltips: { intersect: true, mode: 'nearest', xPadding: 10, yPadding: 10, caretPadding: 10 },

                legend: { display: false, labels: { usePointStyle: false } }, responsive: true,  maintainAspectRatio: false,

                hover: { mode: 'index' },

                scales: {

                    xAxes: [{ display:  false, gridLines: false, scaleLabel: { display: true,  labelString: 'hora' } }],

                    yAxes: [{ display: false, gridLines: false, scaleLabel: { display: true, labelString: 'Value' } }]
                },

                elements: { point: { radius: 3, borderWidth: 0, hoverRadius: 8,  hoverBorderWidth: 2 } }
            }
        };

        var chart = new Chart($('#chart_activity_by_hour_today'), config);

    });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });

    }

    /**
    *
    * addListeners();
    * @description ...
    */

    var addListeners = function(){

        var $grid = $('.grid').isotope({itemSelector: '.element-item', layoutMode: 'fitRows' });

            var filterFns = {

                numberGreaterThan50: function() {

                    var number = $(this).find('.number').text();

                    return parseInt(number, 10) > 50;

                },

                ium: function() {

                    var name = $(this).find('.name').text();

                    return name.match(/ium$/);

                }

            };

            $('.go-to-app-data').on('click', function() {

                var id = $(this).attr('data-id');

                location.href = "museum_app_stats.php?id=" + id;

            });

            

            $('.filters-button-group').on('click', 'button', function() {

                var filterValue = $(this).attr('data-filter');

                filterValue = filterFns[filterValue] || filterValue;

                $grid.isotope({ filter: filterValue });

            });

            // change is-checked class on buttons

            $('.button-group').each(function(i, buttonGroup) {

                var $buttonGroup = $(buttonGroup);

                $buttonGroup.on('click', 'button', function() {

                    $buttonGroup.find('.is-checked').removeClass('is-checked');

                    $(this).addClass('is-checked');

                });

            });


            $('.info-button').click(function() {

                var title = '¿Qué es un usuario ' + $(this).attr('data-title') + '?';

                var content = $(this).attr('data-category');

                swal({

                    title: title,

                    allowOutsideClick: false,

                    html: content,

                    type: "success",

                    confirmButtonText: 'entendido :)'

                })

            });
    }


    return {

        init: function() {

            addListeners();

            statsByLevel();

            loadAverageInteractionsPerHour();

            loadAverageInteractionsPerDay();

            loadAverageDaysInMonth();
            
            loadAverageDaysInMonthInThisYear();

        }

    };

}();

jQuery(document).ready(function() {

    MuseumStatsTouchScreens.init();


});