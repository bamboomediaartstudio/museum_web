/**
 * @summary Manage all the users actions.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
 * @function MuseumStatsTouchScreens
 * @description Initialize and include all the methods of this class.
 */
 var MuseumStatsTouchScreens = function() {

    var helper = Helper();

    /**
    *
    * loadAverageDaysInMonthInThisYear();
    * @description ...
    */

    var loadAverageDaysInMonthInThisYear = function() {

        if ($('#chart_activity_by_month_in_year').length == 0) { return; }

        var request = $.ajax({

            url: "private/users/services/stats/get_historical_data_by_months_in_year.php",

            type: "POST", contentType: false, processData: false, dataType: "json"
        });

        request.done(function(response) {

            console.log(response);

            var chartData = {

                labels: response[0],

                datasets: [{

                    label: "Promedio de usuarios en este mes",  backgroundColor: mUtil.getColor('danger'),  data: response[1]  

                }]
            };

            var chartContainer = $('#chart_activity_by_month_in_year');

            if (chartContainer.length == 0) { return; }

            var chart = new Chart(chartContainer, {

                type: 'bar',

                data: chartData,

                options: {

                    title: { display: false, },

                    tooltips: { intersect: false, mode: 'nearest', xPadding: 10, yPadding: 10, caretPadding: 10 },

                    legend: { display: false },

                    responsive: true, maintainAspectRatio: false, barRadius: 4,

                    scales: {

                        xAxes: [{ display: false, gridLines: false, stacked: true }],

                        yAxes: [{ display: false, stacked: true, gridLines: false }]
                    },

                    layout: { padding: { left: 0, right: 0, top: 0, bottom: 0 } }
                }
            });

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });
    }

    /**
    *
    * loadAverageDaysInMonth();
    * @description ...
    */

    var loadAverageDaysInMonth = function() {

        if ($('#chart_average_by_days_in_month').length == 0) { return; }

        var request = $.ajax({

            url: "private/users/services/stats/get_historical_data_in_month.php",

            type: "POST", contentType: false, processData: false, dataType: "json"
        });

        request.done(function(response) {

            var chartData = {

                labels: response[0],

                datasets: [{

                    label: "Promedio de usuarios en este día del mes", backgroundColor: mUtil.getColor('success'), data: response[1]

                }]
            };

            var chartContainer = $('#chart_average_by_days_in_month');

            if (chartContainer.length == 0) { return; }

            var chart = new Chart(chartContainer, {

                type: 'bar', data: chartData,

                options: {

                    title: { display: false, },

                    tooltips: { intersect: false, mode: 'nearest', xPadding: 10, yPadding: 10, caretPadding: 10 },

                    legend: { display: false }, responsive: true, maintainAspectRatio: false, barRadius: 4,

                    ykeys: ["lala", "lala", "lala", "lala"],

                    scales: {

                        xAxes: [{ display: false, gridLines: false, stacked: true }],

                        yAxes: [{ display: false, stacked: true, gridLines: false }]
                    },

                    layout: { padding: { left: 0, right: 0, top: 0, bottom: 0 } }
                }
            });

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });

    }

    /**
    *
    * createCharts();
    * @description ...
    */

    var createCharts = function() {

        var a = new Array({label: "si", value: $('#guided').val()}, {label: "no", value: $('#not-guided').val()});

        Morris.Donut({ element: 'chart_stats_by_level', data: a, colors: [mUtil.getColor('warning'), mUtil.getColor('danger')] });

        var value1 = parseInt($('#guided').val()) * 100 / $('#all-rows').val();

        var value2 = 100 - value1;

        $('.op1').html(value1.toFixed(2) + "% del total.");

        $('.op2').html(value2.toFixed(2) + "% del total.");

        //...    

        var b = new Array({label: "si", value: $('#recomend').val()}, {label: "no", value: $('#not-recomend').val()});

        Morris.Donut({ element: 'chart_stats_by_level_2', data: b, colors: [mUtil.getColor('accent'), mUtil.getColor('success')] });

        var value3 = parseInt($('#recomend').val()) * 100 / $('#all-rows').val();

        var value4 = 100 - value3;

        $('.op3').html(value3.toFixed(2) + "% del total.");

        $('.op4').html(value4.toFixed(2) + "% del total.");

        //...

        var c = new Array({label: "si", value: $('#completed').val()}, {label: "no", value: $('#not-completed').val()});

        Morris.Donut({ element: 'chart_stats_by_level_3', data: c, colors: [mUtil.getColor('brand'), mUtil.getColor('accent')] });

        var value5 = parseInt($('#completed').val()) * 100 / $('#all-rows').val();

        var value6 = 100 - value5;

        $('.op5').html(value5.toFixed(2) + "% del total.");

        $('.op6').html(value6.toFixed(2) + "% del total.");

    }

    /**
    *
    * loadAverageInteractionsPerDay();
    * @description ...
    */

    var loadAverageInteractionsPerDay = function(){

        var request = $.ajax({

            url: "private/users/services/stats/get_historical_data_by_day.php",

            type: "POST", contentType: false, processData: false, dataType: "json"
        });

        request.done(function(response) {

            var config2 = {

                type: 'line',

                data: {

                    labels: response[0],

                    datasets: [{

                        label: "Promedio de usuarios en este día",

                        borderColor: mUtil.getColor('brand'),

                        borderWidth: 2,

                        pointBackgroundColor: mUtil.getColor('succes'),

                        backgroundColor: mUtil.getColor('danger'),

                        pointHoverBackgroundColor: mUtil.getColor('danger'),

                        pointHoverBorderColor: Chart.helpers.color(mUtil.getColor('danger')).alpha(0.2).rgbString(),

                        data: response[1]

                    }]
                },
                options: {

                    title: { display: false, },
                    
                    tooltips: { intersect: false, mode: 'nearest', xPadding: 10, yPadding: 10, caretPadding: 10 },

                    legend: { display: false, labels: { usePointStyle: false } },

                    responsive: true,

                    maintainAspectRatio: false,

                    hover: { mode: 'index' },

                    scales: {
                        xAxes: [{ display: false, gridLines: false, scaleLabel: { display: true, labelString: 'Month' } }],
                        yAxes: [{ display: false, gridLines: false, scaleLabel: { display: true, labelString: 'Value' } }]
                    },

                    elements: { point: { radius: 3, borderWidth: 0, hoverRadius: 8, hoverBorderWidth: 2 } }
                }
            };

            var chart2 = new Chart($('#chart_activity_by_day_in_week'), config2);


        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });
    }

    /**
    *
    * loadAverageInteractionsPerHour();
    * @description ...
    */

    var loadAverageInteractionsPerHour = function() {

        if ($('#chart_activity_by_hour_today').length == 0) { return; }

        var request = $.ajax({

            url: "private/users/services/stats/get_historical_data_by_hour.php",

            type: "POST", contentType: false, processData: false, dataType: "json"
        });

        request.done(function(response) {

            var config = {

                type: 'line',

                data: {

                    labels: response[0],

                    datasets: [{

                        label: "Promedio de usuarios a esta hora",

                        borderColor: mUtil.getColor('brand'),

                        borderWidth: 2,

                        pointBackgroundColor: mUtil.getColor('success'),

                        backgroundColor: mUtil.getColor('accent'),

                        pointHoverBackgroundColor: mUtil.getColor('brand'),

                        pointHoverBorderColor: Chart.helpers.color(mUtil.getColor('brand')).alpha(0.2).rgbString(),

                        data: response[1]
                    }]
                },
                options: { title: { display: false, },

                tooltips: { intersect: true, mode: 'nearest', xPadding: 10, yPadding: 10, caretPadding: 10 },

                legend: { display: false, labels: { usePointStyle: false } }, responsive: true,  maintainAspectRatio: false,

                hover: { mode: 'index' },

                scales: {

                    xAxes: [{ display:  false, gridLines: false, scaleLabel: { display: true,  labelString: 'hora' } }],

                    yAxes: [{ display: false, gridLines: false, scaleLabel: { display: true, labelString: 'Value' } }]
                },

                elements: { point: { radius: 3, borderWidth: 0, hoverRadius: 8,  hoverBorderWidth: 2 } }
            }
        };

        var chart = new Chart($('#chart_activity_by_hour_today'), config);

    });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

        });

    }

    var addListeners = function(){

        $('.get-content-data').click(function(){

            loadData('brand', 'Calificación del contenido:', 'content_rate');

        });

         $('.get-experience').click(function(){

            loadData('success', 'Calificación de la experiencia:', 'experience_qualification');

        });

         $('.get-attention').click(function(){

            loadData('danger', 'Calificación de la atención:', 'museum_attention');

        });

         $('.get-time').click(function(){

            loadData('warning', 'Tiempo en el museo:', 'time_in_museum');

        });

          $('.get-app-rate').click(function(){

            loadData('accent', 'App rate:', 'app_rate');

        });

         

    }

    var loadData = function(color, title, row){

        helper.blockStage("buscando datos...");

        var request = $.ajax({

            url: "private/users/services/stats/get_survey_stats.php",

            type: "POST", 

            data: { "row" : row},

            dataType: "json"

        });

        request.done(function(response) {

            console.log(response);

            helper.unblockStage();

            $('.modal-title').text(title);

            var total = $('#all-rows').val();

            var st = "";

            var div;

            var label;

            $('.add-data').html("");

            for(var i = 0; i<response[0].length; i++){

                div = '.slider_' + (i + 1);

                label = '.label_content_' + (i + 1); 

                x = response[0][i] * 100 / total;

                st += '<div class="m-widget24__item">';

                if(row == 'time_in_museum'){

                   var timeData = new Array('1 hora o menos', '1 hora', '2 horas', '3 horas', '4 horas', '5 horas', 'más de 5 horas');

                   st+= '<span class="m-widget24__desc label_content_1"><strong>' + response[0][i] + ' usuarios</strong> estuvieron <strong>' +  timeData[i] + '</strong> en el museo</span>';

                }else{

                    st+= '<span class="m-widget24__desc label_content_1"><strong>' + response[0][i] + ' usuarios</strong> calificaron con un <strong>' +  (i + 1) + '</strong></span>';

                }

                
                st += '<span class="m-widget24__stats m--font-' + color + '"> </span>';

                st += '<div class="m--space-10"></div>';

                st += '<div class="progress m-progress--sm">';

                st += '<div class="slider_' + (i+1) + ' progress-bar m--bg-' + color + '" role="progressbar" style="width:' + x +'%;"></div>';

                st += '</div><span class="m-widget24__change">(' + x.toFixed(2) + '% de los usuarios)</span><span class="m-widget24__number"></span></div>';


            }

            $('.add-data').html(st);

            $('.modal').modal('show');

        });

        request.fail(function(jqXHR, textStatus) {

            console.log(jqXHR);

            console.log("error");

            helper.unblockStage();

        });


    }




    return {

        init: function() {

            addListeners();

            createCharts();

            loadAverageInteractionsPerHour();

            loadAverageInteractionsPerDay();

            loadAverageDaysInMonth();
            
            loadAverageDaysInMonthInThisYear();

        }

    };

}();

jQuery(document).ready(function() {

    MuseumStatsTouchScreens.init();


});