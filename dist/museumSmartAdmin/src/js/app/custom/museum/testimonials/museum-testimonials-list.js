/**
 * @summary Manage all the testimonials actions.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */
Dropzone.autoDiscover = false;

/**
 * @function MuseumTestimonialsList
 * @description Initialize and include all the methods of this class.
 */

var MuseumTestimonialsList = function() {

    helper = new Helper();

    var firstLoad = true;

    var caman;

    var vibrance = 0;

    var contrast = 0;

    var exposure = 0;

    var saturation = 0;

    var sharpen = 0;

    var brightness = 0;

    var imageUniqueId;

    var userId;

    var isGreyScale = false;

    var isCustom = false;

    var result = document.getElementById('result');

    var destinationCanvas = document.getElementById('preview-canvas');

    var destCtx;

    var cropper;

    var camanImage = Caman("#preview-canvas");

    var boxDataObject;

    var canvasDataObject;

    var imageDataObject;

    var myCropper;

    var generalSpaceWidth = 550;

    var generalSpaceHeight = 550;

    var userName;

    var originalImageWasChanged = false;

    var imageWasEmpty = false;

    var fileReaderImage;

    var isFirstCrop = true;

    var previouslyDeleted = false;

    var videoModalClosed = true;

    var createCropper = function(boxData = undefined, imageData = undefined) {

        const image = document.getElementById('temp-test');

        myCropper = new Cropper(image, {

            aspectRatio: 1,

            viewMode: 3,

            responsive: true,

            restore: true,

            checkCrossOrigin: true,

            modal: true,

            guides: true,

            center: true,

            highlight: true,

            background: true,

            autoCrop: true,

            dragMode: 'move',

            movable: true,

            rotatable: true,

            scalable: true,

            zoomable: true,

            zoomOnTouch: false,

            zoomOnWheel: true,

            cropBoxMovable: true,

            cropBoxResizable: true,

            minContainerWidth: generalSpaceWidth,

            minContainerHeight: generalSpaceHeight,

            minCropBoxWidth: 50,

            minCropBoxHeight: 50,

            ready: function() {

                isNotNull = true;

                try {

                    Object.keys(boxData).forEach(function(key) {

                        if (boxData[key] == null) {

                            isNotNull = false;

                            return;
                        }

                    });

                } catch (e) {}


                if (boxData != undefined && isNotNull == true) {

                    var data = {
                        'left': Number(boxData.left),
                        'top': Number(boxData.top),

                        'width': Number(boxData.width),
                        'height': Number(boxData.height)
                    };

                    myCropper.setCropBoxData(data);

                }

                //canvas data...

                if (imageData != undefined) {

                    var data2 = {
                        'left': Number(imageData.left),
                        'top': Number(imageData.top),

                        'width': Number(imageData.width),
                        'height': Number(imageData.height)
                    };

                    myCropper.setCanvasData(data2);

                }

                renderEditedCanvas(true, 'ready');

                isFirstCrop = false;

                $('#image_modal').modal('show');

                helper.unblockStage();


            },

            cropmove: function() {},

            cropstart: function(event) {

            },

            cropend: function() {
                renderEditedCanvas(true, 'crop end');
            },

            crop: function(event) {

                var data = event.detail;

                cropper = this.cropper;

                boxDataObject = cropper.getCropBoxData();

                canvasDataObject = cropper.getCanvasData();

                imageDataObject = cropper.getImageData();

                if (!isFirstCrop) renderEditedCanvas(false, 'crop');

            },

        });

        image.addEventListener('zoom', (event) => {

            //renderEditedCanvas(true, 'zoom')

        });

    }

    /**

    * @function addListeners
    * @description Asign all the listeners / event habdlers for this page.
    */

    var addListeners = function() {

        $(window).focus(function() {

            var refresh = localStorage.getItem('refresh');

            if (refresh == 'true') {

                localStorage.removeItem('refresh');

                location.reload();

            }


        });

        $(window).blur(function() {
            localStorage.removeItem('refresh');
        });


        $('#video_modal').on('hidden.bs.modal', function() {

            videoModalClosed = true;

            $("#video_modal iframe").attr("src", $("#video_modal iframe").attr("src"));

            //$("#video_modal iframe").addClass('d-none');

        });

        $('#image_modal').on('hidden.bs.modal', function() {

            destCtx = destinationCanvas.getContext('2d');

            destCtx.clearRect(0, 0, destinationCanvas.width, destinationCanvas.height);

            $("#wo img:last-child").remove();

            if (myCropper != undefined) myCropper.reset();

            if (myCropper != undefined) myCropper.clear();

            if (myCropper != undefined) myCropper.destroy();

            camanImage.revert();

        })

        Caman.Event.listen("renderFinished", function() {

            helper.unblockModal('#image_modal .modal-content');

        });

        Caman.Event.listen("blockFinished", function(job) {});

        /*$('#image_modal').on('hidden.bs.modal', function() {
            
           
        })*/

        $("#img_saturation, #img_brightness, #img_contrast, #img_vibrance, #img_exposure, #img_sharpen").ionRangeSlider({

            min: -100,
            max: 100,
            from: 0,

            onStart: function(data) {

                setRangeSlidersValue();
            },

            onFinish: function(data) {

                isCustom = true;

                setRangeSlidersValue();

                renderEditedCanvas(true, 'on finish filter');
            }
        });

        $("input:file").change(function() {

            if (!this.files[0]) return;

            helper.blockModal('#image_modal .modal-content', 'cargando imagen...');

            var oFReader = new FileReader();

            oFReader.readAsDataURL(this.files[0]);

            oFReader.onload = function(oFREvent) {

                $('.text-alert').addClass('d-none');

                $('.images-containers').removeClass('d-none');

                fileReaderImage = this.result;

                originalImageWasChanged = true;

                //...

                isGreyScale = false;

                isCustom = false;

                //...

                if (myCropper != undefined) myCropper.destroy();

                $("#wo img:last-child").remove();

                $('#wo').prepend($('<img>', {
                    align: 'center',
                    id: 'temp-test',
                    class: 'temp-test',
                    src: this.result
                }));

                createCropper(undefined, undefined);

                $('.save-img-changes').removeClass('d-none');

                helper.unblockModal('#image_modal .modal-content');

                $("input:file")[0].value = '';

            };

        });

        $('.reset-img-content').click(function() {

            resetRangeSlidersValue();

            $('.controllers-group').addClass('d-none');

        })

        $('.save-img-changes').click(function() {

            helper.blockStage('Actualizando imagen...');

            var image = camanImage.toBase64();

            var base = image.substring("data:image/".length, image.indexOf(";base64"));

            $.ajax({

                type: "POST",

                url: "private/users/museum/images_general/update_edited_image.php",

                data: {

                    image: image,

                    imageUniqueId: imageUniqueId,

                    userId: userId,

                    userName: userName,

                    configFolder: 'testimonials/testimonials_picture_folder',

                    config: 'testimonials/testimonials_picture_folder',

                    group: 'testimonials',

                    source: 'testimonials',

                    path: 'testimonials',

                    canvasDataObject: JSON.stringify(canvasDataObject),

                    boxDataObject: JSON.stringify(boxDataObject),

                    imageDataObject: JSON.stringify(imageDataObject),

                    brightness: brightness,

                    saturation: saturation,

                    contrast: contrast,

                    vibrance: vibrance,

                    exposure: exposure,

                    sharpen: sharpen,

                    isGreyScale: isGreyScale,

                    isCustom: isCustom,

                    fileReaderImage: fileReaderImage,

                    imageWasEmpty: imageWasEmpty,

                    previouslyDeleted: previouslyDeleted
                },

                success: function(result) {

                    var action = (imageWasEmpty) ? 'agregó' : 'editó';

                    var st1 = result.msg.replace('%name%', '<b>' + userName + '</b>');

                    var st2 = st1.replace('%action%', '<b>' + action + '</b>');

                    var st3 = st2.replace('%section%', '<b>testimonios</b>');

                    helper.showToastr(result.title, st3);

                    helper.unblockStage();

                    if (imageWasEmpty) location.reload();

                },

                error: function(xhr, ajaxOptions, thrownError) {

                    console.log(xhr.responseText);

                },

                dataType: "json"

            });



        })

        $('.flip-horizontal').click(function() {
            myCropper.scale((myCropper.getData().scaleX == 1) ? -1 : 1, 1);
        });

        $('.flip-vertical').click(function() {
            myCropper.scale(1, (myCropper.getData().scaleY == 1) ? -1 : 1);
        });

        $('.zoom-in').click(function() {
            myCropper.zoom(.1);
        });

        $('.zoom-out').click(function() {
            myCropper.zoom(-.1);
        });

        $('.move-left').click(function() {
            myCropper.move(-10, 0);
        });

        $('.move-right').click(function() {
            myCropper.move(10, 0);
        });

        $('.move-up').click(function() {
            myCropper.move(0, 10);
        });

        $('.move-down').click(function() {
            myCropper.move(0, -10);
        });

        $('.convert-to-grey-scale').click(function() {

            isGreyScale = !isGreyScale;

            isCustom = false;

            setRangeSlidersValue();

            renderEditedCanvas(true, 'grey scale');

        });

        $('.custom-edition').click(function() {

            isGreyScale = false;

            ($('.controllers-group').hasClass('d-none')) ? $('.controllers-group').removeClass('d-none'): $('.controllers-group').addClass('d-none');

        });

        $('.reset-to-default').click(function() {

            isGreyScale = false;

            isCustom = false;

            resetRangeSlidersValue();

            setRangeSlidersValue();

            renderEditedCanvas(true, 'reset to default');

        });


        $("[name='my-checkbox']").bootstrapSwitch();

        window.onbeforeunload = function() {
            window.scrollTo(0, 0);
        }

        $(".nav-item").click(function() {
            history.pushState({}, "", location.pathname.split('/').pop() + "?tabId=" + $(this).attr("data-id"));
        });
    }


    /**
     * @function resetRangeSlidersValue
     * @description Reset all the range sliders.
     */

    var resetRangeSlidersValue = function() {

        $('#img_vibrance').data("ionRangeSlider").update({
            from: 0
        });

        $('#img_saturation').data("ionRangeSlider").update({
            from: 0
        });

        $('#img_sharpen').data("ionRangeSlider").update({
            from: 0
        });

        $('#img_contrast').data("ionRangeSlider").update({
            from: 0
        });

        $('#img_exposure').data("ionRangeSlider").update({
            from: 0
        });

        $('#img_brightness').data("ionRangeSlider").update({
            from: 0
        });
    }

    /**
     * @function setRangeSlidersValue
     * @description Asign values to all the variables.
     */

    var setRangeSlidersValue = function() {

        exposure = $('#img_exposure').data().from;

        saturation = $('#img_saturation').data().from;

        sharpen = $('#img_sharpen').data().from;

        brightness = $('#img_brightness').data().from;

        vibrance = $('#img_vibrance').data().from;

        contrast = $('#img_contrast').data().from;
    }

    /**
     * @function renderEditedCanvas.
     * @description Create the render canvas for CamanJS.
     * @param    {boolean}   applyFilters        - Apply filters?.
     */

    var renderEditedCanvas = function(applyFilters, origin) {

        var doRender = (applyFilters && (isCustom == true || isGreyScale == true)) ? true : false;

        if (doRender) helper.blockModal('#image_modal .modal-content', 'actualizando');

        destCtx = destinationCanvas.getContext('2d');

        destCtx.clearRect(0, 0, destinationCanvas.width, destinationCanvas.height);

        destCtx.drawImage(cropper.getCroppedCanvas({
            width: generalSpaceWidth,
            height: generalSpaceHeight,

            imageSmoothingEnabled: true,
            imageSmoothingQuality: 'high'
        }), 0, 0);

        if (doRender) {

            camanImage.reloadCanvasData();

            camanImage.exposure(parseInt(exposure));

            camanImage.saturation(parseInt(saturation));

            camanImage.sharpen(parseInt(sharpen));

            camanImage.brightness(parseInt(brightness));

            camanImage.contrast(parseInt(contrast));

            camanImage.vibrance(parseInt(vibrance));

            if (isGreyScale) camanImage.greyscale();

            camanImage.render();

        }

    }

    /**
     * @function createDataTable
     * @description Create all the datatables that we use as lists.
     */

    var createDataTable = function() {

        var table = $('table.display').DataTable({

            "pageLength": 50,

            "language": helper.getDataTableLanguageConfig(),

            rowReorder: true,

            rowReorder: {
                update: true
            },

            "ordering": true,

            bAutoWidth: false,

            processing: true,

            "columns": [

                {
                    "width": "5%",
                    responsivePriority: 4,
                    "className": "table-cell-edit",
                    orderable: true,
                    targets: 0
                },

                {
                    "visible": false,
                    orderable: false
                },

                {
                    "width": "5%",
                    responsivePriority: 0,
                    orderable: false,
                    targets: '_all'
                },

                {
                    "width": "5%",
                    responsivePriority: 5,
                    orderable: false,
                    targets: '_all'
                },

                {
                    "width": "5%",
                    responsivePriority: 2,
                    orderable: false,
                    targets: '_all'
                },

                {
                    "width": "5%",
                    responsivePriority: 2,
                    orderable: false,
                    targets: '_all'
                },

                {
                    "width": "5%",
                    responsivePriority: 1,
                    orderable: false,
                    targets: '_all'
                },

                {
                    "visible": false,
                    orderable: false
                },

                {
                    "visible": false,
                    orderable: false
                },

                {
                    "visible": false,
                    orderable: false
                },

            ]

        });

        table.on('row-reorder', function(e, diff, edit) {

            var selectedId = edit.triggerRow.data()[1];

            var selectedName = edit.triggerRow.data()[2];

            var category = edit.triggerRow.data()[9];

            var array = new Array();

            for (var i = 0, ien = diff.length; i < ien; i++) {

                var rowData = table.row(diff[i].node).data();

                console.log(rowData[1], selectedId);

                if (rowData[1] == selectedId) {

                    console.log("condition");

                    var setOld = diff[i].oldData;

                    var setNew = diff[i].newData;

                }

                array.push({
                    id: rowData[1],
                    oldPosition: diff[i].oldData,
                    newPosition: diff[i].newData,
                    name: rowData[2]
                })

            }

            if (array.length > 0) updateOrder(array, selectedName, setOld, setNew, category);

        });

        table.on('row-reordered', function(e, diff, edit) {

            for (var i = 0, ien = diff.length; i < ien; i++) {

                //$(diff[i].node).addClass("reordered");

            }

        });

        $('tbody').on('switchChange.bootstrapSwitch', 'td input[name="my-checkbox"]', function(event, state) {

            var closestRow = $(this).closest('tr');

            var data = table.row(closestRow).data();

            var id = data[1];

            var name = data[2];

            var category = data[3];

            updateUserStatus(id, state, name, category, 'update', closestRow, table.row(closestRow));

        });

        $('tbody').on('click', '.edit_row', function() {

            var id = table.row($(this).parents('tr')).data()[1];

            var sid = table.row($(this).parents('tr')).data()[7];

            var did = table.row($(this).parents('tr')).data()[8];

            //console.log(id, sid, did);

            //return;

            window.location.href = "museum_testimonial_add.php?id=" + sid;

        });

        $('tbody').on('click', '.edit_image', function() {

            previouslyDeleted = false;

            imageUniqueId = ($(this).attr('data-image-id'));

            var mimetype = ($(this).attr('data-image-mimetype'));

            var deleted = ($(this).attr('data-image-deleted'));

            var id = table.row($(this).parents('tr')).data()[7];

            var name = table.row($(this).parents('tr')).data()[2];

            userId = id;

            userName = name;

            $('.modal-title').text("imagen para " + name);

            if (imageUniqueId == "" || deleted == true) {

                imageWasEmpty = true;

                $('#image_modal').modal('show');

                $('.images-containers').addClass('d-none');

                $('.button-label').text('Agregar Imagen');

                $('.text-alert').removeClass('d-none');

                $('.save-img-changes').addClass('d-none');

            } else {

                if (deleted == 1) previouslyDeleted = true;

                $('.button-label').text('Cambiar Imagen');

                $('.text-alert').addClass('d-none');

                $('.images-containers').removeClass('d-none');

                $('.save-img-changes').removeClass('d-none');

                imageWasEmpty = false;

                helper.blockStage('Cargando imagen...');

                var imagePath = 'private/sources/images/testimonials/' + id + '/' + imageUniqueId + '/' + id + "_"

                    +
                    imageUniqueId + "_original." + mimetype + "?rand=" + Math.random();

                    console.log(imagePath);

                if (myCropper != undefined) myCropper.destroy();

                $("#wo img:last-child").remove();

                //...

                var img = new Image();

                img.onload = function() {

                    $.ajax({

                        type: "POST",

                        url: "private/users/museum/images_general/get_image_data.php",

                        data: {
                            id: id,
                            imageUniqueId: imageUniqueId
                        },

                        dataType: "json",

                        success: function(result) {

                            isGreyScale = (result.filtersData.is_grey_scale == 1) ? true : false;

                            vibrance = (result.filtersData.vibrance != null) ? result.filtersData.vibrance : 0;

                            saturation = (result.filtersData.saturation != null) ? result.filtersData.saturation : 0;

                            sharpen = (result.filtersData.sharpen != null) ? result.filtersData.sharpen : 0;

                            contrast = (result.filtersData.contrast != null) ? result.filtersData.contrast : 0;

                            exposure = (result.filtersData.exposure != null) ? result.filtersData.exposure : 0;

                            brightness = (result.filtersData.brightness != null) ? result.filtersData.brightness : 0;

                            $('#img_vibrance').data("ionRangeSlider").update({
                                from: vibrance
                            });

                            $('#img_saturation').data("ionRangeSlider").update({
                                from: saturation
                            });

                            $('#img_sharpen').data("ionRangeSlider").update({
                                from: sharpen
                            });

                            $('#img_contrast').data("ionRangeSlider").update({
                                from: contrast
                            });

                            $('#img_exposure').data("ionRangeSlider").update({
                                from: exposure
                            });

                            $('#img_brightness').data("ionRangeSlider").update({
                                from: brightness
                            });

                            if (vibrance != 0 || saturation != 0 || sharpen != 0 || contrast != 0 || exposure != 0 || brightness != 0) isCustom = true;

                            $('#wo').prepend($('<img>', {
                                align: 'center',
                                id: 'temp-test',
                                class: 'temp-test',
                                src: imagePath
                            }));

                            createCropper(result.boxData, result.canvasData);

                        },

                        error: function(xhr, status, error) {

                            var err = eval("(" + xhr.responseText + ")");

                        },

                    });

                }

                img.onError = function() {
                    helper.unblockStage();
                }

                img.src = imagePath;

            }

        });

        $('tbody').on('click', '.play_video', function() {

            var row = table.row($(this).parents('tr'));

            var data = table.row($(this).parents('tr')).data();

            var id = data[1];

            var youtubeId = $(this).attr('data-yt');

            if(youtubeId == ""){

                Swal({

                    title: "Este testimonio no tiene video.",

                    html: 'Aun no se asignó un video a este testimonio ¿Querés hacerlo ahora?',

                    type: 'error',

                    showCancelButton: true,

                    confirmButtonText: 'Si, subir video',

                    cancelButtonText: 'no, salir'

                }).then((result) => {

                    if (result.value == true){

                        window.location.href = "museum_testimonial_add.php?id=" + id;


                    } 

                });
            
            }else{

                if(videoModalClosed == true){

                    helper.blockStage('Cargando video...');

                    videoModalClosed = false;

                    $("#video_modal iframe").attr("src", "https://www.youtube.com/embed/" + youtubeId);

                    $("#video_modal iframe").on('load', function() {

                        $("#video_modal iframe").off('load');

                        $("#video_modal iframe").removeClass('d-none');

                        $('#video_modal').modal('show');

                        helper.unblockStage();

                    });

                }

            }

        });

        $('tbody').on('click', '.delete_row', function() {

            var row = table.row($(this).parents('tr'));

            var animatable = $(this).parents('tr');

            var data = table.row($(this).parents('tr')).data();

            var order = data[0];

            var id = data[1];

            var name = data[2];

            var category = data[9];

            var actualTable = $("#tab-" + data[8]).DataTable();

            Swal({

                title: "Eliminar miembro testimonio",

                html: 'Esta acción eliminará a <b>' + name + '</b> de forma definitiva del sistema, y esta acción no se puede deshacer. <br><br>Recordá que también podés usar botón de <b>online</b> y <b>offline</b> para modificar la visibilidad de <b>' + name + '</b> en el sistema. <br><br>¿Eliminar de todos modos?',

                type: 'error',

                showCancelButton: true,

                confirmButtonText: 'Si, eliminar',

                cancelButtonText: 'no, salir'

            }).then((result) => {

                if (result.value == true) updateUserStatus(id, 'true', name, category, 'delete', animatable, row, actualTable, order);

            })

        });

    }

    /**
     * @function updateOrder
     * @description Update / sort the order of the items inside the list.
     *
     * @param {Object[]} usersToSort             - Array with objects. Each object has the info for the sort.
     * @param {int} usersToSort[].id             - Object id.
     * @param {int} usersToSort[].oldData        - The old position of the object.
     * @param {int} usersToSort[].newData        - The new position of the object.
     * @param {string} usersToSort[].name        - The name of the sorted person. 
     *
     * @param {string} selectedName              - The actual person.
     * @param {int} setOld                       - The old position of the actual person.
     * @param {int} setNew                       - The new position of the actual person.
     * @param {string} category                  - The actual category of the person.
     */

    var updateOrder = function(usersToSort, selectedName, setOld, setNew, category) {

        helper.blockStage('Actualizando...');

        $.ajax({

            type: "POST",

            url: "private/users/museum/general/update_order.php",

            data: {
                array: usersToSort,
                table: 'museum_testimonials_categories_relations',
                selectedName: selectedName,
                source: 'testimonials',
                category: category
            },


            success: function(result) {

                var newStatus = (result.changeStatus == 'true') ? 'online' : 'offline';

                var previousStatus = (newStatus == 'online') ? 'offline' : 'online';

                var st1 = result.msg.replace('%name%', '<b>' + selectedName + '</b>');

                var st2 = st1.replace('%oldPosition%', '<b>' + setOld + '</b>');

                var st3 = st2.replace('%newPosition%', '<b>' + setNew + '</b>');

                var st4 = st3.replace('%category%', '<b>' + category + '</b>');

                helper.unblockStage();

                helper.showToastr(result.title, st4);

            },

            error: function(xhr, status, error) {

                var err = eval("(" + xhr.responseText + ")");

            },

            dataType: "json"
        });

    }

    /**
     * @function updateUserStatus
     *
     * @description Update / Delete user status from list: we use this to mark the user as online / offline and
     * for delete an user completely from the list.
     *
     * @param {int} id                           - The id to be modify: pay atention that this is not the user ID.
     * @param {boolean} state                    - True or false: use it for both, update and delete.
     * @param {string} name                      - The name of the person being manipulated.
     * @param {string} category                  - The category of the person being manipulated.
     * @param {string} action                    - Posibilities: 'update' or 'delete'. 
     * @param {jQuery} animatable                - We use this to fade out the div on 'delete' action.
     * @param {DataTable} row                    - Reference to the row that we are dealing with.
     * @param {?DataTable} processTable          - the entire table, so we can redraw if necessary.
     * @param {?int} order                       - the actual item order: use it to change all the subsequent data on delete.
     */

    var updateUserStatus = function(id, state, name, category, action, animatable, row, processTable = null, order = null) {

        helper.blockStage((action == 'update') ? 'Actualizando estado...' : 'Eliminando miembro...');

        var url = (action == 'update') ? "private/users/museum/general/update_status.php" :

            "private/users/museum/general/delete_item.php";

        $.ajax({

            type: "POST",

            url: url,

            data: {
                id: id,
                token: $('#token').val(),
                status: state,
                action: action,
                name: name,
                source: 'testimonials',

                table: 'museum_testimonials_categories_relations'
            },

            success: function(result) {

                helper.unblockStage();

                if (action == 'update') {

                    var newStatus = (result.changeStatus == 'true') ? 'online' : 'offline';

                    var previousStatus = (newStatus == 'online') ? 'offline' : 'online';

                    var st1 = result.msg.replace('%name%', '<b>' + name + '</b>');

                    var st2 = st1.replace('%newStatus%', '<b>' + newStatus + '</b>');

                    var st3 = st2.replace('%previousStatus%', '<b>' + previousStatus + '</b>');

                    var st4 = st3.replace('%category%', '<b>' + category + '</b>');

                    helper.showToastr(result.title, st4);

                } else {

                    animatable.fadeOut('slow', 'linear', function() {

                        row.remove().draw(true);

                        var st1 = result.msg.replace('%name%', '<b>' + name + '</b>');

                        var st2 = st1.replace('%category%', '<b>' + category + '</b>');

                        helper.showToastr(result.title, st2, 'error');

                        processTable.rows().every(function(rowIdx, tableLoop, rowLoop) {

                            if (processTable.cell(rowIdx, 0).data() > order) {

                                processTable.cell(rowIdx, 0).data(processTable.cell(rowIdx, 0).data() - 1);

                                //processTable.cell(rowIdx, 2).data("cambio");

                            }

                        })

                    });

                }

            },

            error: function(xhr, status, error) {

                console.log(xhr);

                var err = eval("(" + xhr.responseText + ")");

                alert(err.Message);
            },

            dataType: "json"
        });

    }

    /**
     * @function animateAddedElement.
     * @description animate the last object when we come from editing.
     */

    var animateAddedElement = function() {

        firstLoad = false;

        TweenMax.to(window, 1, {
            scrollTo: {
                y: "max"
            },
            delay: 1
        });

        var y = document.getElementsByClassName('row-1');

        var n = $(y).css("backgroundColor");

        var c = $(y).css("color");

        TweenMax.to(y, 1, {
            backgroundColor: "rgba(132,173,169,0.37)",
            color: '#1c9081',
            ease: Cubic.easeOut,
            delay: 2
        });

        TweenMax.to(y, 1, {
            backgroundColor: n,
            color: c,
            ease: Cubic.easeOut,
            delay: .5,
            delay: 3
        });

    }

    return {

        init: function() {

            addListeners();

            createDataTable();

            var highlight = (helper.getGETVariables(window.location, "highlight") == "true") ? true : false;

            var tabId = helper.getGETVariables(window.location, "tabId");

            if (firstLoad == true && highlight == true && tabId != null) animateAddedElement();


        }

    };

}();

jQuery(document).ready(function() {
    MuseumTestimonialsList.init();
});