/**
 * @summary Manage all the tutors actions.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
 * @function UserAccess
 * @description Initialize and include all the methods of this class.
 */
 var UserAccess = function() {

 	helper = Helper();

     var userName = $('#user-name').val();

     var adminId = $('#admin-id').val();

     var userId = $('#user-id').val();

     /**
     * @function convertToMaster
     * @description update individual value in DB.
     *
     * @param {int} id                 - object id.
     * @param {string} fieldName       - the field to modify
     * @param {string} newValue        - object value.
     * @param {int} filter             - filter type.
     */

     var convertToMaster = function(id, fieldName, newValue, filter) {

          helper.blockStage("actualizando...");

          var request = $.ajax({

               url: "private/users/museum/general/update_value.php",

               type: "POST",

               data: {

                   id: id,

                   fieldName: fieldName,

                   newValue: newValue,

                   filter: filter
              },

              dataType: "json"

         });

          request.done(function(result) {

               console.log(result);

               helper.unblockStage();

               helper.showToastr(result.title, result.msg);

               window.location.href = "users.php";

          });

          request.fail(function(jqXHR, textStatus) {

             console.log(jqXHR);

             console.log("error");

        });

     }


 	/**
     * @function updateValue
     * @description update individual value in DB.
     *
     * @param {int} adminId                 - who is asigning.
     * @param {int} userId 			        - the asigner.
     * @param {int} newValue                - the new valie to asign.
     * @param {int} groupId                 - the group.
     * @param {int} groupName               - the name of the group...just for toastr.
     */

     var updateValue = function(adminId, userId, newValue, groupId, groupName) {

     	console.log(adminId, userId, newValue, groupId, groupName);

     	helper.blockStage("asignando ...");

     	var request = $.ajax({

     		url: "private/users/museum/general/update_access.php",

     		type: "POST",

     		data: {

     			adminId: adminId,

     			userId: userId,

     			newValue: newValue,

     			groupId: groupId
     		},

     		dataType: "json"

     	});

     	request.done(function(result) {

     		console.log(result);

     		var string;

     		if(newValue == 1){

     			string = 'Le asignaste permisos de acceso a ' + userName + ' dentro de la categoría ' + groupName;

     		}else{

     			string = 'Le revocaste permisos de acceso a ' + userName + ' dentro de la categoría ' + groupName;

     		}

     		helper.showToastr('Cambio de acceso en ' + groupName, string);

     		helper.unblockStage();

     	});

     	request.fail(function(jqXHR, textStatus) {

     		console.log(jqXHR);

     		console.log("error");

     	});

     }

     var doMasteralert = function(){

          swal({

               title: "¿Convertir a " + userName + " en master?",

               allowOutsideClick: false,

               html: "No hay nada de malo en hacerlo, pero tomate un ratito y lee detenidamente este mensaje: cuando se convierte a un usuario en usuario master, comparte tu misma jerarquía. Esto quiere decir que tiene tus mismas posibilidades de control.<br><br>No vas a volver a ver esta página, ya que no le podrás asignar o quitar permisos a <strong>" + userName + "</strong>: contará con permisos totales.<br><br><strong>¿Qué pasa si quiero revertir la situación o asigné un usuario master de forma equivocada?</strong><br><br>Tranquilo, podemos quitarle la prioridad master a un usuario: comunicate con Aditivo Interactive Group S.A y nosotros nos encargramos de ello.",

               type: "warning",

               showCancelButton: true,

               confirmButtonText: 'Proceder!',

               cancelButtonText: 'salir'

          }).then((result) => {

               if (result.value) {

                    convertToMaster(userId, 'group_id', 2, 'users');

               }

          })

     }


     return {

     	init: function() {

               $('.give-master-access').click(function(){

                    doMasteralert();
               })

               $('[data-toggle="tooltip"]').tooltip();

               $('.toggler-info:checkbox').change(function() {

                    var newValue = ($(this).is(":checked")) ? 1 : 0;

                    var groupName = $(this).attr('data-name');

                    var groupId = $(this).attr('data-id');

                    updateValue(adminId, userId, newValue, groupId, groupName);

               });

          }

     };

}();

jQuery(document).ready(function() { UserAccess.init(); });