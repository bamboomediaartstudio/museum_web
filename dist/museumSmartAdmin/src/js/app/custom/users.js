/**
 * @summary Manage all the users actions.
 *
 * @description -  
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}  
 *
 * @todo Complete documentation. 
 */

/**
 * @function UsersList
 * @description Initialize and include all the methods of this class.
 */
 var UsersList = function() {

 	var helper = Helper();

 	/**
     * @function deleteUser
     * @description update individual value in DB.
     *
     * @param {int} id                 - object id.
     * @param {string} fieldName       - the field to modify
     * @param {string} newValue        - object value.
     * @param {int} filter             - filter type.
     */

     var deleteUser = function(id, fieldName, newValue, filter) {

     	helper.blockStage("actualizando...");

     	var request = $.ajax({

     		url: "private/users/museum/general/update_value.php",

     		type: "POST",

     		data: {

     			id: id,

     			fieldName: fieldName,

     			newValue: newValue,

     			filter: filter
     		},

     		dataType: "json"

     	});

     	request.done(function(result) {

     		console.log(result);

     		helper.unblockStage();

     		helper.showToastr(result.title, result.msg);

     		window.location.href = "users.php";

     	});

     	request.fail(function(jqXHR, textStatus) {

     		console.log(jqXHR);

     		console.log("error");

     	});

     }

     return {


     	init: function() {

     		$('.info-button').click(function(){

     			var title = '¿Qué es un usuario ' + $(this).attr('data-title') + '?';

     			var content = $(this).attr('data-category');

     			swal({

     				title: title,

     				allowOutsideClick: false,

     				html: content,

     				type: "success",

     				confirmButtonText: 'entendido :)'

     			})

     		});


     		$('.delete-user').click(function(){

     			var name = $(this).attr('data-name');

     			var userId = $(this).attr('data-id');

     			swal({

     				title: "¿Eliminar a " + name + "?",

     				allowOutsideClick: false,

     				html: "Si lo eliminas, " + name + " no podrá acceder más al sistema ni operar desde el admin. ¿Proceder?",

     				type: "error",

     				showCancelButton: true,

     				confirmButtonText: 'Si, eliminar',

     				cancelButtonText: 'salir'

     			}).then((result) => {

     				if (result.value) deleteUser(userId, 'deleted', 1, 'users');

     			})


     		})

     	}

     };

 }();

 jQuery(document).ready(function() { UsersList.init(); });