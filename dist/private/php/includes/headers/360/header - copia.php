<?php

$tomorrow = date("Y-m-d", time() + 86400);

$institutionsCount = $db->query('SELECT * FROM museum_virtual_classes_events WHERE booking_type = ? AND date_start > ? AND  booked_places = ? AND sid = ? and active = ? AND deleted = ?', [1, $tomorrow, 0, $classResult->id, 1, 0])->count();

$peopleCount = $db->query('SELECT * FROM museum_virtual_classes_events WHERE booking_type = ? AND date_start > ? AND  booked_places < ? AND sid = ? and active = ? AND deleted = ?', [2, $tomorrow, 100, $classResult->id, 1, 0])->count();


?>

<div id="pano"></div>

<header class="masthead masthead-individual-class text-center text-white d-flex">


      <div title="<?php echo $classResult->name; ?>" class="my-overlay my-overlay-individual-class"></div>

      <div class="container my-auto">



            <div class="col-lg-10 mx-auto">

                  <h1 class="text-uppercase main-text main-text-section-individual-class">

                        <strong>INSCRIPCIÓN AL RECORRIDO VIRTUAL 360°</strong>

                  </h1>

                  <hr class='main-separator'>

            </div>

            <div class="col-lg-8 mx-auto">

                  <p class="text-uppercase second-text class-second-text">INSCRIBITE COMPLETANDO EL FORMULARIO QUE CORRESPONDA. SELECCIONÁ DÍA Y HORARIO</p>

            </div>

            <div class="col-lg-8 mx-auto">



                  <?php if($institutionsCount >=1){ ?>

                        <a href = "#" role="button" class="mt-3 mt-lg-3 header-buttons btn-sm light go-to-virtual-reservation-institution btn btn-outline-light"> <i class="fas fa-university"></i> INSCRIPCIÓN PARA INSTITUCIONES</a>

                  <?php } ?>

                  <?php if($peopleCount >=1){ ?>

                        <a href = "#" role="button" class="mt-3 mt-lg-3 header-buttons btn-sm light go-to-virtual-reservation btn btn-outline-light"> <i class="fas fa-user"></i> INSCRIPCIÓN PARA INDIVIDUOS</a>

                  <?php } ?>

                  <div class='go-to-content'>

                        <a role="button" class="d-none d-sm-block hvr-hang mt-5 fas fa-2x fa-arrow-circle-down js-scroll-trigger"></a>

                  </div>

            </div>

            <div class="offset-lg-3 col-lg-6" id="countdown-container"></div>

      </div>



</header>


