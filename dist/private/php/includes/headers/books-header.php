<?php
?>

<header class="masthead masthead-books text-center text-white d-flex">

            <div class="my-overlay my-overlay-books"></div>

            <div class="container my-auto">

                  <div class="col-lg-10 mx-auto">

                        <h1 class="text-uppercase main-text main-text-section">

                              <strong>LIBROS A LA VENTA</strong>

                        </h1>

                        <hr class='main-separator'>

                  </div>

                  <div class="col-lg-8 mx-auto">

                        <p class="mb-5 text-uppercase second-text">Las siguientes publicaciones están a su disposición para ser adquiridas en el Museo del Holocausto.</p>

                  </div>

                  <div class="offset-lg-3 col-lg-6" id="countdown-container"></div>

            </div>

      </header>