<?php

$wording = new Wording();

$wording->load('home', 'cursos');

?>

<header class="masthead masthead-classes text-center text-white d-flex">

      <div class="my-overlay my-overlay-shoa"></div>

      <div class="container my-auto">

            <div class="col-lg-10 mx-auto">

                  <h1 class="text-uppercase main-text class-main-text-section">

                        <strong>CLASES VIRTUALES</strong>

                  </h1>

                  <hr class='main-separator'>

            </div>

            <div class="col-lg-8 mx-auto">

                  <p class="mb-5 text-uppercase second-text class-second-text">Te invitamos a conocer nuestras clases virtuales gratuitas para instituciones y particulares.</p>

            </div>

            <div class='go-to-content'>

                  <a role="button" class="hvr-hang mt-5 fas fa-2x fa-arrow-circle-down js-scroll-trigger"></a>

            </div>

      </div>

</header>