<?php

$wording = new Wording();

$wording->load('home', 'cursos');

?>

<header class="masthead masthead-courses text-center text-white d-flex">

      <div class="my-overlay my-overlay-shoa"></div>

      <div class="container my-auto">

            <div class="col-lg-10 mx-auto">

                  <h1 class="text-uppercase main-text main-text-section">

                        <strong>CURSOS</strong>

                  </h1>

                  <hr class='main-separator'>

            </div>

            <div class="col-lg-8 mx-auto">

                  <p class="mb-5 text-uppercase second-text"><?php echo $wording->get('courses_second_line');?></p>

            </div>

            <div class='go-to-content'>

                  <a role="button" class="hvr-hang mt-5 fas fa-2x fa-arrow-circle-down js-scroll-trigger"></a>

            </div>

      </div>

</header>