<?php

if($courseResult->start_date == '0000-00-00' || $courseResult->end_date == '0000-00-00' || 

      $courseResult->start_date == NULL || $courseResult->end_date == NULL){

      $finalTime = '';

}else{

      setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

      $initTime = strftime("del %A, %d de %B del %Y", strtotime($courseResult->start_date));
      
      $endTime = strftime("al %A, %d de %B del %Y", strtotime($courseResult->end_date));

      $finalTime = utf8_encode($initTime . ' ' . $endTime);

}

$detect = new Mobile_Detect;

$isMobile;

if ( $detect->isMobile() ) {

      $isMobile = true;

}else{

      $isMobile = false;

}

//modalities...

$modalitiesSstring = '<strong>MODALIDAD: </strong>';

$queryModalities = $db->query(

      'SELECT * from museum_courses_modalities_relations as mcmr

      inner join museum_courses_modalities as mcm on

      mcmr.id_modality = mcm.id

      WHERE mcmr.id_course = ?', [$courseResult->mid]);

foreach($queryModalities->results() as $modality){

      $modalitiesSstring .= $modality->course_modality . ' / ';

}

$modalitiesSstring = substr($modalitiesSstring, 0, -3);

//enrollment:

$allowEnrollment = $courseResult->allow_enrollment;

//download pdf:

$downloadPDF = $courseResult->allow_program_download;

if($downloadPDF){

      $pdfPath = '../../museumSmartAdmin/dist/default/private/sources/pdf/courses/' . $courseResult->mid . '/pdf/' . $courseResult->url . '.pdf';

}

$whatsappString = urlencode('Hola, ¿cómo estás? Te contacto para información respecto al curso *_' . $courseResult->name . '_*');

$whatsappURL = 'https://wa.me/' . $courseResult->whatsapp . '?text=' . $whatsappString;


?>

<header class="masthead masthead-individual-course text-center text-white d-flex">

      <div id='background-image-main-container'></div>

      <div title="<?php echo $courseResult->name . ', ' . $finalTime; ?>" class="my-overlay my-overlay-individual-course"></div>

      <div class="container my-auto">

            <div class="col-lg-10 mx-auto">

                  <h1 class="text-uppercase main-text main-text-section-individual-course">

                        <strong><?php echo $courseResult->name;?></strong>

                  </h1>

                  <hr class='main-separator'>

            </div>

            <div class="col-lg-8 mx-auto">

                  <p class="text-uppercase second-text"><?php echo $finalTime;?></p>
                  
                  <!--<p class="text-uppercase second-text"><?php echo $modalitiesSstring;?></p>-->

                  <?php if($allowEnrollment){ ?>

                        <a role="button" class="mt-lg-3 header-buttons btn-sm inscription-to-course light btn btn-outline-light"><i class="fas fa-pen"></i> INSCRIBITE</a>

                  <?php } ?>

                  <?php if($isMobile){ ?>
                       
                        <a href= "<?php echo $whatsappURL;?>" role="button" class="mt-lg-3 header-buttons btn-sm light btn btn-outline-light"><i class="fab fa-whatsapp"></i> CONTACTANOS</a>

                  <?php } ?>


                  <?php if($downloadPDF){ ?>

                        <a href = "<?php echo $pdfPath;?>" role="button" class="mt-lg-3 header-buttons btn-sm light more-about-course btn btn-outline-light" download> <i class="fas fa-download"></i> BAJAR PROGRAMA</a>

                  <?php } ?>

                  <div class='go-to-content'>

                        <a role="button" class="d-none d-sm-block hvr-hang mt-5 fas fa-2x fa-arrow-circle-down js-scroll-trigger"></a>

                  </div>

            </div>

            <div class="offset-lg-3 col-lg-6" id="countdown-container"></div>

      </div>

</header>

