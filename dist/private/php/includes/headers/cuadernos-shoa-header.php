<?php
?>

<header class="masthead masthead-cuadernos text-center text-white d-flex">

            <div class="my-overlay my-overlay-cuadernos"></div>

            <div class="container my-auto">

                  <div class="col-lg-10 mx-auto">

                        <h1 class="text-uppercase main-text main-text-section">

                              <strong>CUADERNOS DE LA SHOÁ</strong>

                        </h1>

                        <hr class='main-separator'>

                  </div>

                  <div class="col-lg-8 mx-auto">

                        <p class="mb-5 text-uppercase second-text">Descargá de forma gratuita los cuadernos de La Shoá.</p>

                  </div>

                  <div class="offset-lg-3 col-lg-6" id="countdown-container"></div>

            </div>

      </header>