<?php

(isset($origin) && $origin == 'home') ? $path = '' : $path = '../';

(isset($origin) && $origin == 'home') ? $openPath = 'eventos/' : $openPath = '';


setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

$db = DB::getInstance();

$result = $db->query('

      SELECT *, 

      mc.id                                     as mid, 

      img.unique_id                             as uid,

      mc.description                            as event_description

      from museum_events                        as mc

      LEFT JOIN museum_images                   as img 

      ON                                        mc.id = img.sid 

      AND img.source                            = ?

      AND img.active                            = ?

      AND img.deleted                           = ?

      WHERE mc.active                           = ? 

      AND mc.deleted                            = ? 

      AND mc.is_highlighted                     = ?

      order by                                   mc.id'

      ,                            ['events', 1, 0, 1, 0, 1]);

?>

<header class="text-center text-white d-flex events-masthead">

      <div class="container-fluid">

            <div class="mk-item">

                  <?php foreach($result->results() as $result){ 

                        $url = $openPath . $result->url;

                        if($result->uid != null){

                              $defaultImg = true;

                              $myImg = $path . 'museumSmartAdmin/dist/default/private/sources/images/events/' . $result->mid . '/' . $result->uid . '/' . $result->mid . '_' . $result->uid . '_original.jpeg';

                              $myImgRetina = $path . 'museumSmartAdmin/dist/default/private/sources/images/events/' . $result->mid . '/' . $result->uid . '/' . $result->mid . '_' . $result->uid . '_original@2x.jpeg';

                        }else{

                              $defaultImg = false;

                              $myImg = $path . 'private/img/templates/image-template/template_original.jpg';

                              $myImgRetina = $path . 'private/img/templates/image-template/template_original@2x.jpg';

                        }

                        $start = utf8_encode(strftime("%A, %d de %B de %Y", strtotime($result->start_date)));
                        
                        $startTime = utf8_encode(strftime("%H:%M", strtotime($result->start_date)));

                        ?>

                        <div class="single-item-slider">

                              <div class='background-image-main-container' 

                              style="background-position: center center  !important; background-size: cover !important; background: url(<?php echo $myImgRetina;?>"></div>

                              <div class="my-overlay my-overlay-shoa"></div>

                              <div class="container my-auto">

                                    <div class="col-lg-10 mx-auto">

                                          <h1 class="text-uppercase main-text main-text-event-title ">

                                                <strong></strong>

                                                <strong><?php echo $result->name;?></strong>

                                          </h1>

                                          <hr class='main-separator'>

                                    </div>

                                    <div class="col-lg-8 mx-auto main-text-event-added">

                                          <p class="m text-uppercase second-text"><i class="fal fa-calendar mr-1"></i> <?php echo $start;?></p>

                                           <p class="m text-uppercase second-text"><i class="fal fa-clock mr-1"></i> <?php echo $startTime;?></p>

                                    </div>

                                    <!--<div class="col-lg-8 mx-auto">

                                          <p class="text-uppercase third-text text-light one-line-ellipses"><?php echo 
                                          $result->event_description;?></p>

                                    </div>-->   


                                    <div class="col-lg-8 mx-auto main-text-event-buttons">

                                          <a role="button" href= "<?php echo $url;?>"  class="read-main-news mt-lg-3 header-buttons btn-sm light btn btn-outline-light"><i class="far fa-calendar-alt"></i>      IR AL EVENTO</a>
                
                                    </div>

                              </div>

                        </div>

                  <?php } ?>

            </div>

      </div>

</header>