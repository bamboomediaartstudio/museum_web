<?php


setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

$initTime = strftime("del %A, %d de %B del %Y", strtotime($itemResult->start_date));

$endTime = strftime("al %A, %d de %B del %Y", strtotime($itemResult->end_date));

$finalTime = utf8_encode($initTime . ' ' . $endTime);

///echo $itemResult->name;

//echo $finalTime;

$detect = new Mobile_Detect;

$isMobile;

if ( $detect->isMobile() ) {

      $isMobile = true;

}else{

      $isMobile = false;

}

$allowContract = $itemResult->allow_contract;

$whatsappString = urlencode('Hola, ¿cómo estás? Te contacto para información respecto a la muestra *_' . $itemResult->name . '_*');

$useWhatsApp = $itemResult->whatsapp;

$useEmail = $itemResult->email;

$whatsappURL = 'https://wa.me/' . $itemResult->whatsapp . '?text=' . $whatsappString;


?>

<header class="text-center text-white d-flex events-masthead">

      <div class="container-fluid">

            <div class="mk-item">

                  <div class="single-item-slider">

                        <div class='background-image-main-container' 

                        style="background-position: center center  !important; background-size: cover !important;"></div>

                        <div class="my-overlay my-overlay-shoa"></div>

                        <div class="container my-auto">

                              <div class="col-lg-10 mx-auto">

                                    <h1 class="text-uppercase main-text main-text-exhibition-title text-truncate">

                                          <strong></strong>

                                          <strong><?php echo $itemResult->name;?></strong>

                                    </h1>

                                    <hr class='main-separator'>

                              </div>

                              <div class="col-lg-8 mx-auto">

                                    <p class="text-uppercase second-text"><?php echo $finalTime;?></p>

                              </div>

                              

                              <div class="col-lg-8 mx-auto header-buttons-group">

                                    <?php if($allowContract){ ?>

                                          <a role="button" href="#" class="inscription-to-event mt-lg-3 header-button btn-sm light btn btn-outline-light"><i class="fas fa-th-list mr-2"></i>SOLICITAR MUESTRA</a>


                                    <?php } ?>

                                    <?php if($useWhatsApp != ''){ ?>

                                    <a href= "<?php echo $whatsappURL;?>" role="button" class="mt-lg-3 header-button btn-sm light btn btn-outline-light mr-2"><i class="fab fa-whatsapp"></i> CONTACTANOS</a>

                                    <?php } ?>

                                    <?php if($useEmail != '' && $isMobile == true){ 

                                          $emailSubject = 'Información de la muestra ' . $itemResult->name;

                                          $emailBody = 'Hola, te escribo para solicitar más información respecto a la muestra: ' . $itemResult->name;

                                          $hrefMailString = 'mailto:' . $useEmail . '?subject=' . $emailSubject . '&body=' . $emailBody;

                                          ?>

                                          <a href= "<?php echo $hrefMailString;?>" role="button" class="mt-lg-3 header-button btn-sm light btn btn-outline-light mr-2"><i class="fas fa-envelope"></i> ESCRIBINOS</a>


                                    <?php } ?>

                              </div>

                        </div>

                  </div>

            </div>

      </div>

</header>

