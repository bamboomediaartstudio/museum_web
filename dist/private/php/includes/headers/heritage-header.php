<?php

$query = DB::getInstance()->query('SELECT * from museum_quotes WHERE active = ? AND deleted = ? ORDER BY RAND() LIMIT 1', [1, 0]);

$quote = $query->first()->quote;

$author = $query->first()->author;


?>

<header class="masthead masthead-heritage text-center text-white d-flex">

  <div class="my-overlay my-overlay-library"></div>

  <div class="container my-auto">

    <div class="col-lg-10 mx-auto">

      <h1 class="text-uppercase main-text main-text-section">

        <strong>PATRIMONIO</strong>

      </h1>

      <hr class='main-separator'>

    </div>

    <div class="col-lg-8 mx-auto">

      <p class="mb-2 text-uppercase second-text">«<?php echo trim($quote);?>»</p><p class="quote-text"><?php echo $author;?></p>

    </div>

    <div class='go-to-content'>

      <a role="button" class="hvr-hang mt-5 fas fa-2x fa-arrow-circle-down js-scroll-trigger"></a>

    </div>

  </div>

</header>

<!--<div class="container mt-5">

  <div class="row">

    <div class="col-lg-12 text-center">

      <h2 class="section-heading module-title"><?php echo $wording->get('heritage_first_line');
      ;?></h2>

      <hr class="module-separator">

    </div>

    <div class="col-lg-8 mx-auto">

      <p class="text-light text-faded mb-5 text-center about-reflexiones module-text"><?php echo $wording->get('heritage_second_line');
      ;?></p>

    </div>

  </div>

</div>-->