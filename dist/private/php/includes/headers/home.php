<?php ?>

<header class="masthead masthead-home text-center text-white d-flex">

	<!--<div class="video-contain">-->

		<video class="d-none d-xl-block" id="background-video" width="100%" controls autoplay muted loop>
			<source src="movie.mp4" type="video/mp4">
			<source src="movie.ogg" type="video/ogg">

			<track src="private/vtt/myvideo_es.vtt" kind="descriptions" srclang="es" label="Español">

			<track src="private/vtt/myvideo_es.vtt" kind="captions" srclang="es" label="Español">


		</video>

			<!--</div>-->

			<div class="my-overlay"></div>

			<div class="container my-auto">

				<div class="col-lg-10 mx-auto">

					<h1 class="text-uppercase main-text">

						<strong><?php echo $wording->get('landing_first_line');?></strong>

					</h1>

					<hr class='main-separator'>

				</div>

				<div class="col-lg-8 mx-auto">

					<p class="mb-5 text-uppercase second-text"><?php echo $wording->get('landing_second_line');?></p>

				</div>

				<div class="col-lg-8 mx-auto main-text-event-buttons">

                                                <a role="button" href= "visitas"  class="read-main-news mt-lg-3 header-buttons btn-sm light btn btn-outline-light"><i class="fal fa-ticket-alt mr-2"></i>RESERVÁ TU VISITA</a>

                                          </div>

				<!--<div class="offset-lg-3 col-lg-6" id="countdown-container"></div>-->

			</div>

		</header>