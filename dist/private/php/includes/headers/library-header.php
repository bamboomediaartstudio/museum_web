<?php
?>

<header class="masthead masthead-library text-center text-white d-flex">

      <div class="my-overlay my-overlay-library"></div>

      <div class="container my-auto">

            <div class="col-lg-10 mx-auto">

                  <h1 class="text-uppercase main-text main-text-section">

                        <strong>BIBLIOTECA</strong>

                  </h1>

                  <hr class='main-separator'>

            </div>

            <div class="container">

                  <div class="row">

                        <div class="col-lg-4 col-md-4 text-center">

                              <div class="service-box mx-auto sr-icon-1 mt-3">

                                    <span class="fa-stack fa-1x center">

                                          <i class="fal fa-circle fa-stack-2x"></i>

                                          <i class="fas fa-calendar-alt fa-stack-1x"></i>

                                    </span>

                                    <h3 class="contact-heading mt-3 mb-3 text-medium white">DÍAS Y HORARIOS</h3>

                                    <p class="contact-heading-text white mb-0 text-light">De lunes a jueves.</p>


                              </div>

                        </div>

                        <div class="col-lg-4 col-md-4 text-center">

                              <div class="service-box mt-3 mx-auto sr-icon-1">

                                    <span class="fa-stack fa-1x center">

                                          <i class="fal fa-circle fa-stack-2x"></i>

                                          <i class="fas fa-map fa-stack-1x"></i>

                                    </span>

                                    <h3 class="contact-heading mt-3 mb-3 text-medium white">LUGAR</h3>

                                    <p class="contact-heading-text mb-0 text-light text-up">

                                          José Hernández 1750, Buenos Aires, CABA.

                                    </p>

                              </div><br>

                        </div>

                        <div class="col-lg-4 col-md-4 text-center">

                              <div class="service-box mt-3 mx-auto sr-icon-1">

                                    <span class="fa-stack fa-1x center">

                                          <i class="fal fa-circle fa-stack-2x"></i>

                                          <i class="fas fa-envelope fa-stack-1x"></i>

                                    </span>

                                    <h3 class="contact-heading mt-3 mb-3 text-medium white">CONTACTO</h3>

                                    <p class=" mb-0 text-light text-up white"><a href="biblioteca@museodelholocausto.org.ar" class="white">biblioteca@museodelholocausto.org.ar</a></p>

                              </div>

                        </div>

                  </div>

                  <div class='go-to-content'>

                        <a role="button" class="hvr-hang mt-5 fas fa-2x fa-arrow-circle-down js-scroll-trigger"></a>

                  </div>

            </div>

      </div>

</header>