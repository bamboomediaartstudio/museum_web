<?php
$query = DB::getInstance()->query("SELECT * FROM museum_data");

$te = $query->first()->te;

$te_2 = $query->first()->te_2;

$te_3 = $query->first()->te_3;

$email = $query->first()->email;

$email_2 = $query->first()->email_2;

$email_3 = $query->first()->email_3;

$address = $query->first()->address . $query->first()->address_number . ', ' . $query->first()->zip_code . ', ' . $query->first()->city . '(' . $query->first()->dates_and_times .')';

$address_number = $query->first()->address_number;

$city = $query->first()->city;

$city = $query->first()->city;

$zip_code = $query->first()->zip_code;

?>


<header class="masthead masthead-museum text-center text-white d-flex institucional">
      
      <div class="my-overlay my-overlay-shoa"></div>

      <div class="container my-auto">

            <div class="col-lg-10 mx-auto">

                  <h1 class="text-uppercase main-text main-text-section">

                        <strong>ACERCA DEL MUSEO</strong>

                  </h1>

                  <hr class='main-separator'>

            </div>

            <div class="container">

                  <div class="row">

                        <div class="col-lg-4 col-md-4 text-center">

                              <div class="service-box mx-auto sr-icon-1 mt-3">

                                    <span class="fa-stack fa-1x center">

                                          <i class="fal fa-circle fa-stack-2x"></i>

                                          <i class="fas fa-phone fa-stack-1x"></i>

                                    </span>

                                    <h3 class="contact-heading mt-3 mb-3 text-medium white">LLAMANOS</h3>

                                    <p class="contact-heading-text text-uppercase white mb-0 text-light">

                                          <a class="medium-text white" href="tel:<?php echo $te;?>"><?php echo $te;?></a>

                                          <?php if(isset($te_2) && $te_2 != ""){ ?>

                                           / <a class="medium-text white" href="tel:<?php echo $te_2;?>"><?php echo $te_2;?></a>

                                     <?php } ?>

                                     <?php if(isset($te_3) && $te_3 != ""){ ?>

                                           / <a class="medium-text white" href="tel:<?php echo $te_3;?>"><?php echo $te_3;?></a>

                                     <?php } ?></p>


                         </div>

                   </div>

                   <div class="col-lg-4 col-md-4 text-center">

                        <div class="service-box mt-3 mx-auto sr-icon-1">

                              <span class="fa-stack fa-1x center">

                                    <i class="fal fa-circle fa-stack-2x"></i>

                                    <i class="fas fa-envelope fa-stack-1x"></i>

                              </span>

                              <h3 class="contact-heading mt-3 mb-3 text-medium white">ESCRIBINOS</h3>

                              <p class="contact-heading-text mb-0 text-light text-up">

                                    <a class="medium-text white" href="mailto:<?php echo $email;?>"><?php echo $email;?></a>

                                    <?php if(isset($email_2) && $email_2 != ""){ ?>

                                     / <a class="medium-text white" href="tel:<?php echo $email_2;?>"><?php echo $email_2;?></a>

                               <?php } ?>

                               <?php if(isset($email_3) && $email_3 != ""){ ?>

                                     / <a class="medium-text white" href="tel:<?php echo $email_3;?>"><?php echo $email_3;?></a>

                               <?php } ?>
                         </p>

                   </div><br>

             </div>

             <div class="col-lg-4 col-md-4 text-center">

                  <div class="service-box mt-3 mx-auto sr-icon-1">

                        <span class="fa-stack fa-1x center">

                              <i class="fal fa-circle fa-stack-2x"></i>

                              <i class="fas fa-map-marker-alt fa-stack-1x"></i>

                        </span>

                        <h3 class="contact-heading mt-3 mb-3 text-medium white">VISITANOS</h3>

                        <p class="contact-heading-text mb-0 text-light text-up"><?php echo $address;?></p>

                  </div>

            </div>

      </div>

      <div class='go-to-content'>

                  <a role="button" class="hvr-hang mt-5 fas fa-2x fa-arrow-circle-down js-scroll-trigger"></a>

            </div>

</div>


</div>

</header>