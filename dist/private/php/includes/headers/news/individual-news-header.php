<?php

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

$added = strftime("%A, %d de %B del %Y", strtotime($itemResult->added));

$finalTime = utf8_encode($added);


$detect = new Mobile_Detect;

$isMobile;

if ( $detect->isMobile() ) {

      $isMobile = true;

}else{

      $isMobile = false;

}

//modalities...

?>

<header class="masthead text-center text-white d-flex internal-news-masthead">

      <div id='background-image-main-container'></div>

      <div title="<?php echo $itemResult->title . ', ' . $finalTime; ?>" class="my-overlay my-overlay-individual-news"></div>

      <div class="container my-auto">

            <div class="col-lg-10 mx-auto">

                  <h1 class="text-uppercase main-text main-text-section-individual-news">

                        <strong><?php echo $itemResult->title;?></strong>

                  </h1>

                  <hr class='main-separator'>

            </div>

            <div class="col-lg-8 mx-auto">

                  <p class="text-uppercase second-text"><?php echo $finalTime;?></p>
                  
                  <p class="text-uppercase second-text"><i><?php echo $itemResult->caption;?></i></p>
                  
                  <div class='go-to-content'>

                        <a role="button" class="d-none d-sm-block hvr-hang mt-5 fas fa-2x fa-arrow-circle-down js-scroll-trigger"></a>

                  </div>

            </div>

            <div class="offset-lg-3 col-lg-6" id="countdown-container"></div>

      </div>

</header>

