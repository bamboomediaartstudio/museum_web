<?php
?>

<header class="masthead masthead-courses text-center text-white d-flex">

      <div class="my-overlay my-overlay-shoa"></div>

      <div class="container my-auto">

            <div class="col-lg-10 mx-auto">

                  <h1 class="text-uppercase main-text main-text-section">

                        <strong>MUESTRAS</strong>

                  </h1>

                  <hr class='main-separator'>

            </div>

            <div class="col-lg-8 mx-auto">

                  <p class="mb-5 text-uppercase second-text">Nuestras muestras.</p>

            </div>

            <div class="offset-lg-3 col-lg-6" id="countdown-container"></div>

      </div>

</header>