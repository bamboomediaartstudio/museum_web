<?php
$query = DB::getInstance()->query("SELECT * FROM museum_data");

$te = $query->first()->te;

$te_2 = $query->first()->te_2;

$te_3 = $query->first()->te_3;

$email = $query->first()->email;

$email_2 = $query->first()->email_2;

$email_3 = $query->first()->email_3;

$address = $query->first()->address . $query->first()->address_number . ', ' . $query->first()->zip_code . ', ' . $query->first()->city . '(' . $query->first()->dates_and_times .')';

$address_number = $query->first()->address_number;

$city = $query->first()->city;

$city = $query->first()->city;

$zip_code = $query->first()->zip_code;

?>


<header class="masthead masthead-remodelacion text-center text-white d-flex institucional">

  <div class="my-overlay my-overlay-shoa"></div>

  <div class="container my-auto">

    <div class="col-lg-10 mx-auto">

      <h1 class="text-uppercase main-text main-text-section">

        <strong>LA REMODELACIÓN</strong>

      </h1>

      <hr class='main-separator'>

    </div>

    <div class="container">

      <div class="row">

        <div class="col-lg-6 col-md-6 text-center">

          <div class="service-box mx-auto sr-icon-1 mt-3">

            <span class="fa-stack fa-1x center">

              <i class="fal fa-circle fa-stack-2x white"></i>

              <i class="fas fa-building fa-stack-1x white"></i>

            </span>

            <h3 class="contact-heading mt-3 mb-3 text-medium white">SUPERFICIE TOTAL: 3154 M2</h3>  

          </div>

        </div>

        <div class="col-lg-6 col-md-6 text-center">

          <div class="service-box mt-3 mx-auto sr-icon-1">

            <span class="fa-stack fa-1x center">

              <i class="fal fa-circle fa-stack-2x white"></i>

              <i class="fas fa-building fa-stack-1x white"></i>

            </span>

            <h3 class="contact-heading mt-3 mb-3 text-medium white">MUESTRA PRINCIPAL: 780 M2</h3>
            

          </div><br>

        </div>

      </div>

      <div class='go-to-content'>

        <a role="button" class="hvr-hang mt-5 fas fa-2x fa-arrow-circle-down js-scroll-trigger"></a>

      </div>

    </div>


  </div>

</header>