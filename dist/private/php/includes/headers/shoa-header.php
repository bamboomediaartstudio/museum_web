<?php

$query = DB::getInstance()->query('SELECT * from museum_quotes WHERE active = ? AND deleted = ? ORDER BY RAND() LIMIT 1', [1, 0]);

      $quote = $query->first()->quote;
      
      $author = $query->first()->author;

?>

<header class="masthead masthead-shoa text-center text-white d-flex">

            <div class="my-overlay my-overlay-shoa"></div>

            <div class="container my-auto">

                  <div class="col-lg-10 mx-auto">

                        <h1 class="text-uppercase main-text main-text-section">

                              <strong>LA SHOÁ</strong>

                        </h1>

                        <hr class='main-separator'>

                  </div>

                  <div class="col-lg-8 mx-auto">

                        <!--<p class="mb-5 text-uppercase second-text">«<?php echo $quote;?>»</p><p class="quote-text"><?php echo $author;?></p>-->

                        <p class="mb-5 text-uppercase second-text"> “la exigencia de que Auschwitz no se repita es la primera de todas en la educación”</p><p class="quote-text">Adorno, Theodor</p>

                  </div>

            </div>

      </header>