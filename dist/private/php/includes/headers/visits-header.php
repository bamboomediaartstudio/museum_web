<?php
$query = DB::getInstance()->query("SELECT * FROM museum_data");

$te = $query->first()->te;

$te_2 = $query->first()->te_2;

$te_3 = $query->first()->te_3;

$email = $query->first()->email;

$email_2 = $query->first()->email_2;

$email_3 = $query->first()->email_3;

$address = $query->first()->address . $query->first()->address_number . ', ' . $query->first()->zip_code . ', ' . $query->first()->city;// . '(' . $query->first()->dates_and_times .')';

$address_number = $query->first()->address_number;

$city = $query->first()->city;

$city = $query->first()->city;

$zip_code = $query->first()->zip_code;

?>


<header class="masthead masthead-reflexiones text-center text-white d-flex dias-y-horarios">

      <!--<video class="d-none d-md-block" id="background-video" width="100%" controls autoplay muted loop>

                  <source src="../private/videos/reflexiones.mp4" type="video/mp4">
                        
                        <source src="../private/videos/reflexiones.ogg" type="video/ogg">

                              ...

                        </video>-->

      <div class="my-overlay my-overlay-shoa"></div>

      <div class="container my-auto">

            <div class="col-lg-10 mx-auto">

                  <h1 class="text-uppercase main-text  main-text-section-reopen">

                        <strong>REABRIMOS LA MUESTRA</strong>

                  </h1>

                  <!--<p><strong>Desde el miércoles 18 de noviembre, reabrimos la nueva exhibición permanente para que la puedas recorrer de forma autónoma con audioguía y con todos los protocolos de limpieza y desinfección para cuidar la salud.</strong></p>-->


                  <hr class='main-separator'>

            </div>

            <div class="container mt-5">

                  <div class="row">

                        <div class="col-lg-3 col-md-3 text-center">

                              <div class="service-box mx-auto sr-icon-1 mt-3">

                                    <span class="fa-stack fa-1x center">

                                          <i class="fas fa-circle fa-stack-2x white"></i>

                                          <i class="fas fa-clock fa-stack-1x custom-item-color"></i>

                                    </span>

                                    <!--<h3 class="contact-heading mt-3 mb-3 text-medium white">DÍAS Y HORARIOS</h3>-->

                                    <p class="contact-heading-text white mb-0 text-light mt-3">

                                          Los turnos son de 8 personas cada media hora. El tiempo máximo de recorrido es de 2 horas.

                                    </p>


                         </div>

                   </div>

                   <div class="col-lg-3 col-md-3 text-center">

                  <div class="service-box mt-3 mx-auto sr-icon-1">

                        <span class="fa-stack fa-1x center">

                              <i class="fas fa-circle fa-stack-2x white"></i>

                              <i class="fas fa-ticket-alt fa-stack-1x custom-item-color"></i>

                        </span>

                        <p class="contact-heading-text mb-0 text-light text-up mt-3">Las entradas no tienen costo, solo pedimos inscripción previa.</p>

                  </div>

            </div>


                   <div class="col-lg-3 col-md-3 text-center">

                        <div class="service-box mt-3 mx-auto sr-icon-1">

                              <span class="fa-stack fa-1x center">

                                    <i class="fas fa-circle fa-stack-2x white"></i>

                                    <i class="fas fa-users fa-stack-1x custom-item-color"></i>

                              </span>

                              <p class="contact-heading-text mb-0 text-light text-up mt-3">

                                   No pueden venir más de 4 familiares juntos. Solicitamos que se inscriban individualmente.

                         </p>

                   </div><br>

             </div>

             
            <div class="col-lg-3 col-md-3 text-center">

                  <div class="service-box mt-3 mx-auto sr-icon-1">

                        <span class="fa-stack fa-1x center">

                              <i class="fas fa-circle fa-stack-2x white"></i>

                              <i class="fas fa-shopping-bag fa-stack-1x custom-item-color"></i>

                        </span>

                        <p class="contact-heading-text mb-0 text-light text-up mt-3">No hay guardarropa disponible, venir con bolsos pequeños y livianos.</p>

                  </div>

            </div>

      </div>

      <div class='go-to-content'>

                  <a role="button" class="hvr-hang mt-5 fas fa-2x fa-arrow-circle-down js-scroll-trigger"></a>

            </div>

</div>


</div>

</header>