<?php

$comunicado = 'private/img/templates/comunicado-template/comunicado.png';

$comunicadoRetina = 'private/img/templates/comunicado-template/comunicado@2x.png';

?>

<div class="alert-container d-none">

      <div class="alert-content mt-4 

      offset-2 col-8

      offset-lg-4 col-lg-4 pl-0 pr-0">

      <img class="screen-alert" 

      src="<?php echo $comunicado;?>" 

      srcset ="<?php echo $comunicadoRetina;?>" 

      alt = 'Comunicado del museo'

      title = 'Comunicado del museo'>

      <div class="close-alert float-right pt-2 pb-2 pr-3 pl-3">

            <i class="far fa-times" aria-hidden="true"></i>

      </div>


      <h1 class="alert-title text-center text-uppercase text-light mt-5 pl-2 pr-2 pl-lg-5 pr-lg-5">...</h1>

      <p class="alert-text text-center text-uppercase text-light mt-4 ml-3 mr-3">...</p>

      <div class="text-center mt-4 mb-4"> 

            <button type="button" class="hvr-sweep-to-right btn btn-sm btn-square alert-button">

            LEER COMUNICADO</button>

      </div>


</div>

</div>