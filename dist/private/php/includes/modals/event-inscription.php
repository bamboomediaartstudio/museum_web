<?php
?>

<div class="inscription-modal modal fade" tabindex="-1" role="dialog">

	<div class="modal-dialog modal-lg" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<h5 class="modal-title">INSCRIPCIÓN A EVENTO</h5>

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">

					<span aria-hidden="true">&times;</span>

				</button>

			</div>
			
			<div class="modal-body">

				<p>Formulario de preinscripción al evento <strong><?php echo $itemResult->name;?></strong>.</p>

				<form role="form" id="inscription-form" method="post">
					
					<input type="hidden" id="event-id" name="event-id" value="<?php echo $itemResult->mid;?>">

					<input type="hidden" id="event-name" name="event-name" value="<?php echo $itemResult->name;?>">
					
					<input type="hidden" id="event-free" name="event-free" value="<?php echo $itemResult->is_free;?>">

					<input type="hidden" id="event-url" name="event-url" value="<?php echo $itemResult->url;?>">

					<div class = "row">

						<!-- nombre -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="inscription-name">NOMBRE:</label>
							
							<input type="text" class="form-control" name="inscription-name" id="inscription-name" placeholder="tu nombre">

						</div>

						<!-- apellido -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="inscription-surname">APELLIDO:</label>
							
							<input type="text" name="inscription-surname" class="form-control" id="inscription-surname" placeholder="tu apellido">

						</div>

					</div>

					<div class = "row">

						<!-- email -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="inscription-email">EMAIL:</label>
							
							<input name="email" type="email" class="form-control" id="inscription-email" placeholder="tu email">

							<small id="emailHelp" class="form-text text-muted">No compartiremos esta información con nadie más.</small>
						</div>

						<!-- phone -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="inscription-phone">TELÉFONO:</label>
							
							<input type="number" name="phone" class="form-control" id="inscription-phone" placeholder="tu teléfono">

							<small id="emailHelp" class="form-text text-muted">No compartiremos esta información con nadie máUss.</small>
						</div>

					</div>

					<div class = "row">

						<!-- localidad -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="inscription-city">LOCALIDAD:</label>
							
							<input name="city" type="text" class="form-control" id="inscription-city" placeholder="tu localidad">
							
						</div>

						<!-- address -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="inscription-address">DIRECCIÓN:</label>
							
							<input name="address" type="text" class="form-control" id="inscription-address" placeholder="tu dirección">

						</div>

					</div>

					<p>¿Cómo te enteraste del evento?</p>

					<div class="form-group">

						<div class="custom-control custom-radio custom-control-inline">
							
							<input type="radio" id="newsletter" name="mediumRadioOptions" class="custom-control-input" value="newsletter">
							
							<label class="custom-control-label" for="newsletter">Newsletter</label>
						
						</div>

						<div class="custom-control custom-radio custom-control-inline">
							
							<input type="radio" id="web" name="mediumRadioOptions" class="custom-control-input" value="web">
							
							<label class="custom-control-label" for="web">web</label>
						
						</div>

						<div class="custom-control custom-radio custom-control-inline">
							
							<input type="radio" id="Facebook" name="mediumRadioOptions" class="custom-control-input" value="Facebook">
							
							<label class="custom-control-label" for="Facebook">Facebook</label>
						
						</div>

						<div class="custom-control custom-radio custom-control-inline">
							
							<input type="radio" id="Twitter" name="mediumRadioOptions" class="custom-control-input" value="Twitter">
							
							<label class="custom-control-label" for="Twitter">Twitter</label>
						
						</div>

						<div class="custom-control custom-radio custom-control-inline">
							
							<input type="radio" id="friend" name="mediumRadioOptions" class="custom-control-input" value="Amigo / Conocido">
							
							<label class="custom-control-label" for="friend">Amigo / Conocido</label>
						
						</div>

						<div class="custom-control custom-radio custom-control-inline">
							
							<input type="radio" id="teacher" name="mediumRadioOptions" class="custom-control-input" value="Profesor">
							
							<label class="custom-control-label" for="teacher">Profesor</label>
						
						</div>

						<div class="custom-control custom-radio custom-control-inline">
							
							<input type="radio" id="new" name="mediumRadioOptions" class="custom-control-input" value="Noticias">
							
							<label class="custom-control-label" for="new">Noticias</label>
						
						</div>

					</div>

					<p>¿Es la primera vez que te anotás en un evento del Museo?</p>

					<div class="form-group">

						<div class="custom-control custom-radio custom-control-inline">
							
							<input type="radio" id="si" name="firstTimeRadioOptions" class="custom-control-input" value="si">
							
							<label class="custom-control-label" for="si">Si</label>
						
						</div>

						<div class="custom-control custom-radio custom-control-inline">
							
							<input type="radio" id="no" name="firstTimeRadioOptions" class="custom-control-input" value="no">
							
							<label class="custom-control-label" for="no">No</label>
						
						</div>

					</div>

					<?php if($itemResult->is_free == 0){?>

					<p>Forma de pago (no tenes que pagar ahora: esto es simplemente una preinscripción. Nos pondremos en contacto para cancelar el pago.)</p>

					<div class="form-group">

						<div class="custom-control custom-radio">
							
							<input type="radio" id="option-1" name="paymentRadioOptions" class="custom-control-input" value="Pago en la institución con cualquier medio de pago">
							
							<label class="custom-control-label" for="option-1">Pago en la institución con cualquier medio de pago</label>
						
						</div>

						<div class="custom-control custom-radio">
							
							<input type="radio" id="option-2" name="paymentRadioOptions" class="custom-control-input" value="Con tarjeta de crédito">
							
							<label class="custom-control-label" for="option-2">Con tarjeta de crédito</label>
						
						</div>

						<div class="custom-control custom-radio">
							
							<input type="radio" id="option-3" name="paymentRadioOptions" class="custom-control-input" value="Tranferencia bancaria">
							
							<label class="custom-control-label" for="option-3">Tranferencia bancaria</label>
						
						</div>

						<div class="payment-error"></div>


					</div>

					<?php  }?>
					
				</form>

			</div>
			
			<div class="modal-footer">

				<button form="inscription-form" type="submit" class="hvr-sweep-to-right btn-square btn btn-primary">ENVIAR</button>

				<button type="button" class="hvr-sweep-to-right btn-square btn btn-primary" data-dismiss="modal">CERRAR</button>

			</div>

		</div>

	</div>

</div>