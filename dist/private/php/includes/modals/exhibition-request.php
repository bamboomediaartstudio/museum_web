<?php
?>

<div class="request-modal modal fade" tabindex="-1" role="dialog">

	<div class="modal-dialog modal-lg" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<h5 class="modal-title">SOLICITUD DE MUESTRA</h5>

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">

					<span aria-hidden="true">&times;</span>

				</button>

			</div>
			
			<div class="modal-body">

				<p>Si pertenecés o representás a un colegio, universidad o institución educativa, podés solicitar la muestra  <strong><?php echo $itemResult->name;?></strong>.<br><br>Completá el siguiente formulario y a la brevedad nos pondremos en contacto.	</p>

				<form role="form" id="request-form" method="post">
					
					<input type="hidden" id="exhibition-id" name="exhibition-id" value="<?php echo $itemResult->mid;?>">

					<input type="hidden" id="exhibition-name" name="exhibition-name" value="<?php echo $itemResult->name;?>">
					
					<input type="hidden" id="exhibition-url" name="exhibition-url" value="<?php echo $itemResult->url;?>">

					<div class = "row">

						<!-- nombre -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="inscription-name">TU NOMBRE:</label>
							
							<input type="text" class="form-control" name="inscription-name" id="inscription-name" placeholder="tu nombre">

						</div>

						<!-- apellido -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="inscription-surname">TU APELLIDO:</label>
							
							<input type="text" name="inscription-surname" class="form-control" id="inscription-surname" placeholder="tu apellido">

						</div>

					</div>

					<div class = "row">

						<!-- email -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="inscription-email">TU EMAIL:</label>
							
							<input name="email" type="email" class="form-control" id="inscription-email" placeholder="tu email">

							<small id="emailHelp" class="form-text text-muted">No compartiremos esta información con nadie más.</small>
						</div>

						<!-- phone -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="inscription-phone">TU TELÉFONO:</label>
							
							<input type="number" name="phone" class="form-control" id="inscription-phone" placeholder="tu teléfono">

							<small id="emailHelp" class="form-text text-muted">No compartiremos esta información con nadie máUss.</small>
						</div>

					</div>

					<div class = "row">

						<div class="form-group col-6">

							<label class="text-uppercase text-medium" for="institution-type">TIPO DE INSTITUCIÓN:</label>
							
							<select class="form-control required" id="institution-type" name="institution-type" >

								<option value="">Seleccioná una opción</option> 

								<option value="Colegio público / privado">Colegio público / privado</option>

								<option value="Universidad pública / privada">Universidad pública / privada</option>

								<option value="Instituto profesional">Instituto profesional</option>

								<option value="Otro tipo de institución educativa">Otro tipo de institución educativa</option>

								<option value="ONG">ONG</option>

								<option value="otro">otro</option>

							</select>

						</div>

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="institution-name">NOMBRE DE LA INSTITUCIÓN:</label>
							
							
							<input name="institution-name" type="text" class="form-control" id="institution-name" placeholder="nombre de la institución">

						</div>

						

					</div>

					<div class = "row">

						<!-- localidad -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="institution-city">LOCALIDAD:</label>
							
							<input name="institution-city" type="text" class="form-control" id="institution-city" placeholder="¿dónde queda la institución?">
							
						</div>

						<!-- address -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="institution-address">DIRECCIÓN:</label>
							
							<input name="institution-address" type="text" class="form-control" id="institution-address" placeholder="Dirección de la institución">

						</div>

					</div>

					<div class = "row">

						<!-- localidad -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="institution-city">URL:</label>
							
							<input name="institution-url" type="text" class="form-control" id="institution-url" placeholder="URL de la institución">
							
						</div>						

					</div>

				</form>

			</div>
			
			<div class="modal-footer">

				<button form="request-form" type="submit" class="hvr-sweep-to-right btn-square btn btn-primary">ENVIAR</button>

				<button type="button" class="hvr-sweep-to-right btn-square btn btn-primary" data-dismiss="modal">CERRAR</button>

			</div>

		</div>

	</div>

</div>