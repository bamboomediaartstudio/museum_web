  <?php
  ?>

  <div class="modal fade" id="group-reservation" role="dialog">

    <div class="modal-dialog">

      <div class="modal-content">

        <div class="modal-header">

          <h4 class="modal-title">VISITAS GRUPALES</h4>

          <button type="button" class="close" data-dismiss="modal">&times;</button>

        </div>

        <div class="modal-body">

          <h4 class="modal-title">IMPORTANTE</h4>

          <p>Si vas a registrar un grupo menor a diez personas, por favor contactate a <a href="mailto:visitasguiadas@museodelholocausto.org.ar">visitasguiadas@museodelholocausto.org.ar</a>

          <h4 class="modal-title">SOBRE LAS VISITAS</h4>


          <p>Las visitas guiadas están destinadas a escuelas <strong>primarias</strong>, <strong>medias</strong>, <strong>públicas</strong>, <strong>privadas</strong>, <strong>universidades</strong>, <strong>terciarios</strong> y <strong>grupos de educación no formal</strong>.<br><br>El objetivo es poder acercar a los alumnos a la temática del Holocausto, despertar su interés y concientizarlos acerca de las consecuencias del racismo y la discriminación.<br><br>Con el acompañamiento de una guía especializada, los estudiantes recorrerán la muestra temporaria del Museo del Holocausto y participarán de un espacio de reflexiones y preguntas.<br><br>La visita no tiene cargo. La reserva de turno no implica ningún compromiso por parte del Museo del Holocausto.
          </p>

          <div class="row mt-4">

            <div class="col-12">

              <form role="form" id="group-inscription-form" method="post">

                <input class="booking-id" name="booking-id" type="hidden" value="">

                <h4 class="modal-title">DATOS DE LA INSTITUCIÓN</h4>

                <p>Para una mejor atención, te pedimos que completes todos los campos del formulario.</p>

                <div class="form-group">

                  <label class="text-uppercase text-medium" for="inscription-institution-name">NOMBRE DE LA INSTITUCIÓN:</label>

                  <input type="text" class="form-control" name="inscription-institution-name" id="inscription-institution-name" placeholder="">

                </div>

                <div class="form-group">

                  <label class="text-uppercase text-medium" for="inscription-institution-cuit">CUIT:</label>

                  <input type="number" class="form-control" name="inscription-institution-cuit" id="inscription-institution-cuit" placeholder="Ingresá solo números, sin ningún signo.">

                </div>

                <div class="form-group">

                  <label class="text-uppercase text-medium" for="iva">Condición frente al IVA</label>

                  <select class="form-control" id="iva" name="iva">

                    <option value="consumidor final">Consumidor final</option>

                    <option value="exento">Exento</option>

                    <option value="responsable monotributo">Responsable monotributo</option>

                    <option value="no respsonsable">No respsonsable</option>

                    <option value="cliente del exterior">Cliente del exterior</option>

                    <option value="proveedor del exterior">Proveedor del exterior</option>

                    <option value="pesponsable inscripto">Responsable inscripto</option>

                    <option value="sujeto no categorizado">Sujeto no categorizado</option>

                    <option value="otro">Otro</option>

                  </select>

                </div>

                <div class="form-group">

                  <label class="text-uppercase text-medium" for="inscription-institution-address">DIRECCIÓN:</label>

                  <input type="text" class="form-control" name="inscription-institution-address" id="inscription-institution-address" placeholder="">

                </div>

                <div class="form-group">

                  <label class="text-uppercase text-medium" for="inscription-institution-phone">TELÉFONO:</label>

                  <input type="number" class="form-control" name="inscription-institution-phone" id="inscription-institution-phone" placeholder="Ingresá solamente números.">

                </div>

                <div class="form-group">

                  <label class="text-uppercase text-medium" for="inscription-institution-email">EMAIL:</label>

                  <input type="text" class="form-control" name="inscription-institution-email" id="inscription-institution-email" placeholder="">

                </div>

                <div class="form-group">

                  <label class="text-uppercase text-medium" for="inscription-institution-state">Provincia</label>

                  <select class="form-control" id="inscription-institution-state" name="inscription-institution-state">

                    <option value="Buenos Aires">Buenos Aires</option>

                    <option value="Catamarca">Catamarca</option>

                    <option value="Chaco">Chaco</option>

                    <option value="Chubut">Chubut</option>

                    <option value="Cordoba">Cordoba</option>

                    <option value="Corrientes">Corrientes</option>

                    <option value="Entre Rios">Entre Rios</option>

                    <option value="Formosa">Formosa</option>

                    <option value="Jujuy">Jujuy</option>

                    <option value="La Pampa">La Pampa</option>

                    <option value="La Rioja">La Rioja</option>

                    <option value="Mendoza">Mendoza</option>

                    <option value="Misiones">Misiones</option>

                    <option value="Neuquen">Neuquen</option>

                    <option value="Rio Negro">Rio Negro</option>

                    <option value="Salta">Salta</option>

                    <option value="San Juan">San Juan</option>

                    <option value="San Luis">San Luis</option>

                    <option value="Santa Cruz">Santa Cruz</option>

                    <option value="Santa Fe">Santa Fe</option>

                    <option value="Sgo. del Estero">Sgo. del Estero</option>

                    <option value="Tierra del Fuego">Tierra del Fuego</option>

                    <option value="Tucuman">Tucuman</option>

                  </select>

                </div>

                <div class="form-group">

                  <label class="text-uppercase text-medium" for="inscription-institution-city">LOCALIDAD:</label>

                  <input type="text" class="form-control" name="inscription-institution-city" id="inscription-institution-city" placeholder="">

                </div>

                <fieldset>

                  <legend>Define el nivel escolar</legend>

                  <div class="form-group">

                   <p><strong>Nivel escolar:</strong></p>

                   <div class="custom-control custom-radio">

                    <input type="radio" id="primario" name="education-level" class="custom-control-input" value="primario">

                    <label class="custom-control-label" for="primario">Primario</label>

                  </div>

                  <div class="custom-control custom-radio">

                    <input type="radio" id="secundario" name="education-level" class="custom-control-input" value="secundario">

                    <label class="custom-control-label" for="secundario">Secundario</label>

                  </div>

                  <div class="custom-control custom-radio">

                    <input type="radio" id="terciario" name="education-level" class="custom-control-input" value="terciario">

                    <label class="custom-control-label" for="terciario">Terciario</label>

                  </div>

                  <div class="custom-control custom-radio">

                    <input type="radio" id="universitario" name="education-level" class="custom-control-input" value="universitario">

                    <label class="custom-control-label" for="universitario">Universitario</label>

                  </div>

                  <div class="custom-control custom-radio">

                    <input type="radio" id="educacion-no-formal" name="education-level" class="custom-control-input" value="educacion-no-formal">

                    <label class="custom-control-label" for="educacion-no-formal">Educación No Formal</label>

                  </div>

                  <div class="education-level-error"></div>


                </div>

              </fieldset>

              <fieldset>

                  <legend>Define el grado / año</legend>

                  <div class="form-group">

                    <label class="text-uppercase text-medium" for="education-grade">Indicar Grado o Año</label>

                    <select class="form-control" id="education-grade" name="education-grade">

                      <option value="0">Seleccionar grado / año</option>

                      <option value="1">1</option>

                      <option value="2">2</option>

                      <option value="3">3</option>

                      <option value="4">4</option>

                      <option value="5">5</option>

                      <option value="6">6</option>

                    </select>

                  </div>

            </fieldset>






              <fieldset>

                <legend>Define el carácter de la institución</legend>
                
                <div class="form-group">

                  <p><strong>Carácter de la institución:</strong></p>

                  <div class="custom-control custom-radio">

                    <input type="radio" id="publica" name="institution-type" class="custom-control-input" value="publica">

                    <label class="custom-control-label" for="publica">Pública</label>

                  </div>

                  <div class="custom-control custom-radio">

                    <input type="radio" id="privada" name="institution-type" class="custom-control-input" value="privada">

                    <label class="custom-control-label" for="privada">Privada</label>

                  </div>

                  <div class="institution-type-error"></div>

                </div>

              </fieldset>

              <fieldset>

                <legend>Preguntar por personas con discapacidad</legend>

                <div class="form-group">

                  <p><strong>¿Hay alguna persona con alguna discapacidad?:</strong></p>

                  <div class="custom-control custom-radio">

                    <input type="radio" id="si" name="special-need" class="custom-control-input" value="si">

                    <label class="custom-control-label" for="si">si</label>

                  </div>

                  <div class="custom-control custom-radio">

                    <input type="radio" id="no" name="special-need" class="custom-control-input" value="no">

                    <label class="custom-control-label" for="no">No</label>

                  </div>

                  <div class="special-need-error"></div>

                </div>   

              </fieldset>             

              <h4 class="modal-title">PERSONA DE CONTACTO</h4>

              <p>Esta será la persona con la que el museo se pondrá en contacto para llevar a cabo la coordinación y gestionar todo lo relativo a la visita.</p>

              <div class="form-group">

                <label class="text-uppercase text-medium" for="inscription-contact-name">NOMBRE:</label>

                <input type="text" class="form-control" name="inscription-contact-name" id="inscription-contact-name" placeholder="">

              </div>

              <div class="form-group">

                <label class="text-uppercase text-medium" for="inscription-contact-email">EMAIL:</label>

                <input type="text" class="form-control" name="inscription-contact-email" id="inscription-contact-email" placeholder="">

              </div>

              <div class="form-group">

                <label class="text-uppercase text-medium" for="inscription-contact-phone">TELÉFONO:</label>

                <input type="number" class="form-control" name="inscription-contact-phone" id="inscription-contact-phone" placeholder="Ingresá solamente números">

              </div>

              <div class="form-group">

                <p><strong>¿Es la primera vez que visitas la institución?</strong></p>

                <div class="custom-control custom-radio">

                  <input type="radio" id="ft-si" name="first-time" class="custom-control-input" value="si">

                  <label class="custom-control-label" for="ft-si">si</label>

                </div>

                <div class="custom-control custom-radio">

                  <input type="radio" id="ft-no" name="first-time" class="custom-control-input" value="no">

                  <label class="custom-control-label" for="ft-no">no</label>

                </div>

                <div class="first-time-error"></div>

              </div>

              <div class="form-group">

                <p><strong>¿Es la primera vez que la institución donde trabajas visita el Museo?</strong></p>

                <div class="custom-control custom-radio">

                  <input type="radio" id="institution-first-time-si" name="institution-first-time" class="custom-control-input" value="si">

                  <label class="custom-control-label" for="institution-first-time-si">si</label>

                </div>

                <div class="custom-control custom-radio">

                  <input type="radio" id="institution-first-time-no" name="institution-first-time" class="custom-control-input" value="no">

                  <label class="custom-control-label" for="institution-first-time-no">no</label>

                </div>

                <div class="institution-first-time-error"></div>

              </div>

              <h4 class="modal-title">SELECCIONÁ LA CANTIDAD DE ALUMNOS</h4>

              <p>Para brindarte una mejor atención, necesitamos saber cuantos alumnos vendrán a la muestra. Cada turno puede aceptar hasta 25 personas, y pueden concurrir dos turnos en simultáneo, es decir, 50 personas por franja horaria. En función de la cantidad de alumnos, estimaremos la cantidad de guías necesarios.</p>

              <div class="form-group">

                <label class="text-uppercase text-medium" for="students-selector">Cantidad de alumnos</label>

                <select class="form-control" id="students-selector" name="students-selector">
                  <option value="0">Elegir Cantidad</option>
                  <?php for($i=10; $i<=150;$i++){ ?>

                    <option value="<?php echo $i;?>"><?php echo $i;?></option>

                  <?php } ?>

                </select>

              </div>

              <div class="previous-calendar">

                <h4 class="modal-title">SELECCIONÁ UN DÍA</h4>

                <p class="mt-2 mb-5">Los días marcados en azul son los que tienen turnos. Seleccioná primero el día y luego el turno.<br><br>
                </p>

              </div>

            </div>

            <div class="calendar-container">

              <div class="col-12">

                <div id="m_calendar_1"></div>

              </div>

            </div>


            <div class="container days-checker"></div>

            <div class="container events-by-day">

            </div>

          </div>

        </div>

        <div class="modal-footer">

          <button form="group-inscription-form" id="send-group-booking" type="submit" class="generic-action-button btn modal-btn">Reservar Turno/s</button>

          <button type="button" class="generic-action-button btn modal-btn" data-dismiss="modal">Cerrar</button>

        </div>

      </form>


    </div>

  </div>

</div>