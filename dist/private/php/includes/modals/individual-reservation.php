<?php

$availableIndividualCounter = DB::getInstance()->query('SELECT * FROM museum_booking WHERE sid = ? AND booked_places != available_places AND date_start > NOW()', [2]);

$individualCount = $availableIndividualCounter->count();

$availableGroupCounter = DB::getInstance()->query('SELECT * FROM museum_booking WHERE sid = ? AND booked_places != available_places AND date_start > NOW()', [1]);

$groupCount = $availableIndividualCounter->count();
?>

<input type="hidden" id="individualCount" name="individualCount" value="<?php echo $individualCount;?>">

<input type="hidden" id="groupCount" name="groupCount" value="<?php echo $groupCount;?>">

<input type="hidden" id="origin" name="origin" value="<?php echo $origin;?>">

<div class="modal fade" id="individual-reservation" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <h4 class="modal-title">VISITAS INDIVIDUALES</h4>

        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>

      <div class="modal-body">

        <?php if ($origin != 'admin'){?>

          <!--<p>Para poder reservar tu turno a la <strong>"exhibición permanente"</strong>, completá el siguiente formulario.<br><br>El valor de la visita es de $100 y se debe abonar en el Museo.<br><br>No te olvides descargar la app del Museo Del Holocausto y traer tus auriculares: Si tenés Android <a style="color:blue;text-align:center;" href="https://play.google.com/store/apps/details?id=air.air.shoaMuseum.app" target="_blank">descargala aquí</a> y si tenés IOS  <a style="color:blue;text-align:center;" href="https://apps.apple.com/ar/app/museo-del-holocausto/id1488272387?l=en" target="_blank">descargala aquí</a>.</p>-->

          <p>Para poder reservar tu turno a la <strong>"exhibición permanente"</strong>, completá el siguiente formulario.<br><br>Por favor, revisá el protocolo y tené presente todas las medidas de seguridad necesarias.</p>

        <?php }else{ ?>

          <p>Utilizá este formulario si querés dar inscribir a una persona para una visita guiada individual a la <strong>"exhibición permanente"</strong>.</p>

        <?php } ?>


        <div class="row">

          <div class="col-12">

            <form role="form" id="inscription-form" method="post">

              <input class="booking-id" name="booking-id" type="hidden" value="">

              <input id="tickets-per-person" name="tickets-per-person" type="hidden" value="1">

              <input id="h-place" name="h-place" type="hidden" value="">
              
              <input id="h-readable-start" name="h-readable-start" type="hidden" value="">

              <input id="h-readable-end" name="h-readable-end" type="hidden" value="">

              <div class="form-group">

                <label class="text-uppercase text-medium" for="inscription-name">NOMBRE / NAME:</label>

                <input type="text" class="form-control" name="inscription-name" id="inscription-name" placeholder="">

              </div>

              <div class="form-group">

                <label class="text-uppercase text-medium" for="inscription-surname">APELLIDO / SURNAME:</label>

                <input type="text" class="form-control" name="inscription-surname" id="inscription-surname" placeholder="">

              </div>

              <div class="form-group">

                <label class="text-uppercase text-medium" for="inscription-email">EMAIL:</label>

                <input type="text" class="form-control" name="inscription-email" id="inscription-email" placeholder="email">

              </div>

              <div class="form-group">

                <label class="text-uppercase text-medium" for="inscription-phone">TELÉFONO / PHONE:</label>

                <input type="number" class="form-control" name="inscription-phone" id="inscription-phone" placeholder="Utilizar solamente números">

              </div>

              <div class="form-group">

                <label class="text-uppercase text-medium" for="inscription-dni">DNI O PASAPORTE / PASSPORT :</label>

                <input type="text" class="form-control" name="inscription-dni" id="inscription-dni" placeholder="No utilizar puntos ni espacios">

              </div>

              <fieldset>

                <legend>Indica el motivo de la visita</legend>

                <p><strong>¿Cuál es el motivo de la visita?</strong></p>

                <div class="form-group">

                  <div class="custom-control custom-radio">

                    <input type="radio" id="turismo" name="visit-reason" class="custom-control-input" value="turismo">

                    <label class="custom-control-label" for="turismo">Turismo</label>

                  </div>

                  <div class="custom-control custom-radio">

                    <input type="radio" id="investigacion" name="visit-reason" class="custom-control-input" value="investigacion">

                    <label class="custom-control-label" for="investigacion">Investigación o estudios en la temática</label>

                  </div>

                  <div class="custom-control custom-radio">

                    <input type="radio" id="interes" name="visit-reason" class="custom-control-input" value="interes">

                    <label class="custom-control-label" for="interes">Interés en el tema</label>

                  </div>

                  <div class="custom-control custom-radio">

                    <input type="radio" id="otra" name="visit-reason" class="custom-control-input" value="otra">

                    <label class="custom-control-label" for="otra">otra</label>

                  </div>

                  <div class="visit-reason-error"></div>

                </div>

              </fieldset>

              <fieldset>

                <legend>Indicar como se enteró del museo</legend>

                <p><strong>¿Cómo conociste el Museo del Holocausto?</strong></p>

                <div class="form-group">

                  <div class="custom-control custom-radio">

                    <input type="radio" id="s-newsletter" name="about-museum" class="custom-control-input" value="s-newsletter">

                    <label class="custom-control-label" for="s-newsletter">Newsletter</label>

                  </div>

                  <div class="custom-control custom-radio ">

                    <input type="radio" id="web" name="about-museum" class="custom-control-input" value="web">

                    <label class="custom-control-label" for="web">web</label>

                  </div>

                  <div class="custom-control custom-radio ">

                    <input type="radio" id="Facebook" name="about-museum" class="custom-control-input" value="Facebook">

                    <label class="custom-control-label" for="Facebook">Facebook</label>

                  </div>

                  <div class="custom-control custom-radio ">

                    <input type="radio" id="Twitter" name="about-museum" class="custom-control-input" value="Twitter">

                    <label class="custom-control-label" for="Twitter">Twitter</label>

                  </div>

                  <div class="custom-control custom-radio ">

                    <input type="radio" id="friend" name="about-museum" class="custom-control-input" value="Amigo / Conocido">

                    <label class="custom-control-label" for="friend">Amigo / Conocido</label>

                  </div>

                  <div class="custom-control custom-radio ">

                    <input type="radio" id="teacher" name="about-museum" class="custom-control-input" value="Profesor">

                    <label class="custom-control-label" for="teacher">Profesor</label>

                  </div>

                  <div class="custom-control custom-radio ">

                    <input type="radio" id="new" name="about-museum" class="custom-control-input" value="Noticias">

                    <label class="custom-control-label" for="new">Noticias</label>

                  </div>

                  <div class="about-museum-error"></div>


                </div>

              </fieldset>

              <h4 class="modal-title">SELECCIONÁ UN DÍA</h4>

              <?php if ($origin != 'admin'){?>

                <p class="mt-2 mb-5">Elegí del calendario una de las fechas dispoinibles. Luego podrás seleccionar el turno que mejor se acomode a tu agenda y a tus tiempos.</p>

              <?php }else{ ?>

                <p class="mt-2 mb-5">Elegí del calendario una de las fechas dispoinibles para el visitante.</p>

              <?php } ?>


            </div>

            <div class="calendar-container">

              <div class="col-12">

                <div id="m_calendar_2"></div>

              </div>

            </div>


            <div class="container events-by-day">



            </div>

          </div>

        </div>

        <div class="modal-footer">

          <button form="inscription-form" id="send-individual-booking" type="submit" class="generic-action-button btn modal-btn">Reservar turno</button>

          <button type="button" class="generic-action-button btn modal-btn" data-dismiss="modal">Cerrar</button>

        </div>

      </form>


    </div>

  </div>

</div>