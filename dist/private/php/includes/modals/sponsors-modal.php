<?php
?>

<div class="sponsors-modal modal fade" tabindex="-1" role="dialog">

	<div class="modal-dialog modal-lg" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<h5 class="modal-title">CONTACTANOS</h5>

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">

					<span aria-hidden="true">&times;</span>

				</button>

			</div>
			
			<div class="modal-body">

				<p>Completá este formulario si tenés interés en ser <strong>sponsor</strong>, <strong>donante</strong> o <strong>amigo del museo</strong>.</p>

				<form role="form" id="sponsors-form" method="post">

					<div class = "row">

						<!-- nombre -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="sponsor-modal-name">NOMBRE:</label>
							
							<input type="text" class="form-control" name="sponsor-modal-name" id="sponsor-modal-name" placeholder="tu nombre">

						</div>

						<!-- apellido -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="sponsor-modal-surname">APELLIDO:</label>
							
							<input type="text" name="sponsor-modal-surname" class="form-control" id="sponsor-modal-surname" placeholder="tu apellido">

						</div>

					</div>

					<div class = "row">

						<!-- email -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="sponsor-modal-email">EMAIL:</label>
							
							<input name="sponsor-modal-email" type="email" class="form-control" id="sponsor-modal-email" placeholder="tu email">

							<small id="emailHelp" class="form-text text-muted">No compartiremos esta información con nadie más.</small>
						</div>

						<!-- phone -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="sponsor-modal-phone">TELÉFONO:</label>
							
							<input type="number" name="sponsor-modal-phone" class="form-control" id="sponsor-modal-phone" placeholder="tu teléfono">

							<small id="phoneHelp" class="form-text text-muted">No compartiremos esta información con nadie máUss.</small>
						</div>

					</div>

					<div class = "row">

						<!-- localidad -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="sponsor-modal-city">LOCALIDAD:</label>
							
							<input name="sponsor-modal-city" type="text" class="form-control" id="sponsor-modal-city" placeholder="tu localidad">
							
						</div>

						<!-- address -->

						<div class="form-group col-6">
							
							<label class="text-uppercase text-medium" for="sponsor-modal-address">DIRECCIÓN:</label>
							
							<input name="sponsor-modal-address" type="text" class="form-control" id="sponsor-modal-address" placeholder="tu dirección">

						</div>

					</div>

					<p>Por favor, definí una categoría de tu interés:</p>

					<div class="form-group">

						<div class="custom-control custom-radio custom-control-inline">
							
							<input type="radio" id="sponsor" name="sponsor-type-option" class="custom-control-input" value="sponsor">
							
							<label class="custom-control-label" for="sponsor">Sponsor</label>
						
						</div>

						<div class="custom-control custom-radio custom-control-inline">
							
							<input type="radio" id="donante" name="sponsor-type-option" class="custom-control-input" value="donante">
							
							<label class="custom-control-label" for="donante">Donante</label>
						
						</div>

						<div class="custom-control custom-radio custom-control-inline">
							
							<input type="radio" id="amigo" name="sponsor-type-option" class="custom-control-input" value="amigo">
							
							<label class="custom-control-label" for="amigo">Amigo</label>
						
						</div>

					</div>

					<div class="type-error"></div>

				</form>

			</div>
			
			<div class="modal-footer">

				<button form="sponsors-form" type="submit" class="hvr-sweep-to-right btn-square btn btn-primary">ENVIAR</button>

				<button type="button" class="hvr-sweep-to-right btn-square btn btn-primary" data-dismiss="modal">CERRAR</button>	

			</div>

		</div>

	</div>

</div>