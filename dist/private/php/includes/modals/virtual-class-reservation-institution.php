<?php

$availableIndividualCounter = DB::getInstance()->query('SELECT * FROM museum_booking WHERE sid = ? AND booked_places != available_places AND date_start > NOW()', [2]);

$individualCount = $availableIndividualCounter->count();

$availableGroupCounter = DB::getInstance()->query('SELECT * FROM museum_booking WHERE sid = ? AND booked_places != available_places AND date_start > NOW()', [1]);

$groupCount = $availableIndividualCounter->count();
?>

<input type="hidden" id="origin" name="origin" value="<?php echo $origin;?>">

<div class="modal fade" id="virtual-class-reservation-institution" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <h4 class="modal-title">INSCRIPCIÓN PARA INSTITUCIONES</h4>

        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>

      <div class="modal-body">

        <p>Completá el siguiente formulario para inscribirte a la clase.</p>


        <h4 class="modal-title">DOCENTE A CARGO</h4>

        <p>Completá con los datos del docente a cargo.</p>


        <div class="row">

          <div class="col-12">

            <form role="form" id="virtual-class-institutions-inscription-form" method="post">

              <input class="booking-id" name="booking-id" type="hidden" value="">

              <input id="tickets-per-person" name="tickets-per-person" type="hidden" value="1">

              <input id="h-place" name="h-place" type="hidden" value="">

              <input id="h-readable-start" name="h-readable-start" type="hidden" value="">

              <input id="h-readable-end" name="h-readable-end" type="hidden" value="">

                <div class="form-group">

                  <label class="text-uppercase" for="institution-virtual-class-inscription-name"><span class="text-medium">NOMBRE Y APELLIDO DEL DOCENTE:</span></label>

                  <input type="text" class="form-control" name="institution-virtual-class-inscription-name" id="institution-virtual-class-inscription-name" placeholder="">

                </div>

                <!--<div class="form-group">

                  <label class="text-uppercase " for="institution-virtual-class-inscription-surname"><span class="text-medium">APELLIDO DEL DOCENTE:</span></label>

                  <input type="text" class="form-control" name="institution-virtual-class-inscription-surname" id="institution-virtual-class-inscription-surname" placeholder="">

                </div>-->

                <div class="form-group ">

                  <label class="text-uppercase text-medium" for="institution-virtual-class-inscription-email">EMAIL DE CONTACTO:</label>

                  <input type="text" class="form-control" name="institution-virtual-class-inscription-email" id="institution-virtual-class-inscription-email" placeholder="email">

                </div>

                <div class="form-group ">

                  <label class="text-uppercase text-medium" for="institution-virtual-class-inscription-email-repeat">REPETIR EMAIL:</label>

                  <input type="text" class="form-control" name="institution-virtual-class-inscription-email-repeat" id="institution-virtual-class-inscription-email-repeat" placeholder="repetir email">

                </div>

                <div class="form-group">

                  <label class="text-uppercase" for="institution-virtual-class-inscription-phone"><span class="text-medium">TELÉFONO DE CONTACTO:</span></label>

                  <input type="number" class="form-control" name="institution-virtual-class-inscription-phone" id="institution-virtual-class-inscription-phone" placeholder="Utilizar solamente números">

                </div>

              <h4 class="modal-title">DATOS DE LA INSTITUCIÓN</h4>

              <p>Completá con los datos de la institución.</p>

                <div class="form-group">

                  <label class="text-uppercase text-medium" for="institution-virtual-class-inscription-name-2">NOMBRE DE LA ESCUELA:</label>

                  <input type="text" class="form-control" name="institution-virtual-class-inscription-name-2" id="institution-virtual-class-inscription-name-2" placeholder="">

                </div>

                <div class="form-group">

                  <label class="text-uppercase" for="institution-virtual-class-inscription-address"><span class="text-medium">DIRECCIÓN DE LA ESCUELA:</span></label>

                  <input type="text" class="form-control" name="institution-virtual-class-inscription-address" id="institution-virtual-class-inscription-address" placeholder="">

                </div>

                <div class="form-group">

                  <label class="text-uppercase" for="institution-virtual-class-inscription-students"><span class="text-medium">CANTIDAD DE ALUMNOS:</span><br><small>(máximo: 95 alumnos. De ser más, solicitar otro turno)</small></label>

                  <input type="number"  class="form-control" name="institution-virtual-class-inscription-students" id="institution-virtual-class-inscription-students">

                  <small>Para una experiencia enriquecedora, sugerimos la inscripción de grupos de hasta 50 personas.</small>

                </div>

                <div class="form-group">

                  <label class="text-uppercase text-medium" for="institution-inscription-state">Provincia</label>

                  <select class="form-control" id="institution-inscription-state" name="institution-inscription-state">

                    <option value=""></option>

                    <option value="Buenos Aires">Buenos Aires</option>

                    <option value="Catamarca">Catamarca</option>

                    <option value="Chaco">Chaco</option>

                    <option value="Chubut">Chubut</option>

                    <option value="Cordoba">Cordoba</option>

                    <option value="Corrientes">Corrientes</option>

                    <option value="Entre Rios">Entre Rios</option>

                    <option value="Formosa">Formosa</option>

                    <option value="Jujuy">Jujuy</option>

                    <option value="La Pampa">La Pampa</option>

                    <option value="La Rioja">La Rioja</option>

                    <option value="Mendoza">Mendoza</option>

                    <option value="Misiones">Misiones</option>

                    <option value="Neuquen">Neuquen</option>

                    <option value="Rio Negro">Rio Negro</option>

                    <option value="Salta">Salta</option>

                    <option value="San Juan">San Juan</option>

                    <option value="San Luis">San Luis</option>

                    <option value="Santa Cruz">Santa Cruz</option>

                    <option value="Santa Fe">Santa Fe</option>

                    <option value="Sgo. del Estero">Sgo. del Estero</option>

                    <option value="Tierra del Fuego">Tierra del Fuego</option>

                    <option value="Tucuman">Tucuman</option>

                  </select>

                </div>

              <fieldset class="form-group">

                  <label class="text-uppercase text-medium" for="education-level">NIVEL ESCOLAR</label>

                  <div class="form-group">

                   <div class="custom-control custom-radio ">

                    <input type="radio" id="primario" name="education-level" class="custom-control-input" value="primario">

                    <label class="custom-control-label" for="primario">Primario</label>

                  </div>

                  <div class="custom-control custom-radio ">

                    <input type="radio" id="secundario" name="education-level" class="custom-control-input" value="secundario">

                    <label class="custom-control-label" for="secundario">Secundario</label>

                  </div>

                  <div class="custom-control custom-radio ">

                    <input type="radio" id="terciario" name="education-level" class="custom-control-input" value="terciario">

                    <label class="custom-control-label" for="terciario">Terciario</label>

                  </div>

                  <div class="custom-control custom-radio ">

                    <input type="radio" id="universitario" name="education-level" class="custom-control-input" value="universitario">

                    <label class="custom-control-label" for="universitario">Universitario</label>

                  </div>

                  <div class="custom-control custom-radio ">

                    <input type="radio" id="educacion-no-formal" name="education-level" class="custom-control-input" value="educacion-no-formal">

                    <label class="custom-control-label" for="educacion-no-formal">Educación No Formal</label>

                  </div>

                  <div class="education-level-error"></div>


                </div>

              </fieldset>

              <!-- Grado escolar -->

              <div class="form-group">

                <label class="text-uppercase text-medium" for="education-grade">Grado / Año</label>

                <select class="form-control" id="education-grade" name="education-grade">

                  <option value="0">Seleccionar grado / año</option>

                  <option value="1">1</option>

                  <option value="2">2</option>

                  <option value="3">3</option>

                  <option value="4">4</option>

                  <option value="5">5</option>

                  <option value="6">6</option>

                </select>

              </div>

              <!-- /.grado -->

              <fieldset>

                <label class="text-uppercase text-medium" for="institution-type">Carácter de la institución:</label>

                <div class="form-group">

                  <div class="custom-control custom-radio ">

                    <input type="radio" id="publica" name="institution-type" class="custom-control-input" value="publica">

                    <label class="custom-control-label" for="publica">Pública</label>

                  </div>

                  <div class="custom-control custom-radio ">

                    <input type="radio" id="privada" name="institution-type" class="custom-control-input" value="privada">

                    <label class="custom-control-label" for="privada">Privada</label>

                  </div>

                  <div class="institution-type-error"></div>

                </div>

              </fieldset>

              <!--<fieldset>

                <label class="text-uppercase text-medium" for="inscription-platform">¿Se usará la plataforma online del Museo o de la institución?</label>

                <div class="form-group">

                  <div class="custom-control custom-radio ">

                    <input type="radio" id="museum-platform" name="inscription-platform" class="custom-control-input" value="del museo">

                    <label class="custom-control-label" for="museum-platform">Plataforma del museo.</label>

                  </div>

                  <div class="custom-control custom-radio ">

                    <input type="radio" id="institution-platform" name="inscription-platform" class="custom-control-input" value="de la institución">

                    <label class="custom-control-label" for="institution-platform">Plataforma de la institución.</label>

                  </div>

                  <div class="institution-type-error"></div>

                </div>

              </fieldset>-->


              <h4 class="modal-title">SELECCIONÁ UN DÍA</h4>

              <p class="mt-2 mb-5">Elegí del calendario una de las fechas dispoinibles. Luego podrás seleccionar el turno que mejor se acomode a tu agenda y a tus tiempos.</p>

            </div>

            <div class="calendar-container">

              <div class="col-12">

                <div id="m_calendar_7"></div>

              </div>

            </div>

            <div class="container events-by-day">

            </div>

          </div>

        </div>

        <div class="modal-footer">

          <button form="virtual-class-institutions-inscription-form" id="send-individual-booking" type="submit" class="generic-action-button btn modal-btn">Reservar turno</button>

          <button type="button" class="generic-action-button btn modal-btn" data-dismiss="modal">Cerrar</button>

        </div>

      </form>


    </div>

  </div>

</div>
