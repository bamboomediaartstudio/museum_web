<?php

$availableIndividualCounter = DB::getInstance()->query('SELECT * FROM museum_booking WHERE sid = ? AND booked_places != available_places AND date_start > NOW()', [2]);

$individualCount = $availableIndividualCounter->count();

$availableGroupCounter = DB::getInstance()->query('SELECT * FROM museum_booking WHERE sid = ? AND booked_places != available_places AND date_start > NOW()', [1]);

$groupCount = $availableIndividualCounter->count();
?>

<input type="hidden" id="origin" name="origin" value="<?php echo $origin;?>">

<div class="modal fade" id="virtual-class-reservation" role="dialog">

  <div class="modal-dialog modal-lg">

    <div class="modal-content">

      <div class="modal-header">

        <h4 class="modal-title">INSCRIBITE A LA CLASE VIRTUAL</h4>

        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>

      <div class="modal-body">

         <p>Dejanos tus datos e inscribite a la clase virtual. Al Seleccionar un día del calendario vas a poder ver los distintos horarios disponibles.</p>

    
        <div class="row">

          <div class="col-12">

            <form role="form" id="virtual-class-inscription-form" method="post">

              <input class="booking-id" name="booking-id" type="hidden" value="">

              <input id="tickets-per-person" name="tickets-per-person" type="hidden" value="1">

              <input id="h-place" name="h-place" type="hidden" value="">
              
              <input id="h-readable-start" name="h-readable-start" type="hidden" value="">

              <input id="h-readable-end" name="h-readable-end" type="hidden" value="">

              <div class = "row">

                <div class="form-group col-md-6">

                  <label class="text-uppercase" for="virtual-class-inscription-name"><span class="text-medium">NOMBRE</span> / NAME:</label>

                  <input type="text" class="form-control" name="virtual-class-inscription-name" id="virtual-class-inscription-name" placeholder="">

                </div>

                <div class="form-group col-md-6">

                  <label class="text-uppercase " for="virtual-class-inscription-surname"><span class="text-medium">APELLIDO</span> / SURNAME:</label>

                  <input type="text" class="form-control" name="virtual-class-inscription-surname" id="virtual-class-inscription-surname" placeholder="">

                </div>

              </div>

              <div class = "row">

                <div class="form-group col-md-6">

                  <label class="text-uppercase text-medium" for="virtual-class-inscription-email">EMAIL:</label>

                  <input type="text" class="form-control" name="virtual-class-inscription-email" id="virtual-class-inscription-email" placeholder="email">

                </div>

                <div class="form-group col-md-6">

                  <label class="text-uppercase" for="virtual-class-inscription-phone"><span class="text-medium">TELÉFONO</span> / PHONE:</label>

                  <input type="number" class="form-control" name="virtual-class-inscription-phone" id="virtual-class-inscription-phone" placeholder="Utilizar solamente números">

                </div>

              </div>

              <!--<div class="form-group">

                <label class="text-uppercase text-medium" for="virtual-class-inscription-dni">DNI O PASAPORTE / PASSPORT :</label>

                <input type="text" class="form-control" name="virtual-class-inscription-dni" id="virtual-class-inscription-dni" placeholder="No utilizar puntos ni espacios">

              </div>-->


              <h4 class="modal-title">SELECCIONÁ UN DÍA</h4>

              <p class="mt-2 mb-5">Elegí del calendario una de las fechas dispoinibles. Luego podrás seleccionar el turno que mejor se acomode a tu agenda y a tus tiempos.</p>

            </div>

            <div class="calendar-container">

              <div class="col-12">

                <div id="m_calendar_6"></div>

              </div>

            </div>

            <div class="container events-by-day">

            </div>

          </div>

        </div>

        <div class="modal-footer">

          <button form="virtual-class-inscription-form" id="send-individual-booking" type="submit" class="generic-action-button btn modal-btn">Reservar turno</button>

          <button type="button" class="generic-action-button btn modal-btn" data-dismiss="modal">Cerrar</button>

        </div>

      </form>


    </div>

  </div>

</div>