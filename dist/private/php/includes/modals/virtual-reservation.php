<?php

$availableIndividualCounter = DB::getInstance()->query('SELECT * FROM museum_booking WHERE sid = ? AND booked_places != available_places AND date_start > NOW()', [2]);

$individualCount = $availableIndividualCounter->count();

$availableGroupCounter = DB::getInstance()->query('SELECT * FROM museum_booking WHERE sid = ? AND booked_places != available_places AND date_start > NOW()', [1]);

$groupCount = $availableIndividualCounter->count();
?>

<input type="hidden" id="origin" name="origin" value="<?php echo $origin;?>">

<div class="modal fade" id="virtual-reservation" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <h4 class="modal-title">VISITAS VIRTUALES</h4>

        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>

      <div class="modal-body">

        <?php if ($origin != 'admin'){?>


         <p>Accedé a charlas y recorridos virtuales llevados a cabo por nuestros guías. Completá tus datos y seleccioná el día y el horario de los que se encuentren disponibles</p>

        <?php }else{ ?>

          <p>Utilizá este formulario si querés dar inscribir a una persona para una visita guiada virtual a la <strong>"exhibición permanente"</strong>.</p>

        <?php } ?>


        <div class="row">

          <div class="col-12">

            <form role="form" id="virtual-inscription-form" method="post">

              <input class="booking-id" name="booking-id" type="hidden" value="">

              <input id="tickets-per-person" name="tickets-per-person" type="hidden" value="1">

              <input id="h-place" name="h-place" type="hidden" value="">
              
              <input id="h-readable-start" name="h-readable-start" type="hidden" value="">

              <input id="h-readable-end" name="h-readable-end" type="hidden" value="">

              <div class="form-group">

                <label class="text-uppercase text-medium" for="virtual-inscription-name">NOMBRE / NAME:</label>

                <input type="text" class="form-control" name="virtual-inscription-name" id="virtual-inscription-name" placeholder="">

              </div>

              <div class="form-group">

                <label class="text-uppercase text-medium" for="virtual-inscription-surname">APELLIDO / SURNAME:</label>

                <input type="text" class="form-control" name="virtual-inscription-surname" id="virtual-inscription-surname" placeholder="">

              </div>

              <div class="form-group">

                <label class="text-uppercase text-medium" for="virtual-inscription-email">EMAIL:</label>

                <input type="text" class="form-control" name="virtual-inscription-email" id="virtual-inscription-email" placeholder="email">

              </div>

              <div class="form-group">

                <label class="text-uppercase text-medium" for="virtual-inscription-phone">TELÉFONO / PHONE:</label>

                <input type="number" class="form-control" name="virtual-inscription-phone" id="virtual-inscription-phone" placeholder="Utilizar solamente números">

              </div>

              <div class="form-group">

                <label class="text-uppercase text-medium" for="virtual-inscription-dni">DNI O PASAPORTE / PASSPORT :</label>

                <input type="text" class="form-control" name="virtual-inscription-dni" id="virtual-inscription-dni" placeholder="No utilizar puntos ni espacios">

              </div>


              <h4 class="modal-title">SELECCIONÁ UN DÍA</h4>

              <?php if ($origin != 'admin'){?>

                <p class="mt-2 mb-5">Elegí del calendario una de las fechas dispoinibles. Luego podrás seleccionar el turno que mejor se acomode a tu agenda y a tus tiempos.</p>

              <?php }else{ ?>

                <p class="mt-2 mb-5">Elegí del calendario una de las fechas dispoinibles para el visitante.</p>

              <?php } ?>


            </div>

            <div class="calendar-container">

              <div class="col-12">

                <div id="m_calendar_5"></div>

              </div>

            </div>


            <div class="container events-by-day">



            </div>

          </div>

        </div>

        <div class="modal-footer">

          <button form="virtual-inscription-form" id="send-individual-booking" type="submit" class="generic-action-button btn modal-btn">Reservar turno</button>

          <button type="button" class="generic-action-button btn modal-btn" data-dismiss="modal">Cerrar</button>

        </div>

      </form>


    </div>

  </div>

</div>