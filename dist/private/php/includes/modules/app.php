<?php

?>

<section id="app" class="app-content">

	<div class="container">

		<div class="row">

			<div class="col-lg-6 text-left">

				<h4 class="section-heading module-title text-medium">BAJATE LA APP. DEL MUSEO</h4>

				<hr class="left-separator">

				<p class="dark text-faded mb-5 text-left about-reflexiones module-text text-light">Encontrala en el PlayStore de Android o en el AppStore de Iphone. Vas a poder acceder a la audioguía, información complementaria de la exhibición permanente y contenido exclusivo.</p>

				<div class="app-ranking">

					<i class="fas fa-star"></i>
					
					<i class="fas fa-star"></i>
					
					<i class="fas fa-star"></i>

					<i class="fas fa-star"></i>

					<i class="fas fa-star"></i>

				</div>

				<div class="download-app row mb-5 mt-5">

					<div class="device android-device float-left ml-2">

						<a class="hvr-buzz-out" href="https://play.google.com/store/apps/details?id=air.air.shoaMuseum.app" target="_blank">

							<img class="img-responsive" src='private/img/devices/android.png' srcset="private/img/devices/android-2x.png 2x" alt="App Museo del Holocausto Android" width="180px">

						</a>

					</div>

					<div class="device ios-device float-left ml-lg-3">

						<a class="hvr-buzz-out" href="https://apps.apple.com/ar/app/museo-del-holocausto/id1488272387?l=en" target="_blank">

							<img class="img-responsive" src='private/img/devices/ios.png' srcset="private/img/devices/ios-2x.png 2x" alt="App Museo del Holocausto Android" width="162px">

						</a>

					</div>

				</div>

			</div>

			<div class="col-lg-6 col-12 d-none d-sm-block hvr-buzz-out app-phone">

				<img class="img-responsive d-block mx-auto" src='private/img/phone.png' alt="app museo" width="50%" style="float:none !important;">

			</div>


		</div>

	</div>

</section>