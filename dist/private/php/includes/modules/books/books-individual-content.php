<?php

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

$added = strftime("%A %d de %B del %Y", strtotime($itemResult->added));

$finalTime = 'Añadido el ' . utf8_encode($added);

$itemTitle = $itemResult->title;

$author = $itemResult->author;

$description = $itemResult->book_description;

$isbn = ($itemResult->isbn != '') ? $itemResult->isbn : ' - ';

$pages = ($itemResult->pages != 0) ? $itemResult->pages : ' - ';

$publisher = ($itemResult->publisher != '') ? $itemResult->publisher : ' - ';

$published = ($itemResult->published != '') ? $itemResult->published : ' - ';

$edition = ($itemResult->edition != '') ? $itemResult->edition : ' - ';

$language = ($itemResult->language != '') ? $itemResult->language : ' - ';

$website = ($itemResult->website != '') ? $itemResult->website : ' - ';

$price = ($itemResult->price != 0) ? $itemResult->price : ' - ';

$isOnSale = $itemResult->is_on_sale;

$shareContent = $itemResult->allow_share;

$videosQuery = $db->query('SELECT * FROM museum_youtube_videos WHERE sid = ? AND source = ? AND active = ? AND deleted = ?', [$itemResult->mid, 'book_video', 1, 0]);

$totalVideos = $videosQuery->count();
?>

<div class="container">

	<input type="hidden" id="total-videos" name="total-videos" value="<?php echo $totalVideos;?>">

	<div class="row">

		<div class="col-md-9 col-12">

			<div class="row mt-5">

				<div class="col-12 text-left">

					<h1 class="section-heading module-title text-uppercase"><?php echo $itemTitle;?></h1>

					<hr class="left-separator">


					

					<!--<hr class="left-separator">-->

					

				</div>

				<div class="col-12">

					<p class="dark text-left about-reflexiones module-text text-light"> <?php echo $description;?></p>

				</div>

			</div>

			<div class="col-12 pl-0 d-md-flex">

				<div class="col-md-5 mb-3 float-left main-book-image-container">

					<img class="img-fluid hvr-grow" 

					src="<?php echo $myImg;?>"

					srcset="<?php echo $myImgRetina;?>"

					title="<?php echo 'imagen del libro ' . $itemTitle;?>"

					alt="<?php echo 'imagen del libro ' . $itemTitle;?>"

					>

				</div>

				<div class="col-md-7 float-right">

					<p class="book-data-content section-heading module-title text-uppercase"><?php echo '<b>Autor:</b>' . $author;?></p>

					<p class="book-data-content section-heading module-title text-uppercase"><?php echo '<b>Editorial:</b>' . $publisher;?></p>

					<p class="book-data-content section-heading module-title text-uppercase"><?php echo '<b>ISBN: </b>' . $isbn;?></p>

					<p class="book-data-content section-heading module-title text-uppercase"><?php echo '<b>PÁGINAS: </b>' . $pages;?></p>

					<p class="book-data-content section-heading module-title text-uppercase"><?php echo '<b>PUBLICACIÓN:</b>' . $published;?></p>

					<p class="book-data-content section-heading module-title text-uppercase"><?php echo '<b>EDICIÓN: </b>' . $edition;?></p>

					<p class="book-data-content section-heading module-title text-uppercase"><?php echo '<b>IDIOMA: </b>' . $language;?></p>

					<p class="book-data-content section-heading module-title text-uppercase"><?php echo '<b>PRECIO: </b>' . $price;?></p>

					<p class="book-data-content section-heading module-title text-uppercase">

						<?php echo '<b>URL: </b><a target="_blank" href="' . $website . '"> ' . $website . '</a>';?>

					</p>

				</div>

			</div>

			<?php 

			$imagesQuery = $db->query(

				'SELECT * from museum_images WHERE sid = ? AND source = ? AND active = ? AND deleted = ? ORDER BY internal_order',

				[$itemResult->mid, 'individual_book', 1, 0]);

			if($imagesQuery->count()>=1){

				?>

				<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">

					<div class="slides"></div>

					<h3 class="title"></h3>

					<a class="prev">‹</a>

					<a class="next">›</a>

					<a class="close">×</a>

					<a class="play-pause"></a>

					<ol class="indicator"></ol>

				</div>

				<div id="links" class="row">

					<?php 

					foreach($imagesQuery->results() as $actualImg){ 

						$imageThumb = '../../../museumSmartAdmin/dist/default/private/sources/images/books/' . $itemResult->mid . '/images/' . $actualImg->unique_id . '/' . $itemResult->mid . '_' . $actualImg->unique_id . '_sq.png';

						$imageThumbRetina = '../../../museumSmartAdmin/dist/default/private/sources/images/books/' . $itemResult->mid . '/images/' . $actualImg->unique_id . '/' . $itemResult->mid . '_' . $actualImg->unique_id . '_sq@2x.png';

						$imageRetina = '../../../museumSmartAdmin/dist/default/private/sources/images/books/' . $itemResult->mid . '/images/' . $actualImg->unique_id . '/' . $itemResult->mid . '_' . $actualImg->unique_id . '_original@2x.png';

						$caption = $actualImg->description;

						if($caption == null) $caption = 'imagen del objeto ' . $itemName;

						?>
						<div class="col-4 col-md-4 col-lg-3 mt-5 thumbnail-container">

							<a href="<?php echo $imageRetina;?>" title="<?php echo $caption;?>">

								<img class="img-fluid hvr-grow" 

								src="<?php echo $imageThumb;?>" 

								srcset="<?php echo $imageThumbRetina;?>" 

								alt="<?php echo $caption;?>">

							</a>

						</div>

					<?php } ?>

				</div>

			<?php } ?> 

			<?php if($totalVideos>=1){ ?>

				<div class="row mt-5">

					<div class="col-12 text-left">

						<h4 class="section-heading module-title">VIDEOS</h4>

						<hr class="left-separator">

					</div>

				</div>

				<div id="blueimp-video-carousel" class="blueimp-gallery blueimp-gallery-controls blueimp-gallery-carousel">

					<div class="slides"></div>

					<h3 class="title"></h3>

					<a class="prev">‹</a>

					<a class="next">›</a>

					<a class="play-pause"></a>

				</div>

				<div id="video-links" class="row">

				</div>

			<?php } ?>


			<?php if($shareContent){ ;?>

				<div class="mt-5 mb-5 addthis_inline_share_toolbox_f3qw"></div>

			<?php }?>

		</div>

		<div class="col-2 d-none d-md-block other-items-col">

			<div class="text-left mt-5">

				<h5 class="mb-5 section-heading module-title text-center">OTROS LIBROS</h5>

			</div>

			<div class="col-12">

				<?php

				$itemsQuery = DB::getInstance()->query('

					SELECT *, mc.id as mid, img.unique_id 		as uid 

					FROM museum_books 							as mc

					LEFT JOIN museum_images 					as img 		

					ON mc.id = img.sid AND img.source 			= ?

					WHERE mc.deleted = ?

					ORDER BY mid DESC LIMIT 5', 

					(array('books', 0)));

				foreach($itemsQuery->results() as $lastItem){

					if($lastItem->mid == $itemResult->mid) continue;

					$lastItemName = $lastItem->title;

					$itemURL = '../' . $lastItem->url;

						//image...

					if($lastItem->uid == null){

						$myImg = '../../../private/img/templates/image-template/template_original.jpg';

						$myImgRetina = '../../../private/img/templates/image-template/template_original@2x.jpg';

					}else{

						$myImg = '../../../museumSmartAdmin/dist/default/private/sources/images/books/' . $lastItem->mid . '/' . $lastItem->uid . '/' . $lastItem->mid . '_' . $lastItem->uid . '_medium_sq.png';

						$myImgRetina = '../../../museumSmartAdmin/dist/default/private/sources/images/books/' . $lastItem->mid . '/' . $lastItem->uid . '/' . $lastItem->mid . '_' . $lastItem->uid . '_medium_sq@2x.png';
					}

					?>

					<a class="element-book-item element-item gallery-thumb col-12 mb-3" href="<?php echo $itemURL;?>">

						<div class="">

							<img class="img-fluid hvr-grow" 

							src="<?php echo $myImg;?>" 

							srcset="<?php echo $myImgRetina;?>" 

							alt="<?php echo $lastItemName;?>"

							title="<?php echo $lastItemName;?>">

							<div class="overlay lateral-item-overlay">

								<p class="mr-1 ml-1 font-light text-uppercase text-center lateral-item-name text-truncate" style="padding: 0 !important;"><?php echo $lastItemName;?> </p>

							</div>

						</div>

					</a>

				<?php } ?>

			</div>

		</div>

	</div>

</div>