<div class="container mt-5">

  <div class="row">

    <div class="col-lg-12 text-center">

      <h2 class="section-heading">CÁPSULAS EDUCATIVAS: HERRAMIENTAS PARA EDUCADORES</h2>

      <hr class="module-separator"
>
    </div>

    <div class="col-lg-8 mx-auto">

      <p class="text-light text-faded mb-5 text-center about-reflexiones module-text">En esta página encontrarás propuestas pedagógicas, instancias en vivo, espacios audiovisuales, talleres y algunas sorpresas más, en el marco de las #CápsulasEducativas 2021.</p>

    </div>

  </div>

</div>


<div class="container">
  
  <div class="row">

    <div class="col-lg-3 text-center sr-icon-1">

      <div class="service-box mt-3 mx-auto">

         <img class="protocol-image" src="../../private/img/protocolo/calendar.svg" />

       <h4 class="mt-3 mb-3 text-small text-uppercase custom-item-color capsules-defaults">LUNES 30 DE AGOSTO</h4>

       <p class="text-muted mb-0 text-light text-up">Discursos de odio - parte 1</p>

     </div>
    
    </div>

    <div class="col-lg-3 text-center sr-icon-1">

      <div class="service-box mt-3 mx-auto">

         <img class="protocol-image" src="../../private/img/protocolo/calendar.svg" />

       <h4 class="mt-3 mb-3 text-small text-uppercase custom-item-color capsules-defaults">Martes 31 de agosto</h4>

       <p class="text-muted mb-0 text-light text-up">Discursos de odio - parte 2</p>

     </div>

     </div>

     <div class="col-lg-3 text-center sr-icon-1">

      <div class="service-box mt-3 mx-auto">

         <img class="protocol-image" src="../../private/img/protocolo/calendar.svg" />

       <h4 class="mt-3 mb-3 text-small text-uppercase custom-item-color capsules-defaults">Miércoles 1° de septiembre</h4>

       <p class="text-muted mb-0 text-light text-up">Derechos vulnerados - parte 1</p>

     </div>

     </div>

     <div class="col-lg-3 text-center sr-icon-1">

      <div class="service-box mt-3 mx-auto">

         <img class="protocol-image" src="../../private/img/protocolo/calendar.svg" />

       <h4 class="mt-3 mb-3 text-small text-uppercase custom-item-color capsules-defaults">Jueves 2 de septiembre</h4>

       <p class="text-muted mb-0 text-light text-up">Derechos vulnerados - parte 2</p>

     </div>

     </div>

 </div>

</div>

<div class="mt-5 page-body-content">

		<div class="full-container mt-5">

			<div id="" class='wrapper text-center'>

				<div id="filters" class="btn-group-justified button-group btn-group-sm  filters-button-group mb-5">  

					<button class="filter-button btn button" data-filter="*">TODAS</button>

					<?php

					$menuQuery = DB::getInstance()->query('SELECT * FROM museum_education_keywords WHERE active= ? AND deleted = ?', [1, 0]);

					foreach($menuQuery->results() as $menu){  ?>

						<button class="filter-button btn button" data-filter="<?php echo '.' . str_replace(' ', '-', $menu->name);?>"><?php echo $menu->name;?></button>
						
					<?php }

					?>

				</div>

			</div>

		</div>	

		<div class="container">

			<div class="grid">

				<?php

				$contenetQuery = DB::getInstance()->query('

					SELECT * FROM museum_education_modules 				as mem

					WHERE mem.deleted 														= ? 

					AND mem.active 																= ? 

					ORDER by added DESC', 

					(array(0, 1)));

									
				foreach($contenetQuery->results() as $item){ 

					$itemId = $item->id;

					$uid = $item->uid;

					$filtersQuery = DB::getInstance()->query('

					SELECT * FROM museum_education_modules_keywords_relations 				as 							memkr

					LEFT JOIN museum_education_keywords 															as  						mek

					on memkr.id_keyword 																							= 							mek.id

					WHERE memkr.deleted 																							= 							? 

					AND memkr.active 																									= 							?

					AND memkr.id_module 																							= 							?', 

					(array(0, 1, $itemId)));

					$filters = '';

					foreach($filtersQuery->results() as $fItem){ 

						$filters .= str_replace(' ', '-', $fItem->name) . ' ';

					}

					$itemName = $item->title;

					$buttonLabel = $item->button_label;

					$ext = $item->extension;

					//$itemURL = $item->url;
					
					$caption = $item->caption;

					$link = $item->link;

					$isYoutube = $item->is_youtube;

					$isInstagram = $item->is_instagram;

					$isFacebook = $item->is_facebook;
					
					$isZoom = $item->is_zoom;

					$isMdh = $item->is_mdh;

					$isOther = $item->is_other;

					$isBlank  = $item->is_blank;


					$date = utf8_encode(strftime("%A, %d de %B de %Y", strtotime($item->added)));

					$icon = '';

					//image...

					//filter the extension to create custom buttons...

					$customButton;

					$myImg = '../../private/img/templates/image-template/template_medium.jpg';

					$myImgRetina = '../../private/img/templates/image-template/template_medium@2x.jpg';

					$shadow = '../../private/img/templates/comunicado-template/shadow.png';

					$isFile = false;

					$isImage = false;

					$isVideo = false;

					$isAudio = false;

					$isPDF = false;

					$isCompress = true;

					$isWord = true;
					
					$isExcel = true;

					$isPPT = false;

					if($link != ''){

						$customButton = 'ABRIR LINK';

						if($isYoutube == 1){

							$customButton = ($buttonLabel != '' && $buttonLabel != NULL) ? $buttonLabel : 'VER VIDEO';

							$icon = '../../private/img/icons/youtube.svg';

							$myImg = '../../private/img/templates/image-template/template_video.jpg';

							$myImgRetina = '../../private/img/templates/image-template/template_video.jpg';

						}else if($isInstagram == 1){

							$icon = '../../private/img/icons/instagram.svg';

							$myImg = '../../private/img/templates/image-template/template_instagram.jpg';

							$myImgRetina = '../../private/img/templates/image-template/template_instagram.jpg';

						}else if($isFacebook == 1 ){

							$icon = '../../private/img/icons/facebook.svg';

						}else if($isZoom == 1 ){

							$icon = '../../private/img/icons/zoom.svg';

						}else if($isMdh == 1 ){

							$icon = '../../private/img/icons/mdh.svg';

						}else{

							$icon = '../../private/img/icons/link.svg';

						}


					}

					if($buttonLabel != '' && $buttonLabel != NULL){

						$customButton = $buttonLabel;

					}


					if($ext == '' || !isset($ext) && $link != ''){

						//$customButton = 'ABRIR';

					}else{

						$isFile = true;

						if($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'gif' || $ext == 'webp' || $ext == 'tif' ){

							$isImage = true;

							$customButton = ($buttonLabel != '' && $buttonLabel != NULL) ? $buttonLabel : 'VER IMAGEN';

							$icon = '../../private/img/icons/image.svg';

							$myImg = '../../private/img/templates/image-template/template_image.jpg';

							$myImgRetina = '../../private/img/templates/image-template/template_image.jpg';

						}else if($ext == 'mp4' || $ext == 'webm'){

							$isVideo = true;

							$customButton = ($buttonLabel != '' && $buttonLabel != NULL) ? $buttonLabel : 'VER VIDEO';

							$icon = '../../private/img/icons/video.svg';

							$myImg = '../../private/img/templates/image-template/template_video.jpg';

							$myImgRetina = '../../private/img/templates/image-template/template_video.jpg';

						}else if($ext == 'mp3'){

							$isAudio = true;

							$customButton = ($buttonLabel != '' && $buttonLabel != NULL) ? $buttonLabel : 'REPRODUCIR AUDIO';

							$icon = '../../private/img/icons/audio.svg';

							$myImg = '../../private/img/templates/image-template/template_audio.jpg';

							$myImgRetina = '../../private/img/templates/image-template/template_audio.jpg';

						}else if($ext == 'rar' || $ext == 'zip'){

							$isCompress = true;

							$customButton = ($buttonLabel != '' && $buttonLabel != NULL) ? $buttonLabel : 'DESCARGAR';

							$icon = '../../private/img/icons/file.svg';

							$myImg = '../../private/img/templates/image-template/template_file.jpg';

							$myImgRetina = '../../private/img/templates/image-template/template_file.jpg';

						}else if($ext == 'doc' || $ext == 'docx' || $ext == 'docm' || $ext == 'dotx' || $ext == 'odt' || $ext == 'ods' || $ext == 'odp' ){

							$isWord = true;

							$customButton = ($buttonLabel != '' && $buttonLabel != NULL) ? $buttonLabel : 'DESCARGAR DOCUMENTO';

							$icon = '../../private/img/icons/word.svg';

							$myImg = '../../private/img/templates/image-template/template_word.jpg';

							$myImgRetina = '../../private/img/templates/image-template/template_word.jpg';

						}else if($ext == 'xls' || $ext == 'xlsx'){

							$isExcel = true;

							$customButton = ($buttonLabel != '' && $buttonLabel != NULL) ? $buttonLabel : 'DESCARGAR PLANILLA';

							$icon = '../../private/img/icons/excel.svg';

							$myImg = '../../private/img/templates/image-template/template_excel.jpg';

							$myImgRetina = '../../private/img/templates/image-template/template_excel.jpg';

						}else if($ext == 'ppt' || $ext == 'pptx'){

							$isPPT = true;

							$customButton = ($buttonLabel != '' && $buttonLabel != NULL) ? $buttonLabel : 'DESCARGAR PPT';

							$icon = '../../private/img/icons/ppt.svg';

							$myImg = '../../private/img/templates/image-template/template_ppt.jpg';

							$myImgRetina = '../../private/img/templates/image-template/template_ppt.jpg';


						}else if($ext == 'pdf'){

								$isPDF = true;

								$customButton = ($buttonLabel != '' && $buttonLabel != NULL) ? $buttonLabel : 'ABRIR PDF';

								$icon = '../../private/img/icons/pdf.svg';

								$myImg = '../../private/img/templates/image-template/template_pdf.jpg';

								$myImgRetina = '../../private/img/templates/image-template/template_pdf.jpg';

						}

					}

					?>

					<div class="<?php echo $filters;?> col-lg-6 col-md-6 col-12 news-item news-list-container pr-1 pl-1">

						<div class="image-container mr-2 ml-2 mb-4 item-rounded">

								<img class="news-image hvr-grow img-fluid" 

								src="<?php echo $myImg;?>"

								srcset="<?php echo $myImgRetina;?>" 

								alt="<?php echo $itemName;?>"

								title="<?php echo $itemName;?>">

								<img class="img-shadow position-absolute img-fluid"  src="<?php echo $shadow;?>" 

								srcset ="<?php echo $shadow;?>" >


								<div class="type-icon-container">

									<img class="icon float-right	mr-2"  src="<?php echo $icon;?>">

							</div>

							<!--<h2 class="white text-truncate news-title position-absolute text-uppercase mt-xl-5 mt-md-4"> <?php echo $itemName;?></h2>-->

								<div class="buttons-container row">

									<h2 class="white module-title text-uppercase "> <?php echo $itemName;?></h2>

								<?php if($link != ''){ ?>


									<?php if($isBlank == 1){?>

									<a id="bridger-<?php echo $itemId;?>" class = "bridger-<?php echo $itemId;?> read-btn-capsule btn button" role="button" href="<?php echo $link;?>"><?php echo $customButton;?></a>

								<?php }else{?>

									<a id="bridger-<?php echo $itemId;?>" class = "read-btn-capsule btn button" role="button" data-fancybox data-type="iframe" data-src="<?php echo $link;?>" href="javascript:;"><?php echo $customButton;?></a>

								<?php }?>

							<?php }else if($isFile == true){

								$fileName = $itemId . '_' . $uid . '_uploaded.' . $ext;

								$pathToFile = '../../museumSmartAdmin/dist/default/private/sources/mix/' . $itemId . '/' . $uid . '/' . $fileName;

								?>

								<!-- si es archivo, hay que tomar decisiones en funcion del tipo de archivo. -->

								<?php if($isImage == 1){?>

									<a id="bridger-<?php echo $itemId;?>" class="read-btn-capsule btn button" role="button" href="<?php echo $pathToFile;?>" data-fancybox="image" data-caption=""><?php echo $customButton;?></a>

								<?php }else if($isVideo == true){?>

									<a id="bridger-<?php echo $itemId;?>"  class = "read-btn-capsule btn button" role="button" href="<?php echo $pathToFile;?>" data-fancybox="video" data-caption=""><?php echo $customButton;?></a>

								<?php }else if($isAudio == true){?>


									<a id="bridger-<?php echo $itemId;?>"  class = "read-btn-capsule btn button" role="button" href="<?php echo $pathToFile;?>" data-fancybox data-type="video" data-caption=""><?php echo $customButton;?></a>

								<?php }else if($isPDF == true){?>

									
									<a id="bridger-<?php echo $itemId;?>"  class = "read-btn-capsule btn button" role="button" href="<?php echo $pathToFile;?>" data-caption= "" data-fancybox data-type="pdf"><?php echo $customButton;?></a>
								
									<?php }else if($isWord == true){?>

									<a id="bridger-<?php echo $itemId;?>"  class = "read-btn-capsule btn button" role="button" href="<?php echo $pathToFile;?>" download ="<?php echo $itemName . '.' . $ext;?>"><?php echo $customButton;?></a>

									<?php } ?>

								<?php }else if($isExcel == true){?>

									<a id="bridger-<?php echo $itemId;?>"  class = "read-btn-capsule btn button" role="button" href="<?php echo $pathToFile;?>" download ="<?php echo $itemName . '.' . $ext;?>"><?php echo $customButton;?></a>

							<?php } 

							if($caption != '' && $caption != '<br>'){ ?>

								<a data-button = "<?php echo $customButton;?>" data-id= "<?php echo $itemId;?>" data-caption = "<?php echo $caption;?>" data-title="<?php echo  $itemName;?>" class = "fancybox fancy-popupbox ml-2 read-btn-capsule btn button module-description" role="button">SOBRE EL MÓDULO</a>


							<?php } ?>

							</div>







						</div>

					</div>


				<?php } ?>

			</div>

		</div>

		<div id="no-results" class="container d-none">

			<h4 class="mb-5 text-uppercase text-medium no-result-msg text-center">No se encontraron resultados para este filtro</h4>

		</div>

	</div>

