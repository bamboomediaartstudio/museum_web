

<div class="mt-5 page-body-content">

	<div class="offset-1 col-10 offset-md-2 col-md-8 text-center mt-5">

		<h4 class="section-heading text-medium">NUESTRAS CLASES VIRTUALES</h4>

		<p class="section-description dark mt-5">

			<span class="text-light black">
				Ingresá a la clase que te interese para conocer el programa, días y horarios.
			</span>
		</p>

	</div>

	<div class="container mt-5">

		<div class="grid">

			<?php

			$classesQuery = DB::getInstance()->query('

				SELECT *, mc.id as mid, img.unique_id 		as uid 

				FROM museum_virtual_classes 				as mc

				LEFT JOIN museum_images 					as img 		

				ON mc.id = img.sid AND img.source 			= ?

				AND img.deleted 							= ?

				WHERE mc.deleted 							= ? 

				AND mc.active 								= ? 

				AND mc.is_experience 					     = ? 

				ORDER by mid dESC', 

				(array('classes', 0, 0, 1, 0)));

			$count = 0; 

			foreach($classesQuery->results() as $class){ 

				$filters = '';

				$className = $class->name;

				$classURL = $class->url;

				$classId = $class->id;

				$itemURL = $class->url;
					
				//image...

				if($class->uid == null){
				
					$myImg = '../private/img/templates/image-template/template_medium.jpg';

					$myImgRetina = '../private/img/templates/image-template/template_medium@2x.jpg';

				}else{

					$myImg = '../museumSmartAdmin/dist/default/private/sources/images/classes/' . $class->mid . '/' . $class->uid . '/' . $class->mid . '_' . $class->uid . '_medium.jpeg';

					$myImgRetina = '../museumSmartAdmin/dist/default/private/sources/images/classes/' . $class->mid . '/' . $class->uid . '/' . $class->mid . '_' . $class->uid . '_medium@2x.jpeg';
				}

				$comunicado = '../private/img/templates/comunicado-template/comunicado.png';

				$shadow = '../private/img/templates/comunicado-template/shadow.png';

				$comunicadoRetina = '../private/img/templates/comunicado-template/comunicado@2x.png';


				?>

				<div class="element-item col-md-6 col-12 news-item news-list-container pr-1 pl-1">

					<div class="image-container mr-2 ml-2 mb-4">

						<a href="<?php echo $itemURL;?>">

							<img class="news-image hvr-grow img-fluid" 

							src="<?php echo $myImg;?>"

							srcset="<?php echo $myImgRetina;?>" 

							alt="<?php echo $className;?>"

							title="<?php echo $className;?>">

							<img class="img-shadow position-absolute img-fluid"  src="<?php echo $shadow;?>" 

							srcset ="<?php echo $shadow;?>" >

						</a>

						<h2 class="white text-truncate news-title position-absolute text-uppercase mt-xl-5 mt-md-4"><?php echo $className;?></h2>

						<a href="<?php echo $itemURL;?>" role="button" class="read-btn btn button">VER CLASE</a>

					</div>

				</div>

			<?php } ?>

		</div>

	</div>

	<div id="no-results" class="container d-none">

		<h4 class="mb-5 text-uppercase text-medium no-result-msg text-center">No se encontraron resultados para este filtro</h4>

	</div>

</div>

