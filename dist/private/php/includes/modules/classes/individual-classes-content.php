<?php


$className = $classResult->name;

$showDuration = $classResult->show_duration;

$duration = $classResult->duration;

$showPrice = $classResult->show_price;

$price = '$' . number_format($classResult->price);

$email = $classResult->email;

$phone = $classResult->phone;

$observations = $classResult->observations;

$shareClass = $classResult->allow_share;

$emailSubject = 'Información de la clase virtual ' . $classResult->name;

$emailBody = 'Hola, te escribo para solicitar más información respecto a la clase virtusal: ' . $classResult->name;

$hrefMailString = 'mailto:' . $email . '?subject=' . $emailSubject . '&body=' . $emailBody;

$whatsapp = $classResult->whatsapp;

$whatsappString = urlencode('Hola, ¿cómo estás? Te contacto para información respecto a la clase virtual *_' . $classResult->name . '_*');

$whatsappURL = 'https://wa.me/' . $classResult->whatsapp . '?text=' . $whatsappString;

$downloadPDF = $classResult->allow_program_download;

$description = $classResult->description;


if($downloadPDF == 1){

	$pdfPath = '../../museumSmartAdmin/dist/default/private/sources/pdf/classes/' . $classResult->mid . '/pdf/' . $classResult->url . '.pdf';
}

$videosQuery = $db->query('SELECT * FROM museum_youtube_videos WHERE sid = ? AND source = ? AND active = ? AND deleted = ?', [$classResult->mid, 'class_video', 1, 0]);

$totalVideos = $videosQuery->count();



	?>

	<div class="container" id="course-info">

		<input type="hidden" id="total-videos" name="total-videos" value="<?php echo $totalVideos;?>">

		<div class="row">

			<div class="col-md-9 col-12">

				<!-- about course information -->

				<div class="row mt-5">

					<div class="col-12 text-left">

						<h4 class="section-heading module-title">SOBRE LA CLASE VIRTUAL:</h4>

						<hr class="left-separator">

					</div>

					<div class="col-11">

						<p class="dark text-left about-reflexiones module-text text-light"> <?php echo $description;?></p>

					</div>

				</div>

				<?php

			//we should proceed with this module if:
			// - there is an init time
			// - there is an end time
			// - the number of classes was setted.
			// - the duration was defined
			// - the price should be shown

				if($showDuration != 0 || $showPrice != 0){ ?>

					<div class="row mt-5 course-aditional-info">

						<div class="col-12 text-left">

							<h4 class="section-heading module-title">INFORMACIÓN</h4>

							<hr class="left-separator">

						</div>

						<?php if($downloadPDF == 1){ ?>

							<div class="col-12 course-aditional-info-content">

								<p class="dark text-left about-reflexiones module-text text-light">Podes descargar el programa de la clase haciendo <a href="<?php echo $pdfPath;?>" role="button" class ="" download>click aquí</a></p>

							</div>

						<?php } ?>


						
						<?php if($showDuration != 0){ ?>

							<div class="col-12">

								<p class="dark text-left about-reflexiones module-text text-light"><strong>Duración de la clase:</strong> <?php echo $duration;?></p>

							</div>

						<?php } ;?>

						<?php if($showPrice != 0){ ?>

							<div class="col-12">

								<p class="dark text-left about-reflexiones module-text text-light"><strong>Precio de la clase virtual:</strong> <?php echo $price;?></p>

							</div>

						<?php } ;?>

					</div>

				<?php }; ?>

				<!-- contact -->

				<?php 

		// we should procceed printing this module if:
		// there is an email.
		// there is a phone.
		// there is an address.

				if($email != '' || $email != NULL || $phone != '' || $phone != NULL  || $whatsapp != '' || $whatsapp != NULL){
					?>

					<div class="row mt-5 course-contact-information">

						<div class="col-12 text-left">

							<h4 class="section-heading module-title course-contact-information-title">CONTACTO</h4>

							<hr class="left-separator">

						</div>

						<?php if($email != '' || $email != NULL){ ?>

							<div class="col-12">


								<p class="dark text-left about-reflexiones module-text text-light"><strong>Email: </strong> <a href="<?php echo $hrefMailString;?>"><?php echo $email;?></a></p>



							</div>

						<?php } ;?>

						<?php if($whatsapp != '' || $whatsapp != NULL){ ?>

							<div class="col-12">


								<p class="dark text-left about-reflexiones module-text text-light"><strong>Whatsapp: </strong> <a href="<?php echo $whatsappURL;?>"><?php echo $whatsapp;?></a></p>



							</div>

						<?php } ;?>

						<?php if($phone != '' || $phone != NULL){ ?>

							<div class="col-12">

								<p class="dark text-left about-reflexiones module-text text-light"><strong>Teléfono: </strong><a href="tel:<?php echo $phone;?>"><?php echo $phone;?></a></p>

							</div>

						<?php } ;?>


					</div>

				<?php } ?>

				<!-- info adicional -->

				<?php if($observations != '' || $observations != NULL){ ?>

					<div class="row mt-5">

						<div class="col-12 text-left">

							<h4 class="section-heading module-title">INFORMACIÓN ADICIONAL</h4>

							<hr class="left-separator">

						</div>

						<div class="col-11">

							<p class="dark text-left about-reflexiones module-text text-light"> <?php echo $observations;?></p>

						</div>

					</div>

				<?php } ;?>

				<?php


			$imagesQuery = $db->query(

				'SELECT * from museum_images WHERE sid = ? AND source = ? AND active = ? AND deleted = ? ORDER BY internal_order',

				[$classResult->mid, 'individual_class', 1, 0]);

			if($imagesQuery->count()>=1){

				?>

				<div class="row mt-5">

					<div class="col-12 text-left">

						<h4 class="section-heading module-title">IMÁGENES</h4>

						<hr class="left-separator">

					</div>

				</div>

				<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">

					<div class="slides"></div>

					<h3 class="title"></h3>

					<a class="prev">‹</a>

					<a class="next">›</a>

					<a class="close">×</a>

					<a class="play-pause"></a>

					<ol class="indicator"></ol>

				</div>

				<div id="links" class="row">

					<?php 

					foreach($imagesQuery->results() as $actualImg){ 

						$imageThumb = '../../museumSmartAdmin/dist/default/private/sources/images/classes/' . $classResult->mid . '/images/' . $actualImg->unique_id . '/' . $classResult->mid . '_' . $actualImg->unique_id . '_original.jpeg';

						$imageThumbRetina = '../../museumSmartAdmin/dist/default/private/sources/images/classes/' . $classResult->mid . '/images/' . $actualImg->unique_id . '/' . $classResult->mid . '_' . $actualImg->unique_id . '_original@2x.jpeg';

						$imageRetina = '../../museumSmartAdmin/dist/default/private/sources/images/classes/' . $classResult->mid . '/images/' . $actualImg->unique_id . '/' . $classResult->mid . '_' . $actualImg->unique_id . '_original@2x.jpeg';

						$caption = $actualImg->description;

						if($caption == null) $caption = 'imagen de la clase virtual ' . $className;

						?>
						<div class="col-12 mt-5 thumbnail-container">

							<a href="<?php echo $imageRetina;?>" title="<?php echo $caption;?>">

								<img class="img-fluid hvr-grow" 

								src="<?php echo $imageThumb;?>" 

								srcset="<?php echo $imageThumbRetina;?>" 

								alt="<?php echo $caption;?>">

							</a>

						</div>

					<?php } ?>

				</div>

			<?php } ?> 

			<?php if($totalVideos>=1){

			?>

			<div class="row mt-5">

				<div class="col-12 text-left">

					<h4 class="section-heading module-title">VIDEOS</h4>

					<hr class="left-separator">

				</div>

			</div>

			<div id="blueimp-video-carousel" class="blueimp-gallery blueimp-gallery-controls blueimp-gallery-carousel">

				<div class="slides"></div>

				<h3 class="title"></h3>

				<a class="prev">‹</a>

				<a class="next">›</a>

				<a class="play-pause"></a>

			</div>

			<div id="video-links" class="row">

			</div>

		<?php } ?>


		<?php if($shareClass){ ;?>

			<div class="mt-5 mb-5 addthis_inline_share_toolbox_f3qw"></div>

		<?php }?>

	</div>

	<div class="general-sidebar col-3 d-none d-md-block">

		<div class="text-left mt-5">

			<!--<h5 class="mb-5 section-heading module-title text-center">OTRAS CLASES</h5>-->

		</div>

		<div class="col-12">

			<?php

			$classesQuery = DB::getInstance()->query('

				SELECT *, mc.id as mid, img.unique_id 		as uid 

				FROM museum_virtual_classes 				as mc

				LEFT JOIN museum_images 					as img 		

				ON mc.id = img.sid AND img.source 			= ?

				WHERE mc.deleted = ? AND mc.active = ?

				ORDER BY mid DESC LIMIT 5', 

				(array('classes', 0, 1)));

			foreach($classesQuery->results() as $class){

				if($class->mid == $classResult->mid) continue;

				$className = $class->name;

				$classURL = '../' . $class->url;


				if($class->uid == null){

					$myImg = '../../private/img/templates/image-template/template_medium.jpg';
					
					$myImgRetina = '../../private/img/templates/image-template/template_medium@2x.jpg';

				}else{

					$myImg = '../../museumSmartAdmin/dist/default/private/sources/images/classes/' . $class->mid . '/' . $class->uid . '/' . $class->mid . '_' . $class->uid . '_medium.jpeg';

					$myImgRetina = '../../museumSmartAdmin/dist/default/private/sources/images/classes/' . $class->mid . '/' . $class->uid . '/' . $class->mid . '_' . $class->uid . '_medium@2x.jpeg';
				}

				?>

				<a class="" href="<?php echo $classURL;?>">

					<div class="hover ehover12 general-sidebar-thumb" style="height: auto !important">

						<img class="img-fluid" 

						src="<?php echo $myImg;?>" 

						src="<?php echo $myImgRetina;?>" 

						alt="<?php echo $className;?>"

						title="<?php echo $className;?>">

						<div class="overlay lateral-item-overlay">

							<h2 class="font-light course-lateral-name" style="padding: 0 !important;"><?php echo $className;?> </h2>

						</div>

					</div>

				</a>

	
			<?php } ?>

		</div>

	</div>

</div>

</div>



				
				