<?php
?>

<section id="contact">

	<div id="map"></div>

	<div class="container mt-5 mb-5">

		<div class="row">

			<form id="contact-form" class="col-lg-7 col-12">

				<h3 class="ml-3 text-medium text-uppercase">contacto</h3>

				
				<div class="form-group">

					<label for="name" class="control-label col-md-3 text-light black">NOMBRE

						<span class="required" aria-required="true"> * </span>

					</label>
					
					<div class="col-md-12">

						<input type="text" id="name" name="name" title="name" data-required="1" class="form-control form-input text-light form-control black text-light"> 

					</div>
					
				</div>

				<div class="form-group">

					<label for="surname" class="control-label col-md-3 text-light black">APELLIDO

						<span class="required" aria-required="true"> * </span>

					</label>
					
					<div class="col-md-12">

						<input type="text" id="surname" title="surname" name="surname" data-required="1" class="form-control form-input text-light form-control black text-light"> 

					</div>
					
				</div>


				<div class="form-group">

					<label for="email" class="control-label col-md-3 text-light black">EMAIL

					</label>
					
					<div class="col-md-12">

						<input id="email" type="text" title="email" name="email" data-required="1" class="form-control form-input text-light form-control black text-light"> 

					</div>
					
				</div>

				<div class="form-group">

					<label for="form-subject" class="control-label col-md-3 text-light black">ASUNTO:</label>

					<div class="col-md-12">

						<select title="select" id="form-subject" name="form-subject" class=" form-select form-control form-control required">

							<option disabled selected value>Seleccioná una opción</option>
							
							<option>Información sobre visitas guiadas.</option>
							
							<option>Información sobre cursos.</option>

							<option>Quiero ser donante.</option>

							<option>Quiero ser sponsor.</option>

							<option>Prensa.</option>

							<option>Información sobre la biblioteca.</option>

							<option>Otro</option>

						</select>

					</div>

				</div>


				<div class="form-group">

					<label for="form-text-area" class="control-label col-md-3 text-light black">MENSAJE:</label>


					<div class="col-12">

						<textarea title="text" name="form-text-area" class="black form-input text-light form-control" id="form-text-area" rows="5" placeholder="el mensaje:"></textarea>

					</div>

				</div>

				<button id="course-button" type="submit" class="ml-3 col-4 hvr-sweep-to-right btn btn-square">ENVIAR</button>

			</form>

			<div class="ml-3 col-lg-4 col-11 float-right mt-5">

				<p class="text-light black">

				Utilizá el formulario de contacto para hacenros llegar tus dudas, consultas o inquietudes. Te responderemos a la brevedad.</p>

				<p>
					<span class="text-medium form-separators">DIRECCIÓN:</span><br>

					<span class="text-light black">Dirección: Montevideo 919, C1019ABS CABA.</span>
				</p>

				<p><span class="text-medium form-separators">EMAIL:</span><br><span class="text-light black "><a href="mailto:info@museodelholocausto.org.ar">info@museodelholocausto.org.ar</a>.</span></p>

				<p>

					<span class="text-medium form-separators">TELÉFONO:</span><br>
					<span class="text-light black ">(5411) 3987-1945</span>



				</p>

				<dlv class="social-media-icons-contact">

					<?php

					$smQuery = DB::getInstance()->query('SELECT * from museum_sm WHERE active = ? ORDER BY internal_order ASC', [1]);

					foreach($smQuery->results() as $result){ 

						?>

							<a class="mr-3" target="_blank" href="<?php echo $result->url;?>" data-toggle="tooltip" data-placement="bottom" title="Visitanos en <?php echo $result->name;?>">

								<i class="sm-bottom-icon <?php echo $result->icon;?>"></i>

							</a>


					<?php }?> 

				</dlv>


			</div>

		</div>


	</div>

</section>