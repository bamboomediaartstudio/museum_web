

<div class="mt-5 page-body-content">

	<!--<div class="sm-offset-1 sm-col-8 offset-2 col-8 text-center mt-5">-->

	<div class="offset-1 col-10 offset-md-2 col-md-8 text-center mt-5">

		<h4 class="section-heading  text-medium"><?php echo $wording->get('courses_first_line');
			;?></h4>

		<p class="section-description dark mt-5">

			<span class="text-light black">
				<?php echo $wording->get('courses_second_line');?>
			</span>
		</p>

	</div>

	<div class="full-container mt-5">

		<div id="" class='wrapper text-center'>

			<div id="filters" class="btn-group-justified button-group btn-group-sm  filters-button-group mb-5">  

				<button class="mb-1 filter-button btn  button" data-filter="*">TODOS</button>

				<?php

				$coursesMenuQuery = DB::getInstance()->query('SELECT * FROM museum_courses_modalities WHERE active= ? AND deleted = ?', [1, 0]);

				foreach($coursesMenuQuery->results() as $coursesMenu){  ?>

					<button class="mb-1 filter-button btn button" data-filter="<?php echo '.' . $coursesMenu->modality_tag;?>"><?php echo $coursesMenu->course_modality;?></button>

				<?php }

				?>
				
				<button class="mb-1 filter-button btn  button" data-filter=".next">próximos</button>
				
			</div>

		</div>

	</div>	

	<div class="container">

		<div class="grid">

			<?php

			$coursesQuery = DB::getInstance()->query('

				SELECT *, mc.id as mid, img.unique_id 		as uid 

				FROM museum_courses 						as mc

				LEFT JOIN museum_images 					as img 		

				ON mc.id = img.sid AND img.source 			= ?

				AND img.deleted 							= ?

				WHERE mc.deleted 							= ? 

				AND mc.active 								= ? 

				ORDER by mid dESC', 

				(array('courses', 0, 0, 1)));

			$count = 0; 

			foreach($coursesQuery->results() as $course){ 

				$filters = '';

				$courseName = $course->name;

				$courseURL = $course->url;

				$courseId = $course->id;

				//image...

				if($course->uid == null){

					//$myImg = '../private/img/templates/course-template/course_template_sq.jpg';
					
					$myImg = '../private/img/templates/image-template/template_medium_sq.jpg';

					$myImgRetina = '../private/img/templates/image-template/template_medium_sq@2x.jpg';

				}else{

					$myImg = '../museumSmartAdmin/dist/default/private/sources/images/courses/' . $course->mid . '/' . $course->uid . '/' . $course->mid . '_' . $course->uid . '_medium.jpeg';

					$myImgRetina = '../museumSmartAdmin/dist/default/private/sources/images/courses/' . $course->mid . '/' . $course->uid . '/' . $course->mid . '_' . $course->uid . '_medium@2x.jpeg';
				}

				$maestros = '../private/img/templates/escuela-de-maestros-template/banner-template.png';
				
				$maestrosRetina = '../private/img/templates/escuela-de-maestros-template/banner-template@2x.png';

				$myErrorImage = '../private/img/templates/image-template/template_medium_sq.jpg';

				$myErrorImageRetina = '../private/img/templates/image-template/template_medium_sq@2x.jpg';

				$shadow = '../private/img/templates/comunicado-template/shadow.png';


				//data...

				if($course->start_date == '0000-00-00' || $course->end_date == '0000-00-00' || $course->start_date == NULL || $course->end_date == NULL){

					$finalTime = '';

				}else{

					setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

					$initTime = strftime("del %A, %d de %B del %Y", strtotime($course->start_date));

					$endTime = strftime("al %A, %d de %B del %Y", strtotime($course->end_date));

					$finalTime = utf8_encode($initTime . ' ' . $endTime);

					$now = date('Y-m-d');

					if($course->start_date > $now) { $filters.= 'next' . ' '; }
					
					if($course->end_date < $now) { 

						//$filters.= 'next' . ' '; 

						$finalTime = 'FINALIZADO';  

					}


				}

				//tags...

				$school = false;

				$filterQuery = DB::getInstance()->query(

					'SELECT * FROM museum_courses_modalities_relations as mcmr 

					INNER JOIN museum_courses_modalities as mcm ON mcmr.id_modality = mcm.id 

					WHERE mcmr.id_course = ?', [$course->mid]);

				foreach($filterQuery->results() as $filter){

					if($filter->modality_tag == 'school') $school = true;

					$filters .= $filter->modality_tag . ' ';

				}

				?>

				<div class="<?php echo $filters;?> col-lg-6 col-md-6 col-12 news-item news-list-container pr-1 pl-1">

					<div class="image-container mr-2 ml-2 mb-4">

						<a href="<?php echo $courseURL;?>">

							<img class="news-image hvr-grow img-fluid" 

							src="<?php echo $myImg;?>"

							srcset="<?php echo $myImgRetina;?>" 

							alt="<?php echo $courseName;?>"

							title="<?php echo $courseName;?>">

							<img class="img-shadow position-absolute img-fluid"  src="<?php echo $shadow;?>" 

							srcset ="<?php echo $shadow;?>" >

						</a>

						<h2 class="white text-truncate news-title position-absolute text-uppercase mt-xl-5 mt-md-4"><?php echo $courseName;?></h2>

						<a href="<?php echo $courseURL;?>" role="button" class="read-btn btn button">VER CURSO</a>

					</div>

				</div>

			<?php } ?>

		</div>

	</div>

	<div id="no-results" class="container d-none">

		<h4 class="mb-5 text-uppercase text-medium no-result-msg text-center">No se encontraron resultados para este filtro</h4>

	</div>

</div>

