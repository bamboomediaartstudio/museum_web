<?php

//$detect = new Mobile_Detect;

//$isMobile;

if($courseResult->start_date == '0000-00-00' || $courseResult->end_date == '0000-00-00' || 

	$courseResult->start_date == NULL || $courseResult->end_date == NULL){

	$endTime = '';

$initTime = '';

}else{

	setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

	$initTime = utf8_encode(strftime("%A, %d de %B del %Y", strtotime($courseResult->start_date)));

	$endTime = utf8_encode(strftime("%A, %d de %B del %Y", strtotime($courseResult->end_date)));

}

$courseName = $courseResult->name;

$numberOfClasses = $courseResult->number_of_classes;

$showDuration = $courseResult->show_duration;

$duration = $courseResult->duration;

$showPrice = $courseResult->show_price;

$price = '$' . number_format($courseResult->price);

$email = $courseResult->email;

$phone = $courseResult->phone;

$address = $courseResult->address;

$observations = $courseResult->observations;

$shareCourse = $courseResult->allow_share;

$allowEnrollment = $courseResult->allow_enrollment;

$emailSubject = 'Información del curso ' . $courseResult->name;

$emailBody = 'Hola, te escribo para solicitar más información respecto al curso: ' . $courseResult->name;

$hrefMailString = 'mailto:' . $email . '?subject=' . $emailSubject . '&body=' . $emailBody;

$whatsapp = $courseResult->whatsapp;

$whatsappString = urlencode('Hola, ¿cómo estás? Te contacto para información respecto al curso *_' . $courseResult->name . '_*');

$whatsappURL = 'https://wa.me/' . $courseResult->whatsapp . '?text=' . $whatsappString;

$downloadPDF = $courseResult->allow_program_download;

$description = $courseResult->course_description;

if($courseResult->show_address){

	$mapURL = 'https://www.google.com/maps/search/?api=1&query=' . $courseResult->lat .  ','. $courseResult->long;
}

if($downloadPDF == 1){

	$pdfPath = '../../museumSmartAdmin/dist/default/private/sources/pdf/courses/' . $courseResult->mid . '/pdf/' . $courseResult->url . '.pdf';
}

//SELECT * from museum_images WHERE sid = 65 AND source = 'individual_course' AND active = 1 AND deleted = 0 ORDER BY internal_order

$videosQuery = $db->query('SELECT * FROM museum_youtube_videos WHERE sid = ? AND source = ? AND active = ? AND deleted = ?', [$courseResult->mid, 'course_video', 1, 0]);

$totalVideos = $videosQuery->count();

$queryTutors = $db->query(

	'SELECT *, img.unique_id 								as 			uid,

	mct.id 													as 			mid

	from museum_courses_tutors_relations 					as 			mctr

	LEFT join museum_courses_tutors 						as 			mct 

	on mctr.id_tutor = mct.id  AND mctr.deleted = ?

	LEFT JOIN museum_images 								as 			img 

	ON mct.id = img.sid AND img.source 						= ?

	AND img.deleted 										= ?

	WHERE mctr.id_course = ? AND mct.active = ?

	AND mct.deleted = ?', [0, 'tutors', 0,  $courseResult->mid, 1, 0]);

	?>

	<div class="container" id="course-info">

		<input type="hidden" id="total-videos" name="total-videos" value="<?php echo $totalVideos;?>">

		<div class="row">

			<div class="col-md-9 col-12">

				<!-- about course information -->

				<div class="row mt-5">

					<div class="col-12 text-left">

						<h4 class="section-heading module-title">SOBRE EL CURSO:</h4>

						<hr class="left-separator">

					</div>

					<div class="col-11">

						<p class="dark text-left about-reflexiones module-text text-light"> <?php echo $description;?></p>

					</div>

				</div>

				<?php

			//we should proceed with this module if:
			// - there is an init time
			// - there is an end time
			// - the number of classes was setted.
			// - the duration was defined
			// - the price should be shown

				if($initTime != '' || $endTime != '' || $numberOfClasses != 0 || $showDuration != 0 || $showPrice != 0){ ?>

					<div class="row mt-5 course-aditional-info">

						<div class="col-12 text-left">

							<h4 class="section-heading module-title">INFORMACIÓN</h4>

							<hr class="left-separator">

						</div>

						<?php if($downloadPDF == 1){ ?>

							<div class="col-12 course-aditional-info-content">

								<p class="dark text-left about-reflexiones module-text text-light">Podes descargar el programa al curso haciendo <a href="<?php echo $pdfPath;?>" role="button" class ="" download>click aquí</a></p>

							</div>

						<?php } ?>


						<?php if($initTime != ''){ ?>

							<div class="col-12 course-aditional-info-content">

								<p class="dark text-left about-reflexiones module-text text-light"><strong>Comienzo del curso:</strong> <?php echo $initTime;?></p>

							</div>

						<?php } ;?>

						<?php if($endTime != ''){ ?>

							<div class="col-12">

								<p class="dark text-left about-reflexiones module-text text-light"><strong>Finalización del curso:</strong> <?php echo $endTime;?></p>

							</div>

						<?php } ;?>

						<?php if($numberOfClasses != 0){ ?>

							<div class="col-12">

								<p class="dark text-left about-reflexiones module-text text-light"><strong>Cantidad de clases:</strong> <?php echo $numberOfClasses;?></p>

							</div>

						<?php } ;?>

						<?php if($showDuration != 0){ ?>

							<div class="col-12">

								<p class="dark text-left about-reflexiones module-text text-light"><strong>Duración de las clases:</strong> <?php echo $duration;?></p>

							</div>

						<?php } ;?>

						<?php if($showPrice != 0){ ?>

							<div class="col-12">

								<p class="dark text-left about-reflexiones module-text text-light"><strong>Precio del curso:</strong> <?php echo $price;?></p>

							</div>

						<?php } ;?>

					</div>

				<?php }; ?>

				<!-- contact -->

				<?php 

		// we should procceed printing this module if:
		// there is an email.
		// there is a phone.
		// there is an address.

				if($email != '' || $email != NULL || $phone != '' || $phone != NULL  || $whatsapp != '' || $whatsapp != NULL){
					?>

					<div class="row mt-5 course-contact-information">

						<div class="col-12 text-left">

							<h4 class="section-heading module-title course-contact-information-title">CONTACTO</h4>

							<hr class="left-separator">

						</div>

						<?php if($email != '' || $email != NULL){ ?>

							<div class="col-12">


								<p class="dark text-left about-reflexiones module-text text-light"><strong>Email: </strong> <a href="<?php echo $hrefMailString;?>"><?php echo $email;?></a></p>



							</div>

						<?php } ;?>

						<?php if($whatsapp != '' || $whatsapp != NULL){ ?>

							<div class="col-12">


								<p class="dark text-left about-reflexiones module-text text-light"><strong>Whatsapp: </strong> <a href="<?php echo $whatsappURL;?>"><?php echo $whatsapp;?></a></p>



							</div>

						<?php } ;?>

						<?php if($phone != '' || $phone != NULL){ ?>

							<div class="col-12">

								<p class="dark text-left about-reflexiones module-text text-light"><strong>Teléfono: </strong><a href="tel:<?php echo $phone;?>"><?php echo $phone;?></a></p>

							</div>

						<?php } ;?>


					</div>

				<?php } ?>

				<!-- info adicional -->

				<?php if($observations != '' || $observations != NULL){ ?>

					<div class="row mt-5">

						<div class="col-12 text-left">

							<h4 class="section-heading module-title">INFORMACIÓN ADICIONAL</h4>

							<hr class="left-separator">

						</div>

						<div class="col-11">

							<p class="dark text-left about-reflexiones module-text text-light"> <?php echo $observations;?></p>

						</div>

					</div>

				<?php } ;?>

				<?php if($queryTutors->count() != 0){ ;?>

					<div class="row mt-5">

						<div class="col-12 text-left">

							<h4 class="section-heading module-title">TUTORES</h4>

							<hr class="left-separator">

						</div>

					</div>

					<div class="row col-12">

						<div class="container">

							<div class="row">

								<?php 

								$count;

								foreach($queryTutors->results() as $tutor){ 

									$count+=1;

									$imageAlt = $tutor->name . $tutor->surname . ', tutor del curso ' . $courseName;

									if($tutor->uid == null){

										$imgPath = '../../private/img/templates/tutor-avatar/tutor-template-avatar.jpg';

										$imgPathRetina = '../../private/img/templates/tutor-avatar/tutor-template-avatar@2x.jpg';

									}else{

										$imgPath = '../../museumSmartAdmin/dist/default/private/sources/images/tutors/' . $tutor->mid . '/' . $tutor->uid . '/' . $tutor->mid . '_' . $tutor->uid . '_medium_sq.jpeg';

										$imgPathRetina = '../../museumSmartAdmin/dist/default/private/sources/images/tutors/' . $tutor->mid . '/' . $tutor->uid . '/' . $tutor->mid . '_' . $tutor->uid . '_medium_sq@2x.jpeg';

									}

									?>


									<div class="mt-5 pl-0 col-12 col-md-6 tutor-container">

										<div class="pl-0 pr-0 col-3 float-left">

											<img class="rounded-circle img-fluid mx-auto d-block hvr-grow" 
											
											src="<?php echo $imgPath;?>"

											srcset="<?php echo $imgPathRetina;?>"

											title="<?php echo $imageAlt;?>"

											alt="<?php echo $imageAlt;?>">

										</div>

										<div class="col-9 float-right text-sm-center text-left text-md-left text-lg-left">

											<small class="tutor-name text-bold"><?php echo $tutor->name . ' '  . $tutor->surname;?></small>

											<p class="pl-0 pr-0 tutor-description text-muted text-light text-up"><?php echo $tutor->bio;?>

											<?php if($tutor->email != null){ ?>

												<small class="text-bold text-center text-md-left text-lg-left">
													<a href='mailto:<?php echo $tutor->email;?>'>

														<i class="far fa-envelope-open mr-2"> </i><?php echo $tutor->email;?>

													</a>

												</small>

											<?php } ?>

											<?php if($tutor->phone != null){ ?>

												<br>

												<small class="text-bold text-center text-md-left text-lg-left">

													<a href='tel:<?php echo $tutor->phone;?>'>

														<i class="far fa-phone mr-2"></i>

														<?php echo $tutor->phone;?>

													</a>

												</small>

											<?php } ?>

											<?php if($tutor->whatsapp != null){ 

												$whatsappString = urlencode('Hola' . $tutor->name . '¿cómo estás? Te contacto para información respecto al curso *_' . $courseResult->name . '_*');

												$whatsappURL = 'https://wa.me/' . $tutor->whatsapp . '?text=' . $whatsappString;


												?>

												<br>

												<small class="text-bold text-center text-md-left text-lg-left">

													<a href='<?php echo $whatsappURL;?>'>

														<i class="fab fa-whatsapp mr-2"></i>

														<?php echo $tutor->whatsapp;?>

													</a>

												</small>

											<?php } ?>

										</p>

									</div>

								</div>	


							<?php } ?>

						</div>

					</div>

				</div>

			<?php } ;?>

			<!-- map! -->

			<?php if($courseResult->show_address && $courseResult->address != null ){ ?>

				<div class="row mt-5">

					<div class="col-12 text-left">

						<h4 class="section-heading module-title">DIRECCIÓN</h4>

						<hr class="left-separator">

					</div>

				</div>

				<div class="col-12 pl-0">

					

					<a target='_blank' href='<?php echo $mapURL;?>' class="dark text-left about-reflexiones module-text text-light"><?php echo $address;?></a>

				</div>

				<div class="form-group m-form__group row pt-4 map-container">

					<div class="col-12 input-group">

						<div class="" id="map"></div>	

						<input type="hidden" id="lat" name="lat" value="<?php echo $courseResult->lat?>">

						<input type="hidden" id="long" name="long" value="<?php echo $courseResult->long?>">

					</div>

				</div>

			<?php } ?>

			<!-- close map -->

			<?php 

			$imagesQuery = $db->query(

				'SELECT * from museum_images WHERE sid = ? AND source = ? AND active = ? AND deleted = ? ORDER BY internal_order',

				[$courseResult->mid, 'individual_course', 1, 0]);

			if($imagesQuery->count()>=1){

				?>

				<div class="row mt-5">

					<div class="col-12 text-left">

						<h4 class="section-heading module-title">IMÁGENES</h4>

						<hr class="left-separator">

					</div>

				</div>

				<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">

					<div class="slides"></div>

					<h3 class="title"></h3>

					<a class="prev">‹</a>

					<a class="next">›</a>

					<a class="close">×</a>

					<a class="play-pause"></a>

					<ol class="indicator"></ol>

				</div>

				<div id="links" class="row">

					<?php 

					foreach($imagesQuery->results() as $actualImg){ 

						$imageThumb = '../../museumSmartAdmin/dist/default/private/sources/images/courses/' . $courseResult->mid . '/images/' . $actualImg->unique_id . '/' . $courseResult->mid . '_' . $actualImg->unique_id . '_sq.jpeg';

						$imageThumbRetina = '../../museumSmartAdmin/dist/default/private/sources/images/courses/' . $courseResult->mid . '/images/' . $actualImg->unique_id . '/' . $courseResult->mid . '_' . $actualImg->unique_id . '_sq@2x.jpeg';

						$imageRetina = '../../museumSmartAdmin/dist/default/private/sources/images/courses/' . $courseResult->mid . '/images/' . $actualImg->unique_id . '/' . $courseResult->mid . '_' . $actualImg->unique_id . '_original@2x.jpeg';

						$caption = $actualImg->description;

						if($caption == null) $caption = 'imagen del curso ' . $courseName;

						?>
						<div class="col-12 col-md-4 col-lg-3 mt-5 thumbnail-container">

							<a href="<?php echo $imageRetina;?>" title="<?php echo $caption;?>">

								<img class="img-fluid hvr-grow" 

								src="<?php echo $imageThumb;?>" 

								srcset="<?php echo $imageThumbRetina;?>" 

								alt="<?php echo $caption;?>">

							</a>

						</div>

					<?php } ?>

				</div>

			<?php } ?> 

			<?php if($totalVideos>=1){

			?>

			<div class="row mt-5">

				<div class="col-12 text-left">

					<h4 class="section-heading module-title">VIDEOS</h4>

					<hr class="left-separator">

				</div>

			</div>

			<div id="blueimp-video-carousel" class="blueimp-gallery blueimp-gallery-controls blueimp-gallery-carousel">

				<div class="slides"></div>

				<h3 class="title"></h3>

				<a class="prev">‹</a>

				<a class="next">›</a>

				<a class="play-pause"></a>

			</div>

			<div id="video-links" class="row">

			</div>

		<?php } ?>


		<?php if($allowEnrollment == 1){ ?>

			<div class="row mt-5">

				<div class="col-12 text-left">

					<h4 class="section-heading module-title">PRE INSCRIPCIÓN</h4>

					<hr class="left-separator">

				</div>

				<div class="col-8">

					<p class="dark text-left about-reflexiones module-text text-light"> 

						Podes preinscribirte al curso haciendo <a href="#" role="button" class ="
						inscription-to-course">click aquí</a>.

					</p>


				</div>

			</div>

		<?php } ;?>

		<?php if($shareCourse){ ;?>

			<div class="mt-5 mb-5 addthis_inline_share_toolbox_f3qw"></div>

		<?php }?>

	</div>

	<div class="general-sidebar col-3 d-none d-md-block">

		<div class="text-left mt-5">

			<h5 class="mb-5 section-heading module-title text-center">ÚLTIMOS CURSOS</h5>

		</div>

		<div class="col-12">

			<?php

			$coursesQuery = DB::getInstance()->query('

				SELECT *, mc.id as mid, img.unique_id 		as uid 

				FROM museum_courses 						as mc

				LEFT JOIN museum_images 					as img 		

				ON mc.id = img.sid AND img.source 			= ?

				WHERE mc.deleted = ?

				ORDER BY mid DESC LIMIT 5', 

				(array('courses', 0)));

			foreach($coursesQuery->results() as $course){

				if($course->mid == $courseResult->mid) continue;

				$courseName = $course->name;

				$courseURL = '../' . $course->url;

				$courseId = $course->id;

						//image...

				if($course->uid == null){

					$myImg = '../../private/img/templates/image-template/template_medium.jpg';
					
					$myImgRetina = '../../private/img/templates/image-template/template_medium@2x.jpg';

				}else{

					$myImg = '../../museumSmartAdmin/dist/default/private/sources/images/courses/' . $course->mid . '/' . $course->uid . '/' . $course->mid . '_' . $course->uid . '_medium.jpeg';

					$myImgRetina = '../../museumSmartAdmin/dist/default/private/sources/images/courses/' . $course->mid . '/' . $course->uid . '/' . $course->mid . '_' . $course->uid . '_medium@2x.jpeg';
				}

				?>

				<a class="" href="<?php echo $courseURL;?>">

					<div class="hover ehover12 general-sidebar-thumb" style="height: auto !important">

						<img class="img-fluid" 

						src="<?php echo $myImg;?>" 

						src="<?php echo $myImgRetina;?>" 

						alt="<?php echo $courseName;?>"

						title="<?php echo $courseName;?>">

						<div class="overlay lateral-item-overlay">

							<h2 class="font-light course-lateral-name" style="padding: 0 !important;"><?php echo $courseName;?> </h2>

						</div>

					</div>

				</a>

			<?php } ?>

		</div>

	</div>

</div>

</div>