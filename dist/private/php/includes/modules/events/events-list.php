<div class="page-body-content">

	<div class="offset-1 col-10 offset-md-2 col-md-8 text-center mt-5 mb-5">

		<h4 class="section-heading  text-medium"><?php echo $wording->get('events_first_line');?></h4>

		<p class="section-description dark mt-5">

			<span class="text-light black">
				<?php echo $wording->get('events_second_line');?>
			</span>
		</p>

	</div>

	<div class="full-container mt-5">

		<div id="" class='wrapper text-center'>

			<div id="filters" class="btn-group-justified button-group btn-group-sm btn-square filters-button-group mb-5">  

				<button class="filter-button btn button" data-filter="*">TODOS</button>

				<button class="filter-button btn button" data-filter=".is_previous">PASADOS</button>

				<button class="filter-button btn button" data-filter=".is_in_progress">EN CURSO</button>
				<button class="filter-button btn button" data-filter=".is_next">PRÓXIMOS</button>

				<button class="filter-button btn button" data-filter=".is_highlighted">DESTACADOS!</button>

			</div>

		</div>

	</div>

	<div class="container mt-5">

		<div class="grid row">

			<?php

			$contenetQuery = DB::getInstance()->query('

				SELECT *, mc.id 							as mid, 

				mc.description 								as event_description,

				img.unique_id 								as uid 

				FROM museum_events 							as mc

				LEFT JOIN museum_short_urls 				as short

				ON mc.id = short.sid and short.source = 	?

				LEFT JOIN museum_images 					as img 		

				ON mc.id = img.sid AND img.source 			= ?

				AND img.deleted 							= ?

				WHERE mc.deleted 							= ? 

				AND mc.active 								= ? 

				ORDER by mc.start_date dESC', 

				(array('events', 'events', 0, 0, 1)));

			$count = 0; 

			foreach($contenetQuery->results() as $item){ 

				$isHighlighted = $item->is_highlighted;

				$count+=1;

				$itemName = $item->name;

				$itemURL = $item->url;

				$itemId = $item->id;

				//$start = utf8_encode(strftime("%A, %d de %B de %Y a las %H:%M", strtotime($item->start_date)));
				
				$start = utf8_encode(strftime("%A, %d de %B de %Y", strtotime($item->start_date)));
				
				$startTime = utf8_encode(strftime("%H:%M", strtotime($item->start_date)));

				$monthString = substr(utf8_encode(strftime("%B", strtotime($item->start_date))), 0, 3) . '.';

				$dayNumber = utf8_encode(strftime("%d", strtotime($item->start_date)));

				$date = $start;

				$description = $item->event_description;

				$url = $item->short_url;//'https://www.museodelholocausto.org.ar/eventos/' . $itemURL;

				$twitterShare = 'Mirá este evento del Museo Del Holocausto! ' . $url;

				$whatsappString = urlencode('Hola, ¿cómo estás? Mirá este evento del Museo Del Holocausto de Buenos Aires: *_' . $itemName . '_*. Vas a poder encontrar más información  en ' . $url);

				$whatsappURL = 'https://wa.me/?text=' . $whatsappString;

				$filters = '';

				$now = new DateTime();

				$checkStartDateTime = new DateTime($item->start_date);

				$endDate = new DateTime($item->end_date); 

				//if start date time is < than now, and end date too, is previous.

				if($checkStartDateTime < $now && $endDate < $now)  $filters .= 'is_previous';

				//if start date time is > than now is next.

				if($checkStartDateTime > $now)  $filters .= ' is_next';

				//if start is < than now and now is < than end, it is in progress.

				if($checkStartDateTime < $now && $now < $endDate) $filters .= ' is_in_progress';

				if($item->uid == null){

					$myImg = '../private/img/templates/image-template/template_medium.jpg';

					$myImgRetina = '../private/img/templates/image-template/template_medium@2x.jpg';

				}else{

					$myImg = '../museumSmartAdmin/dist/default/private/sources/images/events/' . $item->mid . '/' . $item->uid . '/' . $item->mid . '_' . $item->uid . '_medium.jpeg';

					$myImgRetina = '../museumSmartAdmin/dist/default/private/sources/images/events/' . $item->mid . '/' . $item->uid . '/' . $item->mid . '_' . $item->uid . '_medium@2x.jpeg';
				}

				$myErrorImage = '../private/img/templates/image-template/template_medium.jpg';

				$myErrorImageRetina = '../private/img/templates/image-template/template_medium@2x.jpg';

				$destacado = '../private/img/templates/destacado-template/destacado.png';

				$destacadoRetina = '../private/img/templates/destacado-template/destacado@2x.png';

				if($isHighlighted == 1) $filters .= ' is_highlighted'; 

				$shadow = '../../private/img/templates/comunicado-template/shadow.png';

				?>

				<div class="event-item col-lg-4 col-md-6 col-12 pr-1 pl-1 <?php echo $filters;?>">

					<div class="event-main-image-container image-container mr-2 ml-2 mb-4">

						<div class="the-super-date col-2 hvr-grow">

							<div class="numbers-container">

								<p class="mb-0 the-number date-number-container text-book white text-center"><?php echo $dayNumber;?></p>

								<p class="text-uppercase date-number-container text-medium white text-center"><?php echo $monthString;?></p>

							</div>


						</div>


						<a href="<?php echo $itemURL;?>">

							<img class="event-image img-fluid" 

							src="<?php echo $myImg;?>"

							srcset="<?php echo $myImgRetina;?>" 

							alt="<?php echo $itemName;?>"

							title="<?php echo $itemName;?>">

							<img class="img-shadow position-absolute img-fluid"  src="<?php echo $shadow;?>" 

							srcset ="<?php echo $shadow;?>" >

						</a>

						<div class="row">

							<div class="event-text-content position-absolute col-10 offset-1 pl-4 pr-0">

								<h2 class="white event-title text-uppercase"><?php echo $itemName;?></h2>

								<p class="event-date-second-line white text-light mb-1"><i class="fal fa-calendar"></i><?php echo '  ' . $date;?><i class="fal fa-clock ml-2"></i><?php echo '  ' . $startTime;?></p>

								<ul class="list-inline event-sharing-content">

									<li class='list-inline-item'>

										<!--<i class="fab fa-facebook"></i>-->

										<a data-toggle="tooltip" data-placement="bottom" title="Hacé click para compartir el evento en Facebook!" class="facebook-share-button" target="_blank" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=300')" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $url;?>">

											<i class="fab fa-facebook"></i>

										</a>

									</li>

									<li class='list-inline-item'>

										<a data-toggle="tooltip" data-placement="bottom" title="Hacé click para compartir el evento por Whatsapp!" class="whatsapp-share-button" href="<?php echo $whatsappURL;?>" data-size="large">

											<i class="fab fa-whatsapp"></i>

										</a>

									</li>

									<li class='list-inline-item'>

										<a data-toggle="tooltip" data-placement="bottom" title="Hacé click para compartir el evento en Twitter!" class="twitter-share-button" href="https://twitter.com/intent/tweet?text=<?php echo $twitterShare;?>" data-size="large">

											<i class="fab fa-twitter"></i>

										</a>

									</li>


								</ul>

								<div class="go-to-event-button mt-4">

									<a href="<?php echo $itemURL;?>" role="button" class="read-event-btn btn button">IR AL EVENTO</a>

								</div>

							</div>

						</div>

						


						

					</div>

				</div>




			<?php } ?>

		</div>

	</div>

	<div id="no-results" class="container d-none">

		<h4 class="mb-5 text-uppercase text-medium no-result-msg text-center">No se encontraron resultados para este filtro</h4>

	</div>

</div>

