<?php

if($itemResult->start_date == '0000-00-00' || $itemResult->end_date == '0000-00-00' || 

	$itemResult->start_date == NULL || $itemResult->end_date == NULL){

	$endTime = '';

$initTime = '';

}else{

	setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

	$initTime = utf8_encode(strftime("%A, %d de %B del %Y", strtotime($itemResult->start_date)));

	$endTime = utf8_encode(strftime("%A, %d de %B del %Y", strtotime($itemResult->end_date)));

}

$eventName = $itemResult->name;

$isFree = $itemResult->is_free;

$email = $itemResult->email;

$phone = $itemResult->phone;

$address = $itemResult->address;

$observations = $itemResult->observations;

$shareEvent = $itemResult->allow_share;

$allowInscription = $itemResult->is_with_inscription;

$emailSubject = 'Información del evento ' . $itemResult->name;

$emailBody = 'Hola, te escribo para solicitar más información respecto al evento: ' . $itemResult->name;

$hrefMailString = 'mailto:' . $email . '?subject=' . $emailSubject . '&body=' . $emailBody;

$whatsapp = $itemResult->whatsapp;

$whatsappString = urlencode('Hola, ¿cómo estás? Te contacto para soslicitar información adicional respecto al evento *_' . $itemResult->name . '_*');

$whatsappURL = 'https://wa.me/' . $itemResult->whatsapp . '?text=' . $whatsappString;

$description = $itemResult->event_description;

if($itemResult->show_address){

	$mapURL = 'https://www.google.com/maps/search/?api=1&query=' . $itemResult->lat .  ','. $itemResult->long;
}

$videosQuery = $db->query('SELECT * FROM museum_youtube_videos WHERE sid = ? AND source = ? AND active = ? AND deleted = ?', [$itemResult->mid, 'event_video', 1, 0]);

$totalVideos = $videosQuery->count();


?>

<div class="container">

	<input type="hidden" id="total-videos" name="total-videos" value="<?php echo $totalVideos;?>">

	<div class="row">

		<div class="col-md-9 col-12">

			<div class="row mt-5">

				<div class="col-12 text-left">

					<h4 class="section-heading module-title">SOBRE EL EVENTO:</h4>

					<hr class="left-separator">

				</div>

				<div class="col-11">

					<p class="dark text-left about-reflexiones module-text text-light"> <?php echo $description;?></p>

				</div>

			</div>

			<?php

			//we should proceed with this module if:
			// - there is an init time
			// - there is an end time
			// - the number of classes was setted.
			// - the price should be shown

			if($initTime != '' || $endTime != '' || $isFree != 0){ ?>

				<div class="row mt-5">

					<div class="col-12 text-left">

						<h4 class="section-heading module-title">INFORMACIÓN</h4>

						<hr class="left-separator">

					</div>

					<?php if($initTime != ''){ ?>

						<div class="col-12">

							<p class="dark text-left about-reflexiones module-text text-light"><strong>Comienzo:</strong> <?php echo $initTime;?></p>

						</div>

					<?php } ;?>

					<?php if($endTime != ''){ ?>

						<div class="col-12">

							<p class="dark text-left about-reflexiones module-text text-light"><strong>Finalización:</strong> <?php echo $endTime;?></p>

						</div>

					<?php } ;?>

					<div class="col-12">

						<p class="dark text-left about-reflexiones module-text text-light"><strong>Gratuito:</strong> <?php echo ($isFree) ? 'si' : 'no';?></p>

					</div>

				</div>

			<?php }; ?>

			<!-- contact -->

			<?php 

		// we should procceed printing this module if:
		// there is an email.
		// there is a phone.
		// there is an address.

			if($email != '' || $email != NULL || $phone != '' || $phone != NULL  || $whatsapp != '' || $whatsapp != NULL){
				?>

				<div class="row mt-5">

					<div class="col-12 text-left">

						<h4 class="section-heading module-title">CONTACTO</h4>

						<hr class="left-separator">

					</div>

					<?php if($email != '' || $email != NULL){ ?>

						<div class="col-12">


							<p class="dark text-left about-reflexiones module-text text-light"><strong>Email: </strong> <a href="<?php echo $hrefMailString;?>"><?php echo $email;?></a></p>



						</div>

					<?php } ;?>

					<?php if($whatsapp != '' || $whatsapp != NULL){ ?>

						<div class="col-12">


							<p class="dark text-left about-reflexiones module-text text-light"><strong>WhatsApp: </strong> <a href="<?php echo $whatsappURL;?>"><?php echo $whatsapp;?></a></p>



						</div>

					<?php } ;?>

					<?php if($phone != '' || $phone != NULL){ ?>

						<div class="col-12">

							<p class="dark text-left about-reflexiones module-text text-light"><strong>Teléfono: </strong><a href="tel:<?php echo $phone;?>"><?php echo $phone;?></a></p>

						</div>

					<?php } ;?>


				</div>

			<?php } ?>

			<!-- info adicional -->

			<?php if($observations != '' && $observations != NULL && $observations != '<br>'){ ?>

				<div class="row mt-5">

					<div class="col-12 text-left">

						<h4 class="section-heading module-title">INFORMACIÓN ADICIONAL</h4>

						<hr class="left-separator">

					</div>

					<div class="col-11">

						<p class="dark text-left about-reflexiones module-text text-light"> <?php echo $observations;?></p>

					</div>

				</div>

			<?php } ;?>


			<!-- map! -->

			<?php if($itemResult->show_address && $itemResult->address != null ){ ?>

				<div class="row mt-5">

					<div class="col-12 text-left">

						<h4 class="section-heading module-title">DIRECCIÓN</h4>

						<hr class="left-separator">

					</div>

				</div>

				<div class="col-12 pl-0">



					<a target='_blank' href='<?php echo $mapURL;?>' class="dark text-left about-reflexiones module-text text-light"><?php echo $address;?></a>

				</div>

				<div class="form-group m-form__group row pt-4 map-container">

					<div class="col-12 input-group">

						<div class="" id="map"></div>	

						<input type="hidden" id="lat" name="lat" value="<?php echo $itemResult->lat?>">

						<input type="hidden" id="long" name="long" value="<?php echo $itemResult->long?>">

					</div>

				</div>

			<?php } ?>

			<!-- close map -->

			<?php 

			$imagesQuery = $db->query(

				'SELECT * from museum_images WHERE sid = ? AND source = ? AND active = ? AND deleted = ? ORDER BY internal_order',

				[$itemResult->mid, 'individual_event', 1, 0]);

			if($imagesQuery->count()>=1){

				?>

				<div class="row mt-5">

					<div class="col-12 text-left">

						<h4 class="section-heading module-title">IMÁGENES</h4>

						<hr class="left-separator">

					</div>

				</div>

				<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">

					<div class="slides"></div>

					<h3 class="title"></h3>

					<a class="prev">‹</a>

					<a class="next">›</a>

					<a class="close">×</a>

					<a class="play-pause"></a>

					<ol class="indicator"></ol>

				</div>

				<div id="links" class="row">

					<?php 

					foreach($imagesQuery->results() as $actualImg){ 

						$imageThumb = '../../museumSmartAdmin/dist/default/private/sources/images/events/' . $itemResult->mid . '/images/' . $actualImg->unique_id . '/' . $itemResult->mid . '_' . $actualImg->unique_id . '_sq.jpeg';

						$imageThumbRetina = '../../museumSmartAdmin/dist/default/private/sources/images/events/' . $itemResult->mid . '/images/' . $actualImg->unique_id . '/' . $itemResult->mid . '_' . $actualImg->unique_id . '_sq@2x.jpeg';

						$imageRetina = '../../museumSmartAdmin/dist/default/private/sources/images/events/' . $itemResult->mid . '/images/' . $actualImg->unique_id . '/' . $itemResult->mid . '_' . $actualImg->unique_id . '_original@2x.jpeg';

						$caption = $actualImg->description;

						if($caption == null) $caption = 'imagen del evento ' . $eventName;

						?>
						<div class="col-12 col-md-4 col-lg-3 mt-5 thumbnail-container">

							<a href="<?php echo $imageRetina;?>" title="<?php echo $caption;?>">

								<img class="img-fluid hvr-grow" 

								src="<?php echo $imageThumb;?>" 

								srcset="<?php echo $imageThumbRetina;?>" 

								alt="<?php echo $caption;?>">

							</a>

						</div>

					<?php } ?>

				</div>

			<?php } ?> 

			<?php if($totalVideos>=1){

				?>

				<div class="row mt-5">

					<div class="col-12 text-left">

						<h4 class="section-heading module-title">VIDEOS</h4>

						<hr class="left-separator">

					</div>

				</div>

				<div id="blueimp-video-carousel" class="blueimp-gallery blueimp-gallery-controls blueimp-gallery-carousel">

					<div class="slides"></div>

					<h3 class="title"></h3>

					<a class="prev">‹</a>

					<a class="next">›</a>

					<a class="play-pause"></a>

				</div>

				<div id="video-links" class="row">

				</div>

			<?php } ?>


			<?php if($allowInscription == 1){ ?>

				<div class="row mt-5">

					<div class="col-12 text-left">

						<h4 class="section-heading module-title">PRE INSCRIPCIÓN</h4>

						<hr class="left-separator">

					</div>

					<div class="col-8">

						<p class="dark text-left about-reflexiones module-text text-light"> 

							Podes preinscribirte al evento haciendo <a href="#" role="button" class ="
							inscription-to-event">click aquí</a>.

						</p>


					</div>

				</div>

			<?php } ;?>

			<?php if($shareEvent){ ;?>

				<div class="mt-5 mb-5 addthis_inline_share_toolbox_f3qw"></div>

			<?php }?>

		</div>

		<div class="general-sidebar events-sidebar col-3 d-none d-md-block">

			<div class="text-left mt-5">

				<h5 class="mb-5 section-heading module-title text-center">OTROS EVENTOS</h5>

			</div>

			<div class="col-12">

				<?php

				$eventsQuery = DB::getInstance()->query('

					SELECT *, mc.id as mid, img.unique_id 		as uid 

					FROM museum_events 							as mc

					LEFT JOIN museum_images 					as img 		

					ON mc.id = img.sid AND img.source 			= ?

					WHERE mc.deleted = ?

					ORDER BY mid DESC LIMIT 5', 

					(array('events', 0)));

				foreach($eventsQuery->results() as $event){

					if($event->mid == $itemResult->mid) continue;

					$eventName = $event->name;

					$eventURL = '../' . $event->url;

					$eventId = $event->id;

						//image...

					if($event->uid == null){

						$myImg = '../../private/img/templates/image-template/template_medium.jpg';

						$myImgRetina = '../../private/img/templates/image-template/template_medium@2x.jpg';

					}else{

						$myImg = '../../museumSmartAdmin/dist/default/private/sources/images/events/' . $event->mid . '/' . $event->uid . '/' . $event->mid . '_' . $event->uid . '_medium.jpeg';

						$myImgRetina = '../../museumSmartAdmin/dist/default/private/sources/images/events/' . $event->mid . '/' . $event->uid . '/' . $event->mid . '_' . $event->uid . '_medium@2x.jpeg';
					}

					?>

					<a href="<?php echo $eventURL;?>">

						<div class="hover ehover12 general-sidebar-thumb" style="height: auto !important">

							<img class="img-fluid" 

							src="<?php echo $myImg;?>" 

							src="<?php echo $myImgRetina;?>" 

							alt="<?php echo $eventName;?>"

							title="<?php echo $eventName;?>">

							<div class="overlay lateral-item-overlay">

								<h2 class="font-light text-truncate" style="padding: 0 !important;"><?php echo $eventName;?> </h2>

							</div>

						</div>

					</a>

				<?php } ?>

			</div>

		</div>

	</div>

</div>