<div class="mt-5 page-body-content">

	<div class="offset-1 col-10 offset-md-2 col-md-8 text-center mt-5">

		<h4 class="section-heading text-medium"><?php echo $wording->get('exhibitions_first_line');
;?></h4>

		<p class="section-description dark mt-5">

			<span class="text-light black">
				<?php echo $wording->get('exhibitions_second_line');
;?>
			</span>
		</p>

	</div>

	<div class="full-container mt-5">

		<div id="" class='wrapper text-center'>

			<div id="filters" class="btn-group-justified button-group btn-group-sm  filters-button-group mb-5">  

				<button class="mb-1 filter-button btn  button" data-filter="*">TODAS</button>

				<?php

				$exhibitionsQuery = DB::getInstance()->query('SELECT * FROM museum_exhibitions_types WHERE active= ? AND deleted = ?', [1, 0]);

				foreach($exhibitionsQuery->results() as $exhibitionMenu){   ?>

					<button class="mb-1 filter-button btn button" data-filter="<?php echo '.' . $exhibitionMenu->type_tag;?>"><?php echo $exhibitionMenu->type;?></button>

				<?php }

				?>
				
				<button class="mb-1 filter-button btn button" data-filter=".next">EN CURSO</button>
				
			</div>

		</div>

	</div>	

	<div class="container">

		<div class="grid">

			<?php

			$contenetQuery = DB::getInstance()->query('

				SELECT *, mc.id 							as mid, 

				mc.description 								as event_description,

				img.unique_id 								as uid 

				FROM museum_exhibitions   					as mc

				LEFT JOIN museum_short_urls 				as short

				ON mc.id = short.sid and short.source = 	?

				LEFT JOIN museum_images 					as img 		

				ON mc.id = img.sid AND img.source 			= ?

				AND img.deleted 							= ?

				WHERE mc.deleted 							= ? 

				AND mc.active 								= ? 

				ORDER by mid dESC', 

				(array('exhibitions', 'exhibitions', 0, 0, 1)));

			$count = 0; 

			foreach($contenetQuery->results() as $exhibition){ 

				$filters = '';

				$exhibitionName = $exhibition->name;

				$exhibitionURL = $exhibition->url;

				$exhibitionId = $exhibition->id;

				//image...

				if($exhibition->uid == null){

					$myImg = '../private/img/templates/image-template/template_medium.jpg';

					$myImgRetina = '../private/img/templates/image-template/template_medium@2x.jpg';

				}else{

					$myImg = '../museumSmartAdmin/dist/default/private/sources/images/exhibitions/' . $exhibition->mid . '/' . $exhibition->uid . '/' . $exhibition->mid . '_' . $exhibition->uid . '_medium.jpeg';

					$myImgRetina = '../museumSmartAdmin/dist/default/private/sources/images/exhibitions/' . $exhibition->mid . '/' . $exhibition->uid . '/' . $exhibition->mid . '_' . $exhibition->uid . '_medium@2x.jpeg';
				}

				$maestros = '../private/img/templates/escuela-de-maestros-template/banner-template.png';

				$maestrosRetina = '../private/img/templates/escuela-de-maestros-template/banner-template@2x.png';

				$shadow = '../private/img/templates/comunicado-template/shadow.png';


				//data...

				if($exhibition->start_date == '0000-00-00' || $exhibition->end_date == '0000-00-00' || $exhibition->start_date == NULL || $exhibition->end_date == NULL){

					$finalTime = '';

				}else{

					setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

					$initTime = strftime("del %A, %d de %B del %Y", strtotime($exhibition->start_date));

					$endTime = strftime("al %A, %d de %B del %Y", strtotime($exhibition->end_date));

					$finalTime = utf8_encode($initTime . ' ' . $endTime);

					$now = date('Y-m-d');

					
				}

				if($exhibition->is_current == '1') { 

					$filters.= 'next' . ' '; 

				}else{
					
					$finalTime = 'MUESTRA FINALIZADA';
				}


				//tags...

				$school = false;

				$filterQuery = DB::getInstance()->query(

					'SELECT * FROM museum_exhibitions_types_relations as metr 

					INNER JOIN museum_exhibitions_types as met ON metr.id_exhibition_type = met.id 

					WHERE metr.id_exhibition = ?', [$exhibition->mid]);

				foreach($filterQuery->results() as $filter){

					//if($filter->modality_tag == 'school') $school = true;

					$filters .= $filter->type_tag . ' ';

				}

				?>

				<div class="<?php echo $filters;?> col-lg-6 col-md-6 col-12 news-item news-list-container pr-1 pl-1">

					<div class="image-container mr-2 ml-2 mb-4">

						<a href="<?php echo $exhibitionURL;?>">

							<img class="news-image hvr-grow img-fluid" 

							src="<?php echo $myImg;?>"

							srcset="<?php echo $myImgRetina;?>" 

							alt="<?php echo $exhibitionName;?>"

							title="<?php echo $exhibitionName;?>">

							<img class="img-shadow position-absolute img-fluid"  src="<?php echo $shadow;?>" 

							srcset ="<?php echo $shadow;?>" >

						</a>

						<h2 class="white text-truncate news-title position-absolute text-uppercase mt-xl-5 mt-md-4"><?php echo $exhibitionName;?></h2>

						<a href="<?php echo $exhibitionURL;?>" role="button" class="read-btn btn button">VER EXHIBICIÓN</a>

					</div>

				</div>

				

			<?php } ?>

		</div>

	</div>

	<div id="no-results" class="container d-none">

		<h4 class="mb-5 text-uppercase text-medium no-result-msg text-center">No se encontraron resultados para este filtro</h4>

	</div>

</div>

