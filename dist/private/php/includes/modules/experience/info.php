<?php
?>

<section id="services" class="visitas-grupales visitas-individuales">

</div>

</div>

    <div class="container  mt-0 pb-5">
  
      <div class="row">

        <div class="offset-3 col-6 col-6 text-center sr-icon-1">

          <div class="service-box  mx-auto ">

            <span class="fa-stack fa-2x center" aria-hidden="true">

             <i class="fal fa-circle fa-stack-2x" aria-hidden="true"></i>

             <i class="fas fa-globe fa-stack-1x" aria-hidden="true"></i>


           </span>

           <h4 class="mt-3 mb-3 text-medium">UN POCO DEL MUSEO 360°</h4>

           <p class="text-muted mb-0 text-light text-up">Conocé cómo es la experiencia virtual de la exhibición permanente.

.<br>Recomendamos el  uso de <strong>Google Chrome</strong> y una conexión a <strong>WiFi</strong> para tener una mejor experiencia de navegación.</p>

         </div>

         <br>

         <a href="https://www.museodelholocausto.org.ar/360" class="generic-action-button btn button go-to-individual-visit">

          <i class="fas fa-hand-pointer mr-2"></i>INGRESAR</a>

        </div>

        </div>

  </div>


<div class="container mt-5">

  <div class="row">

    <div class="col-lg-12 text-center">

      <h2 class="section-heading module-title">RECORRIDO VIRTUAL 360°</h2>

      <hr class="module-separator">

    </div>

    <div class="col-lg-8 mx-auto">

      <p class="text-light text-faded mb-5 text-center about-reflexiones module-text">El Recorrido Virtual 360° es una propuesta digital que permite visitar de forma remota la totalidad de la exhibición permanente del Museo del Holocausto de Buenos Aires.</p>

      <p class="text-light text-faded mb-5 text-center about-reflexiones module-text">Esta propuesta atraviesa todas las fronteras físicas y permite que todas las personas y grupos con conexión a Internet puedan tener la experiencia de visitar el Museo desde sus dispositivos, ya sea acompañado por un guía profesional a través de la plataforma Zoom, o de forma individual.</p>

      <p class="text-light text-faded mb-5 text-center about-reflexiones module-text">El Museo ofrece una visita guiada para el público general y otra para las instituciones educativas que deseen realizar el recorrido con el equipo de guías del Museo.</p>

    </div>

  </div>

</div>

</section>




