<section id="shoa-timeline" class="cronologia-de-la-shoa">

	<div class="container">

		<div class="row">

			<div class="col-lg-12 text-center">

				<h2 class="section-heading text-uppercase">ACCESOS POR SECCIÓN AL MUSEO</h2>

				<p class="col-8 offset-2 section-subheading text-muted text-light">Este material especialmente preparado por el Prof. Abraham Huberman z´l. destaca acontecimientos destacados ocurridos durante los años 1933 y 1945.</p>

			</div>

		</div>

		<div class="row">

			<div class="col-lg-12">

				<ul class="timeline">

					<li>

						<div class="timeline-image">

							<img class="rounded-circle img-fluid" src="../private/img/shoa/timeline/1.jpg" srcset="../private/img/shoa/timeline/1@2x.jpg" alt="Ascenso de Hitler Al Poder" title="Ascenso de Hitler Al Poder">

						</div>

						<div class="timeline-panel">

							<div class="timeline-heading">

								<h4 class="text-bold">1933</h4>

							</div>

							<div class="timeline-body">

								<div class="text-muted">

									<p class="text-medium text-uppercase">3O DE ENERO - Ascenso de Hitler al poder</p>

									<p class="text-light gray-900">El Canciller Hindenburg designa a Hitler como Canciller (primer ministro) de un gobierno de coalición formado por grupos nazis, conservadores y católicos. 
										En ese momento había en Alemania 566.000 judíos. Los nazis habían obtenido el 37% de los votos. Los social- demócratas y comunistas representaban un porcentaje semejante pero jamás formaron una alianza.

									</p>

								</div>

								<div class="text-muted">

									<p class="text-medium text-uppercase">27 DE FEBRERO - Incendio del Reichstag</p>

									<p class="text-light gray-900">Los comunistas son declarados culpables. Se suspenden las garantías constitucionales. Comienzan a desaparecer los rasgos democráticos del Estado alemán.

									</p>

								</div>

								<div class="text-muted">

									<p class="text-medium text-uppercase">20 DE MARZO</p>

									<p class="text-light gray-900">Se inaugura Dachau, el primer campo de concentración. Al día siguiente comienzan a llegar los primeros internados, definidos como “asociales”.
									</p>

								</div>

								<div class="text-muted">

									<p class="text-medium text-uppercase">24 DE MARZO</p>

									<p class="text-light gray-900">Hitler recibe poderes extraordinarios que le permiten gobernar sin tomar en cuenta la Constitución ni el Parlamento. Debían regir por cinco años y podían ser renovados indefinidamente.
									</p>

								</div>

								<div class="text-muted">

									<p class="text-medium text-uppercase">1 DE ABRIL</p>

									<p class="text-light gray-900">Día de boicot nacional contra los negocios de propiedad judía en Alemania.
									</p>

								</div>

								<div class="text-muted">

									<p class="text-medium text-uppercase">21 DE ABRIL</p>

									<p class="text-light gray-900">Se prohíbe la faena ritual judía.
									</p>

								</div>

								<div class="text-muted">

									<p class="text-medium text-uppercase">10 DE MAYO</p>

									<p class="text-light gray-900">Quema de libros cuyo contenido atenta contra el “espíritu alemán” y cuyos autores son judíos y no judíos.
									</p>

								</div>

								<div class="text-muted">

									<p class="text-medium text-uppercase">17 DE MAYO</p>

									<p class="text-light gray-900">Un judío logra que su protesta por la discriminación implantada en Silesia sea aceptada en la Liga de las Naciones. Alemania acepta la intervención de la Liga de las Naciones y elimina la discriminación contra los judíos en ese territorio.
									</p>

								</div>

							</div>

						</div>

					</li>

					<li class="timeline-inverted">

						<div class="timeline-image">

							<img class="rounded-circle img-fluid" src="../private/img/shoa/timeline/2.jpg" srcset="../private/img/shoa/timeline/2@2x.jpg" alt="La Noche de los cuchillos largos" title="La Noche de los cuchillos largos">

						</div>

						<div class="timeline-panel">

							<div class="timeline-heading">

								<h4>1934</h4>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">30 DE JUNIO</p>

								<p class="text-light gray-900">En la llamada “Noche de los cuchillos largos” es eliminado el sector más radical del NSDAP, encabezado enrte otros por Ernst Rohem. Hitler queda como único líder del partido.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">2 DE AGOSTO</p>

								<p class="text-light gray-900">Fallece el presidente Hindenburg y Hitler se convierte en presidente, conservando el cargo de Canciller.
								</p>

							</div>


						</div>

					</li>

					<li>

						<div class="timeline-image">

							<img class="rounded-circle img-fluid" src="../private/img/shoa/timeline/3.jpg" srcset="../private/img/shoa/timeline/3@2x.jpg" alt="Las leyes de Nuremberg" title="Las leyes de Nuremberg">


						</div>

						<div class="timeline-panel">

							<div class="timeline-heading">

								<h4>1935</h4>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">16 DE MARZO</p>

								<p class="text-light gray-900">Alemania instaura el servicio militar obligatorio en abierta oposición al Tratado de Versalles.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">15 DE SEPTIEMBRE</p>

								<p class="text-light gray-900">Se promulgan las llamadas “Leyes de Nuremberg”, que excluyen definitivamente a los judíos de todo aspecto de la vida alemana. Las leyes son dos, “Ley de Ciudadanía del Reich” y “Ley de Defensa y Protección de la Sangre y el Honor alemanes”
								</p>

							</div>

						</div>

					</li>

					<li class="timeline-inverted">

						<div class="timeline-image">

							<img class="rounded-circle img-fluid" src="../private/img/shoa/timeline/4.jpg" srcset="../private/img/shoa/timeline/4@2x.jpg" alt="Hitler durante los Juegos Olímpicos de Berlín" title="Hitler durante los Juegos Olímpicos de Berlín">

						</div>

						<div class="timeline-panel">

							<div class="timeline-heading">

								<h4>1936</h4>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">7 de MARZO</p>

								<p class="text-light gray-900">El ejército alemán ingresa en la región del Rhin que debía permanecer desmilitarizada, violando así el acuerdo de Locarno y sin que se produzcan reacciones por parte de las grandes potencias. Sangriento pogrom contra los judíos de Przitik, Polonia.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">5 DE MAYO</p>

								<p class="text-light gray-900">Finaliza la guerra en Etiopía con la conquista italiana de Addis-Abeba. Esto demostró la impotencia de la Liga de las Naciones para detener la política agresiva de los países totalitarios.


								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">1 DE AGOSTO</p>

								<p class="text-light gray-900">Comienzan los Juegos Olímpicos en Berlín. Toda propaganda antijudía había sido eliminada. La decisión de realizar los juegos en Alemania fue adoptada por el Comité Olímpico en 1931. Los intentos por boicotear los juegos debido a las políticas de discriminación adoptadas por el régimen, fracasaron.
								</p>

							</div>

						</div>
						
					</li>

					<li>

						<div class="timeline-image">

							<img class="rounded-circle img-fluid" src="../private/img/shoa/timeline/5.jpg" srcset="../private/img/shoa/timeline/5@2x.jpg" alt="Publicación de la Encíclica Papal “Mit brennender Sorge”" title="Publicación de la Encíclica Papal “Mit brennender Sorge”">

						</div>

						<div class="timeline-panel">

							<div class="timeline-heading">

								<h4>1937</h4>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">21 DE MARZO</p>

								<p class="text-light gray-900">Publicación de la Encíclica Papal “Mit brennender Sorge” (Con preocupación ardiente) donde el Papa Pío XI repudia el racismo. Sin embargo no menciona a los judíos perseguidos en Alemania. Este documento fue introducido clandestinamente a Alemania y leído desde los púlpitos de las iglesias católicas el Domingo de Ramos, el día domingo anterior a las Pascuas.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">28 DE MAYO</p>

								<p class="text-light gray-900">Neville Chamberlain es designado Primer Ministro de Inglaterra, que habría de implantar la política de apaciguamiento ante Alemania e Italia.
								</p>

							</div>

						</div>

					</li>


					<li class="timeline-inverted">

						<div class="timeline-image">

							<img class="rounded-circle img-fluid" src="../private/img/shoa/timeline/6.jpg" srcset="../private/img/shoa/timeline/6@2x.jpg" alt="La conferencia de Evian" title="La conferencia de Evian">

						</div>

						<div class="timeline-panel">

							<div class="timeline-heading">

								<h4>1938</h4>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">13 DE MARZO</p>

								<p class="text-light gray-900">Se produce el “Anschluss” (la anexión de Austria) por parte de Alemania. Comienzan a aplicarse las medidas antijudías y la presión para la emigración judía del país.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">23 DE MARZO</p>

								<p class="text-light gray-900">Se cancela el reconocimiento jurídico de las organizaciones judías en Alemania.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">16 DE MAYO</p>

								<p class="text-light gray-900">Llega al campo de concentración de Mauthausen el primer grupo de presos políticos . Se los hace trabajar en las canteras de piedra del lugar.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">29 DE MAYO</p>

								<p class="text-light gray-900">Primera ley anti-judía en Hungría.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">6 DE JULIO</p>

								<p class="text-light gray-900">Comienza la Conferencia de Evian, convocada por el presidente Roosevelt, donde habría de tratarse el problema de los refugiados judíos de Austria y Alemania. Con la presencia de representantes de 33 países, la conferencia terminó en un fracaso total.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">9-10 DE NOVIEMBRE</p>

								<p class="text-light gray-900">Se produce el “pogrom” de la Noche de los Cristales (Kristallnacht) en represalia por el asesinato del secretario de la embajada alemana en París, Ernest von Rath, a manos de Herszel Grynszpan. Sinagogas quemadas, comercios destruidos, judíos asesinados y miles enviados a campos de concentración, fue el resultado de la violenta jornada.</p>

							</div>


						</div>

					</li>

					<li>

						<div class="timeline-image">

							<img class="rounded-circle img-fluid" src="../private/img/shoa/timeline/7.jpg" srcset="../private/img/shoa/timeline/7@2x.jpg" alt="Invasión Alemana a Polonia" title="Invasión Alemana a Polonia">

						</div>

						<div class="timeline-panel">

							<div class="timeline-heading">

								<h4>1939</h4>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">30 DE ENERO DE 1939</p>

								<p class="text-light gray-900">Hitler celebra el sexto aniversario de su llegada al poder y pronuncia un discurso en el que menciona que los problemas de Europa no podrían solucionarse hasta que no se llegue a un acuerdo sobre la cuestión judía. Y advierte que “si la judería internacional llegara a desatar una nueva guerra en Europa (…) los judíos de Europa serían aniquilados”.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">2 DE MARZO</p>

								<p class="text-light gray-900">El Cardenal Eugenio Pacelli, anteriormente Nuncio papal en Berlín y gestor del Concordato entre el Vaticano y el Tercer Reich, asume el Papado con el nombre de Pio XII.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">15 DE MARZO</p>

								<p class="text-light gray-900">Se completa el desmembramiento de Checoslovaquia: Eslovaquia se declaró independiente un día antes colocándose bajo la protección de Alemania y el resto del país pasa a ser el Protectorado de Bohemia y Moravia bajo la dominación AlemanaCae Madrid en manos de los franquistas y finaliza la guerra civil en España.


								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">31 DE MARZO</p>

								<p class="text-light gray-900">Ante la demanda alemana de anexar Danzig y el Corredor Polaco, Inglaterra y Francia prometen acudir en ayuda de Polonia en caso de que fuera atacada por Alemania.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">7 DE ABRIL</p>

								<p class="text-light gray-900">Mussolini ordena la ocupación de Albania.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">27 DE ABRIL</p>

								<p class="text-light gray-900">Orden de servicio militar obligatorio en Gran Bretaña. Hitler comunica acerca de la cancelación del acuerdo marítimo de 1935 con Inglaterra.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">5 DE MAYO</p>

								<p class="text-light gray-900">Segunda ley anti-judía en Hungría que determina, de acuerdo a leyes racistas, quién es judío. Además limita la actividad económica judía.

								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">13 DE MAYO</p>

								<p class="text-light gray-900">Comienza su travesía el “Saint Louis”, que parte de Hamburgo con destino a Cuba, llevando a más de 900 refugiados judíos, cuyo ingreso será impedido no sólo en Cuba, sino también en Estados Unidos, debiendo regresar a Alemania.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">1 DE SEPTIMBRE</p>

								<p class="text-light gray-900">Alemania invade Polonia, con la excusa de recuperar el corredor de Danzig. Inglaterra y Francia anuncian que si Alemania no se retira de territorio polaco, considerarán el hecho como causal de guerra.

								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">3 DE SEPTIMBRE</p>

								<p class="text-light gray-900">Inglaterra y Francia declaran formalmente la guerra a Alemania, comenzando la Segunda Guerra Mundial
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">21 DE SEPTIMBRE</p>

								<p class="text-light gray-900">“Carta urgente” de Reinhard Heydrich, jefe del Servicio de Seguridad del Reich, que ordena en secreto la concentración de los judíos de Polonia en ghettos y la creación de los Consejos Judíos (Judenraten) para organizar internamente la vida judía en los ghettos.
								</p>

							</div>

						</div>

					</li>

					<li class="timeline-inverted">

						<div class="timeline-image">

							<img class="rounded-circle img-fluid" src="../private/img/shoa/timeline/8.jpg" srcset="../private/img/shoa/timeline/8@2x.jpg" alt="El campo de concentración de Auschwitz" title="El campo de concentración de Auschwitz">

						</div>

						<div class="timeline-panel">

							<div class="timeline-heading">

								<h4>1940</h4>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">8 DE FEBRERO</p>

								<p class="text-light gray-900">Las autoridades de ocupación alemanas en Polonia ordenan el establecimiento del ghetto de Lodz, segunda ciudad en número de judíos de Polonia, después de Varsovia.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">12 DE ABRIL</p>

								<p class="text-light gray-900">Hans Frank, gobernador alemán de Polonia, ordena que hasta noviembre, la ciudad de Cracovia debe quedar “Jüdenfrei” (Libre de judíos).


								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">27 DE ABRIL</p>

								<p class="text-light gray-900">Himmler, jefe de la SS y la Gestapo, ordena establecer un campo de concentración en Auschwitz. A principios de junio comienzan a llegar los primeros prisioneros, en su mayoría polacos. Se cierra herméticamente el ghetto de Lodz. A partir de ese momento los judíos allí confinados tienen totalmente prohibido salir del ghetto.

								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">10 DE MAYO</p>

								<p class="text-light gray-900">Gran ofensiva alemana en Occidente: se produce la invasión de Francia, Bélgica, Holanda y Luxemburgo.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">14 DE MAYO</p>

								<p class="text-light gray-900">Se rinde Holanda.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">15 DE MAYO</p>

								<p class="text-light gray-900">Los nazis perforan el frente francés en Sedan.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">17 DE MAYO</p>

								<p class="text-light gray-900">Cae Bruselas en manos de los nazis.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">18 DE MAYO</p>

								<p class="text-light gray-900">Cae Amberes en manos de los nazis.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">26 DE MAYO – 4 DE JUNIO</p>

								<p class="text-light gray-900">Evacuación de las tropas inglesas que habían llegado para ayudar a los franceses, a través del puerto de Dunkerque.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">28 DE MAYO</p>

								<p class="text-light gray-900">Bélgica se rinde a los alemanes.</p>

							</div>

						</div>

					</li>

					<li class="timeline">

						<div class="timeline-image">

							<img class="rounded-circle img-fluid" src="../private/img/shoa/timeline/9.jpg" srcset="../private/img/shoa/timeline/9@2x.jpg" alt="Ejecución de un judío en manos de los Einsatzgruppen" title="Ejecución de un judío en manos de los Einsatzgruppen">

						</div>

						<div class="timeline-panel">

							<div class="timeline-heading">

								<h4>1941</h4>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">10 DE ENERO</p>

								<p class="text-light gray-900">Todos los judíos de Holanda son obligados a registrarse ante las autoridades alemanas. Este era un paso previo para su identificación, confiscación de bienes y posterior deportación.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">5 DE FEBRERO</p>

								<p class="text-light gray-900">Se dicta en Rumania la “Ley para la defensa del estado”. Todo judío que transgrediera la ley sería castigado con el doble de la pena que le hubiese correspondido a un rumano no judío.

								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">22 DE FEBRERO</p>

								<p class="text-light gray-900">389 judíos de Amsterdam son deportados al campo de concentración de Buchenwald.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">25 DE FEBRERO</p>

								<p class="text-light gray-900">Huelga general en Amsterdam contra las autoridades Alemanas de ocupación. Es el primer caso en que una población no judía bajo el dominio nazi realiza tal acción de solidaridad con los judíos.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">1 DE MARZO</p>

								<p class="text-light gray-900">Himmler ordena levantar un campo de concentración en Birkenau (Auschwitz II).</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">3 DE MARZO</p>

								<p class="text-light gray-900">Se publica la orden de establecer un ghetto en Cracovia.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">17 DE MAYO</p>

								<p class="text-light gray-900">Cae Bruselas en manos de los nazis.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">20 DE MARZO</p>

								<p class="text-light gray-900">Se cierra el ghetto de Cracovia.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">7 DE ABRIL</p>

								<p class="text-light gray-900">Los judíos de Radom son encerrados en dos ghettos. Los alemanes conquistan Salónica. Motines antijudíos en Ambères (Bélgica).

								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">14 DE ABRIL</p>

								<p class="text-light gray-900">Nuevos ataques antijudíos en Amberes.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">24 DE ABRIL</p>

								<p class="text-light gray-900">Se cierra el ghetto de Lublin.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">27 DE ABRIL</p>

								<p class="text-light gray-900">Se cierra el ghetto de LublinAtenas es conquistada por los alemanes. 1.400 soldados judíos de Eretz Israel que lucharon junto a los ingleses en Grecia son hechos prisioneros de los alemanes.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">2 DE MAYO</p>

								<p class="text-light gray-900">Estallan combates entre el ejército de Irak y tropas inglesas que logran reprimir el levantamiento iraquí. Inmediatamente, estalla un pogrom en Bagdad donde matan a 1000 judíos pero los ingleses no intervienen a favor de los judíos porque entienden que es un problema interno.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">10 DE MAYO</p>

								<p class="text-light gray-900">Rudolf Hess , el número dos en la escala de sucesión de Hitler realiza un vuelo solitario a Inglaterra para supuestamente proponer un acuerdo de paz por separado entre Alemania e Inglaterra.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">15 DE MAYO</p>

								<p class="text-light gray-900">En Rumania se promulga una ley que permite la movilización de judíos para realizar trabajos forzados</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">2 DE JUNIO</p>

								<p class="text-light gray-900">179 judíos son asesinados durante un pogrom realizado en Bagdad donde poco antes había surgido una rebelión que llevó al poder un gobierno pro-nazi. Los británicos, con la ayuda de soldados de Eretz Israel aplastaron esa rebelión; pero no hicieron nada para defender a los judíos de Bagdad.El gobierno de Vichy emite el segundo reglamento para los judíos. Se ordena la realización de un censo de judíos en el territorio bajo el dominio de Vichy.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">6 DE JUNIO</p>

								<p class="text-light gray-900">Se difunde la “orden del comisario”. Los comandantes del ejército alemán deberán ejecutar sumariamente a todos los comisarios políticos (funcionarios del partido comunista encargados de mantener y difundir la línea política del partido comunista). Para los nazis todos los judíos podían ser considerados “comisarios políticos” y promotores del comunismo.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">8 DE JUNIO</p>

								<p class="text-light gray-900">Tropas inglesas invaden Siria y Líbano, territorios bajo el dominio de Vichy. Participan en esa acción fuerzas del Palmach (Fuerzas de choque de la Haganá). En esta acción participa Moshé Dayan y es allí donde pierde un ojo.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">10 DE JUNIO</p>

								<p class="text-light gray-900">Se completa la conquista alemana de la isla de Creta. 120 soldados judíos de Eretz Israel que formaban parte de las tropas británicas caen prisioneros de los alemanes.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">14 DE JUNIO</p>

								<p class="text-light gray-900">Entre 27.000 y 32.000 judíos de Lituania, Letonia, Estonia, Besarabia y Bukovina del Norte son deportados a Siberia por el gobierno soviético.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">18 DE JUNIO</p>

								<p class="text-light gray-900">Se escribe un pacto de amistad entre Alemania y Turquía.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">22 DE JUNIO</p>

								<p class="text-light gray-900">Los nazis invaden la Unión Soviética. Italia y Rumania le declaran la guerra a la Unión Soviética. Detención de los judíos de Zagreb que son deportados a campos de concentración.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">23 DE JUNIO</p>

								<p class="text-light gray-900">Las unidades de los Einsatzgruppen (grupos de tareas) comienzan el asesinato sistemático y masivo de los judíos en los territorios conquistados de la Unión Soviética. Diariamente envían partes acerca de sus actividades. Eslovaquia le declara la guerra a la Unión Soviética. Los nazis conquistan Brest-Litvosk. </p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">24 DE JUNIO</p>

								<p class="text-light gray-900">Vilna y Kovno son conquistadas por los nazis. Sangrientos pogroms realizados por los lituanos contra los judíos.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">25 DE JUNIO</p>

								<p class="text-light gray-900">15.000 judíos son asesinados en el pogrom de Iasi (Rumania).</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">26 DE JUNIO</p>

								<p class="text-light gray-900">La toma de Dvinsk (Letonia). Finlandia participa en la guerra contra la Unión Soviética.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">27 DE JUNIO</p>

								<p class="text-light gray-900">Hungría le declara la guerra a la Unión Soviética. Los nazis conquistan Bialystok y 2.000 judíos son asesinados.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">28 DE JUNIO</p>

								<p class="text-light gray-900">Los nazis conquistan Minsk, capital de Bielorrusia.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">29 DE JUNIO</p>

								<p class="text-light gray-900">Miles de judíos son asesinados en Iasi en lo que se conoce como Domingo Negro.</p>

							</div>		


						</div>

					</li>

					<li class="timeline-inverted">

						<div class="timeline-image">

							<img class="rounded-circle img-fluid" src="../private/img/shoa/timeline/10.jpg" srcset="../private/img/shoa/timeline/10@2x.jpg" alt="Judíos obligados a llevar la estrella amarilla" title="Judíos obligados a llevar la estrella amarilla">

						</div>

						<div class="timeline-panel">

							<div class="timeline-heading">

								<h4>1942</h4>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">16 DE ENERO</p>

								<p class="text-light gray-900">Comienza la deportación de los judíos del ghetto de Lodz hacia Chelmno, donde funcionan unidades móviles para la matanza de judíos por medio de gas.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">20 DE ENERO</p>

								<p class="text-light gray-900">Se realiza la Conferencia de Wannsee, donde se decide cómo implementar la Solución Final de la cuestión judía, es decir, el exterminio de los judíos europeos.

								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">22 DE FEBRERO</p>

								<p class="text-light gray-900">389 judíos de Amsterdam son deportados al campo de concentración de Buchenwald.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">8 DE FEBRERO</p>

								<p class="text-light gray-900">Comienza la deportación de judíos de Salónica hacia el campo de exterminio de Auschwitz.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">23 DE FEBRERO</p>

								<p class="text-light gray-900">Un grupo de judíos de Rumania habían logrado huir de su país, abordando el barco “Struma” con la idea de llegar a Palestina. Mientars navegaba por el mar Negro, choca con una mina hundiéndose. De los 769 pasajeros que escapaban en él, solo uno salva su vida. Cabe mencionar que el gobierno británico había prohibido el ingreso de más judíos a Palestina, a partir del Libro Blanco de 1939.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">1 DE MARZO</p>

								<p class="text-light gray-900">Comienza la construcción del campo de aniquilación de Sobibor. A principios de mayo empieza la aniquilación de judíos.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">2 DE MARZO</p>

								<p class="text-light gray-900">5.000 judíos de Minsk, capital de Bielorusia son asesinados.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">19 DE MARZO</p>

								<p class="text-light gray-900">“Operación Intelectuales”: 50 intelectuales judíos de Cracovia fueron asesinados.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">19 DE MARZO A FINES DE MARZO</p>

								<p class="text-light gray-900">15.000 judíos de Lvov son enviados para ser aniquilados en Belzec.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">25 DE MARZO</p>

								<p class="text-light gray-900">Se establece el Ghetto de Kolomeia en el que se encierran a 18.000 judíos.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">26 DE MARZO</p>

								<p class="text-light gray-900">El primer envío de judíos organizado por Eichmann llega a Auschwitz.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">26 DE MARZO – 20 DE OCTUBRE</p>

								<p class="text-light gray-900">Más de 57.000 judíos son deportados desde Eslovaquia.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">28 DE MARZO</p>

								<p class="text-light gray-900">Más de 57.000 judíos son deportados desde Eslovaquia.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">3 - 4 DE ABRIL</p>

								<p class="text-light gray-900">5.000 judíos de Kolomeia son enviados a Belzec y 250 son asesinados en el ghetto mismo.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">18 DE ABRIL</p>

								<p class="text-light gray-900">“La noche negra”: los nazis recorren las calles del ghetto de Varsovia con listas preparadas y asesinan a 52 judíos.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">20 DE ABRIL</p>

								<p class="text-light gray-900">Los judíos de Lublin son enviados al campo de aniquilación de Belzec.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">29 DE ABRIL</p>

								<p class="text-light gray-900">Los judíos de Holanda deben llevar obligatoriamente una estrella amarilla. En un solo día los 20.000 judíos de Pinsk son obligados a ingresar al ghetto.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">1 DE MAYO</p>

								<p class="text-light gray-900">Liquidación casi total del ghetto de Dvinsk.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">10 DE MAYO</p>

								<p class="text-light gray-900">1.500 judíos de Sosnoviec son deportados a Auschwitz.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">27 DE MAYO</p>

								<p class="text-light gray-900">A partir del 5 de junio los judíos de Bélgica deberán obligatoriamente llevar la señal de la estrella amarilla.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">28 DE MAYO AL 8 DE JUNIO</p>

								<p class="text-light gray-900">6.000 judíos de Cracovia son enviados al campo de muerte de Belzec y 3.000 son asesinados en la ciudad misma.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">4 DE JUNIO</p>

								<p class="text-light gray-900">Muere Heydrich el protector de Bohemia y Moravia como consecuencia de las heridas producidas por un atentado. Estados Unidos le declara la guerra a Rumania.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">7 DE JUNIO</p>

								<p class="text-light gray-900">Todos los judíos de Francia deben llevar la estrella amarilla.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">10 DE JUNIO</p>

								<p class="text-light gray-900">El pueblo de Lidice es destruido por los nazis como venganza por el asesinato de Heydrich.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">11 DE JUNIO</p>

								<p class="text-light gray-900">La oficina de Eichmann ordena comenzar con la deportación de judíos de Francia, Bélgica y Holanda a los campos de muerte en Polonia 3.500 judíos de Tarnow son deportados a Belzec.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">15 AL 18 DE JUNIO</p>

								<p class="text-light gray-900">10.000 judíos de Tarnow son deportados a Belzec.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">18 DE JUNIO</p>

								<p class="text-light gray-900">1.000 judíos de Pzsemyzsel son deportados al campo de Ianowska, cerca de Lvov.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">20 DE JUNIO – 20 DE OCTUBRE</p>

								<p class="text-light gray-900">Son deportados 13.766 judíos de Viena a Terezin.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">22 DE JUNIO</p>

								<p class="text-light gray-900">Son deportados 13.766 judíos de Viena a Terezin.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">26 DE JUNIO</p>

								<p class="text-light gray-900">Deportación de judíos de Bruselas a campos de trabajo en el norte de Francia.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">1 DE JULIO</p>

								<p class="text-light gray-900">La Sipo (policía de seguridad alemana) recibe el manejo del campo de concentración de Westerborg (Holanda)</p>

							</div>		


						</div>

					</li>

					<li class="timeline">

						<div class="timeline-image">

							<img class="rounded-circle img-fluid" src="../private/img/shoa/timeline/11.jpg" srcset="../private/img/shoa/timeline/11@2x.jpg" alt="Levantamiento del gueto de Varsovia." title="Levantamiento del gueto de Varsovia.">

						</div>

						<div class="timeline-panel">

							<div class="timeline-heading">

								<h4>1943</h4>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">1 DE ENERO</p>

								<p class="text-light gray-900">Los nazis les prohíben a los judíos de Holanda retirar dinero de sus cuentas bancarias personales. Todas las cuentas se reúnen en una única cuenta que es confiscada.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">13 DE ENERO</p>

								<p class="text-light gray-900">1.500 judíos de Radom (Polonia) son deportados al campo de muerte de Treblinka.

								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">17 AL 24 DE ENERO</p>

								<p class="text-light gray-900">Conferencia de Casablanca. El Presidente de los EE.UU., Franklin Delano Roosvelt y el primer ministro británico, Winston Churchill, se comprometen a proseguir la guerra contra Alemania hasta su rendición incondicional.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">2 DE FEBRERO</p>

								<p class="text-light gray-900">Los restos del sexto ejército alemán se rinden ante los rusos en la histórica batalla de Stalingrado.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">5 AL 12 DE FEBRERO</p>

								<p class="text-light gray-900">Son deportados 10.000 judíos de Bialistock a Treblinka. 20.000 judíos son asesinados en el ghetto. Se producen actos de resistencia armada.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">26 DE FEBRERO</p>

								<p class="text-light gray-900">Llega el primer grupo de gitanos a Auschwitz que son ubicados en una sección especial de ese campo de muerte.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">4 – 9 DE MARZO</p>

								<p class="text-light gray-900">Arresto y deportación de 4.000 judíos de Tracia (región de Grecia o ocupada por Bulgaria) a Treblinka.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">20 DE MARZO AL 18 DE AGOSTO</p>

								<p class="text-light gray-900">Se realizan 20 deportaciones de judíos de Salónica a Auschwitz. Fueron deportados 43.850 judíos.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">8 – 9 DE ABRIL</p>

								<p class="text-light gray-900">1000 Judíos son asesinados en Tarnopol (Polonia).
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">19 - 30 DE ABRIL</p>

								<p class="text-light gray-900">Conferencia de Bermuda sobre el tema de los refugiados. Los funcionarios americanos y británicos frustran los esfuerzos para salvar judíos impidiéndoles el ingreso en sus respectivos países. La excusa era el temor a la infiltración de agentes nazis entre los refugiados.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">19 ABRIL – 16 DE MAYO</p>

								<p class="text-light gray-900">Rebelión en el Ghetto de Varsovia; destrucción total del ghetto.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">8 DE MAYO</p>

								<p class="text-light gray-900">Cae Mordejai Anielewicz junto con otros luchadores al irrumpir los nazis en el bunker del Comando de la Organización Combatiente Judía en la calle Mila 18 en Varsovia.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">12 DE MAYO</p>

								<p class="text-light gray-900">Se suicida Shmuel Zigelboim representante del Bund en el Consejo Nacional Polaco en Londres, al llegar las noticias sobre la liquidación de los últimos combatientes judíos en Varsovia y también como protesta por el silencio del mundo ante la aniquilación de los judíos de Europa</p>

							</div>

						</div>

					</li>

					<li class="timeline-inverted">

						<div class="timeline-image">

							<img class="rounded-circle img-fluid" src="../private/img/shoa/timeline/12.jpg" srcset="../private/img/shoa/timeline/12@2x.jpg" alt="Proceso de llegada y selección de Judíos en Auschwitz." title="Proceso de llegada y selección de Judíos en Auschwitz">

						</div>

						<div class="timeline-panel">

							<div class="timeline-heading">

								<h4>1944</h4>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">19 DE MARZO</p>

								<p class="text-light gray-900">Hungría es ocupada por los nazis cuando este país intenta salir de la guerra.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">5 DE ABRIL</p>

								<p class="text-light gray-900">Se implanta en Hungría el uso obligatorio de la estrella amarilla para los judíos.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">16 DE ABRIL</p>

								<p class="text-light gray-900">Comienzan los preparativos para la deportación de los judíos de Hungría hacia Auschwitz.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">2 DE MAYO</p>

								<p class="text-light gray-900">Llega a Auschwitz el primer transporte de judíos de Hungría.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">15 DE MAYO – 9 DE JULIO</p>

								<p class="text-light gray-900">Durante ocho semanas son deportados a Auschwitz 437.000 judíos de Hungría.</p>

							</div>


						</div>

					</li>

					<li class="timeline">

						<div class="timeline-image">

							<img class="rounded-circle img-fluid" src="../private/img/shoa/timeline/13.jpg" srcset="../private/img/shoa/timeline/13@2x.jpg" alt="La alemania nazi firma la rendición." title="La alemania nazi firma la rendición">

						</div>

						<div class="timeline-panel">

							<div class="timeline-heading">

								<h4>1945</h4>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">16 DE ENERO</p>

								<p class="text-light gray-900">El ejército rojo libera la ciudad polaca de Kielce. De los miles de judíos que vivían allí antes de la guerra solo 25 sobrevivieron.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">17 DE ENERO</p>

								<p class="text-light gray-900">El ejército rojo libera Varsovia. La ciudad está totalmente destruida.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">18 DE ENERO</p>

								<p class="text-light gray-900">Comienza la evacuación del campo de muerte de Auschwitz. 58.000 prisioneros, la mayoría de ellos judíos son obligados a realizar la “marcha de la muerte” hacia otros campos. 15.000 perecen en esta marcha.
								</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">25 DE ENERO</p>

								<p class="text-light gray-900">136 judíos con pasaportes latinoamericanos, liberados del campo de concentración de Bergen-Belsen se dirigen a Suiza y allí son recibidos.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">25 DE ENERO – 25 DE ABRIL</p>

								<p class="text-light gray-900">50.000 judíos prisioneros del campo de concentración de Stutthof participan de la “marcha de la muerte”. Mueren 26.000.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">27 DE ENERO</p>

								<p class="text-light gray-900">El Ejército Rojo libera el campo de muerte de Auschwitz. Ya casi no quedaban judíos con vida.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">5 DE FEBRERO</p>

								<p class="text-light gray-900">1.200 judíos de Bohemia y Moravia llegan a Suiza a través de la ayuda de la Cruz Roja Internacional</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">26 DE MARZO</p>

								<p class="text-light gray-900">Argentina le declara la guerra a Japón y a su aliado Alemania. Es el último país en hacer esta declaración de guerra.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">5 - 6 DE ABRIL</p>

								<p class="text-light gray-900">Los nazis evacuan gran cantidad de prisioneros del campo de concentración Buchenwald.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">11 DE ABRIL</p>

								<p class="text-light gray-900">Los americanos liberan el campo de concentración de Buchenwald.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">12 DE ABRIL</p>

								<p class="text-light gray-900">Fallece Roosvelt, presidente de los Estados Unidos.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">15 DE ABRIL</p>

								<p class="text-light gray-900">Los británicos liberan el campo de concentración de Bergen-Belsen.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">23 DE ABRIL</p>

								<p class="text-light gray-900">Comienza el ataque ruso contra Berlín.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">28 DE ABRIL</p>

								<p class="text-light gray-900">Partisanos italianos capturan y fusilan a Benito Mussolini.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">30 DE ABRIL</p>

								<p class="text-light gray-900">Hitler se suicida en su bunker en Berlín.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">1 DE MAYO</p>

								<p class="text-light gray-900">Se rinde el ejército alemán en Italia.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">2 DE MAYO</p>

								<p class="text-light gray-900">Berlín es ocupada por las tropas soviéticas.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">3 DE MAYO</p>

								<p class="text-light gray-900">La Cruz Roja Internacional recibe el campo de concentración de Theresienstadt con 17.247 prisioneros.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">4 DE MAYO</p>

								<p class="text-light gray-900">La SS abandona el campo de concentración de Mauthausen.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">7 DE MAYO</p>

								<p class="text-light gray-900">Rendición del ejército alemán ante las fuerzas aliadas.</p>

							</div>

							<div class="text-muted">

								<p class="text-medium text-uppercase">8 DE MAYO</p>

								<p class="text-light gray-900">Rendición incondicional de la Alemania Nazi.</p>

							</div>	


						</div>

					</li>

				</ul>

			</div>

		</div>

	</div>

</section>