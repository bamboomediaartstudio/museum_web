<div id="friends">

	<div class="container mb-5">

		<div class="row">

			<div class="col-lg-8 mx-auto">

				<p class="text-center text-medium text-uppercase module-down">

					<i class=" fa-1x fas fa-info-circle"></i> 

					SI QUERES SER <span data-html="true" class="sponsor-category" data-toggle="tooltip" data-placement="bottom" title="<b>¿QUÉ ES SER SPONSOR?</b><br><br><p class=''><?php echo $wording->get('sponsors_description');?></p">SPONSOR

					</span>, 

					<span data-html="true" class="sponsor-category" data-toggle="tooltip" data-placement="bottom" title="<b>¿QUÉ ES SER DONANTE?</b><br><br><p class=''><?php echo $wording->get('donor_description');?></p">DONANTE

					</span>o 

					<span data-html="true" class="sponsor-category" data-toggle="tooltip" data-placement="bottom" title="<b>¿QUÉ ES SER AMIGO DEL MUSEO?</b><br><br><p class=''><?php echo $wording->get('friend_description');?></p">AMIGO DEL MUSEO

					</span>,

					<span>

						<a role="button" class="open-sponsors-modal text-medium">CONTACTANOS.</a>

					</span>

				</p>

			</div>

		</div>

	</div>

</div>