<div class="container mt-5 heritage-section">

	<div class="col-12 text-center">

		<img class="books-image img-fluid hvr-grow" 

		src="../../private/img/patrimonio/patrimonio.png" 

		src="../../private/img/patrimonio/patrimonio@2x.png" 

		alt="Patrimonio del Museo Del Holocausto"

		title="Patrimonio del Museo Del Holocausto"

		>

	</div>

	<div class="row mt-3">

		<div class="col-lg-12 text-center">

			<h2 class="section-heading module-title"><?php echo $wording->get('heritage_first_line');
			;?></h2>

			<hr class="module-separator">

		</div>

		<div class="col-lg-8 mx-auto">

			<p class="text-light text-faded mb-5 text-center about-reflexiones module-text"><?php echo $wording->get('heritage_second_line');
			;?></p>

		</div>

	</div>

</div>

<div class="mt-5 page-body-content">

	<div class="container">

		<div class="grid row">

			<?php

			$contenetQuery = DB::getInstance()->query('

				SELECT *, mc.id as mid, img.unique_id 		as uid 

				FROM museum_heritage						as mc

				LEFT JOIN museum_images 					as img 		

				ON mc.id = img.sid AND img.source 			= ?

				AND img.deleted 							= ?

				WHERE mc.deleted 							= ? 

				AND mc.active 								= ? 

				ORDER by mid dESC', 

				(array('heritage', 0, 0, 1)));

			$count = 0; 

			foreach($contenetQuery->results() as $item){ 

				$isOdd = false;

				$count+=1;

				if($count%2==0) $isOdd = true;

				$filters = '';

				$itemName = $item->title;

				$itemURL = $item->url;

				$caption = $item->caption;

				$content = $item->content;

				$itemId = $item->id;

				$date = utf8_encode(strftime("%A, %d de %B de %Y", strtotime($item->added)));

					//image...

				if($item->uid == null){

					$myImg = '../../private/img/templates/image-template/template_medium.jpg';

					$myImgRetina = '../../private/img/templates/image-template/template_medium@2x.jpg';

				}else{

					$myImg = '../../museumSmartAdmin/dist/default/private/sources/images/heritage/' . $item->mid . '/' . $item->uid . '/' . $item->mid . '_' . $item->uid . '_medium.jpeg';

					$myImgRetina = '../../museumSmartAdmin/dist/default/private/sources/images/heritage/' . $item->mid . '/' . $item->uid . '/' . $item->mid . '_' . $item->uid . '_medium@2x.jpeg';
				}

				$shadow = '../../private/img/templates/comunicado-template/shadow.png';


				?>

				<div class="col-lg-4 col-md-6 col-12 pr-1 pl-1">

					<div class="heritage-main-image-container image-container mr-2 ml-2 mb-4">

						<a href="<?php echo $itemURL;?>">

							<img class="heritage-list-image news-image hvr-grow img-fluid" 

							src="<?php echo $myImg;?>"

							srcset="<?php echo $myImgRetina;?>" 

							alt="<?php echo $itemName;?>"

							title="<?php echo $itemName;?>">

							<img class="img-shadow position-absolute img-fluid"  src="<?php echo $shadow;?>" 

							srcset ="<?php echo $shadow;?>" >

						</a>

						<div class="heritage-text-content position-absolute">

							<h2 class="white text-truncate heritage-title text-uppercase mt-5"><?php echo $itemName;?></h2>

						</div>

						<a href="<?php echo $itemURL;?>" role="button" class="read-btn btn button">VER OBJETO</a>

					</div>

				</div>

			<?php } ?>

		</div>

	</div>

</div>

