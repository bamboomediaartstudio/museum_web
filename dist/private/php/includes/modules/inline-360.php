<?php
?>

<section id="inline-360">

	<div class="container">

		<div class="col-12 text-center">
			
			<img class="hvr-grow books-image img-fluid" src='private/img/360/360.png' alt="Recorrido Virtual 360°">

		</div>

		<div class="row mt-5">

			<div class="offset-2 col-8 text-center">

				<h2 class="section-heading module-title text-uppercase">RECORRIDO VIRTUAL 360°</h2>

				<p class="text-light text-faded mb-5 text-center about-reflexiones module-text">

					El Recorrido Virtual 360° es una propuesta digital que permite visitar de forma remota la totalidad de la exhibición permanente del Museo del Holocausto de Buenos Aires.<br>Se encuentra disponible de forma guiada tanto para <strong>instituciones</strong> como para <strong>individuos</strong>. También la podés recorrer de forma autoguiada.

				</p>

			</div>

		</div>

		<div class="col-12 text-center">

			<input class="generic-action-button btn button" type="button" onclick="location.href='recorridovirtual360';" value="DESCUBRÍ EL RECORRIDO VIRTUAL 360°" />


		</div>

	</div>

</section>