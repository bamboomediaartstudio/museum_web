<?php 

//(isset($origin) && $origin == 'home') ? $path = '' : $path = '../';

//(isset($origin) && $origin == 'home') ? $openPath = 'cursos/' : $openPath = '../';

$db = DB::getInstance();

$result = $db->query('

SELECT *, 

mc.id                                     as mid, 

img.unique_id                             as uid,

mc.description                            as event_description

from museum_courses                       as mc

LEFT JOIN museum_images                   as img 

ON                                        mc.id = img.sid 

AND img.source                            = ?

AND img.active                            = ?

AND img.deleted                           = ?

WHERE mc.active                           = ? 

AND mc.deleted                            = ? 

order by                                  mc.id'

,                            ['courses', 1, 0, 1, 0]);

//$wording = new Wording();

//$wording->load('home', 'cursos');

?>

<section id="courses" class="home-course">

	<section id="course-header">

		<div class="container">

			<div class="row">

				<div class="mt-0 col-lg-12 text-center">

					<h2 class="section-heading "><?php echo $wording->get('courses_first_line');?></h2>

					<hr class="module-separator">

				</div>

				<div class="col-lg-8 mx-auto">

					<p class="text-light text-faded mb-5 text-center about-reflexiones module-text"><?php echo $wording->get('courses_second_line');?></p>

				</div>

			</div>

		</div>

	</section>

	<section id="course-content">

		<div class="container">

			<div class="single-item">

				<?php 

				foreach($result->results() as $course){

					$url = 'cursos/' . $course->url;

					if($course->uid != null){

						$defaultImg = true;

						$name = $course->name;

						$description = $course->event_description;

						$myImg = 'museumSmartAdmin/dist/default/private/sources/images/courses/' . $course->mid . '/' . $course->uid . '/' . $course->mid . '_' . $course->uid . '_medium.jpeg';

						$myImgRetina = 'museumSmartAdmin/dist/default/private/sources/images/courses/' . $course->mid . '/' . $course->uid . '/' . $course->mid . '_' . $course->uid . '_medium@2x.jpeg';

					}else{

						$defaultImg = false;

						$myImg = 'private/img/templates/image-template/template_original.jpg';

						$myImgRetina = 'private/img/templates/image-template/template_original@2x.jpg';

					}

					?>

					<div class="single-item-move mt-5 mb-5">

						<div class="row">

							<div class="col-lg-6 text-left mb-5">

								<h4 class="section-heading text-medium text-uppercase text-truncate"><?php echo $name;?></h4>

								<hr class="left-separator">

								<p class="dark mb-5 text-left black five-lines">
									<span class="text-left text-light black">
										<?php echo $description;?>
									</span>
								</p>

								<a href="<?php echo $url;?>" role="button" id="course-button" class="hvr-sweep-to-right btn btn-square">IR AL CURSO</a>


							</div>

							<div class="col-lg-6 col-12 d-sm-block course-image-inline-container">

								<img class="img-fluid d-block mx-auto hvr-grow" 

								src='<?php echo $myImg;?>'

								srcset='<?php echo $myImgRetina;?>'

								alt="<?php echo 'imagen del curso ' . $name;?>" 

								title="<?php echo 'imagen del curso ' . $name;?>">

							</div>

						</div>

					</div>

				<?php } ?>			

			</div>

		</div>

	</section>

</section>