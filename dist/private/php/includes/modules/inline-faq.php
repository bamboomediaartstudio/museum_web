<?php

?>

<section id="inline-faq">

	<div class="container pl-md-0 pr-md-0 pl-5 pr-5">

		<div class="row">

			<div class="col-lg-12 text-center text-md-left">

				<h2 class="section-heading module-title">PREGUNTAS FRECUENTES</h2>

				<hr class="left-separator d-none d-md-block">

			</div>
		</div>

	</div>

	<div class="container pl-md-0 pr-md-0 pl-5 pr-5">

		<div class="row">

			<?php 

			$query = DB::getInstance()->query('

				SELECT * FROM museum_faq WHERE active = ? AND deleted = ? ORDER BY internal_order ASC',

				(array(1, 0)));

			$count = 0; 

			foreach($query->results() as $result){ 

			?>

			<div class="col-lg-6 col-md-6 text-left inline-faq-combo">

				<div class="mt-5">

					<h5 class="mt-3 mb-3 text-medium text-uppercase text-center text-md-left"><?php echo $result->question;?></h5>

					<p class="text-muted mb-0 text-light text-up text-center text-md-left"><?php echo $result->answer;?></p>

				</div>

			</div>

			<?php } ?>

			


		</div>

	</div>

</section>