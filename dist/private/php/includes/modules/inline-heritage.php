<?php
?>

<section id="inline-heritage">

	<div class="container">

		<div class="col-12 text-center">
			
			<img class="hvr-grow books-image img-fluid" src='private/img/patrimonio/patrimonio.png' alt="Patrimonio del museo del Holocausto">

		</div>

		<div class="row mt-5">

			<div class="offset-2 col-8 text-center">

				<h2 class="section-heading module-title text-uppercase"><?php echo $wording->get('heritage_first_line');?></h2>

				<p class="text-light text-faded mb-5 text-center about-reflexiones module-text">

					<?php echo $wording->get('heritage_second_line');?>

				</p>

			</div>

		</div>

		<div class="col-12 text-center">

			<!--<a href="museo/patrimonio" target="_blank" class="generic-action-button btn button" role="button">VER PATRIMONIO</a>-->

			<input class="generic-action-button btn button" type="button" onclick="location.href='museo/patrimonio';" value="VER PATRIMONIO" />

			<!--<button class="generic-action-button btn button" data-filter="*">TESTING</button>-->


		</div>

	</div>

</section>