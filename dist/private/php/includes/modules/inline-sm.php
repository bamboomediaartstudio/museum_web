<div class="social-icons float-right">

	<ul class="list-unstyled text-center mb-0">

		<?php

		$smQuery = DB::getInstance()->query('SELECT * from museum_sm WHERE active = ? ORDER BY internal_order ASC', [1]);

		foreach($smQuery->results() as $result){ 

			?>

			<li class="list-unstyled-item">

				<a target="_blank" href="<?php echo $result->url;?>" data-toggle="tooltip" data-placement="left" title="Visitanos en <?php echo $result->name;?>">

					<i class="<?php echo $result->icon;?>"></i>

				</a>

			</li>

		<?php }?> 

	</ul>

</div>