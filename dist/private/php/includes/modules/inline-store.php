<?php
?>

<section id="inline-store">

	<div class="container">

		<div class="col-12 text-center">
			
			<img class="hvr-grow books-image img-fluid" src='private/img/store/books.png' alt="Libros del museo">

		</div>

		<div class="row mt-5">

			<div class="offset-2 col-8 text-center">

				<h2 class="section-heading module-title">BIBLIOTECA</h2>

				        <hr class="module-separator">


				<p class="text-light text-faded mb-5 text-center about-reflexiones module-text">
					
					<!--span class="text-light black">-->

						En nuestra biblioteca podrás encontrar <strong>Libros en idioma extranjero</strong>, <strong>Libros sobre temáticas específicas</strong>(propaganda nazi, campos, guetos, medicina, biografías y testimonios), <strong> espacio de prensa</strong>, <strong>espacio de videoteca</strong> y <strong>hemeroteca</strong><br><br>La biblioteca funciona en <a href="https://maps.google.com/?ll=-34.562154, -58.447477" target="_blank">José Hernández 1750</a> de lunes a jueves de 15 a 20 horas.<br>
						Para consultas, escribir a <a href="mailto:biblioteca@museodelholocausto.org.ar">biblioteca@museodelholocausto.org.ar</a><!--</span>-->
				</p>

			</div>

		</div>

		<div class="col-12 text-center">

			<!--<a href="private/pdf/Biblioteca-Shoa-Base-de-Datos-1.pdf" target="_blank" download="catalogo" class="hvr-sweep-to-right btn btn-square" role="button">DESCARGAR CATÁLOGO</a>-->

			<input class="generic-action-button btn button" type="button" onclick="location.href='museo/biblioteca.php';" value="IR A LA BIBLIOTECA" />

		</div>

	</div>

</section>