<?php
?>

<section id="inline-360">

	<div class="container">

		<div class="col-12 text-center">
			
			<img class="hvr-grow books-image img-fluid" src='private/img/classes/classes.png' alt="Recorrido Virtual 360°">

		</div>

		<div class="row mt-5">

			<div class="offset-2 col-8 text-center">

				<h2 class="section-heading module-title text-uppercase">CLASES VIRTUALES</h2>

				<p class="text-light text-faded mb-5 text-center about-reflexiones module-text">

					Descubrí las clases a distancia que brinda El Museo Del Holocausto de Buenos Aires<br>Las clases se encuentran disponible de forma guiada a través de la plataforma Zoom, tanto para <strong>instituciones</strong> como para <strong>individuos</strong>.

				</p>

			</div>

		</div>

		<div class="col-12 text-center">

			<input class="generic-action-button btn button" type="button" onclick="location.href='clases';" value="VER CLASES VIRTUALES DISPONIBLES" />


		</div>

	</div>

</section>