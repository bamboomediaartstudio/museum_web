<?php

$query = DB::getInstance()->query("SELECT * FROM museum_institutional");

$about = $query->first()->about;

$mission = $query->first()->mission;

$vision = $query->first()->vision;

?>

<section id="about-museum mb-5">

	<div class="container">

		<div class="row">

			<div class="col-12">

				<h5 class="text-uppercase text-medium black content-title">REFLEXIONES DE LA SHOÁ</h5>

				<hr class="left-separator content-separator">

				<p class="dark text-faded text-left about-reflexiones module-text text-light content-text">

					El Museo del Holocausto de Buenos Aires presenta la muestra “Reflexiones sobre la Shoá” mientras prepara su nueva exhibición que se inaugurará en 2019.<br><br>

					Los judíos convivieron en Europa con otras comunidades durante más de dos mil años. Coexistieron de forma pacífica aunque, por momentos, padecieron acusaciones y persecuciones. a pesar de las adversidades, la cultura judía europea pudo crecer y desarrollarse. Fue durante la Segunda Guerra Mundial que la Alemania nazi llevó adelante el Holocausto-Shoá, el exterminio sistemático de seis millones de judíos, de los cuales un millón y medio fueron niños.<br><br>

					Repensar la historia permite formular preguntas incómodas y profundas acerca de la condición humana. Esperamos que con esta muestra se sientan invitados a reflexionar sobre las experiencias del pasado y sobre nuestras acciones en el presente. Promovemos una educación que advierta sobre los peligros de la propagación del odio contra cualquier grupo y fomente el respeto por los derechos humanos y los valores democráticos.<br><br>

				La muestra se puede visitar en José Hernández 1750, sede temporal del Museo, con previa inscripción.</p>

			</div>

  </div>


</section>