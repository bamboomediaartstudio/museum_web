<?php

$query = DB::getInstance()->query("SELECT * FROM museum_institutional");

$about = $query->first()->about;

$mission = $query->first()->mission;

$vision = $query->first()->vision;

$values = $query->first()->values;

?>

<section id="about-museum mb-5" >

	<div class="container">

		<div class="row">

			<div class="col-12">

				<h5 class="pt-5 text-uppercase text-medium black content-title acerca-del-museo">MUSEO DEL HOLOCAUSTO BUENOS AIRES</h5>

				<hr class="left-separator content-separator">

				<p class="dark text-faded text-left about-reflexiones module-text text-light content-text"><?php echo $about;?></p>

			</div>

			<div class="col-lg-4 col-md-6 text-left mision-y-vision">

				<div class="mt-5">

					<h5 class="mt-3 mb-3 text-medium black content-title">MISIÓN</h5>

					<hr class="left-separator content-separator">

					<p class="text-muted mb-0 text-light text-up content-text"><?php echo $mission;?></p>

				</div>

			</div>

			<div class="col-lg-4 col-md-6 text-left">

				<div class="mt-5">

					<h5 class="mt-3 mb-3 text-medium black content-title">VISIÓN</h5>

					<hr class="left-separator content-separator">

					<p class="text-muted mb-0 text-light text-up content-text"><?php echo $vision;?></p>

				</div>

			</div>

			<div class="col-lg-4 col-md-6 text-left">

				<div class="mt-5">

					<h5 class="mt-3 mb-3 text-medium black content-title">VALORES</h5>

					<hr class="left-separator content-separator">

					<p class="text-muted mb-0 text-light text-up content-text"><?php echo $values;?></p>

				</div>

			</div>

		</div>

	</div>


</section>