<?php


?>

<section id="about-museum mb-5" >

	<div class="container">

		<div class="row">

			<div class="col-12">

				<h5 class="pt-5 text-uppercase text-medium black content-title acerca-del-museo">LA REMODELACIÓN</h5>

				<hr class="left-separator content-separator">

				<p class="dark text-faded text-left about-reflexiones module-text text-light content-text">En octubre de 2019 se inaugurará el museo con una exposición de vanguardia cuyos ejes principales serán: narrar la historia del Holocausto de forma didáctica y dinámica, ubicar al visitante en los complejos escenarios y dilemas de decisión impuestos por el régimen nazi, honrar las memorias personales y familiares de las víctimas y reivindicar las historias de quienes rescataron judíos durante la Shoá.
				Al finalizar la remodelación integral, el edificio contará con una superficie total de 3154 m2, unos 988 m2 más que antes estaban en desuso. La muestra principal contará con 780 m2 y compartirá espacio con instituciones que se dedican a trabajar por la memoria del Holocausto y de los sobrevivientes.</p>

			</div>

		</div>

	</div>


</section>