<section id="about-museum mb-5" class="investigaciones">

	<div class="container">

		<div class="row col-12">

			<h5 class="text-medium black content-title text-uppercase">Investigaciones</h5>


		</div>

		<hr class="left-separator content-separator">

		<div class="row">

			<div class="col-lg-5 col-md-5">

				<p class="text-muted text-light content-text">El área de Investigaciones del Museo del Holocausto de Buenos Aires se ocupa de esclarecer el destino sufrido por los ciudadanos argentinos que fueron víctimas de los alemanes, sus aliados y colaboradores entre el 30 de enero de 1933 y el 8 de mayo de 1945.

					Formas de victimización fueron: emigración forzada, evacuación o repatrio; prohibición de ejercer profesiones u oficios; confiscación, requisición, saqueo o daños sobre bienes materiales; arresto ilegal, privación de libertad o adopción de una identidad falsa para eludirla; encierro en guetos; internación en campos de concentración, de canje, de trabajo, de prisioneros de guerra, de tránsito y/o de exterminio; tortura; violación; deportación, trabajo forzado y/o muerte.

					Hasta el momento se han identificado 900 casos de ciudadanos argentinos víctimas del Holocausto. Sabemos que el número es mayor y la investigación continúa abierta para más casos y más datos.

					Invitamos a la comunidad nacional e internacional a colaborar con datos, registros o cualquier información que colabore a ampliar nuestro conocimiento sobre estos argentinos a investigaciones@museodelholocausto.org.ar.

					Conozca la historia de algunos de los ciudadanos argentinos que fueron víctimas del Holocausto. La información publicada es el resultado de las investigaciones desarrolladas desde el Museo del Holocausto de Buenos Aires por el equipo coordinado por Prof. Marcia Ras.
				</p>

			</div>
			<div class="col-lg-7 col-md-7 row investigations-container"><?php

			$query = DB::getInstance()->query('

				SELECT *

				FROM museum_investigations WHERE active = ? AND deleted = ? ORDER by internal_order ASC', (array(1, 0)));

			$count = 0; 

			foreach($query->results() as $result){ 

				$filePath = '../museumSmartAdmin/dist/default/private/sources/pdf/investigations/' . $result->id . '/pdf/' . $result->url . '.pdf';

				?>

				<div class="investigation-item col-6">

					<p class="text-uppercase text-light gray-900 text-truncate">

						<a target="_blank" href="<?php echo $filePath;?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $result->description;?>" data-html="true">

							<img class="pull-left" width="30px" src="../private/img/pdf.svg" alt="<?php echo $result->title;?>">

							<span><?php echo $result->title;?></span>

						</a>

					</p>

				</div>

			<?php } ?>

		</div>

	</div>

</div>

</section>