<section id="services">

  <div class="container">

    <div class="row">

      <div class="col-md-6 mt-3">

        <img class="img-fluid" src="../private/img/biblioteca/books.png" alt="Libros de la biblioteca popular judía" title="Libros de la biblioteca popular judía"></img>

      </div>

      <div class="col-md-6 mt-5">

        <h2 class="text-center text-md-left section-heading module-title text-uppercase">BIBLIOTECA</h2>

        <!--<hr class="left-separator">-->

        <p class="text-center text-md-left text-medium text-uppercase mt-5">En nuestra biblioteca podrás encontrar:</p>

        <ul class="text-uppercase font-light" style="padding-left: 0; list-style: none;">

          <li class="text-center text-md-left mt-4">Libros en idioma extranjero.</li>

          <li class="text-center text-md-left mt-4">Libros sobre temáticas específicas (Propaganda nazi – Campos – Guetos – Medicina – Biografías).</li>

          <li class="text-center text-md-left mt-4">Espacio de prensa</li>

          <li class="text-center text-md-left mt-4">Espacio de videoteca</li>

          <!--<li class="text-center text-md-left mt-4">Espacio de hemeroteca</li>-->

        </ul>  

        <div class="mt-5 text-center text-md-left">

          <a href="../private/pdf/Biblioteca-Shoa-Base-de-Datos-1.pdf" target="_blank" download="Biblioteca-Shoa-Base-de-Datos-1.pdf" class="hvr-sweep-to-right btn btn-square" role="button">DESCARGAR CATÁLOGO</a>

        </div> 

      </div>

    </div>

  </div>


</section>