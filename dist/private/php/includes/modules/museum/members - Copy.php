<?php

?>

<div class="staff">

	<div class="offset-2 col-8 text-center mt-5">

		<h4 class="section-heading  text-medium">CONOCENOS</h4>

		<p class="dark mt-5">

			<span class="text-light black about-staff">
				Somos un grupo interdisciplinario de profesionales que conformamos el <strong>staff</strong>, <strong>la comisión directiva</strong> y a los <strong>contrinuyentes</strong>
			</span>
		</p>

	</div>

	<div class="full-container">

		<div id="" class='wrapper text-center'>

			<div id="filters" class="btn-group-justified btn-group-sm btn-square filters-button-group mb-5">

				<?php

				$staffMenuQuery = DB::getInstance()->query('SELECT * FROM museum_staff_categories WHERE active = ? AND deleted = ? ORDER BY internal_order ASC', [1, 0]);

				foreach($staffMenuQuery->results() as $staffMenu){ 

					$menuFilter = '.filter-' . $staffMenu->id;
					
					$menuSort = 'data-sort-filter-' . $staffMenu->id;

					$menuFunction = 'dataSortFilter' . $staffMenu->id;

					$name = 'button-' . $staffMenu->id;

					?>

					<button id="<?php echo $name;?>" type="button" class="mb-1 btn filter-button staff-button" sort-by='<?php echo $menuSort;?> ' data-function='<?php echo $menuFunction;?>' data-filter=<?php echo $menuFilter;?>><?php echo $staffMenu->category_name;?></button>

				<?php } ?>

			</div>

		</div>

		<div class="container-fluid">

		<div class="grid">

			<div class="first-in-line element-item col-6 filter-1 filter-2 filter-3 filter-4 filter-5" data-category="filter-1 filter-2 filter-3 filter-4 filter-5">

			</div>

			<?php

			$staffQuery = DB::getInstance()->query('

				SELECT *, ms.id as mid, img.unique_id as uid 

				FROM museum_staff 				as ms

				LEFT JOIN museum_staff_roles 	as role  	ON role.id = ms.role 

				LEFT JOIN museum_images 		as img 		ON ms.id = img.sid AND img.source = ? 

				AND img.active = ? AND img.deleted = ? 
				
				', (array('staff', 1, 0)));

			$count = 0; 

			foreach($staffQuery->results() as $member){ 

				$showEmail = $member->show_email_on_web;

				$showPhone = $member->show_phone_on_web;

				if($member->mid != null){

					$myImg = '../museumSmartAdmin/dist/default/private/sources/images/staff/' . $member->mid . '/' . $member->uid . '/' . $member->mid . '_' . $member->uid . '_edited.jpeg';

					$retina = '../museumSmartAdmin/dist/default/private/sources/images/staff/' . $member->mid . '/' . $member->uid . '/' . $member->mid . '_' . $member->uid . '_edited@2x.jpeg 2x';

				}else{

					$myImg = '../private/img/templates/tutor-avatar/empty-avatar.jpg';

					$retina = '../private/img/templates/tutor-avatar/empty-avatar@2x.jpg';

				}


					//categories relations...

				$staffRelationsQuery = DB::getInstance()->query('SELECT * FROM museum_staff_categories_relations WHERE id_museum_staff_user = ? AND deleted = ?

					', array($member->mid, 0));

				$applyFilter = '';

				$allDataSorts = '';

				$dataDort = '';

				foreach($staffRelationsQuery->results() as $userCategories){

					$applyFilter .= ' filter-' . $userCategories->id_museum_staff_category;

					$dataSort = 'data-sort-filter-' . $userCategories->id_museum_staff_category . 

					'=' . $userCategories->internal_order;

					$allDataSorts .= $dataSort . ' ';

				}

				?>

				<div class="element-item col-12 col-lg-6 <?php echo $applyFilter;?>" data-category="<?php echo $applyFilter;?>" <?php echo $allDataSorts;?>>

					<div class="sider-module col-lg-6 col-12 left-side float-left text-center">

						<div class="staff-info-content xs-m-auto">

							<h4 class="staff-member-name text-uppercase text-medium"><?php echo $member->name . ' ' . $member->surname;?></h4>

							<hr class="center-separator staff-separator">

							<p class="role text-uppercase text-light black"><?php echo $member->role;?></p>

							<?php  if($showEmail == 1 && !empty($member->email)){ ?>


								<a id="staff-member-email-id" data-toggle="tooltip" data-placement="top" title="click para copiar" mailto= "<?php echo $member->email;?>" class="staff-member-email text-uppercase font-light black hvr-grow"><i class="fas fa-envelope mr-2">
									

								</i><?php echo $member->email;?></a>

							<?php } ?>

							<?php  if($showPhone == 1 && !empty($member->phone)){

								if($showEmail == 1 && !empty($member->email)) echo '<br>';

								?>

								
								<a href="tel: + <?php echo $member->phone;?>" id="staff-member-phone-id"  class="staff-member-phone text-uppercase font-light black hvr-grow"><i class="fas fa-phone mr-2"></i><?php echo $member->phone;?></a>

							<?php } ?>

						</div>

					</div>

					<div class="sider-module col-lg-6 col-12 right-side float-right text-center">

						<div class="member-img-container">

							<img class="member-img img-fluid hvr-grow" 

							src='<?php echo $myImg;?>' 

							srcset='<?php echo $retina;?>'

							onerror="this.src='../private/img/templates/tutor-avatar/empty-avatar.jpg';
							this.srcset='../private/img/templates/tutor-avatar/empty-avatar@2x.jpg'"

							alt = "<?php echo $member->name . ' ' . $member->surname;?>"

							title = "<?php echo $member->name . ' ' . $member->surname;?>"

							>

						</div>

					</div>

				</div>

			<?php } ?>

		</div>

	</div>

	</div>

</div>