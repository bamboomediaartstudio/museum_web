<?php
?>

<div class="container-full museum-objectives objetivos">

	<div class="arrow-down"></div>

	<div class="mt-5">

		<div class="col-lg-12 text-center">

			<h2 class="section-heading module-title white">OBJETIVOS</h2>

			<hr class="module-separator" style='border:solid white;'>

		</div>

	</div>

	<div class="container">

		<div class="row d-inline-flex mb-5">

			<?php
			$objectivesQuery = DB::getInstance()->query('SELECT * FROM museum_objectives WHERE active = ? AND deleted = ? ORDER BY internal_order ASC', [1, 0]);

			foreach($objectivesQuery->results() as $objective){ 

				$menuFilter = '.filter-' . $objective->id;

				?>

				<div class="col-lg-4 col-md-4 col-12 objective-container">

					<div class="mt-5 d-inline-flex">

						<i class="fas fa-arrow-circle-right white text-center mt-1"></i>

						<p class="white mb-0 text-light objective-text">
							
							<?php
								
								$removeOpenParagraph = str_ireplace('<p>','', $objective->objective);
							
								$removeCloseParagraph = str_ireplace('<p>','', $removeOpenParagraph);
						
							 	echo $removeCloseParagraph;?>


						</p>

					</div>		

				</div>

			<?php } ?>

		</div>

	</div>

</div>