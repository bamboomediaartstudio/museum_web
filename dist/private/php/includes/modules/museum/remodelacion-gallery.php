<div id="visits-gallery" class="galeria-de-imagenes">

  <div class="container mt-5 mb-5">

    <div class="row">

      <div class="col-12">

        <h5 class="text-uppercase text-medium black content-title">IMÁGENES</h5>

        <hr class="left-separator content-separator">

      </div>

      <div id="blueimp-gallery" class="row blueimp-gallery blueimp-gallery-controls">

        <div class="slides"></div>

        <h3 class="title"></h3>

        <a class="prev">‹</a>

        <a class="next">›</a>

        <a class="close">×</a>

        <a class="play-pause"></a>

        <ol class="indicator"></ol>

      </div>

      <div id="links" class="container">

        <div class="row">

          
            <div class="col-md-4 col-lg-3 mt-5 hvr-grow">

              <a href="../private/img/remodelacion/1.jpg" title="Remodelación Museo Del Holocausto">

                <img class="img-thumbnail img-fluid" 

                src="../private/img/remodelacion/1.jpg" alt="Remodelación Museo Del Holocausto">

              </a>

            </div>

            <div class="col-md-4 col-lg-3 mt-5 hvr-grow">

              <a href="../private/img/remodelacion/2.jpg" title="Remodelación Museo Del Holocausto">

                <img class="img-thumbnail img-fluid" 

                src="../private/img/remodelacion/2.jpg" alt="Remodelación Museo Del Holocausto">

              </a>

            </div>

            <div class="col-md-4 col-lg-3 mt-5 hvr-grow">

              <a href="../private/img/remodelacion/3.jpg" title="Remodelación Museo Del Holocausto">

                <img class="img-thumbnail img-fluid" 

                src="../private/img/remodelacion/3.jpg" alt="Remodelación Museo Del Holocausto">

              </a>

            </div>

            <div class="col-md-4 col-lg-3 mt-5 hvr-grow">

              <a href="../private/img/remodelacion/4.jpg" title="Remodelación Museo Del Holocausto">

                <img class="img-thumbnail img-fluid" 

                src="../private/img/remodelacion/4.jpg" alt="Remodelación Museo Del Holocausto">

              </a>

            </div>

            <div class="col-md-4 col-lg-3 mt-5 hvr-grow">

              <a href="../private/img/remodelacion/5.jpg" title="Remodelación Museo Del Holocausto">

                <img class="img-thumbnail img-fluid" 

                src="../private/img/remodelacion/5.jpg" alt="Remodelación Museo Del Holocausto">

              </a>

            </div>

            <div class="col-md-4 col-lg-3 mt-5 hvr-grow">

              <a href="../private/img/remodelacion/6.jpg" title="Remodelación Museo Del Holocausto">

                <img class="img-thumbnail img-fluid" 

                src="../private/img/remodelacion/6.jpg" alt="Remodelación Museo Del Holocausto">

              </a>

            </div>

            <div class="col-md-4 col-lg-3 mt-5 hvr-grow">

              <a href="../private/img/remodelacion/7.jpg" title="Remodelación Museo Del Holocausto">

                <img class="img-thumbnail img-fluid" 

                src="../private/img/remodelacion/7.jpg" alt="Remodelación Museo Del Holocausto">

              </a>

            </div>

            <div class="col-md-4 col-lg-3 mt-5 hvr-grow">

              <a href="../private/img/remodelacion/8.jpg" title="Remodelación Museo Del Holocausto">

                <img class="img-thumbnail img-fluid" 

                src="../private/img/remodelacion/8.jpg" alt="Remodelación Museo Del Holocausto">

              </a>

            </div>

        </div>

      </div>

    </div>

  </div>

</div>