<?php

setlocale(LC_TIME, 'es_ES', 'esp_esp'); 

$added = strftime("%A, %d de %B del %Y", strtotime($itemResult->added));

$finalTime = utf8_encode($added);

$itemName = $itemResult->title;

$content = $itemResult->content;

$shareNews = $itemResult->allow_share;

$videosQuery = $db->query('SELECT * FROM museum_youtube_videos WHERE sid = ? AND source = ? AND active = ? AND deleted = ?', [$itemResult->mid, 'news_video', 1, 0]);

$totalVideos = $videosQuery->count();
?>

<div class="container" id="news-info">

	<input type="hidden" id="total-videos" name="total-videos" value="<?php echo $totalVideos;?>">

	<div class="row">

		<div class="col-md-9 col-12">

			<!-- about the news -->

			<div class="row mt-5">

				<div class="col-12 text-left">

					<h1 class="internal-page-content-main-title section-heading module-title text-uppercase"><?php echo $itemResult->title;?></h1>

					<p class="internal-page-content-main-date section-heading module-title text-uppercase"><?php echo $finalTime;?></p>

					<hr class="left-separator">

					<h4 class="internal-page-content-main-caption section-heading module-title text-justify"><?php echo $itemResult->caption;?></h4>

					<hr class="left-separator">

				</div>

				<div class="col-12">

					<p class="internal-page-content-main-content dark text-justify about-reflexiones module-text text-light"> <?php echo $content;?></p>

				</div>

			</div>

			<?php 

			$imagesQuery = $db->query(

				'SELECT * from museum_images WHERE sid = ? AND source = ? AND active = ? AND deleted = ? ORDER BY internal_order',

				[$itemResult->mid, 'individual_news', 1, 0]);

			if($imagesQuery->count()>=1){

				?>

				<div class="row mt-5">

					<div class="col-12 text-left">

						<h4 class="section-heading module-title">IMÁGENES</h4>

						<hr class="left-separator">

					</div>

				</div>

				<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">

					<div class="slides"></div>

					<h3 class="title"></h3>

					<a class="prev">‹</a>

					<a class="next">›</a>

					<a class="close">×</a>

					<a class="play-pause"></a>

					<ol class="indicator"></ol>

				</div>

				<div id="links" class="row">

					<?php 

					foreach($imagesQuery->results() as $actualImg){ 

						$imageThumb = '../../museumSmartAdmin/dist/default/private/sources/images/news/' . $itemResult->mid . '/images/' . $actualImg->unique_id . '/' . $itemResult->mid . '_' . $actualImg->unique_id . '_sq.jpeg';

						$imageThumbRetina = '../../museumSmartAdmin/dist/default/private/sources/images/news/' . $itemResult->mid . '/images/' . $actualImg->unique_id . '/' . $itemResult->mid . '_' . $actualImg->unique_id . '_sq@2x.jpeg';

						$imageRetina = '../../museumSmartAdmin/dist/default/private/sources/images/news/' . $itemResult->mid . '/images/' . $actualImg->unique_id . '/' . $itemResult->mid . '_' . $actualImg->unique_id . '_original@2x.jpeg';

						$caption = $actualImg->description;

						if($caption == null) $caption = 'imagen de la noticia ' . $itemName;

						?>
						<div class="col-6 col-md-4 col-lg-3 mt-5 thumbnail-container">

							<a href="<?php echo $imageRetina;?>" title="<?php echo $caption;?>">

								<img class="img-fluid hvr-grow" 

								src="<?php echo $imageThumb;?>" 

								srcset="<?php echo $imageThumbRetina;?>" 

								alt="<?php echo $caption;?>">

							</a>

						</div>

					<?php } ?>

				</div>

			<?php } ?> 

			<?php if($totalVideos>=1){ ?>

				<div class="row mt-5">

					<div class="col-12 text-left">

						<h4 class="section-heading module-title">VIDEOS</h4>

						<hr class="left-separator">

					</div>

				</div>

				<div id="blueimp-video-carousel" class="blueimp-gallery blueimp-gallery-controls blueimp-gallery-carousel">

					<div class="slides"></div>

					<h3 class="title"></h3>

					<a class="prev">‹</a>

					<a class="next">›</a>

					<a class="play-pause"></a>

				</div>

				<div id="video-links" class="row">

				</div>

			<?php } ?>


			<?php if($shareNews){ ;?>

				<div class="mt-5 mb-5 addthis_inline_share_toolbox_f3qw"></div>

			<?php }?>

		</div>

		<div class="general-sidebar news-sidebar col-3 d-none d-md-block">

			<div class="text-left mt-5">

				<h5 class="mb-5 section-heading module-title text-center">ÚLTIMAS NOTICIAS</h5>

			</div>

			<div class="col-12">

				<?php

				$itemsQuery = DB::getInstance()->query('

					SELECT *, mc.id as mid, img.unique_id 		as uid 

					FROM museum_news	 						as mc

					LEFT JOIN museum_images 					as img 		

					ON mc.id = img.sid AND img.source 			= ?

					WHERE mc.deleted = ? and mc.active = ?

					ORDER BY mid DESC LIMIT 5', 

					(array('news', 0, 1)));

				foreach($itemsQuery->results() as $lastItem){

					if($lastItem->mid == $itemResult->mid) continue;

					$lastItemName = $lastItem->title;

					$itemURL = '../' . $lastItem->url;

						//image...

					if($lastItem->uid == null){

						$myImg = '../../private/img/templates/image-template/template_medium.jpg';

						$myImgRetina = '../../private/img/templates/image-template/template_medium@2x.jpg';

					}else{

						$myImg = '../../museumSmartAdmin/dist/default/private/sources/images/news/' . $lastItem->mid . '/' . $lastItem->uid . '/' . $lastItem->mid . '_' . $lastItem->uid . '_medium.jpeg';

						$myImgRetina = '../../museumSmartAdmin/dist/default/private/sources/images/news/' . $lastItem->mid . '/' . $lastItem->uid . '/' . $lastItem->mid . '_' . $lastItem->uid . '_medium@2x.jpeg';
					}

					?>

					<a href="<?php echo $itemURL;?>">

						<div class="hover ehover12 general-sidebar-thumb" style="height: auto !important">

							<img class="img-fluid" 

							src="<?php echo $myImg;?>" 

							srcset="<?php echo $myImgRetina;?>" 

							alt="<?php echo $lastItemName;?>"

							title="<?php echo $lastItemName;?>">

							<div class="overlay lateral-item-overlay">

								<h2 class="font-light course-lateral-name" style="padding: 0 !important;"><?php echo $lastItemName;?> </h2>

							</div>

						</div>

					</a>

				<?php } ?>

			</div>

		</div>

	</div>

</div>