<div class="mt-5 page-body-content">

	<!--<div class="sm-offset-1 sm-col-8 offset-2 col-8 text-center mt-5">-->

		<div class="offset-1 col-10 offset-md-2 col-md-8 text-center mt-5">

			<h4 class="section-heading  text-medium"><?php echo $wording->get('news_first_line');
			;?></h4>

			<p class="section-description dark mt-5">

				<span class="text-light black">
					<?php echo $wording->get('news_second_line');?>
				</span>
			</p>

		</div>

		<div class="full-container mt-5">

			<div id="" class='wrapper text-center'>

				<div id="filters" class="btn-group-justified button-group btn-group-sm  filters-button-group mb-5">  

					<button class="filter-button btn   button" data-filter="*">TODAS</button>

					<?php

					$menuQuery = DB::getInstance()->query('SELECT * FROM museum_news_types WHERE active= ? AND deleted = ?', [1, 0]);

					foreach($menuQuery->results() as $menu){  ?>

						<button class="filter-button btn button" data-filter="<?php echo '.' . $menu->type_tag;?>"><?php echo $menu->type;?></button>
						
					<?php }

					?>

				</div>

			</div>

		</div>	

		<div class="container">

			<div class="grid">

				<?php

				$contenetQuery = DB::getInstance()->query('

					SELECT *, mc.id as mid, img.unique_id 		as uid 

					FROM museum_news 							as mc

					LEFT JOIN museum_images 					as img 		

					ON mc.id = img.sid AND img.source 			= ?

					AND img.deleted 							= ?

					WHERE mc.deleted 							= ? 

					AND mc.active 								= ? 

					ORDER by added DESC', 

					(array('news', 0, 0, 1)));

				$count = 0; 

				foreach($contenetQuery->results() as $item){ 

					$isOdd = false;

					$count+=1;

					if($count%2==0) $isOdd = true;

					$filters = '';

					$itemName = $item->title;

					$itemURL = $item->url;
					
					$caption = $item->caption;

					$content = $item->content;

					$itemId = $item->id;

					$date = utf8_encode(strftime("%A, %d de %B de %Y", strtotime($item->added)));

					//image...

					if($item->uid == null){

						$myImg = '../private/img/templates/image-template/template_medium.jpg';

						$myImgRetina = '../private/img/templates/image-template/template_medium@2x.jpg';

					}else{

						$myImg = '../museumSmartAdmin/dist/default/private/sources/images/news/' . $item->mid . '/' . $item->uid . '/' . $item->mid . '_' . $item->uid . '_medium.jpeg';

						$myImgRetina = '../museumSmartAdmin/dist/default/private/sources/images/news/' . $item->mid . '/' . $item->uid . '/' . $item->mid . '_' . $item->uid . '_medium@2x.jpeg';
					}

					$comunicado = '../private/img/templates/comunicado-template/comunicado.png';
					
					$shadow = '../private/img/templates/comunicado-template/shadow.png';

					$comunicadoRetina = '../private/img/templates/comunicado-template/comunicado@2x.png';

				//tags...

					$comunicado = false;

					$filterQuery = DB::getInstance()->query(

						'SELECT * FROM museum_news_types_relations as mcmr 

						INNER JOIN museum_news_types as mcm ON mcmr.id_type = mcm.id 

						WHERE mcmr.id_news = ?', [$item->mid]);

					foreach($filterQuery->results() as $filter){

						$filters .= $filter->type_tag . ' ';

						if($filter->type_tag == 'comunicados'){

							$comunicado = true;

							$myImg = '../private/img/templates/image-template/template_medium.jpg';

							$myImgRetina = '../private/img/templates/image-template/template_medium@2x.jpg';

						}

					}

					?>

					<div class="<?php echo $filters;?> col-lg-6 col-md-6 col-12 news-item news-list-container pr-1 pl-1">

						<div class="image-container mr-2 ml-2 mb-4">

							<a href="<?php echo $itemURL;?>">

								<img class="news-image hvr-grow img-fluid" 

								src="<?php echo $myImg;?>"

								srcset="<?php echo $myImgRetina;?>" 

								alt="<?php echo $itemName;?>"

								title="<?php echo $itemName;?>">

								<img class="img-shadow position-absolute img-fluid"  src="<?php echo $shadow;?>" 

								srcset ="<?php echo $shadow;?>" >

							</a>

							<!--</div>-->

							<h2 class="white text-truncate news-title position-absolute text-uppercase mt-xl-5 mt-md-4"><?php echo $itemName;?></h2>

							<!--<button class="read-btn btn  button btn-small">LEER NOTICIA</button>	-->

							<a href="<?php echo $itemURL;?>" role="button" class="read-btn btn button">LEER NOTICIA</a>

						</div>



					</div>

				<?php } ?>

			</div>

		</div>

		<div id="no-results" class="container d-none">

			<h4 class="mb-5 text-uppercase text-medium no-result-msg text-center">No se encontraron resultados para este filtro</h4>

		</div>

	</div>

