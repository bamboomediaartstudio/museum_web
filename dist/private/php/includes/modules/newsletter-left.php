<?php
?>

<section id="newsletter" class="newsletter-left">

	<div class="container">

		<div class="row">

			<form id="newsletter-form" class="col-12">

				<div class="row">

					<div class="col-lg-12">

						<h2 class="section-heading module-title text-uppercase"><?php echo $firstLine;?> <strong class="text-regular title text-uppercase"><?php echo $secondLine;?>?</strong></h2>


						<hr class="left-separator" style='border-color: #666 !important;'>

					</div>

					<div class="col-lg-12">

						<p class="module-text dark text-faded mb-5 about-reflexiones text-uppercase text-regular"><?php echo $thirdLine;?></p>

					</div>

				</div>

				<div class="row">

					<div class="col-lg-8">

						<input id="newsletter-input" name="name" type="text" class="newsletter-input mb-3 col-12 form-control form-input text-light form-control black text-light" placeholder="tu nombre.">

						<input id="newsletter-input" name="email" type="text" class="newsletter-input mb-3 form-control form-input text-light form-control black text-light" placeholder="tu email.">

						<div id="email_validate"></div><br><br>

						<span class="input-group-btn">

							<button id="subscribe-button" class="hvr-sweep-to-right btn btn-square" type="submit">SUSCRIBIRME</button>

						</span>

					</div>

				</div>

			</form>

		</div>

	</div>

</section>