<?php
?>

<section id="newsletter">

	<div class="container">

		<div class="row">

			<form id="newsletter-form" class="col-12">

				<div class="row">

					<div class="col-lg-12 text-center">

						<h2 class="section-heading module-title">¿TE INTERESAN NUESTRAS <strong class="text-regular">NOTICIAS?</strong></h2>


						<hr class="module-separator">

					</div>

					<div class="col-lg-8 mx-auto">

						<p class="module-text dark text-faded mb-5 text-center about-reflexiones text-uppercase text-regular">Suscribite a nuestro newsletter y recibilas en tu email.</p>

					</div>

				</div>

				<div class="row">

					<div class="col-12">

						<div class="row ml-3 mr-3">

							<input id="newsletter-name" name="name" type="text" class="newsletter-input col-md-4 mb-4 col-12 form-control form-input text-light form-control black text-light" placeholder="nombre" title="nombre newsletter">

							<input id="newsletter-email" name="email" type="text" class="newsletter-input ml-md-2 mr-md-2 col-md-4 mb-4 col-12 form-control form-input text-light form-control black text-light" placeholder="email" title="email newsletter">

							<button id="subscribe-button" class="col-12 mb-4 col-md-3 hvr-sweep-to-right btn btn-small btn-square" type="submit">SUSCRIBIRME</button>

						</div>

						<div id="email_validate" class="ml-3"></div>


					</div>

				</div>

			</form>

		</div>

	</div>

</section>