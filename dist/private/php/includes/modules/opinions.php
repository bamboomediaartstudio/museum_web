<?php
?>
<section id="opinions">

	<div class="container">

		<div class="row">

			<?php

			$smQuery = DB::getInstance()->query('

				SELECT *, mc.id as mid, img.unique_id 		as uid 

				FROM museum_opinions						as mc

				LEFT JOIN museum_images 					as img 		

				ON mc.id = img.sid AND img.source 			= ?

				AND img.deleted 							= ?

				WHERE mc.deleted 							= ? 

				AND mc.active 								= ? 

				ORDER by mc.internal_order ASC', 

				(array('opinions', 0, 0, 1)));

			$count = 0; 

			foreach($smQuery->results() as $result){ 

				if($result->uid == null){

					$myImg = 'private/img/templates/image-template/template_medium_sq.jpg';

					$myImgRetina = 'private/img/templates/image-template/template_medium_sq@2x.jpg';

				}else{

					$myImg = 'museumSmartAdmin/dist/default/private/sources/images/opinions/' . $result->mid . '/' . $result->uid . '/' . $result->mid . '_' . $result->uid . '_edited.jpeg';

					$myImgRetina = 'museumSmartAdmin/dist/default/private/sources/images/opinions/' . $result->mid . '/' . $result->uid . '/' . $result->mid . '_' . $result->uid . '_edited@2x.jpeg';

				}

				?>

				<div class="opinion-item margin-right col-lg-6 col-md-12 text-left pt-5 pb-5">

					<div class="hvr-grow mb-4 col-lg-3 col-md-3 col-12 float-left opinion-img-container flex">

						<img class="rounded-circle img-fluid opinion-img mx-auto d-block" src="<?php echo $myImgRetina;?>">

					</div>

					<blockquote class="blockquote col-lg-9 col-md-9 float-right">

						<p class="text-muted mb-0 text-light text-up quote text-center text-md-left text-lg-left"><?php echo $result->quote;?></p>

						<small class="blockquote-footer text-bold text-center text-md-left text-lg-left">

							<?php echo $result->name . ' ' . $result->surname . ', ' . $result->role . ' - ' . $result->company;?>


						</small>

						<small class="d-block text-light text-center text-md-left text-lg-left"><cite title="Source Title"></cite> - 10 de marzo de 2018</small>

					</blockquote>

				</div>

			<?php }  ?>

		</div>

	</div>

</section>