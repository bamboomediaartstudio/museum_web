<?php

$path = "../private/img/cuadernos";

$names = ["JUSTOS Y SALVADORES", "LAS DOS GUERRAS DEL NAZISMO", "RESISTIR Y SOBREVIVIR", "CARAS DE LO HUMANO: VÍCTIMAS, PERPETRADORES Y TESTIGOS", "MUJERES, VIDAS Y DESTINOS", "CONVIVIR CON EL MAL: GENOCIDIOS DEL SIGLOXX", "ESCAPES Y DESTINOS: DIMENSIÓN GEOGRÁFICA DE LA SHOÁ"];

if ($handle = opendir($path)) {

      $items = [];

      while (false !== ($file = readdir($handle))) {

            if ('.' === $file) continue;

            if ('..' === $file) continue;

            $path_parts = pathinfo($file);

            array_push($items, $path_parts['filename']);

      }

      sort($items);

      closedir($handle);
}

?>

<section id="cuadernos">

    <div class="container mt-5">

        <div class="row">

            <div class="col-lg-12 text-center">

                <h2 class="section-heading module-title text-uppercase">GENERACIONES DE LA SHOÁ EN ARGENTINA...</h2>

                <hr class="module-separator">

            </div>

            <div class="col-lg-8 mx-auto">

                <p class="mb-5 text-center about-reflexiones module-text text-light">

                    Generaciones de la Shoá en Argentina es una organización constituida por voluntarios, cuya misión es estudiar, investigar, educar y transmitir las lecciones del Holocausto.

                    <br>
                    <br> Fundada por sobrevivientes de la Shoá, sus descendientes y familiares, está integrada y sostenida también hoy por personas convencidas de que su estudio es esencial para la prevención, detección y freno de los procesos totalitarios y genocidas. Con ese propósito, desarrollan proyectos, crean materiales pedagógicos y realizan múltiples actividades testimoniales, educativas y culturales. La memoria del Holocausto sostiene estas acciones proyectadas hacia un futuro anhelado sentando los fundamentos para que la dignidad, la ética y la consideración por el prójimo se conviertan en el pilar de la sociedad humana.

                    <br>
                    <br> Nació en 1997 como un grupo autogestivo integrado por sobrevivientes -muchos que fueron niños durante la Shoá-, sus hijos y nietos. En reuniones realizadas en sus propias casas, las distintas generaciones encontraron un espacio para compartir experiencias y transmitirlas. Con la sucesión de los encuentros, los intercambios se fueron centrando en los aspectos vitales y de reconstrucción. El propósito era doble, por un lado, elaborar lo traumático y, por el otro, transmitir lo vivido y aprendido.

                    <br>
                    <br> Aquellas informales reuniones fueron creciendo hasta culminar con un encuentro de gran envergadura en 2004: “De Cara al Futuro”, el Primer Encuentro Internacional en habla hispana, que marcó el inicio de “Generaciones de la Shoá en Argentina” como institución.

                    <br>
                    <br> En 2007 fue invitada por Sherit Hapleitá, la Asociación Israelita de Sobrevivientes de la Persecución Nazi, a trabajar en su sede de Paso 422. Desde entonces Generaciones de la Shoá se asume como heredera del legado de Sherit Hapleitá.

                </p>

            </div>

        </div>

    </div>

    <div class="container">

        <div class="col-lg-12 text-center mt-5">

            <h4 class="section-heading module-title text-uppercase">PARA MÁS INFORMACIÓN:</h4>

            <hr class="module-separator">

        </div>

        <div class="row mb-5">

            <div class="col-lg-4 col-md-4 text-center">

                <div class="service-box mt-5 mx-auto sr-icon-1">

                    <span class="fa-stack fa-2x center">

                                <i class="fal fa-circle fa-stack-2x"></i>

                                <i class="fas fa-globe fa-stack-1x"></i>

                          </span>

                    <h3 class="mt-3 mb-3 text-medium black ">SITIO WEB</h3>

                    <p class="text-muted mb-0 text-light text-up">

                        <a href="http://generaciones-shoa.org.ar" target="_blank">http://generaciones-shoa.org.ar</a>

                    </p>

                </div>

            </div>

            <div class="col-lg-4 col-md-4 text-center">

                <div class="service-box mt-5 mx-auto sr-icon-1">

                    <span class="fa-stack fa-2x center">

                                <i class="fal fa-circle fa-stack-2x"></i>

                                <i class="fas fa-map fa-stack-1x"></i>

                          </span>

                    <h3 class="mt-3 mb-3 text-medium black ">LUGAR</h3>

                    <p class="text-muted mb-0 text-light text-up">Asoc. Civil I.G.J. Nro. 1.738.527Paso 422 Piso 2 - C1031ABJ
                    </p>

                </div>

            </div>

            <div class="col-lg-4 col-md-4 text-center">

                <div class="service-box mt-5 mx-auto sr-icon-1">

                    <span class="fa-stack fa-2x center">

                                <i class="fal fa-circle fa-stack-2x"></i>

                                <i class="fas fa-envelope fa-stack-1x"></i>

                          </span>

                    <h3 class="mt-3 mb-3 text-medium black ">CONTACTO</h3>

                    <p class="text-muted mb-0 text-light text-up">

                        <a href="mailto:secretaria@generaciones-shoa.org.ar">secretaria@generaciones-shoa.org.ar</a>

                    </p>

                </div>

            </div>

        </div>

    </div>

    <div class="container mt-5">

        <div class="row">

            <div class="col-lg-12 text-center mt-5">

                <h2 class="section-heading module-title text-uppercase">CUADERNOS DE LA SHOÁ</h2>

                <hr class="module-separator">

            </div>

            <div class="col-lg-8 mx-auto">

                <p class="dark text-faded mb-5 text-center about-reflexiones module-text text-light">

                    Es una publicación anual destinada a docentes. Aborda en cada número, de manera moderna y pedagógica, una temática diferente relativa a la Shoá. Acompañada de una propuesta didáctica, es una potente herramienta para el trabajo en el aula. Se entrega gratuitamente a escuelas e instituciones.
                    <br>
                    <br> Nuestra editorial crea, edita y publica diversos materiales educativos. También ofrece publicaciones de otras editoriales escritos por nuestros integrantes.

                </p>

                <ul class="text-center text-uppercase font-light" style="padding-left: 0; list-style: none;">

                    <li>Colección “Cuadernos de la Shoá”.</li>

                    <li>Propuestas Pedagógicas.</li>

                    <li>Testimonios de sobrevivientes.</li>

                    <li>Ensayos y trabajos conceptuales.</li>

                </ul>

            </div>

        </div>

    </div>

</section>

<div class="album py-5 bg-light">

    <div class="container">

        <div class="row">

            <?php

              $count = 0;

              foreach ($items as $value) {

                $image = '../private/img/cuadernos/' . $value . '.jpg';

                $name = $names[$count];

                $number = 'NÚMERO ' . $value;

                $alt = 'Nuestra Memoria N ' . $value;

                $filePath = '../private/pdf/nuestra-memoria/nuestra-memoria-' . $value . '.pdf';

                $fileName = 'nuestra-memoria-' . $value . '.pdf';

                $count+=1;

                ?>

                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">

                    <div class="card mb-4 box-shadow" style='overflow:hidden !important;'>

                        <img class="card-img-top hvr-float" src="<?php echo $image;?>" alt="<?php echo $alt;?>">

                        <div class="card-body">

                            <p class="card-text text-light text-uppercase text-dark">
                                <?php echo $number;?>
                            </p>

                            <div class="text-center justify-content-between align-items-center">

                                <div class="btn-group btn-group-sm">

                                    <a target='_blank' class="hvr-sweep-to-right btn-square btn btn-sm btn-outline-secondary" href="<?php echo $filePath;?>" role="button">VER</a>

                                

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <?php } ?>

            </div>

        </div>

    </div>