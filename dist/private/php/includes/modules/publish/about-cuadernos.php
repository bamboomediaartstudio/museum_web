<?php

$path = "../../private/img/cuadernos";

if ($handle = opendir($path)) {

  $items = [];

  while (false !== ($file = readdir($handle))) {

    if ('.' === $file) continue;

    if ('..' === $file) continue;

    $path_parts = pathinfo($file);

    array_push($items, $path_parts['filename']);

  }

  sort($items);

  closedir($handle);

}

?>

<div class="container memory-page">

  <div class="col-12 text-center">

    <img class="books-image img-fluid hvr-grow" 

    src="../../private/img/store/cuadernos.png" 

    srcset="../../private/img/store/cuadernos@2x.png" 

    alt="Cuadernos de La Shoá"

    title="Cuadernos de La Shoá"

    >

  </div>

  <div class="row mt-5">

    <div class="offset-2 col-8 text-center">

      <h4 class="section-title section-heading text-medium">CUADERNOS DE LA SHOÁ</h4>

      <p class="dark mt-5 ">
        
        <span class="black text-light section-text">
          Es una publicación anual, destinada a docentes y capacitadores que aborda en cada número una temática diferente relativa al Holocausto, acompañada de una propuesta didáctica que los convierte en una herramienta indispensable para el trabajo en el aula. Creados, diseñados y redactados por un equipo especializado, enfocan las diferentes temáticas del Holocausto de manera novedosa y profunda con un lenguaje fácilmente comprensible para el lector.<br><br>La parte viva y humana de los Cuadernos de la Shoá la constituye lo testimonial: cada tema y cada capítulo contienen la voz de los sobrevivientes, sus protagonistas. Los testimonios acercan a los lectores la experiencia personal que a veces los relatos históricos dejan de lado. Son las historias en la Historia.<br><br>Además del contenido conceptual, las fotografías, mapas, gráficos e ilustraciones, cada número incluye una exhaustiva referencia fílmica y bibliográfica – textos impresos o virtuales- que permite seguir ampliando la temática y enriquecerla.<br><br>Las tapas y contratapas son obra de reconocidos artistas que colaboran desinteresadamente con la publicación.<br><br>Se entrega sin cargo a escuelas e instituciones.<br><br>Si el Holocausto es el paradigma del Mal, sus lecciones son esenciales para estudiar, conocer y reconocer precozmente los indicios de totalitarismos y procesos genocidas. El objetivo de esta publicación, es aportar elementos que hagan posible la convivencia pacífica y armónica entre las sociedades humanas.<br><br>

        </span>
      </p>

      <h4 class="section-title section-heading text-medium">STAFF</h4>

      <p class="dark mt-5 ">

        <span class="black text-light section-text">

        <strong>Editor Responsable:</strong><br> Aida Jurkiewicz Ender<br><br>
        
        <strong>Coordinación de contenidos, compilación y edición:</strong><br>Aída Jurkiewicz Ender y Diana Wang<br><br>

        <strong>Coordinación de auspicios:</strong><br>Karen Rofchuk y Aida Jurkiewicz Ender<br><br>

        <strong>Colaboración:</strong><br>José Blumenfeld, Jonatan Epsztejn, Ruth Fleischer, Susana Grinspan, Feigue Machabanski, Viviana Rosenthal, Rosa Rotenberg, Natalia Rus, Angela Waksman<br><br>

        <strong>Revisión y Corrección de Textos:</strong><br>José Blumenfeld, Aída Jurkiewicz Ender, Rosa Rotenberg, Diana Wang<br><br>

        <strong>Traducciones:</strong><br>José Blumenfeld<br><br>

        <strong>Diseño Gráfico:</strong><br>Melisa Berlin y Fernando Ender<br><br>


      </span>
      </p>
      

    </div>

  </div>

  <!--<div class="container">

    <div class="col-lg-12 text-center mt-5">

      <h4 class="section-heading module-title text-uppercase more-information">PARA MÁS INFORMACIÓN:</h4>

      <hr class="module-separator">

    </div>

    <div class="row mb-5">

      <div class="col-lg-4 col-md-4 text-center info-block">

        <div class="service-box mt-5 mx-auto sr-icon-1">

          <span class="fa-stack fa-2x center hvr-grow">

            <i class="fal fa-circle fa-stack-2x"></i>

            <i class="fas fa-globe fa-stack-1x"></i>

          </span>

          <h3 class="mt-3 mb-3 text-medium ">SITIO WEB</h3>

          <p class="text-muted mb-0 text-light text-up">

            <a href="http://generaciones-shoa.org.ar" target="_blank">http://generaciones-shoa.org.ar</a>

          </p>

        </div>

      </div>

      <div class="col-lg-4 col-md-4 text-center info-block">

        <div class="service-box mt-5 mx-auto sr-icon-1">

          <span class="fa-stack fa-2x center hvr-grow">

            <i class="fal fa-circle fa-stack-2x"></i>

            <i class="fas fa-map fa-stack-1x"></i>

          </span>

          <h3 class="mt-3 mb-3 text-medium">LUGAR</h3>

          <p class="text-muted mb-0 text-light text-up">Asoc. Civil I.G.J. Nro. 1.738.527Paso 422 Piso 2
          </p>

        </div>

      </div>

      <div class="col-lg-4 col-md-4 text-center info-block">

        <div class="service-box mt-5 mx-auto sr-icon-1">

          <span class="fa-stack fa-2x center hvr-grow">

            <i class="fal fa-circle fa-stack-2x"></i>

            <i class="fas fa-envelope fa-stack-1x"></i>

          </span>

          <h3 class="mt-3 mb-3 text-medium ">CONTACTO</h3>

          <p class="text-muted mb-0 text-light text-up">

            <a href="mailto:secretaria@generaciones-shoa.org.ar">secretaria@generaciones-shoa.org.ar</a>

          </p>

        </div>

      </div>

    </div>

  </div>-->

  <!--<div class="container mt-5">

    <div class="row">

      <div class="col-lg-12 text-center mt-5">

        <h2 class="section-heading module-title text-uppercase cuadernos-title">CUADERNOS DE LA SHOÁ</h2>

        <hr class="module-separator">

      </div>

      <div class="col-lg-8 mx-auto">

        <p class="dark text-faded mb-5 text-center about-reflexiones module-text text-light cuadernos-info-content">

          Es una publicación anual destinada a docentes. Aborda en cada número, de manera moderna y pedagógica, una temática diferente relativa a la Shoá. Acompañada de una propuesta didáctica, es una potente herramienta para el trabajo en el aula. Se entrega gratuitamente a escuelas e instituciones.
          <br>
          <br> Nuestra editorial crea, edita y publica diversos materiales educativos. También ofrece publicaciones de otras editoriales escritos por nuestros integrantes.

        </p>

        <ul class="text-center text-uppercase font-light" style="padding-left: 0; list-style: none;">

          <li class="cuadernos-list-item mb-4">Colección “Cuadernos de la Shoá”.</li>

          <li class="cuadernos-list-item mb-4">Propuestas Pedagógicas.</li>

          <li class="cuadernos-list-item mb-4">Testimonios de sobrevivientes.</li>

          <li class="cuadernos-list-item mb-4">Ensayos y trabajos conceptuales.</li>

        </ul>

      </div>

    </div>

  </div>-->

</div>

<div class="album py-5 bg-light">

  <div class="container">

    <div class="row">

      <?php

      $array = array();

      $array[] = (object) array('name' => 'My name');


      foreach ($items as $value) {

        $path = '';

        $path2 = '';

        $name = '';

        if($value == 1){

          //$path = 'http://www.youblisher.com/p/1000235-Cuadernos-de-la-Shoa-N-1-Justos-y-Salvadores-2010/';

          $path = '../../private/pdf/cuadernos-de-la-shoa/cuaderno_1.pdf';

          $name = 'Cuadernos de la Shoá N°1 “Justos y Salvadores”';

          //$filename = 'cuaderno_1_justos_y_salvadores.pdf';

          $description = "Cuadernos de la Shoá N°1 “Justos y Salvadores”: Rescata las historias de aquellos que en medio del terror, el caos, los prejuicios, la intensa propaganda, los castigos y las penurias, se sobrepusieron al miedo y tendieron su mano a los judíos sentenciados al infortunio y a la muerte. Los Justos y Salvadores representan la esperanza en el género humano y transmitir estas lecciones sobre la solidaridad, la generosidad, el altruismo y la bondad, está dirigido a generar y estimular la conducta de “hacer lo que hay que hacer porque es lo que hay que hacer”.";

        }else if($value ==2){

          $path = '../../private/pdf/cuadernos-de-la-shoa/cuaderno_2.pdf';

          $name = 'Cuadernos de la Shoá N°2 “Las dos Guerras del Nazismo”';

         // $filename = 'cuaderno_2_las_dos_guerras_del_nazismo.pdf';

          $description = "Cuadernos de la Shoá N°2 “Las dos Guerras del Nazismo”: Propone un recorrido temporal simultáneo del Holocausto y la Segunda Guerra Mundial. En páginas contrapuestas se ilustra el paralelo de lo sucedido en ambos frentes, en la misma línea del tiempo. Esta simultaneidad en tiempo y espacio permite estudiarlos como eventos intrínsecamente relacionados y revela las contradicciones a que condujo esta interdependencia. Se hace evidente que el plan de exterminio del pueblo judío no se detuvo ni siquiera cuando ya la guerra estaba claramente perdida en el frente bélico.";

        }else if($value == 3){

          $path = '../../private/pdf/cuadernos-de-la-shoa/cuaderno_3.pdf';

          $name = 'Cuadernos de la Shoá N°3 “Resistir y sobrevivir”';

         // $filename = 'cuaderno_3_resistir_y_sobrevivir.pdf';

          $description = "Cuadernos de la Shoá N°3 “Resistir y sobrevivir”: Expone las múltiples resistencias del pueblo judío en las distintas etapas de su tormento. Desde su exclusión de la condición de ciudadanos hasta el plan de la “Solución Final”, en guetos, campos, escondites y bosques los judíos encontraron múltiples y creativas formas para seguir viviendo, para mantenerse humanos, para enfrentarse y luchar y para evitar ser asesinados. Se describen las resistencias de supervivencia, las culturales, espirituales y la resistencia armada.";

        }else if($value == 4){

          $path = '../../private/pdf/cuadernos-de-la-shoa/cuaderno_4.pdf';

          $name = 'Cuadernos de la Shoá N°4 “Caras de lo Humano”';

          // $filename = 'cuaderno_4_caras_de_lo_humano.pdf';

           $description = "Cuadernos de la Shoá N°4 “Caras de lo Humano”: Expone la condición y grados de la deshumanización tanto de las víctimas como de los perpetradores y los testigos. Revela lo observable en todo proceso genocida, la ausencia de límites morales, la dura evidencia de que “no hay nada que un ser humano no pueda hacerle a otro”. El plan de exterminio deshumanizó a las víctimas para hacer posible su asesinato, pero al mismo tiempo, deshumanizó a los asesinos y a los testigos pasivos que observaron lo que sucedía frente a sus ojos sin intervenir.";

          
        }else if($value == 5){

          $path = '../../private/pdf/cuadernos-de-la-shoa/cuaderno_5.pdf';

          $name = 'Cuadernos de la Shoá N°5 “Mujeres. Vidas y destinos”';

         // $filename = 'cuaderno_5_mujeres_vidas_y_destinos.pdf';

          $description = "Cuadernos de la Shoá N°5 “Mujeres. Vidas y destinos”: Encara las especificidades que vivieron las mujeres, por ser mujeres, en la II Guerra Mundial. A partir de esta guerra, la mayoría de las víctimas fueron civiles, entre ellas, las mujeres y los niños. El rol social y cultural de las mujeres, como portadoras y transmisoras de vida y cuidadoras de sus familias, determinó en las víctimas judías y también en las perpetradoras y las testigos indiferentes, una afectación particular en aquellos aspectos característicos de su lugar como mujeres en la sociedad.";

        }else if($value == 6){

          $name = 'Cuadernos de la Shoá N°6 “Convivir con el MAL. Genocidios del Siglo XX”';

          $path = '../../private/pdf/cuadernos-de-la-shoa/cuaderno_6.pdf';

         // $filename = 'cuaderno_6_convivir_con_el_mal_genocidios_del_siglo_xx.pdf';

          $description = "Cuadernos de la Shoá N°6 “Convivir con el MAL. Genocidios del Siglo XX”: Se expone y desarrolla en este número el concepto del Mal. En la búsqueda de una idea que incluya los diferentes sucesos que a lo largo del siglo XX han sembrado horror, destrucción y muerte en el mundo, el Mal aparece como el hilo conductor, como el paradigma del siglo XX, la forma más extrema de una enfermedad que aflige a la Humanidad. La Shoá es tomada como punto de partida para su estudio y comprensión.";


        }else if($value ==7){

          $path = '../../private/pdf/cuadernos-de-la-shoa/cuaderno_7.pdf';

          $name = 'Escapes y destinos: Dimensión geográfica de La Shoá';

          //$filename = 'cuaderno_7_escapes_y_destinos.pdf';

          $description = "Cuaderno de la Shoá N°7 “Escapes y Destinos. Dimensión Geográfica de la Shoá”:  Encara los recorridos del pueblo judío de manera cronológica desde las primeras dispersiones hasta sus variados destinos después de la Shoá. Los contenidos se acompañan con mapas, cuadros e infografías. Exponer los caminos, los orígenes y los destinos, las idas y vueltas a lo largo del mundo y de la historia, muestra al pueblo judío de una manera visual que permite que la experiencia pueda ser comprendida y valorada. En todas estas trayectorias están presentes los sobrevivientes y sus descendientes, los que finalmente encontraron su destino, los que apostaron a la vida y sembraron sus mejores semillas para un futuro de paz.";

        }else if($value ==8){

          $path = '../../private/pdf/cuadernos-de-la-shoa/cuaderno_8.pdf';

          $name = 'Los niños sin infancia';

         // $filename = 'cuaderno_8_ninos_sin_infancia.pdf';

          $description = "Cuaderno de la Shoá N°8 “Escapes y Destinos. Dimensión Geográfica de la Shoá”:  De todas las victimas de la guerra, los niños son víctimas supremas y universales.";

        }

        $image = '../../private/img/cuadernos/' . $value . '.png';

        $number = 'NÚMERO ' . $value;

        $alt = 'Nuestra Memoria N ' . $value;

        $filePath = '../../private/pdf/nuestra-memoria/nuestra-memoria-' . $value . '.pdf';

        $fileName = 'cuadernos_de_la_shoa_numero_' . $value . '.pdf';

        ?>

        <div class="col-md-6 d-flex cuaderno-image-contenet">

          <div class="book-card mb-4" style='overflow:hidden !important;'>

            <div class="gray-img-container">

              <img class="memory-img card-img-top hvr-grow" src="<?php echo $image;?>" alt="<?php echo $title;?>">

            </div>

            <div class="card-body">

              <input type="hidden" class="description" value="<?php echo $description;?>">

              <p class="text-center book-name card-text text-medium text-uppercase text-truncate">

                CUADERNOS DE LA SHOA

              </p>

              <p class="text-center book-author card-text text-light text-uppercase text-dark text-truncate">

                <?php echo $name;?>

              </p>

              <div class="push-to-bottom text-center">

                <div class="btn-group">


                 <a target="" data-title = "<?php echo $name;?>"  data-info = '<?php echo $description;?>' class="read-info more-info hvr-sweep-to-right btn-square btn btn-sm btn-outline-secondary" role="button">INFO</a>

               </div>

                <div class="btn-group">


                  <a target="_blank" class="more-info hvr-sweep-to-right btn-square btn btn-sm btn-outline-secondary" href="<?php echo $path;?>" role="button">LEER ONLINE</a>


                </div>

                <div class="btn-group">


                  <a class="more-info hvr-sweep-to-right btn-square btn btn-sm btn-outline-secondary" href="<?php echo $path;?>" role="button" download='<?php echo $fileName;?>'>DESCARGAR</a>

              


                </div>

              </div>

            </div>

          </div>

        </div>

        <?php

      }
      ?>

    </div>
  </div>
</div>