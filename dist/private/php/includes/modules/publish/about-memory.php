<?php

$path = "../../private/img/memoria";

if ($handle = opendir($path)) {

  $items = [];

  while (false !== ($file = readdir($handle))) {

    if ('.' === $file) continue;

    if ('..' === $file) continue;

    $path_parts = pathinfo($file);

    array_push($items, $path_parts['filename']);

  }

  sort($items);

  closedir($handle);

}

?>

<div class="container memory-page">

  <div class="col-12 text-center">

    <img class="books-image img-fluid" 

    src="../../private/img/store/memory.png" 

    srcset="../../private/img/store/memory@2x.png" 

    alt="Nuestra Memoria"

    title="Nuestra Memoria"

    >

  </div>

  <div class="row mt-5">

    <div class="offset-2 col-8 text-center">

      <h4 class="section-heading  text-medium">NUESTRA MEMORIA</h4>

      <p class="dark mt-5">
        <span class="text-light black">
          Año a año, el Museo publica "Nuestra Memoria", un libro con artículos sobre la Shoá / Holocausto, escritos por sobrevivientes y expertos académicos.
        </span>
      </p>

    </div>

  </div>


</div>

<div class="album py-5 bg-light">

  <div class="container">

    <div class="row">

      <?php

      foreach ($items as $value) {

        $image = '../../private/img/memoria/' . $value . '.png';

        $number = 'NÚMERO ' . $value;

        $alt = 'Nuestra Memoria N ' . $value;

        $filePath = '../../private/pdf/nuestra-memoria/nuestra-memoria-' . $value . '.pdf';

        $fileName = 'nuestra-memoria-' . $value . '.pdf';

        ?>

        <div class="col-md-4 d-flex">

          <div class="mb-4">

            <div class="gray-img-container">

              <img class="memory-img card-img-top hvr-grow" src="<?php echo $image;?>" alt="<?php echo $title;?>">

            </div>

            <div class="card-body">

              <input type="hidden" class="description" value="<?php echo $description;?>">

              <p class="text-center book-name card-text text-medium text-uppercase text-truncate">

                NUESTRA MEMORIA

              </p>

              <p class="text-center book-author card-text text-light text-uppercase text-dark text-truncate">

                <?php echo $number;?>

              </p>

              <div class="push-to-bottom text-center">

                <div class="btn-group">

                  <a class="more-info hvr-sweep-to-right btn-square btn btn-sm btn-outline-secondary" href="<?php echo $filePath;?>" role="button">VER ONLINE</a>

                </div>

                <div class="btn-group">

                  <a class="more-info hvr-sweep-to-right btn-square btn btn-sm btn-outline-secondary" href="<?php echo $filePath;?>" role="button" download='<?php echo $fileName;?>'> DESCARGAR</a>

                </div>

              </div>

            </div>

          </div>

        </div>

        <!--<div class="col-md-3">

          <div class="card mb-4 box-shadow" style='overflow:hidden !important;'>

            <img class="memory-img card-img-top hvr-grow" src="<?php echo $image;?>" alt="<?php echo $alt;?>">

            <div class="card-body">

              <p class="card-text text-medium text-uppercase">Nuestra Memoria</p>

              <p class="card-text text-light text-uppercase text-dark"><?php echo $number;?></p>

              <div class="d-flex justify-content-between align-items-center">

                <div class="btn-group">

                  <a target='_blank' class="hvr-sweep-to-right btn-square btn btn-sm btn-outline-secondary" href="<?php echo $filePath;?>" role="button">VER ONLINE</a>

                  <a class="hvr-sweep-to-right btn-square btn btn-sm btn-outline-secondary" 
                  href="<?php echo $filePath;?>" role="button" download='<?php echo $fileName;?>'>DESCARGAR</a>

                </div>

              </div>

            </div>

          </div>

        </div>-->

        <?php

      }
      ?>

      

      

      
      
      

      
    </div>
  </div>
</div>