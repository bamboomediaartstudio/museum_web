<section id="inline-store">

    <div class="container">

        <div class="col-12 text-center">

            <img class="books-image img-fluid" src="../../private/img/store/books.png" alt="Libros del museo">

        </div>

        <div class="row mt-5">

            <div class="offset-2 col-8 text-center">

                <h4 class="section-heading  text-medium">DESCARGÁ EL CATÁLOGO</h4>

                <p class="dark mt-5">
                    <span class="text-light black">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore <span class="text-bold">magna aliqua.</span> Ut enim ad minim veniam, quis nostrud <span class="text-bold">exercitation ullamco</span> laboris.
                    </span>
                </p>

            </div>

        </div>

        <div class="mt-5 col-12 text-center">

            <a href="../private/pdf/Biblioteca-Shoa-Base-de-Datos-1.pdf" target="_blank" download="Biblioteca-Shoa-Base-de-Datos-1.pdf" class="hvr-sweep-to-right btn btn-square" role="button">DESCARGAR</a>

        </div>

    </div>

</section>

<div class="album py-5 bg-light">

    <div class="container">

        <div class="row">

            <?php

                  $json = file_get_contents('../private/json/libros.json');

                  $array = json_decode($json, TRUE);

                  $count = 0;

                  foreach($array as $item) { 

                        //echo 'enter: ' . $item['title'] ;

                        $cover = '../private/img/' . $item['cover'];

                        $title = $item['title'];

                        $author = $item['author']; 

                        $isbn = 'ISBN: ' . $item['isbn']; 

                        $published = 'publicado: ' . $item['published']; 

                        $description = $item['description'];

                        ?>

                <div class="col-md-3">

                    <div class="card mb-4 box-shadow" style='overflow:hidden !important;'>

                        <img class="memory-img card-img-top hvr-float" src="<?php echo $cover;?>" alt="<?php echo $alt;?>">

                        <div class="card-body">

                            <input type="hidden" class="description" value="<?php echo $description;?>">

                            <p class="card-text text-medium text-uppercase">

                                <?php echo $title;?>

                            </p>

                            <p class="card-text text-light text-uppercase text-dark">

                                <?php echo $author;?>

                            </p>

                            <div class="d-flex justify-content-between align-items-center">

                                <div class="btn-group">

                                    <a class="more-info hvr-sweep-to-right btn-square btn btn-sm btn-outline-secondary" href="<?php echo $filePath;?>" role="button">DESCRIPCIÓN</a>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <?php } ?>

        </div>
   
  </div>

</div>