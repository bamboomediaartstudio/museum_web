<section id="inline-store" class="books-page">

    <div class="container">

        <div class="col-12 text-center books-main-image">

            <img class="books-image img-fluid hvr-grow" 

            src="../../private/img/store/books.png" 

            srcset="../../private/img/store/books@2x.png" 

            alt="Libros del museo"
            
            title="Libros del museo"

            >

        </div>

        <div class="row mt-5">

            <div class="offset-2 col-8 text-center">

                <h4 class="section-heading text-medium books-title"><?php echo $wording->get('books_first_line');?></h4>

                <p class="dark mt-5">
                    <span class="text-light black books-description">
                        <?php echo $wording->get('books_second_line');?>
                    </span>
                </p>

            </div>

        </div>

        <!--<div class="mt-5 col-12 text-center">

            <a href="../private/pdf/Biblioteca-Shoa-Base-de-Datos-1.pdf" target="_blank" download="Biblioteca-Shoa-Base-de-Datos-1.pdf" class="hvr-sweep-to-right btn btn-square" role="button">DESCARGAR</a>

        </div>-->

    </div>

</section>

<div class="album py-5 bg-light">

    <div class="container">

        <div class="row">

            <?php

            $contenetQuery = DB::getInstance()->query('

                SELECT *, mc.id as mid, img.unique_id       as uid 

                FROM museum_books                           as mc

                LEFT JOIN museum_images                     as img      

                ON mc.id = img.sid AND img.source           = ?

                AND img.deleted                             = ?

                WHERE mc.deleted                            = ? 

                AND mc.active                               = ? 

                ORDER by mc.internal_order ASC', 

                (array('books', 0, 0, 1)));

            $count = 0; 

            foreach($contenetQuery->results() as $item){ 

                $title = $item->title;

                $author = $item->author;
               
                $url = $item->url;

                if($item->uid == null){

                    $myImg = '../../private/img/templates/image-template/no-book-cover.png';

                    $myImgRetina = '../../private/img/templates/image-template/no-book-cover.png';

                }else{

                    $myImg = '../../museumSmartAdmin/dist/default/private/sources/images/books/' . $item->mid . '/' . $item->uid . '/' . $item->mid . '_' . $item->uid . '_original_sq.png';

                    $myImgRetina = '../../museumSmartAdmin/dist/default/private/sources/images/books/' . $item->mid . '/' . $item->uid . '/' . $item->mid . '_' . $item->uid . '_original_sq@2x.png';
                }

                ?>

                <div class="col-md-3 d-flex book-item">

                    <div class="book-card mb-4" style='overflow:hidden !important;'>

                        <div class="gray-img-container">

                            <img class="memory-img card-img-top hvr-grow" src="<?php echo $myImg;?>" alt="<?php echo $title;?>">

                        </div>

                        <div class="card-body">

                            <input type="hidden" class="description" value="<?php echo $description;?>">

                            <p class="text-center book-name card-text text-medium text-uppercase text-truncate">

                                <?php echo $title;?>

                            </p>

                            <p class="text-center book-author card-text text-light text-uppercase text-dark text-truncate">

                                <?php echo $author;?>

                            </p>

                            <div class="push-to-bottom text-center">

                                <div class="btn-group">

                                    <a class="more-info hvr-sweep-to-right btn-square btn btn-sm btn-outline-secondary" href="<?php echo $url;?>" role="button">VER LIBRO</a>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            <?php } ?>

        </div>

    </div>

</div>