<?php

	$query = DB::getInstance()->query('SELECT * from museum_quotes WHERE active = ? AND deleted = ? ORDER BY RAND() LIMIT 1', [1, 0]);

	$quote = $query->first()->quote;
	
	$author = $query->first()->author;
?>

<section id="quotes">

	<div class="container">

		<div class="row">

			<div class="col-lg-12 text-center">

				<p class="quote-content offset-2 col-8 section-heading module-title"><i>«<?php echo $quote;?>»</i></p>

				<hr class="module-separator module-separator-invert">

			</div>

			<div class="col-lg-8 mx-auto">

				<p class="quote-content module-text dark text-faded mb-5 text-center about-reflexiones text-uppercase text-regular"><?php echo $author;?></p>

			</div>

		</div>

	</div>

</section>