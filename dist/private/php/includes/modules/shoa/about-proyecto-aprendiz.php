<?php


?>

<div id="">

	<div class="container">

		<div class="row">

			<div class="col-12">

				<h5 class="pt-5 text-uppercase text-medium black content-title proyecto-aprendiz">Proyecto Aprendiz</h5>

				<hr class="left-separator content-separator">

				<p class="dark text-faded text-left about-reflexiones module-text text-light content-text">El Proyecto Aprendiz es una cadena viva de relatos orales encarnados en jóvenes que luego de estar en contacto directo y personal con sobrevivientes de la Shoá, asumen el legado de contar su historia.

					Esta travesía conjunta entre un sobreviviente y un joven está precedida por una capacitación integradora. Los encuentros transcurren en un marco íntimo y personal, elegido por ellos mismos. El sobreviviente transmite lo que sabe, lo que recuerda, quién ha sido y quién es y el aprendiz se compromete a incorporar el relato y su esencia para transmitirlo en forma oral durante mucho tiempo más.

				</p>

				<i class="text-light black">“Todo aquel que oye a un testigo se convierte en testigo”.<br> Elie Wiesel</i>

			</div>

			<div class="col-lg-6 text-left mision-y-vision">

				<div class="mt-5">

					<h5 class="mt-3 mb-3 text-medium black content-title">Fundamentos y objetivos</h5>

					<hr class="left-separator content-separator">

					<p class="text-muted mb-0 text-light text-up content-text">Educar sobre la Shoá no sólo debe incluir la información fáctica de los hechos sino también los aspectos emocionales y personales; de esta manera el aprendizaje tiene otro anclaje. Las conferencias, películas y libros son condición necesaria pero no suficiente porque la voz y presencia de un sobreviviente ante un auditorio produce un cambio de energía y abre otra escucha. El protagonista ejerce un magnetismo poderoso: ver y escuchar al testigo, su historia personal, proporciona la contundencia de la realidad vivida a la transmisión de la Shoá. Las historias particulares permiten comprender los acontecimientos históricos e identificarse con el aspecto humano. Esa identificación es indispensable para una efectiva incorporación cognitiva. Los hechos pueden ser recordados cuando se vinculan a experiencias y emociones, y solo los testimonios orales hacen posible ese contacto privilegiado.<br><br>		

						El paso del tiempo arriesga con dejar al futuro sin los testimonios privilegiados de los testigos, por ello este proyecto busca preservar el poder de los testimonios orales. Las nuevas voces que encarnan las historias particulares tienen un efecto multiplicador porque a su vez podrán instruir a otras voces y a otras más en una cadena infinita de transmisión de la memoria. Con esta cadena, el sobreviviente seguirá siendo escuchado y su mensaje alcanzará a más personas por más tiempo. Los aprendices tienen la responsabilidad de mantener vivo el testimonio oral.

					</p>

				</div>

			</div>

			<div class="col-lg-6 text-left">

				<div class="mt-5">

					<h5 class="mt-3 mb-3 text-medium black content-title">¿Cómo funciona el Proyecto Aprendiz?</h5>

					<hr class="left-separator content-separator">

					<p class="text-muted mb-0 text-light text-up content-text">Los jóvenes aprendices interactúan con los sobrevivientes, se sumergen en su historia, aprenden sobre su personalidad y sobre los distintos momentos de la vida. De esta manera, toma del sobreviviente todo lo que lo distingue, lo que lo hace ser como es para poder ser su vocero y contar su historia. Esta inmersión y comprensión profunda le permitirá representarlo y mantener vivo el relato oral.<br><br>

						El aprendiz registra el proceso de toda la experiencia en forma de un diario, una bitácora de viaje, que será su memoria de la historia del sobreviviente y del proceso de conocimiento.<br><br>

						Los aprendices, al igual que los sobrevivientes, son acompañados durante el proceso por integrantes del equipo de coordinación. El acompañamiento tiene como objetivo la supervisión cercana y el apoyo, en caso de ser necesario, para resolver o intervenir en cualquier problema o inconveniente durante la experiencia.<br><br>

						Para más información, escribí a <a href="eventos@museodelholocausto.org.ar">eventos@museodelholocausto.org.ar</a>

					</p>

				</div>

			</div>

		</div>

	</div>


</div>