<?php

$query = DB::getInstance()->query("SELECT * FROM museum_shoa_description");

$about = $query->first()->description;

?>

<section id="about-shoa" class="que-fue-la-shoa">

	<div class="container">

		<div class="row">

			<div class="col-lg-6 col-12">

				<h5 class="text-uppercase">¿QUÉ FUE LA SHOÁ?</h5>

				<p class="dark text-faded mb-5 text-left about-reflexiones module-text text-light"><?php echo $about;?></p>

			</div>

			<div class="col-lg-6 text-left">

				<div id="accordion">

					<?php

					$query = DB::getInstance()->query('SELECT * FROM museum_shoa_faq WHERE deleted = ? ORDER BY internal_order', (array(0)));

					$counter = 0;

					$isFirst = true; 

					foreach($query->results() as $shoaFaq){

						$divId = 'heading-' . $counter; 

						$divCollapse = 'collapse-' . $counter;

						$question = $shoaFaq->question;

						$answer = $shoaFaq->answer;

						$counter+=1;

						?>

						<div class="card border-bottom-0">

							<div class="card-header" id="<?php echo $divId;?>" data-toggle="collapse" data-target="#<?php echo $divCollapse;?>" aria-expanded="true" aria-controls="<?php echo $divCollapse;?>">

								<span class="mb-0 text-medium card-content row gray-500">

									<div class="col-1 align-self-center">

										<i class="far fa-plus-square" style=" vertical-align: middle;"></i>

									</div>

									<div class="col-11">

										<?php echo $question;?>

									</div>

								</span>
							</div>

							<div id="<?php echo $divCollapse;?>" class="collapse <?php if($isFirst) echo 'show';?>" aria-labelledby="<?php echo $divId;?>" data-parent="#accordion">
								<div class="card-body text-light gray-900">

									<?php echo $answer;?>

								</div>
							</div>
						</div>

						<?php

						$isFirst = false;

					}
					
					?>

				</div>


			</div>

		</div>

	</section>