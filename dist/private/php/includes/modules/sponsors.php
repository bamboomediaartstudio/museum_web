<?php
?>

<section id="sponsors">

	<div class="container">

		<div class="row">

			<div class="col-lg-12 text-center">

				 <h2 class="section-heading module-title text-uppercase"><?php echo $wording->get('sponsors_first_line');?></h2>


				<hr style='color:red !important;' class="module-separator">

			</div>

			<div class="col-lg-8 mx-auto">

				<p class="module-text dark text-faded mb-5 text-center about-reflexiones text-uppercase text-regular"><?php echo $wording->get('sponsors_second_line');?></p>

			</div>

		</div>

	</div>

	<div class="container">

		<div class="sponsors-logos d-none">

			<?php

			$query = DB::getInstance()->query('

				SELECT * from  museum_images 					as img 		

				WHERE img.source 								= ? 

				AND img.active 									= ? 

				AND img.deleted 								= ? 
				
				ORDER by img.internal_order ASC', 

				(array('sponsors', 1, 0)));

			$count = 0; 

			foreach($query->results() as $result){ 

				$myImg = 'museumSmartAdmin/dist/default/private/sources/images/sponsors/1/images/' . $result->unique_id . '/1_' . $result->unique_id . '_thumb.png';
				
				$myImgRetina = 'museumSmartAdmin/dist/default/private/sources/images/sponsors/1/images/' . $result->unique_id . '/1_' . $result->unique_id . '_thumb@2x.png';

				$description = $result->description;

			?>

			<div class="hvr-grow slide">

				<img class="sponsor-image" src="<?php echo $myImg;?>" srcset= "<?php echo $myImgRetina;?>" alt="<?php echo $description;?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $description;?>">

			</div>

			<?php } ?>

		</div>

	</div>

</section>