<?php
?>

<section id="services" class="visitas-grupales visitas-individuales">

</div>

</div>

<div class="container mt-5">

  <div class="row">

    <div class="col-lg-12 text-center">

      <h2 class="section-heading module-title"><?php echo $wording->get('reflexiones_first_line');?></h2>

      <hr class="module-separator">

    </div>

    <div class="col-lg-8 mx-auto">

      <p class="text-light text-faded mb-5 text-center about-reflexiones module-text"><?php echo $wording->get('reflexiones_second_line');?></p>

    </div>

  </div>

</div>

<div class="container">
  
  <div class="row">

    <div class="col-12 text-center sr-icon-1">

      <div class="service-box mt-5 mx-auto ">

        <span class="fa-stack fa-2x center" aria-hidden="true">

         <i class="fal fa-circle fa-stack-2x" aria-hidden="true"></i>

         <i class="fas fa-laptop fa-stack-1x" aria-hidden="true"></i>

       </span>

       <h4 class="mt-3 mb-3 text-medium">VISITAS VIRTUALES!</h4>

       <p class="text-muted mb-0 text-light text-up">Accedé a charlas y recorridos virtuales llevados a cabo por nuestros guías.</p>

     </div>

     <br>

     <button class="generic-action-button btn button go-to-virtual-visit">

      <i class="fal fa-clock mr-2"></i>RESERVAR DÍA Y HORARIO</button>

    </div>

  </div>

  <div class="row">

    <div class="col-lg-4 col-md-4 text-center sr-icon-1">

      <div class="service-box mt-5 mx-auto ">

        <span class="fa-stack fa-2x center" aria-hidden="true">

         <i class="fal fa-circle fa-stack-2x" aria-hidden="true"></i>

         <i class="fas fa-ticket-alt fa-stack-1x" aria-hidden="true"></i>

       </span>

       <h4 class="mt-3 mb-3 text-medium">VISITAS INDIVIDUALES<br>(CON AUDIOGUÍA)</h4>

       <p class="text-muted mb-0 text-light text-up">Completá este formulario si planeás visitar en solitario el museo. No olvides escargar la app. Link android - link ios</p>

     </div>

     <br>

     <button class="generic-action-button btn button go-to-individual-visit">

      <i class="fal fa-clock mr-2"></i>RESERVAR DÍA Y HORARIO</button>

    </div>

    <div class="col-lg-4 col-md-4 text-center sr-icon-1">

      <div class="service-box mt-5 mx-auto ">

        <span class="fa-stack fa-2x center" aria-hidden="true">

         <i class="fal fa-circle fa-stack-2x" aria-hidden="true"></i>

         <i class="fas fa-ticket-alt fa-stack-1x" aria-hidden="true"></i>

       </span>

       <h4 class="mt-3 mb-3 text-medium">VISITAS GUIADAS<br>(GRUPALES CON GUÍA)</h4>

       <p class="text-muted mb-0 text-light text-up">Completá este formulario si querés recorrer el museo el Museo Del Holocausto con un guía especializado</p>

     </div>

     <br>

     <button class="generic-action-button btn button go-to-individual-visit">

      <i class="fal fa-clock mr-2"></i>RESERVAR DÍA Y HORARIO</button>

    </div>

    <div class="col-lg-4 col-md-4 text-center sr-icon-2">

      <div class="service-box mt-5 mx-auto">

        <span class="fa-stack fa-2x center" aria-hidden="true">

         <i class="fal fa-circle fa-stack-2x" aria-hidden="true"></i>

         <i class="fas fa-users fa-stack-1x" aria-hidden="true"></i>

       </span>

       <h4 class="mt-3 mb-3 text-medium ">VISITAS ESCOLARES Y UNIVERSITARIAS</h4>

       <p class="text-muted mb-0 text-light text-up">Completá este formulario solamente si representás a un colegio, universidad o institución, ya sea pública o privada.</p>

       <?php //echo $wording->get('group_visits');?>

       <!--<p class="text-muted mb-0 text-light text-up"></p> -->

     </div>

     <br>

     <button class="generic-action-button btn button go-to-group-visit">
      <i class="fal fa-clock mr-2"></i>RESERVAR DÍA Y HORARIO</button>

       <!--<a target="_blank" role="button" href="http://bit.ly/2FSKTkS" type="button" class="btn group-reservation main-action-button">

        <i class="fal fa-clock mr-1"></i> RESERVAR DÍA Y HORARIO</a>-->


      </div>

    </div>

  </div>

</section>