<?php
?>

<section id="services" class="visitas-grupales visitas-individuales">


<div class="container mt-5">

  <div class="row">

    <div class="col-lg-12 text-center">

      <h2 class="section-heading module-title">CONOCÉ EL PROTOCOLO DE SEGURIDAD</h2>

      <hr class="module-separator">

    </div>

    <div class="col-lg-8 mx-auto">
      
      <p class="text-light text-faded mb-5 text-center about-reflexiones module-text">Estos son los cuidados que implementamos y que solicitamos respetar a quienes visitan la exhibición permanente del Museo del Holocausto de Buenos Aires durante la pandemia.</p>

    </div>

  </div>

</div>

<div class="container">
  
  <div class="row">

    <div class="col-lg-6 text-center sr-icon-1">

      <div class="service-box mt-5 mx-auto">

         <img class="protocol-image" src="../../../private/img/protocolo/calendar.svg" />

       <h4 class="mt-3 mb-3 text-medium text-uppercase custom-item-color">Reservá tu entrada de forma online</h4>

       <p class="text-muted mb-0 text-light text-up">Para poder recorrer la exhibición permanente, solicitá de forma online una entrada por persona, incluidos los niños.</p>

     </div>
    
    </div>

    <div class="col-lg-6 text-center sr-icon-1">

      <div class="service-box mt-5 mx-auto">

         <img class="protocol-image" src="../../../private/img/protocolo/distance.svg" />

       <h4 class="mt-3 mb-3 text-medium text-uppercase custom-item-color">RESPETÁ LA DISTANCIA MÍNIMA</h4>

       <p class="text-muted mb-0 text-light text-up"> indica el protocolo, se deberá mantener una distancia de 2 metros entre todas las personas.</p>

     </div>

    
    </div>
    
  </div>

   <div class="row">

    <div class="col-lg-6 text-center sr-icon-1">

      <div class="service-box mt-5 mx-auto">

         <img class="protocol-image" src="../../../private/img/protocolo/temperature.svg" />

       <h4 class="mt-3 mb-3 text-medium text-uppercase custom-item-color">Te tomaremos la temperatura</h4>

       <p class="text-muted mb-0 text-light text-up">Para ingresar al Museo, todos los visitantes deben tomarse la temperatura y firmar una declaración jurada.</p>

     </div>
    
    </div>

    <div class="col-lg-6 text-center sr-icon-1">

      <div class="service-box mt-5 mx-auto">

         <img class="protocol-image" src="../../../private/img/protocolo/direction.svg" />

       <h4 class="mt-3 mb-3 text-medium text-uppercase custom-item-color">HAY UN SOLO SENTIDO</h4>

       <p class="text-muted mb-0 text-light text-up">Solicitamos que por favor respetes el sentido único de circulación, marcado en el piso.</p>

     </div>

    
    </div>
    
  </div>

  
  <div class="row">

    <div class="col-lg-6 text-center sr-icon-1">

      <div class="service-box mt-5 mx-auto">

         <img class="protocol-image" src="../../../private/img/protocolo/mask.svg" />

       <h4 class="mt-3 mb-3 text-medium text-uppercase custom-item-color">El barbijo es obligatorio</h4>

       <p class="text-muted mb-0 text-light text-up">El uso de tapabocas que cubran nariz y boca es obligatorio para recorrer toda la exhibición.</p>

     </div>
    
    </div>

    <div class="col-lg-6 text-center sr-icon-1">

      <div class="service-box mt-5 mx-auto">

         <img class="protocol-image" src="../../../private/img/protocolo/notouch.svg" />

       <h4 class="mt-3 mb-3 text-medium text-uppercase custom-item-color">No tocar las gráficas</h4>

       <p class="text-muted mb-0 text-light text-up">Para el cuidado de todos, por favor no toques las gráficas ni los nomencladores de la exhibición.</p>

     </div>

    
    </div>
    
  </div>

  <div class="row">

    <div class="col-lg-6 text-center sr-icon-1">

      <div class="service-box mt-5 mx-auto">

         <img class="protocol-image" src="../../../private/img/protocolo/alcohol.svg" />

       <h4 class="mt-3 mb-3 text-medium text-uppercase custom-item-color">El uso del sanitizante</h4>

       <p class="text-muted mb-0 text-light text-up">Te sugerimos que uses sanitizante durante toda la visita. Podrás encontrarlo fácilmente en toda la exhibición.</p>

     </div>
    
    </div>

    <div class="col-lg-6 text-center sr-icon-1">

      <div class="service-box mt-5 mx-auto">

         <img class="protocol-image" src="../../../private/img/protocolo/devices.svg" />

       <h4 class="mt-3 mb-3 text-medium text-uppercase custom-item-color">Sobre los dispositivos</h4>

       <p class="text-muted mb-0 text-light text-up">Luego de tocar cada pantalla, utilizá sanitizante. Es importante que tus manos estén secas antes de tocar otro dispositivo.</p>

     </div>

    
    </div>
    
  </div>

  </div>


  <div class="container mt-5">

  <div class="row">

    <div class="col-lg-12 text-center">

      <h2 class="section-heading module-title">RESERVÁ TU TURNO ONLINE</h2>

      <hr class="module-separator">

    </div>

    <div class="col-lg-8 mx-auto">

      <p class="text-light text-faded mb-5 text-center about-reflexiones module-text">Momentáneamente el museo solo abre al público en general. La inscripción para <strong>instituciones</strong> aún no fueron habilitadas. Informaremos cuando esto suceda.</p>

    </div>

  </div>

</div>


<!--
<div class="">

    <div class="col-lg-12 col-md-12 text-center sr-icon-1">

      <div class="service-box mx-auto ">

        <span class="fa-stack fa-2x center" aria-hidden="true">

         <i class="fal fa-circle fa-stack-2x" aria-hidden="true"></i>

         <i class="fas fa-ticket-alt fa-stack-1x" aria-hidden="true"></i>

       </span>

       <h4 class="mt-3 mb-3 text-medium">RESERVÁ TU TURNO</h4>

       <p class="text-muted mb-0 text-light text-up">Completá este formulario con tus datos.</p>

     </div>

     <br>

     <button class="generic-action-button btn button go-to-individual-visit">

      <i class="fal fa-clock mr-2"></i>RESERVAR DÍA Y HORARIO</button>

    </div>

</div>
-->

<div class="container">

  <div class="row">

      <div class="col-lg-6 col-md-6 text-center sr-icon-1">

        <div class="service-box mx-auto ">

          <span class="fa-stack fa-2x center" aria-hidden="true">

           <i class="fal fa-circle fa-stack-2x" aria-hidden="true"></i>

           <i class="fas fa-ticket-alt fa-stack-1x" aria-hidden="true"></i>

         </span>

         <h4 class="mt-3 mb-3 text-medium">RESERVÁ TU TURNO</h4>

         <p class="text-muted mb-0 text-light text-up">Completá este formulario con tus datos.</p>

       </div>

       <br>

       <button class="generic-action-button btn button go-to-individual-visit">

        <i class="fal fa-clock mr-2"></i>RESERVAR DÍA Y HORARIO</button>

      </div>


      <div class="col-lg-6 col-md-6 text-center sr-icon-2">

        <div class="service-box mx-auto">

          <span class="fa-stack fa-2x center" aria-hidden="true">

           <i class="fal fa-circle fa-stack-2x" aria-hidden="true"></i>

           <i class="fas fa-users fa-stack-1x" aria-hidden="true"></i>

         </span>

         <h4 class="mt-3 mb-3 text-medium ">VISITAS ESCOLARES Y UNIVERSITARIAS</h4>

         <p class="text-muted mb-0 text-light text-up">Completá este formulario solamente si representás a un colegio, universidad o institución, ya sea pública o privada.</p>

         <?php //echo $wording->get('group_visits');?>

         <!--<p class="text-muted mb-0 text-light text-up"></p> -->

       </div>

       <br>

       <button class="generic-action-button btn button go-to-group-visit">
        <i class="fal fa-clock mr-2"></i>RESERVAR DÍA Y HORARIO</button>

         <!--<a target="_blank" role="button" href="http://bit.ly/2FSKTkS" type="button" class="btn group-reservation main-action-button">

          <i class="fal fa-clock mr-1"></i> RESERVAR DÍA Y HORARIO</a>-->


    </div>

  </div><!-- /.row -->

</div><!-- /.container -->

</section>