<div id="visits-gallery" class="galeria-de-imagenes">

  <div class="container mt-5 mb-5">

    <div class="row">

      <div class="col-12">

        <h5 class="text-uppercase text-medium black content-title">IMÁGENES</h5>

        <hr class="left-separator content-separator">

      </div>

      <div id="blueimp-gallery" class="row blueimp-gallery blueimp-gallery-controls">

        <div class="slides"></div>

        <h3 class="title"></h3>

        <a class="prev">‹</a>

        <a class="next">›</a>

        <a class="close">×</a>

        <a class="play-pause"></a>

        <ol class="indicator"></ol>

      </div>

      <div id="links" class="container">

        <div class="row">

          <?php

          $query = DB::getInstance()->query('

            SELECT * from  museum_images          as img    

            WHERE img.source                = ? 

            AND img.active                  = ? 

            AND img.deleted                 = ? 

            ORDER by img.internal_order ASC', 

            (array('visits', 1, 0)));

          $count = 0; 

          foreach($query->results() as $result){  

            $myImg = '../museumSmartAdmin/dist/default/private/sources/images/visits/1/images/' . $result->unique_id . '/1_' . $result->unique_id . '_sq.jpeg';

            $myImgRetina = '../museumSmartAdmin/dist/default/private/sources/images/visits/1/images/' . $result->unique_id . '/1_' . $result->unique_id . '_sq@2x.jpeg';

            $big = '../museumSmartAdmin/dist/default/private/sources/images/visits/1/images/' . $result->unique_id . '/1_' . $result->unique_id . '_original@2x.jpeg';

            $description = $result->description;

            if($description == '') $description = 'Muestra Reflexiones De La Shoá';

            ?>

            <div class="col-md-4 col-lg-3 mt-5 hvr-grow">

              <a href="<?php echo $big;?>" title="<?php echo $description;?>">

                <img class="img-thumbnail img-fluid " 

                src="<?php echo $myImg;?>" srcset= "<?php echo $myImgRetina;?>"

                alt="<?php echo $description;?>">

              </a>

            </div>

          <?php }  ?>

        </div>

      </div>

    </div>

  </div>

</div>