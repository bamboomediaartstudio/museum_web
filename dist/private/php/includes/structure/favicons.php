<?php 

define('inc', TRUE);

?>

<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">

<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">

<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">

<link rel="manifest" href="/site.webmanifest">

<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#134b95">

<meta name="msapplication-TileColor" content="#2b5797">

<meta name="theme-color" content="#ffffff">

<!-- Facebook Pixel Code -->

<script>

	!function(f,b,e,v,n,t,s)

	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

		n.callMethod.apply(n,arguments):n.queue.push(arguments)};

		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

		n.queue=[];t=b.createElement(e);t.async=!0;

		t.src=v;s=b.getElementsByTagName(e)[0];

		s.parentNode.insertBefore(t,s)}(window,document,'script',

			'https://connect.facebook.net/en_US/fbevents.js');


		fbq('init', '416170755790747'); 

		fbq('track', 'PageView');

	</script>

	<noscript>

		<img alt="" height="1" width="1" 

		src="https://www.facebook.com/tr?id=416170755790747&ev=PageView

		&noscript=1"/>

	</noscript>

<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133657381-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-133657381-1');
</script>