<?php

if(!defined('inc')){

  header("Location: http://museodelholocausto.org.ar/not-found");

  die();
}

?>

<footer>

	<div class="container">

		<span class="footer-text white text-light text-uppercase inline align-middle white">© Museo Del Holocausto <?php echo date("Y"); ?>. Todos Los Derechos Reservados.</span>

			<!--<div class="text-light footer-buttons col-6 text-right">
		
				<ul class="list-inline">
			
					<li class="list-inline-item">
			
						<a href="#">Privacy</a>
			
					</li>
			
					<li class="list-inline-item">
			
						<a href="#">Terms</a>
			
					</li>
			
					<li class="list-inline-item">
			
						<a href="#">FAQ</a>
			
					</li>
			
				</ul>

			</div>-->
	
	</div>

</footer>