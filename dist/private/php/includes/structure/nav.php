<?php

if(!defined('inc')){

  header("Location: http://museodelholocausto.org.ar/not-found");

  die();
}

$privateBase = dirname(dirname(dirname(__FILE__)));

$logoPath = $privateBase . '/includes/structure/svg-logo.php';

$http;

if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {

  $http = 'https://';

}else{

  $http = 'http://';
}

$isOnline = true;

if(!$isOnline){

  $base = $http . $_SERVER['HTTP_HOST'];

  $whitelist = array('127.0.0.1', '::1');

  if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){

    $base = 'http://cloud39.temp.domains/~brpxmzm5/';

    $index = 'http://cloud39.temp.domains/~brpxmzm5/';

  }else{

    $base = $http . $_SERVER['HTTP_HOST'] . '/';

    $index = "http://" . $_SERVER['SERVER_NAME'];
  }

}else{

  $base = $http . $_SERVER['HTTP_HOST'] . '/';

  $index = "http://" . $_SERVER['SERVER_NAME'];


}


?>

<nav id= "mainNav" class="navbar navbar-expand-lg navbar-light fixed-top">

  <div class="container">

    <a class="navbar-brand hvr-grow" href="<?php echo $index;?>">

      <?php include($logoPath);?>

    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">

      <span class="navbar-toggler-icon"></span>

    </button>

    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      <ul class="navbar-nav ml-auto">

        <?php

        $menu = DB::getInstance()->query('SELECT * FROM museum_site_menu WHERE deleted = ? AND active = ? ORDER BY internal_order ASC', (array(0, 1)));

        foreach($menu->results() as $result){ 

          $subMenu = DB::getInstance()->query('SELECT * FROM museum_site_submenu WHERE active = ? AND deleted = ? AND menu_id = ? ORDER BY internal_order ASC', (array(1, 0, $result->id)));

          $submenuItems = $subMenu->count();

          if($submenuItems == 0){

            $finalURL;

            if($result->target != '_blank'){

              $finalURL = $base . $result->folder . '/' . $result->url;

            }else{

              $finalURL = $result->url;

            }
            
            ?>

            <li id="item-<?php echo $result->id;?>" class="nav-item">

              <a target = '<?php echo $result->target;?>' 

                class="nav-link nav-link-custom text-uppercase white " 

                href="<?php echo $finalURL;?>"><?php echo $result->label;?>

              </a>

            </li>

            <?php 

          }else{

            $atm = $base . $result->folder . '/' . $result->url;

            ?>

            <li id="item-<?php echo $result->id;?>" class="nav-item dropdown">

              <a class="nav-link nav-link-custom text-uppercase white" href="<?php echo $atm;?>" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo $result->label;?>
              </a>

              <div class="dropdown-menu" aria-labelledby="item-<?php echo $result->id;?>">

                <?php 

                foreach($subMenu->results() as $submenuResult){ 

                  $submenuPath = $base . $result->folder . '/' . $submenuResult->path;

                  ?>

                  <a class="dropdown-item" href="<?php echo $submenuPath;?>"><?php echo $submenuResult->label;?></a>

                <?php  } ?>

              </div>

            <?php } ?> 

          </li>

        <?php } ?>

      </ul>

    </div>

  </div>

</nav>