<?php

$http;

if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {

	$http = 'https://';

}else{

	$http = 'http://';
}

$base = $http . $_SERVER['HTTP_HOST'];

$whitelist = array('127.0.0.1', '::1');

if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){

  $base = 'http://cloud39.temp.domains/~brpxmzm5/';

}else{

  $base = $http . $_SERVER['HTTP_HOST'] . '/';

}


?>

<ul class="navbar-nav ml-auto">

	<?php 

	$menu = DB::getInstance()->query('SELECT * FROM museum_site_menu WHERE deleted = ? AND active = ? ORDER BY internal_order ASC', (array(0, 1)));

	foreach($menu->results() as $result){ 

		$subMenu = DB::getInstance()->query('SELECT * FROM museum_site_submenu WHERE active = ? AND deleted = ? AND menu_id = ? ORDER BY internal_order ASC', (array(1, 0, $result->id)));

		$submenuItems = $subMenu->count();

		if($submenuItems == 0){

      $finalURL;

      if($result->target != '_blank'){

        $finalURL = $base . $result->folder . '/' . $result->url;

      }else{

        $finalURL = $result->url;

      }

      ?>

      <li id="item-<?php echo $result->id;?>" class="nav-item">

        <a target = '<?php echo $result->target;?>' class="nav-link js-scroll-trigger"><?php echo $result->label;?></a>

      </li>

      <?php 

    }else{ 

      $finalURL = $base . $result->folder . '/' . $result->url;

     ?>

     <li class="nav-item dropdown">

      <a class="nav-link" id="navbarDropdownPortfolio">

       <?php echo $result->label;?>

     </a>

     <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">

       <?php 

       foreach($subMenu->results() as $submenuResult){ 

        $submenuPath = $base . $result->folder . '/' . $submenuResult->path;

        ?>

        <a class="dropdown-item" href="<?php echo $submenuPath;?>"><?php echo $submenuResult->label;?></a>

      <?php } ?>


    </div>

    <?php

  } 

}

?>

</ul>