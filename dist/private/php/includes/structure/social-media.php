<?php 

if(!defined('inc')){

  header("Location: http://museodelholocausto.org.ar/not-found");

  die();
}

?>

<div class="social-media hidden-md-down d-none">

	<ul class="navbar-nav">

		<?php

		$socialMedia = DB::getInstance()->query('SELECT * FROM site_social_media WHERE deleted = ?', (array(0)));

		$count = 0; 

		foreach($socialMedia->results() as $socialMedia){ 

			?>

			<li class="nav-item">

				<a target='<?php echo $socialMedia->target;?>' class="nav-link js-scroll-trigger" href="<?php echo $socialMedia->url;?>">

					<span class="fa-stack-sm fa-stack fa-1x center">

						<i class="fas my-circle social-link-<?php echo strtolower($socialMedia->name);?> fa-circle fa-stack-2x"></i>

						<i class="<?php echo $socialMedia->fa_icon;?> fa-stack-1x"></i>

					</span>

				</a>

			</li>

		<?php } ?>	

	</ul>

</div>

