<?php

$id = $_GET['id'];

setlocale(LC_TIME, 'es_ES', 'esp_esp');

require_once '../museumSmartAdmin/dist/default/private/users/core/init.php';

$db = DB::getInstance();

$query = $db->query(

  'SELECT *,mvc.name AS class_name, users.name AS guide_name FROM museum_virtual_classes_events AS `mvce` 

  INNER JOIN `museum_virtual_classes` AS `mvc` ON (`mvce`.`sid` = `mvc`.`id`) 

  LEFT JOIN `museum_virtual_classes_events_guides_relations` as `guides` ON  (`mvce`.`id` = `guides`.`id_event`)

  LEFT JOIN `users` as `users` ON (`users`.`id` = `guides`.`id_guide`)

  WHERE `mvce`.id = ?', [$id])->first();

$guide_name = ($query->guide_name == "") ? 'aun sin asignar' : $query->guide_name;

$zoom_url = ($query->zoom_url == "") ? 'aun sin asignar' : $query->zoom_url;

$zoom_id = ($query->zoom_id == "") ? 'aun sin asignar' : $query->zoom_id;

$zoom_password = ($query->zoom_password == "") ? 'aun sin asignar' : openssl_decrypt($query->zoom_password_encrypt,"AES-128-ECB", 'Muse@2021!');

$date = utf8_encode(strftime("%d de %B de %Y - %H:%Mhs", strtotime($query->date_start)));


?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="robots" content="noindex, nofollow">
    
    <meta name="description" content="">
    
    <meta name="author" content="">

    <title>PRÓXIMO EVENTO</title>

    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
   
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
   
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <link href="css/coming-soon.css" rel="stylesheet">

  </head>

  <body>

    <div class="overlay">
         
    </div>

    <div class="masthead">
    
      <div class="masthead-bg"></div>
    
      <div class="container h-100">
    
        <div class="row h-100">
    
          <div class="col-12 my-auto">
    
            <div class="masthead-content text-white py-5 py-md-0">
    
              <h1 class="mb-3">TU PRÓXIMO EVENTO</h1>
    
              <p class="mb-5">Esta es la info de tu próximo evento del <b>Museo Del Holocausto:</b><br><br>
                
                <b>clase: </b> <?php echo $query->class_name;?><br>
                
                <b>día y horario: </b> <?php echo $date;?><br>

                <b>guia: </b><?php echo $guide_name;?><br><br>
                
                <b>zoom URL: </b><a href="<?php echo $zoom_url;?>"><?php echo $zoom_url;?></a><br>

                <b>zoom ID: </b><?php echo $zoom_id;?><br>

                <b>zoom password: </b><?php echo $zoom_password;?><br>
      
            </div>
       
          </div>
  
        </div>
  
      </div>
  
    </div>
    
    <div class="social-icons">
      
      <ul class="list-unstyled text-center mb-0">
      
        <li class="list-unstyled-item">
      
          <a href="https://www.instagram.com/museoshoa/">
      
            <i class="fab fa-instagram"></i>
      
          </a>
      
        </li>
      
        <li class="list-unstyled-item">
      
          <a href="https://twitter.com/museoshoa?lang=es">
      
            <i class="fab fa-twitter"></i>
      
          </a>
      
        </li>
      
        <li class="list-unstyled-item">
      
          <a href="https://www.facebook.com/museoshoa/">
      
            <i class="fab fa-facebook-f"></i>
      
          </a>
      
        </li>
      
        <li class="list-unstyled-item">
      
          <a href="https://www.youtube.com/user/MuseodelHolocausto">
      
            <i class="fab fa-youtube"></i>
      
          </a>
      
        </li>
      
      </ul>
    
    </div>

    <script src="vendor/jquery/jquery.min.js"></script>

    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="js/coming-soon.min.js"></script>

  </body>

</html>
