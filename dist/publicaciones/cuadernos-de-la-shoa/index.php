<?php

include('../../museumSmartAdmin/dist/default/private/users/core/init.php');

?>

<!DOCTYPE html>

<head>

      <?php include_once('../../private/php/includes/structure/meta.php');?>

      <title>Museo Del Holocausto - Publicaciones - Cuadernos de La Shoá</title>

      <link href="../../private/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

      <link href="../../private/css/fontawesome/all.css" rel="stylesheet" type="text/css">

      <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

      <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

      <link href="../../private/css/style.css" rel="stylesheet">

      <?php include_once('../../private/php/includes/structure/favicons.php');?>

</head>

<body id="page-top">

      <div class="modal" tabindex="-1" role="dialog">

            <div class="modal-dialog" role="document">

                  <div class="modal-content">

                        <div class="modal-header">
                        
                          <h5 class="modal-title">Información adicional</h5>
                        
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                              <span aria-hidden="true">&times;</span>

                        </button>

                  </div>

                  <div class="modal-body">

                        <p>Modal body text goes here.</p>

                  </div>

                  <div class="modal-footer">

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

                  </div>

            </div>

      </div>

</div>

<?php include('../../private/php/includes/structure/nav.php');?>

<?php include('../../private/php/includes/modules/publish/about-cuadernos.php');?>

<?php include('../../private/php/includes/structure/footer.php');?>

<script src="../../private/vendor/jquery/jquery.min.js"></script>

<script src="../../private/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="../../private/vendor/jquery-easing/jquery.easing.min.js"></script>

<script src="../../private/vendor/scrollreveal/scrollreveal.min.js"></script>

<script src="../../private/vendor/gsap/src/minified/TweenMax.min.js"></script>

<script src="../../private/vendor/gsap/src/minified/TimelineMax.min.js"></script>

<script src="../../private/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

<script src="../../private/vendor/slick/slick.min.js"></script>

<script src="../../private/js/generalConf.min.js"></script>

<script src="../../private/js/helpers.min.js"></script>

<script src="../../private/js/publish.min.js"></script>

</body>

</html>