<?php session_start();

date_default_timezone_set('America/Argentina/Buenos_Aires');

$whitelist = array('127.0.0.1', '::1');

$GLOBALS['config'] = array(

		'mysql'=>array(

		'host'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '127.0.0.1' : '162.214.65.131',

		'username'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? 'root' : 'brpxmzm5_make',

		'password'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? '' : 'n},iT0aJTqnF',

		'db'=> (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) ? 'lasalle_museum' : 'brpxmzm5_museum_smart_admin'
	),

	'remember'=>array('cookie_name'=> 'hash', 'cookie_expiry'=> 604800 ),

	'session'=>array( 'session_name' => 'user', 'token_name' => 'token' ),

	'users'=>array( 'users_profile_picture_folder' => '../sources/images/users/', 'users_profile_picture_max_size' => 20971520)

);



spl_autoload_register(function($class){

	$privateBase = dirname(dirname(dirname(__FILE__)));

	require_once($privateBase .  '/framework/general/' . $class . '.php');

});

//...

$privateBase = dirname(dirname(dirname(__FILE__)));

require_once($privateBase . '/framework/functions/sanitize.php');

//...

$privateBase = dirname(dirname(dirname(__FILE__)));

require_once($privateBase .  '/framework/mailer/Emailer.php');

require_once($privateBase .  '/framework/mailer/EmailTemplate.php');
