/*
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

(function() {

  var Marzipano = window.Marzipano;

  var bowser = window.bowser;

  var screenfull = window.screenfull;

  var data = window.APP_DATA;

  var panoElement = document.querySelector('#pano');

  if (window.matchMedia) {

    var setMode = function() {

      if (mql.matches) {

        document.body.classList.remove('desktop');

        document.body.classList.add('mobile');

      } else {

        document.body.classList.remove('mobile');

        document.body.classList.add('desktop');

      }

    };

    var mql = matchMedia("(max-width: 500px), (max-height: 500px)");

    setMode();

    mql.addListener(setMode);

  } else {

    document.body.classList.add('desktop');
  }

  document.body.classList.add('no-touch');

  window.addEventListener('touchstart', function() {

    document.body.classList.remove('no-touch');

    document.body.classList.add('touch');

  });

  if (bowser.msie && parseFloat(bowser.version) < 11) {

    document.body.classList.add('tooltip-fallback');


  }

  // Viewer options.
  var viewerOpts = {
    controls: {
      mouseViewMode: data.settings.mouseViewMode
    }
  };

  // Initialize viewer.
  var viewer = new Marzipano.Viewer(panoElement, viewerOpts);

  var autorotate = Marzipano.autorotate({
    yawSpeed: 0.03,
    targetPitch: 0,
    targetFov: Math.PI/2
  });

  viewer.startMovement(autorotate);

  viewer.setIdleMovement(3000, autorotate);

  // Create scenes.
  var scenes = data.scenes.map(function(data) {
    var urlPrefix = "tiles";
    var source = Marzipano.ImageUrlSource.fromString(
      urlPrefix + "/" + data.id + "/{z}/{f}/{y}/{x}.jpg",
      { cubeMapPreviewUrl: urlPrefix + "/" + data.id + "/preview.jpg" });
    var geometry = new Marzipano.CubeGeometry(data.levels);

    var limiter = Marzipano.RectilinearView.limit.traditional(data.faceSize, 100*Math.PI/180, 120*Math.PI/180);
    var view = new Marzipano.RectilinearView(data.initialViewParameters, limiter);

    var scene = viewer.createScene({
      source: source,
      geometry: geometry,
      view: view,
      pinFirstLevel: true
    });


    return {
      data: data,
      scene: scene,
      view: view
    };
  });





  function switchScene(scene) {

    scene.view.setParameters(scene.data.initialViewParameters);

    scene.scene.switchTo();

  }

    switchScene(scenes[0]);

})();
