<?php

include('../museumSmartAdmin/dist/default/private/users/core/init.php');

include_once('../private/php/libs/Mobile_Detect.php');

$wording = new Wording();

$wording->load('home', 'home');

$myValue = $wording->get('landing_second_line');

$origin = 'web';

$db = DB::getInstance();

$query = $db->query('SELECT * from museum_virtual_classes WHERE is_experience = ?', [1]);

$classResult = $query->first();

?>

<head>

	<!-- general meta -->

	<?php include_once('../private/php/includes/structure/meta.php');?>

	<!-- Facebook-->

	<meta property="og:image" content="<?php echo $sharingImage;?>">

	<meta property="og:image:height" content="1000">

	<meta property="og:image:width" content="2000">

	<meta property="og:title" content="<?php echo $classResult->name;?>">

	<meta property="og:description" content="<?php echo strip_tags($classResult->description);?>">

	<meta property="og:url" content="<?php echo $actualLink;?>">

	<!-- Twitter -->

	<meta name="twitter:card" content="summary" />

	<meta name="twitter:site" content="@museoshoa" />

	<meta name="twitter:title" content="<?php echo $classResult->name;?>"/>

	<meta name="twitter:description" content="<?php echo strip_tags($classResult->description);?>"/>

	<meta name="twitter:image" content="<?php echo $sharingImage;?>" />

	<!-- title -->

	<title>recorrido virtual 360°</title>

	<!-- styles -->

	<link href="../private/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<link href="../private/css/sweetalert2.min.css" rel="stylesheet">

	<link href="../private/css/fontawesome/all.css" rel="stylesheet" type="text/css">

	<link href="../private/vendor/blueimp-gallery/css/blueimp-gallery.min.css" rel="stylesheet" type="text/css">

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

	<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

	<link href="../private/css/style.css" rel="stylesheet">

	<link href="../private/css/effects.css" rel="stylesheet" type="text/css">

	<link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.css" rel="stylesheet">


	<link rel="stylesheet" href="style.css">

	<?php include_once('../private/php/includes/structure/favicons.php');?>

</head>

<body id="page-top">

	<?php include('../private/php/includes/modals/virtual-class-reservation.php');?>

	<?php include('../private/php/includes/modals/virtual-class-reservation-institution.php');?>

	<?php include('../private/php/includes/structure/nav.php');?>

	<input type="hidden" id="class-id" name="class-id" value="<?php echo $classResult->id;?>">

	<input type="hidden" id="class-name" name="class-name" value="<?php echo $classResult->name;?>">

	<input type="hidden" id="class-img" name="class-img" value="<?php echo $myImg;?>">

	<input type="hidden" id="class-img-retina" name="class-img-retina" value="<?php echo $myImgRetina;?>">

	<input type="hidden" id="default-img" name="default-img" value="<?php echo $defaultImg;?>">

	<?php include('../private/php/includes/headers/360/header.php');?>

	<?php include('../private/php/includes/modules/experience/info.php');?>

	<?php /*include('../private/php/includes/modules/experience/timeline.php');*/?>

	<?php include('../private/php/includes/structure/footer.php');?>

	<script src="vendor/es5-shim.js"></script>

	<script src="vendor/eventShim.js"></script>

	<script src="vendor/classList.js"></script>

	<script src="vendor/requestAnimationFrame.js" ></script>

	<script src="vendor/bowser.min.js" ></script>

	<script src="vendor/marzipano.js" ></script>

	<script src="data.js"></script>

	<script src="360.js"></script>

	<script src="../private/vendor/jquery/jquery.min.js"></script>

	<script src="../private/vendor/scrollreveal/scrollreveal.min.js"></script>

	<script src="../private/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<script src="../private/vendor/gsap/src/minified/TweenMax.min.js"></script>

	<script src="../private/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

	<script src="../private/vendor/jquery-validation/jquery.validate.min.js"></script>

	<script src="../private/vendor/jquery-form/jquery.form.min.js"></script>

	<script src="../private/vendor/sweetalert2/sweetalert2.all.min.js"></script>

	<script src="../private/vendor/block-ui/jquery.blockUI.js"></script>

	<script src="../private/vendor/blueimp-gallery/js/blueimp-gallery.min.js"></script>

	<script src="../private/vendor/blueimp-gallery/js/blueimp-gallery-youtube.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/locale/es.js"></script>

	<script src="../private/js/generalConf.min.js"></script>

	<script src="../private/js/helpers.min.js"></script>

	<script src="../private/js/experience.min.js"></script>


</body>
