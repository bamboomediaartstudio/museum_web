/**
 * @summary Main page for Museo Del Holocausto / Bueno sAires / Argentina.
 *
 * @description -
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */
 "use strict";

/**
 * @function InitBooking
 * @description Initialize and include all the methods for this page.
 */

 var InitBooking = function() {

 	console.log("test stamoss?");

 	var dayWasSelected = false;

 	var groupDayWasSelected = false;

 	var isIndividual = false;

 	var isGroup = false;

 	var groupBookingsArray = new Array();

 	var totalBookings = 0;

 	var actualBookingId;

 	var callReference;

 	var config = new GeneralConfiguration();

 	var helpers = new Helpers();

 	var path = location.protocol + '//' + location.host;

 	var institutionalBookingsCounter = 0;

 	var readablePlace;

 	var readableHour;

 	var readableDay;

 	var finalStudents;

 	var origin = $('#origin').val();

	/**
	* @function InitBooking
	* @description Initialize and include all the methods for this page.
	*/

	// add the notEqual rule here
  	jQuery.validator.addMethod("notEqual", function(value, element, param) {

    	return this.optional(element) || value != param;

  	}, "Please specify a different (non-default) value");

	var formValidation = function() {

		$('#group-inscription-form').validate({

			rules: {

				'inscription-institution-name' :{required:true},

				'inscription-institution-cuit' :{required:true, number:true},

				'inscription-institution-address' : {required: true},

				'inscription-institution-phone' : {required: true, number: true},

				'inscription-institution-email' : {required: true, email: true},

				'inscription-institution-state' : {required: true},

				'inscription-institution-city' : {required: true},

				'education-level' : {required: true},

				'institution-type' : {required: true},

				'special-need' : {required: true},

				'first-time' : {required: true},

				'institution-first-time' : {required: true},

				'inscription-contact-name' : {required: true},

				'inscription-contact-email' : {required: true, email:true},

				'inscription-contact-phone' : {required: true, number:true},

				'students-selector' : {required: true, notEqual: "0"}

			},

			messages: {

				'inscription-institution-name' : 'obligatorio',

				'inscription-institution-cuit' :'obligatorio', 

				'inscription-institution-address' : 'obligatorio',

				'inscription-institution-phone' : 'obligatorio',

				'inscription-institution-email' : 'obligatorio',

				'inscription-institution-state' : 'obligatorio',

				'inscription-institution-city' : 'obligatorio',

				'education-level' : 'obligatorio',

				'institution-type' : 'obligatorio',

				'special-need' : 'obligatorio',

				'first-time' : 'obligatorio',

				'institution-first-time' : 'obligatorio',

				'inscription-contact-name' : 'obligatorio',

				'inscription-contact-email' : 'obligatorio',

				'inscription-contact-phone' : 'obligatorio',

				'students-selector' : 'Obligatorio'

			},

			errorPlacement: function(error, element) {

				if (element.attr("name") == "education-level" ){

					error.insertBefore(".education-level-error");

				}else if(element.attr('name') == 'institution-type'){

					error.insertBefore(".institution-type-error");

				}else if(element.attr('name') == 'special-need'){

					error.insertBefore(".special-need-error");

				}else if(element.attr('name') == 'first-time'){

					error.insertBefore(".first-time-error");

				}else if(element.attr('name') == 'institution-first-time'){

					error.insertBefore(".institution-first-time-error");

				}else{

					error.insertAfter(element);

				}

			},

			invalidHandler: function(e, r) {

				helpers.doPopUp('ERROR EN EL FORMULARIO', 'Hay errores en el formulario. Por favor, revisalo.', 'CERRAR', 'error');

			},

			submitHandler: function(form, event) {

				if(totalBookings != groupBookingsArray.length && isGroup){

					var rest = totalBookings - groupBookingsArray.length;

					var wording = (rest == 1) ? 'TURNO' : 'TURNOS';

					var popupTitle = 'FALTA ELEGIR ' + rest + ' ' + wording;

					var content = 'Para el total de ' + finalStudents + ' alumnos, hace falta seleccionar ' + totalBookings + ' turnos, de los cuales elegiste ' + groupBookingsArray.length + '. Por  favor, cerrá este pop up y seleccioná del calendario el/los turnos/s faltante/s.';

					helpers.doPopUp(popupTitle, content, 'CERRAR', 'error');

					return;

				}else{

					$('#group-reservation').modal('hide');

					helpers.blockStage('GENERANDO RESERVA...');

					$(form).ajaxSubmit({

						cache: false,

						url: path + '/museumSmartAdmin/dist/default/private/users/museum/booking/add_new_group_reservation.php',

						type: 'post',

						data: {bookings_ids:groupBookingsArray, institution_students:finalStudents, origin:origin},

						dataType: 'json',

						success: function(response, status, xhr, $form) {

							$('#group-inscription-form')[0].reset();

							$('#group-reservation').modal('hide');

							groupDayWasSelected = false;

							var msg = "Gracias por tu inscripción a las visitas guiadas grupales. Te enviamos un mail con toda la información. De no recibirlo, revisá la carpeta de spam.";

							helpers.doCallbackPopUp('RESERVA GENERADA EXITOSAMENTE', msg, 'CERRAR', 'success', reloader);

							/*swal({

								type: 'success',

								title: 'RESERVA GENERADA EXITOSAMENTE!',

								html: 'Te enviamos un correo automático con la información de la reserva, días, horarios y turnos. De no recibirlo, tené a bien revisar la carpeta de spam. <br><br>Nos comunicaremos a la brevedad para informarte respecto a los aranceles y la modalidad de pago.',

								confirmButtonText: 'cerrar',

								confirmButtonClass: 'generic-action-button btn',

								buttonsStyling: false,

								imageWidth: 600,

								imageHeight: 600

							}).then((result) => {

								console.log("que onda");

								location.reload();

							});*/

							helpers.unblockStage();

							$('.previous-calendar').hide();

							callReference.fullCalendar('removeEvents');

							callReference.fullCalendar('refetchEvents');
							
							callReference.fullCalendar('destroy');

							$('.days-checker').empty();

							groupBookingsArray = [];

							console.log('reload');

							setTimeout(reloader(), 3000);

						},

						error: function(response, status, xhr, $form) {

							console.log("hay error");

							console.log(response);
						}

					});

				}				

			}

		})

		$('#virtual-inscription-form').validate({

			rules: {

				'virtual-inscription-name': { required: true },

				'virtual-inscription-surname': { required: true },

				'virtual-inscription-email': {

					required: true,

					email: true
				},

				'virtual-inscription-phone': { required: true },

				'virtual-inscription-dni' : { required: true, number: true }
			},

			messages: {

				'virtual-inscription-name': 'El nombre obligatorio',

				'virtual-inscription-surname': 'El apellido es obligatorio',

				'virtual-inscription-email': 'El email es obligatorio',

				'virtual-inscription-phone': 'El teléfono es obligatorio',

				'virtual-inscription-dni': 'El dni es obligatorio'

			},

			errorPlacement: function(error, element) {

				if (element.attr("name") == "visit-reason" ){

					error.insertBefore(".visit-reason-error");

				}else if(element.attr("name") == "about-museum"){

					error.insertBefore(".about-museum-error");

				}else{

					error.insertAfter(element);

				}

			},

			invalidHandler: function(e, r) {

				helpers.doPopUp('ERROR EN EL FORMULARIO', 'Hay errores en el formulario. Por favor, revisalo.', 'CERRAR', 'error');

			},

			submitHandler: function(form, event) {

				if(dayWasSelected == true){

					$('#virtual-reservation').modal('hide');

					helpers.blockStage('GENERANDO RESERVA...');

					$(form).ajaxSubmit({

						cache: false,

						url: path + '/museumSmartAdmin/dist/default/private/users/museum/booking/add_new_virtual_reservation.php',

						data: {readablePlace:readablePlace, readableHour:readableHour, readableDay:readableDay, origin:origin},

						type: 'post',

						dataType: 'json',

						success: function(response, status, xhr, $form) {

							console.log(response);

							$('#virtual-inscription-form')[0].reset();

							$('#virtual-inscription-form').modal('hide');

							dayWasSelected = false;

							if(origin == 'web'){

								helpers.doPopUp('RESERVA GENERADA EXITOSAMENTE!', 'Recibimos tu reserva. Te enviamos un correo con la información. De no recibirlo, revisá la carpeta de SPAM.', 'CERRAR', 'success');

							}else{

								helpers.doPopUp('RESERVA GENERADA', 'Generaste la reserva exitosamente. Se le envió un correo con información del alta inscripto.', 'CERRAR', 'success');

							}

							helpers.unblockStage();

							console.log('reload');

							setTimeout(reloader(), 3000);
						},

						error: function(response, status, xhr, $form) {

							console.log("error!");

							console.log("hay error");

							console.log(response);

							console.log(status);

							console.log(xhr.responseText);

							JSON.parse(xhr.responseText)

						}

					});

				}else{

					helpers.doPopUp('FALTA ELEGIR EL TURNO', 'Por favor, seleccioná uno de los turnos disponibles para el día seleccionado', 'CERRAR', 'error');

				}

			} 

		});



		$('#inscription-form').validate({

			rules: {

				'inscription-name': { required: true },

				'inscription-surname': { required: true },

				'inscription-email': {

					required: true,

					email: true
				},

				'inscription-phone': { required: true },

				'inscription-dni' : { required: true, number: true },

				'visit-reason' : { required: true },

				'about-museum' : { required: true }

			},

			messages: {

				'inscription-name': 'El nombre obligatorio',

				'inscription-surname': 'El apellido es obligatorio',

				'inscription-email': 'El email es obligatorio',

				'inscription-phone': 'El teléfono es obligatorio',

				'inscription-dni': 'El dni es obligatorio',

				'visit-reason': 'Debes indicar el motivo de la visita',

				'about-museum': 'Debes indicar al menos una opción'

			},

			errorPlacement: function(error, element) {

				if (element.attr("name") == "visit-reason" ){

					error.insertBefore(".visit-reason-error");

				}else if(element.attr("name") == "about-museum"){

					error.insertBefore(".about-museum-error");

				}else{

					error.insertAfter(element);

				}

			},

			invalidHandler: function(e, r) {

				helpers.doPopUp('ERROR EN EL FORMULARIO', 'Hay errores en el formulario. Por favor, revisalo.', 'CERRAR', 'error');

			},

			submitHandler: function(form, event) {

				if(dayWasSelected == true){

					$('#individual-reservation').modal('hide');

					helpers.blockStage('GENERANDO RESERVA...');

					$(form).ajaxSubmit({

						cache: false,

						url: path + '/museumSmartAdmin/dist/default/private/users/museum/booking/add_new_individual_reservation.php',

						data: {readablePlace:readablePlace, readableHour:readableHour, readableDay:readableDay, origin:origin},

						type: 'post',

						dataType: 'json',

						success: function(response, status, xhr, $form) {

							console.log(response);

							$('#inscription-form')[0].reset();

							$('#inscription-form').modal('hide');

							dayWasSelected = false;


							var msg = '';


							if(origin == 'web'){

								msg = 'Recibimos tu reserva. Te enviamos un correo con la información. De no recibirlo, revisá la carpeta de SPAM.';

								//helpers.doPopUp('RESERVA GENERADA EXITOSAMENTE!', 'Recibimos tu reserva. Te enviamos un correo con la información. De no recibirlo, revisá la carpeta de SPAM.', 'CERRAR', 'success');

							}else{

								msg = 'Generaste la reserva exitosamente. Se le envió un correo con información del alta inscripto.'

								//helpers.doPopUp('RESERVA GENERADA', 'Generaste la reserva exitosamente. Se le envió un correo con información del alta inscripto.', 'CERRAR', 'success');

							}

							helpers.doCallbackPopUp('RESERVA GENERADA EXITOSAMENTE', msg, 'CERRAR', 'success', reloader);

							helpers.unblockStage();

							
						},

						error: function(response, status, xhr, $form) {

							console.log("error!");

							console.log("hay error");

							console.log(response);

							console.log(status);

							console.log(xhr.responseText);

							JSON.parse(xhr.responseText)

						}

					});

				}else{

					helpers.doPopUp('FALTA ELEGIR EL TURNO', 'Por favor, seleccioná uno de los turnos disponibles para el día seleccionado', 'CERRAR', 'error');

				}

			} 

		});

		}

var reloader = function(){

	location.reload();

}

var addListeners = function(){

	$('.go-to-virtual-visit').click(function(){

		$('#virtual-reservation').modal('show');
	
		initCalendar(6, $('#m_calendar_6'));


	})


	console.log("listeners");

	$('body').on('keyup', '.tickets-stepper', function (event) { 

	    var v = parseInt($(this).val());
        
        var min = parseInt($(this).attr('min'));
        
        var max = parseInt($(this).attr('max'));

        console.log(v + " - " +  min  + " - " +  max);

        if (v < min){

            $(this).val(min);
        
        } else if (v > max){

        	$(this).val(max);
        }

        console.log("apply");
    })

	$('.previous-calendar').hide();

	$('#students-selector').change(function() {

		var studentsPerBooking = 25;

		var students = parseInt($(this).val());

		var counter = $('#groupCount').val();

		var total = Math.ceil(students / studentsPerBooking);

		var maxAmmountOfStudents = studentsPerBooking * counter;

		if((counter * studentsPerBooking) < students){

			var string = 'Para un total de ' + students + ' personas, hacen falta ' + total + ' turnos, pero actualmente solo tenemos  ' + counter + ' disponibles. Por favor, reducí la cantidad de personas a un máximo de ' + maxAmmountOfStudents + ', o escribinos a visitasguiadas@museodelholocausto.org.ar'; 

			helpers.doPopUp('NO HAY SUFICIENTES TURNOS', string, 'CERRAR', 'error');

			return;

		}else{

			var title = students + ' ALUMNOS';

			var bookingWord = (total == 1) ? ' turno.' : ' turnos.';

			totalBookings = total;

			finalStudents = students;

			var content = 'Para un total de <strong>' + students + ' alumnos</strong> hace falta reservar <strong>' + total + bookingWord + '</strong>';

			if(total > 1) content += '<br><br>Navegá el calendario y seleccioná los turnos que mejor se adapten a tu agenda. Podés seleccionar varios turnos en un mismo día, o turnos en distintos días.';

			helpers.doPopUp(title, content, 'CERRAR', 'success');

			if(callReference != undefined){

				callReference.fullCalendar('removeEvents');

				callReference.fullCalendar('refetchEvents');

			}

			groupBookingsArray = [];

			$('.days-checker').empty();

			$('.days-checker').append('<p class="mt-1 mr-2 text-bold text-uppercase text-center">' + groupBookingsArray.length + ' de ' + totalBookings + ' turnos elegidos</p>');

			initCalendar(1, $('#m_calendar_1'));

			$('.previous-calendar').show();

		}

	});​


	$('#group-reservation').on('hidden.bs.modal', function () {

		groupDayWasSelected = false;

		$(".events-by-day").empty();

	});

	$('#individual-reservation').on('hidden.bs.modal', function () {

		dayWasSelected = false;

		$(".events-by-day").empty();

		$(".booking-id").val("");

		$('#tickets-per-person').val("");

	});

	$('body').on('change', '.tickets-stepper', function (event) { 

		$("#tickets-per-person").val($(this).val());

	});

	$('body').on('click', '.date-selector-div', function (event) {

		var numberPattern = /\d+/g;

		var id = $(this).attr('id').match(numberPattern)

		readablePlace = $(this).attr('date-place');

		readableDay = $(this).attr('date-readable-date');

		readableHour = $(this).attr('date-hour-string');

		console.log(readablePlace, readableDay, readableHour);


		actualBookingId = id;

		$(".booking-id").val(id);

		dayWasSelected = true;

		groupDayWasSelected = true;

		if(!isGroup){

			$(".date-selector-div").each(function() { $(this).removeClass('selected-date') });

			$(this).addClass('selected-date');

		}else{

			console.log("entering");

			var removeIt = false;

			var atPosition = 0;

			for(var i=0; i<groupBookingsArray.length; i++){

				if(parseInt(groupBookingsArray[i]) === parseInt(id) ){

					atPosition = i;

					removeIt = true;

					break;
				}
			}

			if(removeIt == true){

				groupBookingsArray.splice(atPosition, 1);	

				$(this).removeClass('selected-date');

			}else{

				groupBookingsArray.push(id);

				$(this).addClass('selected-date');

			}

			$('.days-checker').empty();

			$('.days-checker').append('<p class="mt-1 mr-2 text-bold text-uppercase text-center">' + groupBookingsArray.length + ' de ' + totalBookings + ' turnos elegidos</p>');

			var title;

			var content;

			if(removeIt){

				title = 'TURNO ELIMINADO';

				content = 'Eliminaste uno de los ' + totalBookings + ' turnos necesarios.';

			}else{

				title = 'TURNO SELECCIONADO';

				content = 'Añadiste uno de los ' + totalBookings + ' turnos necesarios.';

			}

			helpers.showToastr(content, title, ((removeIt == true) ? 'error' : 'success'));

			if(totalBookings == groupBookingsArray.length && isGroup){

				var string = 'Ya seleccionaste los <b>' + totalBookings + '</b> turnos necesarios para un total de ' + finalStudents + ' alumnos. Para completar el proceso de la reserva, cerrá este mensaje y presioná el botón "reservar turno/s"';

				helpers.doPopUp('TURNOS SELECCIONADOS', string, 'CERRAR', 'success');

			}

		}


	})

	
	$('.go-to-group-visit').click(function() {

		var counter = $('#groupCount').val();

		if(counter == 0){

			if(origin == 'web'){

				helpers.doPopUp('FECHAS NO DISPONIBLES', 'Por el momento no hay fechas disponibles para las visitas grupales.<br><br>Te pedimos que vuelvas a intentarlo próximamente o te comuniques a visitasguiadas@museodelholocausto.org.ar para más información.', 'CERRAR', 'success');

			}else{

				helpers.doPopUp('NO HAY FECHAS EN EL SISTEMA', 'Antes de poder añadir a una institución, tiene que haber alguna fecha libre asignada en el sistema.', 'CERRAR', 'success');

			}

			return;

		}else{

			$('#group-reservation').modal('show');

			isGroup = true;

		}

	})

	$('.go-to-individual-visit').click(function() {

		var counter = $('#individualCount').val();

		if(counter == 0){

			if(origin == 'web'){

				helpers.doPopUp('FECHAS NO DISPONIBLES', 'Por el momento no hay fechas disponibles para las visitas individuales.<br><br>Te pedimos que vuelvas a intentarlo próximamente o te comuniques a visitasguiadas@museodelholocausto.org.ar para más información.', 'CERRAR', 'success');

			}else{

				helpers.doPopUp('FECHAS NO DISPONIBLES', 'No hay fechas dispinibles en el calendario.<br><br>Da de alta una nueva <strong>visita individual</strong> antes de poder dar de alta a un nuevo usuario.', 'CERRAR', 'success');

			}

			return;

		}else{

			$('#individual-reservation').modal('show');

			initCalendar(2, $('#m_calendar_2'));

		}

	})


}

	/**
	* @function initCalendar
	* @description Init the calendar :D
	*/

	var initCalendar = function(sid, cal) {

		cal.fullCalendar({

			events: function(start, end, timezone, callback) {

				jQuery.ajax({

					url: path + '/museumSmartAdmin/dist/default/private/users/services/get_list_of_events.php',

					type: 'POST',

					dataType: 'json',

					data: {condition:'before', start: start.format(), end: end.format() },

					success: function(doc) {

						var events = [];

						$.map(doc, function( r ) {

							if(r.category_id != sid || (r.available_places == r.booked_places || r.event_active == 0)) return;

							events.push({

								allDay:true,

								rendering: 'background',

								id: r.id, 

								title: r.title, 

								start: r.date_start,

								className : r.className,

								color: '#124c96',

								category_id: r.category_id,

								total_days: r.total_days,

								end: r.date_end,

								end_copy: r.date_end,

								place: r.place,

								available_places: r.available_places,

								booked_places: r.booked_places,

								tickets_per_visitor: r.tickets_per_visitor,

							});

							//events.sort(function(a,b){ return new Date(b.start) - new Date(a.start); });

							//console.log("sorting!");

						});

						console.log(events);

						callback(events);
					}

				});

			},

			dayClick: function(date, jsEvent, view) {

				dayWasSelected = false;

				groupDayWasSelected = false;

				var parsedEvents = new Array();

				cal.fullCalendar('clientEvents', function (event) {

					if (moment(event.start).isSame(date, 'day')) {

						var object = new Object();

						object.id = event.id;

						object.title = event.title;

						object.category_id = event.category_id;

						object.available_places = event.available_places;

						object.booked_places = event.booked_places;

						object.booked_places = event.booked_places;

						object.tickets_per_visitor = event.tickets_per_visitor;

						object.start = event.start;

						object.end_copy = event.end_copy;

						object.place = ((event.category_id == 5) ? 'virtual: tu pc o celular.' : event.place);

						var startingAt  = moment(event.start._i);

						var endingAt = moment(event.end_copy);

						var ms = moment(endingAt,"DD/MM/YYYY HH:mm:ss").diff(moment(startingAt,"DD/MM/YYYY HH:mm:ss"));

						var d = moment.duration(ms);

						object.duration = Math.floor(d.asHours()) + moment.utc(ms).format(":mm") + ' hs.';

						object.start_hour = moment(event.start._i).format('hh:mm a');

						object.end_hour = moment(event.end_copy).format('hh:mm a');

						object.readable_date = event.start.format('ll');

						parsedEvents.push(object);

						//2022-03-11 remove to avoid bad sort hour
						//parsedEvents.sort((a,b) => a.start_hour.localeCompare(b.start_hour));	
						
						console.log("parsed!");


						//parsedEvents.sort(function(a,b){ return new Date(b.start_hour) - new Date(a.start_hour); });
					}


				})

				//console.log(`parsedEvents arr: `);
				//console.log(parsedEvents);

				createEventsByDay(parsedEvents);

			},

			selectable: true,

			height: 380,

			eventLimit: true,

			navLinks: false,

			themeSystem: 'bootstrap4',

			slotDuration: '00:15:00',

			bootstrapFontAwesome: {

				close: 'fa-times',

				prev: 'fa-chevron-left',

				next: 'fa-chevron-right',

				prevYear: 'fa-angle-double-left',

				nextYear: 'fa-angle-double-right'
			}

		});

		callReference = cal;

	}

	var compare = function(a,b) {

		var time1 = parseFloat(a.fc_start_time.replace(':','.').replace(/[^\d.-]/g, ''));

		var time2 = parseFloat(b.fc_start_time.replace(':','.').replace(/[^\d.-]/g, ''));

		if(a.fc_start_time.match(/.*pm/)) time1 += 12; if(b.fc_start_time.match(/.*pm/)) time2 += 12;

		if (time1 < time2) return -1;

		if (time1 > time2) return 1;

		return 0;

	}

	/**
	* @function createEventsByDay
	* @description create all the events by day...
	*/

	var createEventsByDay = function(parsedEvents){

		TweenMax.to($(".events-by-day"), .2, {alpha:0, onComplete:generateList});

		function generateList(){

			$(".events-by-day").empty();

			var textInfo = '';

			if(parsedEvents.length == 0){

				textInfo = 'No quedan visitas guiadas para este día. Los días que aparecen coloreados en el calendario son los que aún cuentan con disponibilidad.';

			}else if(parsedEvents.length == 1){

				textInfo = 'Hay un único horario disponible para el día ' + parsedEvents[0].readable_date;

			}else{

				textInfo = 'Seleccioná una de las ' + parsedEvents.length  + ' opciones disponibles para el día ' + parsedEvents[0].readable_date;

			}

			//$(".events-by-day").append('<p class="mt-1 mr-2">' + groupBookingsArray.length + ' de ' + totalBookings + ' turnos elegidos</p>');

			$(".events-by-day").append('<p class="mt-1 mr-2">' + textInfo + '</p>');

			for(var i= 0; i<parsedEvents.length; i++){

				var isInArray = false;

				for(var j = 0; j<groupBookingsArray.length; j++){

					if(parsedEvents[i].id == groupBookingsArray[j]) isInArray = true;

				}

				var startAt = parsedEvents[i].start_hour;

				var endAt = parsedEvents[i].end_hour;

				var duration = parsedEvents[i].duration;

				var itemId = 'day_selector_n_' + parsedEvents[i].id;

				var available = parseInt(parsedEvents[i].available_places) - parseInt(parsedEvents[i].booked_places);

				var bookings = available + ' disponibles de ' + parsedEvents[i].available_places;

				var hourString = 'de ' + startAt + " a " + endAt + '  <strong> (duración:</strong> ' + duration + ')';

				var check = parseInt(parsedEvents[i].booked_places) + parseInt(parsedEvents[i].tickets_per_visitor);

				var applyMax;

				if(check > parseInt(parsedEvents[i].available_places)){

					applyMax = parseInt(parsedEvents[i].available_places) - parseInt(parsedEvents[i].booked_places);

				}else{

					applyMax = parsedEvents[i].tickets_per_visitor;

				}

				if(parsedEvents[i].category_id ==2  || parsedEvents[i].category_id ==5){

					//$(".events-by-day").append('<div id=' + itemId + ' class="date-selector-div" date-place="' + parsedEvents[i].place + '" date-readable-date="' + parsedEvents[i].readable_date + '" date-hour-string="'+ hourString +'"><div class="row col-12  mt-3"><p class="date-selector-div-text-info text-light col-12 mt-3"><i class="fas fa-map-marker-alt mr-2"></i><strong>lugar: </strong>' + parsedEvents[i].place + '<br><i class="fas fa-calendar-alt mr-2"></i><strong>día: </strong>' + parsedEvents[i].readable_date + '<br><i class="fas fa-clock mr-2"></i><strong>horario:</strong> ' + hourString + ' <br><i class="fas fa-ticket-alt mr-2"></i><strong>turnos disponibles:</strong>' + bookings + '<br><br>Indicanos la cantidad de lugares que querés reservar: <input class="tickets-stepper ml-2" type="number" name="tickets-stepper" min="1" max="' + applyMax + '" step="1" value="1"></p></div></div>');
					
					$(".events-by-day").append('<div id=' + itemId + ' class="date-selector-div" date-place="' + parsedEvents[i].place + '" date-readable-date="' + parsedEvents[i].readable_date + '" date-hour-string="'+ hourString +'"><div class="row col-12  mt-3"><p class="date-selector-div-text-info text-light col-12 mt-3"><i class="fas fa-map-marker-alt mr-2"></i><strong>lugar: </strong>' + parsedEvents[i].place + '<br><i class="fas fa-calendar-alt mr-2"></i><strong>día: </strong>' + parsedEvents[i].readable_date + '<br><i class="fas fa-clock mr-2"></i><strong>horario:</strong> ' + hourString + ' <br><i class="fas fa-ticket-alt mr-2"></i><strong>turnos disponibles: </strong>' + bookings + '</p></div></div>');

				}else{

					$(".events-by-day").append('<div id=' + itemId + ' class="'+ ((isInArray) ? 'selected-date ' : '') + 'date-selector-div"><div class="row col-12  mt-3"><p class="date-selector-div-text-info text-light col-12 mt-3"><i class="fas fa-map-marker-alt mr-2"></i><strong>lugar: </strong>' + parsedEvents[i].place + '<br><i class="fas fa-calendar-alt mr-2"></i><strong>día: </strong>' + parsedEvents[i].readable_date + '<br><i class="fas fa-clock mr-2"></i><strong>horario:</strong> ' + hourString + '</p></div></div>');

				}

			}

			TweenMax.to($(".events-by-day"), .2, {alpha:1});
		}

	}

	return {

		init: function() {

			formValidation();

			addListeners();

		}

	}

}();

jQuery(document).ready(function() { InitBooking.init(); });