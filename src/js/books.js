/**
* @summary La Shoa books -  Museo Del Holocausto / Bueno sAires / Argentina.
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/


"use strict";

/**
* @function InitShoaPage
* @description Initialize and include all the methods for this page.
*/

var InitShoaPage = function() {

      /*constructors*/

      var config = new GeneralConfiguration(); 

      var helpers = new Helpers();

      /**
      * @function addListeners
      * @description initialize all the listeners, methods, initializars for this page.
      */

      var addListeners = function(){

            $('.more-info').click(function(){

                  helpers.doPopUp('PROXIMAMENTE DATA! :)', 'Tenenos paciencia: estamos terminando esta sección :)', 'ENTENDIDO', 'success');

                  /*var description = $(this).parent().parent().parent().find('.description').val();

                  $('.modal-title').text("You new title");

                  $('.modal-body').text(description);

                  $('.modal').modal('show');*/

            })

      }

      /**
      * @function animateIn
      * @description Create all the animations for this page.
      */

      var animateIn = function(){

            TweenMax.to('.my-overlay', 2, {alpha:.7, delay:1.3});

            TweenMax.to('.main-text', 2, {alpha:1, delay:1.3});

            TweenMax.to('.main-separator', 2, {alpha:1, delay:1.5});

            TweenMax.to('.second-text', 2, {alpha:1, delay:1.7});

            TweenMax.to('.quote-text', 2, {alpha:1, delay:1.7});

            TweenMax.to('#countdown-container', 2, {alpha:1, delay:2});

            TweenMax.to('.navbar-brand', 2, {alpha:1, delay:2.5});

            TweenMax.to('.navbar-brand, .navbar-nav', 2, {alpha:1, delay:2.5});

            TweenMax.to(".st0", 5, { fill: "rgb(255,255,255)" });
      }

      /**
      * @function createScrollRevealItems
      * @description Create all the scroll reveal settings.
      */

      var createScrollRevealItems = function(){

            /*window.sr = ScrollReveal();

            sr.reveal('.module-title', {delay:0, duration:1000, scale:.90, opacity:0, easing: 'ease'});

            sr.reveal('.module-separator', {delay:100, duration:1000, scale:.90, opacity:0, easing: 'ease'});

            sr.reveal('.module-text', {delay:200, duration:1000, scale:.90, opacity:0, easing: 'ease'});

            sr.reveal('.sponsors-logos', {  delay:500, duration:1000, easing: 'ease', opacity:0, scale:.95});

            sr.reveal('.module-down', {delay:550, duration:1000, scale:.90, opacity:0, easing: 'ease'});

            sr.reveal('.sr-icon-1', { delay: 220, interval: 200 });

            sr.reveal('.sr-icon-2', { delay: 320, scale: 0 });

            sr.reveal('.sr-icon-3', { delay: 600, scale: 0 });

            sr.reveal('.sr-icon-4', {delay: 800, scale: 0 });

            sr.reveal('.sr-button', { delay: 200, distance: '15px', origin: 'bottom', scale: 0.8 });

            sr.reveal('.sr-contact-1', { delay: 200, scale: 0 });

            sr.reveal('.sr-contact-2', { delay: 400, scale: 0 });*/

      }

      return {

            init: function() {

                  addListeners();

                  animateIn();

                  createScrollRevealItems();

                  helpers.manageNavCollapse();

                  $(window).scroll(helpers.manageNavCollapse);
            }

      };

}();

jQuery(document).ready(function() { InitShoaPage.init(); });