/**
* @summary Create all the content for this section.
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/


"use strict";

/**
* @function SectionPage
* @description Initialize and include all the methods for this page.
*/

var SectionPage = function() {

      /*constructors*/

      var config = new GeneralConfiguration(); 

      var helpers = new Helpers();

      /**
      * @function addListeners
      * @description initialize all the listeners, methods, initializars for this page.
      */

      var addListeners = function(){

            var $grid = $('.grid').imagesLoaded( function() {

                  var $grid = $('.grid').isotope({

                        itemSelector: '.element-item',

                        transformsEnabled: false,

                        layoutMode: 'fitRows',

                        getSortData: {

                        }

                  });

            });

            $grid.on( 'arrangeComplete', function( event, filteredItems ) {

                  var resultCount = filteredItems.length;

                  if(resultCount == 0) {

                        $('#no-results').removeClass('d-none');

                  }

            });

            $('#filters').on( 'click', 'button', function() {

                  var index = $(this).index();

                  var filterValue = $( this ).attr('data-filter');

                  $grid.isotope({ filter: filterValue});

                  $('#no-results').addClass('d-none');

                  //window.history.replaceState('', '', updateURLParameter(window.location.href, "id", index));

            });


            $('.button-group').each( function( i, buttonGroup ) {

                  var $buttonGroup = $( buttonGroup );

                  $buttonGroup.on( 'click', 'button', function() {

                        $buttonGroup.find('.is-checked').removeClass('is-checked');

                        $( this ).addClass('is-checked');

                  });

            });

            var urlParams = new URLSearchParams(window.location.search);

            var myParam = urlParams.get('id');

            $('#filters button').eq(0).trigger('click');

            cursorFocus($('#filters button').eq(0));

           
            $('.js-scroll-trigger').click(function() {

                  var navHeight = $('.navbar').height();

                  var $window = $(window);

                  TweenMax.to($window, 1.5, {

                        scrollTo : { y: $('.my-overlay').height() - navHeight},

                        ease: Power1.easeOut,
                        
                  });

            });

      }

      /**
      * @function updateURLParameter
      * @description Update the URL.
      */

      var updateURLParameter = function(url, param, paramVal){

            var newAdditionalURL = "";
            
            var tempArray = url.split("?");
            
            var baseURL = tempArray[0];
            
            var additionalURL = tempArray[1];
            
            var temp = "";
            if (additionalURL) {

                  tempArray = additionalURL.split("&");

                  for (var i=0; i<tempArray.length; i++){

                        if(tempArray[i].split('=')[0] != param){

                              newAdditionalURL += temp + tempArray[i];
                              temp = "&";
                        }
                  }
            }

            var rows_txt = temp + "" + param + "=" + paramVal;
            
            return baseURL + "?" + newAdditionalURL + rows_txt;
      }

      /**
      * @function cursorFocus
      * @description do cursor focus...
      */

      var cursorFocus = function(elem) {

            var x = window.scrollX, y = window.scrollY;

            elem.focus();

            window.scrollTo(x, y);

      }

      /**
      * @function createScrollRevealItems
      * @description Create all the scroll reveal settings.
      */

      var createScrollRevealItems = function(){

            window.sr = ScrollReveal();
            
            sr.reveal('.my-overlay', {delay:0, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.main-text', {delay:200, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.main-separator', {delay:300, duration:1000, opacity:0, easing: 'ease'});
            
            sr.reveal('.second-text', {delay:400, duration:1000, opacity:0, easing: 'ease'});
            
            sr.reveal('.go-to-content', {delay:400, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.section-heading', {delay:0, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.section-description', {delay:100, duration:1000, opacity:0, easing: 'ease'});
            
            sr.reveal('.filter-button', {delay:300, duration:1000, opacity:0, easing: 'ease', interval: 50});

      }

      return {

            init: function() {

                  addListeners();

                  createScrollRevealItems();

                  helpers.manageNavCollapse();
            }

      };

}();

jQuery(document).ready(function() { SectionPage.init(); });