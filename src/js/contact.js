/**
* @summary Contact page for Museo Del Holocausto / Buenos Aires / Argentina.
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/


//"use strict";

/**
* @function InitContactPage
* @description Initialize and include all the methods for this page.
*/

var InitContactPage = function() {

      /*constructors*/

      var config = new GeneralConfiguration(); 

      var helpers = new Helpers();

      var myForm = $('#contact-form');

      var formValidation = function(){

            myForm.validate({

                  rules: {

                        name: {required: true},

                        surname: {required: true},

                        email: {email:true},

                        'form-subject': {required:true},

                        'form-text-area' : {required:true}

                  },

                  messages: {

                        name: 'El nombre es obligatorio.',

                        surname: 'El apellido es obligatorio.',

                        email: 'Dirección de email inválida.',

                        'form-subject':'Debes seleccionar una opción del menú.',

                        'form-text-area' : 'Falta el mensaje'

                  },

                  highlight: function(element) {

                        $(element).closest('.form-group').addClass('has-error');

                  },

                  unhighlight: function(element) {

                        $(element).closest('.form-group').removeClass('has-error');

                  },

                  invalidHandler: function(e, r) {

                        helpers.doPopUp('Error en el formulario.', 'Hay algunos errores en el formulario. Por favor, revisalo.', 'ENTENDIDO', 'error');


                  },

                  submitHandler: function(form, event) {

                        event.preventDefault();

                        helpers.blockStage('ENVIANDO MENSAJE...');

                        $(form).ajaxSubmit({

                              cache: false,

                              url: '../museumSmartAdmin/dist/default/private/users/web/webmail.php',

                              type: 'post',

                              dataType: 'json',

                              data: {},

                              success: function(response, status, xhr, $form) {

                                    console.log(response);

                                    helpers.doPopUp('Mensaje Enviado!', 'Recibimos tu mensaje. Nos comunicaremos a la brevdad.', 'ENTENDIDO', 'success');

                                    $('#contact-form')[0].reset();

                                    helpers.unblockStage();

                              },

                              error: function(response, status, xhr, $form){

                                    console.log(response);

                                    console.log(status);

                                    console.log(xhr);
                              }

                        });

                        return false;

                  }

            });

      }



      /**
      * @function addListeners
      * @description initialize all the listeners, methods, initializars for this page.
      */

      var addListeners = function(){

            $('[data-toggle="tooltip"]').tooltip()

      }

      /**
      * @function animateIn
      * @description Create all the animations for this page.
      */

      var animateIn = function(){

            TweenMax.to('.my-overlay', 2, {alpha:.7, delay:1.3});

            TweenMax.to('.main-text', 2, {alpha:1, delay:1.3});

            TweenMax.to('.main-separator', 2, {alpha:1, delay:1.5});

            TweenMax.to('.second-text', 2, {alpha:1, delay:1.7});

            TweenMax.to('#countdown-container', 2, {alpha:1, delay:2});

            TweenMax.to('.navbar-brand', 2, {alpha:1, delay:2.5});

            TweenMax.to('.navbar-brand, .navbar-nav', 2, {alpha:1, delay:2.5});

            TweenMax.to(".st0", 5, { fill: "rgb(255,255,255)" });
      }

      /**
      * @function initMap
      * @description Initialize the Google Map and create the marker.
      */

      window.initMap = function(){


            var map;

            var myLatlng = new google.maps.LatLng(-34.597945, -58.389643);

            map = new google.maps.Map(document.getElementById('map'), {

                  center: myLatlng,

                  zoom: 15,

                  mapTypeControl: false,

                  fullscreenControl: false,

                  zoomControl: false,

                  styles: config.getMapStyle()

            });

            var marker = new google.maps.Marker({

                  position: myLatlng,

                  title:"Museo Del Holocausto / Buenos Aires / Argentina",

                  icon: '../private/img/markers/custom.png'

            });

            marker.setMap(map);

      }

      return {

            init: function() {

                  addListeners();

                  animateIn();

                  formValidation();

                  helpers.setHeaderType('contact');

                  helpers.manageNavCollapse();

                  $(window).scroll(helpers.manageNavCollapse);

                  helpers.bodySmoothScroll();
            }

      };

}();

jQuery(document).ready(function() { InitContactPage.init(); });