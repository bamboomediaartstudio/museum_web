/**
* @summary Create all the content for this section.
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/


"use strict";

/**
* @function SectionPage
* @description Initialize and include all the methods for this page.
*/

var SectionPage = function() {

      /*constructors*/

      var config = new GeneralConfiguration(); 

      var helpers = new Helpers();

      var myForm = $('#newsletter-form');

      var formValidation = function(){

            myForm.validate({

                  rules: {

                        email: {email:true, required:true}

                  },

                  messages: {

                        email: 'Dirección de email inválida.'

                  },

                  highlight: function(element) {

                        $(element).closest('.form-group').addClass('has-error');

                  },

                  unhighlight: function(element) {

                        $(element).closest('.form-group').removeClass('has-error');

                  },

                  invalidHandler: function(e, r) {

                        //helpers.doPopUp('Error en el formulario.', 'Hay algunos errores en el formulario. Por favor, revisalo.', 'ENTENDIDO', 'error');

                  },

                  errorPlacement: function (error, element) {

                        var name = $(element).attr("name");
                        
                        error.appendTo($("#" + name + "_validate"));
                  },

                  submitHandler: function(form, event) {

                        event.preventDefault();

                        $(form).ajaxSubmit({

                              cache: false,

                              url: '../museumSmartAdmin/dist/default/private/users/web/newsletter.php',

                              type: 'post',

                              dataType: 'json',

                              data: {},

                              success: function(response, status, xhr, $form) {

                                    console.log(response);

                                    $('#newsletter-form')[0].reset();

                                    helpers.showToastr('te suscribiste al newsletter del Museo del Holocausto.', 'suscripción exitosa.')


                              },

                              error: function(response, status, xhr, $form){

                                    console.log("hay error");

                                    console.log(response);
                              }

                        });


                  }

            });

      }


      /**
      * @function addListeners
      * @description initialize all the listeners, methods, initializars for this page.
      */

      var addListeners = function(){

            

            $('[data-toggle="tooltip"]').tooltip();

            $('.atcb-link').css('font-family', 'CooperHewitt-Light, Arial, sans-serif');

            setTimeout(

                  function() {

                        $('.atcb-item-link').css('font-family', 'CooperHewitt-Light, Arial, sans-serif');

                  }, 

                  500);


            var img;


            if(helpers.isRetina()){

                  img =  $('#item-img').val();

            }else{

                  img =  $('#item-img-retina').val();

            }

            $('#background-image-main-container').css('background-image', 'url(' + img + ')');

            var $grid = $('.grid').imagesLoaded( function() {

                  var $grid = $('.grid').isotope({

                       itemSelector: '.event-item',

                       transformsEnabled: false,

                       layoutMode: 'fitRows',

                       getSortData: {

                       }

                 });

            });

            $grid.on( 'arrangeComplete', function( event, filteredItems ) {

                  var resultCount = filteredItems.length;

                  if(resultCount == 0) {

                        $('#no-results').removeClass('d-none');

                  }

            });

            $('#filters').on( 'click', 'button', function() {

                  var index = $(this).index();

                  var filterValue = $( this ).attr('data-filter');

                  $grid.isotope({ filter: filterValue});

                  $('#no-results').addClass('d-none');

            });


            $('.button-group').each( function( i, buttonGroup ) {

                  var $buttonGroup = $( buttonGroup );

                  $buttonGroup.on( 'click', 'button', function() {

                        $buttonGroup.find('.is-checked').removeClass('is-checked');

                        $( this ).addClass('is-checked');

                  });

            });

            
            $('#filters button').eq(0).trigger('click');

            cursorFocus($('#filters button').eq(0));


            $('.js-scroll-trigger').click(function() {

                  var navHeight = $('.navbar').height();

                  var $window = $(window);

                  TweenMax.to($window, 1.5, {

                        scrollTo : { y: $('.my-overlay').height() - navHeight},

                        ease: Power1.easeOut,

                  });

            });

      }

      /**
      * @function updateURLParameter
      * @description Update the URL.
      */

      var updateURLParameter = function(url, param, paramVal){

            var newAdditionalURL = "";
            
            var tempArray = url.split("?");
            
            var baseURL = tempArray[0];
            
            var additionalURL = tempArray[1];
            
            var temp = "";
            if (additionalURL) {

                  tempArray = additionalURL.split("&");

                  for (var i=0; i<tempArray.length; i++){

                        if(tempArray[i].split('=')[0] != param){

                              newAdditionalURL += temp + tempArray[i];
                              temp = "&";
                        }
                  }
            }

            var rows_txt = temp + "" + param + "=" + paramVal;
            
            return baseURL + "?" + newAdditionalURL + rows_txt;
      }

      /**
      * @function cursorFocus
      * @description do cursor focus...
      */

      var cursorFocus = function(elem) {

            var x = window.scrollX, y = window.scrollY;

            elem.focus();

            window.scrollTo(x, y);

      }

      /**
      * @function createSlickSliders
      * @description Create all the Slick Sliders for the page.
      */

      var createSlickSliders = function(){

            $('.mk-item').slick(config.getInlineSlider());

      };


      /**
      * @function createScrollRevealItems
      * @description Create all the scroll reveal settings.
      */

      var createScrollRevealItems = function(){

            window.sr = ScrollReveal();
            
            /*sr.reveal('.my-overlay', {delay:0, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.main-text', {delay:200, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.main-separator', {delay:300, duration:1000, opacity:0, easing: 'ease'});
            
            sr.reveal('.second-text', {delay:400, duration:1000, opacity:0, easing: 'ease'});
            
            sr.reveal('.third-text', {delay:400, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.read-main-news', {delay:500, duration:1000, scale:.8, opacity:0, easing: 'ease', interval: 50});

            sr.reveal('.see-all', {delay:600, duration:1000, scale:.8, opacity:0, easing: 'ease', interval: 50});

            sr.reveal('.filter-button', {delay:300, duration:1000, opacity:0, easing: 'ease', interval: 50});*/

            //sr.reveal('.event-item', {delay:300, duration:1000, scale:.8, opacity:0, easing: 'ease', interval: 30});



            

      }

      return {

            init: function() {

                  addListeners();

                  createScrollRevealItems();

                  createSlickSliders();

                  formValidation();

                  helpers.manageNavCollapse();

                  $(window).scroll(helpers.manageNavCollapse);

                  //helpers.bodySmoothScroll();


            }

      };

}();

jQuery(document).ready(function() { SectionPage.init(); });