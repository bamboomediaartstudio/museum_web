/**
* @summary Individual virtual class -  Museo Del Holocausto / Bueno sAires / Argentina.
*
* @description -
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}
*
* @todo Complete documentation.
*/

"use strict";

/**
* @function IndividualClass
* @description Initialize and include all the methods for this page.
*/

var IndividualClass = function() {

      var config = new GeneralConfiguration();

      var helpers = new Helpers();

      //var myForm = $('#virtual-class-inscription-form');

      var path = location.protocol + '//' + location.host;

      var callReference;

      var dayWasSelected = false;

      var groupDayWasSelected = false;

      var groupBookingsArray = new Array();

      var readableHour;

      var readableDay;

      var actualBookingId;

      var classId;

      var className;


      /**
      * @function formValidation
      * @description Initialize the validation plugin for the inscription form.
      */

      var formValidation = function(myForm, bookingType, rulesObject, messagesObject){


            var url;

            if(bookingType == 1){

                  url = '../../museumSmartAdmin/dist/default/private/users/museum/classes/add_class_reservation_for_institutions.php';

            }else{

                  url = '../../museumSmartAdmin/dist/default/private/users/museum/classes/add_class_reservation.php'
            }

            myForm.validate({

                  rules: rulesObject,

                  messages: messagesObject,

                  errorPlacement: function(error, element) {

                        if (element.attr("name") == "visit-reason" ){

                              error.insertAfter(".payment-error");

                        }else{

                              error.insertAfter(element);

                        }

                  },

                  highlight: function(element) {


                        $(element).closest('.form-group').addClass('has-error');

                  },

                  unhighlight: function(element) {

                        $(element).closest('.form-group').removeClass('has-error');

                  },

                  invalidHandler: function(e, r) {

                        helpers.doPopUp('Error en el formulario.', 'Hay algunos errores en el formulario. Por favor, revisalo.', 'ENTENDIDO', 'error');

                        return;

                  },

                  submitHandler: function(form, event) {

                        event.preventDefault();

                        if(dayWasSelected == false){

                              helpers.doPopUp('FALTA ELEGIR EL TURNO', 'Por favor, seleccioná uno de los turnos disponibles para el día seleccionado', 'CERRAR', 'error');

                        }else{

                              if(bookingType == 1){

                                    $('#virtual-class-reservation-institution').modal('hide');

                              }else{

                                    $('#virtual-class-reservation').modal('hide');

                              }


                              helpers.blockStage('ENVIANDO...');

                              $(form).ajaxSubmit({

                                    cache: false,

                                    url: url,

                                    type: 'post',

                                    data: {id:classId, bookingId:parseInt(actualBookingId), class: className, readableDay: readableDay, readableHour:readableHour, bookingType:bookingType},

                                    dataType: 'json',

                                    success: function(response, status, xhr, $form) {

                                          
                                          helpers.doPopUp('Inscripción enviada!', 'Recibimos tu inscripción. Te enviamos un correo con información adicional. Si no recibís el correo, no olvides revisar tu carpeta de spam.', 'ENTENDIDO', 'success');

                                          myForm[0].reset();

                                          helpers.unblockStage();

                                          dayWasSelected = false;

                                          groupDayWasSelected = false;

                                          $(".date-selector-div").each(function() { $(this).removeClass('selected-date') });

                                          $('#m_calendar_6, #m_calendar_7').fullCalendar('removeEvents');

                                          $('#m_calendar_6, #m_calendar_7').fullCalendar('refetchEvents');

                                          $('#m_calendar_6, #m_calendar_7').fullCalendar('destroy');

                                          $('.days-checker').empty();

                                          $(".events-by-day").empty();

                                          groupBookingsArray = [];

                                          

                                    },

                                    error: function(response, status, xhr, $form){


                                          console.log(response, status, xhr, $form);

                                     }

                              });

                        }

                  }

            });

      }


      /**
      * @function addListeners
      * @description initialize all the listeners, methods, initializers for this page.
      */

      var addListeners = function(){


            $('body').on('click', '.date-selector-div', function (event) {

                  var numberPattern = /\d+/g;

                  var id = $(this).attr('id').match(numberPattern)

                  readableDay = $(this).attr('date-readable-date');

                  readableHour = $(this).attr('date-hour-string');

                  actualBookingId = id;

                  $(".booking-id").val(id);

                  dayWasSelected = true;

                  groupDayWasSelected = true;

                  $(".date-selector-div").each(function() { $(this).removeClass('selected-date') });

                  $(this).addClass('selected-date');

            })

            if($('#links')[0]){

                  document.getElementById('links').onclick = function (event) {

                        event = event || window.event;

                        var target = event.target || event.srcElement;

                        var link = target.src ? target.parentNode : target;

                        var options = { index: link,  event: event, transitionSpeed: 400 };

                        var links = this.getElementsByTagName('a');

                        blueimp.Gallery(links, options);

                  };

            }

            var img;


            if(helpers.isRetina()){

                  img =  $('#class-img').val();

            }else{

                  img =  $('#class-img-retina').val();

            }


            $('#background-image-main-container').css('background-image', 'url(' + img + ')');


            $('.js-scroll-trigger').click(function() {

                  var navHeight = $('.navbar').height();

                  var $window = $(window);

                  TweenMax.to($window, 1.5, {

                        scrollTo : { y: $('.my-overlay').height() - navHeight},

                        ease: Power1.easeOut

                  });

            });



            $('.go-to-virtual-reservation-institution').click(function(){


                 $('#virtual-class-reservation-institution').modal('show');

                 initCalendar(1, $('#m_calendar_7'));

            });


            $('.go-to-virtual-reservation').click(function(){

                  $('#virtual-class-reservation').modal('show');

                  initCalendar(2, $('#m_calendar_6'));

            });


      };

      /**
      * @function initCalendar
      * @description Init the calendar :D
      */

      var initCalendar = function(sid, cal) {


            cal.fullCalendar({

                  events: function(start, end, timezone, callback) {

                        jQuery.ajax({

                              url: path + '/museumSmartAdmin/dist/default/private/users/services/get_list_of_virtual_classes_events.php',

                              type: 'POST',

                              dataType: 'json',

                              data: {condition:'before', start: start.format(), end: end.format(), id:classId, bookingType:sid },

                              success: function(doc) {

                                    
                                    var events = [];

                                    $.map(doc, function( r ) {

                                          
                                          if(r.available_places == r.booked_places) return;

                                          events.push({

                                                category_id: 1,

                                                allDay:true,

                                                rendering: 'background',

                                                id: r.id,

                                                title: r.title,

                                                start: r.date_start,

                                                className : '"m-fc-event--success"',

                                                color: '#ccc',

                                                end: r.date_end,

                                                end_copy: r.date_end,

                                                available_places: r.available_places,

                                                guide_name: r.guide_name,

                                                booked_places: r.booked_places,

                                                booking_type: r.booking_type,

                                                tickets_per_visitor: 1

                                          });

                                    });

                                    callback(events);
                              }

                        });

                  },

                  dayClick: function(date, jsEvent, view) {

                        dayWasSelected = false;

                        groupDayWasSelected = false;

                        var parsedEvents = new Array();

                        cal.fullCalendar('clientEvents', function (event) {

                              if (moment(event.start).isSame(date, 'day')) {

                                    var object = new Object();

                                    object.id = event.id;

                                    object.title = event.title;

                                    object.category_id = event.category_id;

                                    object.available_places = event.available_places;

                                    object.booked_places = event.booked_places;

                                    object.booked_places = event.booked_places;

                                    object.guide_name = event.guide_name;

                                    object.tickets_per_visitor = event.tickets_per_visitor;

                                    object.start = event.start;

                                    object.end_copy = event.end_copy;
                                    
                                    object.booking_type = event.booking_type;


                                    object.place = ((event.category_id == 5) ? 'virtual: tu pc o celular.' : event.place);

                                    var startingAt  = moment(event.start._i);

                                    var endingAt = moment(event.end_copy);

                                    var ms = moment(endingAt,"DD/MM/YYYY HH:mm:ss").diff(moment(startingAt,"DD/MM/YYYY HH:mm:ss"));

                                    var d = moment.duration(ms);

                                    object.duration = Math.floor(d.asHours()) + moment.utc(ms).format(":mm") + ' hs.';

                                    object.start_hour = moment(event.start._i).format('hh:mm a');

                                    object.end_hour = moment(event.end_copy).format('hh:mm a');

                                    object.readable_date = event.start.format('ll');

                                    parsedEvents.push(object);
                              }


                        })

                        createEventsByDay(parsedEvents);

                  },

                  selectable: true,

                  height: 380,

                  eventLimit: true,

                  navLinks: false,

                  themeSystem: 'bootstrap4',

                  slotDuration: '00:15:00',

                  bootstrapFontAwesome: {

                        close: 'fa-times',

                        prev: 'fa-chevron-left',

                        next: 'fa-chevron-right',

                        prevYear: 'fa-angle-double-left',

                        nextYear: 'fa-angle-double-right'
                  }

            });

            callReference = cal;

      }

      /**
      * @function createEventsByDay
      * @description create all the events by day...
      */

      var createEventsByDay = function(parsedEvents){

            
            TweenMax.to($(".events-by-day"), .2, {alpha:0, onComplete:generateList});

            function generateList(){

                  $(".events-by-day").empty();

                  var textInfo = '';

                  if(parsedEvents.length == 0){

                        textInfo = 'No quedan visitas guiadas para este día. Los días que aparecen coloreados en el calendario son los que aún cuentan con disponibilidad.';

                  }else if(parsedEvents.length == 1){

                        textInfo = 'Hay un único horario disponible para el día ' + parsedEvents[0].readable_date;

                  }else{

                        textInfo = 'Seleccioná una de las ' + parsedEvents.length  + ' opciones disponibles para el día ' + parsedEvents[0].readable_date;

                  }

                  $(".events-by-day").append('<p class="mt-1 mr-2">' + textInfo + '</p>');

                  for(var i= 0; i<parsedEvents.length; i++){

                        var isInArray = false;

                        for(var j = 0; j<groupBookingsArray.length; j++){

                              if(parsedEvents[i].id == groupBookingsArray[j]) isInArray = true;

                        }
                        
                        var startAt = parsedEvents[i].start_hour;

                        var endAt = parsedEvents[i].end_hour;

                        var duration = parsedEvents[i].duration;

                        
                        var name = (parsedEvents[i].guide_name == null) ? 'sin asignar' : parsedEvents[i].guide_name;

                        var itemId = 'day_selector_n_' + parsedEvents[i].id;

                        var available = parseInt(parsedEvents[i].available_places) - parseInt(parsedEvents[i].booked_places);


                        var bookings = "<b>turnos: </b>" + available + ' disponibles de ' + parsedEvents[i].available_places;
                       
                       console.log(parsedEvents[i].available_places + " - " + parsedEvents[i].booked_places);  

                        //var bookings = 'turnos disponibles';

                        var hourString = 'de ' + startAt + " a " + endAt + '  <strong> (duración:</strong> ' + duration + ')';

                        var check = parseInt(parsedEvents[i].booked_places) + parseInt(parsedEvents[i].tickets_per_visitor);

                        var applyMax;

                        if(check > parseInt(parsedEvents[i].available_places)){

                              applyMax = parseInt(parsedEvents[i].available_places) - parseInt(parsedEvents[i].booked_places);

                        }else{

                              applyMax = parsedEvents[i].tickets_per_visitor;

                        }

                        if(parsedEvents[i].booking_type == 1){

                              $(".events-by-day").append('<div id=' + itemId + ' class="date-selector-div" date-readable-date="' + parsedEvents[i].readable_date + '" date-hour-string="'+ hourString +'"><div class="row col-12  mt-3"><p class="date-selector-div-text-info text-light col-12 mt-3"><i class="fas fa-map-marker-alt mr-2"></i><i class="fas fa-calendar-alt mr-2"></i><strong>día: </strong>' + parsedEvents[i].readable_date + '<br><i class="fas fa-clock mr-2"></i><strong>horario:</strong> ' + hourString + ' <br><i class="fas fa-ticket-alt mr-2"></i><strong>turno disponible!</strong><br><i class="fas fa-user mr-2"></i><strong>guía:</strong>: ' + name + '</div></div>');

                        }else{

                              $(".events-by-day").append('<div id=' + itemId + ' class="date-selector-div" date-readable-date="' + parsedEvents[i].readable_date + '" date-hour-string="'+ hourString +'"><div class="row col-12  mt-3"><p class="date-selector-div-text-info text-light col-12 mt-3"><i class="fas fa-map-marker-alt mr-2"></i><i class="fas fa-calendar-alt mr-2"></i><strong>día: </strong>' + parsedEvents[i].readable_date + '<br><i class="fas fa-clock mr-2"></i><strong>horario:</strong> ' + hourString + ' <br><i class="fas fa-ticket-alt mr-2"></i>' + bookings + '<br><i class="fas fa-user mr-2"></i><strong>guía:</strong>: ' + name + '</div></div>');

                        }




                  }

                  TweenMax.to($(".events-by-day"), .2, {alpha:1});
            }

      }



      /**
      * @function createScrollRevealItems
      * @description Create all the scroll reveal settings.
      */

      var createScrollRevealItems = function(){

            window.sr = ScrollReveal();

            sr.reveal('.my-overlay', {delay:0, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.main-text', {delay:200, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.main-separator', {delay:300, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.second-text', {delay:400, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.header-buttons', {delay:500, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.go-to-content', {delay:600, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.section-heading', {distance:'20px', duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.left-separator', {distance:'20px', delay:100, duration:1000, opacity:0, scale:0, easing: 'ease'});

            sr.reveal('.module-text', {distance:'20px', delay:200, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.tutor-container', {distance:'20px', duration:1000, opacity:0, easing: 'ease', interval: 150});

            sr.reveal('.thumbnail-container', {distance:'125px', duration:1000, opacity:0, scale:.5, easing: 'ease', interval: 60});

            sr.reveal('.gallery-thumb', {distance:'10px', duration:1000, opacity:0, scale:.5, easing: 'ease', interval: 60});

            sr.reveal('#blueimp-video-carousel', {distance:'20px', duration:2000, opacity:0, easing: 'ease'});

            sr.reveal('.at-share-btn-elements', {distance:'20px', duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('#background-image-main-container', {delay:300, scale: 1.1, duration:1000, opacity:0, easing: 'ease'});
      }

      return {

            init: function() {

                  console.log("as the spirit wanes the form appears...");


                  classId = parseInt($('#class-id').val());

                  className = ($('#class-name').val());

                  addListeners();

                  var rulesObject = {

                        'virtual-class-inscription-name': {required: true},

                        'virtual-class-inscription-surname': {required: true},

                        'virtual-class-inscription-email': {required: true, email:true},

                        'virtual-class-inscription-phone': {required: true}
                  }

                  var messagesObject = {

                        'virtual-class-inscription-name': 'El nombre es obligatorio.',

                        'virtual-class-inscription-surname': 'El apellido es obligatorio.',

                        'virtual-class-inscription-email': 'El email es obligatorio.',

                        'virtual-class-inscription-phone': 'El teléfono es obligatorio.'
                  }

                  formValidation($('#virtual-class-inscription-form'), 2, rulesObject, messagesObject);

                  //...

                  var institutionRulesObject = {

                        'institution-virtual-class-inscription-name': {required: true},

                        'institution-virtual-class-inscription-email': {required: true, email:true},

                        'institution-virtual-class-inscription-email-repeat': {required: true, equalTo:'#institution-virtual-class-inscription-email'},

                        'institution-virtual-class-inscription-phone': {required: true},

                        'institution-virtual-class-inscription-name-2': {required: true},

                        'institution-virtual-class-inscription-students': {required: true}

                  }

                  var institutionMessagesObject = {

                        'institution-virtual-class-inscription-name': 'El nombre es obligatorio',

                        'institution-virtual-class-inscription-email': 'El email es obligatorio',

                        'institution-virtual-class-inscription-email-repeat': {

                              required : 'es obligatorio',

                             equalTo: 'Debe coincidir con el campo email.'


                        },

                        'institution-virtual-class-inscription-phone': 'El teléfono es obligatorio',

                        'institution-virtual-class-inscription-name-2': 'El nombre de la institución es obligatorio',

                        'institution-virtual-class-inscription-students': 'Debes indicar la cantidad de alumnos. Cantidad máxima: 95'
                  }

                  formValidation($('#virtual-class-institutions-inscription-form'), 1, institutionRulesObject, institutionMessagesObject);

                  createScrollRevealItems();

                  helpers.manageNavCollapse();

                  $(window).scroll(helpers.manageNavCollapse);

            }

      };

}();

jQuery(document).ready(function() { IndividualClass.init(); });
