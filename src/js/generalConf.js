/**
* @summary general config...manage all the objects from here.
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/

var GeneralConfiguration = function() {

	return {

		getBrandsSliderConfig : function(){

			var configObject = {

				slidesToShow: 8,

				slidesToScroll: 1,

				autoplay: true,

				autoplaySpeed: 2000,

				arrows: false,

				dots: false,

				pauseOnHover: true,

				responsive: [

				{

					breakpoint: 1024,

					settings: {

						slidesToShow: 5

					}

				},

				{
					breakpoint: 768,

					settings: {

						slidesToShow: 3

					}

				},

				{

					breakpoint: 520,

					settings: {

						slidesToShow: 2

					}

				}]

			}

			return configObject;

		},

		getInlineSlider : function(){

			var configObject = { 

				dots:true, 

				arrows: false, 

				slidesToShow: 1,

				slidesToScroll: 1,

				autoplay:false,

				infinite:false

			}

			return configObject;

		},

		getMapStyle: function(){

			return [

			{

				"featureType": "all",

				"elementType": "labels.text.fill",

				"stylers": [

				{

					"saturation": 36

				},{

					"color": "#000000"

				},{

					"lightness": 40

				}

				]

			},
			{

				"featureType": "all",

				"elementType": "labels.text.stroke",

				"stylers": [

				{

					"visibility": "on"

				},

				{

					"color": "#000000"

				},

				{

					"lightness": 16

				}]

			},{

				"featureType": "all",

				"elementType": "labels.icon",

				"stylers": [

				{

					"visibility": "off"

				}

				]

			},{

				"featureType": "administrative",

				"elementType": "geometry.fill",

				"stylers": [

				{

					"color": "#000000"

				},{

					"lightness": 20

				}]

			},{

				"featureType": "administrative",

				"elementType": "geometry.stroke",

				"stylers": [

				{

					"color": "#000000"

				},
				{

					"lightness": 17

				},

				{

					"weight": 1.2

				}

				]

			},
			{

				"featureType": "landscape",

				"elementType": "geometry",

				"stylers": [

				{

					"color": "#000000"

				},

				{

					"lightness": 20

				}

				]

			},{

				"featureType": "poi",

				"elementType": "geometry",

				"stylers": [
				{

					"color": "#000000"

				},
				{

					"lightness": 21

				}

				]

			},

			{

				"featureType": "road.highway",

				"elementType": "geometry.fill",

				"stylers": [{

					"color": "#000000"

				},{

					"lightness": 17

				}]

			},{

				"featureType": "road.highway",

				"elementType": "geometry.stroke",

				"stylers": [

				{

					"color": "#000000"
				},{

					"lightness": 29

				},{

					"weight": 0.2

				}

				]

			},
			{

				"featureType": "road.arterial",

				"elementType": "geometry",

				"stylers": [

				{

					"color": "#000000"

				},
				{

					"lightness": 18

				}

				]

			},

			{

				"featureType": "road.local",

				"elementType": "geometry",

				"stylers": [
				{

					"color": "#000000"

				},
				{

					"lightness": 16

				}
				]

			},
			{
				"featureType": "transit",

				"elementType": "geometry",

				"stylers": [

				{

					"color": "#000000"
				},
				{

					"lightness": 19

				}

				]

			},
			{

				"featureType": "water",

				"elementType": "geometry",

				"stylers": [

				{

					"color": "#000000"

				},

				{

					"lightness": 17

				}

				]

			}

			];
		}

	}

}