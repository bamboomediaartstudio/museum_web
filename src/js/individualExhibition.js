/**
* @summary Individual event -  Museo Del Holocausto / Bueno sAires / Argentina.
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/

"use strict";

/**
* @function IndividualExhibition
* @description Initialize and include all the methods for this page.
*/

var IndividualExhibition = function() {

      var map;

      /*constructors for this page: helpers, config, etc...*/

      var config = new GeneralConfiguration(); 

      var helpers = new Helpers();

      var myForm = $('#request-form');

      /**
      * @function formValidation
      * @description Initialize the validation plugin for the inscription form.
      */

      var formValidation = function(){

            myForm.validate({

                  rules: {

                        'inscription-name': {required: true},

                        'inscription-surname': {required: true},

                        'email': {required: true, email:true},

                        'phone': {required: true},

                        'institution-name' : {required: true},

                        'institution-type' : {required: true},

                        'institution-city' : {required: true},

                        'institution-address' : {required: true}

                  },

                  messages: {

                        'inscription-name': 'El nombre es obligatorio.',

                        'inscription-surname': 'El apellido es obligatorio.',

                        'email': 'El email es obligatorio.',

                        'phone': 'El teléfono es obligatorio.',

                        'institution-name' : 'El nombre de la institución es obligatorio',

                        'institution-type' : '¿Qué tipo de institución es?',

                        'institution-city' : '¿Dónde queda la institución?',

                        'institution-address' : 'La dirección es obligatoria'

                  },

                  errorPlacement: function(error, element) {

                        error.insertAfter(element);

                  },

                  highlight: function(element) {


                        $(element).closest('.form-group').addClass('has-error');

                  },

                  unhighlight: function(element) {

                        $(element).closest('.form-group').removeClass('has-error');

                  },

                  invalidHandler: function(e, r) {

                        helpers.doPopUp('Error en el formulario.', 'Hay algunos errores en el formulario. Por favor, revisalo.', 'ENTENDIDO', 'error');

                        return;

                  },

                  submitHandler: function(form, event) {

                        event.preventDefault();

                        $('.request-modal').modal('hide'); 

                        helpers.blockStage('ENVIANDO MENSAJE...');

                        $(form).ajaxSubmit({

                              cache: false,

                              url: '../../museumSmartAdmin/dist/default/private/users/museum/exhibitions/exhibition_add_new_request.php',

                              type: 'post',

                              data: {},

                              dataType: 'json',

                              success: function(response, status, xhr, $form) {

                                    console.log(response);

                                    console.log("que pasos?");

                                    setTimeout(function() {

                                    helpers.doPopUp('Solicitud enviada!', 'Recibimos tu solicitud. Nos comunicaremos a la brevedad.', 'ENTENDIDO', 'success');

                                    $('#request-form')[0].reset();

                                    helpers.unblockStage();

                                    }, 1000);


                              },

                              error: function(response, status, xhr, $form){ 

                                    console.log(response);

                                    console.log(status);

                                    var err = eval("(" + xhr.responseText + ")");

                                    console.log(err);

                              }

                        });

                  }

            });

      }


      /**
      * @function addListeners
      * @description initialize all the listeners, methods, initializers for this page.
      */

      var addListeners = function(){

            if($('#links')[0]){

                  document.getElementById('links').onclick = function (event) {

                        event = event || window.event;

                        var target = event.target || event.srcElement;

                        var link = target.src ? target.parentNode : target;

                        var options = { index: link,  event: event, transitionSpeed: 400 };

                        var links = this.getElementsByTagName('a');

                        blueimp.Gallery(links, options);

                  };

            }

            var img;

            if(helpers.isRetina()){

                  img =  $('#item-img').val();

            }else{

                  img =  $('#item-img-retina').val();

            }

            //btn-vertical

            
            $('.background-image-main-container').css('background-image', 'url(' + img + ')');

            $('.inscription-to-event').click(function(){

                  $('.request-modal').modal('show'); 

            })

            $('.js-scroll-trigger').click(function() {

                  var navHeight = $('.navbar').height();

                  var $window = $(window);

                  TweenMax.to($window, 1.5, {

                        scrollTo : { y: $('.my-overlay').height() - navHeight},

                        ease: Power1.easeOut
                        
                  });

            });

      };

      /**
      * @function window.initMap
      * @description Init the map. Return if there is no latitude / longitude info.
      */

      window.initMap = function() { 

            var lat = $('#lat').val();

            var long = $('#long').val();

            if(lat == undefined && long == undefined) return;

            var myLatlng = new google.maps.LatLng(lat, long);

            map = new google.maps.Map(document.getElementById('map'), {

                  center: myLatlng,

                  mapTypeId: google.maps.MapTypeId.ROADMAP,

                  zoom: 15,

                  mapTypeControl: false,

                  fullscreenControl: false,

                  zoomControl: false

            });

            var marker = new google.maps.Marker({ position: myLatlng, map: map, text: 'Muestra'});

      }

      /**
      * @function createScrollRevealItems
      * @description Create all the scroll reveal settings.
      */

      var createScrollRevealItems = function(){

            window.sr = ScrollReveal();
            
            sr.reveal('.my-overlay', {delay:0, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.main-text', {delay:200, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.main-separator', {delay:300, duration:1000, opacity:0, easing: 'ease'});
            
            sr.reveal('.second-text', {delay:400, duration:1000, opacity:0, easing: 'ease'});
            
            sr.reveal('.header-buttons', {delay:500, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.go-to-content', {delay:600, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.section-heading', {distance:'20px', duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.left-separator', {distance:'20px', delay:100, duration:1000, opacity:0, scale:0, easing: 'ease'});

            sr.reveal('.module-text', {distance:'20px', delay:200, duration:1000, opacity:0, easing: 'ease'});
            
            sr.reveal('.map-container', {distance:'20px', delay:200, duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.tutor-container', {distance:'20px', duration:1000, opacity:0, easing: 'ease', interval: 150});

            sr.reveal('.thumbnail-container', {distance:'125px', duration:1000, opacity:0, scale:.5, easing: 'ease', interval: 60});
            
            sr.reveal('.gallery-thumb', {distance:'10px', duration:1000, opacity:0, scale:.5, easing: 'ease', interval: 60});

            sr.reveal('#blueimp-video-carousel', {distance:'20px', duration:2000, opacity:0, easing: 'ease'});

            sr.reveal('.at-share-btn-elements', {distance:'20px', duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.background-image-main-container', {delay:300, scale: 1.1, duration:1000, opacity:0, easing: 'ease'});
      }

      /**
      * @function loadVideos
      * @description Load all the videos if there is at least one.
      */

      var loadVideos = function(){

            console.log("load!");

            console.log($('#item-id').val());

            var videos = new Array();

            var request = $.ajax({

                  url: "../../museumSmartAdmin/dist/default/private/users/services/get_list_of_youtube_videos.php",

                  type: "POST",

                  data: {id: $('#item-id').val(), source: 'exhibition_video'},

                  dataType: "json"

            });

            request.done(function(result) {

                  for (var i = 0; i < result.length; i++) {

                        var object = new Object();

                        object.title = result[i].description;

                        object.type = 'text/html';

                        object.youtube = result[i].unique_id;

                        object.href =  'https://www.youtube.com/watch?v=' + result[i].unique_id,

                        object.poster = 'https://img.youtube.com/vi/'+ result[i].unique_id + '/maxresdefault.jpg'

                        videos.push(object);

                  }

                  blueimp.Gallery(videos, { container: '#blueimp-video-carousel', carousel: true });

            });

            request.fail(function(jqXHR, textStatus) { console.log(jqXHR); });

      }

      return {

            init: function() {

                  addListeners();

                  formValidation();

                  createScrollRevealItems();

                  if($('#total-videos').val() > 0) loadVideos();

                  helpers.manageNavCollapse();

                  //helpers.bodySmoothScroll();

                  $(window).scroll(helpers.manageNavCollapse);
            }

      };

}();

jQuery(document).ready(function() { IndividualExhibition.init(); });