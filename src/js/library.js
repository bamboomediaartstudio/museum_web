/**
* @summary La Shoa info -  Museo Del Holocausto / Bueno sAires / Argentina.
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/


"use strict";

/**
* @function InitLibraryPage
* @description Initialize and include all the methods for this page.
*/

var InitLibraryPage = function() {

      /*constructors*/

      var config = new GeneralConfiguration(); 

      var helpers = new Helpers();

      /**
      * @function addListeners
      * @description initialize all the listeners, methods, initializars for this page.
      */

      var addListeners = function(){

            if($('#links')[0]){

                  document.getElementById('links').onclick = function (event) {

                        event = event || window.event;

                        var target = event.target || event.srcElement;

                        var link = target.src ? target.parentNode : target;

                        var options = { index: link,  event: event, transitionSpeed: 400 };

                        var links = this.getElementsByTagName('a');

                        blueimp.Gallery(links, options);

                  };

            }

            $('.js-scroll-trigger').click(function() {

                  var navHeight = $('.navbar').height();

                  var $window = $(window);

                  TweenMax.to($window, 1.5, {

                        scrollTo : { y: $('.my-overlay').height() - navHeight},

                        ease: Power1.easeOut,
                        
                  });

            });

      }

      /**
      * @function animateIn
      * @description Create all the animations for this page.
      */

      var animateIn = function(){

            TweenMax.to('.my-overlay', 2, {alpha:.7, delay:1.3});

            TweenMax.to('.main-text', 2, {alpha:1, delay:1.3});

            TweenMax.to('.main-separator', 2, {alpha:1, delay:1.5});

            TweenMax.to('.second-text', 2, {alpha:1, delay:1.7});

            TweenMax.to('.quote-text', 2, {alpha:1, delay:1.7});

            TweenMax.to('#countdown-container', 2, {alpha:1, delay:2});

            TweenMax.to('.navbar-brand', 2, {alpha:1, delay:2.5});

            TweenMax.to('.navbar-brand, .navbar-nav', 2, {alpha:1, delay:2.5});

            TweenMax.to(".st0", 5, { fill: "rgb(255,255,255)" });
      }

      /**
      * @function createScrollRevealItems
      * @description Create all the scroll reveal settings.
      */

      var createScrollRevealItems = function(){

            window.sr = ScrollReveal();

            sr.reveal('.timeline', {delay:0, duration:2000, opacity:0, easing: 'ease'});

            sr.reveal('.timeline-inverted', {delay:0, duration:2000, opacity:0, easing: 'ease'});

      }

      return {

            init: function() {

                  addListeners();

                  animateIn();

                  helpers.manageNavCollapse();

                  $(window).scroll(helpers.manageNavCollapse);

                  helpers.bodySmoothScroll();

                  var type = window.location.hash.substr(1);

                  helpers.manageHash(type);
            }

      };

}();

jQuery(document).ready(function() { InitLibraryPage.init(); });