/**
 * @summary Main page for Museo Del Holocausto / Bueno sAires / Argentina.
 *
 * @description -
 *
 * @author Mariano Makedonsky <info@aditivointeractivegroup.com>
 *
 * @since  1.0.0
 *
 * @see {@link http://www.aditivointeractivegroup.com}
 *
 * @todo Complete documentation.
 */
 "use strict";

/**
 * @function InitMainPage
 * @description Initialize and include all the methods for this page.
 */

 var InitMainPage = function() {

    /*constructors*/

    var config = new GeneralConfiguration();

    var helpers = new Helpers();

    var myForm = $('#newsletter-form');

    var sponsorsForm = $('#sponsors-form');

    var callReference;

    //var sponsorsForm = $('#sponsors-form');

    //var calendar = $('#m_calendar');

    var calendar;

    var alerts = new Array();

    var alertIndex = 0;

    /**
    * @function formValidation
    * @description Initialize the validation plugin for the inscription form.
    */

    var formValidation = function() {

        $('#sponsors-form').validate({

            rules: {

                'sponsor-modal-name': { required: true },

                'sponsor-modal-surname': { required: true },

                'sponsor-modal-email': { email: true, required: true },

                'sponsor-modal-phone': { required: true },

                'sponsor-type-option': { required: true }
            },

            messages: {

                'sponsor-modal-name': 'El nombre obligatorio',

                'sponsor-modal-surname': 'El apellido es obligatorio',

                'sponsor-modal-email': 'La dirección  de email obligatoria.',

                'sponsor-modal-phone': 'El teléfono es obligatorio.',

                'sponsor-type-option': 'Por favor, seleccioná una de las opciones'
            },

            highlight: function(element) { $(element).closest('.form-group').addClass('has-error'); },

            unhighlight: function(element) { $(element).closest('.form-group').removeClass('has-error'); },

            invalidHandler: function(e, r) { },

            errorPlacement: function(error, element) {

                if (element.attr("name") == "sponsor-type-option") {

                    error.insertAfter(".type-error");

                } else {

                    error.insertAfter(element);

                }
            },

            submitHandler: function(form, event) {

                event.preventDefault();

                helpers.blockStage('ENVIANDO MENSAJE...');

                $('.sponsors-modal').modal('toggle');

                $(form).ajaxSubmit({

                    cache: false,

                    url: 'museumSmartAdmin/dist/default/private/users/web/applicants.php',

                    type: 'post',

                    dataType: 'json',

                    data: {},

                    success: function(response, status, xhr, $form) {

                        helpers.doPopUp('Mensaje Enviado!', 'Recibimos tu mensaje. Nos comunicaremos a la brevdad.', 'ENTENDIDO', 'success');

                        $('#sponsors-form')[0].reset();

                        helpers.unblockStage();

                    },

                    error: function(response, status, xhr, $form) {

                        console.log("hay error");

                        console.log(response);
                    }

                });

            }

        });


        myForm.validate({ 

            rules: {

                email: { email: true, required: true }

            },

            messages: { email: 'Dirección de email inválida.' },

            highlight: function(element) { $(element).closest('.form-group').addClass('has-error'); },

            unhighlight: function(element) { $(element).closest('.form-group').removeClass('has-error'); },

            invalidHandler: function(e, r) { },

            errorPlacement: function(error, element) {

                var name = $(element).attr("name");

                error.appendTo($("#" + name + "_validate"));
            },

            submitHandler: function(form, event) {

                event.preventDefault();

                $(form).ajaxSubmit({

                    cache: false,

                    url: 'museumSmartAdmin/dist/default/private/users/web/newsletter.php',

                    type: 'post',

                    dataType: 'json',

                    data: {},

                    success: function(response, status, xhr, $form) {

                        console.log(response);

                        $('#contact-form')[0].reset();

                        helpers.showToastr('te suscribiste al newsletter del Museo del Holocausto.', 'suscripción exitosa.')


                    },

                    error: function(response, status, xhr, $form) {

                        console.log("hay error");

                        console.log(response);
                    }

                });

            }

        });

    }


        /**
         * @function addListeners
         * @description initialize all the listeners, methods, initializars for this page.
         */

         var addListeners = function() {

            $('.open-sponsors-modal').click(function() { $(".sponsors-modal").modal(); })

            $('[data-toggle="tooltip"]').tooltip()

            $('.individual-reservation').click(function() { });


        }

        /**
         * @function animateIn
         * @description Create all the animations for this page.
         */

         var animateIn = function() {

            TweenMax.to('.my-overlay', 2, { alpha: .7, delay: 1.3 });

            TweenMax.to('.main-text', 2, { alpha: 1, delay: 1.3 });

            TweenMax.to('.main-separator', 2, { alpha: 1, delay: 1.5 });

            TweenMax.to('.second-text', 2, { alpha: 1, delay: 1.7 });

            TweenMax.to('.navbar-brand', 2, { alpha: 1, delay: 2.5 });

            TweenMax.to('.navbar-brand, .navbar-nav', 2, { alpha: 1, delay: 2.5 });

            TweenMax.to(".st0", 5, { fill: "rgb(255,255,255)" });
        }


        /**
         * @function createScrollRevealItems
         * @description Create all the scroll reveal settings.
         */

         var createScrollRevealItems = function() {

            window.sr = ScrollReveal();

            sr.reveal('.module-title', { delay: 0, duration: 1000, opacity: 0, easing: 'ease' });

            sr.reveal('.module-separator', { delay: 100, duration: 1000, opacity: 0, easing: 'ease' });

            sr.reveal('.module-text', { delay: 200, duration: 1000, opacity: 0, easing: 'ease' });

            sr.reveal('.sponsors-logos', { delay: 500, duration: 1000, easing: 'ease', opacity: 0, scale: .95 });

            sr.reveal('.module-down', {delay: 550, duration: 1000, opacity: 0, easing: 'ease'});

            sr.reveal('.sr-icon-1', {duration: 1000, opacity: 0, easing: 'ease'});
            
            sr.reveal('.sr-icon-2', {delay: 200, duration: 1000, opacity: 0, easing: 'ease'});
            
            sr.reveal('.inline-faq-combo', {duration: 1000, opacity: 0, easing: 'ease', interval: 50 });
            
            sr.reveal('.fa-star', {duration: 1000, opacity: 0, easing: 'ease', interval: 50 });
            
            sr.reveal('.android-device', {duration: 1200, delay: 200, opacity: 0, easing: 'ease'});
            
            sr.reveal('.ios-device', {duration: 1200, delay: 300, opacity: 0, easing: 'ease'});
            
            sr.reveal('.app-phone', {duration: 1200, delay: 400, opacity: 0, easing: 'ease'});
            
            sr.reveal('#newsletter-name', {duration: 1200, delay: 100, opacity: 0, easing: 'ease'});

            sr.reveal('#newsletter-email', {duration: 1200, delay: 200, opacity: 0, easing: 'ease'});
            
            sr.reveal('#subscribe-button', {duration: 1200, delay: 300, opacity: 0, easing: 'ease'}); 
        }

        /**
        * @function createSlickSliders
        * @description Create all the Slick Sliders for the page.
        */

        var createSlickSliders = function() {

            $('.sponsors-logos').on('init', function(event, slick, direction) { $('.sponsors-logos').removeClass('d-none'); });

            $('.sponsors-logos').slick(config.getBrandsSliderConfig());

            $('.single-item').slick(config.getInlineSlider());

            $('.mk-item-2').slick(config.getInlineSlider());

            $('.mk-item').slick(config.getInlineSlider());

        };

        /**
        * @function initMap
        * @description Initialize the Google Map and create the marker.
        */

        window.initMap = function() {

            var map;

            var myLatlng = new google.maps.LatLng(-34.562386, -58.447446);

            map = new google.maps.Map(document.getElementById('map'), {

                center: myLatlng,

                zoom: 15,

                mapTypeControl: false,

                fullscreenControl: false,

                zoomControl: false,

                styles: config.getMapStyle()

            });

            var marker = new google.maps.Marker({

                position: myLatlng,

                title: "Museo Del Holocausto / Buenos Aires / Argentina",

                icon: 'private/img/markers/custom.png'

            });

            marker.setMap(map);

        }

        /**
         * @function createAlertMessage
         * @description Create the popup alert :).
         */

         var createAlertMessage = function(alertIndex) {

            var title = alerts[alertIndex].title;

            var caption = alerts[alertIndex].caption;

            var button = alerts[alertIndex].button;

            var background_color = alerts[alertIndex].background_color;

            var text_color = alerts[alertIndex].text_color;

            var link = alerts[alertIndex].link;

            var is_blank = alerts[alertIndex].is_blank;

            $('.alert-title').html(title);

            $('.alert-text').html(caption);

            $('.alert-button').html('CERRAR');

            $('.alert-content').css('background-color', background_color);

            $('.alert-container').removeClass('d-none');

            $('.alert-container').css('opacity', 0);

            $('.alert-content').css('opacity', 0);

            $('.alert-title').css('opacity', 0);

            $('.alert-text').css('opacity', 0);

            $('.alert-button').css('opacity', 0);

            TweenMax.to($('.alert-container'), .3, {opacity: 1, delay: 1 }); TweenMax.to($('.alert-content'), .5, {opacity: 1, delay: 1.3, top: '15%'});
            
            TweenMax.to($('.alert-title'), .5, {opacity: 1, delay: 1.5 });
            
            TweenMax.to($('.alert-text'), .5, {opacity: 1, delay: 1.6 });
            
            TweenMax.to($('.alert-button'), .5, {opacity: 1, delay: 1.7 });

            $('.alert-button').click(function() {

                $('.alert-container').click();

                //(is_blank == 0) ? window.location.href = link : window.open(link, '_blank');

            });

            $('.alert-container').click(function() {

                TweenMax.to($('.alert-container'), .3, {opacity: 0, onComplete: removeItem });
                
                function removeItem() {

                    $('.alert-container').addClass('d-none');

                    if (alertIndex < alerts.length - 1) {

                        alertIndex += 1;

                        createAlertMessage(alertIndex);

                    }
                }

            });

        }

        /**
         * @function loadAlerts
         * @description Load all the alerts...
         */

         var loadAlerts = function() {

            var request = $.ajax({

                url: "museumSmartAdmin/dist/default/private/users/services/get_list_of_alerts.php",

                type: "POST",

                dataType: "json"

            });

            request.done(function(result) {

                for (var i = 0; i < result.length; i++) {

                    var object = new Object();

                    object.title = result[i].title;

                    object.caption = result[i].caption;

                    object.link = result[i].link;

                    object.button = result[i].button;

                    object.background_color = '#' + result[i].background_color;

                    object.text_color = '#' + result[i].text_color;

                    object.is_blank = result[i].is_blank;

                    alerts.push(object);

                }

                if (alerts.length >= 1) createAlertMessage(alertIndex);

            });

            request.fail(function(jqXHR, textStatus) { });

        }

        return {

            init: function() {

                formValidation();

                addListeners();

                animateIn();

                createScrollRevealItems();

                createSlickSliders();

                loadAlerts();

                helpers.manageNavCollapse();

                $(window).scroll(helpers.manageNavCollapse);

                //helpers.bodySmoothScroll();
            }

        };

    }();

    jQuery(document).ready(function() { InitMainPage.init(); });