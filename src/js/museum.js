/**
* @summary Museum info -  Museo Del Holocausto / Bueno sAires / Argentina.
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/


"use strict";

/**
* @function InitMuseumPage
* @description Initialize and include all the methods for this page.
*/

var InitMuseumPage = function() {

      /*constructors*/

      var config = new GeneralConfiguration(); 

      var helpers = new Helpers();

      /**
      * @function addListeners
      * @description initialize all the listeners, methods, initializars for this page.
      */

      var addListeners = function(){


            $('[data-toggle="tooltip"]').tooltip();

            $('.js-scroll-trigger').click(function() {

                  var navHeight = $('.navbar').height();

                  var $window = $(window);

                  TweenMax.to($window, 1.5, {

                        scrollTo : { y: $('.my-overlay').height() - navHeight},

                        ease: Power1.easeOut,

                  });

            });

            var $grid = $('.grid').imagesLoaded( function() {

                  var $grid = $('.grid').isotope({

                        itemSelector: '.element-item',

                        //layoutMode: 'fitRows',
                        
                        layoutMode: 'fitRows',

                        //stamp: '.stamp',

                        //stagger: 50, 

                        getSortData: {

                              dataSortFilter1 : function($elem){

                                    return parseInt($($elem).attr('data-sort-filter-1'));

                              },

                              dataSortFilter2 : function($elem){

                                    return parseInt($($elem).attr('data-sort-filter-2'));

                              },

                              dataSortFilter3 : function($elem){

                                    return parseInt($($elem).attr('data-sort-filter-3'));

                              },

                              dataSortFilter4 : function($elem){

                                    return parseInt($($elem).attr('data-sort-filter-4'));

                              },

                              dataSortFilter5 : function($elem){

                                    return parseInt($($elem).attr('data-sort-filter-5'));

                              }

                        }

                  });


                  var iso = $('.grid').data('isotope');

                  //resetElements(iso.getItemElements());

                  //(!helpers.mobileCheck()) ? sortByPairs(iso.getFilteredItemElements()) : sortForMobile(iso.getFilteredItemElements());

                  $("#button-2").trigger("click");


            });


           /* $grid.on( 'layoutComplete', function( event, laidOutItems ) {

                  var iso = $('.grid').data('isotope');

                  resetElements(iso.getItemElements());

                  (!helpers.mobileCheck()) ? sortByPairs(iso.getFilteredItemElements()) : sortForMobile(iso.getFilteredItemElements());


            } );*/


            /*$grid.on( 'arrangeComplete', function() {

                  var iso = $('.grid').data('isotope');

                  resetElements(iso.getItemElements());

                  (!helpers.mobileCheck()) ? sortByPairs(iso.getFilteredItemElements()) : sortForMobile(iso.getFilteredItemElements());

                  
            });*/

            $('.staff-member-email').on('click', function(){

                  $(this).attr("title", "Email copiado!").tooltip("_fixTitle").tooltip("show").attr("title", "Click para copiar.").tooltip("_fixTitle");

                  var string = helpers.removeWhiteSpace($(this).text());

                  helpers.copyToClipboard(string);

            });

            $('#filters').on( 'click', 'button', function() {

                  var filterValue = $( this ).attr('data-filter');

                  var functionToCall = $(this).attr('data-function');

                  $grid.isotope({ filter: filterValue});

                  $grid.isotope({ sortBy: functionToCall});

                  var iso = $('.grid').data('isotope');

                  //resetElements(iso.getItemElements());

                  //(!helpers.mobileCheck()) ? sortByPairs(iso.getFilteredItemElements()) : sortForMobile(iso.getFilteredItemElements());

            });

            
      }

      $('.filters-button-group').each( function( i, buttonGroup ) {

            var $buttonGroup = $( buttonGroup );

            $buttonGroup.on( 'click', 'button', function() {

                  $buttonGroup.find('.is-checked').removeClass('is-checked');

                  $( this ).addClass('is-checked');

            });

      });

          /*setTimeout(function() {

            $("#button-2").trigger("click");

      }, 3000);*/






      /**
      * @function resetElements
      * @description Reset all the divs to its initial status and floating properties.
      * @param elements {Array} - Array with all the elements to reset.
      */

      var resetElements = function(elements){

            for(var j=0; j<elements.length; j++){

                  if($(elements[j]).find('.left-side').hasClass('float-right')){

                        $(elements[j]).find('.left-side').removeClass('float-right');

                        $(elements[j]).find('.left-side').addClass('float-left');

                  }

                  if($(elements[j]).find('.right-side').hasClass('float-left')){

                        $(elements[j]).find('.right-side').removeClass('float-left');

                        $(elements[j]).find('.right-side').addClass('float-right');

                  }

            }

      }

      /**
      * @function sortForMobile
      * @description We are using a different logic to organize mobile, so we use the module %
      * just to check for pairs...
      * @param elements {Array} - Array with all the elements to modify.
      */

      var sortForMobile = function(elements){

            var i;

            for (i = 0; i < elements.length; ++i) {

                  if(i%2 == 0){

                        $(elements[i]).find('.left-side').removeClass('float-left');

                        $(elements[i]).find('.left-side').addClass('float-right');

                        $(elements[i]).find('.right-side').removeClass('float-right');

                        $(elements[i]).find('.right-side').addClass('float-left');
                  }

            }

      }

      /**
      * @function sortByPairs
      * @description Hacky approach to loop and modify content by pairs.
      * @param elements {Array} - Array with all the elements to modify.
      */

      var sortByPairs = function(elements){

            var isWaitMode = false;

            var i;

            for (i = 0; i < elements.length; ++i) {

                  var test = $(elements[i]).attr('data-sort-filter-1');

                  if((i!=0 && i%2==0)){

                        if(isWaitMode){

                              isWaitMode = false;

                        }else{

                              $(elements[i]).find('.left-side').removeClass('float-left');

                              $(elements[i]).find('.left-side').addClass('float-right');

                              $(elements[i]).find('.right-side').removeClass('float-right');

                              $(elements[i]).find('.right-side').addClass('float-left');

                              try{

                                    $(elements[i+1]).find('.left-side').removeClass('float-left');

                                    $(elements[i+1]).find('.left-side').addClass('float-right');

                                    $(elements[i+1]).find('.right-side').removeClass('float-right');

                                    $(elements[i+1]).find('.right-side').addClass('float-left');

                              }catch(e){

                              }

                              isWaitMode = true;

                        }


                  }

            } 

      }

      /**
      * @function animateIn
      * @description Create all the animations for this page.
      */

      var animateIn = function(){

            TweenMax.to('.my-overlay', 2, {alpha:.7, delay:1.3});

            TweenMax.to('.main-text', 2, {alpha:1, delay:1.3});

            TweenMax.to('.main-separator', 2, {alpha:1, delay:1.5});

            TweenMax.to('.second-text', 2, {alpha:1, delay:1.7});

            TweenMax.to('.quote-text', 2, {alpha:1, delay:1.7});

            TweenMax.to('#countdown-container', 2, {alpha:1, delay:2});

            TweenMax.to('.navbar-brand', 2, {alpha:1, delay:2.5});

            TweenMax.to('.navbar-brand, .navbar-nav', 2, {alpha:1, delay:2.5});

            TweenMax.to(".st0", 5, { fill: "rgb(255,255,255)" });
      }

      /**
      * @function createScrollRevealItems
      * @description Create all the scroll reveal settings.
      */

      var createScrollRevealItems = function(){

            window.sr = ScrollReveal();

            sr.reveal('.main-text-section', {delay:0, duration:1000, scale:.90, opacity:0, easing: 'ease'});

            sr.reveal('.service-box', {delay:0, duration:1200, opacity:0, easing: 'ease', interval: 50});

            sr.reveal('.main-separator', {delay:100, duration:1000, scale:.90, opacity:0, easing: 'ease'});

            sr.reveal('.go-to-content', {delay:400, duration:1000, scale:.80, opacity:0, easing: 'ease'});

            sr.reveal('.content-title', {delay:0, duration:1000,  opacity:0, easing: 'ease'});

            sr.reveal('.content-separator', {delay:150, duration:1000,  opacity:0, easing: 'ease'});

            sr.reveal('.content-text', {delay:300, duration:1000,  opacity:0, easing: 'ease'});

            sr.reveal('.section-heading', {duration:1000,  opacity:0, easing: 'ease'});

            sr.reveal('.module-separator', {duration:1000,  opacity:0, easing: 'ease'});

            sr.reveal('.objective-container', {delay:300, duration:1200, opacity:0, easing: 'ease', interval: 50});
            
            sr.reveal('.staff-button', {delay:300, duration:1200, opacity:0, easing: 'ease', interval: 50});

            sr.reveal('.about-staff', {delay:300, duration:1200, opacity:0, easing: 'ease'});

      }

      return {

            init: function() {

                  addListeners();

                  animateIn();

                  createScrollRevealItems();

                  helpers.manageNavCollapse();

                  $(window).scroll(helpers.manageNavCollapse);

                  helpers.bodySmoothScroll();

                  var type = window.location.hash.substr(1);

                  helpers.manageHash(type);

            }

      };

}();

jQuery(document).ready(function() { InitMuseumPage.init(); });