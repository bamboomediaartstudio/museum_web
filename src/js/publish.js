/**
* @summary La Shoa info -  Museo Del Holocausto / Bueno sAires / Argentina.
*
* @description -  
*
* @author Mariano Makedonsky <info@aditivointeractivegroup.com>
*
* @since  1.0.0
*
* @see {@link http://www.aditivointeractivegroup.com}  
*
* @todo Complete documentation. 
*/


"use strict";

/**
* @function InitShoaPage
* @description Initialize and include all the methods for this page.
*/

var InitShoaPage = function() {

      /*constructors*/

      var config = new GeneralConfiguration(); 

      var helpers = new Helpers();

      /**
      * @function addListeners
      * @description initialize all the listeners, methods, initializars for this page.
      */

      var addListeners = function(){

            $('.read-info').click(function(){
                  
                  console.log("read info heere");

                  var description = $(this).attr('data-info');

                  var title = $(this).attr('data-title');

                  
                  $('.modal').modal();

                  $('.modal').find('.modal-title').text(title);

                  $('.modal').find('.modal-body').text(description);

                  console.log("description: " + description);

            })

            /*$.ajax({ 

                  type: 'POST', 

                  url: '../../private/json/cuadernos.json', 
                  
                  data: { get_param: 'value' }, 
                  
                  dataType: 'json',
                  
                  success: function (data) { 

                        var j  = JSON.stringify(data);

                        console.log(j);

                  }
            
            });*/

      }

      /**
      * @function animateIn
      * @description Create all the animations for this page.
      */

      var animateIn = function(){

            TweenMax.to('.my-overlay', 2, {alpha:.7, delay:1.3});

            TweenMax.to('.main-text', 2, {alpha:1, delay:1.3});

            TweenMax.to('.main-separator', 2, {alpha:1, delay:1.5});

            TweenMax.to('.second-text', 2, {alpha:1, delay:1.7});

            TweenMax.to('.quote-text', 2, {alpha:1, delay:1.7});

            TweenMax.to('#countdown-container', 2, {alpha:1, delay:2});

            TweenMax.to('.navbar-brand', 2, {alpha:1, delay:2.5});

            TweenMax.to('.navbar-brand, .navbar-nav', 2, {alpha:1, delay:2.5});

            TweenMax.to(".st0", 5, { fill: "rgb(255,255,255)" });
      }

      /**
      * @function createScrollRevealItems
      * @description Create all the scroll reveal settings.
      */

      var createScrollRevealItems = function(){

            window.sr = ScrollReveal();

            sr.reveal('.books-image', {delay:0, duration:1000, scale:.95, opacity:0, easing: 'ease'});
            
            sr.reveal('.section-title', {delay:100, duration:1000, scale:.95, opacity:0, easing: 'ease'});
            
            sr.reveal('.section-text', {delay:200, duration:1000, scale:.95, opacity:0, easing: 'ease'});
            
            sr.reveal('.more-information', {duration:1000, scale:.95, opacity:0, easing: 'ease'});

            sr.reveal('.info-block', {duration:1000, scale:.95, opacity:0, easing: 'ease', interval:50});

            sr.reveal('.cuadernos-title', {duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.cuadernos-info-content', {duration:1000, opacity:0, easing: 'ease'});

            sr.reveal('.cuadernos-list-item', {duration:1000, scale:.95, opacity:0, easing: 'ease', interval:200});

            sr.reveal('.cuaderno-image-contenet', {duration:1000, scale:.95, opacity:0, easing: 'ease', interval:200});


      }

      return {

            init: function() {

                  addListeners();

                  animateIn();

                  createScrollRevealItems();

                  helpers.manageSmallNavCollapse();

                  $(window).scroll(helpers.manageNavCollapse);

                  //helpers.bodySmoothScroll();

            }

      };

}();

jQuery(document).ready(function() { InitShoaPage.init(); });